.class public final Landroid/support/v7/appcompat/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/appcompat/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f0a0085

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f0a0086

.field public static final abc_input_method_navigation_guard:I = 0x7f0a0000

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f0a0087

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f0a0088

.field public static final abc_primary_text_material_dark:I = 0x7f0a0089

.field public static final abc_primary_text_material_light:I = 0x7f0a008a

.field public static final abc_search_url_text:I = 0x7f0a008b

.field public static final abc_search_url_text_normal:I = 0x7f0a0001

.field public static final abc_search_url_text_pressed:I = 0x7f0a0002

.field public static final abc_search_url_text_selected:I = 0x7f0a0003

.field public static final abc_secondary_text_material_dark:I = 0x7f0a008c

.field public static final abc_secondary_text_material_light:I = 0x7f0a008d

.field public static final accent_material_dark:I = 0x7f0a0006

.field public static final accent_material_light:I = 0x7f0a0007

.field public static final background_floating_material_dark:I = 0x7f0a0008

.field public static final background_floating_material_light:I = 0x7f0a0009

.field public static final background_material_dark:I = 0x7f0a000a

.field public static final background_material_light:I = 0x7f0a000b

.field public static final bright_foreground_disabled_material_dark:I = 0x7f0a0010

.field public static final bright_foreground_disabled_material_light:I = 0x7f0a0011

.field public static final bright_foreground_inverse_material_dark:I = 0x7f0a0012

.field public static final bright_foreground_inverse_material_light:I = 0x7f0a0013

.field public static final bright_foreground_material_dark:I = 0x7f0a0014

.field public static final bright_foreground_material_light:I = 0x7f0a0015

.field public static final button_material_dark:I = 0x7f0a0016

.field public static final button_material_light:I = 0x7f0a0017

.field public static final dim_foreground_disabled_material_dark:I = 0x7f0a002c

.field public static final dim_foreground_disabled_material_light:I = 0x7f0a002d

.field public static final dim_foreground_material_dark:I = 0x7f0a002e

.field public static final dim_foreground_material_light:I = 0x7f0a002f

.field public static final highlighted_text_material_dark:I = 0x7f0a0034

.field public static final highlighted_text_material_light:I = 0x7f0a0035

.field public static final hint_foreground_material_dark:I = 0x7f0a0036

.field public static final hint_foreground_material_light:I = 0x7f0a0037

.field public static final link_text_material_dark:I = 0x7f0a0044

.field public static final link_text_material_light:I = 0x7f0a0045

.field public static final material_blue_grey_800:I = 0x7f0a0048

.field public static final material_blue_grey_900:I = 0x7f0a0049

.field public static final material_blue_grey_950:I = 0x7f0a004a

.field public static final material_deep_teal_200:I = 0x7f0a004b

.field public static final material_deep_teal_500:I = 0x7f0a004c

.field public static final primary_dark_material_dark:I = 0x7f0a005a

.field public static final primary_dark_material_light:I = 0x7f0a005b

.field public static final primary_material_dark:I = 0x7f0a005c

.field public static final primary_material_light:I = 0x7f0a005d

.field public static final primary_text_default_material_dark:I = 0x7f0a005e

.field public static final primary_text_default_material_light:I = 0x7f0a005f

.field public static final primary_text_disabled_material_dark:I = 0x7f0a0060

.field public static final primary_text_disabled_material_light:I = 0x7f0a0061

.field public static final ripple_material_dark:I = 0x7f0a0065

.field public static final ripple_material_light:I = 0x7f0a0066

.field public static final secondary_text_default_material_dark:I = 0x7f0a0067

.field public static final secondary_text_default_material_light:I = 0x7f0a0068

.field public static final secondary_text_disabled_material_dark:I = 0x7f0a0069

.field public static final secondary_text_disabled_material_light:I = 0x7f0a006a

.field public static final switch_thumb_normal_material_dark:I = 0x7f0a0074

.field public static final switch_thumb_normal_material_light:I = 0x7f0a0075
