.class public abstract Landroid/support/v4/preference/PreferenceFragment;
.super Landroid/support/v4/app/Fragment;
.source "PreferenceFragment.java"

# interfaces
.implements Landroid/support/v4/preference/PreferenceManagerCompat$OnPreferenceTreeClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v4/preference/PreferenceFragment$OnPreferenceStartFragmentCallback;
    }
.end annotation


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mHavePrefs:Z

.field private mInitDone:Z

.field private mList:Landroid/widget/ListView;

.field private mListOnKeyListener:Landroid/view/View$OnKeyListener;

.field private mPreferenceManager:Landroid/preference/PreferenceManager;

.field private final mRequestFocus:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 57
    new-instance v0, Landroid/support/v4/preference/PreferenceFragment$1;

    invoke-direct {v0, p0}, Landroid/support/v4/preference/PreferenceFragment$1;-><init>(Landroid/support/v4/preference/PreferenceFragment;)V

    iput-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mHandler:Landroid/os/Handler;

    .line 69
    new-instance v0, Landroid/support/v4/preference/PreferenceFragment$2;

    invoke-direct {v0, p0}, Landroid/support/v4/preference/PreferenceFragment$2;-><init>(Landroid/support/v4/preference/PreferenceFragment;)V

    iput-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mRequestFocus:Ljava/lang/Runnable;

    .line 322
    new-instance v0, Landroid/support/v4/preference/PreferenceFragment$4;

    invoke-direct {v0, p0}, Landroid/support/v4/preference/PreferenceFragment$4;-><init>(Landroid/support/v4/preference/PreferenceFragment;)V

    iput-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mListOnKeyListener:Landroid/view/View$OnKeyListener;

    return-void
.end method

.method static synthetic access$000(Landroid/support/v4/preference/PreferenceFragment;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v4/preference/PreferenceFragment;

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/support/v4/preference/PreferenceFragment;->bindPreferences()V

    return-void
.end method

.method static synthetic access$100(Landroid/support/v4/preference/PreferenceFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Landroid/support/v4/preference/PreferenceFragment;

    .prologue
    .line 41
    iget-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mList:Landroid/widget/ListView;

    return-object v0
.end method

.method private bindPreferences()V
    .locals 3

    .prologue
    .line 267
    invoke-virtual {p0}, Landroid/support/v4/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 268
    .local v0, "preferenceScreen":Landroid/preference/PreferenceScreen;
    if-nez v0, :cond_1

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 269
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/preference/PreferenceFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->bind(Landroid/widget/ListView;)V

    .line 271
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xa

    if-gt v1, v2, :cond_0

    .line 274
    invoke-virtual {p0}, Landroid/support/v4/preference/PreferenceFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    new-instance v2, Landroid/support/v4/preference/PreferenceFragment$3;

    invoke-direct {v2, p0, v0}, Landroid/support/v4/preference/PreferenceFragment$3;-><init>(Landroid/support/v4/preference/PreferenceFragment;Landroid/preference/PreferenceScreen;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method

.method private ensureList()V
    .locals 4

    .prologue
    .line 304
    iget-object v2, p0, Landroid/support/v4/preference/PreferenceFragment;->mList:Landroid/widget/ListView;

    if-eqz v2, :cond_0

    .line 320
    :goto_0
    return-void

    .line 307
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/preference/PreferenceFragment;->getView()Landroid/view/View;

    move-result-object v1

    .line 308
    .local v1, "root":Landroid/view/View;
    if-nez v1, :cond_1

    .line 309
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Content view not yet created"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 311
    :cond_1
    const v2, 0x102000a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 312
    .local v0, "rawListView":Landroid/view/View;
    instance-of v2, v0, Landroid/widget/ListView;

    if-nez v2, :cond_2

    .line 313
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Content has view with id attribute \'android.R.id.list\' that is not a ListView class"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 317
    :cond_2
    check-cast v0, Landroid/widget/ListView;

    .end local v0    # "rawListView":Landroid/view/View;
    iput-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mList:Landroid/widget/ListView;

    .line 318
    iget-object v2, p0, Landroid/support/v4/preference/PreferenceFragment;->mList:Landroid/widget/ListView;

    iget-object v3, p0, Landroid/support/v4/preference/PreferenceFragment;->mListOnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 319
    iget-object v2, p0, Landroid/support/v4/preference/PreferenceFragment;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Landroid/support/v4/preference/PreferenceFragment;->mRequestFocus:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private postBindPreferences()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 262
    iget-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    :goto_0
    return-void

    .line 263
    :cond_0
    iget-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method private requirePreferenceManager()V
    .locals 2

    .prologue
    .line 256
    iget-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    if-nez v0, :cond_0

    .line 257
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "This should be called after super.onCreate."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 259
    :cond_0
    return-void
.end method


# virtual methods
.method public addPreferencesFromResource(I)V
    .locals 3
    .param p1, "preferencesResId"    # I

    .prologue
    .line 225
    invoke-direct {p0}, Landroid/support/v4/preference/PreferenceFragment;->requirePreferenceManager()V

    .line 227
    iget-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    invoke-virtual {p0}, Landroid/support/v4/preference/PreferenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-static {v0, v1, p1, v2}, Landroid/support/v4/preference/PreferenceManagerCompat;->inflateFromResource(Landroid/preference/PreferenceManager;Landroid/app/Activity;ILandroid/preference/PreferenceScreen;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v4/preference/PreferenceFragment;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 230
    return-void
.end method

.method public findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;
    .locals 1
    .param p1, "key"    # Ljava/lang/CharSequence;

    .prologue
    .line 249
    iget-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    if-nez v0, :cond_0

    .line 250
    const/4 v0, 0x0

    .line 252
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    goto :goto_0
.end method

.method public getListView()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 299
    invoke-direct {p0}, Landroid/support/v4/preference/PreferenceFragment;->ensureList()V

    .line 300
    iget-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mList:Landroid/widget/ListView;

    return-object v0
.end method

.method public getPreferenceScreen()Landroid/preference/PreferenceScreen;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    invoke-static {v0}, Landroid/support/v4/preference/PreferenceManagerCompat;->getPreferenceScreen(Landroid/preference/PreferenceManager;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 106
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 108
    iget-boolean v2, p0, Landroid/support/v4/preference/PreferenceFragment;->mHavePrefs:Z

    if-eqz v2, :cond_0

    .line 109
    invoke-direct {p0}, Landroid/support/v4/preference/PreferenceFragment;->bindPreferences()V

    .line 112
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Landroid/support/v4/preference/PreferenceFragment;->mInitDone:Z

    .line 114
    if-eqz p1, :cond_1

    .line 115
    const-string v2, "android:preferences"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 116
    .local v0, "container":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 117
    invoke-virtual {p0}, Landroid/support/v4/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 118
    .local v1, "preferenceScreen":Landroid/preference/PreferenceScreen;
    if-eqz v1, :cond_1

    .line 119
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->restoreHierarchyState(Landroid/os/Bundle;)V

    .line 123
    .end local v0    # "container":Landroid/os/Bundle;
    .end local v1    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    :cond_1
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 166
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 168
    iget-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    invoke-static {v0, p1, p2, p3}, Landroid/support/v4/preference/PreferenceManagerCompat;->dispatchActivityResult(Landroid/preference/PreferenceManager;IILandroid/content/Intent;)V

    .line 170
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "paramBundle"    # Landroid/os/Bundle;

    .prologue
    .line 92
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 93
    invoke-virtual {p0}, Landroid/support/v4/preference/PreferenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/16 v1, 0x64

    invoke-static {v0, v1}, Landroid/support/v4/preference/PreferenceManagerCompat;->newInstance(Landroid/app/Activity;I)Landroid/preference/PreferenceManager;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    .line 94
    iget-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    invoke-static {v0, p0}, Landroid/support/v4/preference/PreferenceManagerCompat;->setFragment(Landroid/preference/PreferenceManager;Landroid/support/v4/preference/PreferenceFragment;)V

    .line 95
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "paramLayoutInflater"    # Landroid/view/LayoutInflater;
    .param p2, "paramViewGroup"    # Landroid/view/ViewGroup;
    .param p3, "paramBundle"    # Landroid/os/Bundle;

    .prologue
    .line 100
    const v0, 0x7f04004c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 148
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 149
    iget-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    invoke-static {v0}, Landroid/support/v4/preference/PreferenceManagerCompat;->dispatchActivityDestroy(Landroid/preference/PreferenceManager;)V

    .line 150
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mList:Landroid/widget/ListView;

    .line 141
    iget-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/v4/preference/PreferenceFragment;->mRequestFocus:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 142
    iget-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 143
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 144
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 1
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 236
    invoke-virtual {p0}, Landroid/support/v4/preference/PreferenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Landroid/support/v4/preference/PreferenceFragment$OnPreferenceStartFragmentCallback;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/preference/PreferenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/support/v4/preference/PreferenceFragment$OnPreferenceStartFragmentCallback;

    invoke-interface {v0, p0, p2}, Landroid/support/v4/preference/PreferenceFragment$OnPreferenceStartFragmentCallback;->onPreferenceStartFragment(Landroid/support/v4/preference/PreferenceFragment;Landroid/preference/Preference;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 154
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 156
    invoke-virtual {p0}, Landroid/support/v4/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 157
    .local v1, "preferenceScreen":Landroid/preference/PreferenceScreen;
    if-eqz v1, :cond_0

    .line 158
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 159
    .local v0, "container":Landroid/os/Bundle;
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->saveHierarchyState(Landroid/os/Bundle;)V

    .line 160
    const-string v2, "android:preferences"

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 162
    .end local v0    # "container":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 127
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 128
    iget-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    invoke-static {v0, p0}, Landroid/support/v4/preference/PreferenceManagerCompat;->setOnPreferenceTreeClickListener(Landroid/preference/PreferenceManager;Landroid/support/v4/preference/PreferenceManagerCompat$OnPreferenceTreeClickListener;)V

    .line 129
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 133
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 134
    iget-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    invoke-static {v0}, Landroid/support/v4/preference/PreferenceManagerCompat;->dispatchActivityStop(Landroid/preference/PreferenceManager;)V

    .line 135
    iget-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/support/v4/preference/PreferenceManagerCompat;->setOnPreferenceTreeClickListener(Landroid/preference/PreferenceManager;Landroid/support/v4/preference/PreferenceManagerCompat$OnPreferenceTreeClickListener;)V

    .line 136
    return-void
.end method

.method public setPreferenceScreen(Landroid/preference/PreferenceScreen;)V
    .locals 1
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;

    .prologue
    .line 187
    iget-object v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    invoke-static {v0, p1}, Landroid/support/v4/preference/PreferenceManagerCompat;->setPreferences(Landroid/preference/PreferenceManager;Landroid/preference/PreferenceScreen;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 189
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mHavePrefs:Z

    .line 190
    iget-boolean v0, p0, Landroid/support/v4/preference/PreferenceFragment;->mInitDone:Z

    if-eqz v0, :cond_0

    .line 191
    invoke-direct {p0}, Landroid/support/v4/preference/PreferenceFragment;->postBindPreferences()V

    .line 194
    :cond_0
    return-void
.end method
