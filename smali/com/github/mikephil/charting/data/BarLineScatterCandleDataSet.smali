.class public abstract Lcom/github/mikephil/charting/data/BarLineScatterCandleDataSet;
.super Lcom/github/mikephil/charting/data/DataSet;
.source "BarLineScatterCandleDataSet.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/github/mikephil/charting/data/Entry;",
        ">",
        "Lcom/github/mikephil/charting/data/DataSet",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected mHighLightColor:I


# virtual methods
.method public getHighLightColor()I
    .locals 1

    .prologue
    .line 39
    .local p0, "this":Lcom/github/mikephil/charting/data/BarLineScatterCandleDataSet;, "Lcom/github/mikephil/charting/data/BarLineScatterCandleDataSet<TT;>;"
    iget v0, p0, Lcom/github/mikephil/charting/data/BarLineScatterCandleDataSet;->mHighLightColor:I

    return v0
.end method
