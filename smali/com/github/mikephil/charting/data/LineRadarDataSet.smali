.class public abstract Lcom/github/mikephil/charting/data/LineRadarDataSet;
.super Lcom/github/mikephil/charting/data/BarLineScatterCandleDataSet;
.source "LineRadarDataSet.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/github/mikephil/charting/data/Entry;",
        ">",
        "Lcom/github/mikephil/charting/data/BarLineScatterCandleDataSet",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private mDrawFilled:Z

.field private mFillAlpha:I

.field private mLineWidth:F


# virtual methods
.method public getFillAlpha()I
    .locals 1

    .prologue
    .line 60
    .local p0, "this":Lcom/github/mikephil/charting/data/LineRadarDataSet;, "Lcom/github/mikephil/charting/data/LineRadarDataSet<TT;>;"
    iget v0, p0, Lcom/github/mikephil/charting/data/LineRadarDataSet;->mFillAlpha:I

    return v0
.end method

.method public getLineWidth()F
    .locals 1

    .prologue
    .line 94
    .local p0, "this":Lcom/github/mikephil/charting/data/LineRadarDataSet;, "Lcom/github/mikephil/charting/data/LineRadarDataSet<TT;>;"
    iget v0, p0, Lcom/github/mikephil/charting/data/LineRadarDataSet;->mLineWidth:F

    return v0
.end method

.method public isDrawFilledEnabled()Z
    .locals 1

    .prologue
    .line 114
    .local p0, "this":Lcom/github/mikephil/charting/data/LineRadarDataSet;, "Lcom/github/mikephil/charting/data/LineRadarDataSet<TT;>;"
    iget-boolean v0, p0, Lcom/github/mikephil/charting/data/LineRadarDataSet;->mDrawFilled:Z

    return v0
.end method
