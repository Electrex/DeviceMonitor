.class public abstract Lcom/github/mikephil/charting/renderer/Renderer;
.super Ljava/lang/Object;
.source "Renderer.java"


# instance fields
.field protected mMaxX:I

.field protected mMinX:I

.field protected mViewPortHandler:Lcom/github/mikephil/charting/renderer/ViewPortHandler;


# direct methods
.method public constructor <init>(Lcom/github/mikephil/charting/renderer/ViewPortHandler;)V
    .locals 1
    .param p1, "viewPortHandler"    # Lcom/github/mikephil/charting/renderer/ViewPortHandler;

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput v0, p0, Lcom/github/mikephil/charting/renderer/Renderer;->mMinX:I

    .line 20
    iput v0, p0, Lcom/github/mikephil/charting/renderer/Renderer;->mMaxX:I

    .line 23
    iput-object p1, p0, Lcom/github/mikephil/charting/renderer/Renderer;->mViewPortHandler:Lcom/github/mikephil/charting/renderer/ViewPortHandler;

    .line 24
    return-void
.end method
