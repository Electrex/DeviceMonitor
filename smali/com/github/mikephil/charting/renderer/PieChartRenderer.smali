.class public Lcom/github/mikephil/charting/renderer/PieChartRenderer;
.super Lcom/github/mikephil/charting/renderer/DataRenderer;
.source "PieChartRenderer.java"


# instance fields
.field protected mBitmapCanvas:Landroid/graphics/Canvas;

.field private mCenterTextPaint:Landroid/graphics/Paint;

.field protected mChart:Lcom/github/mikephil/charting/charts/PieChart;

.field protected mDrawBitmap:Landroid/graphics/Bitmap;

.field private mHolePaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Lcom/github/mikephil/charting/charts/PieChart;Lcom/github/mikephil/charting/animation/ChartAnimator;Lcom/github/mikephil/charting/renderer/ViewPortHandler;)V
    .locals 3
    .param p1, "chart"    # Lcom/github/mikephil/charting/charts/PieChart;
    .param p2, "animator"    # Lcom/github/mikephil/charting/animation/ChartAnimator;
    .param p3, "viewPortHandler"    # Lcom/github/mikephil/charting/renderer/ViewPortHandler;

    .prologue
    const/4 v1, 0x1

    const/4 v2, -0x1

    .line 45
    invoke-direct {p0, p2, p3}, Lcom/github/mikephil/charting/renderer/DataRenderer;-><init>(Lcom/github/mikephil/charting/animation/ChartAnimator;Lcom/github/mikephil/charting/renderer/ViewPortHandler;)V

    .line 46
    iput-object p1, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    .line 48
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mHolePaint:Landroid/graphics/Paint;

    .line 49
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mHolePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 51
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mCenterTextPaint:Landroid/graphics/Paint;

    .line 52
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mCenterTextPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 53
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mCenterTextPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-static {v1}, Lcom/github/mikephil/charting/utils/Utils;->convertDpToPixel(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 54
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mCenterTextPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 56
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mValuePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41500000    # 13.0f

    invoke-static {v1}, Lcom/github/mikephil/charting/utils/Utils;->convertDpToPixel(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 57
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 58
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mValuePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 59
    return-void
.end method


# virtual methods
.method protected drawCenterText(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1, "c"    # Landroid/graphics/Canvas;

    .prologue
    .line 268
    iget-object v10, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v10}, Lcom/github/mikephil/charting/charts/PieChart;->getCenterText()Ljava/lang/String;

    move-result-object v1

    .line 270
    .local v1, "centerText":Ljava/lang/String;
    iget-object v10, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v10}, Lcom/github/mikephil/charting/charts/PieChart;->isDrawCenterTextEnabled()Z

    move-result v10

    if-eqz v10, :cond_0

    if-eqz v1, :cond_0

    .line 272
    iget-object v10, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v10}, Lcom/github/mikephil/charting/charts/PieChart;->getCenterCircleBox()Landroid/graphics/PointF;

    move-result-object v0

    .line 275
    .local v0, "center":Landroid/graphics/PointF;
    const-string v10, "\n"

    invoke-virtual {v1, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 278
    .local v6, "lines":[Ljava/lang/String;
    iget-object v10, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mCenterTextPaint:Landroid/graphics/Paint;

    const/4 v11, 0x0

    aget-object v11, v6, v11

    invoke-static {v10, v11}, Lcom/github/mikephil/charting/utils/Utils;->calcTextHeight(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v10

    int-to-float v5, v10

    .line 279
    .local v5, "lineHeight":F
    const v10, 0x3e4ccccd    # 0.2f

    mul-float v7, v5, v10

    .line 281
    .local v7, "linespacing":F
    array-length v10, v6

    int-to-float v10, v10

    mul-float/2addr v10, v5

    array-length v11, v6

    add-int/lit8 v11, v11, -0x1

    int-to-float v11, v11

    mul-float/2addr v11, v7

    sub-float v8, v10, v11

    .line 283
    .local v8, "totalheight":F
    array-length v2, v6

    .line 285
    .local v2, "cnt":I
    iget v9, v0, Landroid/graphics/PointF;->y:F

    .line 287
    .local v9, "y":F
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v10, v6

    if-ge v3, v10, :cond_0

    .line 289
    array-length v10, v6

    sub-int/2addr v10, v3

    add-int/lit8 v10, v10, -0x1

    aget-object v4, v6, v10

    .line 291
    .local v4, "line":Ljava/lang/String;
    iget v10, v0, Landroid/graphics/PointF;->x:F

    int-to-float v11, v2

    mul-float/2addr v11, v5

    add-float/2addr v11, v9

    const/high16 v12, 0x40000000    # 2.0f

    div-float v12, v8, v12

    sub-float/2addr v11, v12

    iget-object v12, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mCenterTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v10, v11, v12}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 294
    add-int/lit8 v2, v2, -0x1

    .line 295
    sub-float/2addr v9, v7

    .line 287
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 298
    .end local v0    # "center":Landroid/graphics/PointF;
    .end local v2    # "cnt":I
    .end local v3    # "i":I
    .end local v4    # "line":Ljava/lang/String;
    .end local v5    # "lineHeight":F
    .end local v6    # "lines":[Ljava/lang/String;
    .end local v7    # "linespacing":F
    .end local v8    # "totalheight":F
    .end local v9    # "y":F
    :cond_0
    return-void
.end method

.method public drawData(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "c"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v6, 0x0

    .line 78
    iget-object v3, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mDrawBitmap:Landroid/graphics/Bitmap;

    if-nez v3, :cond_0

    .line 79
    iget-object v3, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/renderer/ViewPortHandler;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/renderer/ViewPortHandler;->getChartWidth()F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/renderer/ViewPortHandler;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/renderer/ViewPortHandler;->getChartHeight()F

    move-result v4

    float-to-int v4, v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mDrawBitmap:Landroid/graphics/Bitmap;

    .line 81
    new-instance v3, Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mDrawBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v3, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v3, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mBitmapCanvas:Landroid/graphics/Canvas;

    .line 84
    :cond_0
    iget-object v3, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mDrawBitmap:Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 86
    iget-object v3, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/charts/PieChart;->getData()Lcom/github/mikephil/charting/data/ChartData;

    move-result-object v1

    check-cast v1, Lcom/github/mikephil/charting/data/PieData;

    .line 88
    .local v1, "pieData":Lcom/github/mikephil/charting/data/PieData;
    invoke-virtual {v1}, Lcom/github/mikephil/charting/data/PieData;->getDataSets()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/github/mikephil/charting/data/PieDataSet;

    .line 90
    .local v2, "set":Lcom/github/mikephil/charting/data/PieDataSet;
    invoke-virtual {v2}, Lcom/github/mikephil/charting/data/PieDataSet;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 91
    invoke-virtual {p0, p1, v2}, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->drawDataSet(Landroid/graphics/Canvas;Lcom/github/mikephil/charting/data/PieDataSet;)V

    goto :goto_0

    .line 94
    .end local v2    # "set":Lcom/github/mikephil/charting/data/PieDataSet;
    :cond_2
    iget-object v3, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mDrawBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v6, v6, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 95
    return-void
.end method

.method protected drawDataSet(Landroid/graphics/Canvas;Lcom/github/mikephil/charting/data/PieDataSet;)V
    .locals 16
    .param p1, "c"    # Landroid/graphics/Canvas;
    .param p2, "dataSet"    # Lcom/github/mikephil/charting/data/PieDataSet;

    .prologue
    .line 99
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/charts/PieChart;->getRotationAngle()F

    move-result v8

    .line 101
    .local v8, "angle":F
    const/4 v9, 0x0

    .line 103
    .local v9, "cnt":I
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/data/PieDataSet;->getYVals()Ljava/util/ArrayList;

    move-result-object v12

    .line 104
    .local v12, "entries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/github/mikephil/charting/data/Entry;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/charts/PieChart;->getDrawAngles()[F

    move-result-object v10

    .line 106
    .local v10, "drawAngles":[F
    const/4 v13, 0x0

    .local v13, "j":I
    :goto_0
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v13, v2, :cond_1

    .line 108
    aget v14, v10, v9

    .line 109
    .local v14, "newangle":F
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/data/PieDataSet;->getSliceSpace()F

    move-result v15

    .line 111
    .local v15, "sliceSpace":F
    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/github/mikephil/charting/data/Entry;

    .line 114
    .local v11, "e":Lcom/github/mikephil/charting/data/Entry;
    invoke-virtual {v11}, Lcom/github/mikephil/charting/data/Entry;->getVal()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-double v2, v2

    const-wide v4, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    .line 116
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v11}, Lcom/github/mikephil/charting/data/Entry;->getXIndex()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/charts/PieChart;->getData()Lcom/github/mikephil/charting/data/ChartData;

    move-result-object v2

    check-cast v2, Lcom/github/mikephil/charting/data/PieData;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/github/mikephil/charting/data/PieData;->getIndexOfDataSet(Lcom/github/mikephil/charting/data/DataSet;)I

    move-result v2

    invoke-virtual {v3, v4, v2}, Lcom/github/mikephil/charting/charts/PieChart;->needsHighlight(II)Z

    move-result v2

    if-nez v2, :cond_0

    .line 119
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/github/mikephil/charting/data/PieDataSet;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 120
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mBitmapCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/charts/PieChart;->getCircleBox()Landroid/graphics/RectF;

    move-result-object v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v15, v4

    add-float/2addr v4, v8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mAnimator:Lcom/github/mikephil/charting/animation/ChartAnimator;

    invoke-virtual {v5}, Lcom/github/mikephil/charting/animation/ChartAnimator;->getPhaseY()F

    move-result v5

    mul-float/2addr v5, v14

    const/high16 v6, 0x40000000    # 2.0f

    div-float v6, v15, v6

    sub-float/2addr v5, v6

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 133
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mAnimator:Lcom/github/mikephil/charting/animation/ChartAnimator;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/animation/ChartAnimator;->getPhaseX()F

    move-result v2

    mul-float/2addr v2, v14

    add-float/2addr v8, v2

    .line 134
    add-int/lit8 v9, v9, 0x1

    .line 106
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_0

    .line 136
    .end local v11    # "e":Lcom/github/mikephil/charting/data/Entry;
    .end local v14    # "newangle":F
    .end local v15    # "sliceSpace":F
    :cond_1
    return-void
.end method

.method public drawExtras(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "c"    # Landroid/graphics/Canvas;

    .prologue
    .line 224
    invoke-virtual {p0, p1}, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->drawHole(Landroid/graphics/Canvas;)V

    .line 225
    invoke-virtual {p0, p1}, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->drawCenterText(Landroid/graphics/Canvas;)V

    .line 226
    return-void
.end method

.method public drawHighlighted(Landroid/graphics/Canvas;[Lcom/github/mikephil/charting/utils/Highlight;)V
    .locals 17
    .param p1, "c"    # Landroid/graphics/Canvas;
    .param p2, "indices"    # [Lcom/github/mikephil/charting/utils/Highlight;

    .prologue
    .line 303
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/charts/PieChart;->getRotationAngle()F

    move-result v12

    .line 304
    .local v12, "rotationAngle":F
    const/4 v8, 0x0

    .line 306
    .local v8, "angle":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/charts/PieChart;->getDrawAngles()[F

    move-result-object v10

    .line 307
    .local v10, "drawAngles":[F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/charts/PieChart;->getAbsoluteAngles()[F

    move-result-object v7

    .line 309
    .local v7, "absoluteAngles":[F
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    move-object/from16 v0, p2

    array-length v1, v0

    if-ge v11, v1, :cond_3

    .line 312
    aget-object v1, p2, v11

    invoke-virtual {v1}, Lcom/github/mikephil/charting/utils/Highlight;->getXIndex()I

    move-result v16

    .line 313
    .local v16, "xIndex":I
    array-length v1, v10

    move/from16 v0, v16

    if-lt v0, v1, :cond_1

    .line 309
    :cond_0
    :goto_1
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 316
    :cond_1
    if-nez v16, :cond_2

    .line 317
    move v8, v12

    .line 321
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mAnimator:Lcom/github/mikephil/charting/animation/ChartAnimator;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/animation/ChartAnimator;->getPhaseY()F

    move-result v1

    mul-float/2addr v8, v1

    .line 323
    aget v15, v10, v16

    .line 325
    .local v15, "sliceDegrees":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/charts/PieChart;->getData()Lcom/github/mikephil/charting/data/ChartData;

    move-result-object v1

    check-cast v1, Lcom/github/mikephil/charting/data/PieData;

    aget-object v3, p2, v11

    invoke-virtual {v3}, Lcom/github/mikephil/charting/utils/Highlight;->getDataSetIndex()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/github/mikephil/charting/data/PieData;->getDataSetByIndex(I)Lcom/github/mikephil/charting/data/PieDataSet;

    move-result-object v13

    .line 329
    .local v13, "set":Lcom/github/mikephil/charting/data/PieDataSet;
    if-eqz v13, :cond_0

    .line 332
    invoke-virtual {v13}, Lcom/github/mikephil/charting/data/PieDataSet;->getSelectionShift()F

    move-result v14

    .line 333
    .local v14, "shift":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/charts/PieChart;->getCircleBox()Landroid/graphics/RectF;

    move-result-object v9

    .line 341
    .local v9, "circleBox":Landroid/graphics/RectF;
    new-instance v2, Landroid/graphics/RectF;

    iget v1, v9, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v14

    iget v3, v9, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v14

    iget v4, v9, Landroid/graphics/RectF;->right:F

    add-float/2addr v4, v14

    iget v5, v9, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v5, v14

    invoke-direct {v2, v1, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 346
    .local v2, "highlighted":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/github/mikephil/charting/data/PieDataSet;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 350
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mBitmapCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v13}, Lcom/github/mikephil/charting/data/PieDataSet;->getSliceSpace()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v3, v8

    invoke-virtual {v13}, Lcom/github/mikephil/charting/data/PieDataSet;->getSliceSpace()F

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    sub-float v4, v15, v4

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    goto :goto_1

    .line 319
    .end local v2    # "highlighted":Landroid/graphics/RectF;
    .end local v9    # "circleBox":Landroid/graphics/RectF;
    .end local v13    # "set":Lcom/github/mikephil/charting/data/PieDataSet;
    .end local v14    # "shift":F
    .end local v15    # "sliceDegrees":F
    :cond_2
    add-int/lit8 v1, v16, -0x1

    aget v1, v7, v1

    add-float v8, v12, v1

    goto :goto_2

    .line 353
    .end local v16    # "xIndex":I
    :cond_3
    return-void
.end method

.method protected drawHole(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "c"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v10, 0x42c80000    # 100.0f

    .line 234
    iget-object v5, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v5}, Lcom/github/mikephil/charting/charts/PieChart;->isDrawHoleEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 236
    iget-object v5, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v5}, Lcom/github/mikephil/charting/charts/PieChart;->getTransparentCircleRadius()F

    move-result v4

    .line 237
    .local v4, "transparentCircleRadius":F
    iget-object v5, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v5}, Lcom/github/mikephil/charting/charts/PieChart;->getHoleRadius()F

    move-result v2

    .line 238
    .local v2, "holeRadius":F
    iget-object v5, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v5}, Lcom/github/mikephil/charting/charts/PieChart;->getRadius()F

    move-result v3

    .line 240
    .local v3, "radius":F
    iget-object v5, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v5}, Lcom/github/mikephil/charting/charts/PieChart;->getCenterCircleBox()Landroid/graphics/PointF;

    move-result-object v0

    .line 242
    .local v0, "center":Landroid/graphics/PointF;
    iget-object v5, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mHolePaint:Landroid/graphics/Paint;

    invoke-virtual {v5}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    .line 245
    .local v1, "color":I
    iget-object v5, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mBitmapCanvas:Landroid/graphics/Canvas;

    iget v6, v0, Landroid/graphics/PointF;->x:F

    iget v7, v0, Landroid/graphics/PointF;->y:F

    div-float v8, v3, v10

    mul-float/2addr v8, v2

    iget-object v9, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mHolePaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 248
    cmpl-float v5, v4, v2

    if-lez v5, :cond_0

    .line 251
    iget-object v5, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mHolePaint:Landroid/graphics/Paint;

    const v6, 0x60ffffff

    and-int/2addr v6, v1

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 254
    iget-object v5, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mBitmapCanvas:Landroid/graphics/Canvas;

    iget v6, v0, Landroid/graphics/PointF;->x:F

    iget v7, v0, Landroid/graphics/PointF;->y:F

    div-float v8, v3, v10

    mul-float/2addr v8, v4

    iget-object v9, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mHolePaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 257
    iget-object v5, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mHolePaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 260
    .end local v0    # "center":Landroid/graphics/PointF;
    .end local v1    # "color":I
    .end local v2    # "holeRadius":F
    .end local v3    # "radius":F
    .end local v4    # "transparentCircleRadius":F
    :cond_0
    return-void
.end method

.method public drawValues(Landroid/graphics/Canvas;)V
    .locals 32
    .param p1, "c"    # Landroid/graphics/Canvas;

    .prologue
    .line 141
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/github/mikephil/charting/charts/PieChart;->getCenterCircleBox()Landroid/graphics/PointF;

    move-result-object v7

    .line 144
    .local v7, "center":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/github/mikephil/charting/charts/PieChart;->getRadius()F

    move-result v21

    .line 145
    .local v21, "r":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/github/mikephil/charting/charts/PieChart;->getRotationAngle()F

    move-result v22

    .line 146
    .local v22, "rotationAngle":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/github/mikephil/charting/charts/PieChart;->getDrawAngles()[F

    move-result-object v12

    .line 147
    .local v12, "drawAngles":[F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/github/mikephil/charting/charts/PieChart;->getAbsoluteAngles()[F

    move-result-object v6

    .line 149
    .local v6, "absoluteAngles":[F
    const/high16 v27, 0x40400000    # 3.0f

    div-float v19, v21, v27

    .line 151
    .local v19, "off":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/github/mikephil/charting/charts/PieChart;->isDrawHoleEnabled()Z

    move-result v27

    if-eqz v27, :cond_0

    .line 152
    const/high16 v27, 0x42c80000    # 100.0f

    div-float v27, v21, v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/github/mikephil/charting/charts/PieChart;->getHoleRadius()F

    move-result v28

    mul-float v27, v27, v28

    sub-float v27, v21, v27

    const/high16 v28, 0x40000000    # 2.0f

    div-float v19, v27, v28

    .line 155
    :cond_0
    sub-float v21, v21, v19

    .line 157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/github/mikephil/charting/charts/PieChart;->getData()Lcom/github/mikephil/charting/data/ChartData;

    move-result-object v9

    check-cast v9, Lcom/github/mikephil/charting/data/PieData;

    .line 158
    .local v9, "data":Lcom/github/mikephil/charting/data/PieData;
    invoke-virtual {v9}, Lcom/github/mikephil/charting/data/PieData;->getDataSets()Ljava/util/ArrayList;

    move-result-object v11

    .line 160
    .local v11, "dataSets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/github/mikephil/charting/data/PieDataSet;>;"
    const/4 v8, 0x0

    .line 162
    .local v8, "cnt":I
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_0
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v27

    move/from16 v0, v16

    move/from16 v1, v27

    if-ge v0, v1, :cond_7

    .line 164
    move/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/github/mikephil/charting/data/PieDataSet;

    .line 166
    .local v10, "dataSet":Lcom/github/mikephil/charting/data/PieDataSet;
    invoke-virtual {v10}, Lcom/github/mikephil/charting/data/PieDataSet;->isDrawValuesEnabled()Z

    move-result v27

    if-nez v27, :cond_2

    .line 162
    :cond_1
    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    .line 170
    :cond_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->applyValueTextStyle(Lcom/github/mikephil/charting/data/DataSet;)V

    .line 172
    invoke-virtual {v10}, Lcom/github/mikephil/charting/data/PieDataSet;->getYVals()Ljava/util/ArrayList;

    move-result-object v15

    .line 174
    .local v15, "entries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/github/mikephil/charting/data/Entry;>;"
    const/16 v17, 0x0

    .local v17, "j":I
    :goto_1
    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v27, v0

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v28

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mAnimator:Lcom/github/mikephil/charting/animation/ChartAnimator;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/github/mikephil/charting/animation/ChartAnimator;->getPhaseX()F

    move-result v29

    mul-float v28, v28, v29

    cmpg-float v27, v27, v28

    if-gez v27, :cond_1

    .line 177
    aget v27, v12, v8

    const/high16 v28, 0x40000000    # 2.0f

    div-float v20, v27, v28

    .line 180
    .local v20, "offset":F
    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v28, v0

    aget v27, v6, v8

    add-float v27, v27, v22

    sub-float v27, v27, v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mAnimator:Lcom/github/mikephil/charting/animation/ChartAnimator;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/github/mikephil/charting/animation/ChartAnimator;->getPhaseY()F

    move-result v30

    mul-float v27, v27, v30

    move/from16 v0, v27

    float-to-double v0, v0

    move-wide/from16 v30, v0

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v30

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->cos(D)D

    move-result-wide v30

    mul-double v28, v28, v30

    iget v0, v7, Landroid/graphics/PointF;->x:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-double v0, v0

    move-wide/from16 v30, v0

    add-double v28, v28, v30

    move-wide/from16 v0, v28

    double-to-float v0, v0

    move/from16 v25, v0

    .line 183
    .local v25, "x":F
    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v28, v0

    aget v27, v6, v8

    add-float v27, v27, v22

    sub-float v27, v27, v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mAnimator:Lcom/github/mikephil/charting/animation/ChartAnimator;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/github/mikephil/charting/animation/ChartAnimator;->getPhaseY()F

    move-result v30

    mul-float v27, v27, v30

    move/from16 v0, v27

    float-to-double v0, v0

    move-wide/from16 v30, v0

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v30

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->sin(D)D

    move-result-wide v30

    mul-double v28, v28, v30

    iget v0, v7, Landroid/graphics/PointF;->y:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-double v0, v0

    move-wide/from16 v30, v0

    add-double v28, v28, v30

    move-wide/from16 v0, v28

    double-to-float v0, v0

    move/from16 v26, v0

    .line 187
    .local v26, "y":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/github/mikephil/charting/charts/PieChart;->isUsePercentValuesEnabled()Z

    move-result v27

    if-eqz v27, :cond_4

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/github/mikephil/charting/data/Entry;

    invoke-virtual/range {v27 .. v27}, Lcom/github/mikephil/charting/data/Entry;->getVal()F

    move-result v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/github/mikephil/charting/charts/PieChart;->getYValueSum()F

    move-result v28

    div-float v27, v27, v28

    const/high16 v28, 0x42c80000    # 100.0f

    mul-float v24, v27, v28

    .line 190
    .local v24, "value":F
    :goto_2
    invoke-virtual {v10}, Lcom/github/mikephil/charting/data/PieDataSet;->getValueFormatter()Lcom/github/mikephil/charting/utils/ValueFormatter;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v24

    invoke-interface {v0, v1}, Lcom/github/mikephil/charting/utils/ValueFormatter;->getFormattedValue(F)Ljava/lang/String;

    move-result-object v23

    .line 192
    .local v23, "val":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mChart:Lcom/github/mikephil/charting/charts/PieChart;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/github/mikephil/charting/charts/PieChart;->isDrawSliceTextEnabled()Z

    move-result v13

    .line 193
    .local v13, "drawXVals":Z
    invoke-virtual {v10}, Lcom/github/mikephil/charting/data/PieDataSet;->isDrawValuesEnabled()Z

    move-result v14

    .line 196
    .local v14, "drawYVals":Z
    if-eqz v13, :cond_5

    if-eqz v14, :cond_5

    .line 201
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mValuePaint:Landroid/graphics/Paint;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/Paint;->ascent()F

    move-result v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mValuePaint:Landroid/graphics/Paint;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/graphics/Paint;->descent()F

    move-result v28

    add-float v27, v27, v28

    const v28, 0x3fcccccd    # 1.6f

    mul-float v18, v27, v28

    .line 202
    .local v18, "lineHeight":F
    const/high16 v27, 0x40000000    # 2.0f

    div-float v27, v18, v27

    sub-float v26, v26, v27

    .line 204
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mValuePaint:Landroid/graphics/Paint;

    move-object/from16 v27, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move/from16 v2, v25

    move/from16 v3, v26

    move-object/from16 v4, v27

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 205
    invoke-virtual {v9}, Lcom/github/mikephil/charting/data/PieData;->getXValCount()I

    move-result v27

    move/from16 v0, v17

    move/from16 v1, v27

    if-ge v0, v1, :cond_3

    .line 206
    invoke-virtual {v9}, Lcom/github/mikephil/charting/data/PieData;->getXVals()Ljava/util/ArrayList;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/String;

    add-float v28, v26, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mValuePaint:Landroid/graphics/Paint;

    move-object/from16 v29, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    move/from16 v2, v25

    move/from16 v3, v28

    move-object/from16 v4, v29

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 217
    .end local v18    # "lineHeight":F
    :cond_3
    :goto_3
    add-int/lit8 v8, v8, 0x1

    .line 174
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_1

    .line 187
    .end local v13    # "drawXVals":Z
    .end local v14    # "drawYVals":Z
    .end local v23    # "val":Ljava/lang/String;
    .end local v24    # "value":F
    :cond_4
    move/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/github/mikephil/charting/data/Entry;

    invoke-virtual/range {v27 .. v27}, Lcom/github/mikephil/charting/data/Entry;->getVal()F

    move-result v24

    goto/16 :goto_2

    .line 209
    .restart local v13    # "drawXVals":Z
    .restart local v14    # "drawYVals":Z
    .restart local v23    # "val":Ljava/lang/String;
    .restart local v24    # "value":F
    :cond_5
    if-eqz v13, :cond_6

    if-nez v14, :cond_6

    .line 210
    invoke-virtual {v9}, Lcom/github/mikephil/charting/data/PieData;->getXValCount()I

    move-result v27

    move/from16 v0, v17

    move/from16 v1, v27

    if-ge v0, v1, :cond_3

    .line 211
    invoke-virtual {v9}, Lcom/github/mikephil/charting/data/PieData;->getXVals()Ljava/util/ArrayList;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mValuePaint:Landroid/graphics/Paint;

    move-object/from16 v28, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    move/from16 v2, v25

    move/from16 v3, v26

    move-object/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_3

    .line 212
    :cond_6
    if-nez v13, :cond_3

    if-eqz v14, :cond_3

    .line 214
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mValuePaint:Landroid/graphics/Paint;

    move-object/from16 v27, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move/from16 v2, v25

    move/from16 v3, v26

    move-object/from16 v4, v27

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_3

    .line 220
    .end local v10    # "dataSet":Lcom/github/mikephil/charting/data/PieDataSet;
    .end local v13    # "drawXVals":Z
    .end local v14    # "drawYVals":Z
    .end local v15    # "entries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/github/mikephil/charting/data/Entry;>;"
    .end local v17    # "j":I
    .end local v20    # "offset":F
    .end local v23    # "val":Ljava/lang/String;
    .end local v24    # "value":F
    .end local v25    # "x":F
    .end local v26    # "y":F
    :cond_7
    return-void
.end method

.method public getPaintCenterText()Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mCenterTextPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public getPaintHole()Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/PieChartRenderer;->mHolePaint:Landroid/graphics/Paint;

    return-object v0
.end method
