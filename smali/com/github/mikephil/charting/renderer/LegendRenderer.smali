.class public Lcom/github/mikephil/charting/renderer/LegendRenderer;
.super Lcom/github/mikephil/charting/renderer/Renderer;
.source "LegendRenderer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/github/mikephil/charting/renderer/LegendRenderer$1;
    }
.end annotation


# instance fields
.field protected mLegendFormPaint:Landroid/graphics/Paint;

.field protected mLegendLabelPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Lcom/github/mikephil/charting/renderer/ViewPortHandler;)V
    .locals 3
    .param p1, "viewPortHandler"    # Lcom/github/mikephil/charting/renderer/ViewPortHandler;

    .prologue
    const/4 v2, 0x1

    .line 28
    invoke-direct {p0, p1}, Lcom/github/mikephil/charting/renderer/Renderer;-><init>(Lcom/github/mikephil/charting/renderer/ViewPortHandler;)V

    .line 30
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    .line 31
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41100000    # 9.0f

    invoke-static {v1}, Lcom/github/mikephil/charting/utils/Utils;->convertDpToPixel(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 32
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 34
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendFormPaint:Landroid/graphics/Paint;

    .line 35
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendFormPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 36
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendFormPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 37
    return-void
.end method


# virtual methods
.method public computeLegend(Lcom/github/mikephil/charting/data/ChartData;Lcom/github/mikephil/charting/components/Legend;)Lcom/github/mikephil/charting/components/Legend;
    .locals 17
    .param p2, "legend"    # Lcom/github/mikephil/charting/components/Legend;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/github/mikephil/charting/data/ChartData",
            "<*>;",
            "Lcom/github/mikephil/charting/components/Legend;",
            ")",
            "Lcom/github/mikephil/charting/components/Legend;"
        }
    .end annotation

    .prologue
    .line 64
    .local p1, "data":Lcom/github/mikephil/charting/data/ChartData;, "Lcom/github/mikephil/charting/data/ChartData<*>;"
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 65
    .local v10, "labels":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 68
    .local v3, "colors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/github/mikephil/charting/data/ChartData;->getDataSetCount()I

    move-result v15

    if-ge v6, v15, :cond_6

    .line 70
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/github/mikephil/charting/data/ChartData;->getDataSetByIndex(I)Lcom/github/mikephil/charting/data/DataSet;

    move-result-object v4

    .line 72
    .local v4, "dataSet":Lcom/github/mikephil/charting/data/DataSet;, "Lcom/github/mikephil/charting/data/DataSet<+Lcom/github/mikephil/charting/data/Entry;>;"
    invoke-virtual {v4}, Lcom/github/mikephil/charting/data/DataSet;->getColors()Ljava/util/ArrayList;

    move-result-object v2

    .line 73
    .local v2, "clrs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v4}, Lcom/github/mikephil/charting/data/DataSet;->getEntryCount()I

    move-result v5

    .line 76
    .local v5, "entryCount":I
    instance-of v15, v4, Lcom/github/mikephil/charting/data/BarDataSet;

    if-eqz v15, :cond_2

    move-object v15, v4

    check-cast v15, Lcom/github/mikephil/charting/data/BarDataSet;

    invoke-virtual {v15}, Lcom/github/mikephil/charting/data/BarDataSet;->getStackSize()I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-le v15, v0, :cond_2

    move-object v1, v4

    .line 78
    check-cast v1, Lcom/github/mikephil/charting/data/BarDataSet;

    .line 79
    .local v1, "bds":Lcom/github/mikephil/charting/data/BarDataSet;
    invoke-virtual {v1}, Lcom/github/mikephil/charting/data/BarDataSet;->getStackLabels()[Ljava/lang/String;

    move-result-object v12

    .line 81
    .local v12, "sLabels":[Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-ge v7, v15, :cond_0

    invoke-virtual {v1}, Lcom/github/mikephil/charting/data/BarDataSet;->getStackSize()I

    move-result v15

    if-ge v7, v15, :cond_0

    .line 83
    array-length v15, v12

    rem-int v15, v7, v15

    aget-object v15, v12, v15

    invoke-virtual {v10, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    invoke-virtual {v3, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 88
    :cond_0
    const/4 v15, -0x2

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v3, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    invoke-virtual {v1}, Lcom/github/mikephil/charting/data/BarDataSet;->getLabel()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 68
    .end local v1    # "bds":Lcom/github/mikephil/charting/data/BarDataSet;
    .end local v12    # "sLabels":[Ljava/lang/String;
    :cond_1
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 91
    .end local v7    # "j":I
    :cond_2
    instance-of v15, v4, Lcom/github/mikephil/charting/data/PieDataSet;

    if-eqz v15, :cond_4

    .line 93
    invoke-virtual/range {p1 .. p1}, Lcom/github/mikephil/charting/data/ChartData;->getXVals()Ljava/util/ArrayList;

    move-result-object v14

    .local v14, "xVals":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object v11, v4

    .line 94
    check-cast v11, Lcom/github/mikephil/charting/data/PieDataSet;

    .line 96
    .local v11, "pds":Lcom/github/mikephil/charting/data/PieDataSet;
    const/4 v7, 0x0

    .restart local v7    # "j":I
    :goto_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-ge v7, v15, :cond_3

    if-ge v7, v5, :cond_3

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-ge v7, v15, :cond_3

    .line 98
    invoke-virtual {v14, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    invoke-virtual {v3, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 103
    :cond_3
    const/4 v15, -0x2

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v3, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    invoke-virtual {v11}, Lcom/github/mikephil/charting/data/PieDataSet;->getLabel()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 108
    .end local v7    # "j":I
    .end local v11    # "pds":Lcom/github/mikephil/charting/data/PieDataSet;
    .end local v14    # "xVals":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_4
    const/4 v7, 0x0

    .restart local v7    # "j":I
    :goto_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-ge v7, v15, :cond_1

    if-ge v7, v5, :cond_1

    .line 111
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    if-ge v7, v15, :cond_5

    add-int/lit8 v15, v5, -0x1

    if-ge v7, v15, :cond_5

    .line 113
    const/4 v15, 0x0

    invoke-virtual {v10, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    :goto_5
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    invoke-virtual {v3, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 116
    :cond_5
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/github/mikephil/charting/data/ChartData;->getDataSetByIndex(I)Lcom/github/mikephil/charting/data/DataSet;

    move-result-object v15

    invoke-virtual {v15}, Lcom/github/mikephil/charting/data/DataSet;->getLabel()Ljava/lang/String;

    move-result-object v9

    .line 117
    .local v9, "label":Ljava/lang/String;
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 125
    .end local v2    # "clrs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v4    # "dataSet":Lcom/github/mikephil/charting/data/DataSet;, "Lcom/github/mikephil/charting/data/DataSet<+Lcom/github/mikephil/charting/data/Entry;>;"
    .end local v5    # "entryCount":I
    .end local v7    # "j":I
    .end local v9    # "label":Ljava/lang/String;
    :cond_6
    new-instance v8, Lcom/github/mikephil/charting/components/Legend;

    invoke-direct {v8, v3, v10}, Lcom/github/mikephil/charting/components/Legend;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 127
    .local v8, "l":Lcom/github/mikephil/charting/components/Legend;
    if-eqz p2, :cond_7

    .line 129
    move-object/from16 v0, p2

    invoke-virtual {v8, v0}, Lcom/github/mikephil/charting/components/Legend;->apply(Lcom/github/mikephil/charting/components/Legend;)V

    .line 132
    :cond_7
    invoke-virtual {v8}, Lcom/github/mikephil/charting/components/Legend;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v13

    .line 134
    .local v13, "tf":Landroid/graphics/Typeface;
    if-eqz v13, :cond_8

    .line 135
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    invoke-virtual {v15, v13}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 137
    :cond_8
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/components/Legend;->getTextSize()F

    move-result v16

    invoke-virtual/range {v15 .. v16}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 138
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/components/Legend;->getTextColor()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Landroid/graphics/Paint;->setColor(I)V

    .line 141
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    invoke-virtual {v8, v15}, Lcom/github/mikephil/charting/components/Legend;->calculateDimensions(Landroid/graphics/Paint;)V

    .line 143
    return-object v8
.end method

.method protected drawForm(Landroid/graphics/Canvas;FFILcom/github/mikephil/charting/components/Legend;)V
    .locals 8
    .param p1, "c"    # Landroid/graphics/Canvas;
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "index"    # I
    .param p5, "legend"    # Lcom/github/mikephil/charting/components/Legend;

    .prologue
    .line 411
    invoke-virtual {p5}, Lcom/github/mikephil/charting/components/Legend;->getColors()[I

    move-result-object v0

    aget v0, v0, p4

    const/4 v1, -0x2

    if-ne v0, v1, :cond_0

    .line 430
    :goto_0
    return-void

    .line 414
    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendFormPaint:Landroid/graphics/Paint;

    invoke-virtual {p5}, Lcom/github/mikephil/charting/components/Legend;->getColors()[I

    move-result-object v1

    aget v1, v1, p4

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 416
    invoke-virtual {p5}, Lcom/github/mikephil/charting/components/Legend;->getFormSize()F

    move-result v6

    .line 417
    .local v6, "formsize":F
    const/high16 v0, 0x40000000    # 2.0f

    div-float v7, v6, v0

    .line 419
    .local v7, "half":F
    sget-object v0, Lcom/github/mikephil/charting/renderer/LegendRenderer$1;->$SwitchMap$com$github$mikephil$charting$components$Legend$LegendForm:[I

    invoke-virtual {p5}, Lcom/github/mikephil/charting/components/Legend;->getForm()Lcom/github/mikephil/charting/components/Legend$LegendForm;

    move-result-object v1

    invoke-virtual {v1}, Lcom/github/mikephil/charting/components/Legend$LegendForm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 421
    :pswitch_0
    add-float v0, p2, v7

    iget-object v1, p0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendFormPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, p3, v7, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 424
    :pswitch_1
    sub-float v2, p3, v7

    add-float v3, p2, v6

    add-float v4, p3, v7

    iget-object v5, p0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendFormPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 427
    :pswitch_2
    add-float v3, p2, v6

    iget-object v5, p0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendFormPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 419
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected drawLabel(Landroid/graphics/Canvas;FFLjava/lang/String;)V
    .locals 1
    .param p1, "c"    # Landroid/graphics/Canvas;
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "label"    # Ljava/lang/String;

    .prologue
    .line 441
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p4, p2, p3, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 442
    return-void
.end method

.method public getLabelPaint()Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public renderLegend(Landroid/graphics/Canvas;Lcom/github/mikephil/charting/components/Legend;)V
    .locals 25
    .param p1, "c"    # Landroid/graphics/Canvas;
    .param p2, "legend"    # Lcom/github/mikephil/charting/components/Legend;

    .prologue
    .line 148
    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 398
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v20

    .line 153
    .local v20, "tf":Landroid/graphics/Typeface;
    if-eqz v20, :cond_2

    .line 154
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 156
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getTextSize()F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 157
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getTextColor()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 159
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getLegendLabels()[Ljava/lang/String;

    move-result-object v16

    .line 161
    .local v16, "labels":[Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getFormSize()F

    move-result v14

    .line 164
    .local v14, "formSize":F
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getFormToTextSpace()F

    move-result v3

    add-float v15, v3, v14

    .line 167
    .local v15, "formTextSpaceAndForm":F
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getStackSpace()F

    move-result v18

    .line 171
    .local v18, "stackSpace":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    const-string v4, "AQJ"

    invoke-static {v3, v4}, Lcom/github/mikephil/charting/utils/Utils;->calcTextHeight(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v3, v14

    const/high16 v4, 0x40000000    # 2.0f

    div-float v19, v3, v4

    .line 176
    .local v19, "textDrop":F
    const/16 v17, 0x0

    .line 178
    .local v17, "stack":F
    const/16 v21, 0x0

    .line 180
    .local v21, "wasStacked":Z
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getYOffset()F

    move-result v24

    .line 181
    .local v24, "yoffset":F
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getXOffset()F

    move-result v23

    .line 183
    .local v23, "xoffset":F
    sget-object v3, Lcom/github/mikephil/charting/renderer/LegendRenderer$1;->$SwitchMap$com$github$mikephil$charting$components$Legend$LegendPosition:[I

    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getPosition()Lcom/github/mikephil/charting/components/Legend$LegendPosition;

    move-result-object v4

    invoke-virtual {v4}, Lcom/github/mikephil/charting/components/Legend$LegendPosition;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 186
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/renderer/ViewPortHandler;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/renderer/ViewPortHandler;->contentLeft()F

    move-result v3

    add-float v5, v3, v23

    .line 187
    .local v5, "posX":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/renderer/ViewPortHandler;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/renderer/ViewPortHandler;->getChartHeight()F

    move-result v3

    sub-float v11, v3, v24

    .line 189
    .local v11, "posY":F
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    move-object/from16 v0, v16

    array-length v3, v0

    if-ge v7, v3, :cond_0

    .line 191
    move-object/from16 v0, p2

    iget v3, v0, Lcom/github/mikephil/charting/components/Legend;->mTextHeightMax:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    sub-float v6, v11, v3

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v8, p2

    invoke-virtual/range {v3 .. v8}, Lcom/github/mikephil/charting/renderer/LegendRenderer;->drawForm(Landroid/graphics/Canvas;FFILcom/github/mikephil/charting/components/Legend;)V

    .line 194
    aget-object v3, v16, v7

    if-eqz v3, :cond_4

    .line 197
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getColors()[I

    move-result-object v3

    aget v3, v3, v7

    const/4 v4, -0x2

    if-eq v3, v4, :cond_3

    .line 198
    add-float/2addr v5, v15

    .line 200
    :cond_3
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/github/mikephil/charting/components/Legend;->getLabel(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v5, v11, v3}, Lcom/github/mikephil/charting/renderer/LegendRenderer;->drawLabel(Landroid/graphics/Canvas;FFLjava/lang/String;)V

    .line 201
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    aget-object v4, v16, v7

    invoke-static {v3, v4}, Lcom/github/mikephil/charting/utils/Utils;->calcTextWidth(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getXEntrySpace()F

    move-result v4

    add-float/2addr v3, v4

    add-float/2addr v5, v3

    .line 189
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 204
    :cond_4
    add-float v3, v14, v18

    add-float/2addr v5, v3

    goto :goto_2

    .line 211
    .end local v5    # "posX":F
    .end local v7    # "i":I
    .end local v11    # "posY":F
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/renderer/ViewPortHandler;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/renderer/ViewPortHandler;->contentRight()F

    move-result v3

    sub-float v5, v3, v23

    .line 212
    .restart local v5    # "posX":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/renderer/ViewPortHandler;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/renderer/ViewPortHandler;->getChartHeight()F

    move-result v3

    sub-float v11, v3, v24

    .line 214
    .restart local v11    # "posY":F
    move-object/from16 v0, v16

    array-length v3, v0

    add-int/lit8 v7, v3, -0x1

    .restart local v7    # "i":I
    :goto_3
    if-ltz v7, :cond_0

    .line 216
    aget-object v3, v16, v7

    if-eqz v3, :cond_6

    .line 218
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    aget-object v4, v16, v7

    invoke-static {v3, v4}, Lcom/github/mikephil/charting/utils/Utils;->calcTextWidth(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getXEntrySpace()F

    move-result v4

    add-float/2addr v3, v4

    sub-float/2addr v5, v3

    .line 220
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/github/mikephil/charting/components/Legend;->getLabel(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v5, v11, v3}, Lcom/github/mikephil/charting/renderer/LegendRenderer;->drawLabel(Landroid/graphics/Canvas;FFLjava/lang/String;)V

    .line 221
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getColors()[I

    move-result-object v3

    aget v3, v3, v7

    const/4 v4, -0x2

    if-eq v3, v4, :cond_5

    .line 222
    sub-float/2addr v5, v15

    .line 227
    :cond_5
    :goto_4
    move-object/from16 v0, p2

    iget v3, v0, Lcom/github/mikephil/charting/components/Legend;->mTextHeightMax:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    sub-float v6, v11, v3

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v8, p2

    invoke-virtual/range {v3 .. v8}, Lcom/github/mikephil/charting/renderer/LegendRenderer;->drawForm(Landroid/graphics/Canvas;FFILcom/github/mikephil/charting/components/Legend;)V

    .line 214
    add-int/lit8 v7, v7, -0x1

    goto :goto_3

    .line 224
    :cond_6
    add-float v3, v18, v14

    sub-float/2addr v5, v3

    goto :goto_4

    .line 233
    .end local v5    # "posX":F
    .end local v7    # "i":I
    .end local v11    # "posY":F
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/renderer/ViewPortHandler;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/renderer/ViewPortHandler;->getChartWidth()F

    move-result v3

    move-object/from16 v0, p2

    iget v4, v0, Lcom/github/mikephil/charting/components/Legend;->mTextWidthMax:F

    sub-float/2addr v3, v4

    sub-float v5, v3, v23

    .line 234
    .restart local v5    # "posX":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/renderer/ViewPortHandler;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/renderer/ViewPortHandler;->contentTop()F

    move-result v3

    add-float v11, v3, v24

    .line 236
    .restart local v11    # "posY":F
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_5
    move-object/from16 v0, v16

    array-length v3, v0

    if-ge v7, v3, :cond_0

    .line 238
    add-float v10, v5, v17

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move v12, v7

    move-object/from16 v13, p2

    invoke-virtual/range {v8 .. v13}, Lcom/github/mikephil/charting/renderer/LegendRenderer;->drawForm(Landroid/graphics/Canvas;FFILcom/github/mikephil/charting/components/Legend;)V

    .line 240
    aget-object v3, v16, v7

    if-eqz v3, :cond_9

    .line 242
    if-nez v21, :cond_8

    .line 244
    move/from16 v22, v5

    .line 246
    .local v22, "x":F
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getColors()[I

    move-result-object v3

    aget v3, v3, v7

    const/4 v4, -0x2

    if-eq v3, v4, :cond_7

    .line 247
    add-float v22, v22, v15

    .line 249
    :cond_7
    move-object/from16 v0, p2

    iget v3, v0, Lcom/github/mikephil/charting/components/Legend;->mTextHeightMax:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v3, v11

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/github/mikephil/charting/components/Legend;->getLabel(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/github/mikephil/charting/renderer/LegendRenderer;->drawLabel(Landroid/graphics/Canvas;FFLjava/lang/String;)V

    .line 251
    add-float v11, v11, v19

    .line 258
    .end local v22    # "x":F
    :goto_6
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getYEntrySpace()F

    move-result v3

    add-float/2addr v11, v3

    .line 259
    const/16 v17, 0x0

    .line 236
    :goto_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 253
    :cond_8
    move-object/from16 v0, p2

    iget v3, v0, Lcom/github/mikephil/charting/components/Legend;->mTextHeightMax:F

    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v3, v4

    add-float/2addr v11, v3

    .line 254
    move-object/from16 v0, p2

    iget v3, v0, Lcom/github/mikephil/charting/components/Legend;->mTextHeightMax:F

    sub-float v3, v11, v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/github/mikephil/charting/components/Legend;->getLabel(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v5, v3, v4}, Lcom/github/mikephil/charting/renderer/LegendRenderer;->drawLabel(Landroid/graphics/Canvas;FFLjava/lang/String;)V

    goto :goto_6

    .line 261
    :cond_9
    add-float v3, v14, v18

    add-float v17, v17, v3

    .line 262
    const/16 v21, 0x1

    goto :goto_7

    .line 267
    .end local v5    # "posX":F
    .end local v7    # "i":I
    .end local v11    # "posY":F
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/renderer/ViewPortHandler;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/renderer/ViewPortHandler;->getChartWidth()F

    move-result v3

    move-object/from16 v0, p2

    iget v4, v0, Lcom/github/mikephil/charting/components/Legend;->mTextWidthMax:F

    sub-float/2addr v3, v4

    sub-float v5, v3, v23

    .line 268
    .restart local v5    # "posX":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/renderer/ViewPortHandler;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/renderer/ViewPortHandler;->getChartHeight()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p2

    iget v4, v0, Lcom/github/mikephil/charting/components/Legend;->mNeededHeight:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v4, v6

    sub-float v11, v3, v4

    .line 270
    .restart local v11    # "posY":F
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_8
    move-object/from16 v0, v16

    array-length v3, v0

    if-ge v7, v3, :cond_0

    .line 272
    add-float v10, v5, v17

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move v12, v7

    move-object/from16 v13, p2

    invoke-virtual/range {v8 .. v13}, Lcom/github/mikephil/charting/renderer/LegendRenderer;->drawForm(Landroid/graphics/Canvas;FFILcom/github/mikephil/charting/components/Legend;)V

    .line 274
    aget-object v3, v16, v7

    if-eqz v3, :cond_c

    .line 276
    if-nez v21, :cond_b

    .line 278
    move/from16 v22, v5

    .line 280
    .restart local v22    # "x":F
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getColors()[I

    move-result-object v3

    aget v3, v3, v7

    const/4 v4, -0x2

    if-eq v3, v4, :cond_a

    .line 281
    add-float v22, v22, v15

    .line 283
    :cond_a
    move-object/from16 v0, p2

    iget v3, v0, Lcom/github/mikephil/charting/components/Legend;->mTextHeightMax:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v3, v11

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/github/mikephil/charting/components/Legend;->getLabel(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/github/mikephil/charting/renderer/LegendRenderer;->drawLabel(Landroid/graphics/Canvas;FFLjava/lang/String;)V

    .line 285
    add-float v11, v11, v19

    .line 292
    .end local v22    # "x":F
    :goto_9
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getYEntrySpace()F

    move-result v3

    add-float/2addr v11, v3

    .line 293
    const/16 v17, 0x0

    .line 270
    :goto_a
    add-int/lit8 v7, v7, 0x1

    goto :goto_8

    .line 287
    :cond_b
    move-object/from16 v0, p2

    iget v3, v0, Lcom/github/mikephil/charting/components/Legend;->mTextHeightMax:F

    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v3, v4

    add-float/2addr v11, v3

    .line 288
    move-object/from16 v0, p2

    iget v3, v0, Lcom/github/mikephil/charting/components/Legend;->mTextHeightMax:F

    sub-float v3, v11, v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/github/mikephil/charting/components/Legend;->getLabel(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v5, v3, v4}, Lcom/github/mikephil/charting/renderer/LegendRenderer;->drawLabel(Landroid/graphics/Canvas;FFLjava/lang/String;)V

    goto :goto_9

    .line 295
    :cond_c
    add-float v3, v14, v18

    add-float v17, v17, v3

    .line 296
    const/16 v21, 0x1

    goto :goto_a

    .line 303
    .end local v5    # "posX":F
    .end local v7    # "i":I
    .end local v11    # "posY":F
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/renderer/ViewPortHandler;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/renderer/ViewPortHandler;->getChartWidth()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p2

    iget v4, v0, Lcom/github/mikephil/charting/components/Legend;->mNeededWidth:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v4, v6

    sub-float v5, v3, v4

    .line 304
    .restart local v5    # "posX":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/renderer/ViewPortHandler;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/renderer/ViewPortHandler;->getChartHeight()F

    move-result v3

    sub-float v11, v3, v24

    .line 306
    .restart local v11    # "posY":F
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_b
    move-object/from16 v0, v16

    array-length v3, v0

    if-ge v7, v3, :cond_0

    .line 308
    move-object/from16 v0, p2

    iget v3, v0, Lcom/github/mikephil/charting/components/Legend;->mTextHeightMax:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    sub-float v6, v11, v3

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v8, p2

    invoke-virtual/range {v3 .. v8}, Lcom/github/mikephil/charting/renderer/LegendRenderer;->drawForm(Landroid/graphics/Canvas;FFILcom/github/mikephil/charting/components/Legend;)V

    .line 311
    aget-object v3, v16, v7

    if-eqz v3, :cond_e

    .line 314
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getColors()[I

    move-result-object v3

    aget v3, v3, v7

    const/4 v4, -0x2

    if-eq v3, v4, :cond_d

    .line 315
    add-float/2addr v5, v15

    .line 317
    :cond_d
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/github/mikephil/charting/components/Legend;->getLabel(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v5, v11, v3}, Lcom/github/mikephil/charting/renderer/LegendRenderer;->drawLabel(Landroid/graphics/Canvas;FFLjava/lang/String;)V

    .line 318
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    aget-object v4, v16, v7

    invoke-static {v3, v4}, Lcom/github/mikephil/charting/utils/Utils;->calcTextWidth(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getXEntrySpace()F

    move-result v4

    add-float/2addr v3, v4

    add-float/2addr v5, v3

    .line 306
    :goto_c
    add-int/lit8 v7, v7, 0x1

    goto :goto_b

    .line 321
    :cond_e
    add-float v3, v14, v18

    add-float/2addr v5, v3

    goto :goto_c

    .line 328
    .end local v5    # "posX":F
    .end local v7    # "i":I
    .end local v11    # "posY":F
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/renderer/ViewPortHandler;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/renderer/ViewPortHandler;->getChartWidth()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p2

    iget v4, v0, Lcom/github/mikephil/charting/components/Legend;->mTextWidthMax:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v4, v6

    sub-float v5, v3, v4

    .line 329
    .restart local v5    # "posX":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/renderer/ViewPortHandler;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/renderer/ViewPortHandler;->getChartHeight()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p2

    iget v4, v0, Lcom/github/mikephil/charting/components/Legend;->mNeededHeight:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v4, v6

    sub-float v11, v3, v4

    .line 331
    .restart local v11    # "posY":F
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_d
    move-object/from16 v0, v16

    array-length v3, v0

    if-ge v7, v3, :cond_0

    .line 333
    add-float v10, v5, v17

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move v12, v7

    move-object/from16 v13, p2

    invoke-virtual/range {v8 .. v13}, Lcom/github/mikephil/charting/renderer/LegendRenderer;->drawForm(Landroid/graphics/Canvas;FFILcom/github/mikephil/charting/components/Legend;)V

    .line 335
    aget-object v3, v16, v7

    if-eqz v3, :cond_11

    .line 337
    if-nez v21, :cond_10

    .line 339
    move/from16 v22, v5

    .line 341
    .restart local v22    # "x":F
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getColors()[I

    move-result-object v3

    aget v3, v3, v7

    const/4 v4, -0x2

    if-eq v3, v4, :cond_f

    .line 342
    add-float v22, v22, v15

    .line 344
    :cond_f
    move-object/from16 v0, p2

    iget v3, v0, Lcom/github/mikephil/charting/components/Legend;->mTextHeightMax:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v3, v11

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/github/mikephil/charting/components/Legend;->getLabel(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/github/mikephil/charting/renderer/LegendRenderer;->drawLabel(Landroid/graphics/Canvas;FFLjava/lang/String;)V

    .line 346
    add-float v11, v11, v19

    .line 353
    .end local v22    # "x":F
    :goto_e
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getYEntrySpace()F

    move-result v3

    add-float/2addr v11, v3

    .line 354
    const/16 v17, 0x0

    .line 331
    :goto_f
    add-int/lit8 v7, v7, 0x1

    goto :goto_d

    .line 348
    :cond_10
    move-object/from16 v0, p2

    iget v3, v0, Lcom/github/mikephil/charting/components/Legend;->mTextHeightMax:F

    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v3, v4

    add-float/2addr v11, v3

    .line 349
    move-object/from16 v0, p2

    iget v3, v0, Lcom/github/mikephil/charting/components/Legend;->mTextHeightMax:F

    sub-float v3, v11, v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/github/mikephil/charting/components/Legend;->getLabel(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v5, v3, v4}, Lcom/github/mikephil/charting/renderer/LegendRenderer;->drawLabel(Landroid/graphics/Canvas;FFLjava/lang/String;)V

    goto :goto_e

    .line 356
    :cond_11
    add-float v3, v14, v18

    add-float v17, v17, v3

    .line 357
    const/16 v21, 0x1

    goto :goto_f

    .line 364
    .end local v5    # "posX":F
    .end local v7    # "i":I
    .end local v11    # "posY":F
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/renderer/ViewPortHandler;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/renderer/ViewPortHandler;->getChartWidth()F

    move-result v3

    move-object/from16 v0, p2

    iget v4, v0, Lcom/github/mikephil/charting/components/Legend;->mTextWidthMax:F

    sub-float/2addr v3, v4

    sub-float v5, v3, v23

    .line 365
    .restart local v5    # "posX":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/LegendRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/renderer/ViewPortHandler;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/renderer/ViewPortHandler;->contentTop()F

    move-result v3

    add-float v11, v3, v24

    .line 367
    .restart local v11    # "posY":F
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_10
    move-object/from16 v0, v16

    array-length v3, v0

    if-ge v7, v3, :cond_0

    .line 369
    add-float v10, v5, v17

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move v12, v7

    move-object/from16 v13, p2

    invoke-virtual/range {v8 .. v13}, Lcom/github/mikephil/charting/renderer/LegendRenderer;->drawForm(Landroid/graphics/Canvas;FFILcom/github/mikephil/charting/components/Legend;)V

    .line 371
    aget-object v3, v16, v7

    if-eqz v3, :cond_14

    .line 373
    if-nez v21, :cond_13

    .line 375
    move/from16 v22, v5

    .line 377
    .restart local v22    # "x":F
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getColors()[I

    move-result-object v3

    aget v3, v3, v7

    const/4 v4, -0x2

    if-eq v3, v4, :cond_12

    .line 378
    add-float v22, v22, v15

    .line 380
    :cond_12
    move-object/from16 v0, p2

    iget v3, v0, Lcom/github/mikephil/charting/components/Legend;->mTextHeightMax:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v3, v11

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/github/mikephil/charting/components/Legend;->getLabel(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/github/mikephil/charting/renderer/LegendRenderer;->drawLabel(Landroid/graphics/Canvas;FFLjava/lang/String;)V

    .line 382
    add-float v11, v11, v19

    .line 389
    .end local v22    # "x":F
    :goto_11
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/components/Legend;->getYEntrySpace()F

    move-result v3

    add-float/2addr v11, v3

    .line 390
    const/16 v17, 0x0

    .line 367
    :goto_12
    add-int/lit8 v7, v7, 0x1

    goto :goto_10

    .line 384
    :cond_13
    move-object/from16 v0, p2

    iget v3, v0, Lcom/github/mikephil/charting/components/Legend;->mTextHeightMax:F

    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v3, v4

    add-float/2addr v11, v3

    .line 385
    move-object/from16 v0, p2

    iget v3, v0, Lcom/github/mikephil/charting/components/Legend;->mTextHeightMax:F

    sub-float v3, v11, v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/github/mikephil/charting/components/Legend;->getLabel(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v5, v3, v4}, Lcom/github/mikephil/charting/renderer/LegendRenderer;->drawLabel(Landroid/graphics/Canvas;FFLjava/lang/String;)V

    goto :goto_11

    .line 392
    :cond_14
    add-float v3, v14, v18

    add-float v17, v17, v3

    .line 393
    const/16 v21, 0x1

    goto :goto_12

    .line 183
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
