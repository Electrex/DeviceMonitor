.class public Lcom/github/mikephil/charting/components/YAxis;
.super Lcom/github/mikephil/charting/components/AxisBase;
.source "YAxis.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/github/mikephil/charting/components/YAxis$AxisDependency;,
        Lcom/github/mikephil/charting/components/YAxis$YAxisLabelPosition;
    }
.end annotation


# instance fields
.field private mAxisDependency:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

.field public mAxisMaximum:F

.field public mAxisMinimum:F

.field public mAxisRange:F

.field protected mCustomAxisMax:F

.field protected mCustomAxisMin:F

.field public mDecimals:I

.field private mDrawTopYLabelEntry:Z

.field public mEntries:[F

.field public mEntryCount:I

.field protected mInverted:Z

.field private mLabelCount:I

.field private mLimitLines:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/github/mikephil/charting/components/LimitLine;",
            ">;"
        }
    .end annotation
.end field

.field private mPosition:Lcom/github/mikephil/charting/components/YAxis$YAxisLabelPosition;

.field protected mShowOnlyMinMax:Z

.field protected mSpacePercentBottom:F

.field protected mSpacePercentTop:F

.field protected mStartAtZero:Z

.field protected mValueFormatter:Lcom/github/mikephil/charting/utils/ValueFormatter;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x7fc00000    # NaNf

    const/high16 v3, 0x41200000    # 10.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 100
    invoke-direct {p0}, Lcom/github/mikephil/charting/components/AxisBase;-><init>()V

    .line 28
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mEntries:[F

    .line 37
    const/4 v0, 0x6

    iput v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mLabelCount:I

    .line 40
    iput-boolean v5, p0, Lcom/github/mikephil/charting/components/YAxis;->mDrawTopYLabelEntry:Z

    .line 43
    iput-boolean v2, p0, Lcom/github/mikephil/charting/components/YAxis;->mShowOnlyMinMax:Z

    .line 46
    iput-boolean v2, p0, Lcom/github/mikephil/charting/components/YAxis;->mInverted:Z

    .line 49
    iput-boolean v5, p0, Lcom/github/mikephil/charting/components/YAxis;->mStartAtZero:Z

    .line 55
    iput v4, p0, Lcom/github/mikephil/charting/components/YAxis;->mCustomAxisMin:F

    .line 58
    iput v4, p0, Lcom/github/mikephil/charting/components/YAxis;->mCustomAxisMax:F

    .line 64
    iput v3, p0, Lcom/github/mikephil/charting/components/YAxis;->mSpacePercentTop:F

    .line 70
    iput v3, p0, Lcom/github/mikephil/charting/components/YAxis;->mSpacePercentBottom:F

    .line 72
    iput v1, p0, Lcom/github/mikephil/charting/components/YAxis;->mAxisMaximum:F

    .line 73
    iput v1, p0, Lcom/github/mikephil/charting/components/YAxis;->mAxisMinimum:F

    .line 76
    iput v1, p0, Lcom/github/mikephil/charting/components/YAxis;->mAxisRange:F

    .line 79
    sget-object v0, Lcom/github/mikephil/charting/components/YAxis$YAxisLabelPosition;->OUTSIDE_CHART:Lcom/github/mikephil/charting/components/YAxis$YAxisLabelPosition;

    iput-object v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mPosition:Lcom/github/mikephil/charting/components/YAxis$YAxisLabelPosition;

    .line 101
    sget-object v0, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->LEFT:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    iput-object v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mAxisDependency:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mLimitLines:Ljava/util/ArrayList;

    .line 103
    return-void
.end method

.method public constructor <init>(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)V
    .locals 6
    .param p1, "position"    # Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x7fc00000    # NaNf

    const/high16 v3, 0x41200000    # 10.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 106
    invoke-direct {p0}, Lcom/github/mikephil/charting/components/AxisBase;-><init>()V

    .line 28
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mEntries:[F

    .line 37
    const/4 v0, 0x6

    iput v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mLabelCount:I

    .line 40
    iput-boolean v5, p0, Lcom/github/mikephil/charting/components/YAxis;->mDrawTopYLabelEntry:Z

    .line 43
    iput-boolean v2, p0, Lcom/github/mikephil/charting/components/YAxis;->mShowOnlyMinMax:Z

    .line 46
    iput-boolean v2, p0, Lcom/github/mikephil/charting/components/YAxis;->mInverted:Z

    .line 49
    iput-boolean v5, p0, Lcom/github/mikephil/charting/components/YAxis;->mStartAtZero:Z

    .line 55
    iput v4, p0, Lcom/github/mikephil/charting/components/YAxis;->mCustomAxisMin:F

    .line 58
    iput v4, p0, Lcom/github/mikephil/charting/components/YAxis;->mCustomAxisMax:F

    .line 64
    iput v3, p0, Lcom/github/mikephil/charting/components/YAxis;->mSpacePercentTop:F

    .line 70
    iput v3, p0, Lcom/github/mikephil/charting/components/YAxis;->mSpacePercentBottom:F

    .line 72
    iput v1, p0, Lcom/github/mikephil/charting/components/YAxis;->mAxisMaximum:F

    .line 73
    iput v1, p0, Lcom/github/mikephil/charting/components/YAxis;->mAxisMinimum:F

    .line 76
    iput v1, p0, Lcom/github/mikephil/charting/components/YAxis;->mAxisRange:F

    .line 79
    sget-object v0, Lcom/github/mikephil/charting/components/YAxis$YAxisLabelPosition;->OUTSIDE_CHART:Lcom/github/mikephil/charting/components/YAxis$YAxisLabelPosition;

    iput-object v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mPosition:Lcom/github/mikephil/charting/components/YAxis$YAxisLabelPosition;

    .line 107
    iput-object p1, p0, Lcom/github/mikephil/charting/components/YAxis;->mAxisDependency:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mLimitLines:Ljava/util/ArrayList;

    .line 109
    return-void
.end method


# virtual methods
.method public getAxisDependency()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mAxisDependency:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    return-object v0
.end method

.method public getAxisMaxValue()F
    .locals 1

    .prologue
    .line 298
    iget v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mCustomAxisMax:F

    return v0
.end method

.method public getAxisMinValue()F
    .locals 1

    .prologue
    .line 273
    iget v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mCustomAxisMin:F

    return v0
.end method

.method public getFormattedLabel(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 396
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mEntries:[F

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 397
    :cond_0
    const-string v0, ""

    .line 399
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/github/mikephil/charting/components/YAxis;->getValueFormatter()Lcom/github/mikephil/charting/utils/ValueFormatter;

    move-result-object v0

    iget-object v1, p0, Lcom/github/mikephil/charting/components/YAxis;->mEntries:[F

    aget v1, v1, p1

    invoke-interface {v0, v1}, Lcom/github/mikephil/charting/utils/ValueFormatter;->getFormattedValue(F)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getLabelCount()I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mLabelCount:I

    return v0
.end method

.method public getLabelPosition()Lcom/github/mikephil/charting/components/YAxis$YAxisLabelPosition;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mPosition:Lcom/github/mikephil/charting/components/YAxis$YAxisLabelPosition;

    return-object v0
.end method

.method public getLimitLines()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/github/mikephil/charting/components/LimitLine;",
            ">;"
        }
    .end annotation

    .prologue
    .line 269
    iget-object v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mLimitLines:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSpaceBottom()F
    .locals 1

    .prologue
    .line 353
    iget v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mSpacePercentBottom:F

    return v0
.end method

.method public getSpaceTop()F
    .locals 1

    .prologue
    .line 335
    iget v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mSpacePercentTop:F

    return v0
.end method

.method public getValueFormatter()Lcom/github/mikephil/charting/utils/ValueFormatter;
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mValueFormatter:Lcom/github/mikephil/charting/utils/ValueFormatter;

    return-object v0
.end method

.method public isDrawTopYLabelEntryEnabled()Z
    .locals 1

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mDrawTopYLabelEntry:Z

    return v0
.end method

.method public isInverted()Z
    .locals 1

    .prologue
    .line 212
    iget-boolean v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mInverted:Z

    return v0
.end method

.method public isShowOnlyMinMaxEnabled()Z
    .locals 1

    .prologue
    .line 193
    iget-boolean v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mShowOnlyMinMax:Z

    return v0
.end method

.method public isStartAtZeroEnabled()Z
    .locals 1

    .prologue
    .line 230
    iget-boolean v0, p0, Lcom/github/mikephil/charting/components/YAxis;->mStartAtZero:Z

    return v0
.end method

.method public needsDefaultFormatter()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 435
    iget-object v1, p0, Lcom/github/mikephil/charting/components/YAxis;->mValueFormatter:Lcom/github/mikephil/charting/utils/ValueFormatter;

    if-nez v1, :cond_1

    .line 440
    :cond_0
    :goto_0
    return v0

    .line 437
    :cond_1
    iget-object v1, p0, Lcom/github/mikephil/charting/components/YAxis;->mValueFormatter:Lcom/github/mikephil/charting/utils/ValueFormatter;

    instance-of v1, v1, Lcom/github/mikephil/charting/utils/DefaultValueFormatter;

    if-nez v1, :cond_0

    .line 440
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setValueFormatter(Lcom/github/mikephil/charting/utils/ValueFormatter;)V
    .locals 0
    .param p1, "f"    # Lcom/github/mikephil/charting/utils/ValueFormatter;

    .prologue
    .line 413
    if-nez p1, :cond_0

    .line 417
    :goto_0
    return-void

    .line 416
    :cond_0
    iput-object p1, p0, Lcom/github/mikephil/charting/components/YAxis;->mValueFormatter:Lcom/github/mikephil/charting/utils/ValueFormatter;

    goto :goto_0
.end method
