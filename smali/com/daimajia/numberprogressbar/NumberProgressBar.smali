.class public Lcom/daimajia/numberprogressbar/NumberProgressBar;
.super Landroid/view/View;
.source "NumberProgressBar.java"

# interfaces
.implements Lcom/koushikdutta/ion/ProgressCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/daimajia/numberprogressbar/NumberProgressBar$ProgressTextVisibility;
    }
.end annotation


# instance fields
.field private final default_progress_text_offset:F

.field private final default_reached_bar_height:F

.field private final default_reached_color:I

.field private final default_text_color:I

.field private final default_text_size:F

.field private final default_unreached_bar_height:F

.field private final default_unreached_color:I

.field private mCurrentDrawText:Ljava/lang/String;

.field private mDrawReachedBar:Z

.field private mDrawTextEnd:F

.field private mDrawTextStart:F

.field private mDrawTextWidth:F

.field private mDrawUnreachedBar:Z

.field private mIfDrawText:Z

.field private mMax:I

.field private mOffset:F

.field private mPrefix:Ljava/lang/String;

.field private mProgress:I

.field private mReachedBarColor:I

.field private mReachedBarHeight:F

.field private mReachedBarPaint:Landroid/graphics/Paint;

.field private mReachedRectF:Landroid/graphics/RectF;

.field private mSuffix:Ljava/lang/String;

.field private mTextColor:I

.field private mTextPaint:Landroid/graphics/Paint;

.field private mTextSize:F

.field private mUnreachedBarColor:I

.field private mUnreachedBarHeight:F

.field private mUnreachedBarPaint:Landroid/graphics/Paint;

.field private mUnreachedRectF:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 180
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 181
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 184
    const v0, 0x7f010003

    invoke-direct {p0, p1, p2, v0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 185
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/16 v9, 0x42

    const/16 v8, 0xcc

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 188
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    const/16 v3, 0x64

    iput v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mMax:I

    .line 32
    iput v6, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mProgress:I

    .line 67
    const-string v3, "%"

    iput-object v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mSuffix:Ljava/lang/String;

    .line 72
    const-string v3, ""

    iput-object v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mPrefix:Ljava/lang/String;

    .line 75
    const/16 v3, 0x91

    const/16 v4, 0xf1

    invoke-static {v9, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    iput v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->default_text_color:I

    .line 76
    const/16 v3, 0x91

    const/16 v4, 0xf1

    invoke-static {v9, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    iput v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->default_reached_color:I

    .line 77
    invoke-static {v8, v8, v8}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    iput v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->default_unreached_color:I

    .line 139
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3, v5, v5, v5, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedRectF:Landroid/graphics/RectF;

    .line 143
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3, v5, v5, v5, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedRectF:Landroid/graphics/RectF;

    .line 153
    iput-boolean v7, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mDrawUnreachedBar:Z

    .line 155
    iput-boolean v7, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mDrawReachedBar:Z

    .line 157
    iput-boolean v7, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mIfDrawText:Z

    .line 190
    const/high16 v3, 0x3fc00000    # 1.5f

    invoke-virtual {p0, v3}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->dp2px(F)F

    move-result v3

    iput v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->default_reached_bar_height:F

    .line 191
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p0, v3}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->dp2px(F)F

    move-result v3

    iput v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->default_unreached_bar_height:F

    .line 192
    const/high16 v3, 0x41200000    # 10.0f

    invoke-virtual {p0, v3}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->sp2px(F)F

    move-result v3

    iput v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->default_text_size:F

    .line 193
    const/high16 v3, 0x40400000    # 3.0f

    invoke-virtual {p0, v3}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->dp2px(F)F

    move-result v3

    iput v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->default_progress_text_offset:F

    .line 196
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    sget-object v4, Lorg/namelessrom/devicecontrol/R$styleable;->NumberProgressBar:[I

    invoke-virtual {v3, p2, v4, p3, v6}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 200
    .local v0, "attributes":Landroid/content/res/TypedArray;
    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->isInEditMode()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 201
    iget v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->default_reached_color:I

    .line 206
    .local v1, "defaultColor":I
    :goto_0
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    iput v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedBarColor:I

    .line 207
    const/4 v3, 0x2

    iget v4, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->default_unreached_color:I

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    iput v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedBarColor:I

    .line 208
    const/4 v3, 0x7

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    iput v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextColor:I

    .line 209
    const/4 v3, 0x6

    iget v4, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->default_text_size:F

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextSize:F

    .line 211
    const/4 v3, 0x4

    iget v4, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->default_reached_bar_height:F

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedBarHeight:F

    .line 212
    const/4 v3, 0x5

    iget v4, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->default_unreached_bar_height:F

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedBarHeight:F

    .line 213
    const/16 v3, 0x8

    iget v4, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->default_progress_text_offset:F

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mOffset:F

    .line 215
    const/16 v3, 0x9

    invoke-virtual {v0, v3, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 216
    .local v2, "textVisible":I
    if-eqz v2, :cond_0

    .line 217
    iput-boolean v6, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mIfDrawText:Z

    .line 220
    :cond_0
    invoke-virtual {v0, v6, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->setProgress(I)V

    .line 221
    const/16 v3, 0x64

    invoke-virtual {v0, v7, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->setMax(I)V

    .line 223
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 225
    invoke-direct {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->initializePainters()V

    .line 227
    return-void

    .line 203
    .end local v1    # "defaultColor":I
    .end local v2    # "textVisible":I
    :cond_1
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v3

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/Application;->getAccentColor()I

    move-result v1

    .restart local v1    # "defaultColor":I
    goto :goto_0
.end method

.method private calculateDrawRectF()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    .line 313
    const-string v1, "%d"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getProgress()I

    move-result v3

    mul-int/lit8 v3, v3, 0x64

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getMax()I

    move-result v4

    div-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mCurrentDrawText:Ljava/lang/String;

    .line 314
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mPrefix:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mCurrentDrawText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mSuffix:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mCurrentDrawText:Ljava/lang/String;

    .line 315
    iget-object v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mCurrentDrawText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mDrawTextWidth:F

    .line 317
    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getProgress()I

    move-result v1

    if-nez v1, :cond_1

    .line 318
    iput-boolean v6, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mDrawReachedBar:Z

    .line 319
    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mDrawTextStart:F

    .line 329
    :goto_0
    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v5

    iget-object v2, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->descent()F

    move-result v2

    iget-object v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->ascent()F

    move-result v3

    add-float/2addr v2, v3

    div-float/2addr v2, v5

    sub-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v1, v1

    iput v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mDrawTextEnd:F

    .line 331
    iget v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mDrawTextStart:F

    iget v2, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mDrawTextWidth:F

    add-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    .line 332
    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mDrawTextWidth:F

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mDrawTextStart:F

    .line 333
    iget-object v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedRectF:Landroid/graphics/RectF;

    iget v2, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mDrawTextStart:F

    iget v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mOffset:F

    sub-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->right:F

    .line 336
    :cond_0
    iget v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mDrawTextStart:F

    iget v2, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mDrawTextWidth:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mOffset:F

    add-float v0, v1, v2

    .line 337
    .local v0, "unreachedBarStart":F
    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_2

    .line 338
    iput-boolean v6, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mDrawUnreachedBar:Z

    .line 346
    :goto_1
    return-void

    .line 321
    .end local v0    # "unreachedBarStart":F
    :cond_1
    iput-boolean v7, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mDrawReachedBar:Z

    .line 322
    iget-object v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedRectF:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getPaddingLeft()I

    move-result v2

    int-to-float v2, v2

    iput v2, v1, Landroid/graphics/RectF;->left:F

    .line 323
    iget-object v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedRectF:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v5

    iget v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedBarHeight:F

    div-float/2addr v3, v5

    sub-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->top:F

    .line 324
    iget-object v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedRectF:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getMax()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3f800000    # 1.0f

    mul-float/2addr v3, v4

    div-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getProgress()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mOffset:F

    sub-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getPaddingLeft()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->right:F

    .line 325
    iget-object v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedRectF:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v5

    iget v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedBarHeight:F

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    .line 326
    iget-object v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedRectF:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iget v2, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mOffset:F

    add-float/2addr v1, v2

    iput v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mDrawTextStart:F

    goto/16 :goto_0

    .line 340
    .restart local v0    # "unreachedBarStart":F
    :cond_2
    iput-boolean v7, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mDrawUnreachedBar:Z

    .line 341
    iget-object v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedRectF:Landroid/graphics/RectF;

    iput v0, v1, Landroid/graphics/RectF;->left:F

    .line 342
    iget-object v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedRectF:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iput v2, v1, Landroid/graphics/RectF;->right:F

    .line 343
    iget-object v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedRectF:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v5

    iget v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedBarHeight:F

    neg-float v3, v3

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->top:F

    .line 344
    iget-object v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedRectF:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v5

    iget v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedBarHeight:F

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1
.end method

.method private calculateDrawRectFWithoutProgressText()V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 300
    iget-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedRectF:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 301
    iget-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedRectF:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    iget v2, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedBarHeight:F

    div-float/2addr v2, v4

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 302
    iget-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedRectF:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getMax()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x3f800000    # 1.0f

    mul-float/2addr v2, v3

    div-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getProgress()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getPaddingLeft()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 303
    iget-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedRectF:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    iget v2, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedBarHeight:F

    div-float/2addr v2, v4

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 305
    iget-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedRectF:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedRectF:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 306
    iget-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedRectF:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 307
    iget-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedRectF:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    iget v2, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedBarHeight:F

    neg-float v2, v2

    div-float/2addr v2, v4

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 308
    iget-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedRectF:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    iget v2, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedBarHeight:F

    div-float/2addr v2, v4

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 309
    return-void
.end method

.method private initializePainters()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 287
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedBarPaint:Landroid/graphics/Paint;

    .line 288
    iget-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedBarPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedBarColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 290
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedBarPaint:Landroid/graphics/Paint;

    .line 291
    iget-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedBarPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedBarColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 293
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextPaint:Landroid/graphics/Paint;

    .line 294
    iget-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 295
    iget-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextSize:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 296
    return-void
.end method

.method private measure(IZ)I
    .locals 6
    .param p1, "measureSpec"    # I
    .param p2, "isWidth"    # Z

    .prologue
    .line 246
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 247
    .local v0, "mode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 248
    .local v3, "size":I
    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getPaddingLeft()I

    move-result v4

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getPaddingRight()I

    move-result v5

    add-int v1, v4, v5

    .line 249
    .local v1, "padding":I
    :goto_0
    const/high16 v4, 0x40000000    # 2.0f

    if-ne v0, v4, :cond_2

    .line 250
    move v2, v3

    .line 263
    .local v2, "result":I
    :cond_0
    :goto_1
    return v2

    .line 248
    .end local v1    # "padding":I
    .end local v2    # "result":I
    :cond_1
    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getPaddingBottom()I

    move-result v5

    add-int v1, v4, v5

    goto :goto_0

    .line 252
    .restart local v1    # "padding":I
    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getSuggestedMinimumWidth()I

    move-result v2

    .line 253
    .restart local v2    # "result":I
    :goto_2
    add-int/2addr v2, v1

    .line 254
    const/high16 v4, -0x80000000

    if-ne v0, v4, :cond_0

    .line 255
    if-eqz p2, :cond_4

    .line 256
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto :goto_1

    .line 252
    .end local v2    # "result":I
    :cond_3
    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getSuggestedMinimumHeight()I

    move-result v2

    goto :goto_2

    .line 259
    .restart local v2    # "result":I
    :cond_4
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto :goto_1
.end method


# virtual methods
.method public dp2px(F)F
    .locals 3
    .param p1, "dp"    # F

    .prologue
    .line 496
    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    .line 497
    .local v0, "scale":F
    mul-float v1, p1, v0

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    return v1
.end method

.method public getMax()I
    .locals 1

    .prologue
    .line 376
    iget v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mMax:I

    return v0
.end method

.method public getPrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mPrefix:Ljava/lang/String;

    return-object v0
.end method

.method public getProgress()I
    .locals 1

    .prologue
    .line 372
    iget v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mProgress:I

    return v0
.end method

.method public getProgressTextSize()F
    .locals 1

    .prologue
    .line 360
    iget v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextSize:F

    return v0
.end method

.method public getReachedBarColor()I
    .locals 1

    .prologue
    .line 368
    iget v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedBarColor:I

    return v0
.end method

.method public getReachedBarHeight()F
    .locals 1

    .prologue
    .line 380
    iget v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedBarHeight:F

    return v0
.end method

.method public getSuffix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 429
    iget-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mSuffix:Ljava/lang/String;

    return-object v0
.end method

.method protected getSuggestedMinimumHeight()I
    .locals 3

    .prologue
    .line 236
    iget v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextSize:F

    float-to-int v0, v0

    iget v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedBarHeight:F

    float-to-int v1, v1

    iget v2, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedBarHeight:F

    float-to-int v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method protected getSuggestedMinimumWidth()I
    .locals 1

    .prologue
    .line 231
    iget v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextSize:F

    float-to-int v0, v0

    return v0
.end method

.method public getTextColor()I
    .locals 1

    .prologue
    .line 352
    iget v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextColor:I

    return v0
.end method

.method public getUnreachedBarColor()I
    .locals 1

    .prologue
    .line 364
    iget v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedBarColor:I

    return v0
.end method

.method public getUnreachedBarHeight()F
    .locals 1

    .prologue
    .line 384
    iget v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedBarHeight:F

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 268
    iget-boolean v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mIfDrawText:Z

    if-eqz v0, :cond_3

    .line 269
    invoke-direct {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->calculateDrawRectF()V

    .line 274
    :goto_0
    iget-boolean v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mDrawReachedBar:Z

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedRectF:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedBarPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 278
    :cond_0
    iget-boolean v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mDrawUnreachedBar:Z

    if-eqz v0, :cond_1

    .line 279
    iget-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedRectF:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedBarPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 282
    :cond_1
    iget-boolean v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mIfDrawText:Z

    if-eqz v0, :cond_2

    .line 283
    iget-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mCurrentDrawText:Ljava/lang/String;

    iget v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mDrawTextStart:F

    iget v2, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mDrawTextEnd:F

    iget-object v3, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 284
    :cond_2
    return-void

    .line 271
    :cond_3
    invoke-direct {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->calculateDrawRectFWithoutProgressText()V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 241
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->measure(IZ)I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, p2, v1}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->measure(IZ)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->setMeasuredDimension(II)V

    .line 242
    return-void
.end method

.method public onProgress(JJ)V
    .locals 7
    .param p1, "downloaded"    # J
    .param p3, "total"    # J

    .prologue
    .line 161
    const-wide/16 v2, 0x0

    cmp-long v1, p3, v2

    if-lez v1, :cond_0

    .line 162
    long-to-double v2, p1

    long-to-double v4, p3

    div-double/2addr v2, v4

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    mul-double/2addr v2, v4

    double-to-int v0, v2

    .line 166
    .local v0, "progress":I
    :goto_0
    new-instance v1, Lcom/daimajia/numberprogressbar/NumberProgressBar$1;

    invoke-direct {v1, p0, v0}, Lcom/daimajia/numberprogressbar/NumberProgressBar$1;-><init>(Lcom/daimajia/numberprogressbar/NumberProgressBar;I)V

    invoke-virtual {p0, v1}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->post(Ljava/lang/Runnable;)Z

    .line 171
    return-void

    .line 164
    .end local v0    # "progress":I
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "progress":I
    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 476
    instance-of v1, p1, Landroid/os/Bundle;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 477
    check-cast v0, Landroid/os/Bundle;

    .line 478
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "text_color"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextColor:I

    .line 479
    const-string v1, "text_size"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextSize:F

    .line 480
    const-string v1, "reached_bar_height"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedBarHeight:F

    .line 481
    const-string v1, "unreached_bar_height"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedBarHeight:F

    .line 482
    const-string v1, "reached_bar_color"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedBarColor:I

    .line 483
    const-string v1, "unreached_bar_color"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedBarColor:I

    .line 484
    invoke-direct {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->initializePainters()V

    .line 485
    const-string v1, "max"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->setMax(I)V

    .line 486
    const-string v1, "progress"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->setProgress(I)V

    .line 487
    const-string v1, "prefix"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->setPrefix(Ljava/lang/String;)V

    .line 488
    const-string v1, "suffix"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->setSuffix(Ljava/lang/String;)V

    .line 489
    const-string v1, "saved_instance"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 493
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 492
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 459
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 460
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "saved_instance"

    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 461
    const-string v1, "text_color"

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getTextColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 462
    const-string v1, "text_size"

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getProgressTextSize()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 463
    const-string v1, "reached_bar_height"

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getReachedBarHeight()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 464
    const-string v1, "unreached_bar_height"

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getUnreachedBarHeight()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 465
    const-string v1, "reached_bar_color"

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getReachedBarColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 466
    const-string v1, "unreached_bar_color"

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getUnreachedBarColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 467
    const-string v1, "max"

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getMax()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 468
    const-string v1, "progress"

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getProgress()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 469
    const-string v1, "suffix"

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getSuffix()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    const-string v1, "prefix"

    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getPrefix()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    return-object v0
.end method

.method public setMax(I)V
    .locals 0
    .param p1, "Max"    # I

    .prologue
    .line 414
    if-lez p1, :cond_0

    .line 415
    iput p1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mMax:I

    .line 416
    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->invalidate()V

    .line 418
    :cond_0
    return-void
.end method

.method public setPrefix(Ljava/lang/String;)V
    .locals 1
    .param p1, "prefix"    # Ljava/lang/String;

    .prologue
    .line 433
    if-nez p1, :cond_0

    .line 434
    const-string v0, ""

    iput-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mPrefix:Ljava/lang/String;

    .line 438
    :goto_0
    return-void

    .line 436
    :cond_0
    iput-object p1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mPrefix:Ljava/lang/String;

    goto :goto_0
.end method

.method public setProgress(I)V
    .locals 1
    .param p1, "Progress"    # I

    .prologue
    .line 451
    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getMax()I

    move-result v0

    if-gt p1, v0, :cond_0

    if-ltz p1, :cond_0

    .line 452
    iput p1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mProgress:I

    .line 453
    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->invalidate()V

    .line 455
    :cond_0
    return-void
.end method

.method public setProgressTextColor(I)V
    .locals 2
    .param p1, "TextColor"    # I

    .prologue
    .line 396
    iput p1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextColor:I

    .line 397
    iget-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 398
    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->invalidate()V

    .line 399
    return-void
.end method

.method public setProgressTextSize(F)V
    .locals 2
    .param p1, "TextSize"    # F

    .prologue
    .line 390
    iput p1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextSize:F

    .line 391
    iget-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mTextSize:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 392
    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->invalidate()V

    .line 393
    return-void
.end method

.method public setProgressTextVisibility(Lcom/daimajia/numberprogressbar/NumberProgressBar$ProgressTextVisibility;)V
    .locals 1
    .param p1, "visibility"    # Lcom/daimajia/numberprogressbar/NumberProgressBar$ProgressTextVisibility;

    .prologue
    .line 506
    sget-object v0, Lcom/daimajia/numberprogressbar/NumberProgressBar$ProgressTextVisibility;->Visible:Lcom/daimajia/numberprogressbar/NumberProgressBar$ProgressTextVisibility;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mIfDrawText:Z

    .line 507
    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->invalidate()V

    .line 508
    return-void

    .line 506
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setReachedBarColor(I)V
    .locals 2
    .param p1, "ProgressColor"    # I

    .prologue
    .line 408
    iput p1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedBarColor:I

    .line 409
    iget-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedBarPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedBarColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 410
    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->invalidate()V

    .line 411
    return-void
.end method

.method public setSuffix(Ljava/lang/String;)V
    .locals 1
    .param p1, "suffix"    # Ljava/lang/String;

    .prologue
    .line 421
    if-nez p1, :cond_0

    .line 422
    const-string v0, ""

    iput-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mSuffix:Ljava/lang/String;

    .line 426
    :goto_0
    return-void

    .line 424
    :cond_0
    iput-object p1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mSuffix:Ljava/lang/String;

    goto :goto_0
.end method

.method public setUnreachedBarColor(I)V
    .locals 2
    .param p1, "BarColor"    # I

    .prologue
    .line 402
    iput p1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedBarColor:I

    .line 403
    iget-object v0, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mUnreachedBarPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/daimajia/numberprogressbar/NumberProgressBar;->mReachedBarColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 404
    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->invalidate()V

    .line 405
    return-void
.end method

.method public sp2px(F)F
    .locals 2
    .param p1, "sp"    # F

    .prologue
    .line 501
    invoke-virtual {p0}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->scaledDensity:F

    .line 502
    .local v0, "scale":F
    mul-float v1, p1, v0

    return v1
.end method
