.class public abstract Lcom/balysv/materialmenu/MaterialMenuBase;
.super Ljava/lang/Object;
.source "MaterialMenuBase.java"


# instance fields
.field private currentState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

.field private drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;


# direct methods
.method public constructor <init>(Landroid/app/Activity;ILcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;)V
    .locals 6
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "color"    # I
    .param p3, "stroke"    # Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    .prologue
    .line 45
    const/16 v4, 0x320

    const/16 v5, 0x190

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/balysv/materialmenu/MaterialMenuBase;-><init>(Landroid/app/Activity;ILcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;II)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;ILcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;II)V
    .locals 7
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "color"    # I
    .param p3, "stroke"    # Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;
    .param p4, "transformDuration"    # I
    .param p5, "pressedDuration"    # I

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    sget-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->BURGER:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    iput-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuBase;->currentState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    .line 53
    new-instance v0, Lcom/balysv/materialmenu/MaterialMenuDrawable;

    const/4 v4, 0x1

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/balysv/materialmenu/MaterialMenuDrawable;-><init>(Landroid/content/Context;ILcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;III)V

    iput-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuBase;->drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    .line 54
    invoke-virtual {p0, p1}, Lcom/balysv/materialmenu/MaterialMenuBase;->setActionBarSettings(Landroid/app/Activity;)V

    .line 55
    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuBase;->providesActionBar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    invoke-direct {p0, p1}, Lcom/balysv/materialmenu/MaterialMenuBase;->setupActionBar(Landroid/app/Activity;)V

    .line 58
    :cond_0
    return-void
.end method

.method private setupActionBar(Landroid/app/Activity;)V
    .locals 7
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v6, 0x0

    .line 61
    invoke-virtual {p0, p1}, Lcom/balysv/materialmenu/MaterialMenuBase;->getActionBarHomeView(Landroid/app/Activity;)Landroid/view/View;

    move-result-object v1

    .line 62
    .local v1, "iconView":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/balysv/materialmenu/MaterialMenuBase;->getActionBarUpView(Landroid/app/Activity;)Landroid/view/View;

    move-result-object v3

    .line 64
    .local v3, "upView":Landroid/view/View;
    if-eqz v1, :cond_0

    if-nez v3, :cond_1

    .line 65
    :cond_0
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Could not find ActionBar views"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 69
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 70
    .local v0, "iconParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 71
    iput v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 72
    iput v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 73
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 76
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 77
    .local v2, "upParams":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0042

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 78
    iput v6, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 79
    invoke-virtual {v3, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 80
    return-void
.end method


# virtual methods
.method public final animatePressedState(Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;)V
    .locals 2
    .param p1, "state"    # Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/balysv/materialmenu/MaterialMenuBase;->currentState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    .line 110
    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuBase;->getDrawable()Lcom/balysv/materialmenu/MaterialMenuDrawable;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animateIconState(Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;Z)V

    .line 111
    return-void
.end method

.method public final animateState(Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;)V
    .locals 2
    .param p1, "state"    # Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/balysv/materialmenu/MaterialMenuBase;->currentState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    .line 104
    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuBase;->getDrawable()Lcom/balysv/materialmenu/MaterialMenuDrawable;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animateIconState(Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;Z)V

    .line 105
    return-void
.end method

.method protected abstract getActionBarHomeView(Landroid/app/Activity;)Landroid/view/View;
.end method

.method protected abstract getActionBarUpView(Landroid/app/Activity;)Landroid/view/View;
.end method

.method public final getDrawable()Lcom/balysv/materialmenu/MaterialMenuDrawable;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuBase;->drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 163
    const-string v0, "material_menu_icon_state"

    iget-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuBase;->currentState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    invoke-virtual {v1}, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    return-void
.end method

.method protected abstract providesActionBar()Z
.end method

.method protected abstract setActionBarSettings(Landroid/app/Activity;)V
.end method

.method public final setNeverDrawTouch(Z)V
    .locals 1
    .param p1, "neverDrawTouch"    # Z

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuBase;->getDrawable()Lcom/balysv/materialmenu/MaterialMenuDrawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->setNeverDrawTouch(Z)V

    .line 155
    return-void
.end method

.method public final setState(Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;)V
    .locals 1
    .param p1, "state"    # Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/balysv/materialmenu/MaterialMenuBase;->currentState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    .line 93
    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuBase;->getDrawable()Lcom/balysv/materialmenu/MaterialMenuDrawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->setIconState(Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;)V

    .line 94
    return-void
.end method

.method public syncState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Bundle;

    .prologue
    .line 172
    if-eqz p1, :cond_1

    .line 173
    const-string v1, "material_menu_icon_state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 174
    .local v0, "iconStateName":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 175
    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->BURGER:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    invoke-virtual {v1}, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->name()Ljava/lang/String;

    move-result-object v0

    .line 177
    :cond_0
    invoke-static {v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->valueOf(Ljava/lang/String;)Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/balysv/materialmenu/MaterialMenuBase;->setState(Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;)V

    .line 179
    .end local v0    # "iconStateName":Ljava/lang/String;
    :cond_1
    return-void
.end method
