.class public Lcom/balysv/materialmenu/MaterialMenuView;
.super Landroid/view/View;
.source "MaterialMenuView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/balysv/materialmenu/MaterialMenuView$1;,
        Lcom/balysv/materialmenu/MaterialMenuView$SavedState;
    }
.end annotation


# instance fields
.field private currentState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

.field private drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;


# direct methods
.method private adjustDrawablePadding()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 202
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuView;->drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuView;->drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    iget-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuView;->drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    invoke-virtual {v1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuView;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/balysv/materialmenu/MaterialMenuView;->drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    invoke-virtual {v2}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuView;->getPaddingTop()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v0, v4, v4, v1, v2}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->setBounds(IIII)V

    .line 209
    :cond_0
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 85
    invoke-super {p0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 86
    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuView;->getPaddingLeft()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuView;->getPaddingTop()I

    move-result v1

    if-eqz v1, :cond_1

    .line 87
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v0

    .line 88
    .local v0, "saveCount":I
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 89
    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuView;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuView;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 90
    iget-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuView;->drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    invoke-virtual {v1, p1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 91
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 95
    .end local v0    # "saveCount":I
    :goto_0
    return-void

    .line 93
    :cond_1
    iget-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuView;->drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    invoke-virtual {v1, p1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public getDrawable()Lcom/balysv/materialmenu/MaterialMenuDrawable;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuView;->drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    return-object v0
.end method

.method public getState()Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuView;->drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    invoke-virtual {v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->getIconState()Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    move-result-object v0

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 168
    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuView;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuView;->getPaddingRight()I

    move-result v3

    add-int v0, v2, v3

    .line 169
    .local v0, "paddingX":I
    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuView;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuView;->getPaddingBottom()I

    move-result v3

    add-int v1, v2, v3

    .line 171
    .local v1, "paddingY":I
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_0

    .line 172
    iget-object v2, p0, Lcom/balysv/materialmenu/MaterialMenuView;->drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    invoke-virtual {v2}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->getIntrinsicWidth()I

    move-result v2

    add-int/2addr v2, v0

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 173
    iget-object v2, p0, Lcom/balysv/materialmenu/MaterialMenuView;->drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    invoke-virtual {v2}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->getIntrinsicHeight()I

    move-result v2

    add-int/2addr v2, v1

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 174
    invoke-virtual {p0, p1, p2}, Lcom/balysv/materialmenu/MaterialMenuView;->setMeasuredDimension(II)V

    .line 178
    :goto_0
    return-void

    .line 176
    :cond_0
    iget-object v2, p0, Lcom/balysv/materialmenu/MaterialMenuView;->drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    invoke-virtual {v2}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->getIntrinsicWidth()I

    move-result v2

    add-int/2addr v2, v0

    iget-object v3, p0, Lcom/balysv/materialmenu/MaterialMenuView;->drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    invoke-virtual {v3}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->getIntrinsicHeight()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {p0, v2, v3}, Lcom/balysv/materialmenu/MaterialMenuView;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 196
    move-object v0, p1

    check-cast v0, Lcom/balysv/materialmenu/MaterialMenuView$SavedState;

    .line 197
    .local v0, "savedState":Lcom/balysv/materialmenu/MaterialMenuView$SavedState;
    invoke-virtual {v0}, Lcom/balysv/materialmenu/MaterialMenuView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 198
    iget-object v1, v0, Lcom/balysv/materialmenu/MaterialMenuView$SavedState;->state:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    invoke-virtual {p0, v1}, Lcom/balysv/materialmenu/MaterialMenuView;->setState(Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;)V

    .line 199
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 188
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 189
    .local v1, "superState":Landroid/os/Parcelable;
    new-instance v0, Lcom/balysv/materialmenu/MaterialMenuView$SavedState;

    invoke-direct {v0, v1}, Lcom/balysv/materialmenu/MaterialMenuView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 190
    .local v0, "savedState":Lcom/balysv/materialmenu/MaterialMenuView$SavedState;
    iget-object v2, p0, Lcom/balysv/materialmenu/MaterialMenuView;->currentState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    iput-object v2, v0, Lcom/balysv/materialmenu/MaterialMenuView$SavedState;->state:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    .line 191
    return-object v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 182
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 183
    invoke-direct {p0}, Lcom/balysv/materialmenu/MaterialMenuView;->adjustDrawablePadding()V

    .line 184
    return-void
.end method

.method public setColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 133
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuView;->drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    invoke-virtual {v0, p1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->setColor(I)V

    .line 134
    return-void
.end method

.method public setInterpolator(Landroid/view/animation/Interpolator;)V
    .locals 1
    .param p1, "interpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuView;->drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    invoke-virtual {v0, p1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 149
    return-void
.end method

.method public setPadding(IIII)V
    .locals 0
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 99
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->setPadding(IIII)V

    .line 100
    invoke-direct {p0}, Lcom/balysv/materialmenu/MaterialMenuView;->adjustDrawablePadding()V

    .line 101
    return-void
.end method

.method public setPressedDuration(I)V
    .locals 1
    .param p1, "duration"    # I

    .prologue
    .line 143
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuView;->drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    invoke-virtual {v0, p1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->setPressedDuration(I)V

    .line 144
    return-void
.end method

.method public setRTLEnabled(Z)V
    .locals 1
    .param p1, "rtlEnabled"    # Z

    .prologue
    .line 153
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuView;->drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    invoke-virtual {v0, p1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->setRTLEnabled(Z)V

    .line 154
    return-void
.end method

.method public setState(Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;)V
    .locals 1
    .param p1, "state"    # Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/balysv/materialmenu/MaterialMenuView;->currentState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    .line 111
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuView;->drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    invoke-virtual {v0, p1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->setIconState(Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;)V

    .line 112
    return-void
.end method

.method public setTransformationDuration(I)V
    .locals 1
    .param p1, "duration"    # I

    .prologue
    .line 138
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuView;->drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    invoke-virtual {v0, p1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->setTransformationDuration(I)V

    .line 139
    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1, "who"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuView;->drawable:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
