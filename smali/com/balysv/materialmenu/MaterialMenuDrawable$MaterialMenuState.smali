.class final Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;
.super Landroid/graphics/drawable/Drawable$ConstantState;
.source "MaterialMenuDrawable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/balysv/materialmenu/MaterialMenuDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MaterialMenuState"
.end annotation


# instance fields
.field private changingConfigurations:I

.field final synthetic this$0:Lcom/balysv/materialmenu/MaterialMenuDrawable;


# direct methods
.method private constructor <init>(Lcom/balysv/materialmenu/MaterialMenuDrawable;)V
    .locals 0

    .prologue
    .line 884
    iput-object p1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;->this$0:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V

    .line 885
    return-void
.end method

.method synthetic constructor <init>(Lcom/balysv/materialmenu/MaterialMenuDrawable;Lcom/balysv/materialmenu/MaterialMenuDrawable$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/balysv/materialmenu/MaterialMenuDrawable;
    .param p2, "x1"    # Lcom/balysv/materialmenu/MaterialMenuDrawable$1;

    .prologue
    .line 881
    invoke-direct {p0, p1}, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;-><init>(Lcom/balysv/materialmenu/MaterialMenuDrawable;)V

    return-void
.end method

.method static synthetic access$502(Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;I)I
    .locals 0
    .param p0, "x0"    # Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;
    .param p1, "x1"    # I

    .prologue
    .line 881
    iput p1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;->changingConfigurations:I

    return p1
.end method


# virtual methods
.method public getChangingConfigurations()I
    .locals 1

    .prologue
    .line 900
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;->changingConfigurations:I

    return v0
.end method

.method public newDrawable()Landroid/graphics/drawable/Drawable;
    .locals 15

    .prologue
    .line 889
    new-instance v1, Lcom/balysv/materialmenu/MaterialMenuDrawable;

    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;->this$0:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    # getter for: Lcom/balysv/materialmenu/MaterialMenuDrawable;->circlePaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->access$600(Lcom/balysv/materialmenu/MaterialMenuDrawable;)Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v2

    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;->this$0:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    # getter for: Lcom/balysv/materialmenu/MaterialMenuDrawable;->stroke:Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;
    invoke-static {v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->access$700(Lcom/balysv/materialmenu/MaterialMenuDrawable;)Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    move-result-object v3

    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;->this$0:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    # getter for: Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformation:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->access$800(Lcom/balysv/materialmenu/MaterialMenuDrawable;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->getDuration()J

    move-result-wide v4

    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;->this$0:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    # getter for: Lcom/balysv/materialmenu/MaterialMenuDrawable;->pressedCircle:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->access$900(Lcom/balysv/materialmenu/MaterialMenuDrawable;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->getDuration()J

    move-result-wide v6

    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;->this$0:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    # getter for: Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I
    invoke-static {v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->access$1000(Lcom/balysv/materialmenu/MaterialMenuDrawable;)I

    move-result v8

    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;->this$0:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    # getter for: Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I
    invoke-static {v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->access$1100(Lcom/balysv/materialmenu/MaterialMenuDrawable;)I

    move-result v9

    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;->this$0:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    # getter for: Lcom/balysv/materialmenu/MaterialMenuDrawable;->iconWidth:F
    invoke-static {v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->access$1200(Lcom/balysv/materialmenu/MaterialMenuDrawable;)F

    move-result v10

    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;->this$0:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    # getter for: Lcom/balysv/materialmenu/MaterialMenuDrawable;->circleRadius:F
    invoke-static {v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->access$1300(Lcom/balysv/materialmenu/MaterialMenuDrawable;)F

    move-result v11

    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;->this$0:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    # getter for: Lcom/balysv/materialmenu/MaterialMenuDrawable;->strokeWidth:F
    invoke-static {v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->access$1400(Lcom/balysv/materialmenu/MaterialMenuDrawable;)F

    move-result v12

    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;->this$0:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    # getter for: Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip1:F
    invoke-static {v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->access$1500(Lcom/balysv/materialmenu/MaterialMenuDrawable;)F

    move-result v13

    const/4 v14, 0x0

    invoke-direct/range {v1 .. v14}, Lcom/balysv/materialmenu/MaterialMenuDrawable;-><init>(ILcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;JJIIFFFFLcom/balysv/materialmenu/MaterialMenuDrawable$1;)V

    .line 893
    .local v1, "drawable":Lcom/balysv/materialmenu/MaterialMenuDrawable;
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;->this$0:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    # getter for: Lcom/balysv/materialmenu/MaterialMenuDrawable;->animatingIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;
    invoke-static {v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->access$300(Lcom/balysv/materialmenu/MaterialMenuDrawable;)Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;->this$0:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    # getter for: Lcom/balysv/materialmenu/MaterialMenuDrawable;->animatingIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;
    invoke-static {v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->access$300(Lcom/balysv/materialmenu/MaterialMenuDrawable;)Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->setIconState(Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;)V

    .line 894
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;->this$0:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    # getter for: Lcom/balysv/materialmenu/MaterialMenuDrawable;->rtlEnabled:Z
    invoke-static {v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->access$1800(Lcom/balysv/materialmenu/MaterialMenuDrawable;)Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->setRTLEnabled(Z)V

    .line 895
    return-object v1

    .line 893
    :cond_0
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;->this$0:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    # getter for: Lcom/balysv/materialmenu/MaterialMenuDrawable;->currentIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;
    invoke-static {v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->access$1700(Lcom/balysv/materialmenu/MaterialMenuDrawable;)Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    move-result-object v0

    goto :goto_0
.end method
