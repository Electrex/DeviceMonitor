.class Lcom/balysv/materialmenu/MaterialMenuDrawable$1;
.super Landroid/util/Property;
.source "MaterialMenuDrawable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/balysv/materialmenu/MaterialMenuDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/Property",
        "<",
        "Lcom/balysv/materialmenu/MaterialMenuDrawable;",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/balysv/materialmenu/MaterialMenuDrawable;


# direct methods
.method constructor <init>(Lcom/balysv/materialmenu/MaterialMenuDrawable;Ljava/lang/Class;Ljava/lang/String;)V
    .locals 0
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 708
    .local p2, "x0":Ljava/lang/Class;, "Ljava/lang/Class<Ljava/lang/Float;>;"
    iput-object p1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable$1;->this$0:Lcom/balysv/materialmenu/MaterialMenuDrawable;

    invoke-direct {p0, p2, p3}, Landroid/util/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public get(Lcom/balysv/materialmenu/MaterialMenuDrawable;)Ljava/lang/Float;
    .locals 1
    .param p1, "object"    # Lcom/balysv/materialmenu/MaterialMenuDrawable;

    .prologue
    .line 711
    invoke-virtual {p1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->getTransformationValue()Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 708
    check-cast p1, Lcom/balysv/materialmenu/MaterialMenuDrawable;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/balysv/materialmenu/MaterialMenuDrawable$1;->get(Lcom/balysv/materialmenu/MaterialMenuDrawable;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public set(Lcom/balysv/materialmenu/MaterialMenuDrawable;Ljava/lang/Float;)V
    .locals 0
    .param p1, "object"    # Lcom/balysv/materialmenu/MaterialMenuDrawable;
    .param p2, "value"    # Ljava/lang/Float;

    .prologue
    .line 716
    invoke-virtual {p1, p2}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->setTransformationValue(Ljava/lang/Float;)V

    .line 717
    return-void
.end method

.method public bridge synthetic set(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 708
    check-cast p1, Lcom/balysv/materialmenu/MaterialMenuDrawable;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Float;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/balysv/materialmenu/MaterialMenuDrawable$1;->set(Lcom/balysv/materialmenu/MaterialMenuDrawable;Ljava/lang/Float;)V

    return-void
.end method
