.class Lcom/balysv/materialmenu/MaterialMenuView$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "MaterialMenuView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/balysv/materialmenu/MaterialMenuView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/balysv/materialmenu/MaterialMenuView$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected state:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 233
    new-instance v0, Lcom/balysv/materialmenu/MaterialMenuView$SavedState$1;

    invoke-direct {v0}, Lcom/balysv/materialmenu/MaterialMenuView$SavedState$1;-><init>()V

    sput-object v0, Lcom/balysv/materialmenu/MaterialMenuView$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 223
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 224
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->valueOf(Ljava/lang/String;)Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    move-result-object v0

    iput-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuView$SavedState;->state:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    .line 225
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/balysv/materialmenu/MaterialMenuView$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/balysv/materialmenu/MaterialMenuView$1;

    .prologue
    .line 215
    invoke-direct {p0, p1}, Lcom/balysv/materialmenu/MaterialMenuView$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(Landroid/os/Parcelable;)V
    .locals 0
    .param p1, "superState"    # Landroid/os/Parcelable;

    .prologue
    .line 219
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 220
    return-void
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 229
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 230
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuView$SavedState;->state:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    invoke-virtual {v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 231
    return-void
.end method
