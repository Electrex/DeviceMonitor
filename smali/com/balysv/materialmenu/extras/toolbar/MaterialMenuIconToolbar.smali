.class public abstract Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;
.super Lcom/balysv/materialmenu/MaterialMenuBase;
.source "MaterialMenuIconToolbar.java"


# direct methods
.method public constructor <init>(Landroid/app/Activity;ILcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "color"    # I
    .param p3, "stroke"    # Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    .prologue
    .line 13
    invoke-direct {p0, p1, p2, p3}, Lcom/balysv/materialmenu/MaterialMenuBase;-><init>(Landroid/app/Activity;ILcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;)V

    .line 14
    return-void
.end method


# virtual methods
.method protected final getActionBarHomeView(Landroid/app/Activity;)Landroid/view/View;
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 32
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final getActionBarUpView(Landroid/app/Activity;)Landroid/view/View;
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 37
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getToolbarViewId()I
.end method

.method protected final providesActionBar()Z
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return v0
.end method

.method protected final setActionBarSettings(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;->getToolbarViewId()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 27
    .local v0, "toolbar":Landroid/support/v7/widget/Toolbar;
    invoke-virtual {p0}, Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;->getDrawable()Lcom/balysv/materialmenu/MaterialMenuDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    .line 28
    return-void
.end method
