.class public final enum Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;
.super Ljava/lang/Enum;
.source "MaterialMenuDrawable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/balysv/materialmenu/MaterialMenuDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "IconState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

.field public static final enum ARROW:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

.field public static final enum BURGER:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

.field public static final enum CHECK:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

.field public static final enum X:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 42
    new-instance v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    const-string v1, "BURGER"

    invoke-direct {v0, v1, v2}, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->BURGER:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    new-instance v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    const-string v1, "ARROW"

    invoke-direct {v0, v1, v3}, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->ARROW:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    new-instance v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    const-string v1, "X"

    invoke-direct {v0, v1, v4}, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->X:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    new-instance v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    const-string v1, "CHECK"

    invoke-direct {v0, v1, v5}, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->CHECK:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    .line 41
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->BURGER:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->ARROW:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->X:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->CHECK:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->$VALUES:[Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 41
    const-class v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    return-object v0
.end method

.method public static values()[Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->$VALUES:[Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    invoke-virtual {v0}, [Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    return-object v0
.end method
