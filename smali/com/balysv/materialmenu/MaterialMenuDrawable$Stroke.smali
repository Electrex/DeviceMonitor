.class public final enum Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;
.super Ljava/lang/Enum;
.source "MaterialMenuDrawable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/balysv/materialmenu/MaterialMenuDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Stroke"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

.field public static final enum EXTRA_THIN:Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

.field public static final enum REGULAR:Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

.field public static final enum THIN:Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;


# instance fields
.field private final strokeWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 91
    new-instance v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    const-string v1, "REGULAR"

    invoke-direct {v0, v1, v4, v5}, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;->REGULAR:Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    .line 95
    new-instance v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    const-string v1, "THIN"

    invoke-direct {v0, v1, v2, v3}, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;->THIN:Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    .line 99
    new-instance v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    const-string v1, "EXTRA_THIN"

    invoke-direct {v0, v1, v3, v2}, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;->EXTRA_THIN:Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    .line 87
    new-array v0, v5, [Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;->REGULAR:Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    aput-object v1, v0, v4

    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;->THIN:Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    aput-object v1, v0, v2

    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;->EXTRA_THIN:Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    aput-object v1, v0, v3

    sput-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;->$VALUES:[Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "strokeWidth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 103
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 104
    iput p3, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;->strokeWidth:I

    .line 105
    return-void
.end method

.method static synthetic access$000(Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;)I
    .locals 1
    .param p0, "x0"    # Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    .prologue
    .line 87
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;->strokeWidth:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 87
    const-class v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    return-object v0
.end method

.method public static values()[Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;->$VALUES:[Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    invoke-virtual {v0}, [Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    return-object v0
.end method
