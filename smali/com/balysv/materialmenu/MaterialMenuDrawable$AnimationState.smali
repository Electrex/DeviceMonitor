.class public final enum Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;
.super Ljava/lang/Enum;
.source "MaterialMenuDrawable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/balysv/materialmenu/MaterialMenuDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AnimationState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

.field public static final enum ARROW_CHECK:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

.field public static final enum ARROW_X:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

.field public static final enum BURGER_ARROW:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

.field public static final enum BURGER_CHECK:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

.field public static final enum BURGER_X:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

.field public static final enum X_CHECK:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 46
    new-instance v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    const-string v1, "BURGER_ARROW"

    invoke-direct {v0, v1, v3}, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->BURGER_ARROW:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    new-instance v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    const-string v1, "BURGER_X"

    invoke-direct {v0, v1, v4}, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->BURGER_X:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    new-instance v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    const-string v1, "ARROW_X"

    invoke-direct {v0, v1, v5}, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->ARROW_X:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    new-instance v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    const-string v1, "ARROW_CHECK"

    invoke-direct {v0, v1, v6}, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->ARROW_CHECK:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    new-instance v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    const-string v1, "BURGER_CHECK"

    invoke-direct {v0, v1, v7}, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->BURGER_CHECK:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    new-instance v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    const-string v1, "X_CHECK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->X_CHECK:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    .line 45
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->BURGER_ARROW:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->BURGER_X:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->ARROW_X:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->ARROW_CHECK:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->BURGER_CHECK:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->X_CHECK:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->$VALUES:[Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 45
    const-class v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    return-object v0
.end method

.method public static values()[Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->$VALUES:[Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    invoke-virtual {v0}, [Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    return-object v0
.end method
