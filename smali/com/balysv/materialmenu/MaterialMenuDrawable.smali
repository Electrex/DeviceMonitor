.class public Lcom/balysv/materialmenu/MaterialMenuDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "MaterialMenuDrawable.java"

# interfaces
.implements Landroid/graphics/drawable/Animatable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/balysv/materialmenu/MaterialMenuDrawable$5;,
        Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;,
        Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;,
        Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;,
        Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;
    }
.end annotation


# instance fields
.field private animatingIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

.field private animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

.field private final circlePaint:Landroid/graphics/Paint;

.field private final circleRadius:F

.field private currentIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

.field private final dip1:F

.field private final dip2:F

.field private final dip3:F

.field private final dip4:F

.field private final dip6:F

.field private final dip8:F

.field private final diph:F

.field private drawTouchCircle:Z

.field private final gridPaint:Landroid/graphics/Paint;

.field private final height:I

.field private final iconPaint:Landroid/graphics/Paint;

.field private final iconWidth:F

.field private final lock:Ljava/lang/Object;

.field private materialMenuState:Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;

.field private neverDrawTouch:Z

.field private pressedCircle:Landroid/animation/ObjectAnimator;

.field private pressedProgressProperty:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Lcom/balysv/materialmenu/MaterialMenuDrawable;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private pressedProgressValue:F

.field private rtlEnabled:Z

.field private final sidePadding:F

.field private final stroke:Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

.field private final strokeWidth:F

.field private final topPadding:F

.field private transformation:Landroid/animation/ObjectAnimator;

.field private transformationProperty:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Lcom/balysv/materialmenu/MaterialMenuDrawable;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private transformationRunning:Z

.field private transformationValue:F

.field private final width:I


# direct methods
.method private constructor <init>(ILcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;JJIIFFFF)V
    .locals 3
    .param p1, "color"    # I
    .param p2, "stroke"    # Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;
    .param p3, "transformDuration"    # J
    .param p5, "pressedDuration"    # J
    .param p7, "width"    # I
    .param p8, "height"    # I
    .param p9, "iconWidth"    # F
    .param p10, "circleRadius"    # F
    .param p11, "strokeWidth"    # F
    .param p12, "dip1"    # F

    .prologue
    .line 224
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 164
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->lock:Ljava/lang/Object;

    .line 166
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->gridPaint:Landroid/graphics/Paint;

    .line 167
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->iconPaint:Landroid/graphics/Paint;

    .line 168
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->circlePaint:Landroid/graphics/Paint;

    .line 170
    const/4 v0, 0x0

    iput v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationValue:F

    .line 171
    const/4 v0, 0x0

    iput v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->pressedProgressValue:F

    .line 172
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationRunning:Z

    .line 174
    sget-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->BURGER:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    iput-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->currentIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    .line 175
    sget-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->BURGER_ARROW:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    iput-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    .line 707
    new-instance v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$1;

    const-class v1, Ljava/lang/Float;

    const-string v2, "transformation"

    invoke-direct {v0, p0, v1, v2}, Lcom/balysv/materialmenu/MaterialMenuDrawable$1;-><init>(Lcom/balysv/materialmenu/MaterialMenuDrawable;Ljava/lang/Class;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationProperty:Landroid/util/Property;

    .line 720
    new-instance v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$2;

    const-class v1, Ljava/lang/Float;

    const-string v2, "pressedProgress"

    invoke-direct {v0, p0, v1, v2}, Lcom/balysv/materialmenu/MaterialMenuDrawable$2;-><init>(Lcom/balysv/materialmenu/MaterialMenuDrawable;Ljava/lang/Class;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->pressedProgressProperty:Landroid/util/Property;

    .line 225
    iput p12, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip1:F

    .line 226
    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr v0, p12

    iput v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip2:F

    .line 227
    const/high16 v0, 0x40400000    # 3.0f

    mul-float/2addr v0, p12

    iput v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    .line 228
    const/high16 v0, 0x40800000    # 4.0f

    mul-float/2addr v0, p12

    iput v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip4:F

    .line 229
    const/high16 v0, 0x40c00000    # 6.0f

    mul-float/2addr v0, p12

    iput v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip6:F

    .line 230
    const/high16 v0, 0x41000000    # 8.0f

    mul-float/2addr v0, p12

    iput v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip8:F

    .line 231
    const/high16 v0, 0x40000000    # 2.0f

    div-float v0, p12, v0

    iput v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->diph:F

    .line 232
    iput-object p2, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->stroke:Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    .line 233
    iput p7, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    .line 234
    iput p8, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    .line 235
    iput p9, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->iconWidth:F

    .line 236
    iput p10, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->circleRadius:F

    .line 237
    iput p11, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->strokeWidth:F

    .line 238
    int-to-float v0, p7

    sub-float/2addr v0, p9

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->sidePadding:F

    .line 239
    int-to-float v0, p8

    const/high16 v1, 0x40a00000    # 5.0f

    iget v2, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->topPadding:F

    .line 241
    invoke-direct {p0, p1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->initPaint(I)V

    .line 242
    long-to-int v0, p3

    long-to-int v1, p5

    invoke-direct {p0, v0, v1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->initAnimations(II)V

    .line 244
    new-instance v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;-><init>(Lcom/balysv/materialmenu/MaterialMenuDrawable;Lcom/balysv/materialmenu/MaterialMenuDrawable$1;)V

    iput-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->materialMenuState:Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;

    .line 245
    return-void
.end method

.method synthetic constructor <init>(ILcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;JJIIFFFFLcom/balysv/materialmenu/MaterialMenuDrawable$1;)V
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;
    .param p3, "x2"    # J
    .param p5, "x3"    # J
    .param p7, "x4"    # I
    .param p8, "x5"    # I
    .param p9, "x6"    # F
    .param p10, "x7"    # F
    .param p11, "x8"    # F
    .param p12, "x9"    # F
    .param p13, "x10"    # Lcom/balysv/materialmenu/MaterialMenuDrawable$1;

    .prologue
    .line 39
    invoke-direct/range {p0 .. p12}, Lcom/balysv/materialmenu/MaterialMenuDrawable;-><init>(ILcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;JJIIFFFF)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;III)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "color"    # I
    .param p3, "stroke"    # Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;
    .param p4, "scale"    # I
    .param p5, "transformDuration"    # I
    .param p6, "pressedDuration"    # I

    .prologue
    const/high16 v5, 0x42200000    # 40.0f

    const/4 v2, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    .line 195
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 164
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->lock:Ljava/lang/Object;

    .line 166
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->gridPaint:Landroid/graphics/Paint;

    .line 167
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->iconPaint:Landroid/graphics/Paint;

    .line 168
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->circlePaint:Landroid/graphics/Paint;

    .line 170
    iput v2, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationValue:F

    .line 171
    iput v2, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->pressedProgressValue:F

    .line 172
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationRunning:Z

    .line 174
    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->BURGER:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    iput-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->currentIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    .line 175
    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->BURGER_ARROW:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    iput-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    .line 707
    new-instance v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$1;

    const-class v2, Ljava/lang/Float;

    const-string v3, "transformation"

    invoke-direct {v1, p0, v2, v3}, Lcom/balysv/materialmenu/MaterialMenuDrawable$1;-><init>(Lcom/balysv/materialmenu/MaterialMenuDrawable;Ljava/lang/Class;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationProperty:Landroid/util/Property;

    .line 720
    new-instance v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$2;

    const-class v2, Ljava/lang/Float;

    const-string v3, "pressedProgress"

    invoke-direct {v1, p0, v2, v3}, Lcom/balysv/materialmenu/MaterialMenuDrawable$2;-><init>(Lcom/balysv/materialmenu/MaterialMenuDrawable;Ljava/lang/Class;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->pressedProgressProperty:Landroid/util/Property;

    .line 196
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 198
    .local v0, "resources":Landroid/content/res/Resources;
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dpToPx(Landroid/content/res/Resources;F)F

    move-result v1

    int-to-float v2, p4

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip1:F

    .line 199
    invoke-static {v0, v4}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dpToPx(Landroid/content/res/Resources;F)F

    move-result v1

    int-to-float v2, p4

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip2:F

    .line 200
    const/high16 v1, 0x40400000    # 3.0f

    invoke-static {v0, v1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dpToPx(Landroid/content/res/Resources;F)F

    move-result v1

    int-to-float v2, p4

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    .line 201
    const/high16 v1, 0x40800000    # 4.0f

    invoke-static {v0, v1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dpToPx(Landroid/content/res/Resources;F)F

    move-result v1

    int-to-float v2, p4

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip4:F

    .line 202
    const/high16 v1, 0x40c00000    # 6.0f

    invoke-static {v0, v1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dpToPx(Landroid/content/res/Resources;F)F

    move-result v1

    int-to-float v2, p4

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip6:F

    .line 203
    const/high16 v1, 0x41000000    # 8.0f

    invoke-static {v0, v1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dpToPx(Landroid/content/res/Resources;F)F

    move-result v1

    int-to-float v2, p4

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip8:F

    .line 204
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip1:F

    div-float/2addr v1, v4

    iput v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->diph:F

    .line 206
    iput-object p3, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->stroke:Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    .line 207
    invoke-static {v0, v5}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dpToPx(Landroid/content/res/Resources;F)F

    move-result v1

    int-to-float v2, p4

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    .line 208
    invoke-static {v0, v5}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dpToPx(Landroid/content/res/Resources;F)F

    move-result v1

    int-to-float v2, p4

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    .line 209
    const/high16 v1, 0x41a00000    # 20.0f

    invoke-static {v0, v1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dpToPx(Landroid/content/res/Resources;F)F

    move-result v1

    int-to-float v2, p4

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->iconWidth:F

    .line 210
    const/high16 v1, 0x41900000    # 18.0f

    invoke-static {v0, v1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dpToPx(Landroid/content/res/Resources;F)F

    move-result v1

    int-to-float v2, p4

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->circleRadius:F

    .line 211
    # getter for: Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;->strokeWidth:I
    invoke-static {p3}, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;->access$000(Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dpToPx(Landroid/content/res/Resources;F)F

    move-result v1

    int-to-float v2, p4

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->strokeWidth:F

    .line 213
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    int-to-float v1, v1

    iget v2, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->iconWidth:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v4

    iput v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->sidePadding:F

    .line 214
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    int-to-float v1, v1

    const/high16 v2, 0x40a00000    # 5.0f

    iget v3, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    div-float/2addr v1, v4

    iput v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->topPadding:F

    .line 216
    invoke-direct {p0, p2}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->initPaint(I)V

    .line 217
    invoke-direct {p0, p5, p6}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->initAnimations(II)V

    .line 219
    new-instance v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;-><init>(Lcom/balysv/materialmenu/MaterialMenuDrawable;Lcom/balysv/materialmenu/MaterialMenuDrawable$1;)V

    iput-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->materialMenuState:Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;

    .line 220
    return-void
.end method

.method static synthetic access$1000(Lcom/balysv/materialmenu/MaterialMenuDrawable;)I
    .locals 1
    .param p0, "x0"    # Lcom/balysv/materialmenu/MaterialMenuDrawable;

    .prologue
    .line 39
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    return v0
.end method

.method static synthetic access$1100(Lcom/balysv/materialmenu/MaterialMenuDrawable;)I
    .locals 1
    .param p0, "x0"    # Lcom/balysv/materialmenu/MaterialMenuDrawable;

    .prologue
    .line 39
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    return v0
.end method

.method static synthetic access$1200(Lcom/balysv/materialmenu/MaterialMenuDrawable;)F
    .locals 1
    .param p0, "x0"    # Lcom/balysv/materialmenu/MaterialMenuDrawable;

    .prologue
    .line 39
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->iconWidth:F

    return v0
.end method

.method static synthetic access$1300(Lcom/balysv/materialmenu/MaterialMenuDrawable;)F
    .locals 1
    .param p0, "x0"    # Lcom/balysv/materialmenu/MaterialMenuDrawable;

    .prologue
    .line 39
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->circleRadius:F

    return v0
.end method

.method static synthetic access$1400(Lcom/balysv/materialmenu/MaterialMenuDrawable;)F
    .locals 1
    .param p0, "x0"    # Lcom/balysv/materialmenu/MaterialMenuDrawable;

    .prologue
    .line 39
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->strokeWidth:F

    return v0
.end method

.method static synthetic access$1500(Lcom/balysv/materialmenu/MaterialMenuDrawable;)F
    .locals 1
    .param p0, "x0"    # Lcom/balysv/materialmenu/MaterialMenuDrawable;

    .prologue
    .line 39
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip1:F

    return v0
.end method

.method static synthetic access$1700(Lcom/balysv/materialmenu/MaterialMenuDrawable;)Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;
    .locals 1
    .param p0, "x0"    # Lcom/balysv/materialmenu/MaterialMenuDrawable;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->currentIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/balysv/materialmenu/MaterialMenuDrawable;)Z
    .locals 1
    .param p0, "x0"    # Lcom/balysv/materialmenu/MaterialMenuDrawable;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->rtlEnabled:Z

    return v0
.end method

.method static synthetic access$202(Lcom/balysv/materialmenu/MaterialMenuDrawable;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/balysv/materialmenu/MaterialMenuDrawable;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationRunning:Z

    return p1
.end method

.method static synthetic access$300(Lcom/balysv/materialmenu/MaterialMenuDrawable;)Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;
    .locals 1
    .param p0, "x0"    # Lcom/balysv/materialmenu/MaterialMenuDrawable;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animatingIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    return-object v0
.end method

.method static synthetic access$402(Lcom/balysv/materialmenu/MaterialMenuDrawable;F)F
    .locals 0
    .param p0, "x0"    # Lcom/balysv/materialmenu/MaterialMenuDrawable;
    .param p1, "x1"    # F

    .prologue
    .line 39
    iput p1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->pressedProgressValue:F

    return p1
.end method

.method static synthetic access$600(Lcom/balysv/materialmenu/MaterialMenuDrawable;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/balysv/materialmenu/MaterialMenuDrawable;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->circlePaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$700(Lcom/balysv/materialmenu/MaterialMenuDrawable;)Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;
    .locals 1
    .param p0, "x0"    # Lcom/balysv/materialmenu/MaterialMenuDrawable;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->stroke:Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    return-object v0
.end method

.method static synthetic access$800(Lcom/balysv/materialmenu/MaterialMenuDrawable;)Landroid/animation/ObjectAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/balysv/materialmenu/MaterialMenuDrawable;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformation:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method static synthetic access$900(Lcom/balysv/materialmenu/MaterialMenuDrawable;)Landroid/animation/ObjectAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/balysv/materialmenu/MaterialMenuDrawable;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->pressedCircle:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method static dpToPx(Landroid/content/res/Resources;F)F
    .locals 2
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "dp"    # F

    .prologue
    .line 905
    const/4 v0, 0x1

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v0, p1, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method

.method private drawBottomLine(Landroid/graphics/Canvas;F)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "ratio"    # F

    .prologue
    .line 464
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 465
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 467
    const/4 v10, 0x0

    .local v10, "rotation":F
    const/4 v6, 0x0

    .local v6, "pivotX":F
    const/4 v8, 0x0

    .line 468
    .local v8, "pivotY":F
    const/4 v11, 0x0

    .line 470
    .local v11, "rotation2":F
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v5, v12

    add-float v7, v0, v5

    .line 471
    .local v7, "pivotX2":F
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    int-to-float v0, v0

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->topPadding:F

    sub-float/2addr v0, v5

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip2:F

    sub-float v9, v0, v5

    .line 473
    .local v9, "pivotY2":F
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->sidePadding:F

    .line 474
    .local v1, "startX":F
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    int-to-float v0, v0

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->topPadding:F

    sub-float/2addr v0, v5

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip2:F

    sub-float v2, v0, v5

    .line 475
    .local v2, "startY":F
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    int-to-float v0, v0

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->sidePadding:F

    sub-float v3, v0, v5

    .line 476
    .local v3, "stopX":F
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    int-to-float v0, v0

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->topPadding:F

    sub-float/2addr v0, v5

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip2:F

    sub-float v4, v0, v5

    .line 478
    .local v4, "stopY":F
    sget-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$5;->$SwitchMap$com$balysv$materialmenu$MaterialMenuDrawable$AnimationState:[I

    iget-object v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    invoke-virtual {v5}, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->ordinal()I

    move-result v5

    aget v0, v0, v5

    packed-switch v0, :pswitch_data_0

    .line 565
    :goto_0
    invoke-virtual {p1, v10, v6, v8}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 566
    invoke-virtual {p1, v11, v7, v9}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 567
    iget-object v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->iconPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 568
    return-void

    .line 480
    :pswitch_0
    invoke-direct {p0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->isMorphingForward()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 482
    const/high16 v0, 0x43070000    # 135.0f

    mul-float v10, v0, p2

    .line 488
    :goto_1
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v6, v0

    .line 489
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v8, v0

    .line 492
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    int-to-float v0, v0

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->sidePadding:F

    sub-float/2addr v0, v5

    invoke-direct {p0, p2}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->resolveStrokeModifier(F)F

    move-result v5

    sub-float v3, v0, v5

    .line 493
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->sidePadding:F

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    mul-float/2addr v5, p2

    add-float v1, v0, v5

    .line 494
    goto :goto_0

    .line 485
    :cond_0
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v5, p2

    const/high16 v12, 0x43610000    # 225.0f

    mul-float/2addr v5, v12

    add-float v10, v0, v5

    goto :goto_1

    .line 496
    :pswitch_1
    invoke-direct {p0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->isMorphingForward()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 498
    const/high16 v0, -0x3d4c0000    # -90.0f

    mul-float v11, v0, p2

    .line 504
    :goto_2
    const/high16 v0, -0x3dd00000    # -44.0f

    mul-float v10, v0, p2

    .line 507
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->sidePadding:F

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip4:F

    add-float v6, v0, v5

    .line 508
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    int-to-float v0, v0

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->topPadding:F

    sub-float/2addr v0, v5

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    sub-float v8, v0, v5

    .line 511
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    mul-float/2addr v0, p2

    add-float/2addr v3, v0

    .line 512
    goto :goto_0

    .line 501
    :cond_1
    const/high16 v0, 0x42b40000    # 90.0f

    mul-float v11, v0, p2

    goto :goto_2

    .line 515
    :pswitch_2
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v5, 0x43350000    # 181.0f

    mul-float/2addr v5, p2

    add-float v10, v0, v5

    .line 516
    const/high16 v0, -0x3d4c0000    # -90.0f

    mul-float v11, v0, p2

    .line 519
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->sidePadding:F

    iget v12, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip4:F

    add-float/2addr v5, v12

    iget v12, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    div-int/lit8 v12, v12, 0x2

    int-to-float v12, v12

    sub-float/2addr v5, v12

    mul-float/2addr v5, p2

    add-float v6, v0, v5

    .line 520
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    iget v12, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->topPadding:F

    sub-float/2addr v5, v12

    iget v12, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    sub-float/2addr v5, v12

    mul-float/2addr v5, p2

    add-float v8, v0, v5

    .line 523
    invoke-direct {p0, p2}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->resolveStrokeModifier(F)F

    move-result v0

    sub-float/2addr v3, v0

    .line 524
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v5, p2

    mul-float/2addr v0, v5

    add-float/2addr v1, v0

    .line 525
    goto/16 :goto_0

    .line 528
    :pswitch_3
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v5, -0x3d4c0000    # -90.0f

    mul-float/2addr v5, p2

    add-float v10, v0, v5

    .line 531
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    mul-float/2addr v5, p2

    sub-float v6, v0, v5

    .line 532
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    mul-float/2addr v5, p2

    sub-float v8, v0, v5

    .line 535
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->resolveStrokeModifier(F)F

    move-result v0

    sub-float/2addr v3, v0

    .line 536
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    add-float/2addr v1, v0

    .line 537
    goto/16 :goto_0

    .line 540
    :pswitch_4
    const/high16 v0, 0x42340000    # 45.0f

    mul-float v10, p2, v0

    .line 543
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    mul-float/2addr v5, p2

    sub-float v6, v0, v5

    .line 544
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    mul-float/2addr v5, p2

    sub-float v8, v0, v5

    .line 547
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    mul-float/2addr v0, p2

    add-float/2addr v1, v0

    .line 548
    invoke-direct {p0, p2}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->resolveStrokeModifier(F)F

    move-result v0

    sub-float/2addr v3, v0

    .line 549
    goto/16 :goto_0

    .line 552
    :pswitch_5
    const/high16 v0, -0x3d4c0000    # -90.0f

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v5, p2

    mul-float v11, v0, v5

    .line 553
    const/high16 v0, -0x3dd00000    # -44.0f

    const/high16 v5, 0x42b20000    # 89.0f

    mul-float/2addr v5, p2

    add-float v10, v0, v5

    .line 556
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->sidePadding:F

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip4:F

    add-float/2addr v0, v5

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    iget v12, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->sidePadding:F

    sub-float/2addr v5, v12

    iget v12, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    sub-float/2addr v5, v12

    iget v12, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip4:F

    sub-float/2addr v5, v12

    mul-float/2addr v5, p2

    add-float v6, v0, v5

    .line 557
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    int-to-float v0, v0

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->topPadding:F

    sub-float/2addr v0, v5

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    sub-float/2addr v0, v5

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->topPadding:F

    iget v12, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    div-int/lit8 v12, v12, 0x2

    int-to-float v12, v12

    add-float/2addr v5, v12

    iget v12, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    int-to-float v12, v12

    sub-float/2addr v5, v12

    mul-float/2addr v5, p2

    add-float v8, v0, v5

    .line 560
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    const/high16 v12, 0x3f800000    # 1.0f

    sub-float/2addr v12, p2

    mul-float/2addr v5, v12

    sub-float/2addr v0, v5

    add-float/2addr v1, v0

    .line 561
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p2

    invoke-direct {p0, v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->resolveStrokeModifier(F)F

    move-result v0

    sub-float/2addr v3, v0

    goto/16 :goto_0

    .line 478
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private drawMiddleLine(Landroid/graphics/Canvas;F)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "ratio"    # F

    .prologue
    .line 295
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 296
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 298
    const/4 v9, 0x0

    .line 299
    .local v9, "rotation":F
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v7, v0

    .line 300
    .local v7, "pivotX":F
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v8, v0

    .line 301
    .local v8, "pivotY":F
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->sidePadding:F

    .line 302
    .local v1, "startX":F
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->topPadding:F

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v5, v10

    const/high16 v10, 0x40a00000    # 5.0f

    mul-float/2addr v5, v10

    add-float v2, v0, v5

    .line 303
    .local v2, "startY":F
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    int-to-float v0, v0

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->sidePadding:F

    sub-float v3, v0, v5

    .line 304
    .local v3, "stopX":F
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->topPadding:F

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v5, v10

    const/high16 v10, 0x40a00000    # 5.0f

    mul-float/2addr v5, v10

    add-float v4, v0, v5

    .line 305
    .local v4, "stopY":F
    const/16 v6, 0xff

    .line 307
    .local v6, "alpha":I
    sget-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$5;->$SwitchMap$com$balysv$materialmenu$MaterialMenuDrawable$AnimationState:[I

    iget-object v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    invoke-virtual {v5}, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->ordinal()I

    move-result v5

    aget v0, v0, v5

    packed-switch v0, :pswitch_data_0

    .line 363
    :goto_0
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->iconPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 364
    invoke-virtual {p1, v9, v7, v8}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 365
    iget-object v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->iconPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 366
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->iconPaint:Landroid/graphics/Paint;

    const/16 v5, 0xff

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 367
    return-void

    .line 310
    :pswitch_0
    invoke-direct {p0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->isMorphingForward()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 311
    const/high16 v0, 0x43340000    # 180.0f

    mul-float v9, p2, v0

    .line 316
    :goto_1
    invoke-direct {p0, p2}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->resolveStrokeModifier(F)F

    move-result v0

    mul-float/2addr v0, p2

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v0, v5

    sub-float/2addr v3, v0

    .line 317
    goto :goto_0

    .line 313
    :cond_0
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v5, p2

    const/high16 v10, 0x43340000    # 180.0f

    mul-float/2addr v5, v10

    add-float v9, v0, v5

    goto :goto_1

    .line 320
    :pswitch_1
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p2

    const/high16 v5, 0x437f0000    # 255.0f

    mul-float/2addr v0, v5

    float-to-int v6, v0

    .line 321
    goto :goto_0

    .line 324
    :pswitch_2
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p2

    const/high16 v5, 0x437f0000    # 255.0f

    mul-float/2addr v0, v5

    float-to-int v6, v0

    .line 325
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p2

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip2:F

    mul-float/2addr v0, v5

    add-float/2addr v1, v0

    .line 326
    goto :goto_0

    .line 328
    :pswitch_3
    invoke-direct {p0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->isMorphingForward()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 330
    const/high16 v0, 0x43070000    # 135.0f

    mul-float v9, p2, v0

    .line 332
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v0, v5

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip4:F

    mul-float/2addr v5, p2

    add-float/2addr v0, v5

    add-float/2addr v1, v0

    .line 333
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip8:F

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->diph:F

    add-float/2addr v0, v5

    mul-float/2addr v0, p2

    add-float/2addr v3, v0

    .line 341
    :goto_2
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    const/high16 v10, 0x40000000    # 2.0f

    mul-float/2addr v5, v10

    add-float v7, v0, v5

    .line 342
    goto :goto_0

    .line 336
    :cond_1
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v5, 0x43070000    # 135.0f

    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v10, p2

    mul-float/2addr v5, v10

    sub-float v9, v0, v5

    .line 338
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v0, v5

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip4:F

    add-float/2addr v0, v5

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v5, p2

    iget v10, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip2:F

    iget v11, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->diph:F

    add-float/2addr v10, v11

    mul-float/2addr v5, v10

    sub-float/2addr v0, v5

    add-float/2addr v1, v0

    .line 339
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip8:F

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v5, p2

    iget v10, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip2:F

    iget v11, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip6:F

    add-float/2addr v10, v11

    mul-float/2addr v5, v10

    sub-float/2addr v0, v5

    add-float/2addr v3, v0

    goto :goto_2

    .line 345
    :pswitch_4
    const/high16 v0, 0x43070000    # 135.0f

    mul-float v9, p2, v0

    .line 347
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip4:F

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v5, v10

    add-float/2addr v0, v5

    mul-float/2addr v0, p2

    add-float/2addr v1, v0

    .line 348
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip8:F

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->diph:F

    add-float/2addr v0, v5

    mul-float/2addr v0, p2

    add-float/2addr v3, v0

    .line 349
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    const/high16 v10, 0x40000000    # 2.0f

    mul-float/2addr v5, v10

    add-float v7, v0, v5

    .line 350
    goto/16 :goto_0

    .line 353
    :pswitch_5
    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr v0, p2

    float-to-int v6, v0

    .line 355
    const/high16 v0, 0x43070000    # 135.0f

    mul-float v9, p2, v0

    .line 357
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip4:F

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v5, v10

    add-float/2addr v0, v5

    mul-float/2addr v0, p2

    add-float/2addr v1, v0

    .line 358
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip8:F

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->diph:F

    add-float/2addr v0, v5

    mul-float/2addr v0, p2

    add-float/2addr v3, v0

    .line 359
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v5, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    const/high16 v10, 0x40000000    # 2.0f

    mul-float/2addr v5, v10

    add-float v7, v0, v5

    goto/16 :goto_0

    .line 307
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private drawTopLine(Landroid/graphics/Canvas;F)V
    .locals 15
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "ratio"    # F

    .prologue
    .line 370
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 372
    const/4 v12, 0x0

    .local v12, "rotation":F
    const/4 v8, 0x0

    .local v8, "pivotX":F
    const/4 v10, 0x0

    .line 373
    .local v10, "pivotY":F
    const/4 v13, 0x0

    .line 375
    .local v13, "rotation2":F
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v6, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v6, v14

    add-float v9, v1, v6

    .line 376
    .local v9, "pivotX2":F
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->topPadding:F

    iget v6, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip2:F

    add-float v11, v1, v6

    .line 378
    .local v11, "pivotY2":F
    iget v2, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->sidePadding:F

    .line 379
    .local v2, "startX":F
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->topPadding:F

    iget v6, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip2:F

    add-float v3, v1, v6

    .line 380
    .local v3, "startY":F
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    int-to-float v1, v1

    iget v6, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->sidePadding:F

    sub-float v4, v1, v6

    .line 381
    .local v4, "stopX":F
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->topPadding:F

    iget v6, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip2:F

    add-float v5, v1, v6

    .line 382
    .local v5, "stopY":F
    const/16 v7, 0xff

    .line 384
    .local v7, "alpha":I
    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$5;->$SwitchMap$com$balysv$materialmenu$MaterialMenuDrawable$AnimationState:[I

    iget-object v6, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    invoke-virtual {v6}, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->ordinal()I

    move-result v6

    aget v1, v1, v6

    packed-switch v1, :pswitch_data_0

    .line 456
    :goto_0
    iget-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->iconPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 457
    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v8, v10}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 458
    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v9, v11}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 459
    iget-object v6, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->iconPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 460
    iget-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->iconPaint:Landroid/graphics/Paint;

    const/16 v6, 0xff

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 461
    return-void

    .line 386
    :pswitch_0
    invoke-direct {p0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->isMorphingForward()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 388
    const/high16 v1, 0x43610000    # 225.0f

    mul-float v12, p2, v1

    .line 394
    :goto_1
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v8, v1

    .line 395
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v10, v1

    .line 398
    move/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->resolveStrokeModifier(F)F

    move-result v1

    sub-float/2addr v4, v1

    .line 399
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    mul-float v1, v1, p2

    add-float/2addr v2, v1

    .line 401
    goto :goto_0

    .line 391
    :cond_0
    const/high16 v1, 0x43610000    # 225.0f

    const/high16 v6, 0x3f800000    # 1.0f

    sub-float v6, v6, p2

    const/high16 v14, 0x43070000    # 135.0f

    mul-float/2addr v6, v14

    add-float v12, v1, v6

    goto :goto_1

    .line 404
    :pswitch_1
    const/high16 v1, 0x42300000    # 44.0f

    mul-float v12, v1, p2

    .line 405
    const/high16 v1, 0x42b40000    # 90.0f

    mul-float v13, v1, p2

    .line 408
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->sidePadding:F

    iget v6, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip4:F

    add-float v8, v1, v6

    .line 409
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->topPadding:F

    iget v6, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    add-float v10, v1, v6

    .line 412
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    mul-float v1, v1, p2

    add-float/2addr v4, v1

    .line 413
    goto :goto_0

    .line 416
    :pswitch_2
    const/high16 v1, 0x43610000    # 225.0f

    const/high16 v6, -0x3ccb0000    # -181.0f

    mul-float v6, v6, p2

    add-float v12, v1, v6

    .line 417
    const/high16 v1, 0x42b40000    # 90.0f

    mul-float v13, v1, p2

    .line 420
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v6, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->sidePadding:F

    iget v14, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip4:F

    add-float/2addr v6, v14

    iget v14, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    div-int/lit8 v14, v14, 0x2

    int-to-float v14, v14

    sub-float/2addr v6, v14

    mul-float v6, v6, p2

    add-float v8, v1, v6

    .line 421
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v6, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->topPadding:F

    iget v14, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    add-float/2addr v6, v14

    iget v14, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    div-int/lit8 v14, v14, 0x2

    int-to-float v14, v14

    sub-float/2addr v6, v14

    mul-float v6, v6, p2

    add-float v10, v1, v6

    .line 424
    move/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->resolveStrokeModifier(F)F

    move-result v1

    sub-float/2addr v4, v1

    .line 425
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    const/high16 v6, 0x3f800000    # 1.0f

    sub-float v6, v6, p2

    mul-float/2addr v1, v6

    add-float/2addr v2, v1

    .line 426
    goto/16 :goto_0

    .line 429
    :pswitch_3
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v1, v1, p2

    const/high16 v6, 0x437f0000    # 255.0f

    mul-float/2addr v1, v6

    float-to-int v7, v1

    .line 431
    const/high16 v12, 0x43610000    # 225.0f

    .line 432
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v8, v1

    .line 433
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v10, v1

    .line 436
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, v1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->resolveStrokeModifier(F)F

    move-result v1

    sub-float/2addr v4, v1

    .line 437
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    add-float/2addr v2, v1

    .line 438
    goto/16 :goto_0

    .line 441
    :pswitch_4
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v1, v1, p2

    const/high16 v6, 0x437f0000    # 255.0f

    mul-float/2addr v1, v6

    float-to-int v7, v1

    .line 442
    goto/16 :goto_0

    .line 445
    :pswitch_5
    const/high16 v12, 0x42300000    # 44.0f

    .line 446
    const/high16 v13, 0x42b40000    # 90.0f

    .line 447
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->sidePadding:F

    iget v6, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip4:F

    add-float v8, v1, v6

    .line 448
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->topPadding:F

    iget v6, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    add-float v10, v1, v6

    .line 449
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    add-float/2addr v4, v1

    .line 452
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v1, v1, p2

    const/high16 v6, 0x437f0000    # 255.0f

    mul-float/2addr v1, v6

    float-to-int v7, v1

    goto/16 :goto_0

    .line 384
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private drawTouchCircle(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 290
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 291
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->pressedProgressValue:F

    iget-object v3, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->circlePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 292
    return-void
.end method

.method private initAnimations(II)V
    .locals 4
    .param p1, "transformDuration"    # I
    .param p2, "pressedDuration"    # I

    .prologue
    .line 753
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationProperty:Landroid/util/Property;

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformation:Landroid/animation/ObjectAnimator;

    .line 754
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformation:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 755
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformation:Landroid/animation/ObjectAnimator;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 756
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformation:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$3;

    invoke-direct {v1, p0}, Lcom/balysv/materialmenu/MaterialMenuDrawable$3;-><init>(Lcom/balysv/materialmenu/MaterialMenuDrawable;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 764
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->pressedProgressProperty:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->pressedCircle:Landroid/animation/ObjectAnimator;

    .line 765
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->pressedCircle:Landroid/animation/ObjectAnimator;

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 766
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->pressedCircle:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 767
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->pressedCircle:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$4;

    invoke-direct {v1, p0}, Lcom/balysv/materialmenu/MaterialMenuDrawable$4;-><init>(Lcom/balysv/materialmenu/MaterialMenuDrawable;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 776
    return-void

    .line 764
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method private initPaint(I)V
    .locals 4
    .param p1, "color"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 248
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->gridPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 249
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->gridPaint:Landroid/graphics/Paint;

    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 250
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->gridPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 252
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->iconPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 253
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->iconPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 254
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->iconPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->strokeWidth:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 255
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->iconPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 257
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->circlePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 258
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->circlePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 259
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->circlePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 260
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->circlePaint:Landroid/graphics/Paint;

    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 262
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    invoke-virtual {p0, v2, v2, v0, v1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->setBounds(IIII)V

    .line 263
    return-void
.end method

.method private isMorphingForward()Z
    .locals 2

    .prologue
    .line 571
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationValue:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private resolveStrokeModifier(F)F
    .locals 3
    .param p1, "ratio"    # F

    .prologue
    .line 575
    sget-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$5;->$SwitchMap$com$balysv$materialmenu$MaterialMenuDrawable$Stroke:[I

    iget-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->stroke:Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    invoke-virtual {v1}, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 592
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 577
    :pswitch_0
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->ARROW_X:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->X_CHECK:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    if-ne v0, v1, :cond_1

    .line 578
    :cond_0
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip6:F

    mul-float/2addr v1, p1

    sub-float/2addr v0, v1

    goto :goto_0

    .line 580
    :cond_1
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    mul-float/2addr v0, p1

    goto :goto_0

    .line 582
    :pswitch_1
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->ARROW_X:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->X_CHECK:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    if-ne v0, v1, :cond_3

    .line 583
    :cond_2
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->diph:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip6:F

    iget v2, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->diph:F

    add-float/2addr v1, v2

    mul-float/2addr v1, p1

    sub-float/2addr v0, v1

    goto :goto_0

    .line 585
    :cond_3
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip3:F

    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->diph:F

    add-float/2addr v0, v1

    mul-float/2addr v0, p1

    goto :goto_0

    .line 587
    :pswitch_2
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->ARROW_X:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    sget-object v1, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->X_CHECK:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    if-ne v0, v1, :cond_5

    .line 588
    :cond_4
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip4:F

    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip6:F

    iget v2, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip1:F

    add-float/2addr v1, v2

    mul-float/2addr v1, p1

    sub-float/2addr v0, v1

    goto :goto_0

    .line 590
    :cond_5
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->dip4:F

    mul-float/2addr v0, p1

    goto :goto_0

    .line 575
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private resolveTransformation()Z
    .locals 14

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 779
    iget-object v10, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->currentIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    sget-object v11, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->BURGER:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    if-ne v10, v11, :cond_2

    move v5, v8

    .line 780
    .local v5, "isCurrentBurger":Z
    :goto_0
    iget-object v10, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->currentIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    sget-object v11, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->ARROW:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    if-ne v10, v11, :cond_3

    move v4, v8

    .line 781
    .local v4, "isCurrentArrow":Z
    :goto_1
    iget-object v10, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->currentIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    sget-object v11, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->X:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    if-ne v10, v11, :cond_4

    move v7, v8

    .line 782
    .local v7, "isCurrentX":Z
    :goto_2
    iget-object v10, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->currentIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    sget-object v11, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->CHECK:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    if-ne v10, v11, :cond_5

    move v6, v8

    .line 783
    .local v6, "isCurrentCheck":Z
    :goto_3
    iget-object v10, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animatingIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    sget-object v11, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->BURGER:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    if-ne v10, v11, :cond_6

    move v1, v8

    .line 784
    .local v1, "isAnimatingBurger":Z
    :goto_4
    iget-object v10, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animatingIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    sget-object v11, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->ARROW:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    if-ne v10, v11, :cond_7

    move v0, v8

    .line 785
    .local v0, "isAnimatingArrow":Z
    :goto_5
    iget-object v10, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animatingIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    sget-object v11, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->X:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    if-ne v10, v11, :cond_8

    move v3, v8

    .line 786
    .local v3, "isAnimatingX":Z
    :goto_6
    iget-object v10, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animatingIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    sget-object v11, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->CHECK:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    if-ne v10, v11, :cond_9

    move v2, v8

    .line 788
    .local v2, "isAnimatingCheck":Z
    :goto_7
    if-eqz v5, :cond_0

    if-nez v0, :cond_1

    :cond_0
    if-eqz v4, :cond_a

    if-eqz v1, :cond_a

    .line 789
    :cond_1
    sget-object v8, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->BURGER_ARROW:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    iput-object v8, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    .line 815
    .end local v5    # "isCurrentBurger":Z
    :goto_8
    return v5

    .end local v0    # "isAnimatingArrow":Z
    .end local v1    # "isAnimatingBurger":Z
    .end local v2    # "isAnimatingCheck":Z
    .end local v3    # "isAnimatingX":Z
    .end local v4    # "isCurrentArrow":Z
    .end local v6    # "isCurrentCheck":Z
    .end local v7    # "isCurrentX":Z
    :cond_2
    move v5, v9

    .line 779
    goto :goto_0

    .restart local v5    # "isCurrentBurger":Z
    :cond_3
    move v4, v9

    .line 780
    goto :goto_1

    .restart local v4    # "isCurrentArrow":Z
    :cond_4
    move v7, v9

    .line 781
    goto :goto_2

    .restart local v7    # "isCurrentX":Z
    :cond_5
    move v6, v9

    .line 782
    goto :goto_3

    .restart local v6    # "isCurrentCheck":Z
    :cond_6
    move v1, v9

    .line 783
    goto :goto_4

    .restart local v1    # "isAnimatingBurger":Z
    :cond_7
    move v0, v9

    .line 784
    goto :goto_5

    .restart local v0    # "isAnimatingArrow":Z
    :cond_8
    move v3, v9

    .line 785
    goto :goto_6

    .restart local v3    # "isAnimatingX":Z
    :cond_9
    move v2, v9

    .line 786
    goto :goto_7

    .line 793
    .restart local v2    # "isAnimatingCheck":Z
    :cond_a
    if-eqz v4, :cond_b

    if-nez v3, :cond_c

    :cond_b
    if-eqz v7, :cond_d

    if-eqz v0, :cond_d

    .line 794
    :cond_c
    sget-object v8, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->ARROW_X:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    iput-object v8, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    move v5, v4

    .line 795
    goto :goto_8

    .line 798
    :cond_d
    if-eqz v5, :cond_e

    if-nez v3, :cond_f

    :cond_e
    if-eqz v7, :cond_10

    if-eqz v1, :cond_10

    .line 799
    :cond_f
    sget-object v8, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->BURGER_X:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    iput-object v8, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    goto :goto_8

    .line 803
    :cond_10
    if-eqz v4, :cond_11

    if-nez v2, :cond_12

    :cond_11
    if-eqz v6, :cond_13

    if-eqz v0, :cond_13

    .line 804
    :cond_12
    sget-object v8, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->ARROW_CHECK:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    iput-object v8, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    move v5, v4

    .line 805
    goto :goto_8

    .line 808
    :cond_13
    if-eqz v5, :cond_14

    if-nez v2, :cond_15

    :cond_14
    if-eqz v6, :cond_16

    if-eqz v1, :cond_16

    .line 809
    :cond_15
    sget-object v8, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->BURGER_CHECK:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    iput-object v8, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    goto :goto_8

    .line 813
    :cond_16
    if-eqz v7, :cond_17

    if-nez v2, :cond_18

    :cond_17
    if-eqz v6, :cond_19

    if-eqz v3, :cond_19

    .line 814
    :cond_18
    sget-object v8, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->X_CHECK:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    iput-object v8, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    move v5, v7

    .line 815
    goto :goto_8

    .line 818
    :cond_19
    new-instance v10, Ljava/lang/IllegalStateException;

    const-string v11, "Animating from %s to %s is not supported"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    iget-object v13, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->currentIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    aput-object v13, v12, v9

    iget-object v9, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animatingIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    aput-object v9, v12, v8

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v10, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v10
.end method


# virtual methods
.method public animateIconState(Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;Z)V
    .locals 2
    .param p1, "state"    # Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;
    .param p2, "drawTouch"    # Z

    .prologue
    .line 665
    iget-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 666
    :try_start_0
    iget-boolean v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationRunning:Z

    if-eqz v0, :cond_0

    .line 667
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->end()V

    .line 668
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->pressedCircle:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->end()V

    .line 670
    :cond_0
    iput-boolean p2, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->drawTouchCircle:Z

    .line 671
    iput-object p1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animatingIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    .line 672
    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->start()V

    .line 673
    monitor-exit v1

    .line 674
    return-void

    .line 673
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 270
    iget v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationValue:F

    cmpg-float v1, v1, v4

    if-gtz v1, :cond_3

    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationValue:F

    .line 272
    .local v0, "ratio":F
    :goto_0
    iget-boolean v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->rtlEnabled:Z

    if-eqz v1, :cond_0

    .line 273
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 274
    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {p1, v1, v4, v3, v3}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 275
    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->getIntrinsicWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v1, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 278
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->drawTopLine(Landroid/graphics/Canvas;F)V

    .line 279
    invoke-direct {p0, p1, v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->drawMiddleLine(Landroid/graphics/Canvas;F)V

    .line 280
    invoke-direct {p0, p1, v0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->drawBottomLine(Landroid/graphics/Canvas;F)V

    .line 282
    iget-boolean v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->rtlEnabled:Z

    if-eqz v1, :cond_1

    .line 283
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 286
    :cond_1
    iget-boolean v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->drawTouchCircle:Z

    if-eqz v1, :cond_2

    invoke-direct {p0, p1}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->drawTouchCircle(Landroid/graphics/Canvas;)V

    .line 287
    :cond_2
    return-void

    .line 270
    .end local v0    # "ratio":F
    :cond_3
    const/high16 v1, 0x40000000    # 2.0f

    iget v2, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationValue:F

    sub-float v0, v1, v2

    goto :goto_0
.end method

.method public getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 2

    .prologue
    .line 871
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->materialMenuState:Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;

    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->getChangingConfigurations()I

    move-result v1

    # setter for: Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;->changingConfigurations:I
    invoke-static {v0, v1}, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;->access$502(Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;I)I

    .line 872
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->materialMenuState:Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;

    return-object v0
.end method

.method public getIconState()Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;
    .locals 1

    .prologue
    .line 701
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->currentIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    return-object v0
.end method

.method public getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 865
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->height:I

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 860
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->width:I

    return v0
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 604
    const/4 v0, -0x2

    return v0
.end method

.method public getPressedProgress()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 743
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->pressedProgressValue:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTransformationValue()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 734
    iget v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationValue:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 855
    iget-boolean v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationRunning:Z

    return v0
.end method

.method public mutate()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 877
    new-instance v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;-><init>(Lcom/balysv/materialmenu/MaterialMenuDrawable;Lcom/balysv/materialmenu/MaterialMenuDrawable$1;)V

    iput-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->materialMenuState:Lcom/balysv/materialmenu/MaterialMenuDrawable$MaterialMenuState;

    .line 878
    return-object p0
.end method

.method public setAlpha(I)V
    .locals 1
    .param p1, "alpha"    # I

    .prologue
    .line 596
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->iconPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 597
    return-void
.end method

.method public setColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 612
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->iconPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 613
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->circlePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 614
    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->invalidateSelf()V

    .line 615
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1
    .param p1, "cf"    # Landroid/graphics/ColorFilter;

    .prologue
    .line 600
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->iconPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 601
    return-void
.end method

.method public setIconState(Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;)V
    .locals 3
    .param p1, "iconState"    # Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    .prologue
    .line 634
    iget-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 635
    :try_start_0
    iget-boolean v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationRunning:Z

    if-eqz v0, :cond_0

    .line 636
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 637
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationRunning:Z

    .line 640
    :cond_0
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->currentIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    if-ne v0, p1, :cond_1

    monitor-exit v1

    .line 662
    :goto_0
    return-void

    .line 642
    :cond_1
    sget-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$5;->$SwitchMap$com$balysv$materialmenu$MaterialMenuDrawable$IconState:[I

    invoke-virtual {p1}, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 659
    :goto_1
    iput-object p1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->currentIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    .line 660
    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->invalidateSelf()V

    .line 661
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 644
    :pswitch_0
    :try_start_1
    sget-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->BURGER_ARROW:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    iput-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    .line 645
    const/4 v0, 0x0

    iput v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationValue:F

    goto :goto_1

    .line 648
    :pswitch_1
    sget-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->BURGER_ARROW:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    iput-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    .line 649
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationValue:F

    goto :goto_1

    .line 652
    :pswitch_2
    sget-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->BURGER_X:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    iput-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    .line 653
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationValue:F

    goto :goto_1

    .line 656
    :pswitch_3
    sget-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;->BURGER_CHECK:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    iput-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animationState:Lcom/balysv/materialmenu/MaterialMenuDrawable$AnimationState;

    .line 657
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationValue:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 642
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setInterpolator(Landroid/view/animation/Interpolator;)V
    .locals 1
    .param p1, "interpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    .line 626
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 627
    return-void
.end method

.method public setNeverDrawTouch(Z)V
    .locals 0
    .param p1, "neverDrawTouch"    # Z

    .prologue
    .line 630
    iput-boolean p1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->neverDrawTouch:Z

    .line 631
    return-void
.end method

.method public setPressedDuration(I)V
    .locals 4
    .param p1, "duration"    # I

    .prologue
    .line 622
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->pressedCircle:Landroid/animation/ObjectAnimator;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 623
    return-void
.end method

.method public setPressedProgress(Ljava/lang/Float;)V
    .locals 6
    .param p1, "value"    # Ljava/lang/Float;

    .prologue
    .line 747
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->pressedProgressValue:F

    .line 748
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->circlePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x43480000    # 200.0f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v3

    iget v4, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->circleRadius:F

    const v5, 0x3f9c28f6    # 1.22f

    mul-float/2addr v4, v5

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 749
    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->invalidateSelf()V

    .line 750
    return-void
.end method

.method public setRTLEnabled(Z)V
    .locals 0
    .param p1, "rtlEnabled"    # Z

    .prologue
    .line 696
    iput-boolean p1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->rtlEnabled:Z

    .line 697
    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->invalidateSelf()V

    .line 698
    return-void
.end method

.method public setTransformationDuration(I)V
    .locals 4
    .param p1, "duration"    # I

    .prologue
    .line 618
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformation:Landroid/animation/ObjectAnimator;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 619
    return-void
.end method

.method public setTransformationValue(Ljava/lang/Float;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/Float;

    .prologue
    .line 738
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationValue:F

    .line 739
    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->invalidateSelf()V

    .line 740
    return-void
.end method

.method public start()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v6, 0x1

    .line 824
    iget-boolean v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationRunning:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animatingIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->animatingIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    iget-object v4, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->currentIconState:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    if-ne v1, v4, :cond_1

    .line 843
    :cond_0
    :goto_0
    return-void

    .line 825
    :cond_1
    iput-boolean v6, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationRunning:Z

    .line 827
    invoke-direct {p0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->resolveTransformation()Z

    move-result v0

    .line 828
    .local v0, "direction":Z
    iget-object v4, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformation:Landroid/animation/ObjectAnimator;

    new-array v5, v8, [F

    if-eqz v0, :cond_4

    move v1, v2

    :goto_1
    aput v1, v5, v7

    if-eqz v0, :cond_5

    :goto_2
    aput v3, v5, v6

    invoke-virtual {v4, v5}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 832
    iget-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 834
    iget-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->pressedCircle:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 835
    iget-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->pressedCircle:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 837
    :cond_2
    iget-boolean v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->drawTouchCircle:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->neverDrawTouch:Z

    if-nez v1, :cond_3

    .line 838
    iget-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->pressedCircle:Landroid/animation/ObjectAnimator;

    new-array v3, v8, [F

    aput v2, v3, v7

    iget v2, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->circleRadius:F

    const v4, 0x3f9c28f6    # 1.22f

    mul-float/2addr v2, v4

    aput v2, v3, v6

    invoke-virtual {v1, v3}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 839
    iget-object v1, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->pressedCircle:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 842
    :cond_3
    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->invalidateSelf()V

    goto :goto_0

    :cond_4
    move v1, v3

    .line 828
    goto :goto_1

    :cond_5
    const/high16 v3, 0x40000000    # 2.0f

    goto :goto_2
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 846
    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 847
    iget-object v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->end()V

    .line 852
    :goto_0
    return-void

    .line 849
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/balysv/materialmenu/MaterialMenuDrawable;->transformationRunning:Z

    .line 850
    invoke-virtual {p0}, Lcom/balysv/materialmenu/MaterialMenuDrawable;->invalidateSelf()V

    goto :goto_0
.end method
