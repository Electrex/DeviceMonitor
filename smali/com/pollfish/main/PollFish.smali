.class public Lcom/pollfish/main/PollFish;
.super Ljava/lang/Object;


# static fields
.field protected static TAG:Ljava/lang/String;

.field protected static overlayLayout:Lcom/pollfish/d/a;

.field protected static pollfishActivity:Landroid/app/Activity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "PollFish"

    sput-object v0, Lcom/pollfish/main/PollFish;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lcom/pollfish/main/PollFish;->overlayLayout:Lcom/pollfish/d/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static Initialise(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;IZLcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Ljava/lang/String;ZILandroid/view/ViewGroup;Ljava/lang/String;)V
    .locals 19

    const-class v1, Lcom/pollfish/f/a;

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    sput-boolean v1, Lcom/pollfish/f/a;->a:Z

    :cond_0
    sput-object p0, Lcom/pollfish/main/PollFish;->pollfishActivity:Landroid/app/Activity;

    const/4 v1, 0x0

    sput-object v1, Lcom/pollfish/main/PollFish;->overlayLayout:Lcom/pollfish/d/a;

    sget-object v1, Lcom/pollfish/main/PollFish;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Postback server-to-server param - request_uuid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p15

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-ltz p13, :cond_3

    const/4 v4, 0x1

    sget-object v1, Lcom/pollfish/main/PollFish;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Testing for survey with id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p13

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and isDebuggable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    if-eqz v4, :cond_1

    sget-object v1, Lcom/pollfish/main/PollFish;->TAG:Ljava/lang/String;

    const-string v2, "Pollfish runs in developer mode"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    if-eqz p4, :cond_2

    sget-object v1, Lcom/pollfish/main/PollFish;->TAG:Ljava/lang/String;

    const-string v2, "Pollfish runs in custom mode"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/pollfish/f/c;->a(Landroid/app/Activity;)Z

    move-result v1

    sget-object v2, Lcom/pollfish/main/PollFish;->TAG:Ljava/lang/String;

    const-string v5, "You are using Pollfish SDK for Google Play Store"

    invoke-static {v2, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p11, :cond_6

    move-object/from16 v15, p11

    :goto_1
    sget-object v2, Lcom/pollfish/main/PollFish;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "server To Connect Url: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_7

    sget-object v1, Lcom/pollfish/main/PollFish;->TAG:Ljava/lang/String;

    const-string v2, "All permissions in place"

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    new-instance v1, Lcom/pollfish/f/a;

    new-instance v8, Lcom/pollfish/main/PollFish$3;

    invoke-direct {v8}, Lcom/pollfish/main/PollFish$3;-><init>()V

    move-object/from16 v2, p0

    move-object/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    move-object/from16 v13, p9

    move-object/from16 v14, p10

    move/from16 v16, p13

    move-object/from16 v17, p14

    move-object/from16 v18, p15

    invoke-direct/range {v1 .. v18}, Lcom/pollfish/f/a;-><init>(Landroid/app/Activity;Ljava/lang/String;ZLcom/pollfish/constants/Position;IZLcom/pollfish/interfaces/a$f;Lcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Ljava/lang/String;ILandroid/view/ViewGroup;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/pollfish/f/a;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    return-void

    :cond_3
    if-nez p12, :cond_5

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_3
    move v4, v1

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_3

    :cond_5
    const/4 v4, 0x0

    goto :goto_0

    :cond_6
    const-string v15, "https://wss.pollfish.com"

    goto :goto_1

    :catch_0
    move-exception v1

    sget-object v2, Lcom/pollfish/main/PollFish;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " Error while trying to begin LifeCycle: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_7
    sget-object v1, Lcom/pollfish/main/PollFish;->TAG:Ljava/lang/String;

    const-string v2, "Pollfish requires the following permission: INTERNET. Please place it in your manifest file"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public static customInit(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;I)V
    .locals 16

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, -0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    invoke-static/range {v0 .. v15}, Lcom/pollfish/main/PollFish;->Initialise(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;IZLcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Ljava/lang/String;ZILandroid/view/ViewGroup;Ljava/lang/String;)V

    return-void
.end method

.method public static customInit(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;ILandroid/view/ViewGroup;)V
    .locals 16

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, -0x1

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v14, p4

    invoke-static/range {v0 .. v15}, Lcom/pollfish/main/PollFish;->Initialise(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;IZLcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Ljava/lang/String;ZILandroid/view/ViewGroup;Ljava/lang/String;)V

    return-void
.end method

.method public static customInit(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;ILandroid/view/ViewGroup;Ljava/lang/String;)V
    .locals 16

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v14, p4

    move-object/from16 v15, p5

    invoke-static/range {v0 .. v15}, Lcom/pollfish/main/PollFish;->Initialise(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;IZLcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Ljava/lang/String;ZILandroid/view/ViewGroup;Ljava/lang/String;)V

    return-void
.end method

.method public static customInit(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;ILcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;)V
    .locals 16

    const/4 v4, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, -0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-static/range {v0 .. v15}, Lcom/pollfish/main/PollFish;->Initialise(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;IZLcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Ljava/lang/String;ZILandroid/view/ViewGroup;Ljava/lang/String;)V

    return-void
.end method

.method public static customInit(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;ILcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Landroid/view/ViewGroup;)V
    .locals 16

    const/4 v4, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, -0x1

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v14, p10

    invoke-static/range {v0 .. v15}, Lcom/pollfish/main/PollFish;->Initialise(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;IZLcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Ljava/lang/String;ZILandroid/view/ViewGroup;Ljava/lang/String;)V

    return-void
.end method

.method public static customInit(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;ILcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Landroid/view/ViewGroup;Ljava/lang/String;)V
    .locals 16

    const/4 v4, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v14, p10

    move-object/from16 v15, p11

    invoke-static/range {v0 .. v15}, Lcom/pollfish/main/PollFish;->Initialise(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;IZLcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Ljava/lang/String;ZILandroid/view/ViewGroup;Ljava/lang/String;)V

    return-void
.end method

.method public static hide()V
    .locals 3

    sget-object v0, Lcom/pollfish/main/PollFish;->TAG:Ljava/lang/String;

    const-string v1, "manually called hide()"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/pollfish/main/PollFish;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "overlayLayout: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/pollfish/main/PollFish;->overlayLayout:Lcom/pollfish/d/a;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-class v0, Lcom/pollfish/f/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    sput-boolean v0, Lcom/pollfish/f/a;->a:Z

    :cond_0
    sget-object v0, Lcom/pollfish/main/PollFish;->overlayLayout:Lcom/pollfish/d/a;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/pollfish/main/PollFish;->pollfishActivity:Landroid/app/Activity;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/pollfish/main/PollFish;->pollfishActivity:Landroid/app/Activity;

    new-instance v1, Lcom/pollfish/main/PollFish$2;

    invoke-direct {v1}, Lcom/pollfish/main/PollFish$2;-><init>()V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-object v0, Lcom/pollfish/main/PollFish;->TAG:Ljava/lang/String;

    const-string v1, "pollfishActivity==null"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static init(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;I)V
    .locals 16

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, -0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    invoke-static/range {v0 .. v15}, Lcom/pollfish/main/PollFish;->Initialise(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;IZLcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Ljava/lang/String;ZILandroid/view/ViewGroup;Ljava/lang/String;)V

    return-void
.end method

.method public static init(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;ILandroid/view/ViewGroup;)V
    .locals 16

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, -0x1

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v14, p4

    invoke-static/range {v0 .. v15}, Lcom/pollfish/main/PollFish;->Initialise(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;IZLcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Ljava/lang/String;ZILandroid/view/ViewGroup;Ljava/lang/String;)V

    return-void
.end method

.method public static init(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;ILandroid/view/ViewGroup;Ljava/lang/String;)V
    .locals 16

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v14, p4

    move-object/from16 v15, p5

    invoke-static/range {v0 .. v15}, Lcom/pollfish/main/PollFish;->Initialise(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;IZLcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Ljava/lang/String;ZILandroid/view/ViewGroup;Ljava/lang/String;)V

    return-void
.end method

.method public static init(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;ILcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;)V
    .locals 16

    const/4 v4, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, -0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-static/range {v0 .. v15}, Lcom/pollfish/main/PollFish;->Initialise(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;IZLcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Ljava/lang/String;ZILandroid/view/ViewGroup;Ljava/lang/String;)V

    return-void
.end method

.method public static init(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;ILcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Landroid/view/ViewGroup;)V
    .locals 16

    const/4 v4, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, -0x1

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v14, p10

    invoke-static/range {v0 .. v15}, Lcom/pollfish/main/PollFish;->Initialise(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;IZLcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Ljava/lang/String;ZILandroid/view/ViewGroup;Ljava/lang/String;)V

    return-void
.end method

.method public static init(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;ILcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Landroid/view/ViewGroup;Ljava/lang/String;)V
    .locals 16

    const/4 v4, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v14, p10

    move-object/from16 v15, p11

    invoke-static/range {v0 .. v15}, Lcom/pollfish/main/PollFish;->Initialise(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;IZLcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Ljava/lang/String;ZILandroid/view/ViewGroup;Ljava/lang/String;)V

    return-void
.end method

.method private static init(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;ILjava/lang/String;ZILandroid/view/ViewGroup;Ljava/lang/String;)V
    .locals 16

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v11, p4

    move/from16 v12, p5

    move/from16 v13, p6

    move-object/from16 v14, p7

    move-object/from16 v15, p8

    invoke-static/range {v0 .. v15}, Lcom/pollfish/main/PollFish;->Initialise(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;IZLcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Ljava/lang/String;ZILandroid/view/ViewGroup;Ljava/lang/String;)V

    return-void
.end method

.method public static isPollfishPanelOpen()Z
    .locals 2

    sget-object v0, Lcom/pollfish/main/PollFish;->overlayLayout:Lcom/pollfish/d/a;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/pollfish/main/PollFish;->overlayLayout:Lcom/pollfish/d/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/pollfish/d/a;->c(Z)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setAttributesMap(Ljava/util/Map;)V
    .locals 3

    if-eqz p0, :cond_1

    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_1

    sget-object v0, Lcom/pollfish/main/PollFish;->overlayLayout:Lcom/pollfish/d/a;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/pollfish/main/PollFish;->overlayLayout:Lcom/pollfish/d/a;

    invoke-virtual {v0, p0}, Lcom/pollfish/d/a;->a(Ljava/util/Map;)V

    sget-object v0, Lcom/pollfish/main/PollFish;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAttributesMap - attrMap:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/pollfish/main/PollFish;->TAG:Ljava/lang/String;

    const-string v1, "setAttributesMap - PollFish.overlayLayout == null"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/pollfish/main/PollFish;->TAG:Ljava/lang/String;

    const-string v1, "Attributes Map cannot be null or empty"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static show()V
    .locals 2

    sget-object v0, Lcom/pollfish/main/PollFish;->TAG:Ljava/lang/String;

    const-string v1, "manually called show()"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-class v0, Lcom/pollfish/f/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    sput-boolean v0, Lcom/pollfish/f/a;->a:Z

    :cond_0
    sget-object v0, Lcom/pollfish/main/PollFish;->overlayLayout:Lcom/pollfish/d/a;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/pollfish/main/PollFish;->pollfishActivity:Landroid/app/Activity;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/pollfish/main/PollFish;->pollfishActivity:Landroid/app/Activity;

    new-instance v1, Lcom/pollfish/main/PollFish$1;

    invoke-direct {v1}, Lcom/pollfish/main/PollFish$1;-><init>()V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-object v0, Lcom/pollfish/main/PollFish;->TAG:Ljava/lang/String;

    const-string v1, "pollfishActivity==null"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
