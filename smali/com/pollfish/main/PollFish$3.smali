.class final Lcom/pollfish/main/PollFish$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/pollfish/interfaces/a$f;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pollfish/main/PollFish;->Initialise(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;IZLcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Ljava/lang/String;ZILandroid/view/ViewGroup;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/pollfish/d/a;)V
    .locals 3

    sget-object v0, Lcom/pollfish/main/PollFish;->TAG:Ljava/lang/String;

    const-string v1, "OnSurveyRenderedListenerListener - use generated overlayLayout"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    sput-object p1, Lcom/pollfish/main/PollFish;->overlayLayout:Lcom/pollfish/d/a;

    sget-object v0, Lcom/pollfish/main/PollFish;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "shouldHide: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/pollfish/f/a;->a:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/pollfish/main/PollFish;->overlayLayout:Lcom/pollfish/d/a;

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/pollfish/f/a;->a:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/pollfish/main/PollFish;->overlayLayout:Lcom/pollfish/d/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/pollfish/d/a;->b(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/pollfish/main/PollFish;->overlayLayout:Lcom/pollfish/d/a;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/pollfish/f/a;->a:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/pollfish/main/PollFish;->overlayLayout:Lcom/pollfish/d/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/pollfish/d/a;->b(Z)V

    goto :goto_0
.end method
