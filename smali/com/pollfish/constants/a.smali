.class public final enum Lcom/pollfish/constants/a;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/pollfish/constants/a;

.field public static final enum b:Lcom/pollfish/constants/a;

.field public static final enum c:Lcom/pollfish/constants/a;

.field private static final synthetic d:[Lcom/pollfish/constants/a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/pollfish/constants/a;

    const-string v1, "NOTHING_SHOWN"

    invoke-direct {v0, v1, v2}, Lcom/pollfish/constants/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pollfish/constants/a;->a:Lcom/pollfish/constants/a;

    new-instance v0, Lcom/pollfish/constants/a;

    const-string v1, "INDICATOR_SHOWN"

    invoke-direct {v0, v1, v3}, Lcom/pollfish/constants/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pollfish/constants/a;->b:Lcom/pollfish/constants/a;

    new-instance v0, Lcom/pollfish/constants/a;

    const-string v1, "PANEL_OPEN"

    invoke-direct {v0, v1, v4}, Lcom/pollfish/constants/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pollfish/constants/a;->c:Lcom/pollfish/constants/a;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/pollfish/constants/a;

    sget-object v1, Lcom/pollfish/constants/a;->a:Lcom/pollfish/constants/a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/pollfish/constants/a;->b:Lcom/pollfish/constants/a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/pollfish/constants/a;->c:Lcom/pollfish/constants/a;

    aput-object v1, v0, v4

    sput-object v0, Lcom/pollfish/constants/a;->d:[Lcom/pollfish/constants/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method
