.class public Lcom/pollfish/a/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pollfish/a/a$a;
    }
.end annotation


# static fields
.field private static t:I

.field private static u:Ljava/lang/String;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:I

.field private f:Z

.field private g:I

.field private h:I

.field private i:Z

.field private j:Landroid/graphics/Bitmap;

.field private k:Landroid/graphics/Bitmap;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Z

.field private r:Z

.field private s:Lcom/pollfish/constants/Position;

.field private v:Lcom/pollfish/interfaces/a$d;

.field private w:Lcom/pollfish/interfaces/a$b;

.field private x:Landroid/app/Activity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/pollfish/a/a;->t:I

    const-string v0, ""

    sput-object v0, Lcom/pollfish/a/a;->u:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;Ljava/lang/String;IILcom/pollfish/constants/Position;Lcom/pollfish/interfaces/a$d;Ljava/lang/String;Ljava/lang/String;Lcom/pollfish/interfaces/a$b;Landroid/app/Activity;Ljava/lang/String;ZZI)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/pollfish/a/a;->j:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/pollfish/a/a;->k:Landroid/graphics/Bitmap;

    const-string v1, ""

    iput-object v1, p0, Lcom/pollfish/a/a;->l:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, p0, Lcom/pollfish/a/a;->m:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, p0, Lcom/pollfish/a/a;->n:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, p0, Lcom/pollfish/a/a;->o:Ljava/lang/String;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/pollfish/a/a;->v:Lcom/pollfish/interfaces/a$d;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/pollfish/a/a;->w:Lcom/pollfish/interfaces/a$b;

    iput-object p1, p0, Lcom/pollfish/a/a;->c:Ljava/lang/String;

    iput p3, p0, Lcom/pollfish/a/a;->d:I

    iput-object p2, p0, Lcom/pollfish/a/a;->a:Ljava/lang/String;

    iput-object p4, p0, Lcom/pollfish/a/a;->b:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/pollfish/a/a;->i:Z

    iput-object p6, p0, Lcom/pollfish/a/a;->l:Ljava/lang/String;

    iput-object p7, p0, Lcom/pollfish/a/a;->m:Ljava/lang/String;

    iput p8, p0, Lcom/pollfish/a/a;->g:I

    iput p9, p0, Lcom/pollfish/a/a;->h:I

    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/pollfish/a/a;->n:Ljava/lang/String;

    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/pollfish/a/a;->o:Ljava/lang/String;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/pollfish/a/a;->x:Landroid/app/Activity;

    move/from16 v0, p17

    iput-boolean v0, p0, Lcom/pollfish/a/a;->f:Z

    move/from16 v0, p18

    iput-boolean v0, p0, Lcom/pollfish/a/a;->r:Z

    move/from16 v0, p19

    iput v0, p0, Lcom/pollfish/a/a;->e:I

    const-string v1, "PanelObj"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PanelObj width_percentage: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "height_percentage: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "PanelObj"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PanelObj hasAcceptedTerms: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/pollfish/a/a;->f:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    sput v1, Lcom/pollfish/a/a;->t:I

    iput-object p10, p0, Lcom/pollfish/a/a;->s:Lcom/pollfish/constants/Position;

    iput-object p11, p0, Lcom/pollfish/a/a;->v:Lcom/pollfish/interfaces/a$d;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/pollfish/a/a;->w:Lcom/pollfish/interfaces/a$b;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/pollfish/a/a;->q:Z

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/pollfish/a/a;->p:Ljava/lang/String;

    if-eqz p5, :cond_1

    invoke-virtual {p0}, Lcom/pollfish/a/a;->g()V

    :cond_0
    :goto_0
    const-string v1, "PanelObj"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PanelObj with s_id:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and response_type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/pollfish/a/a;->a()V

    return-void

    :cond_1
    const-string v1, "PanelObj"

    const-string v2, "no images to download move to survey on overlay"

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p11, :cond_0

    iget-object v1, p0, Lcom/pollfish/a/a;->v:Lcom/pollfish/interfaces/a$d;

    invoke-interface {v1, p0}, Lcom/pollfish/interfaces/a$d;->a(Lcom/pollfish/a/a;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/pollfish/a/a;)Lcom/pollfish/constants/Position;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/a/a;->s:Lcom/pollfish/constants/Position;

    return-object v0
.end method

.method static synthetic o()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/pollfish/a/a;->u:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic p()I
    .locals 2

    sget v0, Lcom/pollfish/a/a;->t:I

    add-int/lit8 v1, v0, -0x1

    sput v1, Lcom/pollfish/a/a;->t:I

    return v0
.end method

.method static synthetic q()I
    .locals 1

    sget v0, Lcom/pollfish/a/a;->t:I

    return v0
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcom/pollfish/constants/Position;[I)Landroid/graphics/drawable/Drawable;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "PanelObj"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getIndicatorImage pos: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/pollfish/a/a;->i:Z

    if-nez v0, :cond_8

    const/4 v0, 0x0

    sget-object v1, Lcom/pollfish/constants/Position;->BOTTOM_LEFT:Lcom/pollfish/constants/Position;

    if-eq p2, v1, :cond_0

    sget-object v1, Lcom/pollfish/constants/Position;->TOP_LEFT:Lcom/pollfish/constants/Position;

    if-eq p2, v1, :cond_0

    sget-object v1, Lcom/pollfish/constants/Position;->MIDDLE_LEFT:Lcom/pollfish/constants/Position;

    if-ne p2, v1, :cond_4

    :cond_0
    const-string v1, "PanelObj"

    const-string v2, "(pos == Position.BOTTOM_LEFT || pos == Position.TOP_LEFT|| pos == Position.MIDDLE_LEFT) "

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pollfish/a/a;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/pollfish/a/a;->a:Ljava/lang/String;

    const-string v1, "award"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p1}, Lcom/pollfish/e/a;->a(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    aput v1, p3, v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    aput v1, p3, v4

    :cond_2
    invoke-static {p1, v0}, Lcom/pollfish/e/a;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_3
    invoke-static {p1}, Lcom/pollfish/e/a;->c(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/pollfish/a/a;->a:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v0, p0, Lcom/pollfish/a/a;->a:Ljava/lang/String;

    const-string v1, "award"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-static {p1}, Lcom/pollfish/e/a;->b(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_5
    :goto_2
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    aput v1, p3, v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    aput v1, p3, v4

    :cond_6
    invoke-static {p1, v0}, Lcom/pollfish/e/a;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    :cond_7
    invoke-static {p1}, Lcom/pollfish/e/a;->d(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_2

    :cond_8
    sget-object v0, Lcom/pollfish/constants/Position;->BOTTOM_LEFT:Lcom/pollfish/constants/Position;

    if-eq p2, v0, :cond_9

    sget-object v0, Lcom/pollfish/constants/Position;->TOP_LEFT:Lcom/pollfish/constants/Position;

    if-eq p2, v0, :cond_9

    sget-object v0, Lcom/pollfish/constants/Position;->MIDDLE_LEFT:Lcom/pollfish/constants/Position;

    if-ne p2, v0, :cond_a

    :cond_9
    iget-object v0, p0, Lcom/pollfish/a/a;->j:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    aput v1, p3, v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    aput v0, p3, v4

    iget-object v0, p0, Lcom/pollfish/a/a;->j:Landroid/graphics/Bitmap;

    invoke-static {p1, v0}, Lcom/pollfish/e/a;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    :cond_a
    iget-object v0, p0, Lcom/pollfish/a/a;->k:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    aput v1, p3, v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    aput v0, p3, v4

    iget-object v0, p0, Lcom/pollfish/a/a;->k:Landroid/graphics/Bitmap;

    invoke-static {p1, v0}, Lcom/pollfish/e/a;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1
.end method

.method public a()V
    .locals 9

    const/4 v7, 0x0

    const-string v0, "PanelObj"

    const-string v1, "downloadAssets()"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/a/a;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pollfish/a/a;->o:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/pollfish/a/a;->w:Lcom/pollfish/interfaces/a$b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pollfish/a/a;->w:Lcom/pollfish/interfaces/a$b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pollfish/a/a;->w:Lcom/pollfish/interfaces/a$b;

    invoke-interface {v0}, Lcom/pollfish/interfaces/a$b;->c()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    :try_start_0
    new-instance v8, Lorg/json/JSONArray;

    iget-object v0, p0, Lcom/pollfish/a/a;->o:Ljava/lang/String;

    invoke-direct {v8, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    if-eqz v8, :cond_3

    const-string v0, "PanelObj"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "downloadAssets: num:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    if-eqz v8, :cond_4

    iget-object v0, p0, Lcom/pollfish/a/a;->w:Lcom/pollfish/interfaces/a$b;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/pollfish/a/a;->w:Lcom/pollfish/interfaces/a$b;

    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/pollfish/interfaces/a$b;->b(I)V

    :cond_4
    move v6, v7

    :goto_1
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v6, v0, :cond_9

    invoke-virtual {v8, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "cache_path"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "url_path"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v2, "file_type"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    const-string v0, "./"

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "./"

    const-string v2, "/"

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    :cond_5
    const-string v0, "PanelObj"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "downloadAssets: attributeCachePath:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "PanelObj"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "downloadAssets: attributeType:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "PanelObj"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "downloadAssets: attributeUrlPath:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/pollfish/a/c;

    const-string v2, ""

    const-string v3, ""

    invoke-direct/range {v0 .. v5}, Lcom/pollfish/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    if-eqz v0, :cond_7

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/pollfish/a/a;->x:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "pollfish_cache"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/pollfish/a/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_8

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "PanelObj"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Creating directory: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    iget-object v1, p0, Lcom/pollfish/a/a;->w:Lcom/pollfish/interfaces/a$b;

    invoke-interface {v1, v0}, Lcom/pollfish/interfaces/a$b;->a(Lcom/pollfish/a/c;)V

    :cond_7
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_1

    :cond_8
    const-string v0, "PanelObj"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " already exists in cache!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/a/a;->w:Lcom/pollfish/interfaces/a$b;

    invoke-interface {v0}, Lcom/pollfish/interfaces/a$b;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v1, "PanelObj"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "downloadAssets error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/a/a;->w:Lcom/pollfish/interfaces/a$b;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/pollfish/a/a;->w:Lcom/pollfish/interfaces/a$b;

    invoke-interface {v0, v7}, Lcom/pollfish/interfaces/a$b;->b(I)V

    :cond_9
    iget-object v0, p0, Lcom/pollfish/a/a;->w:Lcom/pollfish/interfaces/a$b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pollfish/a/a;->w:Lcom/pollfish/interfaces/a$b;

    invoke-interface {v0}, Lcom/pollfish/interfaces/a$b;->c()V

    goto/16 :goto_0
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 0

    iput-object p1, p0, Lcom/pollfish/a/a;->j:Landroid/graphics/Bitmap;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pollfish/a/a;->q:Z

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/a/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public b(Landroid/graphics/Bitmap;)V
    .locals 0

    iput-object p1, p0, Lcom/pollfish/a/a;->k:Landroid/graphics/Bitmap;

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/a/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/a/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lcom/pollfish/a/a;->d:I

    return v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/pollfish/a/a;->q:Z

    return v0
.end method

.method public g()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/pollfish/a/a;->s:Lcom/pollfish/constants/Position;

    sget-object v1, Lcom/pollfish/constants/Position;->BOTTOM_LEFT:Lcom/pollfish/constants/Position;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/pollfish/a/a;->s:Lcom/pollfish/constants/Position;

    sget-object v1, Lcom/pollfish/constants/Position;->TOP_LEFT:Lcom/pollfish/constants/Position;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/pollfish/a/a;->s:Lcom/pollfish/constants/Position;

    sget-object v1, Lcom/pollfish/constants/Position;->MIDDLE_LEFT:Lcom/pollfish/constants/Position;

    if-ne v0, v1, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/pollfish/a/a;->l:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget v0, Lcom/pollfish/a/a;->t:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/pollfish/a/a;->t:I

    new-instance v0, Lcom/pollfish/a/a$a;

    iget-object v1, p0, Lcom/pollfish/a/a;->l:Ljava/lang/String;

    iget-object v2, p0, Lcom/pollfish/a/a;->v:Lcom/pollfish/interfaces/a$d;

    invoke-direct {v0, p0, v1, p0, v2}, Lcom/pollfish/a/a$a;-><init>(Lcom/pollfish/a/a;Ljava/lang/String;Lcom/pollfish/a/a;Lcom/pollfish/interfaces/a$d;)V

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/pollfish/a/a$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/pollfish/a/a;->m:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget v0, Lcom/pollfish/a/a;->t:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/pollfish/a/a;->t:I

    new-instance v0, Lcom/pollfish/a/a$a;

    iget-object v1, p0, Lcom/pollfish/a/a;->m:Ljava/lang/String;

    iget-object v2, p0, Lcom/pollfish/a/a;->v:Lcom/pollfish/interfaces/a$d;

    invoke-direct {v0, p0, v1, p0, v2}, Lcom/pollfish/a/a$a;-><init>(Lcom/pollfish/a/a;Ljava/lang/String;Lcom/pollfish/a/a;Lcom/pollfish/interfaces/a$d;)V

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/pollfish/a/a$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public h()I
    .locals 1

    iget v0, p0, Lcom/pollfish/a/a;->g:I

    return v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lcom/pollfish/a/a;->h:I

    return v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/a/a;->n:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/a/a;->p:Ljava/lang/String;

    return-object v0
.end method

.method public l()Z
    .locals 1

    iget-boolean v0, p0, Lcom/pollfish/a/a;->f:Z

    return v0
.end method

.method public m()Z
    .locals 1

    iget-boolean v0, p0, Lcom/pollfish/a/a;->r:Z

    return v0
.end method

.method public n()I
    .locals 1

    iget v0, p0, Lcom/pollfish/a/a;->e:I

    return v0
.end method
