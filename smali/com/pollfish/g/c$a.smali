.class public Lcom/pollfish/g/c$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pollfish/g/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/pollfish/g/c;


# direct methods
.method public constructor <init>(Lcom/pollfish/g/c;)V
    .locals 0

    iput-object p1, p0, Lcom/pollfish/g/c$a;->a:Lcom/pollfish/g/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clearCacheQueue()V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/pollfish/g/c$a;->a:Lcom/pollfish/g/c;

    invoke-static {v0}, Lcom/pollfish/g/c;->a(Lcom/pollfish/g/c;)Lcom/pollfish/interfaces/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/pollfish/interfaces/b;->d()V

    return-void
.end method

.method public close()V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/pollfish/g/c$a;->a:Lcom/pollfish/g/c;

    invoke-static {v0}, Lcom/pollfish/g/c;->a(Lcom/pollfish/g/c;)Lcom/pollfish/interfaces/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/pollfish/interfaces/b;->b()V

    return-void
.end method

.method public closeAndNoShow()V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/pollfish/g/c$a;->a:Lcom/pollfish/g/c;

    invoke-static {v0}, Lcom/pollfish/g/c;->a(Lcom/pollfish/g/c;)Lcom/pollfish/interfaces/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/pollfish/interfaces/b;->h()V

    return-void
.end method

.method public getDeviceInfo()Ljava/lang/String;
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/pollfish/g/c$a;->a:Lcom/pollfish/g/c;

    invoke-static {v0}, Lcom/pollfish/g/c;->a(Lcom/pollfish/g/c;)Lcom/pollfish/interfaces/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/pollfish/interfaces/b;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFromServer()Ljava/lang/String;
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const-string v0, "PollfishWebView"

    const-string v1, "JsObject getFromServer"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/g/c$a;->a:Lcom/pollfish/g/c;

    invoke-static {v0}, Lcom/pollfish/g/c;->a(Lcom/pollfish/g/c;)Lcom/pollfish/interfaces/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/pollfish/interfaces/b;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public openWebsite(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/pollfish/g/c$a;->a:Lcom/pollfish/g/c;

    invoke-static {v0}, Lcom/pollfish/g/c;->a(Lcom/pollfish/g/c;)Lcom/pollfish/interfaces/b;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pollfish/interfaces/b;->a(Ljava/lang/String;)V

    return-void
.end method

.method public sendToServer(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/pollfish/g/c$a;->a:Lcom/pollfish/g/c;

    invoke-static {v0}, Lcom/pollfish/g/c;->a(Lcom/pollfish/g/c;)Lcom/pollfish/interfaces/b;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/pollfish/interfaces/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public sentDataOfUserConsentToServer(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/pollfish/g/c$a;->a:Lcom/pollfish/g/c;

    invoke-static {v0}, Lcom/pollfish/g/c;->a(Lcom/pollfish/g/c;)Lcom/pollfish/interfaces/b;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/pollfish/interfaces/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setSurveyCompleted()V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/pollfish/g/c$a;->a:Lcom/pollfish/g/c;

    invoke-static {v0}, Lcom/pollfish/g/c;->a(Lcom/pollfish/g/c;)Lcom/pollfish/interfaces/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/pollfish/interfaces/b;->c()V

    return-void
.end method

.method public showToastMsg(Ljava/lang/String;)V
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const-string v0, "PollfishWebView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " showToastMsg : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/g/c$a;->a:Lcom/pollfish/g/c;

    invoke-static {v0}, Lcom/pollfish/g/c;->b(Lcom/pollfish/g/c;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public textFieldFocus()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/pollfish/g/c$a;->a:Lcom/pollfish/g/c;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/pollfish/g/c;->a(Lcom/pollfish/g/c;Z)Z

    return-void
.end method

.method public textFieldUnFocus()V
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/pollfish/g/c$a;->a:Lcom/pollfish/g/c;

    invoke-static {v0, v2}, Lcom/pollfish/g/c;->a(Lcom/pollfish/g/c;Z)Z

    iget-object v0, p0, Lcom/pollfish/g/c$a;->a:Lcom/pollfish/g/c;

    invoke-static {v0}, Lcom/pollfish/g/c;->b(Lcom/pollfish/g/c;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/pollfish/g/c$a;->a:Lcom/pollfish/g/c;

    invoke-virtual {v1}, Lcom/pollfish/g/c;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return-void
.end method

.method public userNotEligible()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const-string v0, "PollfishWebView"

    const-string v1, "userNotEligible()"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/g/c$a;->a:Lcom/pollfish/g/c;

    invoke-static {v0}, Lcom/pollfish/g/c;->a(Lcom/pollfish/g/c;)Lcom/pollfish/interfaces/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/pollfish/interfaces/b;->f()V

    return-void
.end method

.method public webViewLoaded()V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/pollfish/g/c$a;->a:Lcom/pollfish/g/c;

    invoke-static {v0}, Lcom/pollfish/g/c;->a(Lcom/pollfish/g/c;)Lcom/pollfish/interfaces/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/pollfish/interfaces/b;->a()V

    return-void
.end method
