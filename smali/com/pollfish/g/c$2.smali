.class Lcom/pollfish/g/c$2;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pollfish/g/c;-><init>(Landroid/content/Context;Lcom/pollfish/interfaces/b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pollfish/g/c;


# direct methods
.method constructor <init>(Lcom/pollfish/g/c;)V
    .locals 0

    iput-object p1, p0, Lcom/pollfish/g/c$2;->a:Lcom/pollfish/g/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4

    const/4 v0, 0x1

    const-string v1, "PollfishWebView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onKeyDown: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " event.getAction(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    sparse-switch p2, :sswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    iget-object v1, p0, Lcom/pollfish/g/c$2;->a:Lcom/pollfish/g/c;

    invoke-static {v1}, Lcom/pollfish/g/c;->a(Lcom/pollfish/g/c;)Lcom/pollfish/interfaces/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/pollfish/interfaces/b;->b()V

    iget-object v1, p0, Lcom/pollfish/g/c$2;->a:Lcom/pollfish/g/c;

    const-string v2, "javascript:Pollfish.mobile.interface.loseFocus(true);"

    invoke-virtual {v1, v2}, Lcom/pollfish/g/c;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lcom/pollfish/g/c$2;->a:Lcom/pollfish/g/c;

    const-string v2, "javascript:Pollfish.mobile.interface.loseFocus(true);"

    invoke-virtual {v1, v2}, Lcom/pollfish/g/c;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    const/4 v2, 0x6

    if-ne v1, v2, :cond_0

    :cond_2
    iget-object v1, p0, Lcom/pollfish/g/c$2;->a:Lcom/pollfish/g/c;

    const-string v2, "javascript:Pollfish.mobile.interface.loseFocus(true);"

    invoke-virtual {v1, v2}, Lcom/pollfish/g/c;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x42 -> :sswitch_1
    .end sparse-switch
.end method
