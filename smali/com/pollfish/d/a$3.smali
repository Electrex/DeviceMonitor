.class Lcom/pollfish/d/a$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/pollfish/interfaces/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pollfish/d/a;->e()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pollfish/d/a;


# direct methods
.method constructor <init>(Lcom/pollfish/d/a;)V
    .locals 0

    iput-object p1, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    const-string v0, "OverlayLayout"

    const-string v1, "webViewLoaded()"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/pollfish/d/a;->b(Lcom/pollfish/d/a;Z)Z

    :try_start_0
    invoke-static {}, Lcom/pollfish/d/a;->k()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/pollfish/d/a$3$1;

    invoke-direct {v0, p0}, Lcom/pollfish/d/a$3$1;-><init>(Lcom/pollfish/d/a$3;)V

    iget-object v1, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v1}, Lcom/pollfish/d/a;->d(Lcom/pollfish/d/a;)Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/pollfish/f/c;->a(Landroid/content/Context;Ljava/lang/Runnable;I)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/pollfish/d/a$3$2;

    invoke-direct {v0, p0}, Lcom/pollfish/d/a$3$2;-><init>(Lcom/pollfish/d/a$3;)V

    iget-object v1, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v1}, Lcom/pollfish/d/a;->d(Lcom/pollfish/d/a;)Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/pollfish/f/c;->a(Landroid/content/Context;Ljava/lang/Runnable;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "webViewLoaded() error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "openWebsite("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "http://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "https://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v1, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v1}, Lcom/pollfish/d/a;->d(Lcom/pollfish/d/a;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "openWebsite error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    const/4 v8, 0x0

    const/4 v5, 0x0

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sentDataOfUserConsentToServer with url:  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/pollfish/d/a;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "allowedDataBinaryStr:  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/pollfish/c/e;

    iget-object v1, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v1}, Lcom/pollfish/d/a;->c(Lcom/pollfish/d/a;)Lcom/pollfish/a/b;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/pollfish/d/a;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/pollfish/d/a;->m()I

    move-result v3

    invoke-static {}, Lcom/pollfish/d/a;->n()Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v6}, Lcom/pollfish/d/a;->h(Lcom/pollfish/d/a;)Lcom/pollfish/interfaces/a$c;

    move-result-object v6

    iget-object v7, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v7}, Lcom/pollfish/d/a;->d(Lcom/pollfish/d/a;)Landroid/app/Activity;

    move-result-object v7

    move-object v9, v5

    move-object v10, v5

    move-object v11, p2

    invoke-direct/range {v0 .. v11}, Lcom/pollfish/c/e;-><init>(Lcom/pollfish/a/b;Ljava/lang/String;ILjava/lang/String;Lorg/json/JSONObject;Lcom/pollfish/interfaces/a$c;Landroid/app/Activity;ZLjava/lang/String;Lcom/pollfish/a/a;Ljava/lang/String;)V

    new-array v1, v8, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/pollfish/c/e;->c([Ljava/lang/Object;)Lcom/pollfish/c/h;

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    const/4 v8, 0x0

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sentToServer with url:  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/pollfish/d/a;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and save at queue: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "jsonParams:  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz p3, :cond_0

    :try_start_1
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v8

    :cond_0
    :goto_0
    :try_start_2
    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tempSaveAtQueue:  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/pollfish/c/e;

    iget-object v1, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v1}, Lcom/pollfish/d/a;->c(Lcom/pollfish/d/a;)Lcom/pollfish/a/b;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/pollfish/d/a;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/pollfish/d/a;->m()I

    move-result v3

    invoke-static {}, Lcom/pollfish/d/a;->n()Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v6}, Lcom/pollfish/d/a;->h(Lcom/pollfish/d/a;)Lcom/pollfish/interfaces/a$c;

    move-result-object v6

    iget-object v7, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v7}, Lcom/pollfish/d/a;->d(Lcom/pollfish/d/a;)Landroid/app/Activity;

    move-result-object v7

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, Lcom/pollfish/c/e;-><init>(Lcom/pollfish/a/b;Ljava/lang/String;ILjava/lang/String;Lorg/json/JSONObject;Lcom/pollfish/interfaces/a$c;Landroid/app/Activity;ZLjava/lang/String;Lcom/pollfish/a/a;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/pollfish/c/e;->c([Ljava/lang/Object;)Lcom/pollfish/c/h;

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "while converting saveAtQueue:  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error while parsing sentToServer from javascript : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public b()V
    .locals 3

    const-string v0, "OverlayLayout"

    const-string v1, "closePanel()"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/pollfish/d/a$3$3;

    invoke-direct {v0, p0}, Lcom/pollfish/d/a$3$3;-><init>(Lcom/pollfish/d/a$3;)V

    iget-object v1, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v1}, Lcom/pollfish/d/a;->d(Lcom/pollfish/d/a;)Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/pollfish/f/c;->a(Landroid/content/Context;Ljava/lang/Runnable;I)V

    return-void
.end method

.method public c()V
    .locals 4

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSurveyCompleted() short survey: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v2}, Lcom/pollfish/d/a;->e(Lcom/pollfish/d/a;)Lcom/pollfish/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pollfish/a/a;->m()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " panelObj.getSurveyPrice():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v2}, Lcom/pollfish/d/a;->e(Lcom/pollfish/d/a;)Lcom/pollfish/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pollfish/a/a;->n()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->d(Lcom/pollfish/d/a;)Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->d(Lcom/pollfish/d/a;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;

    iget-object v1, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v1}, Lcom/pollfish/d/a;->e(Lcom/pollfish/d/a;)Lcom/pollfish/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pollfish/a/a;->m()Z

    move-result v1

    iget-object v2, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v2}, Lcom/pollfish/d/a;->e(Lcom/pollfish/d/a;)Lcom/pollfish/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pollfish/a/a;->n()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;->onPollfishSurveyCompleted(ZI)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->e(Lcom/pollfish/d/a;)Lcom/pollfish/a/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/pollfish/a/a;->a(Z)V

    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->f(Lcom/pollfish/d/a;)Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "OverlayLayout"

    const-string v1, "pollfishSurveyCompletedListenerExt!=null"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "short survey: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v2}, Lcom/pollfish/d/a;->e(Lcom/pollfish/d/a;)Lcom/pollfish/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pollfish/a/a;->m()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " panelObj.getSurveyPrice():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v2}, Lcom/pollfish/d/a;->e(Lcom/pollfish/d/a;)Lcom/pollfish/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pollfish/a/a;->n()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->f(Lcom/pollfish/d/a;)Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;

    move-result-object v0

    iget-object v1, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v1}, Lcom/pollfish/d/a;->e(Lcom/pollfish/d/a;)Lcom/pollfish/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pollfish/a/a;->m()Z

    move-result v1

    iget-object v2, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v2}, Lcom/pollfish/d/a;->e(Lcom/pollfish/d/a;)Lcom/pollfish/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pollfish/a/a;->n()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;->onPollfishSurveyCompleted(ZI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error while setting  project e: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public d()V
    .locals 2

    const-string v0, "OverlayLayout"

    const-string v1, "ClearQueueTask()"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/pollfish/c/d;

    iget-object v1, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v1}, Lcom/pollfish/d/a;->d(Lcom/pollfish/d/a;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pollfish/c/d;-><init>(Landroid/app/Activity;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/pollfish/c/d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 2

    const-string v0, "OverlayLayout"

    const-string v1, "getFromServer()"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->e(Lcom/pollfish/d/a;)Lcom/pollfish/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->e(Lcom/pollfish/d/a;)Lcom/pollfish/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pollfish/a/a;->j()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public f()V
    .locals 4

    const-string v0, "OverlayLayout"

    const-string v1, "userNotEligible()"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->d(Lcom/pollfish/d/a;)Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->d(Lcom/pollfish/d/a;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;

    invoke-interface {v0}, Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;->onUserNotEligible()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->e(Lcom/pollfish/d/a;)Lcom/pollfish/a/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/pollfish/a/a;->a(Z)V

    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->g(Lcom/pollfish/d/a;)Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "OverlayLayout"

    const-string v1, "pollfishUserNotEligibleListenerExt!=null"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->g(Lcom/pollfish/d/a;)Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;->onUserNotEligible()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error while userNotEligible e: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public g()Ljava/lang/String;
    .locals 5

    const/4 v1, 0x0

    const-string v0, "OverlayLayout"

    const-string v2, "getDeviceInfo()"

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->c(Lcom/pollfish/d/a;)Lcom/pollfish/a/b;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->c(Lcom/pollfish/d/a;)Lcom/pollfish/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pollfish/a/b;->C()Lcom/pollfish/constants/Position;

    move-result-object v0

    sget-object v2, Lcom/pollfish/constants/Position;->TOP_RIGHT:Lcom/pollfish/constants/Position;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->c(Lcom/pollfish/d/a;)Lcom/pollfish/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pollfish/a/b;->C()Lcom/pollfish/constants/Position;

    move-result-object v0

    sget-object v2, Lcom/pollfish/constants/Position;->BOTTOM_RIGHT:Lcom/pollfish/constants/Position;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->c(Lcom/pollfish/d/a;)Lcom/pollfish/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pollfish/a/b;->C()Lcom/pollfish/constants/Position;

    move-result-object v0

    sget-object v2, Lcom/pollfish/constants/Position;->MIDDLE_RIGHT:Lcom/pollfish/constants/Position;

    if-ne v0, v2, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v2}, Lcom/pollfish/d/a;->e(Lcom/pollfish/d/a;)Lcom/pollfish/a/a;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v1}, Lcom/pollfish/d/a;->e(Lcom/pollfish/d/a;)Lcom/pollfish/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pollfish/a/a;->l()Z

    move-result v1

    :cond_1
    const-string v2, "OverlayLayout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getDeviceInfo(): response: {\"version\": \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v4}, Lcom/pollfish/d/a;->c(Lcom/pollfish/d/a;)Lcom/pollfish/a/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pollfish/a/b;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\", \"language\": \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v4}, Lcom/pollfish/d/a;->c(Lcom/pollfish/d/a;)Lcom/pollfish/a/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pollfish/a/b;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\", \"host\": \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/pollfish/d/a;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\", \"position\" : \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\", \"hasaccepted\" : \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\"}"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "{\"version\": \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v3}, Lcom/pollfish/d/a;->c(Lcom/pollfish/d/a;)Lcom/pollfish/a/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pollfish/a/b;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\", \"language\": \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v3}, Lcom/pollfish/d/a;->c(Lcom/pollfish/d/a;)Lcom/pollfish/a/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pollfish/a/b;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\", \"host\": \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/pollfish/d/a;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\", \"position\" : \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\", \"hasaccepted\" : \""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\"}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_2
    const-string v0, ""

    goto :goto_1

    :cond_3
    move v0, v1

    goto/16 :goto_0
.end method

.method public h()V
    .locals 4

    const-string v0, "OverlayLayout"

    const-string v1, "closeAndNoShow()"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->e(Lcom/pollfish/d/a;)Lcom/pollfish/a/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/pollfish/a/a;->a(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Lcom/pollfish/d/a$3$4;

    invoke-direct {v0, p0}, Lcom/pollfish/d/a$3$4;-><init>(Lcom/pollfish/d/a$3;)V

    iget-object v1, p0, Lcom/pollfish/d/a$3;->a:Lcom/pollfish/d/a;

    invoke-static {v1}, Lcom/pollfish/d/a;->d(Lcom/pollfish/d/a;)Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/pollfish/f/c;->a(Landroid/content/Context;Ljava/lang/Runnable;I)V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error while no thanks e: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
