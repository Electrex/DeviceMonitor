.class Lcom/pollfish/d/a$a;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pollfish/d/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/pollfish/d/a;


# direct methods
.method private constructor <init>(Lcom/pollfish/d/a;)V
    .locals 0

    iput-object p1, p0, Lcom/pollfish/d/a$a;->a:Lcom/pollfish/d/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pollfish/d/a;Lcom/pollfish/d/a$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pollfish/d/a$a;-><init>(Lcom/pollfish/d/a;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/pollfish/d/a$a;->a:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->a(Lcom/pollfish/d/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pollfish/d/a$a;->a:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->b(Lcom/pollfish/d/a;)Lcom/pollfish/g/c;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/pollfish/d/a;->k()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/pollfish/d/a$a;->a:Lcom/pollfish/d/a;

    invoke-virtual {v0}, Lcom/pollfish/d/a;->c()V

    :cond_0
    iget-object v0, p0, Lcom/pollfish/d/a$a;->a:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->c(Lcom/pollfish/d/a;)Lcom/pollfish/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pollfish/a/b;->B()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/pollfish/d/a$a;->a:Lcom/pollfish/d/a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/pollfish/d/a;->a(Lcom/pollfish/d/a;Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/pollfish/d/a$a;->a:Lcom/pollfish/d/a;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/pollfish/d/a;->a(Lcom/pollfish/d/a;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IndicatorClickListener e: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
