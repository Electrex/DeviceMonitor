.class Lcom/pollfish/d/a$5$1;
.super Ljava/util/TimerTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pollfish/d/a$5;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/Timer;

.field final synthetic b:Lcom/pollfish/d/a$5;


# direct methods
.method constructor <init>(Lcom/pollfish/d/a$5;Ljava/util/Timer;)V
    .locals 0

    iput-object p1, p0, Lcom/pollfish/d/a$5$1;->b:Lcom/pollfish/d/a$5;

    iput-object p2, p0, Lcom/pollfish/d/a$5$1;->a:Ljava/util/Timer;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/pollfish/d/a$5$1;->b:Lcom/pollfish/d/a$5;

    iget-object v0, v0, Lcom/pollfish/d/a$5;->b:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->k(Lcom/pollfish/d/a;)I

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkIfPollfishParamsObjReady: wait a liitle to check if pollfishParamsObj.getAdvertising_id() becomes !=null  - pollfishParamsObjTimerTimes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/d/a$5$1;->b:Lcom/pollfish/d/a$5;

    iget-object v2, v2, Lcom/pollfish/d/a$5;->b:Lcom/pollfish/d/a;

    invoke-static {v2}, Lcom/pollfish/d/a;->l(Lcom/pollfish/d/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/pollfish/d/a$5$1;->b:Lcom/pollfish/d/a$5;

    iget-object v0, v0, Lcom/pollfish/d/a$5;->b:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->c(Lcom/pollfish/d/a;)Lcom/pollfish/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pollfish/a/b;->J()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/pollfish/d/a$5$1;->b:Lcom/pollfish/d/a$5;

    iget-object v0, v0, Lcom/pollfish/d/a$5;->b:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->l(Lcom/pollfish/d/a;)I

    move-result v0

    const/16 v1, 0xe

    if-le v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/pollfish/d/a$5$1;->a:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iget-object v0, p0, Lcom/pollfish/d/a$5$1;->b:Lcom/pollfish/d/a$5;

    iget-object v0, v0, Lcom/pollfish/d/a$5;->b:Lcom/pollfish/d/a;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/pollfish/d/a;->b(Lcom/pollfish/d/a;I)I

    iget-object v0, p0, Lcom/pollfish/d/a$5$1;->b:Lcom/pollfish/d/a$5;

    iget-object v0, v0, Lcom/pollfish/d/a$5;->b:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/d/a;->c(Lcom/pollfish/d/a;)Lcom/pollfish/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pollfish/a/b;->J()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v0, "OverlayLayout"

    const-string v1, "advertising ID retrieved - cancelling timer - sending attributes"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x7d0

    iget-object v1, p0, Lcom/pollfish/d/a$5$1;->b:Lcom/pollfish/d/a$5;

    iget-object v1, v1, Lcom/pollfish/d/a$5;->b:Lcom/pollfish/d/a;

    new-instance v2, Lcom/pollfish/d/a$5$1$1;

    invoke-direct {v2, p0}, Lcom/pollfish/d/a$5$1$1;-><init>(Lcom/pollfish/d/a$5$1;)V

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Lcom/pollfish/d/a;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "OverlayLayout"

    const-string v1, "null advertising ID - cancelling timer"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "paramsObjTime: e: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a$5$1;->a:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iget-object v0, p0, Lcom/pollfish/d/a$5$1;->b:Lcom/pollfish/d/a$5;

    iget-object v0, v0, Lcom/pollfish/d/a$5;->b:Lcom/pollfish/d/a;

    invoke-static {v0, v6}, Lcom/pollfish/d/a;->b(Lcom/pollfish/d/a;I)I

    goto :goto_0
.end method
