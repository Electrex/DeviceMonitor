.class public Lcom/pollfish/d/a;
.super Landroid/widget/RelativeLayout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pollfish/d/a$a;
    }
.end annotation


# static fields
.field private static a:I

.field private static i:Z

.field private static m:Ljava/lang/String;

.field private static n:I

.field private static o:Ljava/lang/String;


# instance fields
.field private b:Landroid/app/Activity;

.field private c:Lcom/pollfish/a/a;

.field private d:Landroid/widget/Button;

.field private e:Landroid/widget/Button;

.field private f:Lcom/pollfish/g/c;

.field private g:Lcom/pollfish/a/b;

.field private h:Lcom/pollfish/constants/a;

.field private j:Z

.field private k:Lcom/pollfish/interfaces/a$c;

.field private l:Lcom/pollfish/interfaces/a$b;

.field private p:I

.field private q:I

.field private r:Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;

.field private s:Lcom/pollfish/interfaces/PollfishOpenedListener;

.field private t:Lcom/pollfish/interfaces/PollfishClosedListener;

.field private u:Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;

.field private v:Landroid/graphics/Point;

.field private w:I

.field private x:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    sput v0, Lcom/pollfish/d/a;->a:I

    sput-boolean v0, Lcom/pollfish/d/a;->i:Z

    sput-object v1, Lcom/pollfish/d/a;->m:Ljava/lang/String;

    const/4 v0, -0x1

    sput v0, Lcom/pollfish/d/a;->n:I

    sput-object v1, Lcom/pollfish/d/a;->o:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/Activity;Lcom/pollfish/a/b;ZLcom/pollfish/interfaces/a$c;Lcom/pollfish/interfaces/a$b;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/pollfish/d/a;->j:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/pollfish/d/a;->p:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/pollfish/d/a;->q:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/pollfish/d/a;->v:Landroid/graphics/Point;

    const/4 v0, 0x1

    iput v0, p0, Lcom/pollfish/d/a;->w:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/pollfish/d/a;->x:I

    const-string v0, "OverlayLayout"

    const-string v1, "new OverlayLayout()"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    sput-boolean p4, Lcom/pollfish/d/a;->i:Z

    invoke-virtual {p0}, Lcom/pollfish/d/a;->removeAllViews()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/pollfish/d/a;->a(Z)V

    sput-object p11, Lcom/pollfish/d/a;->m:Ljava/lang/String;

    sput p12, Lcom/pollfish/d/a;->n:I

    sput-object p13, Lcom/pollfish/d/a;->o:Ljava/lang/String;

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "serverToConnect: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/pollfish/d/a;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "request_uuid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/pollfish/d/a;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p7, p0, Lcom/pollfish/d/a;->r:Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;

    iput-object p8, p0, Lcom/pollfish/d/a;->s:Lcom/pollfish/interfaces/PollfishOpenedListener;

    iput-object p9, p0, Lcom/pollfish/d/a;->t:Lcom/pollfish/interfaces/PollfishClosedListener;

    iput-object p10, p0, Lcom/pollfish/d/a;->u:Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/pollfish/d/a;->e:Landroid/widget/Button;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/pollfish/d/a;->j:Z

    iput-object p2, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/pollfish/d/a;->c:Lcom/pollfish/a/a;

    iput-object p5, p0, Lcom/pollfish/d/a;->k:Lcom/pollfish/interfaces/a$c;

    iput-object p6, p0, Lcom/pollfish/d/a;->l:Lcom/pollfish/interfaces/a$b;

    iput-object p3, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    iget-object v0, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v0}, Lcom/pollfish/a/b;->u()Landroid/graphics/Point;

    move-result-object v0

    iput-object v0, p0, Lcom/pollfish/d/a;->v:Landroid/graphics/Point;

    :cond_0
    sget-object v0, Lcom/pollfish/constants/a;->a:Lcom/pollfish/constants/a;

    iput-object v0, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pollfishViewStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/pollfish/d/a$1;

    invoke-direct {v1, p0}, Lcom/pollfish/d/a$1;-><init>(Lcom/pollfish/d/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic a(Lcom/pollfish/d/a;I)I
    .locals 0

    iput p1, p0, Lcom/pollfish/d/a;->p:I

    return p1
.end method

.method static synthetic a(Lcom/pollfish/d/a;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    return-object v0
.end method

.method public static a(I)V
    .locals 0

    sput p0, Lcom/pollfish/d/a;->a:I

    return-void
.end method

.method static synthetic a(Lcom/pollfish/d/a;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pollfish/d/a;->d(Z)V

    return-void
.end method

.method static synthetic b(Lcom/pollfish/d/a;I)I
    .locals 0

    iput p1, p0, Lcom/pollfish/d/a;->q:I

    return p1
.end method

.method static synthetic b(Lcom/pollfish/d/a;)Lcom/pollfish/g/c;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    return-object v0
.end method

.method static synthetic b(Lcom/pollfish/d/a;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/pollfish/d/a;->j:Z

    return p1
.end method

.method static synthetic c(Lcom/pollfish/d/a;)Lcom/pollfish/a/b;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    return-object v0
.end method

.method static synthetic d(Lcom/pollfish/d/a;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    return-object v0
.end method

.method private d(Z)V
    .locals 6

    const/4 v4, 0x0

    const/4 v2, 0x0

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "hideIndicator("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/pollfish/d/a;->requestLayout()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    iget-object v1, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v1}, Lcom/pollfish/a/b;->C()Lcom/pollfish/constants/Position;

    move-result-object v1

    const/16 v3, 0x3e8

    move v5, v2

    invoke-static/range {v0 .. v5}, Lcom/pollfish/f/a/b;->a(Landroid/view/View;Lcom/pollfish/constants/Position;ZILandroid/view/View$OnClickListener;I)V

    goto :goto_1
.end method

.method static synthetic e(Lcom/pollfish/d/a;)Lcom/pollfish/a/a;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/d/a;->c:Lcom/pollfish/a/a;

    return-object v0
.end method

.method static synthetic f(Lcom/pollfish/d/a;)Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/d/a;->r:Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;

    return-object v0
.end method

.method static synthetic g(Lcom/pollfish/d/a;)Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/d/a;->u:Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;

    return-object v0
.end method

.method static synthetic h(Lcom/pollfish/d/a;)Lcom/pollfish/interfaces/a$c;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/d/a;->k:Lcom/pollfish/interfaces/a$c;

    return-object v0
.end method

.method static synthetic i(Lcom/pollfish/d/a;)I
    .locals 2

    iget v0, p0, Lcom/pollfish/d/a;->p:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/pollfish/d/a;->p:I

    return v0
.end method

.method public static j()I
    .locals 1

    sget v0, Lcom/pollfish/d/a;->a:I

    return v0
.end method

.method static synthetic j(Lcom/pollfish/d/a;)I
    .locals 1

    iget v0, p0, Lcom/pollfish/d/a;->p:I

    return v0
.end method

.method static synthetic k(Lcom/pollfish/d/a;)I
    .locals 2

    iget v0, p0, Lcom/pollfish/d/a;->q:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/pollfish/d/a;->q:I

    return v0
.end method

.method static synthetic k()Z
    .locals 1

    sget-boolean v0, Lcom/pollfish/d/a;->i:Z

    return v0
.end method

.method static synthetic l(Lcom/pollfish/d/a;)I
    .locals 1

    iget v0, p0, Lcom/pollfish/d/a;->q:I

    return v0
.end method

.method static synthetic l()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/pollfish/d/a;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic m()I
    .locals 1

    sget v0, Lcom/pollfish/d/a;->n:I

    return v0
.end method

.method static synthetic n()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/pollfish/d/a;->o:Ljava/lang/String;

    return-object v0
.end method

.method private o()V
    .locals 12

    const/4 v11, 0x4

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x0

    const-string v1, "OverlayLayout"

    const-string v4, "createIndicator()"

    invoke-static {v1, v4}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/widget/Button;

    iget-object v4, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    invoke-direct {v1, v4}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    iget-object v1, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    const/16 v4, 0x56da

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setId(I)V

    :try_start_0
    iget-object v1, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    invoke-static {v1}, Lcom/pollfish/f/a/b;->a(Landroid/app/Activity;)I

    move-result v1

    const-string v4, "OverlayLayout"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MIDDLE statusBarHeight: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v4, v4, 0x400

    if-eqz v4, :cond_5

    :goto_0
    if-eqz v3, :cond_f

    move v6, v0

    :goto_1
    const/4 v1, 0x2

    new-array v7, v1, [I

    iget-object v1, p0, Lcom/pollfish/d/a;->c:Lcom/pollfish/a/a;

    iget-object v3, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    iget-object v4, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v4}, Lcom/pollfish/a/b;->C()Lcom/pollfish/constants/Position;

    move-result-object v4

    invoke-virtual {v1, v3, v4, v7}, Lcom/pollfish/a/a;->a(Landroid/content/Context;Lcom/pollfish/constants/Position;[I)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v1

    :try_start_1
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v4, 0x0

    aget v4, v7, v4

    iget-object v5, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/pollfish/f/c;->a(ILandroid/content/Context;)I

    move-result v4

    const/4 v5, 0x1

    aget v5, v7, v5

    iget-object v8, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    invoke-virtual {v8}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/pollfish/f/c;->a(ILandroid/content/Context;)I

    move-result v5

    invoke-direct {v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    :try_start_2
    const-string v4, "OverlayLayout"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "createIndicator - statusBarHeight: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "OverlayLayout"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "createIndicator - getWidth(): "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/pollfish/d/a;->getWidth()I

    move-result v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "OverlayLayout"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "createIndicator - panelWidth: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v8, p0, Lcom/pollfish/d/a;->w:I

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "OverlayLayout"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "createIndicator - getHeight(): "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/pollfish/d/a;->getHeight()I

    move-result v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "OverlayLayout"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "createIndicator - panelHeight: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v8, p0, Lcom/pollfish/d/a;->x:I

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v4}, Lcom/pollfish/a/b;->C()Lcom/pollfish/constants/Position;

    move-result-object v4

    sget-object v5, Lcom/pollfish/constants/Position;->BOTTOM_LEFT:Lcom/pollfish/constants/Position;

    if-eq v4, v5, :cond_0

    iget-object v4, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v4}, Lcom/pollfish/a/b;->C()Lcom/pollfish/constants/Position;

    move-result-object v4

    sget-object v5, Lcom/pollfish/constants/Position;->TOP_LEFT:Lcom/pollfish/constants/Position;

    if-eq v4, v5, :cond_0

    iget-object v4, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v4}, Lcom/pollfish/a/b;->C()Lcom/pollfish/constants/Position;

    move-result-object v4

    sget-object v5, Lcom/pollfish/constants/Position;->MIDDLE_LEFT:Lcom/pollfish/constants/Position;

    if-ne v4, v5, :cond_6

    :cond_0
    const/16 v4, 0x9

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    move v5, v0

    :goto_2
    const-string v4, "OverlayLayout"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Pollfish Panel rightMargin: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v4}, Lcom/pollfish/a/b;->C()Lcom/pollfish/constants/Position;

    move-result-object v4

    sget-object v8, Lcom/pollfish/constants/Position;->BOTTOM_LEFT:Lcom/pollfish/constants/Position;

    if-eq v4, v8, :cond_1

    iget-object v4, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v4}, Lcom/pollfish/a/b;->C()Lcom/pollfish/constants/Position;

    move-result-object v4

    sget-object v8, Lcom/pollfish/constants/Position;->BOTTOM_RIGHT:Lcom/pollfish/constants/Position;

    if-ne v4, v8, :cond_8

    :cond_1
    const/16 v4, 0xc

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v4, p0, Lcom/pollfish/d/a;->c:Lcom/pollfish/a/a;

    invoke-virtual {v4}, Lcom/pollfish/a/a;->i()I

    move-result v4

    const/16 v7, 0x64

    if-ne v4, v7, :cond_2

    invoke-virtual {p0}, Lcom/pollfish/d/a;->getHeight()I

    move-result v4

    sub-int/2addr v4, v6

    iget v7, p0, Lcom/pollfish/d/a;->x:I

    sub-int/2addr v4, v7

    if-gez v4, :cond_d

    const-string v7, "OverlayLayout"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Pollfish Panel bottomMargin: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, " <0 changing to zero"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :goto_3
    const-string v4, "OverlayLayout"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Pollfish Panel bottomMargin: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    iget-object v7, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v7}, Lcom/pollfish/a/b;->D()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    invoke-virtual {v8}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/pollfish/f/c;->a(FLandroid/content/Context;)F

    move-result v7

    float-to-int v7, v7

    add-int/2addr v0, v7

    invoke-virtual {v3, v4, v6, v5, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :goto_4
    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    if-eqz v0, :cond_3

    :try_start_3
    iget-object v0, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V
    :try_end_3
    .catch Ljava/lang/NoSuchMethodError; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :goto_5
    iget-object v0, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    invoke-virtual {v0, v11}, Landroid/widget/Button;->setVisibility(I)V

    :cond_3
    iget-object v0, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    if-eqz v0, :cond_4

    if-nez v3, :cond_b

    :cond_4
    :goto_6
    return-void

    :cond_5
    move v3, v0

    goto/16 :goto_0

    :cond_6
    const/16 v4, 0xb

    :try_start_4
    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {p0}, Lcom/pollfish/d/a;->getWidth()I

    move-result v4

    iget v5, p0, Lcom/pollfish/d/a;->w:I

    sub-int/2addr v4, v5

    if-lez v4, :cond_e

    iget-object v4, p0, Lcom/pollfish/d/a;->c:Lcom/pollfish/a/a;

    invoke-virtual {v4}, Lcom/pollfish/a/a;->h()I

    move-result v4

    const/16 v5, 0x64

    if-ne v4, v5, :cond_7

    invoke-virtual {p0}, Lcom/pollfish/d/a;->getWidth()I

    move-result v4

    iget v5, p0, Lcom/pollfish/d/a;->w:I

    sub-int/2addr v4, v5

    move v5, v4

    goto/16 :goto_2

    :cond_7
    move v5, v0

    goto/16 :goto_2

    :cond_8
    iget-object v0, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v0}, Lcom/pollfish/a/b;->C()Lcom/pollfish/constants/Position;

    move-result-object v0

    sget-object v4, Lcom/pollfish/constants/Position;->TOP_LEFT:Lcom/pollfish/constants/Position;

    if-eq v0, v4, :cond_9

    iget-object v0, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v0}, Lcom/pollfish/a/b;->C()Lcom/pollfish/constants/Position;

    move-result-object v0

    sget-object v4, Lcom/pollfish/constants/Position;->TOP_RIGHT:Lcom/pollfish/constants/Position;

    if-ne v0, v4, :cond_a

    :cond_9
    const/16 v0, 0xa

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v4}, Lcom/pollfish/a/b;->D()I

    move-result v4

    int-to-float v4, v4

    iget-object v7, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/pollfish/f/c;->a(FLandroid/content/Context;)F

    move-result v4

    float-to-int v4, v4

    add-int/2addr v4, v6

    const/4 v6, 0x0

    invoke-virtual {v3, v0, v4, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_4

    :catch_0
    move-exception v0

    :goto_7
    const-string v4, "OverlayLayout"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "create indicator exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    goto/16 :goto_4

    :cond_a
    :try_start_5
    const-string v0, "OverlayLayout"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "MIDDLE getHeight(): "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/pollfish/d/a;->getHeight()I

    move-result v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "OverlayLayout"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "MIDDLE dimensions[1]: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v8, 0x1

    aget v8, v7, v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "OverlayLayout"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "MIDDLE (int) Utilities.convertDpToPixel(pollfishParamsObj.getIndicatorPadding(),activity.getApplicationContext()): "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v8, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v8}, Lcom/pollfish/a/b;->D()I

    move-result v8

    int-to-float v8, v8

    iget-object v9, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/pollfish/f/c;->a(FLandroid/content/Context;)F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "OverlayLayout"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "MIDDLE position: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/pollfish/d/a;->getHeight()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    const/4 v9, 0x1

    aget v9, v7, v9

    div-int/lit8 v9, v9, 0x2

    sub-int/2addr v8, v9

    iget-object v9, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v9}, Lcom/pollfish/a/b;->D()I

    move-result v9

    int-to-float v9, v9

    iget-object v10, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    invoke-virtual {v10}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/pollfish/f/c;->a(FLandroid/content/Context;)F

    move-result v9

    float-to-int v9, v9

    add-int/2addr v8, v9

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0xa

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/pollfish/d/a;->getHeight()I

    move-result v4

    sub-int/2addr v4, v6

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v6

    const/4 v6, 0x1

    aget v6, v7, v6

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v4, v6

    iget-object v6, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v6}, Lcom/pollfish/a/b;->D()I

    move-result v6

    int-to-float v6, v6

    iget-object v7, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/pollfish/f/c;->a(FLandroid/content/Context;)F

    move-result v6

    float-to-int v6, v6

    add-int/2addr v4, v6

    const/4 v6, 0x0

    invoke-virtual {v3, v0, v4, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_4

    :catch_1
    move-exception v0

    const-string v2, "OverlayLayout"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "create indicator exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/pollfish/f/b;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_5

    :catch_2
    move-exception v0

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "create indicator exception: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_b
    iget-object v0, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    invoke-virtual {p0, v0, v3}, Lcom/pollfish/d/a;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :goto_8
    iget-object v0, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    invoke-virtual {v0, v11}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_6

    :cond_c
    const-string v0, "OverlayLayout"

    const-string v1, "indicator view has already a parent view"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    :catch_3
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    goto/16 :goto_7

    :catch_4
    move-exception v0

    move-object v3, v2

    goto/16 :goto_7

    :cond_d
    move v0, v4

    goto/16 :goto_3

    :cond_e
    move v5, v0

    goto/16 :goto_2

    :cond_f
    move v6, v1

    goto/16 :goto_1
.end method


# virtual methods
.method public a()V
    .locals 2

    const-string v0, "OverlayLayout"

    const-string v1, "addPollfishIndicatorAndPanel()"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/pollfish/d/a;->e()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "OverlayLayout"

    const-string v1, "addPollfishIndicatorAndPanel(): pollFishPanel not null - do not create"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    const-string v0, "OverlayLayout"

    const-string v1, "failed to create pollfish Panel"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/pollfish/a/a;)V
    .locals 6

    const/16 v5, 0x64

    const/4 v0, 0x0

    const-string v1, "OverlayLayout"

    const-string v2, "proceedPanel()"

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    if-eqz v1, :cond_3

    if-eqz p1, :cond_3

    iput-object p1, p0, Lcom/pollfish/d/a;->c:Lcom/pollfish/a/a;

    :try_start_0
    iget-object v1, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    invoke-static {v1}, Lcom/pollfish/f/a/b;->a(Landroid/app/Activity;)I

    move-result v1

    invoke-virtual {p1}, Lcom/pollfish/a/a;->h()I

    move-result v2

    iget-object v3, p0, Lcom/pollfish/d/a;->v:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    mul-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x64

    iput v2, p0, Lcom/pollfish/d/a;->w:I

    invoke-virtual {p1}, Lcom/pollfish/a/a;->i()I

    move-result v2

    iget-object v3, p0, Lcom/pollfish/d/a;->v:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    sub-int v1, v3, v1

    mul-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x64

    iput v1, p0, Lcom/pollfish/d/a;->x:I

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Panel width: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/pollfish/d/a;->w:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/pollfish/d/a;->x:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v1}, Lcom/pollfish/a/b;->B()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    if-nez v1, :cond_4

    invoke-direct {p0}, Lcom/pollfish/d/a;->o()V

    :cond_0
    :goto_1
    :try_start_1
    iget-object v1, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    invoke-static {v1}, Lcom/pollfish/f/a/b;->a(Landroid/app/Activity;)I

    move-result v1

    invoke-virtual {p1}, Lcom/pollfish/a/a;->h()I

    move-result v2

    iget-object v3, p0, Lcom/pollfish/d/a;->v:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    mul-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x64

    iput v2, p0, Lcom/pollfish/d/a;->w:I

    invoke-virtual {p1}, Lcom/pollfish/a/a;->i()I

    move-result v2

    iget-object v3, p0, Lcom/pollfish/d/a;->v:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    sub-int/2addr v3, v1

    mul-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x64

    iput v2, p0, Lcom/pollfish/d/a;->x:I

    const-string v2, "OverlayLayout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Panel width: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/pollfish/d/a;->w:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " height: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/pollfish/d/a;->x:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    if-eqz v2, :cond_a

    const-string v2, "OverlayLayout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "statusBarHeight:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v2, v2, 0x400

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_b

    :goto_3
    const-string v1, "OverlayLayout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "app is on fullScreen (status bar visible): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getWidth(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/pollfish/d/a;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "OverlayLayout getHeight(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/pollfish/d/a;->getHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Pollfish Panel panelWidth: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/pollfish/d/a;->w:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " panelHeight: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/pollfish/d/a;->x:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, p0, Lcom/pollfish/d/a;->w:I

    iget v3, p0, Lcom/pollfish/d/a;->x:I

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v2}, Lcom/pollfish/a/b;->C()Lcom/pollfish/constants/Position;

    move-result-object v2

    sget-object v3, Lcom/pollfish/constants/Position;->TOP_RIGHT:Lcom/pollfish/constants/Position;

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v2}, Lcom/pollfish/a/b;->C()Lcom/pollfish/constants/Position;

    move-result-object v2

    sget-object v3, Lcom/pollfish/constants/Position;->BOTTOM_RIGHT:Lcom/pollfish/constants/Position;

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v2}, Lcom/pollfish/a/b;->C()Lcom/pollfish/constants/Position;

    move-result-object v2

    sget-object v3, Lcom/pollfish/constants/Position;->MIDDLE_RIGHT:Lcom/pollfish/constants/Position;

    if-ne v2, v3, :cond_7

    :cond_1
    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {p0}, Lcom/pollfish/d/a;->getWidth()I

    move-result v2

    iget v3, p0, Lcom/pollfish/d/a;->w:I

    sub-int/2addr v2, v3

    if-lez v2, :cond_2

    invoke-virtual {p1}, Lcom/pollfish/a/a;->h()I

    move-result v2

    if-ne v2, v5, :cond_6

    invoke-virtual {p0}, Lcom/pollfish/d/a;->getWidth()I

    move-result v2

    iget v3, p0, Lcom/pollfish/d/a;->w:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    :cond_2
    :goto_4
    const-string v2, "OverlayLayout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Pollfish Panel panelObj.getHeight_percentage(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/pollfish/a/a;->i()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/pollfish/d/a;->getHeight()I

    move-result v2

    sub-int/2addr v2, v0

    iget v3, p0, Lcom/pollfish/d/a;->x:I

    sub-int/2addr v2, v3

    if-lez v2, :cond_9

    invoke-virtual {p1}, Lcom/pollfish/a/a;->i()I

    move-result v2

    if-ne v2, v5, :cond_8

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    :goto_5
    const-string v2, "OverlayLayout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Pollfish Panel params.topMargin: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_6
    const-string v2, "OverlayLayout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getHeight(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/pollfish/d/a;->getHeight()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "OverlayLayout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "screenSize.y: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/pollfish/d/a;->v:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "OverlayLayout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "statusBarHeight: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "panelHeight: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/pollfish/d/a;->x:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "params.topMargin: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    invoke-virtual {p0, v0, v1}, Lcom/pollfish/d/a;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/pollfish/d/a;->f()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_3
    :goto_7
    return-void

    :catch_0
    move-exception v1

    const-string v1, "OverlayLayout"

    const-string v2, "Error while calculating panel size"

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    const-string v1, "OverlayLayout"

    const-string v2, "addPollfishIndicatorAndPanel(): indicator not null - do not create"

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    move v2, v0

    goto/16 :goto_2

    :cond_6
    const/4 v2, 0x0

    :try_start_2
    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_4

    :catch_1
    move-exception v0

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    :cond_7
    const/16 v2, 0x9

    :try_start_3
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto/16 :goto_4

    :cond_8
    invoke-virtual {p0}, Lcom/pollfish/d/a;->getHeight()I

    move-result v2

    sub-int/2addr v2, v0

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v0

    iget v3, p0, Lcom/pollfish/d/a;->x:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto/16 :goto_5

    :cond_9
    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto/16 :goto_6

    :cond_a
    const-string v0, "OverlayLayout"

    const-string v1, "pollFishPanel == null"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_7

    :cond_b
    move v0, v1

    goto/16 :goto_3
.end method

.method public a(Ljava/util/Map;)V
    .locals 5

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAttributesMap: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    const-string v2, "attributes"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "OverlayLayout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "attrObject.toString():  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sentToServer with url:  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/pollfish/d/a;->m:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/device/set/attributes"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {p0, v1}, Lcom/pollfish/d/a;->a(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "PollFish"

    const-string v2, "Something is wrong with your attributes. Please follow suggested guidelines"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error while parsing sentToServer from javascript : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 1

    new-instance v0, Lcom/pollfish/d/a$5;

    invoke-direct {v0, p0, p1}, Lcom/pollfish/d/a$5;-><init>(Lcom/pollfish/d/a;Lorg/json/JSONObject;)V

    invoke-virtual {p0, v0}, Lcom/pollfish/d/a;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Z)V
    .locals 4

    const-string v0, "OverlayLayout"

    const-string v1, "hidePanelAndIndicator()"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/pollfish/d/a;->a(ZZ)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/pollfish/d/a;->d(Z)V

    sget-object v0, Lcom/pollfish/constants/a;->a:Lcom/pollfish/constants/a;

    iput-object v0, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error while trying to hide panel and indicator: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(ZZ)V
    .locals 13

    const/4 v12, 0x4

    const/4 v5, 0x0

    const/4 v8, 0x0

    const-string v0, "OverlayLayout"

    const-string v1, "hidePanel()"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a;->c:Lcom/pollfish/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    new-instance v0, Lcom/pollfish/c/e;

    iget-object v1, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/pollfish/d/a;->m:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/v2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/device/set/survey/hidden"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/pollfish/d/a;->n:I

    sget-object v4, Lcom/pollfish/d/a;->o:Ljava/lang/String;

    iget-object v6, p0, Lcom/pollfish/d/a;->k:Lcom/pollfish/interfaces/a$c;

    iget-object v7, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    iget-object v10, p0, Lcom/pollfish/d/a;->c:Lcom/pollfish/a/a;

    move-object v9, v5

    move-object v11, v5

    invoke-direct/range {v0 .. v11}, Lcom/pollfish/c/e;-><init>(Lcom/pollfish/a/b;Ljava/lang/String;ILjava/lang/String;Lorg/json/JSONObject;Lcom/pollfish/interfaces/a$c;Landroid/app/Activity;ZLjava/lang/String;Lcom/pollfish/a/a;Ljava/lang/String;)V

    new-array v1, v8, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/pollfish/c/e;->c([Ljava/lang/Object;)Lcom/pollfish/c/h;

    :cond_0
    invoke-virtual {p0, v8}, Lcom/pollfish/d/a;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    sget-object v1, Lcom/pollfish/constants/a;->c:Lcom/pollfish/constants/a;

    if-eq v0, v1, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/pollfish/d/a;->e:Landroid/widget/Button;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/pollfish/d/a;->e:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    iget-object v0, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/pollfish/constants/a;->a:Lcom/pollfish/constants/a;

    iput-object v0, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pollfishViewStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v0}, Lcom/pollfish/a/b;->B()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-direct {p0, v8}, Lcom/pollfish/d/a;->d(Z)V

    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    instance-of v0, v0, Lcom/pollfish/interfaces/PollfishClosedListener;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    check-cast v0, Lcom/pollfish/interfaces/PollfishClosedListener;

    invoke-interface {v0}, Lcom/pollfish/interfaces/PollfishClosedListener;->onPollfishClosed()V

    :cond_5
    :goto_2
    if-nez p1, :cond_9

    iget-object v0, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    invoke-virtual {v0}, Lcom/pollfish/g/c;->getVisibility()I

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    invoke-virtual {v0, v12}, Lcom/pollfish/g/c;->setVisibility(I)V

    :cond_6
    :goto_3
    iget-object v0, p0, Lcom/pollfish/d/a;->e:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pollfish/d/a;->e:Landroid/widget/Button;

    invoke-virtual {v0, v12}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/pollfish/d/a;->c:Lcom/pollfish/a/a;

    if-eqz v0, :cond_4

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "panelObj.isPollfishCompleted(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/d/a;->c:Lcom/pollfish/a/a;

    invoke-virtual {v2}, Lcom/pollfish/a/a;->f()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a;->c:Lcom/pollfish/a/a;

    invoke-virtual {v0}, Lcom/pollfish/a/a;->f()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/pollfish/d/a;->b()V

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lcom/pollfish/d/a;->t:Lcom/pollfish/interfaces/PollfishClosedListener;

    if-eqz v0, :cond_5

    const-string v0, "OverlayLayout"

    const-string v1, "pollfishClosedListenerExt!=null"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a;->t:Lcom/pollfish/interfaces/PollfishClosedListener;

    invoke-interface {v0}, Lcom/pollfish/interfaces/PollfishClosedListener;->onPollfishClosed()V

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    invoke-virtual {v0}, Lcom/pollfish/g/c;->getVisibility()I

    move-result v0

    if-nez v0, :cond_6

    iget-object v6, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    iget-object v0, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v0}, Lcom/pollfish/a/b;->C()Lcom/pollfish/constants/Position;

    move-result-object v7

    const/16 v9, 0x3e8

    move-object v10, v5

    move v11, v8

    invoke-static/range {v6 .. v11}, Lcom/pollfish/f/a/b;->a(Landroid/view/View;Lcom/pollfish/constants/Position;ZILandroid/view/View$OnClickListener;I)V

    goto :goto_3
.end method

.method public b()V
    .locals 6

    const-string v0, "OverlayLayout"

    const-string v1, "showIndicator()"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a;->c:Lcom/pollfish/a/a;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/pollfish/d/a;->c:Lcom/pollfish/a/a;

    invoke-virtual {v0}, Lcom/pollfish/a/a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v0}, Lcom/pollfish/a/b;->B()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/pollfish/d/a;->c()V

    :cond_2
    iget-object v0, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    sget-object v1, Lcom/pollfish/constants/a;->c:Lcom/pollfish/constants/a;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    sget-object v1, Lcom/pollfish/constants/a;->b:Lcom/pollfish/constants/a;

    if-ne v0, v1, :cond_0

    :cond_3
    sget-boolean v0, Lcom/pollfish/d/a;->i:Z

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    iget-object v1, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v1}, Lcom/pollfish/a/b;->C()Lcom/pollfish/constants/Position;

    move-result-object v1

    const/4 v2, 0x1

    const/16 v3, 0x3e8

    new-instance v4, Lcom/pollfish/d/a$a;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/pollfish/d/a$a;-><init>(Lcom/pollfish/d/a;Lcom/pollfish/d/a$1;)V

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/pollfish/f/a/b;->a(Landroid/view/View;Lcom/pollfish/constants/Position;ZILandroid/view/View$OnClickListener;I)V

    sget-object v0, Lcom/pollfish/constants/a;->b:Lcom/pollfish/constants/a;

    iput-object v0, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pollfishViewStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->bringToFront()V

    iget-object v0, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    iget-object v0, p0, Lcom/pollfish/d/a;->d:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "animateView e: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public b(Z)V
    .locals 0

    sput-boolean p1, Lcom/pollfish/d/a;->i:Z

    return-void
.end method

.method public c()V
    .locals 14

    const/4 v13, -0x1

    const/4 v12, 0x1

    const/4 v5, 0x0

    const/4 v8, 0x0

    const-string v0, "OverlayLayout"

    const-string v1, "showPanel()"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a;->c:Lcom/pollfish/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/pollfish/c/e;

    iget-object v1, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/pollfish/d/a;->m:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/v2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/device/set/survey/viewed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/pollfish/d/a;->n:I

    sget-object v4, Lcom/pollfish/d/a;->o:Ljava/lang/String;

    iget-object v6, p0, Lcom/pollfish/d/a;->k:Lcom/pollfish/interfaces/a$c;

    iget-object v7, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    iget-object v10, p0, Lcom/pollfish/d/a;->c:Lcom/pollfish/a/a;

    move-object v9, v5

    move-object v11, v5

    invoke-direct/range {v0 .. v11}, Lcom/pollfish/c/e;-><init>(Lcom/pollfish/a/b;Ljava/lang/String;ILjava/lang/String;Lorg/json/JSONObject;Lcom/pollfish/interfaces/a$c;Landroid/app/Activity;ZLjava/lang/String;Lcom/pollfish/a/a;Ljava/lang/String;)V

    new-array v1, v8, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/pollfish/c/e;->c([Ljava/lang/Object;)Lcom/pollfish/c/h;

    :cond_0
    iget-object v0, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v0}, Lcom/pollfish/a/b;->B()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, v12}, Lcom/pollfish/d/a;->d(Z)V

    :goto_0
    iget-object v0, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    sget-object v1, Lcom/pollfish/constants/a;->c:Lcom/pollfish/constants/a;

    if-ne v0, v1, :cond_3

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-direct {p0, v8}, Lcom/pollfish/d/a;->d(Z)V

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/pollfish/constants/a;->c:Lcom/pollfish/constants/a;

    iput-object v0, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pollfishViewStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a;->c:Lcom/pollfish/a/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pollfish/d/a;->c:Lcom/pollfish/a/a;

    invoke-virtual {v0}, Lcom/pollfish/a/a;->f()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    instance-of v0, v0, Lcom/pollfish/interfaces/PollfishOpenedListener;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    check-cast v0, Lcom/pollfish/interfaces/PollfishOpenedListener;

    invoke-interface {v0}, Lcom/pollfish/interfaces/PollfishOpenedListener;->onPollfishOpened()V

    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/pollfish/d/a;->e:Landroid/widget/Button;

    if-nez v0, :cond_5

    const-string v0, "OverlayLayout"

    const-string v1, "add semi-trasparent view"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v13, v13}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    new-instance v0, Landroid/widget/Button;

    iget-object v2, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    invoke-direct {v0, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pollfish/d/a;->e:Landroid/widget/Button;

    iget-object v0, p0, Lcom/pollfish/d/a;->e:Landroid/widget/Button;

    const v2, 0xad92

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setId(I)V

    :try_start_0
    iget-object v0, p0, Lcom/pollfish/d/a;->e:Landroid/widget/Button;

    iget-object v2, p0, Lcom/pollfish/d/a;->c:Lcom/pollfish/a/a;

    invoke-virtual {v2}, Lcom/pollfish/a/a;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundColor(I)V

    const-string v0, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "semiTrasparentView.setBackgroundColor: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/pollfish/d/a;->c:Lcom/pollfish/a/a;

    invoke-virtual {v3}, Lcom/pollfish/a/a;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_3
    const-string v0, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pollFishPanel.getId(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    invoke-virtual {v3}, Lcom/pollfish/g/c;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a;->e:Landroid/widget/Button;

    invoke-virtual {p0, v0, v1}, Lcom/pollfish/d/a;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_5
    iget-object v0, p0, Lcom/pollfish/d/a;->e:Landroid/widget/Button;

    new-instance v1, Lcom/pollfish/d/a$2;

    invoke-direct {v1, p0}, Lcom/pollfish/d/a$2;-><init>(Lcom/pollfish/d/a;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-boolean v0, Lcom/pollfish/d/a;->i:Z

    if-nez v0, :cond_1

    invoke-virtual {p0, v12}, Lcom/pollfish/d/a;->setFocusableInTouchMode(Z)V

    invoke-virtual {p0}, Lcom/pollfish/d/a;->requestFocus()Z

    :try_start_1
    iget-object v0, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    iget-object v1, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v1}, Lcom/pollfish/a/b;->C()Lcom/pollfish/constants/Position;

    move-result-object v1

    const/4 v2, 0x1

    const/16 v3, 0x3e8

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/pollfish/f/a/b;->a(Landroid/view/View;Lcom/pollfish/constants/Position;ZILandroid/view/View$OnClickListener;I)V

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pollFishPanel.getVisibility()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    invoke-virtual {v2}, Lcom/pollfish/g/c;->getVisibility()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :cond_6
    :goto_4
    iget-object v0, p0, Lcom/pollfish/d/a;->e:Landroid/widget/Button;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/pollfish/d/a;->e:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lcom/pollfish/d/a;->e:Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    :cond_7
    iget-object v0, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    invoke-virtual {v0}, Lcom/pollfish/g/c;->bringToFront()V

    :try_start_2
    iget-object v0, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    const/high16 v1, 0x41f00000    # 30.0f

    invoke-virtual {v0, v1}, Lcom/pollfish/g/c;->setTranslationZ(F)V

    iget-object v0, p0, Lcom/pollfish/d/a;->e:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTranslationZ(F)V

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pollFishPanel.getTranslationZ(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    invoke-virtual {v2}, Lcom/pollfish/g/c;->getTranslationZ()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "semiTrasparentView.getTranslationZ(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/d/a;->e:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getTranslationZ()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pollFishPanel.getElevation(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    invoke-virtual {v2}, Lcom/pollfish/g/c;->getElevation()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "semiTrasparentView.getElevation(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/d/a;->e:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getElevation()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/NoSuchMethodError; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    goto/16 :goto_1

    :catch_0
    move-exception v0

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Trying to set elevation - NoSuchMethodError: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_8
    iget-object v0, p0, Lcom/pollfish/d/a;->s:Lcom/pollfish/interfaces/PollfishOpenedListener;

    if-eqz v0, :cond_4

    const-string v0, "OverlayLayout"

    const-string v1, "pollfishOpenedListenerExt!=null"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a;->s:Lcom/pollfish/interfaces/PollfishOpenedListener;

    invoke-interface {v0}, Lcom/pollfish/interfaces/PollfishOpenedListener;->onPollfishOpened()V

    goto/16 :goto_2

    :catch_1
    move-exception v0

    const-string v2, "OverlayLayout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "semiTrasparentView.setBackgroundColor e: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a;->e:Landroid/widget/Button;

    const-string v2, "#55000000"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundColor(I)V

    goto/16 :goto_3

    :catch_2
    move-exception v0

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "animateView e: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    invoke-virtual {v0, v8}, Lcom/pollfish/g/c;->setVisibility(I)V

    :cond_9
    iget-object v0, p0, Lcom/pollfish/d/a;->e:Landroid/widget/Button;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/pollfish/d/a;->e:Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_4

    :catch_3
    move-exception v0

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Trying to set elevation - Exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public c(Z)Z
    .locals 5

    const/4 v0, 0x1

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isPollfishPanelOpen(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    sget-object v2, Lcom/pollfish/constants/a;->c:Lcom/pollfish/constants/a;

    if-ne v1, v2, :cond_1

    if-eqz p1, :cond_0

    const-string v1, "OverlayLayout"

    const-string v2, "Panel is open -> closing!"

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {p0, v1, v2}, Lcom/pollfish/d/a;->a(ZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const-string v1, "OverlayLayout"

    const-string v2, "Panel is open"

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return v0

    :catch_0
    move-exception v1

    const-string v2, "OverlayLayout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isPollfishPanelOpen() exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public d()V
    .locals 4

    const-string v0, "OverlayLayout"

    const-string v1, "showPollfishOnStartup()"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a;->c:Lcom/pollfish/a/a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/pollfish/d/a;->g:Lcom/pollfish/a/b;

    invoke-virtual {v0}, Lcom/pollfish/a/b;->B()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "OverlayLayout"

    const-string v1, "showPollfishOnStartup(): show Panel call in custom mode"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    sget-object v1, Lcom/pollfish/constants/a;->c:Lcom/pollfish/constants/a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    sget-object v1, Lcom/pollfish/constants/a;->b:Lcom/pollfish/constants/a;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/pollfish/d/a;->c()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "showPollfishOnStartup() error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    :try_start_1
    const-string v0, "OverlayLayout"

    const-string v1, "showPollfishOnStartup(): show on server mode"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a;->c:Lcom/pollfish/a/a;

    invoke-virtual {v0}, Lcom/pollfish/a/a;->e()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    sget-object v1, Lcom/pollfish/constants/a;->b:Lcom/pollfish/constants/a;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    sget-object v1, Lcom/pollfish/constants/a;->c:Lcom/pollfish/constants/a;

    if-eq v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/pollfish/d/a;->b()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "showPollfishOnStartup error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    sget-object v1, Lcom/pollfish/constants/a;->c:Lcom/pollfish/constants/a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    sget-object v1, Lcom/pollfish/constants/a;->b:Lcom/pollfish/constants/a;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/pollfish/d/a;->c()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :cond_3
    const-string v0, "OverlayLayout"

    const-string v1, "panelObj == null"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public e()V
    .locals 3

    const-string v0, "OverlayLayout"

    const-string v1, "createPollfishPanel()"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/pollfish/g/c;

    iget-object v1, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    new-instance v2, Lcom/pollfish/d/a$3;

    invoke-direct {v2, p0}, Lcom/pollfish/d/a$3;-><init>(Lcom/pollfish/d/a;)V

    invoke-direct {v0, v1, v2}, Lcom/pollfish/g/c;-><init>(Landroid/content/Context;Lcom/pollfish/interfaces/b;)V

    iput-object v0, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    iget-object v0, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    const v1, 0x32dcd4

    invoke-virtual {v0, v1}, Lcom/pollfish/g/c;->setId(I)V

    iget-object v0, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/pollfish/g/c;->setVisibility(I)V

    return-void
.end method

.method public f()V
    .locals 1

    new-instance v0, Lcom/pollfish/d/a$4;

    invoke-direct {v0, p0}, Lcom/pollfish/d/a$4;-><init>(Lcom/pollfish/d/a;)V

    invoke-virtual {p0, v0}, Lcom/pollfish/d/a;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public g()Ljava/util/HashMap;
    .locals 5

    const/4 v3, 0x0

    const-string v0, "OverlayLayout"

    const-string v1, "checkTasksAndContinueLoadPanel"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/d/a;->l:Lcom/pollfish/interfaces/a$b;

    invoke-interface {v0}, Lcom/pollfish/interfaces/a$b;->b()Ljava/util/HashMap;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v0, "OverlayLayout"

    const-string v2, " all tasks set "

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "OverlayLayout"

    const-string v2, "check if files/assets exist-> proceed!!"

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    const-string v0, "OverlayLayout"

    const-string v2, "call pollFishPanel.loadContent()"

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/pollfish/d/a$6;

    invoke-direct {v0, p0}, Lcom/pollfish/d/a$6;-><init>(Lcom/pollfish/d/a;)V

    iget-object v2, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Lcom/pollfish/f/c;->a(Landroid/content/Context;Ljava/lang/Runnable;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v2, "OverlayLayout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkTasksAndContinueLoadPanel() error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v0, "OverlayLayout"

    const-string v2, "check if files/assets exist - You have to replace strings before showing webview!"

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-boolean v0, Lcom/pollfish/d/a;->i:Z

    if-nez v0, :cond_1

    new-instance v0, Lcom/pollfish/d/a$7;

    invoke-direct {v0, p0, v1}, Lcom/pollfish/d/a$7;-><init>(Lcom/pollfish/d/a;Ljava/util/HashMap;)V

    iget-object v2, p0, Lcom/pollfish/d/a;->b:Landroid/app/Activity;

    invoke-static {v2, v0, v3}, Lcom/pollfish/f/c;->a(Landroid/content/Context;Ljava/lang/Runnable;I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/pollfish/d/a;->a(Z)V

    goto :goto_0

    :cond_2
    const-string v0, "OverlayLayout"

    const-string v2, "not all tasks set so far, check in a while!"

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public h()V
    .locals 4

    const-string v0, "OverlayLayout"

    const-string v1, "hidePollfishManually() - manualy by the user"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "PollFish"

    const-string v1, "Developer called hide Pollfish"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "before hide pollfishViewStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    sput-boolean v0, Lcom/pollfish/f/a;->a:Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/pollfish/d/a;->b(Z)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/pollfish/d/a;->a(Z)V

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "after hide pollfishViewStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception while hidePollfish(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public i()V
    .locals 4

    const/4 v2, 0x0

    const-string v0, "OverlayLayout"

    const-string v1, "showPollfishManually() - manualy by the user"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "PollFish"

    const-string v1, "Developer called show Pollfish"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sput-boolean v2, Lcom/pollfish/f/a;->a:Z

    invoke-virtual {p0, v2}, Lcom/pollfish/d/a;->b(Z)V

    iget-boolean v0, p0, Lcom/pollfish/d/a;->j:Z

    if-eqz v0, :cond_0

    :try_start_0
    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "before show pollfishViewStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/pollfish/d/a;->d()V

    const-string v0, "OverlayLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "after show pollfishViewStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception while showPollfish(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4

    const/4 v0, 0x1

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onKeyDown: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " pollfishViewStatus: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/pollfish/d/a;->f:Lcom/pollfish/g/c;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/pollfish/d/a;->h:Lcom/pollfish/constants/a;

    sget-object v2, Lcom/pollfish/constants/a;->c:Lcom/pollfish/constants/a;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/pollfish/d/a;->setFocusableInTouchMode(Z)V

    invoke-virtual {p0}, Lcom/pollfish/d/a;->requestFocus()Z

    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/pollfish/d/a;->a(ZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "OverlayLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onKeyDown: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
