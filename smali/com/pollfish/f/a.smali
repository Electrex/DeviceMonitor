.class public Lcom/pollfish/f/a;
.super Ljava/lang/Object;


# static fields
.field public static a:Z

.field private static p:Ljava/lang/String;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Landroid/view/ViewGroup;

.field private C:Lcom/pollfish/interfaces/a$b;

.field private D:Lcom/pollfish/interfaces/a$e;

.field private E:Lcom/pollfish/interfaces/a$a;

.field private F:Lcom/pollfish/interfaces/a$c;

.field private G:Lcom/pollfish/interfaces/a$d;

.field private b:Lcom/pollfish/interfaces/a$f;

.field private c:Landroid/app/Activity;

.field private d:Lcom/pollfish/a/b;

.field private e:Lcom/pollfish/a/a;

.field private f:Lcom/pollfish/d/a;

.field private g:Ljava/lang/String;

.field private h:Lcom/pollfish/constants/Position;

.field private i:I

.field private j:Z

.field private k:Z

.field private l:I

.field private m:Ljava/util/HashMap;

.field private n:I

.field private o:Ljava/util/Timer;

.field private q:Z

.field private r:Z

.field private s:Lcom/pollfish/interfaces/PollfishSurveyReceivedListener;

.field private t:Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;

.field private u:Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;

.field private v:Lcom/pollfish/interfaces/PollfishOpenedListener;

.field private w:Lcom/pollfish/interfaces/PollfishClosedListener;

.field private x:Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;

.field private y:Ljava/lang/String;

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/pollfish/f/a;->a:Z

    const-string v0, ""

    sput-object v0, Lcom/pollfish/f/a;->p:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;ZLcom/pollfish/constants/Position;IZLcom/pollfish/interfaces/a$f;Lcom/pollfish/interfaces/PollfishSurveyReceivedListener;Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Ljava/lang/String;ILandroid/view/ViewGroup;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/pollfish/f/a;->b:Lcom/pollfish/interfaces/a$f;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/pollfish/f/a;->d:Lcom/pollfish/a/b;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/pollfish/f/a;->e:Lcom/pollfish/a/a;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/pollfish/f/a;->f:Lcom/pollfish/d/a;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/pollfish/f/a;->h:Lcom/pollfish/constants/Position;

    const/4 v1, 0x0

    iput v1, p0, Lcom/pollfish/f/a;->l:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/pollfish/f/a;->n:I

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/pollfish/f/a;->s:Lcom/pollfish/interfaces/PollfishSurveyReceivedListener;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/pollfish/f/a;->t:Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/pollfish/f/a;->u:Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/pollfish/f/a;->v:Lcom/pollfish/interfaces/PollfishOpenedListener;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/pollfish/f/a;->w:Lcom/pollfish/interfaces/PollfishClosedListener;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/pollfish/f/a;->y:Ljava/lang/String;

    new-instance v1, Lcom/pollfish/f/a$1;

    invoke-direct {v1, p0}, Lcom/pollfish/f/a$1;-><init>(Lcom/pollfish/f/a;)V

    iput-object v1, p0, Lcom/pollfish/f/a;->C:Lcom/pollfish/interfaces/a$b;

    new-instance v1, Lcom/pollfish/f/a$2;

    invoke-direct {v1, p0}, Lcom/pollfish/f/a$2;-><init>(Lcom/pollfish/f/a;)V

    iput-object v1, p0, Lcom/pollfish/f/a;->D:Lcom/pollfish/interfaces/a$e;

    new-instance v1, Lcom/pollfish/f/a$3;

    invoke-direct {v1, p0}, Lcom/pollfish/f/a$3;-><init>(Lcom/pollfish/f/a;)V

    iput-object v1, p0, Lcom/pollfish/f/a;->E:Lcom/pollfish/interfaces/a$a;

    new-instance v1, Lcom/pollfish/f/a$4;

    invoke-direct {v1, p0}, Lcom/pollfish/f/a$4;-><init>(Lcom/pollfish/f/a;)V

    iput-object v1, p0, Lcom/pollfish/f/a;->F:Lcom/pollfish/interfaces/a$c;

    new-instance v1, Lcom/pollfish/f/a$5;

    invoke-direct {v1, p0}, Lcom/pollfish/f/a$5;-><init>(Lcom/pollfish/f/a;)V

    iput-object v1, p0, Lcom/pollfish/f/a;->G:Lcom/pollfish/interfaces/a$d;

    iput-object p8, p0, Lcom/pollfish/f/a;->s:Lcom/pollfish/interfaces/PollfishSurveyReceivedListener;

    iput-object p9, p0, Lcom/pollfish/f/a;->t:Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;

    iput-object p10, p0, Lcom/pollfish/f/a;->u:Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;

    iput-object p11, p0, Lcom/pollfish/f/a;->v:Lcom/pollfish/interfaces/PollfishOpenedListener;

    iput-object p12, p0, Lcom/pollfish/f/a;->w:Lcom/pollfish/interfaces/PollfishClosedListener;

    iput-object p13, p0, Lcom/pollfish/f/a;->x:Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;

    const/4 v1, 0x0

    iput v1, p0, Lcom/pollfish/f/a;->l:I

    iput-object p1, p0, Lcom/pollfish/f/a;->c:Landroid/app/Activity;

    iput-object p2, p0, Lcom/pollfish/f/a;->g:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/pollfish/f/a;->k:Z

    iput-object p4, p0, Lcom/pollfish/f/a;->h:Lcom/pollfish/constants/Position;

    iput p5, p0, Lcom/pollfish/f/a;->i:I

    iput-boolean p6, p0, Lcom/pollfish/f/a;->j:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/pollfish/f/a;->q:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/pollfish/f/a;->r:Z

    iput-object p7, p0, Lcom/pollfish/f/a;->b:Lcom/pollfish/interfaces/a$f;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/pollfish/f/a;->m:Ljava/util/HashMap;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/pollfish/f/a;->y:Ljava/lang/String;

    move/from16 v0, p15

    iput v0, p0, Lcom/pollfish/f/a;->z:I

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/pollfish/f/a;->A:Ljava/lang/String;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/pollfish/f/a;->B:Landroid/view/ViewGroup;

    return-void
.end method

.method static synthetic a(Lcom/pollfish/f/a;I)I
    .locals 0

    iput p1, p0, Lcom/pollfish/f/a;->l:I

    return p1
.end method

.method static synthetic a(Lcom/pollfish/f/a;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/f/a;->c:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic a(Lcom/pollfish/f/a;Ljava/util/Timer;)Ljava/util/Timer;
    .locals 0

    iput-object p1, p0, Lcom/pollfish/f/a;->o:Ljava/util/Timer;

    return-object p1
.end method

.method static synthetic a(Lcom/pollfish/f/a;Lcom/pollfish/a/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pollfish/f/a;->c(Lcom/pollfish/a/a;)V

    return-void
.end method

.method private a(Z)V
    .locals 13

    const-string v0, "LifeCycle"

    const-string v1, "reorder app Layouts - add Pollfish layout"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/pollfish/f/a;->c:Landroid/app/Activity;

    iget-object v1, p0, Lcom/pollfish/f/a;->d:Lcom/pollfish/a/b;

    iget-object v3, p0, Lcom/pollfish/f/a;->F:Lcom/pollfish/interfaces/a$c;

    iget-object v4, p0, Lcom/pollfish/f/a;->C:Lcom/pollfish/interfaces/a$b;

    iget-object v5, p0, Lcom/pollfish/f/a;->u:Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;

    iget-object v6, p0, Lcom/pollfish/f/a;->v:Lcom/pollfish/interfaces/PollfishOpenedListener;

    iget-object v7, p0, Lcom/pollfish/f/a;->w:Lcom/pollfish/interfaces/PollfishClosedListener;

    iget-object v8, p0, Lcom/pollfish/f/a;->x:Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;

    iget-object v9, p0, Lcom/pollfish/f/a;->y:Ljava/lang/String;

    iget v10, p0, Lcom/pollfish/f/a;->z:I

    iget-object v11, p0, Lcom/pollfish/f/a;->A:Ljava/lang/String;

    iget-object v12, p0, Lcom/pollfish/f/a;->B:Landroid/view/ViewGroup;

    move v2, p1

    invoke-static/range {v0 .. v12}, Lcom/pollfish/f/a/b;->a(Landroid/app/Activity;Lcom/pollfish/a/b;ZLcom/pollfish/interfaces/a$c;Lcom/pollfish/interfaces/a$b;Lcom/pollfish/interfaces/PollfishSurveyCompletedListener;Lcom/pollfish/interfaces/PollfishOpenedListener;Lcom/pollfish/interfaces/PollfishClosedListener;Lcom/pollfish/interfaces/PollfishUserNotEligibleListener;Ljava/lang/String;ILjava/lang/String;Landroid/view/ViewGroup;)Lcom/pollfish/d/a;

    move-result-object v0

    iput-object v0, p0, Lcom/pollfish/f/a;->f:Lcom/pollfish/d/a;

    const-string v0, "LifeCycle"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "serverToConnectUrl: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/f/a;->y:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/f/a;->f:Lcom/pollfish/d/a;

    if-eqz v0, :cond_0

    const-string v0, "LifeCycle"

    const-string v1, "overlayLayout != null"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/f/a;->b:Lcom/pollfish/interfaces/a$f;

    iget-object v1, p0, Lcom/pollfish/f/a;->f:Lcom/pollfish/d/a;

    invoke-interface {v0, v1}, Lcom/pollfish/interfaces/a$f;->a(Lcom/pollfish/d/a;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "LifeCycle"

    const-string v1, "overlayLayout == null"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "LifeCycle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error LayoutUtils.createLayout: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/pollfish/f/a;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/pollfish/f/a;->r:Z

    return p1
.end method

.method static synthetic b(Lcom/pollfish/f/a;I)I
    .locals 0

    iput p1, p0, Lcom/pollfish/f/a;->n:I

    return p1
.end method

.method static synthetic b(Lcom/pollfish/f/a;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/pollfish/f/a;->r:Z

    return v0
.end method

.method static synthetic b(Lcom/pollfish/f/a;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/pollfish/f/a;->q:Z

    return p1
.end method

.method static synthetic c(Lcom/pollfish/f/a;)I
    .locals 1

    iget v0, p0, Lcom/pollfish/f/a;->l:I

    return v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/pollfish/f/a;->p:Ljava/lang/String;

    return-object v0
.end method

.method private c(Lcom/pollfish/a/a;)V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "LifeCycle"

    const-string v1, "panelObject != null - canceling timer - moving to proceedPanel"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/pollfish/f/a$6;

    invoke-direct {v0, p0, p1}, Lcom/pollfish/f/a$6;-><init>(Lcom/pollfish/f/a;Lcom/pollfish/a/a;)V

    iget-object v1, p0, Lcom/pollfish/f/a;->c:Landroid/app/Activity;

    invoke-static {v1, v0, v2}, Lcom/pollfish/f/c;->a(Landroid/content/Context;Ljava/lang/Runnable;I)V

    iget-object v0, p0, Lcom/pollfish/f/a;->o:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pollfish/f/a;->o:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/pollfish/f/a;->o:Ljava/util/Timer;

    :cond_0
    iput v2, p0, Lcom/pollfish/f/a;->n:I

    return-void
.end method

.method static synthetic d(Lcom/pollfish/f/a;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/pollfish/f/a;->q:Z

    return v0
.end method

.method static synthetic e(Lcom/pollfish/f/a;)I
    .locals 2

    iget v0, p0, Lcom/pollfish/f/a;->l:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/pollfish/f/a;->l:I

    return v0
.end method

.method static synthetic f(Lcom/pollfish/f/a;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/f/a;->m:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic g(Lcom/pollfish/f/a;)Lcom/pollfish/a/b;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/f/a;->d:Lcom/pollfish/a/b;

    return-object v0
.end method

.method static synthetic h(Lcom/pollfish/f/a;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/f/a;->y:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/pollfish/f/a;)I
    .locals 1

    iget v0, p0, Lcom/pollfish/f/a;->z:I

    return v0
.end method

.method static synthetic j(Lcom/pollfish/f/a;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/f/a;->A:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/pollfish/f/a;)Lcom/pollfish/interfaces/a$c;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/f/a;->F:Lcom/pollfish/interfaces/a$c;

    return-object v0
.end method

.method static synthetic l(Lcom/pollfish/f/a;)Lcom/pollfish/interfaces/a$d;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/f/a;->G:Lcom/pollfish/interfaces/a$d;

    return-object v0
.end method

.method static synthetic m(Lcom/pollfish/f/a;)Lcom/pollfish/interfaces/a$b;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/f/a;->C:Lcom/pollfish/interfaces/a$b;

    return-object v0
.end method

.method static synthetic n(Lcom/pollfish/f/a;)Lcom/pollfish/a/a;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/f/a;->e:Lcom/pollfish/a/a;

    return-object v0
.end method

.method static synthetic o(Lcom/pollfish/f/a;)Lcom/pollfish/interfaces/PollfishSurveyReceivedListener;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/f/a;->s:Lcom/pollfish/interfaces/PollfishSurveyReceivedListener;

    return-object v0
.end method

.method static synthetic p(Lcom/pollfish/f/a;)Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/f/a;->t:Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;

    return-object v0
.end method

.method static synthetic q(Lcom/pollfish/f/a;)Lcom/pollfish/interfaces/a$e;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/f/a;->D:Lcom/pollfish/interfaces/a$e;

    return-object v0
.end method

.method static synthetic r(Lcom/pollfish/f/a;)Lcom/pollfish/d/a;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/f/a;->f:Lcom/pollfish/d/a;

    return-object v0
.end method

.method static synthetic s(Lcom/pollfish/f/a;)I
    .locals 1

    iget v0, p0, Lcom/pollfish/f/a;->n:I

    return v0
.end method

.method static synthetic t(Lcom/pollfish/f/a;)Ljava/util/Timer;
    .locals 1

    iget-object v0, p0, Lcom/pollfish/f/a;->o:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic u(Lcom/pollfish/f/a;)I
    .locals 2

    iget v0, p0, Lcom/pollfish/f/a;->n:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/pollfish/f/a;->n:I

    return v0
.end method


# virtual methods
.method public a()V
    .locals 7

    const-string v0, "LifeCycle"

    const-string v1, "Pollfish checkAdIdAvailability()..."

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "LifeCycle"

    const-string v1, "check if overlay view already exists (changed app hierarchy before)"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    const-string v0, "LifeCycle"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "userLayout: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/f/a;->B:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/f/a;->c:Landroid/app/Activity;

    iget-object v1, p0, Lcom/pollfish/f/a;->B:Landroid/view/ViewGroup;

    invoke-static {v0, v1}, Lcom/pollfish/f/a/b;->a(Landroid/app/Activity;Landroid/view/ViewGroup;)Lcom/pollfish/d/a;

    move-result-object v0

    iput-object v0, p0, Lcom/pollfish/f/a;->f:Lcom/pollfish/d/a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lcom/pollfish/f/a;->f:Lcom/pollfish/d/a;

    if-eqz v0, :cond_0

    const-string v0, "LifeCycle"

    const-string v1, "existing overlay found - reset view hierarchy to initial state"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_1
    iget-object v0, p0, Lcom/pollfish/f/a;->f:Lcom/pollfish/d/a;

    invoke-static {v0}, Lcom/pollfish/f/a/b;->a(Lcom/pollfish/d/a;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    const-string v0, "LifeCycle"

    const-string v1, "Loading Pollfish parameters.."

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/pollfish/a/b;

    iget-object v1, p0, Lcom/pollfish/f/a;->c:Landroid/app/Activity;

    iget-object v2, p0, Lcom/pollfish/f/a;->g:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/pollfish/f/a;->k:Z

    iget-object v4, p0, Lcom/pollfish/f/a;->h:Lcom/pollfish/constants/Position;

    iget v5, p0, Lcom/pollfish/f/a;->i:I

    iget-boolean v6, p0, Lcom/pollfish/f/a;->j:Z

    invoke-direct/range {v0 .. v6}, Lcom/pollfish/a/b;-><init>(Landroid/app/Activity;Ljava/lang/String;ZLcom/pollfish/constants/Position;IZ)V

    iput-object v0, p0, Lcom/pollfish/f/a;->d:Lcom/pollfish/a/b;

    const-string v0, "LifeCycle"

    const-string v1, "Pollfish parameters loaded"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "LifeCycle"

    const-string v1, "Re-aranging view hierarchy"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_2
    invoke-virtual {p0}, Lcom/pollfish/f/a;->b()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    const-string v0, "LifeCycle"

    const-string v1, "Check advertising id..."

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/pollfish/c/a;

    iget-object v1, p0, Lcom/pollfish/f/a;->c:Landroid/app/Activity;

    iget-object v2, p0, Lcom/pollfish/f/a;->E:Lcom/pollfish/interfaces/a$a;

    invoke-direct {v0, v1, v2}, Lcom/pollfish/c/a;-><init>(Landroid/app/Activity;Lcom/pollfish/interfaces/a$a;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/pollfish/c/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_3
    return-void

    :catch_0
    move-exception v0

    const-string v1, "LifeCycle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error while getExistingOverlay"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "LifeCycle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bringAppViewsToPriorOverlayState error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_0
    const-string v0, "LifeCycle"

    const-string v1, "no existing overlay found"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v1, "LifeCycle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error in createViewsLayout: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public a(Lcom/pollfish/a/a;)V
    .locals 6

    const/4 v5, 0x0

    if-eqz p1, :cond_1

    :try_start_0
    invoke-direct {p0, p1}, Lcom/pollfish/f/a;->c(Lcom/pollfish/a/a;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "LifeCycle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "timer: e: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/f/a;->o:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pollfish/f/a;->o:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/pollfish/f/a;->o:Ljava/util/Timer;

    :cond_0
    iput v5, p0, Lcom/pollfish/f/a;->n:I

    goto :goto_0

    :cond_1
    const/16 v4, 0x32

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/pollfish/f/a;->o:Ljava/util/Timer;

    iget-object v0, p0, Lcom/pollfish/f/a;->o:Ljava/util/Timer;

    new-instance v1, Lcom/pollfish/f/a$7;

    invoke-direct {v1, p0, p1}, Lcom/pollfish/f/a$7;-><init>(Lcom/pollfish/f/a;Lcom/pollfish/a/a;)V

    int-to-long v2, v5

    int-to-long v4, v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 12

    const/4 v9, 0x0

    const/4 v8, 0x0

    const-string v0, "LifeCycle"

    const-string v1, "Pollfish lifecycle begin()..."

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/pollfish/f/a;->d:Lcom/pollfish/a/b;

    invoke-virtual {v0, p1}, Lcom/pollfish/a/b;->d(Ljava/lang/String;)V

    const-string v0, "LifeCycle"

    const-string v1, "read assets and copy to internal cache or download if needed"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/pollfish/c/i;

    iget-object v1, p0, Lcom/pollfish/f/a;->C:Lcom/pollfish/interfaces/a$b;

    iget-object v2, p0, Lcom/pollfish/f/a;->c:Landroid/app/Activity;

    invoke-direct {v0, v1, v2}, Lcom/pollfish/c/i;-><init>(Lcom/pollfish/interfaces/a$b;Landroid/app/Activity;)V

    new-array v1, v8, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/pollfish/c/i;->c([Ljava/lang/Object;)Lcom/pollfish/c/h;

    const-string v0, "LifeCycle"

    const-string v1, "check queue..."

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/f/a;->c:Landroid/app/Activity;

    const-string v1, "pollfish_pref_queue"

    invoke-static {v0, v1}, Lcom/pollfish/f/c;->a(Landroid/app/Activity;Ljava/lang/String;)I

    move-result v0

    const-string v1, "LifeCycle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "number of times tried to empty queue with no result: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    const-string v0, "LifeCycle"

    const-string v1, "check queue, not tried too many times yet - try again"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/pollfish/c/c;

    iget-object v1, p0, Lcom/pollfish/f/a;->d:Lcom/pollfish/a/b;

    iget-object v2, p0, Lcom/pollfish/f/a;->F:Lcom/pollfish/interfaces/a$c;

    iget-object v3, p0, Lcom/pollfish/f/a;->D:Lcom/pollfish/interfaces/a$e;

    iget-object v4, p0, Lcom/pollfish/f/a;->c:Landroid/app/Activity;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/pollfish/c/c;-><init>(Lcom/pollfish/a/b;Lcom/pollfish/interfaces/a$c;Lcom/pollfish/interfaces/a$e;Landroid/app/Activity;)V

    new-array v1, v8, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/pollfish/c/c;->c([Ljava/lang/Object;)Lcom/pollfish/c/h;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "LifeCycle"

    const-string v1, "clearing queue after multiple attends to clear the queue"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/pollfish/c/d;

    iget-object v1, p0, Lcom/pollfish/f/a;->c:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/pollfish/c/d;-><init>(Landroid/app/Activity;)V

    new-array v1, v8, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/pollfish/c/d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/pollfish/f/a;->c:Landroid/app/Activity;

    const-string v1, "pollfish_pref_queue"

    invoke-static {v0, v1, v8}, Lcom/pollfish/f/c;->a(Landroid/app/Activity;Ljava/lang/String;I)V

    const-string v0, "LifeCycle"

    const-string v1, "registering..."

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/pollfish/c/e;

    iget-object v1, p0, Lcom/pollfish/f/a;->d:Lcom/pollfish/a/b;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/pollfish/f/a;->y:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/v2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/device/register"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/pollfish/f/a;->z:I

    iget-object v4, p0, Lcom/pollfish/f/a;->A:Ljava/lang/String;

    iget-object v5, p0, Lcom/pollfish/f/a;->d:Lcom/pollfish/a/b;

    invoke-static {v5}, Lcom/pollfish/f/c;->a(Lcom/pollfish/a/b;)Lorg/json/JSONObject;

    move-result-object v5

    iget-object v6, p0, Lcom/pollfish/f/a;->F:Lcom/pollfish/interfaces/a$c;

    iget-object v7, p0, Lcom/pollfish/f/a;->c:Landroid/app/Activity;

    move-object v10, v9

    move-object v11, v9

    invoke-direct/range {v0 .. v11}, Lcom/pollfish/c/e;-><init>(Lcom/pollfish/a/b;Ljava/lang/String;ILjava/lang/String;Lorg/json/JSONObject;Lcom/pollfish/interfaces/a$c;Landroid/app/Activity;ZLjava/lang/String;Lcom/pollfish/a/a;Ljava/lang/String;)V

    new-array v1, v8, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/pollfish/c/e;->c([Ljava/lang/Object;)Lcom/pollfish/c/h;

    goto :goto_0
.end method

.method public b()V
    .locals 2

    const-string v0, "LifeCycle"

    const-string v1, "createViewsLayout()"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    const-string v0, "LifeCycle"

    const-string v1, "call to re-order layouts with overlay view inside"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-boolean v0, Lcom/pollfish/f/a;->a:Z

    invoke-direct {p0, v0}, Lcom/pollfish/f/a;->a(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "LifeCycle"

    const-string v1, "error in reOrderLayouts after custom indicator check fired"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Lcom/pollfish/a/a;)V
    .locals 0

    iput-object p1, p0, Lcom/pollfish/f/a;->e:Lcom/pollfish/a/a;

    return-void
.end method
