.class Lcom/pollfish/f/a$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/pollfish/interfaces/a$e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pollfish/f/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pollfish/f/a;


# direct methods
.method constructor <init>(Lcom/pollfish/f/a;)V
    .locals 0

    iput-object p1, p0, Lcom/pollfish/f/a$2;->a:Lcom/pollfish/f/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 12

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-string v0, "LifeCycle"

    const-string v1, "onReturnFromQueueProceedToRegister..."

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "LifeCycle"

    const-string v1, "registering..."

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/pollfish/c/e;

    iget-object v1, p0, Lcom/pollfish/f/a$2;->a:Lcom/pollfish/f/a;

    invoke-static {v1}, Lcom/pollfish/f/a;->g(Lcom/pollfish/f/a;)Lcom/pollfish/a/b;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/pollfish/f/a$2;->a:Lcom/pollfish/f/a;

    invoke-static {v3}, Lcom/pollfish/f/a;->h(Lcom/pollfish/f/a;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/v2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/device/register"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/pollfish/f/a$2;->a:Lcom/pollfish/f/a;

    invoke-static {v3}, Lcom/pollfish/f/a;->i(Lcom/pollfish/f/a;)I

    move-result v3

    iget-object v4, p0, Lcom/pollfish/f/a$2;->a:Lcom/pollfish/f/a;

    invoke-static {v4}, Lcom/pollfish/f/a;->j(Lcom/pollfish/f/a;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/pollfish/f/a$2;->a:Lcom/pollfish/f/a;

    invoke-static {v5}, Lcom/pollfish/f/a;->g(Lcom/pollfish/f/a;)Lcom/pollfish/a/b;

    move-result-object v5

    invoke-static {v5}, Lcom/pollfish/f/c;->a(Lcom/pollfish/a/b;)Lorg/json/JSONObject;

    move-result-object v5

    iget-object v6, p0, Lcom/pollfish/f/a$2;->a:Lcom/pollfish/f/a;

    invoke-static {v6}, Lcom/pollfish/f/a;->k(Lcom/pollfish/f/a;)Lcom/pollfish/interfaces/a$c;

    move-result-object v6

    iget-object v7, p0, Lcom/pollfish/f/a$2;->a:Lcom/pollfish/f/a;

    invoke-static {v7}, Lcom/pollfish/f/a;->a(Lcom/pollfish/f/a;)Landroid/app/Activity;

    move-result-object v7

    move-object v10, v9

    move-object v11, v9

    invoke-direct/range {v0 .. v11}, Lcom/pollfish/c/e;-><init>(Lcom/pollfish/a/b;Ljava/lang/String;ILjava/lang/String;Lorg/json/JSONObject;Lcom/pollfish/interfaces/a$c;Landroid/app/Activity;ZLjava/lang/String;Lcom/pollfish/a/a;Ljava/lang/String;)V

    new-array v1, v8, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/pollfish/c/e;->c([Ljava/lang/Object;)Lcom/pollfish/c/h;

    return-void
.end method
