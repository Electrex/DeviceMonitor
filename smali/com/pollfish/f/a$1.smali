.class Lcom/pollfish/f/a$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/pollfish/interfaces/a$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pollfish/f/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pollfish/f/a;


# direct methods
.method constructor <init>(Lcom/pollfish/f/a;)V
    .locals 0

    iput-object p1, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    invoke-static {}, Lcom/pollfish/f/a;->c()Ljava/lang/String;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->c(Lcom/pollfish/f/a;)I

    move-result v0

    const/4 v2, 0x1

    if-lt v0, v2, :cond_0

    iget-object v0, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->e(Lcom/pollfish/f/a;)I

    :goto_0
    const-string v0, "LifeCycle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "assetsAndCacheListener: updateNumOfTranfers...numOfAssetsLeftToBeCopiedInCache: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v3}, Lcom/pollfish/f/a;->c(Lcom/pollfish/f/a;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    return-void

    :cond_0
    iget-object v0, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/pollfish/f/a;->a(Lcom/pollfish/f/a;I)I

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(I)V
    .locals 4

    invoke-static {}, Lcom/pollfish/f/a;->c()Ljava/lang/String;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/pollfish/f/a;->a(Lcom/pollfish/f/a;Z)Z

    const-string v0, "LifeCycle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "assetsAndCacheListener: checkedAfterJSONIfTransferAssets: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v3}, Lcom/pollfish/f/a;->b(Lcom/pollfish/f/a;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v0, p1}, Lcom/pollfish/f/a;->a(Lcom/pollfish/f/a;I)I

    const-string v0, "LifeCycle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "assetsAndCacheListener: onNumOfTranfers...assetsLeftToBeCopiedInCache: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v3}, Lcom/pollfish/f/a;->c(Lcom/pollfish/f/a;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/pollfish/a/c;)V
    .locals 4

    invoke-static {}, Lcom/pollfish/f/a;->c()Ljava/lang/String;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    const-string v0, "LifeCycle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onTransferFromMemoryFinished: failed to write in cache file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/pollfish/a/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/pollfish/a/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/pollfish/a/c;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/pollfish/f/a$1;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/pollfish/c/f;

    iget-object v2, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->a(Lcom/pollfish/f/a;)Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, p1, v2, p0}, Lcom/pollfish/c/f;-><init>(Lcom/pollfish/a/c;Landroid/app/Activity;Lcom/pollfish/interfaces/a$b;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Lcom/pollfish/c/f;->c([Ljava/lang/Object;)Lcom/pollfish/c/h;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    const-string v0, "LifeCycle"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "assetsAndCacheListener: deleteFailedTransferAndDownload: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/pollfish/f/a;->c()Ljava/lang/String;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->f(Lcom/pollfish/f/a;)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->f(Lcom/pollfish/f/a;)Ljava/util/HashMap;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "LifeCycle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "assetsAndCacheListener: urlsToBeReplaceInHtmlOfWebview.size(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v3}, Lcom/pollfish/f/a;->f(Lcom/pollfish/f/a;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    :try_start_1
    monitor-exit v1

    return-void

    :catch_0
    move-exception v0

    const-string v2, "LifeCycle"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteFileDownloadedSucessfully error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    invoke-static {}, Lcom/pollfish/f/a;->c()Ljava/lang/String;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    const-string v0, "LifeCycle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "assetsAndCacheListener: addUrlsToBeDownloadedOrReplacedLater: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " toUrl: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->f(Lcom/pollfish/f/a;)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->f(Lcom/pollfish/f/a;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const-string v0, "LifeCycle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "assetsAndCacheListener: urlsToBeReplaceInHtmlOfWebview.size(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v3}, Lcom/pollfish/f/a;->f(Lcom/pollfish/f/a;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()Ljava/util/HashMap;
    .locals 5

    const/4 v0, 0x0

    invoke-static {}, Lcom/pollfish/f/a;->c()Ljava/lang/String;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    const-string v2, "LifeCycle"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkedAfterRegisterIfDownloadAssets: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v4}, Lcom/pollfish/f/a;->d(Lcom/pollfish/f/a;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " checkedAfterJSONIfTransferAssets: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v4}, Lcom/pollfish/f/a;->b(Lcom/pollfish/f/a;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->d(Lcom/pollfish/f/a;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->b(Lcom/pollfish/f/a;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->f(Lcom/pollfish/f/a;)Ljava/util/HashMap;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v2, "LifeCycle"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "assetsAndCacheListener: shouldProceedOrReplace?  urlsToBeReplaceInHtmlOfWebview.size(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v4}, Lcom/pollfish/f/a;->f(Lcom/pollfish/f/a;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " assetsLeftToBeCopiedInCache: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v4}, Lcom/pollfish/f/a;->c(Lcom/pollfish/f/a;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->c(Lcom/pollfish/f/a;)I

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->f(Lcom/pollfish/f/a;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-lez v2, :cond_1

    const-string v0, "LifeCycle"

    const-string v2, "You should replace the urls in html!"

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->f(Lcom/pollfish/f/a;)Ljava/util/HashMap;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->c(Lcom/pollfish/f/a;)I

    move-result v2

    if-lez v2, :cond_2

    const-string v2, "LifeCycle"

    const-string v3, "Something is being downloaded/copied - you should wait"

    invoke-static {v2, v3}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->f(Lcom/pollfish/f/a;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    const-string v0, "LifeCycle"

    const-string v2, "Everything is finished - proceed to show Pollfish panel"

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public b(I)V
    .locals 4

    invoke-static {}, Lcom/pollfish/f/a;->c()Ljava/lang/String;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    iget-object v2, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->c(Lcom/pollfish/f/a;)I

    move-result v2

    add-int/2addr v2, p1

    invoke-static {v0, v2}, Lcom/pollfish/f/a;->a(Lcom/pollfish/f/a;I)I

    const-string v0, "LifeCycle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "assetsAndCacheListener: increaseNumOfAssetsLeftToBeDownloadedBy: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v3}, Lcom/pollfish/f/a;->c(Lcom/pollfish/f/a;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()V
    .locals 4

    invoke-static {}, Lcom/pollfish/f/a;->c()Ljava/lang/String;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/pollfish/f/a;->b(Lcom/pollfish/f/a;Z)Z

    const-string v0, "LifeCycle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "assetsAndCacheListener: checkedAfterRegisterIfDownloadAssets: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/pollfish/f/a$1;->a:Lcom/pollfish/f/a;

    invoke-static {v3}, Lcom/pollfish/f/a;->d(Lcom/pollfish/f/a;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
