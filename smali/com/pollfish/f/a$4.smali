.class Lcom/pollfish/f/a$4;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/pollfish/interfaces/a$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pollfish/f/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pollfish/f/a;


# direct methods
.method constructor <init>(Lcom/pollfish/f/a;)V
    .locals 0

    iput-object p1, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/pollfish/a/d;)V
    .locals 14

    const-string v0, "LifeCycle"

    const-string v1, "onContactServerFinished..."

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_d

    invoke-virtual {p1}, Lcom/pollfish/a/d;->h()Z

    move-result v0

    if-nez v0, :cond_d

    const-string v0, "LifeCycle"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onContactServerFinished with SurveyResponse status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/pollfish/a/d;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "LifeCycle"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onContactServerFinished with SurveyResponse hasAcceptedTerms: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/pollfish/a/d;->f()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "LifeCycle"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "time out happened: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/pollfish/a/d;->h()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/pollfish/a/d;->f()Z

    move-result v12

    invoke-virtual {p1}, Lcom/pollfish/a/d;->a()I

    move-result v13

    invoke-virtual {p1}, Lcom/pollfish/a/d;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->h(Lcom/pollfish/f/a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/v2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/device/register"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xc8

    if-eq v13, v0, :cond_0

    const/16 v0, 0xcc

    if-ne v13, v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/pollfish/a/d;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/pollfish/a/d;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/pollfish/a/d;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    const-string v0, "LifeCycle"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "User has accepted terms and conditions - send User Consent Data: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/pollfish/a/d;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "LifeCycle"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sent at url: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->h(Lcom/pollfish/f/a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/v2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/device/info"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/pollfish/c/e;

    iget-object v1, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v1}, Lcom/pollfish/f/a;->g(Lcom/pollfish/f/a;)Lcom/pollfish/a/b;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v3}, Lcom/pollfish/f/a;->h(Lcom/pollfish/f/a;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/v2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/device/info"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v3}, Lcom/pollfish/f/a;->i(Lcom/pollfish/f/a;)I

    move-result v3

    iget-object v4, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v4}, Lcom/pollfish/f/a;->j(Lcom/pollfish/f/a;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v6}, Lcom/pollfish/f/a;->k(Lcom/pollfish/f/a;)Lcom/pollfish/interfaces/a$c;

    move-result-object v6

    iget-object v7, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v7}, Lcom/pollfish/f/a;->a(Lcom/pollfish/f/a;)Landroid/app/Activity;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {p1}, Lcom/pollfish/a/d;->g()Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {v0 .. v11}, Lcom/pollfish/c/e;-><init>(Lcom/pollfish/a/b;Ljava/lang/String;ILjava/lang/String;Lorg/json/JSONObject;Lcom/pollfish/interfaces/a$c;Landroid/app/Activity;ZLjava/lang/String;Lcom/pollfish/a/a;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/pollfish/c/e;->c([Ljava/lang/Object;)Lcom/pollfish/c/h;

    :cond_1
    const/16 v0, 0xc8

    if-ne v13, v0, :cond_8

    invoke-virtual {p1}, Lcom/pollfish/a/d;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->h(Lcom/pollfish/f/a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/v2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/device/register"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v6, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-virtual {p1}, Lcom/pollfish/a/d;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v1}, Lcom/pollfish/f/a;->g(Lcom/pollfish/f/a;)Lcom/pollfish/a/b;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->l(Lcom/pollfish/f/a;)Lcom/pollfish/interfaces/a$d;

    move-result-object v2

    iget-object v3, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v3}, Lcom/pollfish/f/a;->m(Lcom/pollfish/f/a;)Lcom/pollfish/interfaces/a$b;

    move-result-object v3

    iget-object v4, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v4}, Lcom/pollfish/f/a;->a(Lcom/pollfish/f/a;)Landroid/app/Activity;

    move-result-object v4

    move v5, v12

    invoke-static/range {v0 .. v5}, Lcom/pollfish/f/c;->a(Ljava/lang/String;Lcom/pollfish/a/b;Lcom/pollfish/interfaces/a$d;Lcom/pollfish/interfaces/a$b;Landroid/app/Activity;Z)Lcom/pollfish/a/a;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/pollfish/f/a;->b(Lcom/pollfish/a/a;)V

    const-string v0, "LifeCycle"

    const-string v1, "ContactServerFinishedListener: returned from a register request"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->a(Lcom/pollfish/f/a;)Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/pollfish/interfaces/PollfishSurveyReceivedListener;

    if-eqz v0, :cond_6

    const-string v0, "LifeCycle"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ContactServerFinishedListener: Survey or award is there : isShortSurvey "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->n(Lcom/pollfish/f/a;)Lcom/pollfish/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pollfish/a/a;->m()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " survey Price: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->n(Lcom/pollfish/f/a;)Lcom/pollfish/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pollfish/a/a;->n()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->a(Lcom/pollfish/f/a;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/pollfish/interfaces/PollfishSurveyReceivedListener;

    iget-object v1, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v1}, Lcom/pollfish/f/a;->n(Lcom/pollfish/f/a;)Lcom/pollfish/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pollfish/a/a;->m()Z

    move-result v1

    iget-object v2, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->n(Lcom/pollfish/f/a;)Lcom/pollfish/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pollfish/a/a;->n()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/pollfish/interfaces/PollfishSurveyReceivedListener;->onPollfishSurveyReceived(ZI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    const-string v0, "LifeCycle"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "surveyResponse.getServerUrl(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/pollfish/a/d;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0xc8

    if-eq v13, v0, :cond_3

    const/16 v0, 0x196

    if-eq v13, v0, :cond_3

    invoke-virtual {p1}, Lcom/pollfish/a/d;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->h(Lcom/pollfish/f/a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/v2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/device/register"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :try_start_1
    iget-object v0, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->a(Lcom/pollfish/f/a;)Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;

    if-eqz v0, :cond_c

    const-string v0, "LifeCycle"

    const-string v1, "firing PollfishSurveyNotAvailableListener after registering"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->a(Lcom/pollfish/f/a;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;

    invoke-interface {v0}, Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;->onPollfishSurveyNotAvailable()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_3
    :goto_1
    invoke-virtual {p1}, Lcom/pollfish/a/d;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0xc8

    if-eq v13, v0, :cond_4

    const/16 v0, 0xcc

    if-ne v13, v0, :cond_5

    :cond_4
    invoke-virtual {p1}, Lcom/pollfish/a/d;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->h(Lcom/pollfish/f/a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/v2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/device/register"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "LifeCycle"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "try to delete file with name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/pollfish/a/d;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from queue on sucessful sent"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/pollfish/c/b;

    invoke-virtual {p1}, Lcom/pollfish/a/d;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->a(Lcom/pollfish/f/a;)Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v3}, Lcom/pollfish/f/a;->q(Lcom/pollfish/f/a;)Lcom/pollfish/interfaces/a$e;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/pollfish/c/b;-><init>(Ljava/lang/String;Landroid/app/Activity;Lcom/pollfish/interfaces/a$e;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/pollfish/c/b;->c([Ljava/lang/Object;)Lcom/pollfish/c/h;

    :cond_5
    :goto_2
    return-void

    :cond_6
    :try_start_2
    iget-object v0, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->o(Lcom/pollfish/f/a;)Lcom/pollfish/interfaces/PollfishSurveyReceivedListener;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v0, "LifeCycle"

    const-string v1, "ContactServerFinishedListener: Survey or award is there and this.pollfishSurveyReceivedListenerExt!=null"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "LifeCycle"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ContactServerFinishedListener: panelObj.isShortSurvey(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->n(Lcom/pollfish/f/a;)Lcom/pollfish/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pollfish/a/a;->m()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " survey Price: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->n(Lcom/pollfish/f/a;)Lcom/pollfish/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pollfish/a/a;->n()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->o(Lcom/pollfish/f/a;)Lcom/pollfish/interfaces/PollfishSurveyReceivedListener;

    move-result-object v0

    iget-object v1, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v1}, Lcom/pollfish/f/a;->n(Lcom/pollfish/f/a;)Lcom/pollfish/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pollfish/a/a;->m()Z

    move-result v1

    iget-object v2, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->n(Lcom/pollfish/f/a;)Lcom/pollfish/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pollfish/a/a;->n()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/pollfish/interfaces/PollfishSurveyReceivedListener;->onPollfishSurveyReceived(ZI)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v0, "LifeCycle"

    const-string v1, "Could not notify listener on survey received"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const-string v0, "LifeCycle"

    const-string v1, "ContactServerFinishedListener: Response from server not from registering"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    const/16 v0, 0xcc

    if-ne v13, v0, :cond_9

    const-string v0, "LifeCycle"

    const-string v1, "ContactServerFinishedListener: Nothing to show"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    const/16 v0, 0x190

    if-ne v13, v0, :cond_a

    const-string v0, "LifeCycle"

    const-string v1, "ContactServerFinishedListener: Wrong or Bad arguments"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    const/16 v0, 0x191

    if-ne v13, v0, :cond_b

    const-string v0, "LifeCycle"

    const-string v1, "ContactServerFinishedListener: Encryption is wrong"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    const/16 v0, 0x196

    if-ne v13, v0, :cond_2

    const-string v0, "Pollfish"

    const-string v1, "Wrong Pollfish API Key"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_c
    :try_start_3
    iget-object v0, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->p(Lcom/pollfish/f/a;)Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v0, "LifeCycle"

    const-string v1, "firing PollfishSurveyNotAvailableListener after registering pollfishSurveyNotAvailableListenerExt!=null"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->p(Lcom/pollfish/f/a;)Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;->onPollfishSurveyNotAvailable()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_1

    :catch_1
    move-exception v0

    const-string v1, "LifeCycle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ContactServerFinishedListener: Could not notify listener on nothing to show: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_d
    if-eqz p1, :cond_f

    invoke-virtual {p1}, Lcom/pollfish/a/d;->h()Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "LifeCycle"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "time out happened: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/pollfish/a/d;->h()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_4
    iget-object v0, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->a(Lcom/pollfish/f/a;)Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;

    if-eqz v0, :cond_e

    const-string v0, "LifeCycle"

    const-string v1, "firing PollfishSurveyNotAvailableListener after registering"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->a(Lcom/pollfish/f/a;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;

    invoke-interface {v0}, Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;->onPollfishSurveyNotAvailable()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_2

    :catch_2
    move-exception v0

    const-string v1, "LifeCycle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ContactServerFinishedListener: Could not notify listener on nothing to show: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_e
    :try_start_5
    iget-object v0, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->p(Lcom/pollfish/f/a;)Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v0, "LifeCycle"

    const-string v1, "firing PollfishSurveyNotAvailableListener after registering pollfishSurveyNotAvailableListenerExt!=null"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/f/a$4;->a:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->p(Lcom/pollfish/f/a;)Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/pollfish/interfaces/PollfishSurveyNotAvailableListener;->onPollfishSurveyNotAvailable()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_2

    :cond_f
    const-string v0, "LifeCycle"

    const-string v1, "ContactServerFinishedListener: Nothing to show - no server response (serverResponse = null) "

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method
