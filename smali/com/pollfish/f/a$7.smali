.class Lcom/pollfish/f/a$7;
.super Ljava/util/TimerTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pollfish/f/a;->a(Lcom/pollfish/a/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pollfish/a/a;

.field final synthetic b:Lcom/pollfish/f/a;


# direct methods
.method constructor <init>(Lcom/pollfish/f/a;Lcom/pollfish/a/a;)V
    .locals 0

    iput-object p1, p0, Lcom/pollfish/f/a$7;->b:Lcom/pollfish/f/a;

    iput-object p2, p0, Lcom/pollfish/f/a$7;->a:Lcom/pollfish/a/a;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    const-string v0, "LifeCycle"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checking if panelObject!=null after coming from registering timerTimes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/f/a$7;->b:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->s(Lcom/pollfish/f/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/pollfish/f/a$7;->a:Lcom/pollfish/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pollfish/f/a$7;->b:Lcom/pollfish/f/a;

    iget-object v1, p0, Lcom/pollfish/f/a$7;->a:Lcom/pollfish/a/a;

    invoke-static {v0, v1}, Lcom/pollfish/f/a;->a(Lcom/pollfish/f/a;Lcom/pollfish/a/a;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/pollfish/f/a$7;->a:Lcom/pollfish/a/a;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/pollfish/f/a$7;->b:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->s(Lcom/pollfish/f/a;)I

    move-result v0

    const/16 v1, 0x64

    if-le v0, v1, :cond_3

    iget-object v0, p0, Lcom/pollfish/f/a$7;->b:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->t(Lcom/pollfish/f/a;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pollfish/f/a$7;->b:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->t(Lcom/pollfish/f/a;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iget-object v0, p0, Lcom/pollfish/f/a$7;->b:Lcom/pollfish/f/a;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/pollfish/f/a;->a(Lcom/pollfish/f/a;Ljava/util/Timer;)Ljava/util/Timer;

    :cond_1
    iget-object v0, p0, Lcom/pollfish/f/a$7;->b:Lcom/pollfish/f/a;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/pollfish/f/a;->b(Lcom/pollfish/f/a;I)I

    const-string v0, "LifeCycle"

    const-string v1, "panelObject == null - canceling timer - tried too many times - timerTimes>100"

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "LifeCycle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "timer: e: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/pollfish/f/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pollfish/f/a$7;->b:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->t(Lcom/pollfish/f/a;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pollfish/f/a$7;->b:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->t(Lcom/pollfish/f/a;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iget-object v0, p0, Lcom/pollfish/f/a$7;->b:Lcom/pollfish/f/a;

    invoke-static {v0, v5}, Lcom/pollfish/f/a;->a(Lcom/pollfish/f/a;Ljava/util/Timer;)Ljava/util/Timer;

    :cond_2
    iget-object v0, p0, Lcom/pollfish/f/a$7;->b:Lcom/pollfish/f/a;

    invoke-static {v0, v4}, Lcom/pollfish/f/a;->b(Lcom/pollfish/f/a;I)I

    goto :goto_0

    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/pollfish/f/a$7;->b:Lcom/pollfish/f/a;

    invoke-static {v0}, Lcom/pollfish/f/a;->u(Lcom/pollfish/f/a;)I

    const-string v0, "LifeCycle"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "panelObject == null - trying again for timerTimes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/pollfish/f/a$7;->b:Lcom/pollfish/f/a;

    invoke-static {v2}, Lcom/pollfish/f/a;->s(Lcom/pollfish/f/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pollfish/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
