.class public final Lcom/stericson/roottools/internal/RootToolsInternalMethods;
.super Ljava/lang/Object;
.source "RootToolsInternalMethods.java"


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private commandWait(Lcom/stericson/roottools/execution/Command;)V
    .locals 4
    .param p1, "cmd"    # Lcom/stericson/roottools/execution/Command;

    .prologue
    .line 1017
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/stericson/roottools/execution/Command;->isFinished()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1018
    const-string v1, "RootTools v3.4"

    invoke-static {}, Lcom/stericson/roottools/execution/Shell;->getOpenShell()Lcom/stericson/roottools/execution/Shell;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/stericson/roottools/execution/Shell;->getCommandQueuePositionString(Lcom/stericson/roottools/execution/Command;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 1021
    monitor-enter p1

    .line 1023
    :try_start_0
    invoke-virtual {p1}, Lcom/stericson/roottools/execution/Command;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1024
    const-wide/16 v2, 0x7d0

    invoke-virtual {p1, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1027
    :cond_1
    :goto_1
    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1029
    invoke-virtual {p1}, Lcom/stericson/roottools/execution/Command;->isExecuting()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/stericson/roottools/execution/Command;->isFinished()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1030
    sget-boolean v1, Lcom/stericson/roottools/execution/Shell;->isExecuting:Z

    if-nez v1, :cond_2

    sget-boolean v1, Lcom/stericson/roottools/execution/Shell;->isReading:Z

    if-nez v1, :cond_2

    .line 1031
    const-string v1, "RootTools v3.4"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Waiting for a command to be executed in a shell that is not executing and not reading! \n\n Command: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/stericson/roottools/execution/Command;->getCommand()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1036
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    .line 1037
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Exception;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 1038
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1027
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 1039
    :cond_2
    sget-boolean v1, Lcom/stericson/roottools/execution/Shell;->isExecuting:Z

    if-eqz v1, :cond_3

    sget-boolean v1, Lcom/stericson/roottools/execution/Shell;->isReading:Z

    if-nez v1, :cond_3

    .line 1040
    const-string v1, "RootTools v3.4"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Waiting for a command to be executed in a shell that is executing but not reading! \n\n Command: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/stericson/roottools/execution/Command;->getCommand()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1045
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    .line 1046
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Exception;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 1047
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 1049
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    const-string v1, "RootTools v3.4"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Waiting for a command to be executed in a shell that is not reading! \n\n Command: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/stericson/roottools/execution/Command;->getCommand()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1054
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    .line 1055
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Exception;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 1056
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 1061
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    return-void

    .line 1026
    :catch_0
    move-exception v1

    goto/16 :goto_1
.end method

.method public static getInstance()V
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/stericson/roottools/internal/RootToolsInternalMethods;

    invoke-direct {v0}, Lcom/stericson/roottools/internal/RootToolsInternalMethods;-><init>()V

    invoke-static {v0}, Lcom/stericson/roottools/RootTools;->setRim(Lcom/stericson/roottools/internal/RootToolsInternalMethods;)V

    .line 68
    return-void
.end method


# virtual methods
.method public exists(Ljava/lang/String;)Z
    .locals 11
    .param p1, "file"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 223
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 225
    .local v5, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/stericson/roottools/internal/RootToolsInternalMethods$1;

    new-array v8, v6, [Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ls "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v7

    invoke-direct {v0, p0, v7, v8, v5}, Lcom/stericson/roottools/internal/RootToolsInternalMethods$1;-><init>(Lcom/stericson/roottools/internal/RootToolsInternalMethods;I[Ljava/lang/String;Ljava/util/List;)V

    .line 235
    .local v0, "command":Lcom/stericson/roottools/execution/CommandCapture;
    :try_start_0
    invoke-static {}, Lcom/stericson/roottools/execution/Shell;->isAnyShellOpen()Z

    move-result v8

    if-nez v8, :cond_1

    .line 236
    invoke-static {}, Lcom/stericson/roottools/execution/Shell;->startShell()Lcom/stericson/roottools/execution/Shell;

    move-result-object v8

    invoke-virtual {v8, v0}, Lcom/stericson/roottools/execution/Shell;->add(Lcom/stericson/roottools/execution/Command;)Lcom/stericson/roottools/execution/Command;

    .line 237
    invoke-direct {p0, v0}, Lcom/stericson/roottools/internal/RootToolsInternalMethods;->commandWait(Lcom/stericson/roottools/execution/Command;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 247
    :goto_0
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 248
    .local v4, "line":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 271
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "line":Ljava/lang/String;
    :goto_1
    return v6

    .line 240
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/stericson/roottools/execution/Shell;->getOpenShell()Lcom/stericson/roottools/execution/Shell;

    move-result-object v8

    invoke-virtual {v8, v0}, Lcom/stericson/roottools/execution/Shell;->add(Lcom/stericson/roottools/execution/Command;)Lcom/stericson/roottools/execution/Command;

    .line 241
    invoke-direct {p0, v0}, Lcom/stericson/roottools/internal/RootToolsInternalMethods;->commandWait(Lcom/stericson/roottools/execution/Command;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 243
    :catch_0
    move-exception v1

    .local v1, "e":Ljava/lang/Exception;
    move v6, v7

    .line 244
    goto :goto_1

    .line 252
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_2
    const/4 v8, 0x0

    :try_start_2
    invoke-static {v8}, Lcom/stericson/roottools/RootTools;->closeShell(Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 255
    :goto_2
    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 257
    :try_start_3
    invoke-static {}, Lcom/stericson/roottools/execution/Shell;->startRootShell()Lcom/stericson/roottools/execution/Shell;

    move-result-object v8

    invoke-virtual {v8, v0}, Lcom/stericson/roottools/execution/Shell;->add(Lcom/stericson/roottools/execution/Command;)Lcom/stericson/roottools/execution/Command;

    .line 258
    invoke-direct {p0, v0}, Lcom/stericson/roottools/internal/RootToolsInternalMethods;->commandWait(Lcom/stericson/roottools/execution/Command;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 264
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 265
    .local v2, "final_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 267
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 268
    .restart local v4    # "line":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    goto :goto_1

    .line 259
    .end local v2    # "final_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "line":Ljava/lang/String;
    :catch_1
    move-exception v1

    .restart local v1    # "e":Ljava/lang/Exception;
    move v6, v7

    .line 260
    goto :goto_1

    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v2    # "final_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_4
    move v6, v7

    .line 271
    goto :goto_1

    .line 253
    .end local v2    # "final_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_2
    move-exception v8

    goto :goto_2
.end method

.method public findBinary(Ljava/lang/String;)Z
    .locals 17
    .param p1, "binaryName"    # Ljava/lang/String;

    .prologue
    .line 282
    const/4 v10, 0x0

    .line 283
    .local v10, "found":Z
    sget-object v2, Lcom/stericson/roottools/RootTools;->lastFoundBinaryPaths:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 285
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 286
    .local v6, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v2, 0x8

    new-array v14, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "/sbin/"

    aput-object v3, v14, v2

    const/4 v2, 0x1

    const-string v3, "/system/bin/"

    aput-object v3, v14, v2

    const/4 v2, 0x2

    const-string v3, "/system/xbin/"

    aput-object v3, v14, v2

    const/4 v2, 0x3

    const-string v3, "/data/local/xbin/"

    aput-object v3, v14, v2

    const/4 v2, 0x4

    const-string v3, "/data/local/bin/"

    aput-object v3, v14, v2

    const/4 v2, 0x5

    const-string v3, "/system/sd/xbin/"

    aput-object v3, v14, v2

    const/4 v2, 0x6

    const-string v3, "/system/bin/failsafe/"

    aput-object v3, v14, v2

    const/4 v2, 0x7

    const-string v3, "/data/local/"

    aput-object v3, v14, v2

    .line 290
    .local v14, "places":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Checking for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 294
    move-object v8, v14

    .local v8, "arr$":[Ljava/lang/String;
    :try_start_0
    array-length v12, v8

    .local v12, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_0
    if-ge v11, v12, :cond_0

    aget-object v7, v8, v11

    .line 295
    .local v7, "path":Ljava/lang/String;
    new-instance v1, Lcom/stericson/roottools/internal/RootToolsInternalMethods$2;

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "stat "

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    move-object/from16 v2, p0

    move-object/from16 v5, p1

    invoke-direct/range {v1 .. v7}, Lcom/stericson/roottools/internal/RootToolsInternalMethods$2;-><init>(Lcom/stericson/roottools/internal/RootToolsInternalMethods;I[Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    .line 308
    .local v1, "cc":Lcom/stericson/roottools/execution/CommandCapture;
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/stericson/roottools/RootTools;->getShell(Z)Lcom/stericson/roottools/execution/Shell;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/stericson/roottools/execution/Shell;->add(Lcom/stericson/roottools/execution/Command;)Lcom/stericson/roottools/execution/Command;

    .line 309
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/stericson/roottools/internal/RootToolsInternalMethods;->commandWait(Lcom/stericson/roottools/execution/Command;)V

    .line 294
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 313
    .end local v1    # "cc":Lcom/stericson/roottools/execution/CommandCapture;
    .end local v7    # "path":Ljava/lang/String;
    :cond_0
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_1

    const/4 v10, 0x1

    .line 319
    .end local v11    # "i$":I
    .end local v12    # "len$":I
    :goto_1
    if-nez v10, :cond_3

    .line 320
    const-string v2, "Trying second method"

    invoke-static {v2}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 322
    move-object v8, v14

    array-length v12, v8

    .restart local v12    # "len$":I
    const/4 v11, 0x0

    .restart local v11    # "i$":I
    :goto_2
    if-ge v11, v12, :cond_3

    aget-object v15, v8, v11

    .line 323
    .local v15, "where":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/stericson/roottools/RootTools;->exists(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 324
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " was found here: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 325
    invoke-interface {v6, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 326
    const/4 v10, 0x1

    .line 322
    :goto_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 313
    .end local v15    # "where":Ljava/lang/String;
    :cond_1
    const/4 v10, 0x0

    goto :goto_1

    .line 314
    .end local v11    # "i$":I
    .end local v12    # "len$":I
    :catch_0
    move-exception v9

    .line 315
    .local v9, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " was not found, more information MAY be available with "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Debugging on."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    goto :goto_1

    .line 328
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v11    # "i$":I
    .restart local v12    # "len$":I
    .restart local v15    # "where":Ljava/lang/String;
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " was NOT found here: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    goto :goto_3

    .line 333
    .end local v11    # "i$":I
    .end local v12    # "len$":I
    .end local v15    # "where":Ljava/lang/String;
    :cond_3
    if-nez v10, :cond_5

    .line 334
    const-string v2, "Trying third method"

    invoke-static {v2}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 337
    :try_start_1
    invoke-static {}, Lcom/stericson/roottools/RootTools;->getPath()Ljava/util/List;

    move-result-object v13

    .line 339
    .local v13, "paths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v13, :cond_5

    .line 340
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 341
    .restart local v7    # "path":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2f

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/stericson/roottools/RootTools;->exists(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 342
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " was found here: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 343
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 344
    const/4 v10, 0x1

    goto :goto_4

    .line 346
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " was NOT found here: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    .line 350
    .end local v7    # "path":Ljava/lang/String;
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v13    # "paths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_1
    move-exception v9

    .line 351
    .restart local v9    # "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " was not found, more information MAY be available with"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Debugging on."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 356
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_5
    invoke-static {v6}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 358
    sget-object v2, Lcom/stericson/roottools/RootTools;->lastFoundBinaryPaths:Ljava/util/List;

    invoke-interface {v2, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 360
    return v10
.end method

.method public isAccessGiven()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 498
    :try_start_0
    const-string v3, "Checking for Root access"

    invoke-static {v3}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 499
    const/4 v3, 0x0

    sput-boolean v3, Lcom/stericson/roottools/internal/InternalVariables;->accessGiven:Z

    .line 501
    new-instance v0, Lcom/stericson/roottools/internal/RootToolsInternalMethods$6;

    const/4 v3, 0x2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "id"

    aput-object v6, v4, v5

    invoke-direct {v0, p0, v3, v4}, Lcom/stericson/roottools/internal/RootToolsInternalMethods$6;-><init>(Lcom/stericson/roottools/internal/RootToolsInternalMethods;I[Ljava/lang/String;)V

    .line 521
    .local v0, "command":Lcom/stericson/roottools/execution/CommandCapture;
    invoke-static {}, Lcom/stericson/roottools/execution/Shell;->startRootShell()Lcom/stericson/roottools/execution/Shell;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/stericson/roottools/execution/Shell;->add(Lcom/stericson/roottools/execution/Command;)Lcom/stericson/roottools/execution/Command;

    .line 522
    invoke-direct {p0, v0}, Lcom/stericson/roottools/internal/RootToolsInternalMethods;->commandWait(Lcom/stericson/roottools/execution/Command;)V

    .line 524
    sget-boolean v2, Lcom/stericson/roottools/internal/InternalVariables;->accessGiven:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 528
    .end local v0    # "command":Lcom/stericson/roottools/execution/CommandCapture;
    :goto_0
    return v2

    .line 526
    :catch_0
    move-exception v1

    .line 527
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public offerBusyBox()V
    .locals 1

    .prologue
    .line 1012
    const-string v0, "Launching Market for BusyBox"

    invoke-static {v0}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 1013
    const-string v0, "market://details?id=stericson.busybox"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->showInPlaystore(Ljava/lang/String;)Z

    .line 1014
    return-void
.end method
