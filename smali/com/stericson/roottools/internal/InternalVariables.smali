.class public Lcom/stericson/roottools/internal/InternalVariables;
.super Ljava/lang/Object;
.source "InternalVariables.java"


# static fields
.field protected static accessGiven:Z

.field protected static found:Z

.field protected static inode:Ljava/lang/String;

.field protected static nativeToolsReady:Z

.field protected static pid_list:Ljava/lang/String;

.field protected static processRunning:Z

.field protected static final psPattern:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    sput-boolean v0, Lcom/stericson/roottools/internal/InternalVariables;->accessGiven:Z

    .line 43
    sput-boolean v0, Lcom/stericson/roottools/internal/InternalVariables;->nativeToolsReady:Z

    .line 44
    sput-boolean v0, Lcom/stericson/roottools/internal/InternalVariables;->found:Z

    .line 45
    sput-boolean v0, Lcom/stericson/roottools/internal/InternalVariables;->processRunning:Z

    .line 47
    const-string v0, ""

    sput-object v0, Lcom/stericson/roottools/internal/InternalVariables;->pid_list:Ljava/lang/String;

    .line 48
    const-string v0, ""

    sput-object v0, Lcom/stericson/roottools/internal/InternalVariables;->inode:Ljava/lang/String;

    .line 65
    const-string v0, "^\\S+\\s+([0-9]+).*$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/stericson/roottools/internal/InternalVariables;->psPattern:Ljava/util/regex/Pattern;

    .line 66
    return-void
.end method
