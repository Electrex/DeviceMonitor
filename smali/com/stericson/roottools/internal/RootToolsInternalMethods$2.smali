.class Lcom/stericson/roottools/internal/RootToolsInternalMethods$2;
.super Lcom/stericson/roottools/execution/CommandCapture;
.source "RootToolsInternalMethods.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stericson/roottools/internal/RootToolsInternalMethods;->findBinary(Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/stericson/roottools/internal/RootToolsInternalMethods;

.field final synthetic val$binaryName:Ljava/lang/String;

.field final synthetic val$list:Ljava/util/List;

.field final synthetic val$path:Ljava/lang/String;


# direct methods
.method varargs constructor <init>(Lcom/stericson/roottools/internal/RootToolsInternalMethods;I[Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # I
    .param p3, "x1"    # [Ljava/lang/String;

    .prologue
    .line 295
    iput-object p1, p0, Lcom/stericson/roottools/internal/RootToolsInternalMethods$2;->this$0:Lcom/stericson/roottools/internal/RootToolsInternalMethods;

    iput-object p4, p0, Lcom/stericson/roottools/internal/RootToolsInternalMethods$2;->val$binaryName:Ljava/lang/String;

    iput-object p5, p0, Lcom/stericson/roottools/internal/RootToolsInternalMethods$2;->val$list:Ljava/util/List;

    iput-object p6, p0, Lcom/stericson/roottools/internal/RootToolsInternalMethods$2;->val$path:Ljava/lang/String;

    invoke-direct {p0, p2, p3}, Lcom/stericson/roottools/execution/CommandCapture;-><init>(I[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public commandOutput(ILjava/lang/String;)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "line"    # Ljava/lang/String;

    .prologue
    .line 298
    const-string v0, "File: "

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/stericson/roottools/internal/RootToolsInternalMethods$2;->val$binaryName:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/stericson/roottools/internal/RootToolsInternalMethods$2;->val$list:Ljava/util/List;

    iget-object v1, p0, Lcom/stericson/roottools/internal/RootToolsInternalMethods$2;->val$path:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 301
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/stericson/roottools/internal/RootToolsInternalMethods$2;->val$binaryName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " was found here: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/stericson/roottools/internal/RootToolsInternalMethods$2;->val$path:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 304
    :cond_0
    invoke-static {p2}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 305
    return-void
.end method
