.class Lcom/stericson/roottools/execution/Shell$1;
.super Ljava/lang/Object;
.source "Shell.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/stericson/roottools/execution/Shell;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/stericson/roottools/execution/Shell;


# direct methods
.method constructor <init>(Lcom/stericson/roottools/execution/Shell;)V
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 294
    :cond_0
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->commands:Ljava/util/List;
    invoke-static {v3}, Lcom/stericson/roottools/execution/Shell;->access$100(Lcom/stericson/roottools/execution/Shell;)Ljava/util/List;

    move-result-object v4

    monitor-enter v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 300
    :goto_1
    :try_start_1
    iget-object v3, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->close:Z
    invoke-static {v3}, Lcom/stericson/roottools/execution/Shell;->access$200(Lcom/stericson/roottools/execution/Shell;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->write:I
    invoke-static {v3}, Lcom/stericson/roottools/execution/Shell;->access$300(Lcom/stericson/roottools/execution/Shell;)I

    move-result v3

    iget-object v5, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->commands:Ljava/util/List;
    invoke-static {v5}, Lcom/stericson/roottools/execution/Shell;->access$100(Lcom/stericson/roottools/execution/Shell;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lt v3, v5, :cond_1

    .line 301
    const/4 v3, 0x0

    sput-boolean v3, Lcom/stericson/roottools/execution/Shell;->isExecuting:Z

    .line 302
    iget-object v3, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->commands:Ljava/util/List;
    invoke-static {v3}, Lcom/stericson/roottools/execution/Shell;->access$100(Lcom/stericson/roottools/execution/Shell;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->wait()V

    goto :goto_1

    .line 304
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 349
    :catch_0
    move-exception v3

    move-object v1, v3

    .line 350
    .local v1, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v3, v4, v1}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;ILjava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 352
    iget-object v3, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # setter for: Lcom/stericson/roottools/execution/Shell;->write:I
    invoke-static {v3, v6}, Lcom/stericson/roottools/execution/Shell;->access$302(Lcom/stericson/roottools/execution/Shell;I)I

    .line 353
    iget-object v3, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    iget-object v4, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->outputStream:Ljava/io/OutputStreamWriter;
    invoke-static {v4}, Lcom/stericson/roottools/execution/Shell;->access$600(Lcom/stericson/roottools/execution/Shell;)Ljava/io/OutputStreamWriter;

    move-result-object v4

    # invokes: Lcom/stericson/roottools/execution/Shell;->closeQuietly(Ljava/io/Writer;)V
    invoke-static {v3, v4}, Lcom/stericson/roottools/execution/Shell;->access$800(Lcom/stericson/roottools/execution/Shell;Ljava/io/Writer;)V

    .line 355
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_3
    return-void

    .line 304
    :cond_1
    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 306
    :try_start_5
    iget-object v3, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->write:I
    invoke-static {v3}, Lcom/stericson/roottools/execution/Shell;->access$300(Lcom/stericson/roottools/execution/Shell;)I

    move-result v3

    const/16 v4, 0x3e8

    if-lt v3, v4, :cond_3

    .line 311
    :goto_4
    iget-object v3, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->read:I
    invoke-static {v3}, Lcom/stericson/roottools/execution/Shell;->access$400(Lcom/stericson/roottools/execution/Shell;)I

    move-result v3

    iget-object v4, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->write:I
    invoke-static {v4}, Lcom/stericson/roottools/execution/Shell;->access$300(Lcom/stericson/roottools/execution/Shell;)I

    move-result v4

    if-eq v3, v4, :cond_2

    .line 312
    const-string v3, "Waiting for read and write to catch up before cleanup."

    invoke-static {v3}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    goto :goto_4

    .line 349
    :catch_1
    move-exception v3

    move-object v1, v3

    goto :goto_2

    .line 317
    :cond_2
    iget-object v3, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # invokes: Lcom/stericson/roottools/execution/Shell;->cleanCommands()V
    invoke-static {v3}, Lcom/stericson/roottools/execution/Shell;->access$500(Lcom/stericson/roottools/execution/Shell;)V

    .line 326
    :cond_3
    iget-object v3, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->write:I
    invoke-static {v3}, Lcom/stericson/roottools/execution/Shell;->access$300(Lcom/stericson/roottools/execution/Shell;)I

    move-result v3

    iget-object v4, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->commands:Ljava/util/List;
    invoke-static {v4}, Lcom/stericson/roottools/execution/Shell;->access$100(Lcom/stericson/roottools/execution/Shell;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_4

    .line 327
    const/4 v3, 0x1

    sput-boolean v3, Lcom/stericson/roottools/execution/Shell;->isExecuting:Z

    .line 328
    iget-object v3, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->commands:Ljava/util/List;
    invoke-static {v3}, Lcom/stericson/roottools/execution/Shell;->access$100(Lcom/stericson/roottools/execution/Shell;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->write:I
    invoke-static {v4}, Lcom/stericson/roottools/execution/Shell;->access$300(Lcom/stericson/roottools/execution/Shell;)I

    move-result v4

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/stericson/roottools/execution/Command;

    .line 329
    .local v0, "cmd":Lcom/stericson/roottools/execution/Command;
    invoke-virtual {v0}, Lcom/stericson/roottools/execution/Command;->startExecution()V

    .line 330
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Executing: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/stericson/roottools/execution/Command;->getCommand()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 332
    iget-object v3, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->outputStream:Ljava/io/OutputStreamWriter;
    invoke-static {v3}, Lcom/stericson/roottools/execution/Shell;->access$600(Lcom/stericson/roottools/execution/Shell;)Ljava/io/OutputStreamWriter;

    move-result-object v3

    invoke-virtual {v0}, Lcom/stericson/roottools/execution/Command;->getCommand()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 333
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\necho F*D^W@#FGF "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->totalExecuted:I
    invoke-static {v4}, Lcom/stericson/roottools/execution/Shell;->access$700(Lcom/stericson/roottools/execution/Shell;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " $?\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 334
    .local v2, "line":Ljava/lang/String;
    iget-object v3, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->outputStream:Ljava/io/OutputStreamWriter;
    invoke-static {v3}, Lcom/stericson/roottools/execution/Shell;->access$600(Lcom/stericson/roottools/execution/Shell;)Ljava/io/OutputStreamWriter;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 335
    iget-object v3, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->outputStream:Ljava/io/OutputStreamWriter;
    invoke-static {v3}, Lcom/stericson/roottools/execution/Shell;->access$600(Lcom/stericson/roottools/execution/Shell;)Ljava/io/OutputStreamWriter;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->flush()V

    .line 336
    iget-object v3, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # operator++ for: Lcom/stericson/roottools/execution/Shell;->write:I
    invoke-static {v3}, Lcom/stericson/roottools/execution/Shell;->access$308(Lcom/stericson/roottools/execution/Shell;)I

    .line 337
    iget-object v3, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # operator++ for: Lcom/stericson/roottools/execution/Shell;->totalExecuted:I
    invoke-static {v3}, Lcom/stericson/roottools/execution/Shell;->access$708(Lcom/stericson/roottools/execution/Shell;)I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_0

    .line 352
    .end local v0    # "cmd":Lcom/stericson/roottools/execution/Command;
    .end local v2    # "line":Ljava/lang/String;
    :catchall_1
    move-exception v3

    iget-object v4, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # setter for: Lcom/stericson/roottools/execution/Shell;->write:I
    invoke-static {v4, v6}, Lcom/stericson/roottools/execution/Shell;->access$302(Lcom/stericson/roottools/execution/Shell;I)I

    .line 353
    iget-object v4, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    iget-object v5, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->outputStream:Ljava/io/OutputStreamWriter;
    invoke-static {v5}, Lcom/stericson/roottools/execution/Shell;->access$600(Lcom/stericson/roottools/execution/Shell;)Ljava/io/OutputStreamWriter;

    move-result-object v5

    # invokes: Lcom/stericson/roottools/execution/Shell;->closeQuietly(Ljava/io/Writer;)V
    invoke-static {v4, v5}, Lcom/stericson/roottools/execution/Shell;->access$800(Lcom/stericson/roottools/execution/Shell;Ljava/io/Writer;)V

    throw v3

    .line 338
    :cond_4
    :try_start_6
    iget-object v3, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->close:Z
    invoke-static {v3}, Lcom/stericson/roottools/execution/Shell;->access$200(Lcom/stericson/roottools/execution/Shell;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 342
    const/4 v3, 0x0

    sput-boolean v3, Lcom/stericson/roottools/execution/Shell;->isExecuting:Z

    .line 343
    iget-object v3, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->outputStream:Ljava/io/OutputStreamWriter;
    invoke-static {v3}, Lcom/stericson/roottools/execution/Shell;->access$600(Lcom/stericson/roottools/execution/Shell;)Ljava/io/OutputStreamWriter;

    move-result-object v3

    const-string v4, "\nexit 0\n"

    invoke-virtual {v3, v4}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 344
    iget-object v3, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->outputStream:Ljava/io/OutputStreamWriter;
    invoke-static {v3}, Lcom/stericson/roottools/execution/Shell;->access$600(Lcom/stericson/roottools/execution/Shell;)Ljava/io/OutputStreamWriter;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->flush()V

    .line 345
    const-string v3, "Closing shell"

    invoke-static {v3}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 352
    iget-object v3, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # setter for: Lcom/stericson/roottools/execution/Shell;->write:I
    invoke-static {v3, v6}, Lcom/stericson/roottools/execution/Shell;->access$302(Lcom/stericson/roottools/execution/Shell;I)I

    .line 353
    iget-object v3, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    iget-object v4, p0, Lcom/stericson/roottools/execution/Shell$1;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->outputStream:Ljava/io/OutputStreamWriter;
    invoke-static {v4}, Lcom/stericson/roottools/execution/Shell;->access$600(Lcom/stericson/roottools/execution/Shell;)Ljava/io/OutputStreamWriter;

    move-result-object v4

    # invokes: Lcom/stericson/roottools/execution/Shell;->closeQuietly(Ljava/io/Writer;)V
    invoke-static {v3, v4}, Lcom/stericson/roottools/execution/Shell;->access$800(Lcom/stericson/roottools/execution/Shell;Ljava/io/Writer;)V

    goto/16 :goto_3
.end method
