.class public abstract Lcom/stericson/roottools/execution/Command;
.super Ljava/lang/Object;
.source "Command.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stericson/roottools/execution/Command$1;,
        Lcom/stericson/roottools/execution/Command$ExecutionMonitor;
    }
.end annotation


# instance fields
.field command:[Ljava/lang/String;

.field executing:Z

.field executionMonitor:Lcom/stericson/roottools/execution/Command$ExecutionMonitor;

.field exitCode:I

.field finished:Z

.field id:I

.field mHandler:Landroid/os/Handler;

.field terminated:Z

.field timeout:I


# direct methods
.method public varargs constructor <init>(I[Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "command"    # [Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/stericson/roottools/execution/Command;->executionMonitor:Lcom/stericson/roottools/execution/Command$ExecutionMonitor;

    .line 35
    iput-boolean v1, p0, Lcom/stericson/roottools/execution/Command;->executing:Z

    .line 38
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/stericson/roottools/execution/Command;->command:[Ljava/lang/String;

    .line 39
    iput-boolean v1, p0, Lcom/stericson/roottools/execution/Command;->finished:Z

    .line 40
    iput-boolean v1, p0, Lcom/stericson/roottools/execution/Command;->terminated:Z

    .line 41
    const/4 v0, -0x1

    iput v0, p0, Lcom/stericson/roottools/execution/Command;->exitCode:I

    .line 42
    iput v1, p0, Lcom/stericson/roottools/execution/Command;->id:I

    .line 43
    const/16 v0, 0x4e20

    iput v0, p0, Lcom/stericson/roottools/execution/Command;->timeout:I

    .line 54
    iput-object p2, p0, Lcom/stericson/roottools/execution/Command;->command:[Ljava/lang/String;

    .line 55
    iput p1, p0, Lcom/stericson/roottools/execution/Command;->id:I

    .line 56
    return-void
.end method


# virtual methods
.method protected commandFinished()V
    .locals 5

    .prologue
    .line 104
    iget-boolean v2, p0, Lcom/stericson/roottools/execution/Command;->terminated:Z

    if-eqz v2, :cond_0

    .line 123
    :goto_0
    return-void

    .line 107
    :cond_0
    monitor-enter p0

    .line 108
    :try_start_0
    iget-object v2, p0, Lcom/stericson/roottools/execution/Command;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_1

    .line 109
    iget-object v2, p0, Lcom/stericson/roottools/execution/Command;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 110
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 111
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "action"

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 112
    const-string v2, "id"

    iget v3, p0, Lcom/stericson/roottools/execution/Command;->id:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 113
    const-string v2, "exit_code"

    iget v3, p0, Lcom/stericson/roottools/execution/Command;->exitCode:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 114
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 115
    iget-object v2, p0, Lcom/stericson/roottools/execution/Command;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 120
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Command "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/stericson/roottools/execution/Command;->id:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " finished."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 121
    invoke-virtual {p0}, Lcom/stericson/roottools/execution/Command;->finishCommand()V

    .line 122
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 117
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/stericson/roottools/execution/Command;->getCommandListener()Lcom/stericson/roottools/execution/CommandListener;

    move-result-object v2

    iget v3, p0, Lcom/stericson/roottools/execution/Command;->id:I

    iget v4, p0, Lcom/stericson/roottools/execution/Command;->exitCode:I

    invoke-interface {v2, v3, v4}, Lcom/stericson/roottools/execution/CommandListener;->commandCompleted(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method protected finishCommand()V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/stericson/roottools/execution/Command;->executing:Z

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/stericson/roottools/execution/Command;->finished:Z

    .line 67
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 68
    return-void
.end method

.method public getCommand()Ljava/lang/String;
    .locals 7

    .prologue
    .line 71
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 73
    .local v4, "sb":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/stericson/roottools/execution/Command;->command:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v0, v1, v2

    .local v0, "aCommand":Ljava/lang/String;
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0xa

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 75
    .end local v0    # "aCommand":Ljava/lang/String;
    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public abstract getCommandListener()Lcom/stericson/roottools/execution/CommandListener;
.end method

.method public isExecuting()Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/stericson/roottools/execution/Command;->executing:Z

    return v0
.end method

.method public isFinished()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/stericson/roottools/execution/Command;->finished:Z

    return v0
.end method

.method protected output(ILjava/lang/String;)V
    .locals 4
    .param p1, "id"    # I
    .param p2, "line"    # Ljava/lang/String;

    .prologue
    .line 148
    iget-object v2, p0, Lcom/stericson/roottools/execution/Command;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 149
    iget-object v2, p0, Lcom/stericson/roottools/execution/Command;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 150
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 151
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "action"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 152
    const-string v2, "id"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 153
    const-string v2, "text"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 155
    iget-object v2, p0, Lcom/stericson/roottools/execution/Command;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 159
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :goto_0
    return-void

    .line 157
    :cond_0
    invoke-virtual {p0}, Lcom/stericson/roottools/execution/Command;->getCommandListener()Lcom/stericson/roottools/execution/CommandListener;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Lcom/stericson/roottools/execution/CommandListener;->commandOutput(ILjava/lang/String;)V

    goto :goto_0
.end method

.method protected setExitCode(I)V
    .locals 1
    .param p1, "code"    # I

    .prologue
    .line 85
    monitor-enter p0

    .line 86
    :try_start_0
    iput p1, p0, Lcom/stericson/roottools/execution/Command;->exitCode:I

    .line 87
    monitor-exit p0

    .line 88
    return-void

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected startExecution()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 91
    new-instance v0, Lcom/stericson/roottools/execution/Command$ExecutionMonitor;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/stericson/roottools/execution/Command$ExecutionMonitor;-><init>(Lcom/stericson/roottools/execution/Command;Lcom/stericson/roottools/execution/Command$1;)V

    iput-object v0, p0, Lcom/stericson/roottools/execution/Command;->executionMonitor:Lcom/stericson/roottools/execution/Command$ExecutionMonitor;

    .line 92
    iget-object v0, p0, Lcom/stericson/roottools/execution/Command;->executionMonitor:Lcom/stericson/roottools/execution/Command$ExecutionMonitor;

    invoke-virtual {v0, v2}, Lcom/stericson/roottools/execution/Command$ExecutionMonitor;->setPriority(I)V

    .line 93
    iget-object v0, p0, Lcom/stericson/roottools/execution/Command;->executionMonitor:Lcom/stericson/roottools/execution/Command$ExecutionMonitor;

    invoke-virtual {v0}, Lcom/stericson/roottools/execution/Command$ExecutionMonitor;->start()V

    .line 94
    iput-boolean v2, p0, Lcom/stericson/roottools/execution/Command;->executing:Z

    .line 95
    return-void
.end method

.method public terminate(Ljava/lang/String;)V
    .locals 1
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 98
    invoke-static {}, Lcom/stericson/roottools/execution/Shell;->closeAll()V

    .line 99
    const-string v0, "Terminating all shells."

    invoke-static {v0}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 100
    invoke-virtual {p0, p1}, Lcom/stericson/roottools/execution/Command;->terminated(Ljava/lang/String;)V

    .line 101
    return-void
.end method

.method protected terminated(Ljava/lang/String;)V
    .locals 4
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 126
    monitor-enter p0

    .line 127
    :try_start_0
    iget-object v2, p0, Lcom/stericson/roottools/execution/Command;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 128
    iget-object v2, p0, Lcom/stericson/roottools/execution/Command;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 129
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 130
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "action"

    const/4 v3, 0x3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 131
    const-string v2, "id"

    iget v3, p0, Lcom/stericson/roottools/execution/Command;->id:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 132
    const-string v2, "text"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 134
    iget-object v2, p0, Lcom/stericson/roottools/execution/Command;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 139
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Command "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/stericson/roottools/execution/Command;->id:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " did not finish because it was terminated. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Termination reason: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 141
    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/stericson/roottools/execution/Command;->setExitCode(I)V

    .line 142
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/stericson/roottools/execution/Command;->terminated:Z

    .line 143
    invoke-virtual {p0}, Lcom/stericson/roottools/execution/Command;->finishCommand()V

    .line 144
    monitor-exit p0

    .line 145
    return-void

    .line 136
    :cond_0
    invoke-virtual {p0}, Lcom/stericson/roottools/execution/Command;->getCommandListener()Lcom/stericson/roottools/execution/CommandListener;

    move-result-object v2

    iget v3, p0, Lcom/stericson/roottools/execution/Command;->id:I

    invoke-interface {v2, v3, p1}, Lcom/stericson/roottools/execution/CommandListener;->commandTerminated(ILjava/lang/String;)V

    goto :goto_0

    .line 144
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
