.class Lcom/stericson/roottools/execution/Command$ExecutionMonitor;
.super Ljava/lang/Thread;
.source "Command.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/stericson/roottools/execution/Command;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExecutionMonitor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/stericson/roottools/execution/Command;


# direct methods
.method private constructor <init>(Lcom/stericson/roottools/execution/Command;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/stericson/roottools/execution/Command$ExecutionMonitor;->this$0:Lcom/stericson/roottools/execution/Command;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/stericson/roottools/execution/Command;Lcom/stericson/roottools/execution/Command$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/stericson/roottools/execution/Command;
    .param p2, "x1"    # Lcom/stericson/roottools/execution/Command$1;

    .prologue
    .line 161
    invoke-direct {p0, p1}, Lcom/stericson/roottools/execution/Command$ExecutionMonitor;-><init>(Lcom/stericson/roottools/execution/Command;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 163
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/stericson/roottools/execution/Command$ExecutionMonitor;->this$0:Lcom/stericson/roottools/execution/Command;

    iget-boolean v0, v0, Lcom/stericson/roottools/execution/Command;->finished:Z

    if-nez v0, :cond_1

    .line 164
    monitor-enter p0

    .line 166
    :try_start_0
    iget-object v0, p0, Lcom/stericson/roottools/execution/Command$ExecutionMonitor;->this$0:Lcom/stericson/roottools/execution/Command;

    iget v0, v0, Lcom/stericson/roottools/execution/Command;->timeout:I

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    :goto_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 170
    iget-object v0, p0, Lcom/stericson/roottools/execution/Command$ExecutionMonitor;->this$0:Lcom/stericson/roottools/execution/Command;

    iget-boolean v0, v0, Lcom/stericson/roottools/execution/Command;->finished:Z

    if-nez v0, :cond_0

    .line 171
    const-string v0, "Timeout Exception has occurred."

    invoke-static {v0}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lcom/stericson/roottools/execution/Command$ExecutionMonitor;->this$0:Lcom/stericson/roottools/execution/Command;

    const-string v1, "Timeout Exception"

    invoke-virtual {v0, v1}, Lcom/stericson/roottools/execution/Command;->terminate(Ljava/lang/String;)V

    goto :goto_0

    .line 168
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 175
    :cond_1
    return-void

    .line 167
    :catch_0
    move-exception v0

    goto :goto_1
.end method
