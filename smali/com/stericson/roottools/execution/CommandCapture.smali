.class public Lcom/stericson/roottools/execution/CommandCapture;
.super Lcom/stericson/roottools/execution/Command;
.source "CommandCapture.java"

# interfaces
.implements Lcom/stericson/roottools/execution/CommandListener;


# instance fields
.field private final sb:Ljava/lang/StringBuilder;


# direct methods
.method public varargs constructor <init>(I[Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "command"    # [Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/stericson/roottools/execution/Command;-><init>(I[Ljava/lang/String;)V

    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/stericson/roottools/execution/CommandCapture;->sb:Ljava/lang/StringBuilder;

    .line 31
    return-void
.end method


# virtual methods
.method public commandCompleted(II)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "exitcode"    # I

    .prologue
    .line 56
    return-void
.end method

.method public commandOutput(ILjava/lang/String;)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "line"    # Ljava/lang/String;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/stericson/roottools/execution/CommandCapture;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 45
    const-string v0, "Command"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method public commandTerminated(ILjava/lang/String;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "reason"    # Ljava/lang/String;

    .prologue
    .line 51
    return-void
.end method

.method public getCommandListener()Lcom/stericson/roottools/execution/CommandListener;
    .locals 0

    .prologue
    .line 39
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/stericson/roottools/execution/CommandCapture;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
