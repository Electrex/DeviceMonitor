.class public Lcom/stericson/roottools/execution/Shell;
.super Ljava/lang/Object;
.source "Shell.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stericson/roottools/execution/Shell$Worker;
    }
.end annotation


# static fields
.field private static customShell:Lcom/stericson/roottools/execution/Shell;

.field private static error:Ljava/lang/String;

.field public static isExecuting:Z

.field public static isReading:Z

.field private static rootShell:Lcom/stericson/roottools/execution/Shell;

.field private static shell:Lcom/stericson/roottools/execution/Shell;

.field private static shellTimeout:I


# instance fields
.field private close:Z

.field private final commands:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/stericson/roottools/execution/Command;",
            ">;"
        }
    .end annotation
.end field

.field private final errorStream:Ljava/io/BufferedReader;

.field private final input:Ljava/lang/Runnable;

.field private final inputStream:Ljava/io/BufferedReader;

.field private isCleaning:Z

.field public isClosed:Z

.field private final output:Ljava/lang/Runnable;

.field private final outputStream:Ljava/io/OutputStreamWriter;

.field private final proc:Ljava/lang/Process;

.field private read:I

.field private totalExecuted:I

.field private totalRead:I

.field private write:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 54
    const-string v0, ""

    sput-object v0, Lcom/stericson/roottools/execution/Shell;->error:Ljava/lang/String;

    .line 56
    sput-object v1, Lcom/stericson/roottools/execution/Shell;->rootShell:Lcom/stericson/roottools/execution/Shell;

    .line 57
    sput-object v1, Lcom/stericson/roottools/execution/Shell;->shell:Lcom/stericson/roottools/execution/Shell;

    .line 58
    sput-object v1, Lcom/stericson/roottools/execution/Shell;->customShell:Lcom/stericson/roottools/execution/Shell;

    .line 60
    const/16 v0, 0x61a8

    sput v0, Lcom/stericson/roottools/execution/Shell;->shellTimeout:I

    .line 61
    sput-boolean v2, Lcom/stericson/roottools/execution/Shell;->isExecuting:Z

    .line 62
    sput-boolean v2, Lcom/stericson/roottools/execution/Shell;->isReading:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 8
    .param p1, "cmd"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/concurrent/TimeoutException;,
            Lcom/stericson/roottools/exceptions/RootDeniedException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/stericson/roottools/execution/Shell;->commands:Ljava/util/List;

    .line 52
    iput-boolean v6, p0, Lcom/stericson/roottools/execution/Shell;->close:Z

    .line 63
    iput-boolean v6, p0, Lcom/stericson/roottools/execution/Shell;->isClosed:Z

    .line 67
    iput v6, p0, Lcom/stericson/roottools/execution/Shell;->read:I

    .line 68
    iput v6, p0, Lcom/stericson/roottools/execution/Shell;->write:I

    .line 69
    iput v6, p0, Lcom/stericson/roottools/execution/Shell;->totalExecuted:I

    .line 70
    iput v6, p0, Lcom/stericson/roottools/execution/Shell;->totalRead:I

    .line 71
    iput-boolean v6, p0, Lcom/stericson/roottools/execution/Shell;->isCleaning:Z

    .line 289
    new-instance v4, Lcom/stericson/roottools/execution/Shell$1;

    invoke-direct {v4, p0}, Lcom/stericson/roottools/execution/Shell$1;-><init>(Lcom/stericson/roottools/execution/Shell;)V

    iput-object v4, p0, Lcom/stericson/roottools/execution/Shell;->input:Ljava/lang/Runnable;

    .line 371
    new-instance v4, Lcom/stericson/roottools/execution/Shell$3;

    invoke-direct {v4, p0}, Lcom/stericson/roottools/execution/Shell$3;-><init>(Lcom/stericson/roottools/execution/Shell;)V

    iput-object v4, p0, Lcom/stericson/roottools/execution/Shell;->output:Ljava/lang/Runnable;

    .line 76
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Starting shell: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 78
    new-instance v4, Ljava/lang/ProcessBuilder;

    new-array v5, v7, [Ljava/lang/String;

    aput-object p1, v5, v6

    invoke-direct {v4, v5}, Ljava/lang/ProcessBuilder;-><init>([Ljava/lang/String;)V

    invoke-virtual {v4, v7}, Ljava/lang/ProcessBuilder;->redirectErrorStream(Z)Ljava/lang/ProcessBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/ProcessBuilder;->start()Ljava/lang/Process;

    move-result-object v4

    iput-object v4, p0, Lcom/stericson/roottools/execution/Shell;->proc:Ljava/lang/Process;

    .line 79
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/InputStreamReader;

    iget-object v6, p0, Lcom/stericson/roottools/execution/Shell;->proc:Ljava/lang/Process;

    invoke-virtual {v6}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    const-string v7, "UTF-8"

    invoke-direct {v5, v6, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v4, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iput-object v4, p0, Lcom/stericson/roottools/execution/Shell;->inputStream:Ljava/io/BufferedReader;

    .line 80
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/InputStreamReader;

    iget-object v6, p0, Lcom/stericson/roottools/execution/Shell;->proc:Ljava/lang/Process;

    invoke-virtual {v6}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v6

    const-string v7, "UTF-8"

    invoke-direct {v5, v6, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v4, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iput-object v4, p0, Lcom/stericson/roottools/execution/Shell;->errorStream:Ljava/io/BufferedReader;

    .line 81
    new-instance v4, Ljava/io/OutputStreamWriter;

    iget-object v5, p0, Lcom/stericson/roottools/execution/Shell;->proc:Ljava/lang/Process;

    invoke-virtual {v5}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    const-string v6, "UTF-8"

    invoke-direct {v4, v5, v6}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/stericson/roottools/execution/Shell;->outputStream:Ljava/io/OutputStreamWriter;

    .line 86
    new-instance v3, Lcom/stericson/roottools/execution/Shell$Worker;

    iget-object v4, p0, Lcom/stericson/roottools/execution/Shell;->proc:Ljava/lang/Process;

    iget-object v5, p0, Lcom/stericson/roottools/execution/Shell;->inputStream:Ljava/io/BufferedReader;

    iget-object v6, p0, Lcom/stericson/roottools/execution/Shell;->outputStream:Ljava/io/OutputStreamWriter;

    const/4 v7, 0x0

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/stericson/roottools/execution/Shell$Worker;-><init>(Ljava/lang/Process;Ljava/io/BufferedReader;Ljava/io/OutputStreamWriter;Lcom/stericson/roottools/execution/Shell$1;)V

    .line 87
    .local v3, "worker":Lcom/stericson/roottools/execution/Shell$Worker;
    invoke-virtual {v3}, Lcom/stericson/roottools/execution/Shell$Worker;->start()V

    .line 98
    :try_start_0
    sget v4, Lcom/stericson/roottools/execution/Shell;->shellTimeout:I

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Lcom/stericson/roottools/execution/Shell$Worker;->join(J)V

    .line 103
    iget v4, v3, Lcom/stericson/roottools/execution/Shell$Worker;->exit:I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v5, -0x38f

    if-ne v4, v5, :cond_0

    .line 106
    :try_start_1
    iget-object v4, p0, Lcom/stericson/roottools/execution/Shell;->proc:Ljava/lang/Process;

    invoke-virtual {v4}, Ljava/lang/Process;->destroy()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 109
    :goto_0
    :try_start_2
    iget-object v4, p0, Lcom/stericson/roottools/execution/Shell;->inputStream:Ljava/io/BufferedReader;

    invoke-direct {p0, v4}, Lcom/stericson/roottools/execution/Shell;->closeQuietly(Ljava/io/Reader;)V

    .line 110
    iget-object v4, p0, Lcom/stericson/roottools/execution/Shell;->errorStream:Ljava/io/BufferedReader;

    invoke-direct {p0, v4}, Lcom/stericson/roottools/execution/Shell;->closeQuietly(Ljava/io/Reader;)V

    .line 111
    iget-object v4, p0, Lcom/stericson/roottools/execution/Shell;->outputStream:Ljava/io/OutputStreamWriter;

    invoke-direct {p0, v4}, Lcom/stericson/roottools/execution/Shell;->closeQuietly(Ljava/io/Writer;)V

    .line 113
    new-instance v4, Ljava/util/concurrent/TimeoutException;

    sget-object v5, Lcom/stericson/roottools/execution/Shell;->error:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 149
    :catch_0
    move-exception v0

    .line 150
    .local v0, "ex":Ljava/lang/InterruptedException;
    invoke-virtual {v3}, Lcom/stericson/roottools/execution/Shell$Worker;->interrupt()V

    .line 151
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V

    .line 152
    new-instance v4, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v4}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw v4

    .line 118
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :cond_0
    :try_start_3
    iget v4, v3, Lcom/stericson/roottools/execution/Shell$Worker;->exit:I
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    const/16 v5, -0x2a

    if-ne v4, v5, :cond_1

    .line 121
    :try_start_4
    iget-object v4, p0, Lcom/stericson/roottools/execution/Shell;->proc:Ljava/lang/Process;

    invoke-virtual {v4}, Ljava/lang/Process;->destroy()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    .line 124
    :goto_1
    :try_start_5
    iget-object v4, p0, Lcom/stericson/roottools/execution/Shell;->inputStream:Ljava/io/BufferedReader;

    invoke-direct {p0, v4}, Lcom/stericson/roottools/execution/Shell;->closeQuietly(Ljava/io/Reader;)V

    .line 125
    iget-object v4, p0, Lcom/stericson/roottools/execution/Shell;->errorStream:Ljava/io/BufferedReader;

    invoke-direct {p0, v4}, Lcom/stericson/roottools/execution/Shell;->closeQuietly(Ljava/io/Reader;)V

    .line 126
    iget-object v4, p0, Lcom/stericson/roottools/execution/Shell;->outputStream:Ljava/io/OutputStreamWriter;

    invoke-direct {p0, v4}, Lcom/stericson/roottools/execution/Shell;->closeQuietly(Ljava/io/Writer;)V

    .line 128
    new-instance v4, Lcom/stericson/roottools/exceptions/RootDeniedException;

    const-string v5, "Root Access Denied"

    invoke-direct {v4, v5}, Lcom/stericson/roottools/exceptions/RootDeniedException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 141
    :cond_1
    new-instance v1, Ljava/lang/Thread;

    iget-object v4, p0, Lcom/stericson/roottools/execution/Shell;->input:Ljava/lang/Runnable;

    const-string v5, "Shell Input"

    invoke-direct {v1, v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 142
    .local v1, "si":Ljava/lang/Thread;
    const/4 v4, 0x5

    invoke-virtual {v1, v4}, Ljava/lang/Thread;->setPriority(I)V

    .line 143
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 145
    new-instance v2, Ljava/lang/Thread;

    iget-object v4, p0, Lcom/stericson/roottools/execution/Shell;->output:Ljava/lang/Runnable;

    const-string v5, "Shell Output"

    invoke-direct {v2, v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 146
    .local v2, "so":Ljava/lang/Thread;
    const/4 v4, 0x5

    invoke-virtual {v2, v4}, Ljava/lang/Thread;->setPriority(I)V

    .line 147
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0

    .line 154
    return-void

    .line 122
    .end local v1    # "si":Ljava/lang/Thread;
    .end local v2    # "so":Ljava/lang/Thread;
    :catch_1
    move-exception v4

    goto :goto_1

    .line 107
    :catch_2
    move-exception v4

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/stericson/roottools/execution/Shell;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/stericson/roottools/execution/Shell;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/stericson/roottools/execution/Shell;->commands:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/stericson/roottools/execution/Shell;)I
    .locals 1
    .param p0, "x0"    # Lcom/stericson/roottools/execution/Shell;

    .prologue
    .line 43
    iget v0, p0, Lcom/stericson/roottools/execution/Shell;->totalRead:I

    return v0
.end method

.method static synthetic access$1008(Lcom/stericson/roottools/execution/Shell;)I
    .locals 2
    .param p0, "x0"    # Lcom/stericson/roottools/execution/Shell;

    .prologue
    .line 43
    iget v0, p0, Lcom/stericson/roottools/execution/Shell;->totalRead:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/stericson/roottools/execution/Shell;->totalRead:I

    return v0
.end method

.method static synthetic access$1100(Lcom/stericson/roottools/execution/Shell;)Ljava/lang/Process;
    .locals 1
    .param p0, "x0"    # Lcom/stericson/roottools/execution/Shell;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/stericson/roottools/execution/Shell;->proc:Ljava/lang/Process;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/stericson/roottools/execution/Shell;)Ljava/io/BufferedReader;
    .locals 1
    .param p0, "x0"    # Lcom/stericson/roottools/execution/Shell;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/stericson/roottools/execution/Shell;->errorStream:Ljava/io/BufferedReader;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/stericson/roottools/execution/Shell;Ljava/io/Reader;)V
    .locals 0
    .param p0, "x0"    # Lcom/stericson/roottools/execution/Shell;
    .param p1, "x1"    # Ljava/io/Reader;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/stericson/roottools/execution/Shell;->closeQuietly(Ljava/io/Reader;)V

    return-void
.end method

.method static synthetic access$1402(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 43
    sput-object p0, Lcom/stericson/roottools/execution/Shell;->error:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/stericson/roottools/execution/Shell;)Z
    .locals 1
    .param p0, "x0"    # Lcom/stericson/roottools/execution/Shell;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/stericson/roottools/execution/Shell;->close:Z

    return v0
.end method

.method static synthetic access$300(Lcom/stericson/roottools/execution/Shell;)I
    .locals 1
    .param p0, "x0"    # Lcom/stericson/roottools/execution/Shell;

    .prologue
    .line 43
    iget v0, p0, Lcom/stericson/roottools/execution/Shell;->write:I

    return v0
.end method

.method static synthetic access$302(Lcom/stericson/roottools/execution/Shell;I)I
    .locals 0
    .param p0, "x0"    # Lcom/stericson/roottools/execution/Shell;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/stericson/roottools/execution/Shell;->write:I

    return p1
.end method

.method static synthetic access$308(Lcom/stericson/roottools/execution/Shell;)I
    .locals 2
    .param p0, "x0"    # Lcom/stericson/roottools/execution/Shell;

    .prologue
    .line 43
    iget v0, p0, Lcom/stericson/roottools/execution/Shell;->write:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/stericson/roottools/execution/Shell;->write:I

    return v0
.end method

.method static synthetic access$400(Lcom/stericson/roottools/execution/Shell;)I
    .locals 1
    .param p0, "x0"    # Lcom/stericson/roottools/execution/Shell;

    .prologue
    .line 43
    iget v0, p0, Lcom/stericson/roottools/execution/Shell;->read:I

    return v0
.end method

.method static synthetic access$402(Lcom/stericson/roottools/execution/Shell;I)I
    .locals 0
    .param p0, "x0"    # Lcom/stericson/roottools/execution/Shell;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/stericson/roottools/execution/Shell;->read:I

    return p1
.end method

.method static synthetic access$408(Lcom/stericson/roottools/execution/Shell;)I
    .locals 2
    .param p0, "x0"    # Lcom/stericson/roottools/execution/Shell;

    .prologue
    .line 43
    iget v0, p0, Lcom/stericson/roottools/execution/Shell;->read:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/stericson/roottools/execution/Shell;->read:I

    return v0
.end method

.method static synthetic access$500(Lcom/stericson/roottools/execution/Shell;)V
    .locals 0
    .param p0, "x0"    # Lcom/stericson/roottools/execution/Shell;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/stericson/roottools/execution/Shell;->cleanCommands()V

    return-void
.end method

.method static synthetic access$600(Lcom/stericson/roottools/execution/Shell;)Ljava/io/OutputStreamWriter;
    .locals 1
    .param p0, "x0"    # Lcom/stericson/roottools/execution/Shell;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/stericson/roottools/execution/Shell;->outputStream:Ljava/io/OutputStreamWriter;

    return-object v0
.end method

.method static synthetic access$700(Lcom/stericson/roottools/execution/Shell;)I
    .locals 1
    .param p0, "x0"    # Lcom/stericson/roottools/execution/Shell;

    .prologue
    .line 43
    iget v0, p0, Lcom/stericson/roottools/execution/Shell;->totalExecuted:I

    return v0
.end method

.method static synthetic access$708(Lcom/stericson/roottools/execution/Shell;)I
    .locals 2
    .param p0, "x0"    # Lcom/stericson/roottools/execution/Shell;

    .prologue
    .line 43
    iget v0, p0, Lcom/stericson/roottools/execution/Shell;->totalExecuted:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/stericson/roottools/execution/Shell;->totalExecuted:I

    return v0
.end method

.method static synthetic access$800(Lcom/stericson/roottools/execution/Shell;Ljava/io/Writer;)V
    .locals 0
    .param p0, "x0"    # Lcom/stericson/roottools/execution/Shell;
    .param p1, "x1"    # Ljava/io/Writer;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/stericson/roottools/execution/Shell;->closeQuietly(Ljava/io/Writer;)V

    return-void
.end method

.method static synthetic access$900(Lcom/stericson/roottools/execution/Shell;)Ljava/io/BufferedReader;
    .locals 1
    .param p0, "x0"    # Lcom/stericson/roottools/execution/Shell;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/stericson/roottools/execution/Shell;->inputStream:Ljava/io/BufferedReader;

    return-object v0
.end method

.method private cleanCommands()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 179
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/stericson/roottools/execution/Shell;->isCleaning:Z

    .line 180
    const/16 v2, 0x2ee

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 181
    .local v1, "toClean":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cleaning up: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 182
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 183
    iget-object v2, p0, Lcom/stericson/roottools/execution/Shell;->commands:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 182
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 186
    :cond_0
    iget-object v2, p0, Lcom/stericson/roottools/execution/Shell;->commands:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/stericson/roottools/execution/Shell;->read:I

    .line 187
    iget-object v2, p0, Lcom/stericson/roottools/execution/Shell;->commands:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/stericson/roottools/execution/Shell;->write:I

    .line 188
    iput-boolean v4, p0, Lcom/stericson/roottools/execution/Shell;->isCleaning:Z

    .line 189
    return-void
.end method

.method public static closeAll()V
    .locals 0

    .prologue
    .line 251
    invoke-static {}, Lcom/stericson/roottools/execution/Shell;->closeShell()V

    .line 252
    invoke-static {}, Lcom/stericson/roottools/execution/Shell;->closeRootShell()V

    .line 253
    invoke-static {}, Lcom/stericson/roottools/execution/Shell;->closeCustomShell()V

    .line 254
    return-void
.end method

.method public static closeCustomShell()V
    .locals 1

    .prologue
    .line 236
    sget-object v0, Lcom/stericson/roottools/execution/Shell;->customShell:Lcom/stericson/roottools/execution/Shell;

    if-nez v0, :cond_0

    .line 238
    :goto_0
    return-void

    .line 237
    :cond_0
    sget-object v0, Lcom/stericson/roottools/execution/Shell;->customShell:Lcom/stericson/roottools/execution/Shell;

    invoke-virtual {v0}, Lcom/stericson/roottools/execution/Shell;->close()V

    goto :goto_0
.end method

.method private closeQuietly(Ljava/io/Reader;)V
    .locals 1
    .param p1, "input"    # Ljava/io/Reader;

    .prologue
    .line 193
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/Reader;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 194
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private closeQuietly(Ljava/io/Writer;)V
    .locals 1
    .param p1, "output"    # Ljava/io/Writer;

    .prologue
    .line 199
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/Writer;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 200
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static closeRootShell()V
    .locals 1

    .prologue
    .line 241
    sget-object v0, Lcom/stericson/roottools/execution/Shell;->rootShell:Lcom/stericson/roottools/execution/Shell;

    if-nez v0, :cond_0

    .line 243
    :goto_0
    return-void

    .line 242
    :cond_0
    sget-object v0, Lcom/stericson/roottools/execution/Shell;->rootShell:Lcom/stericson/roottools/execution/Shell;

    invoke-virtual {v0}, Lcom/stericson/roottools/execution/Shell;->close()V

    goto :goto_0
.end method

.method public static closeShell()V
    .locals 1

    .prologue
    .line 246
    sget-object v0, Lcom/stericson/roottools/execution/Shell;->shell:Lcom/stericson/roottools/execution/Shell;

    if-nez v0, :cond_0

    .line 248
    :goto_0
    return-void

    .line 247
    :cond_0
    sget-object v0, Lcom/stericson/roottools/execution/Shell;->shell:Lcom/stericson/roottools/execution/Shell;

    invoke-virtual {v0}, Lcom/stericson/roottools/execution/Shell;->close()V

    goto :goto_0
.end method

.method public static getOpenShell()Lcom/stericson/roottools/execution/Shell;
    .locals 1

    .prologue
    .line 264
    sget-object v0, Lcom/stericson/roottools/execution/Shell;->customShell:Lcom/stericson/roottools/execution/Shell;

    if-eqz v0, :cond_0

    .line 265
    sget-object v0, Lcom/stericson/roottools/execution/Shell;->customShell:Lcom/stericson/roottools/execution/Shell;

    .line 268
    :goto_0
    return-object v0

    .line 266
    :cond_0
    sget-object v0, Lcom/stericson/roottools/execution/Shell;->rootShell:Lcom/stericson/roottools/execution/Shell;

    if-eqz v0, :cond_1

    .line 267
    sget-object v0, Lcom/stericson/roottools/execution/Shell;->rootShell:Lcom/stericson/roottools/execution/Shell;

    goto :goto_0

    .line 268
    :cond_1
    sget-object v0, Lcom/stericson/roottools/execution/Shell;->shell:Lcom/stericson/roottools/execution/Shell;

    goto :goto_0
.end method

.method public static isAnyShellOpen()Z
    .locals 1

    .prologue
    .line 278
    sget-object v0, Lcom/stericson/roottools/execution/Shell;->shell:Lcom/stericson/roottools/execution/Shell;

    if-nez v0, :cond_0

    sget-object v0, Lcom/stericson/roottools/execution/Shell;->rootShell:Lcom/stericson/roottools/execution/Shell;

    if-nez v0, :cond_0

    sget-object v0, Lcom/stericson/roottools/execution/Shell;->customShell:Lcom/stericson/roottools/execution/Shell;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static startRootShell()Lcom/stericson/roottools/execution/Shell;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/concurrent/TimeoutException;,
            Lcom/stericson/roottools/exceptions/RootDeniedException;
        }
    .end annotation

    .prologue
    .line 506
    const/16 v0, 0x4e20

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/stericson/roottools/execution/Shell;->startRootShell(II)Lcom/stericson/roottools/execution/Shell;

    move-result-object v0

    return-object v0
.end method

.method public static startRootShell(I)Lcom/stericson/roottools/execution/Shell;
    .locals 1
    .param p0, "timeout"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/concurrent/TimeoutException;,
            Lcom/stericson/roottools/exceptions/RootDeniedException;
        }
    .end annotation

    .prologue
    .line 511
    const/4 v0, 0x3

    invoke-static {p0, v0}, Lcom/stericson/roottools/execution/Shell;->startRootShell(II)Lcom/stericson/roottools/execution/Shell;

    move-result-object v0

    return-object v0
.end method

.method public static startRootShell(II)Lcom/stericson/roottools/execution/Shell;
    .locals 5
    .param p0, "timeout"    # I
    .param p1, "retry"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/concurrent/TimeoutException;,
            Lcom/stericson/roottools/exceptions/RootDeniedException;
        }
    .end annotation

    .prologue
    .line 517
    sput p0, Lcom/stericson/roottools/execution/Shell;->shellTimeout:I

    .line 519
    sget-object v4, Lcom/stericson/roottools/execution/Shell;->rootShell:Lcom/stericson/roottools/execution/Shell;

    if-nez v4, :cond_1

    .line 520
    const-string v4, "Starting Root Shell!"

    invoke-static {v4}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 521
    const-string v0, "su"

    .line 523
    .local v0, "cmd":Ljava/lang/String;
    const/4 v2, 0x0

    .line 524
    .local v2, "retries":I
    :goto_0
    sget-object v4, Lcom/stericson/roottools/execution/Shell;->rootShell:Lcom/stericson/roottools/execution/Shell;

    if-nez v4, :cond_2

    .line 526
    :try_start_0
    new-instance v4, Lcom/stericson/roottools/execution/Shell;

    invoke-direct {v4, v0}, Lcom/stericson/roottools/execution/Shell;-><init>(Ljava/lang/String;)V

    sput-object v4, Lcom/stericson/roottools/execution/Shell;->rootShell:Lcom/stericson/roottools/execution/Shell;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 527
    :catch_0
    move-exception v1

    .line 528
    .local v1, "e":Ljava/io/IOException;
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "retries":I
    .local v3, "retries":I
    if-lt v2, p1, :cond_0

    .line 529
    const-string v4, "IOException, could not start shell"

    invoke-static {v4}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 530
    throw v1

    :cond_0
    move v2, v3

    .line 532
    .end local v3    # "retries":I
    .restart local v2    # "retries":I
    goto :goto_0

    .line 535
    .end local v0    # "cmd":Ljava/lang/String;
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "retries":I
    :cond_1
    const-string v4, "Using Existing Root Shell!"

    invoke-static {v4}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 538
    :cond_2
    sget-object v4, Lcom/stericson/roottools/execution/Shell;->rootShell:Lcom/stericson/roottools/execution/Shell;

    return-object v4
.end method

.method public static startShell()Lcom/stericson/roottools/execution/Shell;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 559
    const/16 v0, 0x4e20

    invoke-static {v0}, Lcom/stericson/roottools/execution/Shell;->startShell(I)Lcom/stericson/roottools/execution/Shell;

    move-result-object v0

    return-object v0
.end method

.method public static startShell(I)Lcom/stericson/roottools/execution/Shell;
    .locals 3
    .param p0, "timeout"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 563
    sput p0, Lcom/stericson/roottools/execution/Shell;->shellTimeout:I

    .line 566
    :try_start_0
    sget-object v1, Lcom/stericson/roottools/execution/Shell;->shell:Lcom/stericson/roottools/execution/Shell;

    if-nez v1, :cond_0

    .line 567
    const-string v1, "Starting Shell!"

    invoke-static {v1}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 568
    new-instance v1, Lcom/stericson/roottools/execution/Shell;

    const-string v2, "/system/bin/sh"

    invoke-direct {v1, v2}, Lcom/stericson/roottools/execution/Shell;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/stericson/roottools/execution/Shell;->shell:Lcom/stericson/roottools/execution/Shell;

    .line 570
    :goto_0
    sget-object v1, Lcom/stericson/roottools/execution/Shell;->shell:Lcom/stericson/roottools/execution/Shell;

    return-object v1

    .line 569
    :cond_0
    const-string v1, "Using Existing Shell!"

    invoke-static {v1}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/stericson/roottools/exceptions/RootDeniedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 571
    :catch_0
    move-exception v0

    .line 573
    .local v0, "e":Lcom/stericson/roottools/exceptions/RootDeniedException;
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1
.end method


# virtual methods
.method public add(Lcom/stericson/roottools/execution/Command;)Lcom/stericson/roottools/execution/Command;
    .locals 2
    .param p1, "command"    # Lcom/stericson/roottools/execution/Command;

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/stericson/roottools/execution/Shell;->close:Z

    if-eqz v0, :cond_0

    .line 160
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to add commands to a closed shell"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 164
    :cond_0
    iget-boolean v0, p0, Lcom/stericson/roottools/execution/Shell;->isCleaning:Z

    if-nez v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/stericson/roottools/execution/Shell;->commands:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    invoke-virtual {p0}, Lcom/stericson/roottools/execution/Shell;->notifyThreads()V

    .line 171
    return-object p1
.end method

.method public close()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 204
    iget-object v2, p0, Lcom/stericson/roottools/execution/Shell;->commands:Ljava/util/List;

    monitor-enter v2

    .line 209
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/stericson/roottools/execution/Shell;->close:Z

    .line 210
    invoke-virtual {p0}, Lcom/stericson/roottools/execution/Shell;->notifyThreads()V

    .line 211
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213
    const/4 v0, 0x0

    .line 214
    .local v0, "count":I
    :cond_0
    sget-boolean v1, Lcom/stericson/roottools/execution/Shell;->isExecuting:Z

    if-eqz v1, :cond_1

    .line 215
    const-string v1, "Waiting on shell to finish executing before closing..."

    invoke-static {v1}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 216
    add-int/lit8 v0, v0, 0x1

    .line 219
    const/16 v1, 0x3e8

    if-le v0, v1, :cond_0

    .line 224
    :cond_1
    const-string v1, "Shell Closed!"

    invoke-static {v1}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 226
    sget-object v1, Lcom/stericson/roottools/execution/Shell;->rootShell:Lcom/stericson/roottools/execution/Shell;

    if-ne p0, v1, :cond_3

    .line 227
    sput-object v3, Lcom/stericson/roottools/execution/Shell;->rootShell:Lcom/stericson/roottools/execution/Shell;

    .line 233
    :cond_2
    :goto_0
    return-void

    .line 211
    .end local v0    # "count":I
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 228
    .restart local v0    # "count":I
    :cond_3
    sget-object v1, Lcom/stericson/roottools/execution/Shell;->shell:Lcom/stericson/roottools/execution/Shell;

    if-ne p0, v1, :cond_4

    .line 229
    sput-object v3, Lcom/stericson/roottools/execution/Shell;->shell:Lcom/stericson/roottools/execution/Shell;

    goto :goto_0

    .line 230
    :cond_4
    sget-object v1, Lcom/stericson/roottools/execution/Shell;->customShell:Lcom/stericson/roottools/execution/Shell;

    if-ne p0, v1, :cond_2

    .line 231
    sput-object v3, Lcom/stericson/roottools/execution/Shell;->customShell:Lcom/stericson/roottools/execution/Shell;

    goto :goto_0
.end method

.method public getCommandQueuePosition(Lcom/stericson/roottools/execution/Command;)I
    .locals 1
    .param p1, "cmd"    # Lcom/stericson/roottools/execution/Command;

    .prologue
    .line 256
    iget-object v0, p0, Lcom/stericson/roottools/execution/Shell;->commands:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getCommandQueuePositionString(Lcom/stericson/roottools/execution/Command;)Ljava/lang/String;
    .locals 2
    .param p1, "cmd"    # Lcom/stericson/roottools/execution/Command;

    .prologue
    .line 259
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Command is in position "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/stericson/roottools/execution/Shell;->getCommandQueuePosition(Lcom/stericson/roottools/execution/Command;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " currently executing command at position "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/stericson/roottools/execution/Shell;->write:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isClosed()Z
    .locals 1

    .prologue
    .line 156
    iget-boolean v0, p0, Lcom/stericson/roottools/execution/Shell;->close:Z

    return v0
.end method

.method protected notifyThreads()V
    .locals 1

    .prologue
    .line 359
    new-instance v0, Lcom/stericson/roottools/execution/Shell$2;

    invoke-direct {v0, p0}, Lcom/stericson/roottools/execution/Shell$2;-><init>(Lcom/stericson/roottools/execution/Shell;)V

    invoke-virtual {v0}, Lcom/stericson/roottools/execution/Shell$2;->start()V

    .line 366
    return-void
.end method

.method public processErrors(Lcom/stericson/roottools/execution/Command;)V
    .locals 4
    .param p1, "command"    # Lcom/stericson/roottools/execution/Command;

    .prologue
    .line 476
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/stericson/roottools/execution/Shell;->errorStream:Ljava/io/BufferedReader;

    invoke-virtual {v2}, Ljava/io/BufferedReader;->ready()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    .line 477
    iget-object v2, p0, Lcom/stericson/roottools/execution/Shell;->errorStream:Ljava/io/BufferedReader;

    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .line 482
    .local v1, "line":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 494
    .end local v1    # "line":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 489
    .restart local v1    # "line":Ljava/lang/String;
    :cond_1
    iget v2, p1, Lcom/stericson/roottools/execution/Command;->id:I

    invoke-virtual {p1, v2, v1}, Lcom/stericson/roottools/execution/Command;->output(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 491
    .end local v1    # "line":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 492
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v2, v3, v0}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;ILjava/lang/Exception;)V

    goto :goto_1
.end method
