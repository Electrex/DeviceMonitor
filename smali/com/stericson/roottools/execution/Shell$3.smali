.class Lcom/stericson/roottools/execution/Shell$3;
.super Ljava/lang/Object;
.source "Shell.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/stericson/roottools/execution/Shell;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/stericson/roottools/execution/Shell;


# direct methods
.method constructor <init>(Lcom/stericson/roottools/execution/Shell;)V
    .locals 0

    .prologue
    .line 371
    iput-object p1, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 374
    const/4 v1, 0x0

    .line 376
    .local v1, "command":Lcom/stericson/roottools/execution/Command;
    :cond_0
    :goto_0
    :try_start_0
    iget-object v8, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->close:Z
    invoke-static {v8}, Lcom/stericson/roottools/execution/Shell;->access$200(Lcom/stericson/roottools/execution/Shell;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 377
    const/4 v8, 0x0

    sput-boolean v8, Lcom/stericson/roottools/execution/Shell;->isReading:Z

    .line 378
    iget-object v8, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->inputStream:Ljava/io/BufferedReader;
    invoke-static {v8}, Lcom/stericson/roottools/execution/Shell;->access$900(Lcom/stericson/roottools/execution/Shell;)Ljava/io/BufferedReader;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .line 379
    .local v6, "outputLine":Ljava/lang/String;
    const/4 v8, 0x1

    sput-boolean v8, Lcom/stericson/roottools/execution/Shell;->isReading:Z

    .line 384
    if-nez v6, :cond_3

    .line 444
    .end local v6    # "outputLine":Ljava/lang/String;
    :cond_1
    :goto_1
    const-string v8, "Read all output"

    invoke-static {v8}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 446
    :try_start_1
    iget-object v8, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->proc:Ljava/lang/Process;
    invoke-static {v8}, Lcom/stericson/roottools/execution/Shell;->access$1100(Lcom/stericson/roottools/execution/Shell;)Ljava/lang/Process;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Process;->waitFor()I

    .line 447
    iget-object v8, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->proc:Ljava/lang/Process;
    invoke-static {v8}, Lcom/stericson/roottools/execution/Shell;->access$1100(Lcom/stericson/roottools/execution/Shell;)Ljava/lang/Process;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Process;->destroy()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 450
    :goto_2
    :try_start_2
    iget-object v8, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    iget-object v9, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->outputStream:Ljava/io/OutputStreamWriter;
    invoke-static {v9}, Lcom/stericson/roottools/execution/Shell;->access$600(Lcom/stericson/roottools/execution/Shell;)Ljava/io/OutputStreamWriter;

    move-result-object v9

    # invokes: Lcom/stericson/roottools/execution/Shell;->closeQuietly(Ljava/io/Writer;)V
    invoke-static {v8, v9}, Lcom/stericson/roottools/execution/Shell;->access$800(Lcom/stericson/roottools/execution/Shell;Ljava/io/Writer;)V

    .line 451
    iget-object v8, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    iget-object v9, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->errorStream:Ljava/io/BufferedReader;
    invoke-static {v9}, Lcom/stericson/roottools/execution/Shell;->access$1200(Lcom/stericson/roottools/execution/Shell;)Ljava/io/BufferedReader;

    move-result-object v9

    # invokes: Lcom/stericson/roottools/execution/Shell;->closeQuietly(Ljava/io/Reader;)V
    invoke-static {v8, v9}, Lcom/stericson/roottools/execution/Shell;->access$1300(Lcom/stericson/roottools/execution/Shell;Ljava/io/Reader;)V

    .line 452
    iget-object v8, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    iget-object v9, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->inputStream:Ljava/io/BufferedReader;
    invoke-static {v9}, Lcom/stericson/roottools/execution/Shell;->access$900(Lcom/stericson/roottools/execution/Shell;)Ljava/io/BufferedReader;

    move-result-object v9

    # invokes: Lcom/stericson/roottools/execution/Shell;->closeQuietly(Ljava/io/Reader;)V
    invoke-static {v8, v9}, Lcom/stericson/roottools/execution/Shell;->access$1300(Lcom/stericson/roottools/execution/Shell;Ljava/io/Reader;)V

    .line 454
    :goto_3
    iget-object v8, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->read:I
    invoke-static {v8}, Lcom/stericson/roottools/execution/Shell;->access$400(Lcom/stericson/roottools/execution/Shell;)I

    move-result v8

    iget-object v9, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->commands:Ljava/util/List;
    invoke-static {v9}, Lcom/stericson/roottools/execution/Shell;->access$100(Lcom/stericson/roottools/execution/Shell;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-ge v8, v9, :cond_8

    .line 455
    if-nez v1, :cond_2

    iget-object v8, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->commands:Ljava/util/List;
    invoke-static {v8}, Lcom/stericson/roottools/execution/Shell;->access$100(Lcom/stericson/roottools/execution/Shell;)Ljava/util/List;

    move-result-object v8

    iget-object v9, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->read:I
    invoke-static {v9}, Lcom/stericson/roottools/execution/Shell;->access$400(Lcom/stericson/roottools/execution/Shell;)I

    move-result v9

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Lcom/stericson/roottools/execution/Command;

    move-object v1, v0

    .line 457
    :cond_2
    const-string v8, "Unexpected Termination."

    invoke-virtual {v1, v8}, Lcom/stericson/roottools/execution/Command;->terminated(Ljava/lang/String;)V

    .line 458
    const/4 v1, 0x0

    .line 459
    iget-object v8, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    # operator++ for: Lcom/stericson/roottools/execution/Shell;->read:I
    invoke-static {v8}, Lcom/stericson/roottools/execution/Shell;->access$408(Lcom/stericson/roottools/execution/Shell;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 464
    :catch_0
    move-exception v2

    .line 465
    .local v2, "e":Ljava/io/IOException;
    :try_start_3
    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    invoke-static {v8, v9, v2}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;ILjava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 467
    const-string v8, "Shell destroyed"

    invoke-static {v8}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 468
    iget-object v8, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    iput-boolean v11, v8, Lcom/stericson/roottools/execution/Shell;->isClosed:Z

    .line 469
    sput-boolean v10, Lcom/stericson/roottools/execution/Shell;->isReading:Z

    .line 471
    .end local v2    # "e":Ljava/io/IOException;
    :goto_4
    return-void

    .line 386
    .restart local v6    # "outputLine":Ljava/lang/String;
    :cond_3
    if-nez v1, :cond_5

    .line 387
    :try_start_4
    iget-object v8, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->read:I
    invoke-static {v8}, Lcom/stericson/roottools/execution/Shell;->access$400(Lcom/stericson/roottools/execution/Shell;)I

    move-result v8

    iget-object v9, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->commands:Ljava/util/List;
    invoke-static {v9}, Lcom/stericson/roottools/execution/Shell;->access$100(Lcom/stericson/roottools/execution/Shell;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-lt v8, v9, :cond_4

    .line 388
    iget-object v8, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->close:Z
    invoke-static {v8}, Lcom/stericson/roottools/execution/Shell;->access$200(Lcom/stericson/roottools/execution/Shell;)Z

    move-result v8

    if-eqz v8, :cond_0

    goto/16 :goto_1

    .line 392
    :cond_4
    iget-object v8, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->commands:Ljava/util/List;
    invoke-static {v8}, Lcom/stericson/roottools/execution/Shell;->access$100(Lcom/stericson/roottools/execution/Shell;)Ljava/util/List;

    move-result-object v8

    iget-object v9, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->read:I
    invoke-static {v9}, Lcom/stericson/roottools/execution/Shell;->access$400(Lcom/stericson/roottools/execution/Shell;)I

    move-result v9

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Lcom/stericson/roottools/execution/Command;

    move-object v1, v0

    .line 400
    :cond_5
    const-string v8, "F*D^W@#FGF"

    invoke-virtual {v6, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 403
    .local v7, "pos":I
    const/4 v8, -0x1

    if-ne v7, v8, :cond_7

    .line 407
    iget v8, v1, Lcom/stericson/roottools/execution/Command;->id:I

    invoke-virtual {v1, v8, v6}, Lcom/stericson/roottools/execution/Command;->output(ILjava/lang/String;)V

    .line 414
    :cond_6
    :goto_5
    if-ltz v7, :cond_0

    .line 415
    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 416
    const-string v8, " "

    invoke-virtual {v6, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 418
    .local v4, "fields":[Ljava/lang/String;
    array-length v8, v4

    if-lt v8, v12, :cond_0

    const/4 v8, 0x1

    aget-object v8, v4, v8
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v8, :cond_0

    .line 419
    const/4 v5, 0x0

    .line 422
    .local v5, "id":I
    const/4 v8, 0x1

    :try_start_5
    aget-object v8, v4, v8

    invoke-static {v8}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v5

    .line 425
    :goto_6
    const/4 v3, -0x1

    .line 428
    .local v3, "exitCode":I
    const/4 v8, 0x2

    :try_start_6
    aget-object v8, v4, v8

    invoke-static {v8}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v3

    .line 431
    :goto_7
    :try_start_7
    iget-object v8, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    # getter for: Lcom/stericson/roottools/execution/Shell;->totalRead:I
    invoke-static {v8}, Lcom/stericson/roottools/execution/Shell;->access$1000(Lcom/stericson/roottools/execution/Shell;)I

    move-result v8

    if-ne v5, v8, :cond_0

    .line 432
    iget-object v8, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    invoke-virtual {v8, v1}, Lcom/stericson/roottools/execution/Shell;->processErrors(Lcom/stericson/roottools/execution/Command;)V

    .line 433
    invoke-virtual {v1, v3}, Lcom/stericson/roottools/execution/Command;->setExitCode(I)V

    .line 434
    invoke-virtual {v1}, Lcom/stericson/roottools/execution/Command;->commandFinished()V

    .line 435
    const/4 v1, 0x0

    .line 437
    iget-object v8, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    # operator++ for: Lcom/stericson/roottools/execution/Shell;->read:I
    invoke-static {v8}, Lcom/stericson/roottools/execution/Shell;->access$408(Lcom/stericson/roottools/execution/Shell;)I

    .line 438
    iget-object v8, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    # operator++ for: Lcom/stericson/roottools/execution/Shell;->totalRead:I
    invoke-static {v8}, Lcom/stericson/roottools/execution/Shell;->access$1008(Lcom/stericson/roottools/execution/Shell;)I
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 467
    .end local v3    # "exitCode":I
    .end local v4    # "fields":[Ljava/lang/String;
    .end local v5    # "id":I
    .end local v6    # "outputLine":Ljava/lang/String;
    .end local v7    # "pos":I
    :catchall_0
    move-exception v8

    const-string v9, "Shell destroyed"

    invoke-static {v9}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 468
    iget-object v9, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    iput-boolean v11, v9, Lcom/stericson/roottools/execution/Shell;->isClosed:Z

    .line 469
    sput-boolean v10, Lcom/stericson/roottools/execution/Shell;->isReading:Z

    throw v8

    .line 408
    .restart local v6    # "outputLine":Ljava/lang/String;
    .restart local v7    # "pos":I
    :cond_7
    if-lez v7, :cond_6

    .line 412
    :try_start_8
    iget v8, v1, Lcom/stericson/roottools/execution/Command;->id:I

    const/4 v9, 0x0

    invoke-virtual {v6, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/stericson/roottools/execution/Command;->output(ILjava/lang/String;)V

    goto :goto_5

    .line 462
    .end local v6    # "outputLine":Ljava/lang/String;
    .end local v7    # "pos":I
    :cond_8
    iget-object v8, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    const/4 v9, 0x0

    # setter for: Lcom/stericson/roottools/execution/Shell;->read:I
    invoke-static {v8, v9}, Lcom/stericson/roottools/execution/Shell;->access$402(Lcom/stericson/roottools/execution/Shell;I)I
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 467
    const-string v8, "Shell destroyed"

    invoke-static {v8}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;)V

    .line 468
    iget-object v8, p0, Lcom/stericson/roottools/execution/Shell$3;->this$0:Lcom/stericson/roottools/execution/Shell;

    iput-boolean v11, v8, Lcom/stericson/roottools/execution/Shell;->isClosed:Z

    .line 469
    sput-boolean v10, Lcom/stericson/roottools/execution/Shell;->isReading:Z

    goto/16 :goto_4

    .line 448
    :catch_1
    move-exception v8

    goto/16 :goto_2

    .line 429
    .restart local v3    # "exitCode":I
    .restart local v4    # "fields":[Ljava/lang/String;
    .restart local v5    # "id":I
    .restart local v6    # "outputLine":Ljava/lang/String;
    .restart local v7    # "pos":I
    :catch_2
    move-exception v8

    goto :goto_7

    .line 423
    .end local v3    # "exitCode":I
    :catch_3
    move-exception v8

    goto :goto_6
.end method
