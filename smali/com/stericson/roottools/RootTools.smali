.class public final Lcom/stericson/roottools/RootTools;
.super Ljava/lang/Object;
.source "RootTools.java"


# static fields
.field public static debugMode:Z

.field public static final lastFoundBinaryPaths:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static rim:Lcom/stericson/roottools/internal/RootToolsInternalMethods;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    sput-object v0, Lcom/stericson/roottools/RootTools;->rim:Lcom/stericson/roottools/internal/RootToolsInternalMethods;

    .line 70
    const/4 v0, 0x0

    sput-boolean v0, Lcom/stericson/roottools/RootTools;->debugMode:Z

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/stericson/roottools/RootTools;->lastFoundBinaryPaths:Ljava/util/List;

    return-void
.end method

.method public static closeAllShells()V
    .locals 0

    .prologue
    .line 114
    invoke-static {}, Lcom/stericson/roottools/execution/Shell;->closeAll()V

    return-void
.end method

.method public static closeShell(Z)V
    .locals 0
    .param p0, "root"    # Z

    .prologue
    .line 123
    if-eqz p0, :cond_0

    invoke-static {}, Lcom/stericson/roottools/execution/Shell;->closeRootShell()V

    .line 124
    :goto_0
    return-void

    .line 123
    :cond_0
    invoke-static {}, Lcom/stericson/roottools/execution/Shell;->closeShell()V

    goto :goto_0
.end method

.method public static exists(Ljava/lang/String;)Z
    .locals 1
    .param p0, "file"    # Ljava/lang/String;

    .prologue
    .line 133
    invoke-static {}, Lcom/stericson/roottools/RootTools;->getInternals()Lcom/stericson/roottools/internal/RootToolsInternalMethods;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/stericson/roottools/internal/RootToolsInternalMethods;->exists(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static findBinary(Ljava/lang/String;)Z
    .locals 1
    .param p0, "binaryName"    # Ljava/lang/String;

    .prologue
    .line 142
    invoke-static {}, Lcom/stericson/roottools/RootTools;->getInternals()Lcom/stericson/roottools/internal/RootToolsInternalMethods;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/stericson/roottools/internal/RootToolsInternalMethods;->findBinary(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static getInternals()Lcom/stericson/roottools/internal/RootToolsInternalMethods;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/stericson/roottools/RootTools;->rim:Lcom/stericson/roottools/internal/RootToolsInternalMethods;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/stericson/roottools/internal/RootToolsInternalMethods;->getInstance()V

    .line 63
    :cond_0
    sget-object v0, Lcom/stericson/roottools/RootTools;->rim:Lcom/stericson/roottools/internal/RootToolsInternalMethods;

    return-object v0
.end method

.method public static getPath()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 265
    const-string v0, "PATH"

    invoke-static {v0}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getShell(Z)Lcom/stericson/roottools/execution/Shell;
    .locals 1
    .param p0, "root"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/concurrent/TimeoutException;,
            Lcom/stericson/roottools/exceptions/RootDeniedException;
        }
    .end annotation

    .prologue
    .line 300
    const/16 v0, 0x61a8

    invoke-static {p0, v0}, Lcom/stericson/roottools/RootTools;->getShell(ZI)Lcom/stericson/roottools/execution/Shell;

    move-result-object v0

    return-object v0
.end method

.method public static getShell(ZI)Lcom/stericson/roottools/execution/Shell;
    .locals 1
    .param p0, "root"    # Z
    .param p1, "timeout"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/concurrent/TimeoutException;,
            Lcom/stericson/roottools/exceptions/RootDeniedException;
        }
    .end annotation

    .prologue
    .line 282
    if-eqz p0, :cond_0

    invoke-static {p1}, Lcom/stericson/roottools/execution/Shell;->startRootShell(I)Lcom/stericson/roottools/execution/Shell;

    move-result-object v0

    .line 283
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/stericson/roottools/execution/Shell;->startShell(I)Lcom/stericson/roottools/execution/Shell;

    move-result-object v0

    goto :goto_0
.end method

.method public static isAccessGiven()Z
    .locals 1

    .prologue
    .line 384
    invoke-static {}, Lcom/stericson/roottools/RootTools;->getInternals()Lcom/stericson/roottools/internal/RootToolsInternalMethods;

    move-result-object v0

    invoke-virtual {v0}, Lcom/stericson/roottools/internal/RootToolsInternalMethods;->isAccessGiven()Z

    move-result v0

    return v0
.end method

.method public static isBusyboxAvailable()Z
    .locals 1

    .prologue
    .line 389
    const-string v0, "busybox"

    invoke-static {v0}, Lcom/stericson/roottools/RootTools;->findBinary(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isRootAvailable()Z
    .locals 1

    .prologue
    .line 406
    const-string v0, "su"

    invoke-static {v0}, Lcom/stericson/roottools/RootTools;->findBinary(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static log(Ljava/lang/String;)V
    .locals 2
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 427
    const/4 v0, 0x3

    invoke-static {v1, p0, v0, v1}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Exception;)V

    return-void
.end method

.method public static log(Ljava/lang/String;ILjava/lang/Exception;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;
    .param p1, "type"    # I
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    .line 459
    const/4 v0, 0x0

    invoke-static {v0, p0, p1, p2}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Exception;)V

    .line 460
    return-void
.end method

.method public static log(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "TAG"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 442
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/stericson/roottools/RootTools;->log(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Exception;)V

    return-void
.end method

.method public static log(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Exception;)V
    .locals 1
    .param p0, "TAG"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "e"    # Ljava/lang/Exception;

    .prologue
    .line 478
    sget-boolean v0, Lcom/stericson/roottools/RootTools;->debugMode:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 497
    :cond_0
    :goto_0
    return-void

    .line 482
    :cond_1
    if-nez p0, :cond_2

    .line 483
    const-string p0, "RootTools v3.4"

    .line 486
    :cond_2
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 488
    :pswitch_0
    invoke-static {p0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 491
    :pswitch_1
    invoke-static {p0, p1, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 494
    :pswitch_2
    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 486
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static offerBusyBox()V
    .locals 1

    .prologue
    .line 412
    invoke-static {}, Lcom/stericson/roottools/RootTools;->getInternals()Lcom/stericson/roottools/internal/RootToolsInternalMethods;

    move-result-object v0

    invoke-virtual {v0}, Lcom/stericson/roottools/internal/RootToolsInternalMethods;->offerBusyBox()V

    .line 413
    return-void
.end method

.method public static setRim(Lcom/stericson/roottools/internal/RootToolsInternalMethods;)V
    .locals 0
    .param p0, "rim"    # Lcom/stericson/roottools/internal/RootToolsInternalMethods;

    .prologue
    .line 59
    sput-object p0, Lcom/stericson/roottools/RootTools;->rim:Lcom/stericson/roottools/internal/RootToolsInternalMethods;

    return-void
.end method
