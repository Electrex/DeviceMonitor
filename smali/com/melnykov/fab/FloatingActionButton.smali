.class public Lcom/melnykov/fab/FloatingActionButton;
.super Landroid/widget/ImageButton;
.source "FloatingActionButton.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/melnykov/fab/FloatingActionButton$ScrollViewScrollDetectorImpl;
    }
.end annotation


# instance fields
.field private mColorNormal:I

.field private mColorPressed:I

.field private mColorRipple:I

.field private final mInterpolator:Landroid/view/animation/Interpolator;

.field private mMarginsSet:Z

.field private mScrollThreshold:I

.field private mShadow:Z

.field private mShadowSize:I

.field private mType:I

.field private mVisible:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/melnykov/fab/FloatingActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/melnykov/fab/FloatingActionButton;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 69
    invoke-direct {p0, p1, p2}, Lcom/melnykov/fab/FloatingActionButton;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/melnykov/fab/FloatingActionButton;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 74
    invoke-direct {p0, p1, p2}, Lcom/melnykov/fab/FloatingActionButton;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/melnykov/fab/FloatingActionButton;)I
    .locals 1
    .param p0, "x0"    # Lcom/melnykov/fab/FloatingActionButton;

    .prologue
    .line 37
    iget v0, p0, Lcom/melnykov/fab/FloatingActionButton;->mType:I

    return v0
.end method

.method static synthetic access$100(Lcom/melnykov/fab/FloatingActionButton;I)I
    .locals 1
    .param p0, "x0"    # Lcom/melnykov/fab/FloatingActionButton;
    .param p1, "x1"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/melnykov/fab/FloatingActionButton;->getDimension(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/melnykov/fab/FloatingActionButton;ZZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/melnykov/fab/FloatingActionButton;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/melnykov/fab/FloatingActionButton;->toggle(ZZZ)V

    return-void
.end method

.method private createDrawable(I)Landroid/graphics/drawable/Drawable;
    .locals 9
    .param p1, "color"    # I

    .prologue
    const/4 v1, 0x1

    .line 130
    new-instance v6, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v6}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    .line 131
    .local v6, "ovalShape":Landroid/graphics/drawable/shapes/OvalShape;
    new-instance v8, Landroid/graphics/drawable/ShapeDrawable;

    invoke-direct {v8, v6}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 132
    .local v8, "shapeDrawable":Landroid/graphics/drawable/ShapeDrawable;
    invoke-virtual {v8}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 134
    iget-boolean v2, p0, Lcom/melnykov/fab/FloatingActionButton;->mShadow:Z

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/melnykov/fab/FloatingActionButton;->hasLollipopApi()Z

    move-result v2

    if-nez v2, :cond_1

    .line 135
    invoke-virtual {p0}, Lcom/melnykov/fab/FloatingActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v2, p0, Lcom/melnykov/fab/FloatingActionButton;->mType:I

    if-nez v2, :cond_0

    const v2, 0x7f020056

    :goto_0
    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 138
    .local v7, "shadowDrawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    aput-object v7, v2, v3

    aput-object v8, v2, v1

    invoke-direct {v0, v2}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 140
    .local v0, "layerDrawable":Landroid/graphics/drawable/LayerDrawable;
    iget v2, p0, Lcom/melnykov/fab/FloatingActionButton;->mShadowSize:I

    iget v3, p0, Lcom/melnykov/fab/FloatingActionButton;->mShadowSize:I

    iget v4, p0, Lcom/melnykov/fab/FloatingActionButton;->mShadowSize:I

    iget v5, p0, Lcom/melnykov/fab/FloatingActionButton;->mShadowSize:I

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/drawable/LayerDrawable;->setLayerInset(IIIII)V

    .line 143
    .end local v0    # "layerDrawable":Landroid/graphics/drawable/LayerDrawable;
    .end local v7    # "shadowDrawable":Landroid/graphics/drawable/Drawable;
    :goto_1
    return-object v0

    .line 135
    :cond_0
    const v2, 0x7f020057

    goto :goto_0

    :cond_1
    move-object v0, v8

    .line 143
    goto :goto_1
.end method

.method private getColor(I)I
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/melnykov/fab/FloatingActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method private getDimension(I)I
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/melnykov/fab/FloatingActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method private getMarginBottom()I
    .locals 3

    .prologue
    .line 202
    const/4 v1, 0x0

    .line 203
    .local v1, "marginBottom":I
    invoke-virtual {p0}, Lcom/melnykov/fab/FloatingActionButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 204
    .local v0, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    instance-of v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v2, :cond_0

    .line 205
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .end local v0    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 207
    :cond_0
    return v1
.end method

.method private getTypedArray(Landroid/content/Context;Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeSet"    # Landroid/util/AttributeSet;
    .param p3, "attr"    # [I

    .prologue
    const/4 v0, 0x0

    .line 148
    invoke-virtual {p1, p2, p3, v0, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    return-object v0
.end method

.method private hasHoneycombApi()Z
    .locals 2

    .prologue
    .line 380
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hasJellyBeanApi()Z
    .locals 2

    .prologue
    .line 376
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hasLollipopApi()Z
    .locals 2

    .prologue
    .line 372
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeSet"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x1

    .line 90
    iput-boolean v1, p0, Lcom/melnykov/fab/FloatingActionButton;->mVisible:Z

    .line 91
    const v0, 0x7f0a0046

    invoke-direct {p0, v0}, Lcom/melnykov/fab/FloatingActionButton;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/melnykov/fab/FloatingActionButton;->mColorNormal:I

    .line 92
    const v0, 0x7f0a0047

    invoke-direct {p0, v0}, Lcom/melnykov/fab/FloatingActionButton;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/melnykov/fab/FloatingActionButton;->mColorPressed:I

    .line 93
    const v0, 0x106000b

    invoke-direct {p0, v0}, Lcom/melnykov/fab/FloatingActionButton;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/melnykov/fab/FloatingActionButton;->mColorRipple:I

    .line 94
    const/4 v0, 0x0

    iput v0, p0, Lcom/melnykov/fab/FloatingActionButton;->mType:I

    .line 95
    iput-boolean v1, p0, Lcom/melnykov/fab/FloatingActionButton;->mShadow:Z

    .line 96
    invoke-virtual {p0}, Lcom/melnykov/fab/FloatingActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b003c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/melnykov/fab/FloatingActionButton;->mScrollThreshold:I

    .line 97
    const v0, 0x7f0b003d

    invoke-direct {p0, v0}, Lcom/melnykov/fab/FloatingActionButton;->getDimension(I)I

    move-result v0

    iput v0, p0, Lcom/melnykov/fab/FloatingActionButton;->mShadowSize:I

    .line 98
    if-eqz p2, :cond_0

    .line 99
    invoke-direct {p0, p1, p2}, Lcom/melnykov/fab/FloatingActionButton;->initAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 101
    :cond_0
    invoke-direct {p0}, Lcom/melnykov/fab/FloatingActionButton;->updateBackground()V

    .line 102
    return-void
.end method

.method private initAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeSet"    # Landroid/util/AttributeSet;

    .prologue
    .line 105
    sget-object v1, Lorg/namelessrom/devicecontrol/R$styleable;->FloatingActionButton:[I

    invoke-direct {p0, p1, p2, v1}, Lcom/melnykov/fab/FloatingActionButton;->getTypedArray(Landroid/content/Context;Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 106
    .local v0, "attr":Landroid/content/res/TypedArray;
    if-eqz v0, :cond_0

    .line 108
    const/4 v1, 0x1

    const v2, 0x7f0a0046

    :try_start_0
    invoke-direct {p0, v2}, Lcom/melnykov/fab/FloatingActionButton;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/melnykov/fab/FloatingActionButton;->mColorNormal:I

    .line 110
    const/4 v1, 0x0

    const v2, 0x7f0a0047

    invoke-direct {p0, v2}, Lcom/melnykov/fab/FloatingActionButton;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/melnykov/fab/FloatingActionButton;->mColorPressed:I

    .line 112
    const/4 v1, 0x2

    const v2, 0x106000b

    invoke-direct {p0, v2}, Lcom/melnykov/fab/FloatingActionButton;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/melnykov/fab/FloatingActionButton;->mColorRipple:I

    .line 114
    const/4 v1, 0x3

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/melnykov/fab/FloatingActionButton;->mShadow:Z

    .line 115
    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/melnykov/fab/FloatingActionButton;->mType:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 120
    :cond_0
    return-void

    .line 117
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1
.end method

.method private setBackgroundCompat(Landroid/graphics/drawable/Drawable;)V
    .locals 7
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 179
    invoke-direct {p0}, Lcom/melnykov/fab/FloatingActionButton;->hasLollipopApi()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 180
    iget-boolean v1, p0, Lcom/melnykov/fab/FloatingActionButton;->mShadow:Z

    if-eqz v1, :cond_0

    const v1, 0x7f0b003b

    invoke-direct {p0, v1}, Lcom/melnykov/fab/FloatingActionButton;->getDimension(I)I

    move-result v1

    int-to-float v1, v1

    :goto_0
    invoke-virtual {p0, v1}, Lcom/melnykov/fab/FloatingActionButton;->setElevation(F)V

    .line 181
    new-instance v0, Landroid/graphics/drawable/RippleDrawable;

    new-instance v1, Landroid/content/res/ColorStateList;

    new-array v2, v6, [[I

    new-array v3, v5, [I

    aput-object v3, v2, v5

    new-array v3, v6, [I

    iget v4, p0, Lcom/melnykov/fab/FloatingActionButton;->mColorRipple:I

    aput v4, v3, v5

    invoke-direct {v1, v2, v3}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 184
    .local v0, "rippleDrawable":Landroid/graphics/drawable/RippleDrawable;
    new-instance v1, Lcom/melnykov/fab/FloatingActionButton$1;

    invoke-direct {v1, p0}, Lcom/melnykov/fab/FloatingActionButton$1;-><init>(Lcom/melnykov/fab/FloatingActionButton;)V

    invoke-virtual {p0, v1}, Lcom/melnykov/fab/FloatingActionButton;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 192
    invoke-virtual {p0, v6}, Lcom/melnykov/fab/FloatingActionButton;->setClipToOutline(Z)V

    .line 193
    invoke-virtual {p0, v0}, Lcom/melnykov/fab/FloatingActionButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 199
    .end local v0    # "rippleDrawable":Landroid/graphics/drawable/RippleDrawable;
    :goto_1
    return-void

    .line 180
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 194
    :cond_1
    invoke-direct {p0}, Lcom/melnykov/fab/FloatingActionButton;->hasJellyBeanApi()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 195
    invoke-virtual {p0, p1}, Lcom/melnykov/fab/FloatingActionButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 197
    :cond_2
    invoke-virtual {p0, p1}, Lcom/melnykov/fab/FloatingActionButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method private setMarginsWithoutShadow()V
    .locals 7

    .prologue
    .line 160
    iget-boolean v5, p0, Lcom/melnykov/fab/FloatingActionButton;->mMarginsSet:Z

    if-nez v5, :cond_0

    .line 161
    invoke-virtual {p0}, Lcom/melnykov/fab/FloatingActionButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    instance-of v5, v5, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v5, :cond_0

    .line 162
    invoke-virtual {p0}, Lcom/melnykov/fab/FloatingActionButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 164
    .local v1, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v6, p0, Lcom/melnykov/fab/FloatingActionButton;->mShadowSize:I

    sub-int v2, v5, v6

    .line 165
    .local v2, "leftMargin":I
    iget v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v6, p0, Lcom/melnykov/fab/FloatingActionButton;->mShadowSize:I

    sub-int v4, v5, v6

    .line 166
    .local v4, "topMargin":I
    iget v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v6, p0, Lcom/melnykov/fab/FloatingActionButton;->mShadowSize:I

    sub-int v3, v5, v6

    .line 167
    .local v3, "rightMargin":I
    iget v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iget v6, p0, Lcom/melnykov/fab/FloatingActionButton;->mShadowSize:I

    sub-int v0, v5, v6

    .line 168
    .local v0, "bottomMargin":I
    invoke-virtual {v1, v2, v4, v3, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 170
    invoke-virtual {p0}, Lcom/melnykov/fab/FloatingActionButton;->requestLayout()V

    .line 171
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/melnykov/fab/FloatingActionButton;->mMarginsSet:Z

    .line 174
    .end local v0    # "bottomMargin":I
    .end local v1    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v2    # "leftMargin":I
    .end local v3    # "rightMargin":I
    .end local v4    # "topMargin":I
    :cond_0
    return-void
.end method

.method private toggle(ZZZ)V
    .locals 6
    .param p1, "visible"    # Z
    .param p2, "animate"    # Z
    .param p3, "force"    # Z

    .prologue
    .line 299
    iget-boolean v3, p0, Lcom/melnykov/fab/FloatingActionButton;->mVisible:Z

    if-ne v3, p1, :cond_0

    if-eqz p3, :cond_1

    .line 300
    :cond_0
    iput-boolean p1, p0, Lcom/melnykov/fab/FloatingActionButton;->mVisible:Z

    .line 301
    invoke-virtual {p0}, Lcom/melnykov/fab/FloatingActionButton;->getHeight()I

    move-result v0

    .line 302
    .local v0, "height":I
    if-nez v0, :cond_2

    if-nez p3, :cond_2

    .line 303
    invoke-virtual {p0}, Lcom/melnykov/fab/FloatingActionButton;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    .line 304
    .local v2, "vto":Landroid/view/ViewTreeObserver;
    invoke-virtual {v2}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 305
    new-instance v3, Lcom/melnykov/fab/FloatingActionButton$2;

    invoke-direct {v3, p0, p1, p2}, Lcom/melnykov/fab/FloatingActionButton$2;-><init>(Lcom/melnykov/fab/FloatingActionButton;ZZ)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 333
    .end local v0    # "height":I
    .end local v2    # "vto":Landroid/view/ViewTreeObserver;
    :cond_1
    :goto_0
    return-void

    .line 319
    .restart local v0    # "height":I
    :cond_2
    if-eqz p1, :cond_3

    const/4 v1, 0x0

    .line 320
    .local v1, "translationY":I
    :goto_1
    if-eqz p2, :cond_4

    .line 321
    invoke-virtual {p0}, Lcom/melnykov/fab/FloatingActionButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    iget-object v4, p0, Lcom/melnykov/fab/FloatingActionButton;->mInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    const-wide/16 v4, 0xc8

    invoke-virtual {v3, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    int-to-float v4, v1

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 329
    :goto_2
    invoke-direct {p0}, Lcom/melnykov/fab/FloatingActionButton;->hasHoneycombApi()Z

    move-result v3

    if-nez v3, :cond_1

    .line 330
    invoke-virtual {p0, p1}, Lcom/melnykov/fab/FloatingActionButton;->setClickable(Z)V

    goto :goto_0

    .line 319
    .end local v1    # "translationY":I
    :cond_3
    invoke-direct {p0}, Lcom/melnykov/fab/FloatingActionButton;->getMarginBottom()I

    move-result v3

    add-int v1, v0, v3

    goto :goto_1

    .line 325
    .restart local v1    # "translationY":I
    :cond_4
    int-to-float v3, v1

    invoke-virtual {p0, v3}, Lcom/melnykov/fab/FloatingActionButton;->setTranslationY(F)V

    goto :goto_2
.end method

.method private updateBackground()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 123
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 124
    .local v0, "drawable":Landroid/graphics/drawable/StateListDrawable;
    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x10100a7

    aput v2, v1, v3

    iget v2, p0, Lcom/melnykov/fab/FloatingActionButton;->mColorPressed:I

    invoke-direct {p0, v2}, Lcom/melnykov/fab/FloatingActionButton;->createDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 125
    new-array v1, v3, [I

    iget v2, p0, Lcom/melnykov/fab/FloatingActionButton;->mColorNormal:I

    invoke-direct {p0, v2}, Lcom/melnykov/fab/FloatingActionButton;->createDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 126
    invoke-direct {p0, v0}, Lcom/melnykov/fab/FloatingActionButton;->setBackgroundCompat(Landroid/graphics/drawable/Drawable;)V

    .line 127
    return-void
.end method


# virtual methods
.method public attachToScrollView(Lcom/melnykov/fab/ObservableScrollView;)V
    .locals 1
    .param p1, "scrollView"    # Lcom/melnykov/fab/ObservableScrollView;

    .prologue
    .line 344
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/melnykov/fab/FloatingActionButton;->attachToScrollView(Lcom/melnykov/fab/ObservableScrollView;Lcom/melnykov/fab/ScrollDirectionListener;)V

    .line 345
    return-void
.end method

.method public attachToScrollView(Lcom/melnykov/fab/ObservableScrollView;Lcom/melnykov/fab/ScrollDirectionListener;)V
    .locals 2
    .param p1, "scrollView"    # Lcom/melnykov/fab/ObservableScrollView;
    .param p2, "listener"    # Lcom/melnykov/fab/ScrollDirectionListener;

    .prologue
    .line 365
    new-instance v0, Lcom/melnykov/fab/FloatingActionButton$ScrollViewScrollDetectorImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/melnykov/fab/FloatingActionButton$ScrollViewScrollDetectorImpl;-><init>(Lcom/melnykov/fab/FloatingActionButton;Lcom/melnykov/fab/FloatingActionButton$1;)V

    .line 366
    .local v0, "scrollDetector":Lcom/melnykov/fab/FloatingActionButton$ScrollViewScrollDetectorImpl;
    # invokes: Lcom/melnykov/fab/FloatingActionButton$ScrollViewScrollDetectorImpl;->setListener(Lcom/melnykov/fab/ScrollDirectionListener;)V
    invoke-static {v0, p2}, Lcom/melnykov/fab/FloatingActionButton$ScrollViewScrollDetectorImpl;->access$800(Lcom/melnykov/fab/FloatingActionButton$ScrollViewScrollDetectorImpl;Lcom/melnykov/fab/ScrollDirectionListener;)V

    .line 367
    iget v1, p0, Lcom/melnykov/fab/FloatingActionButton;->mScrollThreshold:I

    invoke-virtual {v0, v1}, Lcom/melnykov/fab/FloatingActionButton$ScrollViewScrollDetectorImpl;->setScrollThreshold(I)V

    .line 368
    invoke-virtual {p1, v0}, Lcom/melnykov/fab/ObservableScrollView;->setOnScrollChangedListener(Lcom/melnykov/fab/ObservableScrollView$OnScrollChangedListener;)V

    .line 369
    return-void
.end method

.method public getColorNormal()I
    .locals 1

    .prologue
    .line 222
    iget v0, p0, Lcom/melnykov/fab/FloatingActionButton;->mColorNormal:I

    return v0
.end method

.method public getColorPressed()I
    .locals 1

    .prologue
    .line 237
    iget v0, p0, Lcom/melnykov/fab/FloatingActionButton;->mColorPressed:I

    return v0
.end method

.method public getColorRipple()I
    .locals 1

    .prologue
    .line 252
    iget v0, p0, Lcom/melnykov/fab/FloatingActionButton;->mColorRipple:I

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 275
    iget v0, p0, Lcom/melnykov/fab/FloatingActionButton;->mType:I

    return v0
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 287
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/melnykov/fab/FloatingActionButton;->hide(Z)V

    .line 288
    return-void
.end method

.method public hide(Z)V
    .locals 1
    .param p1, "animate"    # Z

    .prologue
    const/4 v0, 0x0

    .line 295
    invoke-direct {p0, v0, p1, v0}, Lcom/melnykov/fab/FloatingActionButton;->toggle(ZZZ)V

    .line 296
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 79
    invoke-super {p0, p1, p2}, Landroid/widget/ImageButton;->onMeasure(II)V

    .line 80
    iget v1, p0, Lcom/melnykov/fab/FloatingActionButton;->mType:I

    if-nez v1, :cond_1

    const v1, 0x7f0b003f

    :goto_0
    invoke-direct {p0, v1}, Lcom/melnykov/fab/FloatingActionButton;->getDimension(I)I

    move-result v0

    .line 82
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/melnykov/fab/FloatingActionButton;->mShadow:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/melnykov/fab/FloatingActionButton;->hasLollipopApi()Z

    move-result v1

    if-nez v1, :cond_0

    .line 83
    iget v1, p0, Lcom/melnykov/fab/FloatingActionButton;->mShadowSize:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 84
    invoke-direct {p0}, Lcom/melnykov/fab/FloatingActionButton;->setMarginsWithoutShadow()V

    .line 86
    :cond_0
    invoke-virtual {p0, v0, v0}, Lcom/melnykov/fab/FloatingActionButton;->setMeasuredDimension(II)V

    .line 87
    return-void

    .line 80
    .end local v0    # "size":I
    :cond_1
    const v1, 0x7f0b003e

    goto :goto_0
.end method

.method public setColorNormal(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 211
    iget v0, p0, Lcom/melnykov/fab/FloatingActionButton;->mColorNormal:I

    if-eq p1, v0, :cond_0

    .line 212
    iput p1, p0, Lcom/melnykov/fab/FloatingActionButton;->mColorNormal:I

    .line 213
    invoke-direct {p0}, Lcom/melnykov/fab/FloatingActionButton;->updateBackground()V

    .line 215
    :cond_0
    return-void
.end method

.method public setColorNormalResId(I)V
    .locals 1
    .param p1, "colorResId"    # I

    .prologue
    .line 218
    invoke-direct {p0, p1}, Lcom/melnykov/fab/FloatingActionButton;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/melnykov/fab/FloatingActionButton;->setColorNormal(I)V

    .line 219
    return-void
.end method

.method public setColorPressed(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 226
    iget v0, p0, Lcom/melnykov/fab/FloatingActionButton;->mColorPressed:I

    if-eq p1, v0, :cond_0

    .line 227
    iput p1, p0, Lcom/melnykov/fab/FloatingActionButton;->mColorPressed:I

    .line 228
    invoke-direct {p0}, Lcom/melnykov/fab/FloatingActionButton;->updateBackground()V

    .line 230
    :cond_0
    return-void
.end method

.method public setColorPressedResId(I)V
    .locals 1
    .param p1, "colorResId"    # I

    .prologue
    .line 233
    invoke-direct {p0, p1}, Lcom/melnykov/fab/FloatingActionButton;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/melnykov/fab/FloatingActionButton;->setColorPressed(I)V

    .line 234
    return-void
.end method

.method public setColorRipple(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 241
    iget v0, p0, Lcom/melnykov/fab/FloatingActionButton;->mColorRipple:I

    if-eq p1, v0, :cond_0

    .line 242
    iput p1, p0, Lcom/melnykov/fab/FloatingActionButton;->mColorRipple:I

    .line 243
    invoke-direct {p0}, Lcom/melnykov/fab/FloatingActionButton;->updateBackground()V

    .line 245
    :cond_0
    return-void
.end method

.method public setColorRippleResId(I)V
    .locals 1
    .param p1, "colorResId"    # I

    .prologue
    .line 248
    invoke-direct {p0, p1}, Lcom/melnykov/fab/FloatingActionButton;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/melnykov/fab/FloatingActionButton;->setColorRipple(I)V

    .line 249
    return-void
.end method

.method public setShadow(Z)V
    .locals 1
    .param p1, "shadow"    # Z

    .prologue
    .line 256
    iget-boolean v0, p0, Lcom/melnykov/fab/FloatingActionButton;->mShadow:Z

    if-eq p1, v0, :cond_0

    .line 257
    iput-boolean p1, p0, Lcom/melnykov/fab/FloatingActionButton;->mShadow:Z

    .line 258
    invoke-direct {p0}, Lcom/melnykov/fab/FloatingActionButton;->updateBackground()V

    .line 260
    :cond_0
    return-void
.end method

.method public setType(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 267
    iget v0, p0, Lcom/melnykov/fab/FloatingActionButton;->mType:I

    if-eq p1, v0, :cond_0

    .line 268
    iput p1, p0, Lcom/melnykov/fab/FloatingActionButton;->mType:I

    .line 269
    invoke-direct {p0}, Lcom/melnykov/fab/FloatingActionButton;->updateBackground()V

    .line 271
    :cond_0
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 283
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/melnykov/fab/FloatingActionButton;->show(Z)V

    .line 284
    return-void
.end method

.method public show(Z)V
    .locals 2
    .param p1, "animate"    # Z

    .prologue
    .line 291
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/melnykov/fab/FloatingActionButton;->toggle(ZZZ)V

    .line 292
    return-void
.end method
