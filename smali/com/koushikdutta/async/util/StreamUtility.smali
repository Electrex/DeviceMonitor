.class public Lcom/koushikdutta/async/util/StreamUtility;
.super Ljava/lang/Object;
.source "StreamUtility.java"


# direct methods
.method public static varargs closeQuietly([Ljava/io/Closeable;)V
    .locals 5
    .param p0, "closeables"    # [Ljava/io/Closeable;

    .prologue
    .line 93
    if-nez p0, :cond_1

    .line 104
    :cond_0
    return-void

    .line 95
    :cond_1
    move-object v0, p0

    .local v0, "arr$":[Ljava/io/Closeable;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 96
    .local v1, "closeable":Ljava/io/Closeable;
    if-eqz v1, :cond_2

    .line 98
    :try_start_0
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 99
    :catch_0
    move-exception v4

    goto :goto_1
.end method
