.class public Lcom/koushikdutta/async/http/ResponseCacheMiddleware;
.super Lcom/koushikdutta/async/http/SimpleMiddleware;
.source "ResponseCacheMiddleware.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryEditor;,
        Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedSocket;,
        Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedSSLSocket;,
        Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryCacheResponse;,
        Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;,
        Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedBodyEmitter;,
        Lcom/koushikdutta/async/http/ResponseCacheMiddleware$BodyCacher;,
        Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CacheData;
    }
.end annotation


# instance fields
.field private cache:Lcom/koushikdutta/async/util/FileCache;

.field private cacheHitCount:I

.field private cacheStoreCount:I

.field private caching:Z

.field private conditionalCacheHitCount:I

.field private networkCount:I

.field private server:Lcom/koushikdutta/async/AsyncServer;

.field private writeAbortCount:I

.field private writeSuccessCount:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/koushikdutta/async/http/SimpleMiddleware;-><init>()V

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->caching:Z

    .line 65
    return-void
.end method

.method static synthetic access$300(Lcom/koushikdutta/async/http/ResponseCacheMiddleware;)Lcom/koushikdutta/async/AsyncServer;
    .locals 1
    .param p0, "x0"    # Lcom/koushikdutta/async/http/ResponseCacheMiddleware;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->server:Lcom/koushikdutta/async/AsyncServer;

    return-object v0
.end method

.method static synthetic access$400(Lcom/koushikdutta/async/http/ResponseCacheMiddleware;)Lcom/koushikdutta/async/util/FileCache;
    .locals 1
    .param p0, "x0"    # Lcom/koushikdutta/async/http/ResponseCacheMiddleware;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->cache:Lcom/koushikdutta/async/util/FileCache;

    return-object v0
.end method

.method static synthetic access$508(Lcom/koushikdutta/async/http/ResponseCacheMiddleware;)I
    .locals 2
    .param p0, "x0"    # Lcom/koushikdutta/async/http/ResponseCacheMiddleware;

    .prologue
    .line 46
    iget v0, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->writeSuccessCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->writeSuccessCount:I

    return v0
.end method

.method static synthetic access$608(Lcom/koushikdutta/async/http/ResponseCacheMiddleware;)I
    .locals 2
    .param p0, "x0"    # Lcom/koushikdutta/async/http/ResponseCacheMiddleware;

    .prologue
    .line 46
    iget v0, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->writeAbortCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->writeAbortCount:I

    return v0
.end method

.method public static addCache(Lcom/koushikdutta/async/http/AsyncHttpClient;Ljava/io/File;J)Lcom/koushikdutta/async/http/ResponseCacheMiddleware;
    .locals 6
    .param p0, "client"    # Lcom/koushikdutta/async/http/AsyncHttpClient;
    .param p1, "cacheDir"    # Ljava/io/File;
    .param p2, "size"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getMiddleware()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware;

    .line 69
    .local v1, "middleware":Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware;
    instance-of v3, v1, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;

    if-eqz v3, :cond_0

    .line 70
    new-instance v3, Ljava/io/IOException;

    const-string v4, "Response cache already added to http client"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 72
    .end local v1    # "middleware":Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware;
    :cond_1
    new-instance v2, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;

    invoke-direct {v2}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;-><init>()V

    .line 73
    .local v2, "ret":Lcom/koushikdutta/async/http/ResponseCacheMiddleware;
    invoke-virtual {p0}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getServer()Lcom/koushikdutta/async/AsyncServer;

    move-result-object v3

    iput-object v3, v2, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->server:Lcom/koushikdutta/async/AsyncServer;

    .line 74
    new-instance v3, Lcom/koushikdutta/async/util/FileCache;

    const/4 v4, 0x0

    invoke-direct {v3, p1, p2, p3, v4}, Lcom/koushikdutta/async/util/FileCache;-><init>(Ljava/io/File;JZ)V

    iput-object v3, v2, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->cache:Lcom/koushikdutta/async/util/FileCache;

    .line 75
    invoke-virtual {p0, v2}, Lcom/koushikdutta/async/http/AsyncHttpClient;->insertMiddleware(Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware;)V

    .line 76
    return-object v2
.end method


# virtual methods
.method public getFileCache()Lcom/koushikdutta/async/util/FileCache;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->cache:Lcom/koushikdutta/async/util/FileCache;

    return-object v0
.end method

.method public getSocket(Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$GetSocketData;)Lcom/koushikdutta/async/future/Cancellable;
    .locals 24
    .param p1, "data"    # Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$GetSocketData;

    .prologue
    .line 95
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->cache:Lcom/koushikdutta/async/util/FileCache;

    move-object/from16 v20, v0

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->caching:Z

    move/from16 v20, v0

    if-eqz v20, :cond_0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$GetSocketData;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->getHeaders()Lcom/koushikdutta/async/http/libcore/RequestHeaders;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/koushikdutta/async/http/libcore/RequestHeaders;->isNoCache()Z

    move-result v20

    if-eqz v20, :cond_1

    .line 96
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->networkCount:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->networkCount:I

    .line 97
    const/16 v20, 0x0

    .line 186
    :goto_0
    return-object v20

    .line 100
    :cond_1
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$GetSocketData;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->getUri()Landroid/net/Uri;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v20 .. v20}, Lcom/koushikdutta/async/util/FileCache;->toKeyString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 101
    .local v12, "key":Ljava/lang/String;
    const/16 v18, 0x0

    .line 105
    .local v18, "snapshot":[Ljava/io/FileInputStream;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->cache:Lcom/koushikdutta/async/util/FileCache;

    move-object/from16 v20, v0

    const/16 v21, 0x2

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v12, v1}, Lcom/koushikdutta/async/util/FileCache;->get(Ljava/lang/String;I)[Ljava/io/FileInputStream;

    move-result-object v18

    .line 106
    if-nez v18, :cond_2

    .line 107
    move-object/from16 v0, p0

    iget v0, v0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->networkCount:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->networkCount:I

    .line 108
    const/16 v20, 0x0

    goto :goto_0

    .line 110
    :cond_2
    const/16 v20, 0x1

    aget-object v20, v18, v20

    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->available()I

    move-result v20

    move/from16 v0, v20

    int-to-long v8, v0

    .line 111
    .local v8, "contentLength":J
    new-instance v11, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;

    const/16 v20, 0x0

    aget-object v20, v18, v20

    move-object/from16 v0, v20

    invoke-direct {v11, v0}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    .local v11, "entry":Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$GetSocketData;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->getUri()Landroid/net/Uri;

    move-result-object v20

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$GetSocketData;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->getMethod()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$GetSocketData;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->getHeaders()Lcom/koushikdutta/async/http/libcore/RequestHeaders;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/koushikdutta/async/http/libcore/RequestHeaders;->getHeaders()Lcom/koushikdutta/async/http/libcore/RawHeaders;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->toMultimap()Ljava/util/Map;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v11, v0, v1, v2}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->matches(Landroid/net/Uri;Ljava/lang/String;Ljava/util/Map;)Z

    move-result v20

    if-nez v20, :cond_3

    .line 122
    move-object/from16 v0, p0

    iget v0, v0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->networkCount:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->networkCount:I

    .line 123
    invoke-static/range {v18 .. v18}, Lcom/koushikdutta/async/util/StreamUtility;->closeQuietly([Ljava/io/Closeable;)V

    .line 124
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 113
    .end local v8    # "contentLength":J
    .end local v11    # "entry":Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;
    :catch_0
    move-exception v10

    .line 115
    .local v10, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->networkCount:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->networkCount:I

    .line 116
    invoke-static/range {v18 .. v18}, Lcom/koushikdutta/async/util/StreamUtility;->closeQuietly([Ljava/io/Closeable;)V

    .line 117
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 127
    .end local v10    # "e":Ljava/io/IOException;
    .restart local v8    # "contentLength":J
    .restart local v11    # "entry":Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;
    :cond_3
    new-instance v7, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryCacheResponse;

    const/16 v20, 0x1

    aget-object v20, v18, v20

    move-object/from16 v0, v20

    invoke-direct {v7, v11, v0}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryCacheResponse;-><init>(Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;Ljava/io/FileInputStream;)V

    .line 132
    .local v7, "candidate":Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryCacheResponse;
    :try_start_1
    invoke-virtual {v7}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryCacheResponse;->getHeaders()Ljava/util/Map;

    move-result-object v16

    .line 133
    .local v16, "responseHeadersMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-virtual {v7}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryCacheResponse;->getBody()Ljava/io/FileInputStream;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    .line 140
    .local v5, "cachedResponseBody":Ljava/io/FileInputStream;
    if-eqz v16, :cond_4

    if-nez v5, :cond_5

    .line 141
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->networkCount:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->networkCount:I

    .line 142
    invoke-static/range {v18 .. v18}, Lcom/koushikdutta/async/util/StreamUtility;->closeQuietly([Ljava/io/Closeable;)V

    .line 143
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 135
    .end local v5    # "cachedResponseBody":Ljava/io/FileInputStream;
    .end local v16    # "responseHeadersMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    :catch_1
    move-exception v10

    .line 136
    .local v10, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->networkCount:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->networkCount:I

    .line 137
    invoke-static/range {v18 .. v18}, Lcom/koushikdutta/async/util/StreamUtility;->closeQuietly([Ljava/io/Closeable;)V

    .line 138
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 146
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v5    # "cachedResponseBody":Ljava/io/FileInputStream;
    .restart local v16    # "responseHeadersMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    :cond_5
    invoke-static/range {v16 .. v16}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->fromMultimap(Ljava/util/Map;)Lcom/koushikdutta/async/http/libcore/RawHeaders;

    move-result-object v13

    .line 147
    .local v13, "rawResponseHeaders":Lcom/koushikdutta/async/http/libcore/RawHeaders;
    new-instance v6, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$GetSocketData;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->getUri()Landroid/net/Uri;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v6, v0, v13}, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;-><init>(Landroid/net/Uri;Lcom/koushikdutta/async/http/libcore/RawHeaders;)V

    .line 148
    .local v6, "cachedResponseHeaders":Lcom/koushikdutta/async/http/libcore/ResponseHeaders;
    const-string v20, "Content-Length"

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v13, v0, v1}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const-string v20, "Content-Encoding"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->removeAll(Ljava/lang/String;)V

    .line 150
    const-string v20, "Transfer-Encoding"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->removeAll(Ljava/lang/String;)V

    .line 151
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    move-wide/from16 v0, v20

    move-wide/from16 v2, v22

    invoke-virtual {v6, v0, v1, v2, v3}, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;->setLocalTimestamps(JJ)V

    .line 153
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 154
    .local v14, "now":J
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$GetSocketData;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->getHeaders()Lcom/koushikdutta/async/http/libcore/RequestHeaders;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v6, v14, v15, v0}, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;->chooseResponseSource(JLcom/koushikdutta/async/http/libcore/RequestHeaders;)Lcom/koushikdutta/async/http/libcore/ResponseSource;

    move-result-object v17

    .line 156
    .local v17, "responseSource":Lcom/koushikdutta/async/http/libcore/ResponseSource;
    sget-object v20, Lcom/koushikdutta/async/http/libcore/ResponseSource;->CACHE:Lcom/koushikdutta/async/http/libcore/ResponseSource;

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    if-ne v0, v1, :cond_7

    .line 157
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$GetSocketData;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    move-object/from16 v20, v0

    const-string v21, "Response retrieved from cache"

    invoke-virtual/range {v20 .. v21}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->logi(Ljava/lang/String;)V

    .line 158
    # invokes: Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->isHttps()Z
    invoke-static {v11}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->access$000(Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;)Z

    move-result v20

    if-eqz v20, :cond_6

    new-instance v19, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedSSLSocket;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v7, v8, v9}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedSSLSocket;-><init>(Lcom/koushikdutta/async/http/ResponseCacheMiddleware;Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryCacheResponse;J)V

    .line 159
    .local v19, "socket":Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedSocket;
    :goto_1
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedSocket;->pending:Lcom/koushikdutta/async/ByteBufferList;

    move-object/from16 v20, v0

    invoke-virtual {v13}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->toHeaderString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->getBytes()[B

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/koushikdutta/async/ByteBufferList;->add(Ljava/nio/ByteBuffer;)V

    .line 161
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->server:Lcom/koushikdutta/async/AsyncServer;

    move-object/from16 v20, v0

    new-instance v21, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$1;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$1;-><init>(Lcom/koushikdutta/async/http/ResponseCacheMiddleware;Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$GetSocketData;Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedSocket;)V

    invoke-virtual/range {v20 .. v21}, Lcom/koushikdutta/async/AsyncServer;->post(Ljava/lang/Runnable;)Ljava/lang/Object;

    .line 168
    move-object/from16 v0, p0

    iget v0, v0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->cacheHitCount:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->cacheHitCount:I

    .line 169
    new-instance v20, Lcom/koushikdutta/async/future/SimpleCancellable;

    invoke-direct/range {v20 .. v20}, Lcom/koushikdutta/async/future/SimpleCancellable;-><init>()V

    goto/16 :goto_0

    .line 158
    .end local v19    # "socket":Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedSocket;
    :cond_6
    new-instance v19, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedSocket;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v7, v8, v9}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedSocket;-><init>(Lcom/koushikdutta/async/http/ResponseCacheMiddleware;Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryCacheResponse;J)V

    goto :goto_1

    .line 171
    :cond_7
    sget-object v20, Lcom/koushikdutta/async/http/libcore/ResponseSource;->CONDITIONAL_CACHE:Lcom/koushikdutta/async/http/libcore/ResponseSource;

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    if-ne v0, v1, :cond_8

    .line 172
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$GetSocketData;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    move-object/from16 v20, v0

    const-string v21, "Response may be served from conditional cache"

    invoke-virtual/range {v20 .. v21}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->logi(Ljava/lang/String;)V

    .line 173
    new-instance v4, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CacheData;

    invoke-direct {v4}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CacheData;-><init>()V

    .line 174
    .local v4, "cacheData":Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CacheData;
    move-object/from16 v0, v18

    iput-object v0, v4, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CacheData;->snapshot:[Ljava/io/FileInputStream;

    .line 175
    iput-wide v8, v4, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CacheData;->contentLength:J

    .line 176
    iput-object v6, v4, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CacheData;->cachedResponseHeaders:Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    .line 177
    iput-object v7, v4, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CacheData;->candidate:Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryCacheResponse;

    .line 178
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$GetSocketData;->state:Lcom/koushikdutta/async/util/UntypedHashtable;

    move-object/from16 v20, v0

    const-string v21, "cache-data"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v4}, Lcom/koushikdutta/async/util/UntypedHashtable;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 179
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 182
    .end local v4    # "cacheData":Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CacheData;
    :cond_8
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$GetSocketData;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    move-object/from16 v20, v0

    const-string v21, "Response can not be served from cache"

    invoke-virtual/range {v20 .. v21}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->logd(Ljava/lang/String;)V

    .line 184
    move-object/from16 v0, p0

    iget v0, v0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->networkCount:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->networkCount:I

    .line 185
    invoke-static/range {v18 .. v18}, Lcom/koushikdutta/async/util/StreamUtility;->closeQuietly([Ljava/io/Closeable;)V

    .line 186
    const/16 v20, 0x0

    goto/16 :goto_0
.end method

.method public onBodyDecoder(Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;)V
    .locals 12
    .param p1, "data"    # Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;

    .prologue
    const/4 v11, 0x1

    .line 210
    iget-object v9, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->socket:Lcom/koushikdutta/async/AsyncSocket;

    const-class v10, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedSocket;

    invoke-static {v9, v10}, Lcom/koushikdutta/async/Util;->getWrappedSocket(Lcom/koushikdutta/async/AsyncSocket;Ljava/lang/Class;)Lcom/koushikdutta/async/AsyncSocket;

    move-result-object v2

    check-cast v2, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedSocket;

    .line 211
    .local v2, "cached":Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedSocket;
    if-eqz v2, :cond_1

    .line 212
    iget-object v9, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->headers:Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    invoke-virtual {v9}, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;->getHeaders()Lcom/koushikdutta/async/http/libcore/RawHeaders;

    move-result-object v9

    const-string v10, "X-Served-From"

    const-string v11, "cache"

    invoke-virtual {v9, v10, v11}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    :cond_0
    :goto_0
    return-void

    .line 216
    :cond_1
    iget-object v9, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->state:Lcom/koushikdutta/async/util/UntypedHashtable;

    const-string v10, "cache-data"

    invoke-virtual {v9, v10}, Lcom/koushikdutta/async/util/UntypedHashtable;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CacheData;

    .line 217
    .local v1, "cacheData":Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CacheData;
    if-eqz v1, :cond_3

    .line 218
    iget-object v9, v1, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CacheData;->cachedResponseHeaders:Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    iget-object v10, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->headers:Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    invoke-virtual {v9, v10}, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;->validate(Lcom/koushikdutta/async/http/libcore/ResponseHeaders;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 219
    iget-object v9, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    const-string v10, "Serving response from conditional cache"

    invoke-virtual {v9, v10}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->logi(Ljava/lang/String;)V

    .line 220
    iget-object v9, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->headers:Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    invoke-virtual {v9}, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;->getHeaders()Lcom/koushikdutta/async/http/libcore/RawHeaders;

    move-result-object v9

    const-string v10, "Content-Length"

    invoke-virtual {v9, v10}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->removeAll(Ljava/lang/String;)V

    .line 221
    iget-object v9, v1, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CacheData;->cachedResponseHeaders:Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    iget-object v10, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->headers:Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    invoke-virtual {v9, v10}, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;->combine(Lcom/koushikdutta/async/http/libcore/ResponseHeaders;)Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    move-result-object v9

    iput-object v9, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->headers:Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    .line 222
    iget-object v9, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->headers:Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    invoke-virtual {v9}, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;->getHeaders()Lcom/koushikdutta/async/http/libcore/RawHeaders;

    move-result-object v9

    iget-object v10, v1, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CacheData;->cachedResponseHeaders:Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    invoke-virtual {v10}, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;->getHeaders()Lcom/koushikdutta/async/http/libcore/RawHeaders;

    move-result-object v10

    invoke-virtual {v10}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->getStatusLine()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->setStatusLine(Ljava/lang/String;)V

    .line 224
    iget-object v9, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->headers:Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    invoke-virtual {v9}, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;->getHeaders()Lcom/koushikdutta/async/http/libcore/RawHeaders;

    move-result-object v9

    const-string v10, "X-Served-From"

    const-string v11, "conditional-cache"

    invoke-virtual {v9, v10, v11}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    iget v9, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->conditionalCacheHitCount:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->conditionalCacheHitCount:I

    .line 227
    new-instance v0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedBodyEmitter;

    iget-object v9, v1, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CacheData;->candidate:Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryCacheResponse;

    iget-wide v10, v1, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CacheData;->contentLength:J

    invoke-direct {v0, v9, v10, v11}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedBodyEmitter;-><init>(Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryCacheResponse;J)V

    .line 228
    .local v0, "bodySpewer":Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedBodyEmitter;
    iget-object v9, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->bodyEmitter:Lcom/koushikdutta/async/DataEmitter;

    invoke-virtual {v0, v9}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedBodyEmitter;->setDataEmitter(Lcom/koushikdutta/async/DataEmitter;)V

    .line 229
    iput-object v0, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->bodyEmitter:Lcom/koushikdutta/async/DataEmitter;

    .line 230
    invoke-virtual {v0}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedBodyEmitter;->spew()V

    goto :goto_0

    .line 235
    .end local v0    # "bodySpewer":Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedBodyEmitter;
    :cond_2
    iget-object v9, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->state:Lcom/koushikdutta/async/util/UntypedHashtable;

    const-string v10, "cache-data"

    invoke-virtual {v9, v10}, Lcom/koushikdutta/async/util/UntypedHashtable;->remove(Ljava/lang/String;)V

    .line 236
    iget-object v9, v1, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CacheData;->snapshot:[Ljava/io/FileInputStream;

    invoke-static {v9}, Lcom/koushikdutta/async/util/StreamUtility;->closeQuietly([Ljava/io/Closeable;)V

    .line 239
    :cond_3
    iget-boolean v9, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->caching:Z

    if-eqz v9, :cond_0

    .line 242
    iget-object v9, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->headers:Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    iget-object v10, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    invoke-virtual {v10}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->getHeaders()Lcom/koushikdutta/async/http/libcore/RequestHeaders;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;->isCacheable(Lcom/koushikdutta/async/http/libcore/RequestHeaders;)Z

    move-result v9

    if-eqz v9, :cond_4

    iget-object v9, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    invoke-virtual {v9}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->getMethod()Ljava/lang/String;

    move-result-object v9

    const-string v10, "GET"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    .line 248
    :cond_4
    iget v9, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->networkCount:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->networkCount:I

    .line 249
    iget-object v9, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    const-string v10, "Response is not cacheable"

    invoke-virtual {v9, v10}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->logd(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 253
    :cond_5
    new-array v9, v11, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    invoke-virtual {v11}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->getUri()Landroid/net/Uri;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v9}, Lcom/koushikdutta/async/util/FileCache;->toKeyString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 254
    .local v7, "key":Ljava/lang/String;
    iget-object v9, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    invoke-virtual {v9}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->getHeaders()Lcom/koushikdutta/async/http/libcore/RequestHeaders;

    move-result-object v9

    invoke-virtual {v9}, Lcom/koushikdutta/async/http/libcore/RequestHeaders;->getHeaders()Lcom/koushikdutta/async/http/libcore/RawHeaders;

    move-result-object v9

    iget-object v10, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->headers:Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    invoke-virtual {v10}, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;->getVaryFields()Ljava/util/Set;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->getAll(Ljava/util/Set;)Lcom/koushikdutta/async/http/libcore/RawHeaders;

    move-result-object v8

    .line 255
    .local v8, "varyHeaders":Lcom/koushikdutta/async/http/libcore/RawHeaders;
    new-instance v6, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;

    iget-object v9, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    invoke-virtual {v9}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->getUri()Landroid/net/Uri;

    move-result-object v9

    iget-object v10, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    iget-object v11, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->headers:Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    invoke-direct {v6, v9, v8, v10, v11}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;-><init>(Landroid/net/Uri;Lcom/koushikdutta/async/http/libcore/RawHeaders;Lcom/koushikdutta/async/http/AsyncHttpRequest;Lcom/koushikdutta/async/http/libcore/ResponseHeaders;)V

    .line 256
    .local v6, "entry":Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;
    new-instance v3, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$BodyCacher;

    const/4 v9, 0x0

    invoke-direct {v3, v9}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$BodyCacher;-><init>(Lcom/koushikdutta/async/http/ResponseCacheMiddleware$1;)V

    .line 257
    .local v3, "cacher":Lcom/koushikdutta/async/http/ResponseCacheMiddleware$BodyCacher;
    new-instance v5, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryEditor;

    invoke-direct {v5, p0, v7}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryEditor;-><init>(Lcom/koushikdutta/async/http/ResponseCacheMiddleware;Ljava/lang/String;)V

    .line 259
    .local v5, "editor":Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryEditor;
    :try_start_0
    invoke-virtual {v6, v5}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->writeTo(Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryEditor;)V

    .line 261
    const/4 v9, 0x1

    invoke-virtual {v5, v9}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryEditor;->newOutputStream(I)Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    iput-object v5, v3, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$BodyCacher;->editor:Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryEditor;

    .line 271
    iget-object v9, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->bodyEmitter:Lcom/koushikdutta/async/DataEmitter;

    invoke-virtual {v3, v9}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$BodyCacher;->setDataEmitter(Lcom/koushikdutta/async/DataEmitter;)V

    .line 272
    iput-object v3, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->bodyEmitter:Lcom/koushikdutta/async/DataEmitter;

    .line 274
    iget-object v9, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->state:Lcom/koushikdutta/async/util/UntypedHashtable;

    const-string v10, "body-cacher"

    invoke-virtual {v9, v10, v3}, Lcom/koushikdutta/async/util/UntypedHashtable;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 275
    iget-object v9, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnBodyData;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    const-string v10, "Caching response"

    invoke-virtual {v9, v10}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->logd(Ljava/lang/String;)V

    .line 276
    iget v9, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->cacheStoreCount:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->cacheStoreCount:I

    goto/16 :goto_0

    .line 263
    :catch_0
    move-exception v4

    .line 265
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryEditor;->abort()V

    .line 266
    iget v9, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->networkCount:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->networkCount:I

    goto/16 :goto_0
.end method

.method public onRequestComplete(Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnRequestCompleteData;)V
    .locals 6
    .param p1, "data"    # Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnRequestCompleteData;

    .prologue
    .line 282
    iget-object v3, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnRequestCompleteData;->state:Lcom/koushikdutta/async/util/UntypedHashtable;

    const-string v4, "cache-data"

    invoke-virtual {v3, v4}, Lcom/koushikdutta/async/util/UntypedHashtable;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CacheData;

    .line 283
    .local v0, "cacheData":Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CacheData;
    if-eqz v0, :cond_0

    iget-object v3, v0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CacheData;->snapshot:[Ljava/io/FileInputStream;

    if-eqz v3, :cond_0

    .line 284
    iget-object v3, v0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CacheData;->snapshot:[Ljava/io/FileInputStream;

    invoke-static {v3}, Lcom/koushikdutta/async/util/StreamUtility;->closeQuietly([Ljava/io/Closeable;)V

    .line 286
    :cond_0
    iget-object v3, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnRequestCompleteData;->socket:Lcom/koushikdutta/async/AsyncSocket;

    const-class v4, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedSocket;

    invoke-static {v3, v4}, Lcom/koushikdutta/async/Util;->getWrappedSocket(Lcom/koushikdutta/async/AsyncSocket;Ljava/lang/Class;)Lcom/koushikdutta/async/AsyncSocket;

    move-result-object v1

    check-cast v1, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedSocket;

    .line 287
    .local v1, "cachedSocket":Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedSocket;
    if-eqz v1, :cond_1

    .line 288
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/io/Closeable;

    const/4 v4, 0x0

    iget-object v5, v1, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$CachedSocket;->cacheResponse:Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryCacheResponse;

    invoke-virtual {v5}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryCacheResponse;->getBody()Ljava/io/FileInputStream;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/koushikdutta/async/util/StreamUtility;->closeQuietly([Ljava/io/Closeable;)V

    .line 290
    :cond_1
    iget-object v3, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnRequestCompleteData;->state:Lcom/koushikdutta/async/util/UntypedHashtable;

    const-string v4, "body-cacher"

    invoke-virtual {v3, v4}, Lcom/koushikdutta/async/util/UntypedHashtable;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$BodyCacher;

    .line 291
    .local v2, "cacher":Lcom/koushikdutta/async/http/ResponseCacheMiddleware$BodyCacher;
    if-eqz v2, :cond_2

    .line 292
    iget-object v3, p1, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$OnRequestCompleteData;->exception:Ljava/lang/Exception;

    if-eqz v3, :cond_3

    .line 293
    invoke-virtual {v2}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$BodyCacher;->abort()V

    .line 297
    :cond_2
    :goto_0
    return-void

    .line 295
    :cond_3
    invoke-virtual {v2}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$BodyCacher;->commit()V

    goto :goto_0
.end method
