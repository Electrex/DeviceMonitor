.class final Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;
.super Ljava/lang/Object;
.source "ResponseCacheMiddleware.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/koushikdutta/async/http/ResponseCacheMiddleware;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Entry"
.end annotation


# instance fields
.field private final cipherSuite:Ljava/lang/String;

.field private final localCertificates:[Ljava/security/cert/Certificate;

.field private final peerCertificates:[Ljava/security/cert/Certificate;

.field private final requestMethod:Ljava/lang/String;

.field private final responseHeaders:Lcom/koushikdutta/async/http/libcore/RawHeaders;

.field private final uri:Ljava/lang/String;

.field private final varyHeaders:Lcom/koushikdutta/async/http/libcore/RawHeaders;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lcom/koushikdutta/async/http/libcore/RawHeaders;Lcom/koushikdutta/async/http/AsyncHttpRequest;Lcom/koushikdutta/async/http/libcore/ResponseHeaders;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "varyHeaders"    # Lcom/koushikdutta/async/http/libcore/RawHeaders;
    .param p3, "request"    # Lcom/koushikdutta/async/http/AsyncHttpRequest;
    .param p4, "responseHeaders"    # Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    .prologue
    const/4 v1, 0x0

    .line 555
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 556
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->uri:Ljava/lang/String;

    .line 557
    iput-object p2, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->varyHeaders:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    .line 558
    invoke-virtual {p3}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->getMethod()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->requestMethod:Ljava/lang/String;

    .line 559
    invoke-virtual {p4}, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;->getHeaders()Lcom/koushikdutta/async/http/libcore/RawHeaders;

    move-result-object v0

    iput-object v0, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->responseHeaders:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    .line 572
    iput-object v1, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->cipherSuite:Ljava/lang/String;

    .line 573
    iput-object v1, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->peerCertificates:[Ljava/security/cert/Certificate;

    .line 574
    iput-object v1, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->localCertificates:[Ljava/security/cert/Certificate;

    .line 576
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 10
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 518
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 519
    const/4 v1, 0x0

    .line 521
    .local v1, "reader":Lcom/koushikdutta/async/http/libcore/StrictLineReader;
    :try_start_0
    new-instance v2, Lcom/koushikdutta/async/http/libcore/StrictLineReader;

    sget-object v5, Lcom/koushikdutta/async/util/Charsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-direct {v2, p1, v5}, Lcom/koushikdutta/async/http/libcore/StrictLineReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 522
    .end local v1    # "reader":Lcom/koushikdutta/async/http/libcore/StrictLineReader;
    .local v2, "reader":Lcom/koushikdutta/async/http/libcore/StrictLineReader;
    :try_start_1
    invoke-virtual {v2}, Lcom/koushikdutta/async/http/libcore/StrictLineReader;->readLine()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->uri:Ljava/lang/String;

    .line 523
    invoke-virtual {v2}, Lcom/koushikdutta/async/http/libcore/StrictLineReader;->readLine()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->requestMethod:Ljava/lang/String;

    .line 524
    new-instance v5, Lcom/koushikdutta/async/http/libcore/RawHeaders;

    invoke-direct {v5}, Lcom/koushikdutta/async/http/libcore/RawHeaders;-><init>()V

    iput-object v5, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->varyHeaders:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    .line 525
    invoke-virtual {v2}, Lcom/koushikdutta/async/http/libcore/StrictLineReader;->readInt()I

    move-result v4

    .line 526
    .local v4, "varyRequestHeaderLineCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_0

    .line 527
    iget-object v5, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->varyHeaders:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    invoke-virtual {v2}, Lcom/koushikdutta/async/http/libcore/StrictLineReader;->readLine()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->addLine(Ljava/lang/String;)V

    .line 526
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 530
    :cond_0
    new-instance v5, Lcom/koushikdutta/async/http/libcore/RawHeaders;

    invoke-direct {v5}, Lcom/koushikdutta/async/http/libcore/RawHeaders;-><init>()V

    iput-object v5, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->responseHeaders:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    .line 531
    iget-object v5, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->responseHeaders:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    invoke-virtual {v2}, Lcom/koushikdutta/async/http/libcore/StrictLineReader;->readLine()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->setStatusLine(Ljava/lang/String;)V

    .line 532
    invoke-virtual {v2}, Lcom/koushikdutta/async/http/libcore/StrictLineReader;->readInt()I

    move-result v3

    .line 533
    .local v3, "responseHeaderLineCount":I
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    .line 534
    iget-object v5, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->responseHeaders:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    invoke-virtual {v2}, Lcom/koushikdutta/async/http/libcore/StrictLineReader;->readLine()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->addLine(Ljava/lang/String;)V

    .line 533
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 546
    :cond_1
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->cipherSuite:Ljava/lang/String;

    .line 547
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->peerCertificates:[Ljava/security/cert/Certificate;

    .line 548
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->localCertificates:[Ljava/security/cert/Certificate;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 551
    new-array v5, v9, [Ljava/io/Closeable;

    aput-object v2, v5, v7

    aput-object p1, v5, v8

    invoke-static {v5}, Lcom/koushikdutta/async/util/StreamUtility;->closeQuietly([Ljava/io/Closeable;)V

    .line 553
    return-void

    .line 551
    .end local v0    # "i":I
    .end local v2    # "reader":Lcom/koushikdutta/async/http/libcore/StrictLineReader;
    .end local v3    # "responseHeaderLineCount":I
    .end local v4    # "varyRequestHeaderLineCount":I
    .restart local v1    # "reader":Lcom/koushikdutta/async/http/libcore/StrictLineReader;
    :catchall_0
    move-exception v5

    :goto_2
    new-array v6, v9, [Ljava/io/Closeable;

    aput-object v1, v6, v7

    aput-object p1, v6, v8

    invoke-static {v6}, Lcom/koushikdutta/async/util/StreamUtility;->closeQuietly([Ljava/io/Closeable;)V

    throw v5

    .end local v1    # "reader":Lcom/koushikdutta/async/http/libcore/StrictLineReader;
    .restart local v2    # "reader":Lcom/koushikdutta/async/http/libcore/StrictLineReader;
    :catchall_1
    move-exception v5

    move-object v1, v2

    .end local v2    # "reader":Lcom/koushikdutta/async/http/libcore/StrictLineReader;
    .restart local v1    # "reader":Lcom/koushikdutta/async/http/libcore/StrictLineReader;
    goto :goto_2
.end method

.method static synthetic access$000(Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;)Z
    .locals 1
    .param p0, "x0"    # Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;

    .prologue
    .line 463
    invoke-direct {p0}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->isHttps()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;)Lcom/koushikdutta/async/http/libcore/RawHeaders;
    .locals 1
    .param p0, "x0"    # Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;

    .prologue
    .line 463
    iget-object v0, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->responseHeaders:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    return-object v0
.end method

.method private isHttps()Z
    .locals 2

    .prologue
    .line 607
    iget-object v0, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->uri:Ljava/lang/String;

    const-string v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private writeCertArray(Ljava/io/Writer;[Ljava/security/cert/Certificate;)V
    .locals 9
    .param p1, "writer"    # Ljava/io/Writer;
    .param p2, "certificates"    # [Ljava/security/cert/Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 631
    if-nez p2, :cond_1

    .line 632
    const-string v7, "-1\n"

    invoke-virtual {p1, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 645
    :cond_0
    return-void

    .line 636
    :cond_1
    :try_start_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    array-length v8, p2

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0xa

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 637
    move-object v0, p2

    .local v0, "arr$":[Ljava/security/cert/Certificate;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v0, v4

    .line 638
    .local v2, "certificate":Ljava/security/cert/Certificate;
    invoke-virtual {v2}, Ljava/security/cert/Certificate;->getEncoded()[B

    move-result-object v1

    .line 639
    .local v1, "bytes":[B
    const/4 v7, 0x0

    invoke-static {v1, v7}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v6

    .line 640
    .local v6, "line":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0xa

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 637
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 642
    .end local v0    # "arr$":[Ljava/security/cert/Certificate;
    .end local v1    # "bytes":[B
    .end local v2    # "certificate":Ljava/security/cert/Certificate;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "line":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 643
    .local v3, "e":Ljava/security/cert/CertificateEncodingException;
    new-instance v7, Ljava/io/IOException;

    invoke-virtual {v3}, Ljava/security/cert/CertificateEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7
.end method


# virtual methods
.method public matches(Landroid/net/Uri;Ljava/lang/String;Ljava/util/Map;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "requestMethod"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .line 649
    .local p3, "requestHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    iget-object v0, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->uri:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->requestMethod:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    iget-object v1, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->responseHeaders:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    invoke-direct {v0, p1, v1}, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;-><init>(Landroid/net/Uri;Lcom/koushikdutta/async/http/libcore/RawHeaders;)V

    iget-object v1, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->varyHeaders:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    invoke-virtual {v1}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->toMultimap()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;->varyMatches(Ljava/util/Map;Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeTo(Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryEditor;)V
    .locals 6
    .param p1, "editor"    # Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryEditor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0xa

    .line 579
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$EntryEditor;->newOutputStream(I)Ljava/io/FileOutputStream;

    move-result-object v1

    .line 580
    .local v1, "out":Ljava/io/OutputStream;
    new-instance v2, Ljava/io/BufferedWriter;

    new-instance v3, Ljava/io/OutputStreamWriter;

    sget-object v4, Lcom/koushikdutta/async/util/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v3, v1, v4}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 582
    .local v2, "writer":Ljava/io/Writer;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->uri:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 583
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->requestMethod:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 584
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->varyHeaders:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    invoke-virtual {v4}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->length()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 585
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->varyHeaders:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    invoke-virtual {v3}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 586
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->varyHeaders:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    invoke-virtual {v4, v0}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->getFieldName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->varyHeaders:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    invoke-virtual {v4, v0}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->getValue(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 585
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 590
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->responseHeaders:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    invoke-virtual {v4}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->getStatusLine()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 591
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->responseHeaders:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    invoke-virtual {v4}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->length()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 592
    const/4 v0, 0x0

    :goto_1
    iget-object v3, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->responseHeaders:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    invoke-virtual {v3}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->length()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 593
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->responseHeaders:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    invoke-virtual {v4, v0}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->getFieldName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->responseHeaders:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    invoke-virtual {v4, v0}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->getValue(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 592
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 597
    :cond_1
    invoke-direct {p0}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->isHttps()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 598
    invoke-virtual {v2, v5}, Ljava/io/Writer;->write(I)V

    .line 599
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->cipherSuite:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 600
    iget-object v3, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->peerCertificates:[Ljava/security/cert/Certificate;

    invoke-direct {p0, v2, v3}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->writeCertArray(Ljava/io/Writer;[Ljava/security/cert/Certificate;)V

    .line 601
    iget-object v3, p0, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->localCertificates:[Ljava/security/cert/Certificate;

    invoke-direct {p0, v2, v3}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware$Entry;->writeCertArray(Ljava/io/Writer;[Ljava/security/cert/Certificate;)V

    .line 603
    :cond_2
    invoke-virtual {v2}, Ljava/io/Writer;->close()V

    .line 604
    return-void
.end method
