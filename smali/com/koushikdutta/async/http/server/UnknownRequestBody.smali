.class public Lcom/koushikdutta/async/http/server/UnknownRequestBody;
.super Ljava/lang/Object;
.source "UnknownRequestBody.java"

# interfaces
.implements Lcom/koushikdutta/async/http/body/AsyncHttpRequestBody;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/koushikdutta/async/http/body/AsyncHttpRequestBody",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field emitter:Lcom/koushikdutta/async/DataEmitter;

.field length:I

.field private mContentType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "contentType"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lcom/koushikdutta/async/http/server/UnknownRequestBody;->length:I

    .line 14
    iput-object p1, p0, Lcom/koushikdutta/async/http/server/UnknownRequestBody;->mContentType:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method public getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/koushikdutta/async/http/server/UnknownRequestBody;->mContentType:Ljava/lang/String;

    return-object v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/koushikdutta/async/http/server/UnknownRequestBody;->length:I

    return v0
.end method

.method public parse(Lcom/koushikdutta/async/DataEmitter;Lcom/koushikdutta/async/callback/CompletedCallback;)V
    .locals 1
    .param p1, "emitter"    # Lcom/koushikdutta/async/DataEmitter;
    .param p2, "completed"    # Lcom/koushikdutta/async/callback/CompletedCallback;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/koushikdutta/async/http/server/UnknownRequestBody;->emitter:Lcom/koushikdutta/async/DataEmitter;

    .line 66
    invoke-interface {p1, p2}, Lcom/koushikdutta/async/DataEmitter;->setEndCallback(Lcom/koushikdutta/async/callback/CompletedCallback;)V

    .line 67
    new-instance v0, Lcom/koushikdutta/async/NullDataCallback;

    invoke-direct {v0}, Lcom/koushikdutta/async/NullDataCallback;-><init>()V

    invoke-interface {p1, v0}, Lcom/koushikdutta/async/DataEmitter;->setDataCallback(Lcom/koushikdutta/async/callback/DataCallback;)V

    .line 68
    return-void
.end method

.method public readFullyOnRequest()Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    return v0
.end method

.method public write(Lcom/koushikdutta/async/http/AsyncHttpRequest;Lcom/koushikdutta/async/DataSink;Lcom/koushikdutta/async/callback/CompletedCallback;)V
    .locals 1
    .param p1, "request"    # Lcom/koushikdutta/async/http/AsyncHttpRequest;
    .param p2, "sink"    # Lcom/koushikdutta/async/DataSink;
    .param p3, "completed"    # Lcom/koushikdutta/async/callback/CompletedCallback;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/koushikdutta/async/http/server/UnknownRequestBody;->emitter:Lcom/koushikdutta/async/DataEmitter;

    invoke-static {v0, p2, p3}, Lcom/koushikdutta/async/Util;->pump(Lcom/koushikdutta/async/DataEmitter;Lcom/koushikdutta/async/DataSink;Lcom/koushikdutta/async/callback/CompletedCallback;)V

    .line 27
    iget-object v0, p0, Lcom/koushikdutta/async/http/server/UnknownRequestBody;->emitter:Lcom/koushikdutta/async/DataEmitter;

    invoke-interface {v0}, Lcom/koushikdutta/async/DataEmitter;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/koushikdutta/async/http/server/UnknownRequestBody;->emitter:Lcom/koushikdutta/async/DataEmitter;

    invoke-interface {v0}, Lcom/koushikdutta/async/DataEmitter;->resume()V

    .line 29
    :cond_0
    return-void
.end method
