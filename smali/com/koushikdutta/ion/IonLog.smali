.class Lcom/koushikdutta/ion/IonLog;
.super Ljava/lang/Object;
.source "IonLog.java"


# static fields
.field public static debug:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const/4 v0, 0x1

    sput-boolean v0, Lcom/koushikdutta/ion/IonLog;->debug:Z

    return-void
.end method

.method public static w(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p0, "message"    # Ljava/lang/String;
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    .line 25
    const-string v0, "ION"

    invoke-static {v0, p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 26
    return-void
.end method
