.class Lcom/koushikdutta/ion/LoadDeepZoom$1;
.super Ljava/lang/Object;
.source "LoadDeepZoom.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/koushikdutta/ion/LoadDeepZoom;->onCompleted(Ljava/lang/Exception;Ljava/io/File;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/koushikdutta/ion/LoadDeepZoom;

.field final synthetic val$tempFile:Ljava/io/File;


# direct methods
.method constructor <init>(Lcom/koushikdutta/ion/LoadDeepZoom;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/koushikdutta/ion/LoadDeepZoom$1;->this$0:Lcom/koushikdutta/ion/LoadDeepZoom;

    iput-object p2, p0, Lcom/koushikdutta/ion/LoadDeepZoom$1;->val$tempFile:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 17

    .prologue
    .line 48
    const/4 v7, 0x0

    .line 52
    .local v7, "fin":Ljava/io/FileInputStream;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/koushikdutta/ion/LoadDeepZoom$1;->this$0:Lcom/koushikdutta/ion/LoadDeepZoom;

    iget-object v12, v12, Lcom/koushikdutta/ion/LoadDeepZoom;->fileCache:Lcom/koushikdutta/async/util/FileCache;

    if-eqz v12, :cond_0

    .line 53
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/koushikdutta/ion/LoadDeepZoom$1;->this$0:Lcom/koushikdutta/ion/LoadDeepZoom;

    iget-object v12, v12, Lcom/koushikdutta/ion/LoadDeepZoom;->fileCache:Lcom/koushikdutta/async/util/FileCache;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/koushikdutta/ion/LoadDeepZoom$1;->this$0:Lcom/koushikdutta/ion/LoadDeepZoom;

    iget-object v13, v13, Lcom/koushikdutta/ion/LoadDeepZoom;->key:Ljava/lang/String;

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/io/File;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/LoadDeepZoom$1;->val$tempFile:Ljava/io/File;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-virtual {v12, v13, v14}, Lcom/koushikdutta/async/util/FileCache;->commitTempFiles(Ljava/lang/String;[Ljava/io/File;)V

    .line 54
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/koushikdutta/ion/LoadDeepZoom$1;->this$0:Lcom/koushikdutta/ion/LoadDeepZoom;

    iget-object v12, v12, Lcom/koushikdutta/ion/LoadDeepZoom;->fileCache:Lcom/koushikdutta/async/util/FileCache;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/koushikdutta/ion/LoadDeepZoom$1;->this$0:Lcom/koushikdutta/ion/LoadDeepZoom;

    iget-object v13, v13, Lcom/koushikdutta/ion/LoadDeepZoom;->key:Ljava/lang/String;

    invoke-virtual {v12, v13}, Lcom/koushikdutta/async/util/FileCache;->getFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    .line 60
    .local v6, "file":Ljava/io/File;
    :goto_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/koushikdutta/ion/LoadDeepZoom$1;->this$0:Lcom/koushikdutta/ion/LoadDeepZoom;

    iget-object v12, v12, Lcom/koushikdutta/ion/LoadDeepZoom;->ion:Lcom/koushikdutta/ion/Ion;

    invoke-virtual {v12}, Lcom/koushikdutta/ion/Ion;->getBitmapCache()Lcom/koushikdutta/ion/bitmap/IonBitmapCache;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v12, v6, v13, v14}, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->prepareBitmapOptions(Ljava/io/File;II)Landroid/graphics/BitmapFactory$Options;

    move-result-object v10

    .line 61
    .local v10, "options":Landroid/graphics/BitmapFactory$Options;
    if-nez v10, :cond_1

    .line 62
    new-instance v12, Ljava/lang/Exception;

    const-string v13, "BitmapFactory.Options failed to load"

    invoke-direct {v12, v13}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v12
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    .end local v6    # "file":Ljava/io/File;
    .end local v10    # "options":Landroid/graphics/BitmapFactory$Options;
    :catch_0
    move-exception v5

    .line 105
    .local v5, "e":Ljava/lang/Exception;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/koushikdutta/ion/LoadDeepZoom$1;->this$0:Lcom/koushikdutta/ion/LoadDeepZoom;

    const/4 v13, 0x0

    invoke-virtual {v12, v5, v13}, Lcom/koushikdutta/ion/LoadDeepZoom;->report(Ljava/lang/Exception;Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 108
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/io/Closeable;

    const/4 v13, 0x0

    aput-object v7, v12, v13

    invoke-static {v12}, Lcom/koushikdutta/async/util/StreamUtility;->closeQuietly([Ljava/io/Closeable;)V

    .line 110
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 58
    :cond_0
    :try_start_2
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/koushikdutta/ion/LoadDeepZoom$1;->val$tempFile:Ljava/io/File;

    .restart local v6    # "file":Ljava/io/File;
    goto :goto_0

    .line 63
    .restart local v10    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_1
    new-instance v11, Landroid/graphics/Point;

    iget v12, v10, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v13, v10, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-direct {v11, v12, v13}, Landroid/graphics/Point;-><init>(II)V

    .line 64
    .local v11, "size":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/koushikdutta/ion/LoadDeepZoom$1;->this$0:Lcom/koushikdutta/ion/LoadDeepZoom;

    iget-boolean v12, v12, Lcom/koushikdutta/ion/LoadDeepZoom;->animateGif:Z

    if-eqz v12, :cond_6

    const-string v12, "image/gif"

    iget-object v13, v10, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    invoke-static {v12, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 65
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/koushikdutta/ion/LoadDeepZoom$1;->this$0:Lcom/koushikdutta/ion/LoadDeepZoom;

    iget-object v12, v12, Lcom/koushikdutta/ion/LoadDeepZoom;->fileCache:Lcom/koushikdutta/async/util/FileCache;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/koushikdutta/ion/LoadDeepZoom$1;->this$0:Lcom/koushikdutta/ion/LoadDeepZoom;

    iget-object v13, v13, Lcom/koushikdutta/ion/LoadDeepZoom;->key:Ljava/lang/String;

    invoke-virtual {v12, v13}, Lcom/koushikdutta/async/util/FileCache;->get(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v7

    .line 66
    new-instance v3, Lcom/koushikdutta/ion/gif/GifDecoder;

    new-instance v12, Lcom/koushikdutta/ion/LoadDeepZoom$1$1;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/koushikdutta/ion/LoadDeepZoom$1$1;-><init>(Lcom/koushikdutta/ion/LoadDeepZoom$1;)V

    invoke-direct {v3, v7, v12}, Lcom/koushikdutta/ion/gif/GifDecoder;-><init>(Ljava/io/InputStream;Lcom/koushikdutta/ion/gif/GifAction;)V

    .line 72
    .local v3, "decoder":Lcom/koushikdutta/ion/gif/GifDecoder;
    invoke-virtual {v3}, Lcom/koushikdutta/ion/gif/GifDecoder;->run()V

    .line 73
    invoke-virtual {v3}, Lcom/koushikdutta/ion/gif/GifDecoder;->getFrameCount()I

    move-result v12

    if-nez v12, :cond_2

    .line 74
    new-instance v12, Ljava/lang/Exception;

    const-string v13, "failed to load gif"

    invoke-direct {v12, v13}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v12
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 108
    .end local v3    # "decoder":Lcom/koushikdutta/ion/gif/GifDecoder;
    .end local v6    # "file":Ljava/io/File;
    .end local v10    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v11    # "size":Landroid/graphics/Point;
    :catchall_0
    move-exception v12

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/io/Closeable;

    const/4 v14, 0x0

    aput-object v7, v13, v14

    invoke-static {v13}, Lcom/koushikdutta/async/util/StreamUtility;->closeQuietly([Ljava/io/Closeable;)V

    throw v12

    .line 75
    .restart local v3    # "decoder":Lcom/koushikdutta/ion/gif/GifDecoder;
    .restart local v6    # "file":Ljava/io/File;
    .restart local v10    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v11    # "size":Landroid/graphics/Point;
    :cond_2
    :try_start_3
    invoke-virtual {v3}, Lcom/koushikdutta/ion/gif/GifDecoder;->getFrameCount()I

    move-result v12

    new-array v2, v12, [Landroid/graphics/Bitmap;

    .line 76
    .local v2, "bitmaps":[Landroid/graphics/Bitmap;
    invoke-virtual {v3}, Lcom/koushikdutta/ion/gif/GifDecoder;->getDelays()[I

    move-result-object v4

    .line 77
    .local v4, "delays":[I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_2
    invoke-virtual {v3}, Lcom/koushikdutta/ion/gif/GifDecoder;->getFrameCount()I

    move-result v12

    if-ge v8, v12, :cond_4

    .line 78
    invoke-virtual {v3, v8}, Lcom/koushikdutta/ion/gif/GifDecoder;->getFrameImage(I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 79
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    if-nez v1, :cond_3

    .line 80
    new-instance v12, Ljava/lang/Exception;

    const-string v13, "failed to load gif frame"

    invoke-direct {v12, v13}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v12

    .line 81
    :cond_3
    aput-object v1, v2, v8

    .line 77
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 83
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    :cond_4
    new-instance v9, Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/koushikdutta/ion/LoadDeepZoom$1;->this$0:Lcom/koushikdutta/ion/LoadDeepZoom;

    iget-object v12, v12, Lcom/koushikdutta/ion/LoadDeepZoom;->key:Ljava/lang/String;

    iget-object v13, v10, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    invoke-direct {v9, v12, v13, v2, v11}, Lcom/koushikdutta/ion/bitmap/BitmapInfo;-><init>(Ljava/lang/String;Ljava/lang/String;[Landroid/graphics/Bitmap;Landroid/graphics/Point;)V

    .line 84
    .local v9, "info":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    iput-object v4, v9, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->delays:[I

    .line 85
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/koushikdutta/ion/LoadDeepZoom$1;->this$0:Lcom/koushikdutta/ion/LoadDeepZoom;

    iget-object v12, v12, Lcom/koushikdutta/ion/LoadDeepZoom;->emitterTransform:Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;

    if-eqz v12, :cond_5

    .line 86
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/koushikdutta/ion/LoadDeepZoom$1;->this$0:Lcom/koushikdutta/ion/LoadDeepZoom;

    iget-object v12, v12, Lcom/koushikdutta/ion/LoadDeepZoom;->emitterTransform:Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;

    invoke-virtual {v12}, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->loadedFrom()I

    move-result v12

    iput v12, v9, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->loadedFrom:I

    .line 89
    :goto_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/koushikdutta/ion/LoadDeepZoom$1;->this$0:Lcom/koushikdutta/ion/LoadDeepZoom;

    const/4 v13, 0x0

    invoke-virtual {v12, v13, v9}, Lcom/koushikdutta/ion/LoadDeepZoom;->report(Ljava/lang/Exception;Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 108
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/io/Closeable;

    const/4 v13, 0x0

    aput-object v7, v12, v13

    invoke-static {v12}, Lcom/koushikdutta/async/util/StreamUtility;->closeQuietly([Ljava/io/Closeable;)V

    goto/16 :goto_1

    .line 88
    :cond_5
    const/4 v12, 0x1

    :try_start_4
    iput v12, v9, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->loadedFrom:I

    goto :goto_3

    .line 93
    .end local v2    # "bitmaps":[Landroid/graphics/Bitmap;
    .end local v3    # "decoder":Lcom/koushikdutta/ion/gif/GifDecoder;
    .end local v4    # "delays":[I
    .end local v8    # "i":I
    .end local v9    # "info":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    :cond_6
    invoke-virtual {v6}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    invoke-static {v12, v13}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/lang/String;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v3

    .line 94
    .local v3, "decoder":Landroid/graphics/BitmapRegionDecoder;
    new-instance v12, Landroid/graphics/Rect;

    const/4 v13, 0x0

    const/4 v14, 0x0

    iget v15, v11, Landroid/graphics/Point;->x:I

    iget v0, v11, Landroid/graphics/Point;->y:I

    move/from16 v16, v0

    invoke-direct/range {v12 .. v16}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v12, v10}, Landroid/graphics/BitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 95
    .restart local v1    # "bitmap":Landroid/graphics/Bitmap;
    if-nez v1, :cond_7

    .line 96
    new-instance v12, Ljava/lang/Exception;

    const-string v13, "unable to load decoder"

    invoke-direct {v12, v13}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v12

    .line 97
    :cond_7
    const/4 v12, 0x1

    new-array v2, v12, [Landroid/graphics/Bitmap;

    const/4 v12, 0x0

    aput-object v1, v2, v12

    .line 99
    .restart local v2    # "bitmaps":[Landroid/graphics/Bitmap;
    new-instance v9, Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/koushikdutta/ion/LoadDeepZoom$1;->this$0:Lcom/koushikdutta/ion/LoadDeepZoom;

    iget-object v12, v12, Lcom/koushikdutta/ion/LoadDeepZoom;->key:Ljava/lang/String;

    iget-object v13, v10, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    invoke-direct {v9, v12, v13, v2, v11}, Lcom/koushikdutta/ion/bitmap/BitmapInfo;-><init>(Ljava/lang/String;Ljava/lang/String;[Landroid/graphics/Bitmap;Landroid/graphics/Point;)V

    .line 100
    .restart local v9    # "info":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    iput-object v3, v9, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->decoder:Landroid/graphics/BitmapRegionDecoder;

    .line 101
    iput-object v6, v9, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->decoderFile:Ljava/io/File;

    .line 102
    const/4 v12, 0x3

    iput v12, v9, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->loadedFrom:I

    .line 103
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/koushikdutta/ion/LoadDeepZoom$1;->this$0:Lcom/koushikdutta/ion/LoadDeepZoom;

    const/4 v13, 0x0

    invoke-virtual {v12, v13, v9}, Lcom/koushikdutta/ion/LoadDeepZoom;->report(Ljava/lang/Exception;Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 108
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/io/Closeable;

    const/4 v13, 0x0

    aput-object v7, v12, v13

    invoke-static {v12}, Lcom/koushikdutta/async/util/StreamUtility;->closeQuietly([Ljava/io/Closeable;)V

    goto/16 :goto_1
.end method
