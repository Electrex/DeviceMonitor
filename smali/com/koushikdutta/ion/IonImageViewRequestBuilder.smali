.class public Lcom/koushikdutta/ion/IonImageViewRequestBuilder;
.super Lcom/koushikdutta/ion/IonBitmapRequestBuilder;
.source "IonImageViewRequestBuilder.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final FUTURE_IMAGEVIEW_NULL_URI:Lcom/koushikdutta/ion/IonDrawable$ImageViewFutureImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/koushikdutta/ion/IonImageViewRequestBuilder;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/koushikdutta/ion/IonImageViewRequestBuilder;->$assertionsDisabled:Z

    .line 21
    new-instance v0, Lcom/koushikdutta/ion/IonImageViewRequestBuilder$1;

    invoke-direct {v0}, Lcom/koushikdutta/ion/IonImageViewRequestBuilder$1;-><init>()V

    sput-object v0, Lcom/koushikdutta/ion/IonImageViewRequestBuilder;->FUTURE_IMAGEVIEW_NULL_URI:Lcom/koushikdutta/ion/IonDrawable$ImageViewFutureImpl;

    return-void

    .line 20
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/koushikdutta/ion/Ion;)V
    .locals 0
    .param p1, "ion"    # Lcom/koushikdutta/ion/Ion;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/koushikdutta/ion/IonBitmapRequestBuilder;-><init>(Lcom/koushikdutta/ion/Ion;)V

    .line 43
    return-void
.end method
