.class abstract Lcom/koushikdutta/ion/IonBitmapRequestBuilder;
.super Ljava/lang/Object;
.source "IonBitmapRequestBuilder.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final FUTURE_BITMAP_NULL_URI:Lcom/koushikdutta/async/future/SimpleFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/koushikdutta/async/future/SimpleFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field animateGif:Z

.field ion:Lcom/koushikdutta/ion/Ion;

.field scaleMode:Lcom/koushikdutta/ion/ScaleMode;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/koushikdutta/ion/IonBitmapRequestBuilder;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/koushikdutta/ion/IonBitmapRequestBuilder;->$assertionsDisabled:Z

    .line 26
    new-instance v0, Lcom/koushikdutta/ion/IonBitmapRequestBuilder$1;

    invoke-direct {v0}, Lcom/koushikdutta/ion/IonBitmapRequestBuilder$1;-><init>()V

    sput-object v0, Lcom/koushikdutta/ion/IonBitmapRequestBuilder;->FUTURE_BITMAP_NULL_URI:Lcom/koushikdutta/async/future/SimpleFuture;

    return-void

    .line 25
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/koushikdutta/ion/Ion;)V
    .locals 1
    .param p1, "ion"    # Lcom/koushikdutta/ion/Ion;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    sget-object v0, Lcom/koushikdutta/ion/ScaleMode;->FitXY:Lcom/koushikdutta/ion/ScaleMode;

    iput-object v0, p0, Lcom/koushikdutta/ion/IonBitmapRequestBuilder;->scaleMode:Lcom/koushikdutta/ion/ScaleMode;

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/koushikdutta/ion/IonBitmapRequestBuilder;->animateGif:Z

    .line 62
    iput-object p1, p0, Lcom/koushikdutta/ion/IonBitmapRequestBuilder;->ion:Lcom/koushikdutta/ion/Ion;

    .line 63
    return-void
.end method
