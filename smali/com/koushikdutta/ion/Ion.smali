.class public Lcom/koushikdutta/ion/Ion;
.super Ljava/lang/Object;
.source "Ion.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/koushikdutta/ion/Ion$Config;,
        Lcom/koushikdutta/ion/Ion$FutureSet;
    }
.end annotation


# static fields
.field static availableProcessors:I

.field static bitmapExecutorService:Ljava/util/concurrent/ExecutorService;

.field static instances:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/koushikdutta/ion/Ion;",
            ">;"
        }
    .end annotation
.end field

.field static ioExecutorService:Ljava/util/concurrent/ExecutorService;

.field static final mainHandler:Landroid/os/Handler;


# instance fields
.field assetLoader:Lcom/koushikdutta/ion/loader/AssetLoader;

.field bitmapBuilder:Lcom/koushikdutta/ion/IonImageViewRequestBuilder;

.field bitmapCache:Lcom/koushikdutta/ion/bitmap/IonBitmapCache;

.field bitmapsPending:Lcom/koushikdutta/async/util/HashList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/koushikdutta/async/util/HashList",
            "<",
            "Lcom/koushikdutta/async/future/FutureCallback",
            "<",
            "Lcom/koushikdutta/ion/bitmap/BitmapInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field config:Lcom/koushikdutta/ion/Ion$Config;

.field contentLoader:Lcom/koushikdutta/ion/loader/ContentLoader;

.field context:Landroid/content/Context;

.field cookieMiddleware:Lcom/koushikdutta/ion/cookie/CookieMiddleware;

.field fileLoader:Lcom/koushikdutta/ion/loader/FileLoader;

.field httpClient:Lcom/koushikdutta/async/http/AsyncHttpClient;

.field httpLoader:Lcom/koushikdutta/ion/loader/HttpLoader;

.field inFlight:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/Object;",
            "Lcom/koushikdutta/ion/Ion$FutureSet;",
            ">;"
        }
    .end annotation
.end field

.field loaders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/koushikdutta/ion/Loader;",
            ">;"
        }
    .end annotation
.end field

.field logLevel:I

.field logtag:Ljava/lang/String;

.field name:Ljava/lang/String;

.field packageIconLoader:Lcom/koushikdutta/ion/loader/PackageIconLoader;

.field private processDeferred:Ljava/lang/Runnable;

.field resourceLoader:Lcom/koushikdutta/ion/loader/ResourceLoader;

.field responseCache:Lcom/koushikdutta/async/http/ResponseCacheMiddleware;

.field storeCache:Lcom/koushikdutta/async/util/FileCache;

.field userAgent:Ljava/lang/String;

.field videoLoader:Lcom/koushikdutta/ion/loader/VideoLoader;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/koushikdutta/ion/Ion;->mainHandler:Landroid/os/Handler;

    .line 57
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    sput v0, Lcom/koushikdutta/ion/Ion;->availableProcessors:I

    .line 58
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/koushikdutta/ion/Ion;->ioExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 59
    sget v0, Lcom/koushikdutta/ion/Ion;->availableProcessors:I

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    sget v0, Lcom/koushikdutta/ion/Ion;->availableProcessors:I

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    :goto_0
    sput-object v0, Lcom/koushikdutta/ion/Ion;->bitmapExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/koushikdutta/ion/Ion;->instances:Ljava/util/HashMap;

    return-void

    .line 59
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/koushikdutta/ion/Ion;->loaders:Ljava/util/ArrayList;

    .line 164
    new-instance v3, Lcom/koushikdutta/async/util/HashList;

    invoke-direct {v3}, Lcom/koushikdutta/async/util/HashList;-><init>()V

    iput-object v3, p0, Lcom/koushikdutta/ion/Ion;->bitmapsPending:Lcom/koushikdutta/async/util/HashList;

    .line 165
    new-instance v3, Lcom/koushikdutta/ion/Ion$Config;

    invoke-direct {v3, p0}, Lcom/koushikdutta/ion/Ion$Config;-><init>(Lcom/koushikdutta/ion/Ion;)V

    iput-object v3, p0, Lcom/koushikdutta/ion/Ion;->config:Lcom/koushikdutta/ion/Ion$Config;

    .line 168
    new-instance v3, Lcom/koushikdutta/ion/IonImageViewRequestBuilder;

    invoke-direct {v3, p0}, Lcom/koushikdutta/ion/IonImageViewRequestBuilder;-><init>(Lcom/koushikdutta/ion/Ion;)V

    iput-object v3, p0, Lcom/koushikdutta/ion/Ion;->bitmapBuilder:Lcom/koushikdutta/ion/IonImageViewRequestBuilder;

    .line 294
    new-instance v3, Lcom/koushikdutta/ion/Ion$1;

    invoke-direct {v3, p0}, Lcom/koushikdutta/ion/Ion$1;-><init>(Lcom/koushikdutta/ion/Ion;)V

    iput-object v3, p0, Lcom/koushikdutta/ion/Ion;->processDeferred:Ljava/lang/Runnable;

    .line 421
    new-instance v3, Ljava/util/WeakHashMap;

    invoke-direct {v3}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v3, p0, Lcom/koushikdutta/ion/Ion;->inFlight:Ljava/util/WeakHashMap;

    .line 171
    new-instance v3, Lcom/koushikdutta/async/http/AsyncHttpClient;

    new-instance v4, Lcom/koushikdutta/async/AsyncServer;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ion-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/koushikdutta/async/AsyncServer;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Lcom/koushikdutta/async/http/AsyncHttpClient;-><init>(Lcom/koushikdutta/async/AsyncServer;)V

    iput-object v3, p0, Lcom/koushikdutta/ion/Ion;->httpClient:Lcom/koushikdutta/async/http/AsyncHttpClient;

    .line 172
    iget-object v3, p0, Lcom/koushikdutta/ion/Ion;->httpClient:Lcom/koushikdutta/async/http/AsyncHttpClient;

    invoke-virtual {v3}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getSSLSocketMiddleware()Lcom/koushikdutta/async/http/AsyncSSLSocketMiddleware;

    move-result-object v3

    new-instance v4, Lorg/apache/http/conn/ssl/BrowserCompatHostnameVerifier;

    invoke-direct {v4}, Lorg/apache/http/conn/ssl/BrowserCompatHostnameVerifier;-><init>()V

    invoke-virtual {v3, v4}, Lcom/koushikdutta/async/http/AsyncSSLSocketMiddleware;->setHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V

    .line 173
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/koushikdutta/ion/Ion;->context:Landroid/content/Context;

    .line 174
    iput-object p2, p0, Lcom/koushikdutta/ion/Ion;->name:Ljava/lang/String;

    .line 176
    new-instance v2, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 178
    .local v2, "ionCacheDir":Ljava/io/File;
    :try_start_0
    iget-object v3, p0, Lcom/koushikdutta/ion/Ion;->httpClient:Lcom/koushikdutta/async/http/AsyncHttpClient;

    const-wide/32 v4, 0xa00000

    invoke-static {v3, v2, v4, v5}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->addCache(Lcom/koushikdutta/async/http/AsyncHttpClient;Ljava/io/File;J)Lcom/koushikdutta/async/http/ResponseCacheMiddleware;

    move-result-object v3

    iput-object v3, p0, Lcom/koushikdutta/ion/Ion;->responseCache:Lcom/koushikdutta/async/http/ResponseCacheMiddleware;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 191
    :goto_0
    new-instance v3, Lcom/koushikdutta/async/util/FileCache;

    new-instance v4, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v5

    invoke-direct {v4, v5, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-wide v6, 0x7fffffffffffffffL

    const/4 v5, 0x0

    invoke-direct {v3, v4, v6, v7, v5}, Lcom/koushikdutta/async/util/FileCache;-><init>(Ljava/io/File;JZ)V

    iput-object v3, p0, Lcom/koushikdutta/ion/Ion;->storeCache:Lcom/koushikdutta/async/util/FileCache;

    .line 194
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x9

    if-lt v3, v4, :cond_0

    .line 195
    invoke-direct {p0}, Lcom/koushikdutta/ion/Ion;->addCookieMiddleware()V

    .line 197
    :cond_0
    iget-object v3, p0, Lcom/koushikdutta/ion/Ion;->httpClient:Lcom/koushikdutta/async/http/AsyncHttpClient;

    invoke-virtual {v3}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getSocketMiddleware()Lcom/koushikdutta/async/http/AsyncSocketMiddleware;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/koushikdutta/async/http/AsyncSocketMiddleware;->setConnectAllAddresses(Z)V

    .line 198
    iget-object v3, p0, Lcom/koushikdutta/ion/Ion;->httpClient:Lcom/koushikdutta/async/http/AsyncHttpClient;

    invoke-virtual {v3}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getSSLSocketMiddleware()Lcom/koushikdutta/async/http/AsyncSSLSocketMiddleware;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/koushikdutta/async/http/AsyncSSLSocketMiddleware;->setConnectAllAddresses(Z)V

    .line 200
    new-instance v3, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;

    invoke-direct {v3, p0}, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;-><init>(Lcom/koushikdutta/ion/Ion;)V

    iput-object v3, p0, Lcom/koushikdutta/ion/Ion;->bitmapCache:Lcom/koushikdutta/ion/bitmap/IonBitmapCache;

    .line 202
    invoke-virtual {p0}, Lcom/koushikdutta/ion/Ion;->configure()Lcom/koushikdutta/ion/Ion$Config;

    move-result-object v3

    new-instance v4, Lcom/koushikdutta/ion/loader/VideoLoader;

    invoke-direct {v4}, Lcom/koushikdutta/ion/loader/VideoLoader;-><init>()V

    iput-object v4, p0, Lcom/koushikdutta/ion/Ion;->videoLoader:Lcom/koushikdutta/ion/loader/VideoLoader;

    invoke-virtual {v3, v4}, Lcom/koushikdutta/ion/Ion$Config;->addLoader(Lcom/koushikdutta/ion/Loader;)Lcom/koushikdutta/ion/Ion$Config;

    move-result-object v3

    new-instance v4, Lcom/koushikdutta/ion/loader/PackageIconLoader;

    invoke-direct {v4}, Lcom/koushikdutta/ion/loader/PackageIconLoader;-><init>()V

    iput-object v4, p0, Lcom/koushikdutta/ion/Ion;->packageIconLoader:Lcom/koushikdutta/ion/loader/PackageIconLoader;

    invoke-virtual {v3, v4}, Lcom/koushikdutta/ion/Ion$Config;->addLoader(Lcom/koushikdutta/ion/Loader;)Lcom/koushikdutta/ion/Ion$Config;

    move-result-object v3

    new-instance v4, Lcom/koushikdutta/ion/loader/HttpLoader;

    invoke-direct {v4}, Lcom/koushikdutta/ion/loader/HttpLoader;-><init>()V

    iput-object v4, p0, Lcom/koushikdutta/ion/Ion;->httpLoader:Lcom/koushikdutta/ion/loader/HttpLoader;

    invoke-virtual {v3, v4}, Lcom/koushikdutta/ion/Ion$Config;->addLoader(Lcom/koushikdutta/ion/Loader;)Lcom/koushikdutta/ion/Ion$Config;

    move-result-object v3

    new-instance v4, Lcom/koushikdutta/ion/loader/ContentLoader;

    invoke-direct {v4}, Lcom/koushikdutta/ion/loader/ContentLoader;-><init>()V

    iput-object v4, p0, Lcom/koushikdutta/ion/Ion;->contentLoader:Lcom/koushikdutta/ion/loader/ContentLoader;

    invoke-virtual {v3, v4}, Lcom/koushikdutta/ion/Ion$Config;->addLoader(Lcom/koushikdutta/ion/Loader;)Lcom/koushikdutta/ion/Ion$Config;

    move-result-object v3

    new-instance v4, Lcom/koushikdutta/ion/loader/ResourceLoader;

    invoke-direct {v4}, Lcom/koushikdutta/ion/loader/ResourceLoader;-><init>()V

    iput-object v4, p0, Lcom/koushikdutta/ion/Ion;->resourceLoader:Lcom/koushikdutta/ion/loader/ResourceLoader;

    invoke-virtual {v3, v4}, Lcom/koushikdutta/ion/Ion$Config;->addLoader(Lcom/koushikdutta/ion/Loader;)Lcom/koushikdutta/ion/Ion$Config;

    move-result-object v3

    new-instance v4, Lcom/koushikdutta/ion/loader/AssetLoader;

    invoke-direct {v4}, Lcom/koushikdutta/ion/loader/AssetLoader;-><init>()V

    iput-object v4, p0, Lcom/koushikdutta/ion/Ion;->assetLoader:Lcom/koushikdutta/ion/loader/AssetLoader;

    invoke-virtual {v3, v4}, Lcom/koushikdutta/ion/Ion$Config;->addLoader(Lcom/koushikdutta/ion/Loader;)Lcom/koushikdutta/ion/Ion$Config;

    move-result-object v3

    new-instance v4, Lcom/koushikdutta/ion/loader/FileLoader;

    invoke-direct {v4}, Lcom/koushikdutta/ion/loader/FileLoader;-><init>()V

    iput-object v4, p0, Lcom/koushikdutta/ion/Ion;->fileLoader:Lcom/koushikdutta/ion/loader/FileLoader;

    invoke-virtual {v3, v4}, Lcom/koushikdutta/ion/Ion$Config;->addLoader(Lcom/koushikdutta/ion/Loader;)Lcom/koushikdutta/ion/Ion$Config;

    .line 210
    return-void

    .line 180
    :catch_0
    move-exception v0

    .line 181
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "unable to set up response cache, clearing"

    invoke-static {v3, v0}, Lcom/koushikdutta/ion/IonLog;->w(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 182
    invoke-static {v2}, Lcom/koushikdutta/async/util/FileUtility;->deleteDirectory(Ljava/io/File;)Z

    .line 184
    :try_start_1
    iget-object v3, p0, Lcom/koushikdutta/ion/Ion;->httpClient:Lcom/koushikdutta/async/http/AsyncHttpClient;

    const-wide/32 v4, 0xa00000

    invoke-static {v3, v2, v4, v5}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->addCache(Lcom/koushikdutta/async/http/AsyncHttpClient;Ljava/io/File;J)Lcom/koushikdutta/async/http/ResponseCacheMiddleware;

    move-result-object v3

    iput-object v3, p0, Lcom/koushikdutta/ion/Ion;->responseCache:Lcom/koushikdutta/async/http/ResponseCacheMiddleware;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 186
    :catch_1
    move-exception v1

    .line 187
    .local v1, "ex":Ljava/io/IOException;
    const-string v3, "unable to set up response cache, failing"

    invoke-static {v3, v0}, Lcom/koushikdutta/ion/IonLog;->w(Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0
.end method

.method private addCookieMiddleware()V
    .locals 4

    .prologue
    .line 424
    iget-object v0, p0, Lcom/koushikdutta/ion/Ion;->httpClient:Lcom/koushikdutta/async/http/AsyncHttpClient;

    new-instance v1, Lcom/koushikdutta/ion/cookie/CookieMiddleware;

    iget-object v2, p0, Lcom/koushikdutta/ion/Ion;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/koushikdutta/ion/Ion;->name:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/koushikdutta/ion/cookie/CookieMiddleware;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/koushikdutta/ion/Ion;->cookieMiddleware:Lcom/koushikdutta/ion/cookie/CookieMiddleware;

    invoke-virtual {v0, v1}, Lcom/koushikdutta/async/http/AsyncHttpClient;->insertMiddleware(Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware;)V

    .line 425
    return-void
.end method

.method public static getBitmapLoadExecutorService()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 213
    sget-object v0, Lcom/koushikdutta/ion/Ion;->bitmapExecutorService:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public static getDefault(Landroid/content/Context;)Lcom/koushikdutta/ion/Ion;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 120
    const-string v0, "ion"

    invoke-static {p0, v0}, Lcom/koushikdutta/ion/Ion;->getInstance(Landroid/content/Context;Ljava/lang/String;)Lcom/koushikdutta/ion/Ion;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;Ljava/lang/String;)Lcom/koushikdutta/ion/Ion;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 130
    if-nez p0, :cond_0

    .line 131
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Can not pass null context in to retrieve ion instance"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 132
    :cond_0
    sget-object v1, Lcom/koushikdutta/ion/Ion;->instances:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/koushikdutta/ion/Ion;

    .line 133
    .local v0, "instance":Lcom/koushikdutta/ion/Ion;
    if-nez v0, :cond_1

    .line 134
    sget-object v1, Lcom/koushikdutta/ion/Ion;->instances:Ljava/util/HashMap;

    new-instance v0, Lcom/koushikdutta/ion/Ion;

    .end local v0    # "instance":Lcom/koushikdutta/ion/Ion;
    invoke-direct {v0, p0, p1}, Lcom/koushikdutta/ion/Ion;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .restart local v0    # "instance":Lcom/koushikdutta/ion/Ion;
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    :cond_1
    return-object v0
.end method

.method public static with(Landroid/support/v4/app/Fragment;)Lcom/koushikdutta/ion/builder/LoadBuilder;
    .locals 1
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/Fragment;",
            ")",
            "Lcom/koushikdutta/ion/builder/LoadBuilder",
            "<",
            "Lcom/koushikdutta/ion/builder/Builders$Any$B;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/koushikdutta/ion/Ion;->getDefault(Landroid/content/Context;)Lcom/koushikdutta/ion/Ion;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/koushikdutta/ion/Ion;->build(Landroid/support/v4/app/Fragment;)Lcom/koushikdutta/ion/builder/LoadBuilder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method addFutureInFlight(Lcom/koushikdutta/async/future/Future;Ljava/lang/Object;)V
    .locals 2
    .param p1, "future"    # Lcom/koushikdutta/async/future/Future;
    .param p2, "group"    # Ljava/lang/Object;

    .prologue
    .line 350
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/koushikdutta/async/future/Future;->isDone()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {p1}, Lcom/koushikdutta/async/future/Future;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 363
    :cond_0
    :goto_0
    return-void

    .line 354
    :cond_1
    monitor-enter p0

    .line 355
    :try_start_0
    iget-object v1, p0, Lcom/koushikdutta/ion/Ion;->inFlight:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p2}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/koushikdutta/ion/Ion$FutureSet;

    .line 356
    .local v0, "members":Lcom/koushikdutta/ion/Ion$FutureSet;
    if-nez v0, :cond_2

    .line 357
    new-instance v0, Lcom/koushikdutta/ion/Ion$FutureSet;

    .end local v0    # "members":Lcom/koushikdutta/ion/Ion$FutureSet;
    invoke-direct {v0}, Lcom/koushikdutta/ion/Ion$FutureSet;-><init>()V

    .line 358
    .restart local v0    # "members":Lcom/koushikdutta/ion/Ion$FutureSet;
    iget-object v1, p0, Lcom/koushikdutta/ion/Ion;->inFlight:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p2, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 362
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/koushikdutta/ion/Ion$FutureSet;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 360
    .end local v0    # "members":Lcom/koushikdutta/ion/Ion$FutureSet;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public build(Landroid/support/v4/app/Fragment;)Lcom/koushikdutta/ion/builder/LoadBuilder;
    .locals 2
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/Fragment;",
            ")",
            "Lcom/koushikdutta/ion/builder/LoadBuilder",
            "<",
            "Lcom/koushikdutta/ion/builder/Builders$Any$B;",
            ">;"
        }
    .end annotation

    .prologue
    .line 266
    new-instance v0, Lcom/koushikdutta/ion/IonRequestBuilder;

    new-instance v1, Lcom/koushikdutta/ion/ContextReference$SupportFragmentContextReference;

    invoke-direct {v1, p1}, Lcom/koushikdutta/ion/ContextReference$SupportFragmentContextReference;-><init>(Landroid/support/v4/app/Fragment;)V

    invoke-direct {v0, v1, p0}, Lcom/koushikdutta/ion/IonRequestBuilder;-><init>(Lcom/koushikdutta/ion/ContextReference;Lcom/koushikdutta/ion/Ion;)V

    return-object v0
.end method

.method public configure()Lcom/koushikdutta/ion/Ion$Config;
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Lcom/koushikdutta/ion/Ion;->config:Lcom/koushikdutta/ion/Ion$Config;

    return-object v0
.end method

.method public getBitmapCache()Lcom/koushikdutta/ion/bitmap/IonBitmapCache;
    .locals 1

    .prologue
    .line 626
    iget-object v0, p0, Lcom/koushikdutta/ion/Ion;->bitmapCache:Lcom/koushikdutta/ion/bitmap/IonBitmapCache;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/koushikdutta/ion/Ion;->context:Landroid/content/Context;

    return-object v0
.end method

.method public getHttpClient()Lcom/koushikdutta/async/http/AsyncHttpClient;
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lcom/koushikdutta/ion/Ion;->httpClient:Lcom/koushikdutta/async/http/AsyncHttpClient;

    return-object v0
.end method

.method public getServer()Lcom/koushikdutta/async/AsyncServer;
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/koushikdutta/ion/Ion;->httpClient:Lcom/koushikdutta/async/http/AsyncHttpClient;

    invoke-virtual {v0}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getServer()Lcom/koushikdutta/async/AsyncServer;

    move-result-object v0

    return-object v0
.end method

.method processDeferred()V
    .locals 2

    .prologue
    .line 326
    sget-object v0, Lcom/koushikdutta/ion/Ion;->mainHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/koushikdutta/ion/Ion;->processDeferred:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 327
    sget-object v0, Lcom/koushikdutta/ion/Ion;->mainHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/koushikdutta/ion/Ion;->processDeferred:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 328
    return-void
.end method
