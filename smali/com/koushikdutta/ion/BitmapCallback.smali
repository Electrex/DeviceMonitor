.class abstract Lcom/koushikdutta/ion/BitmapCallback;
.super Ljava/lang/Object;
.source "BitmapCallback.java"


# instance fields
.field ion:Lcom/koushikdutta/ion/Ion;

.field key:Ljava/lang/String;

.field put:Z


# direct methods
.method protected constructor <init>(Lcom/koushikdutta/ion/Ion;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "ion"    # Lcom/koushikdutta/ion/Ion;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "put"    # Z

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p2, p0, Lcom/koushikdutta/ion/BitmapCallback;->key:Ljava/lang/String;

    .line 18
    iput-boolean p3, p0, Lcom/koushikdutta/ion/BitmapCallback;->put:Z

    .line 19
    iput-object p1, p0, Lcom/koushikdutta/ion/BitmapCallback;->ion:Lcom/koushikdutta/ion/Ion;

    .line 21
    iget-object v0, p1, Lcom/koushikdutta/ion/Ion;->bitmapsPending:Lcom/koushikdutta/async/util/HashList;

    invoke-virtual {v0, p2, p0}, Lcom/koushikdutta/async/util/HashList;->tag(Ljava/lang/String;Ljava/lang/Object;)V

    .line 22
    return-void
.end method


# virtual methods
.method protected onReported()V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/koushikdutta/ion/BitmapCallback;->ion:Lcom/koushikdutta/ion/Ion;

    invoke-virtual {v0}, Lcom/koushikdutta/ion/Ion;->processDeferred()V

    .line 32
    return-void
.end method

.method put()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/koushikdutta/ion/BitmapCallback;->put:Z

    return v0
.end method

.method protected report(Ljava/lang/Exception;Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V
    .locals 2
    .param p1, "e"    # Ljava/lang/Exception;
    .param p2, "info"    # Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    .prologue
    .line 35
    sget-object v0, Lcom/koushikdutta/ion/Ion;->mainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/koushikdutta/ion/BitmapCallback$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/koushikdutta/ion/BitmapCallback$1;-><init>(Lcom/koushikdutta/ion/BitmapCallback;Lcom/koushikdutta/ion/bitmap/BitmapInfo;Ljava/lang/Exception;)V

    invoke-static {v0, v1}, Lcom/koushikdutta/async/AsyncServer;->post(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 61
    return-void
.end method
