.class Lcom/koushikdutta/ion/RequestBodyUploadObserver$1;
.super Ljava/lang/Object;
.source "RequestBodyUploadObserver.java"

# interfaces
.implements Lcom/koushikdutta/async/DataSink;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/koushikdutta/ion/RequestBodyUploadObserver;->write(Lcom/koushikdutta/async/http/AsyncHttpRequest;Lcom/koushikdutta/async/DataSink;Lcom/koushikdutta/async/callback/CompletedCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/koushikdutta/ion/RequestBodyUploadObserver;

.field totalWritten:I

.field final synthetic val$length:I

.field final synthetic val$sink:Lcom/koushikdutta/async/DataSink;


# direct methods
.method constructor <init>(Lcom/koushikdutta/ion/RequestBodyUploadObserver;Lcom/koushikdutta/async/DataSink;I)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/koushikdutta/ion/RequestBodyUploadObserver$1;->this$0:Lcom/koushikdutta/ion/RequestBodyUploadObserver;

    iput-object p2, p0, Lcom/koushikdutta/ion/RequestBodyUploadObserver$1;->val$sink:Lcom/koushikdutta/async/DataSink;

    iput p3, p0, Lcom/koushikdutta/ion/RequestBodyUploadObserver$1;->val$length:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public end()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/koushikdutta/ion/RequestBodyUploadObserver$1;->val$sink:Lcom/koushikdutta/async/DataSink;

    invoke-interface {v0}, Lcom/koushikdutta/async/DataSink;->end()V

    .line 71
    return-void
.end method

.method public getWriteableCallback()Lcom/koushikdutta/async/callback/WritableCallback;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/koushikdutta/ion/RequestBodyUploadObserver$1;->val$sink:Lcom/koushikdutta/async/DataSink;

    invoke-interface {v0}, Lcom/koushikdutta/async/DataSink;->getWriteableCallback()Lcom/koushikdutta/async/callback/WritableCallback;

    move-result-object v0

    return-object v0
.end method

.method public isOpen()Z
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/koushikdutta/ion/RequestBodyUploadObserver$1;->val$sink:Lcom/koushikdutta/async/DataSink;

    invoke-interface {v0}, Lcom/koushikdutta/async/DataSink;->isOpen()Z

    move-result v0

    return v0
.end method

.method public setClosedCallback(Lcom/koushikdutta/async/callback/CompletedCallback;)V
    .locals 1
    .param p1, "handler"    # Lcom/koushikdutta/async/callback/CompletedCallback;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/koushikdutta/ion/RequestBodyUploadObserver$1;->val$sink:Lcom/koushikdutta/async/DataSink;

    invoke-interface {v0, p1}, Lcom/koushikdutta/async/DataSink;->setClosedCallback(Lcom/koushikdutta/async/callback/CompletedCallback;)V

    .line 76
    return-void
.end method

.method public setWriteableCallback(Lcom/koushikdutta/async/callback/WritableCallback;)V
    .locals 1
    .param p1, "handler"    # Lcom/koushikdutta/async/callback/WritableCallback;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/koushikdutta/ion/RequestBodyUploadObserver$1;->val$sink:Lcom/koushikdutta/async/DataSink;

    invoke-interface {v0, p1}, Lcom/koushikdutta/async/DataSink;->setWriteableCallback(Lcom/koushikdutta/async/callback/WritableCallback;)V

    .line 51
    return-void
.end method

.method public write(Lcom/koushikdutta/async/ByteBufferList;)V
    .locals 8
    .param p1, "bb"    # Lcom/koushikdutta/async/ByteBufferList;

    .prologue
    .line 41
    invoke-virtual {p1}, Lcom/koushikdutta/async/ByteBufferList;->remaining()I

    move-result v0

    .line 42
    .local v0, "start":I
    iget-object v2, p0, Lcom/koushikdutta/ion/RequestBodyUploadObserver$1;->val$sink:Lcom/koushikdutta/async/DataSink;

    invoke-interface {v2, p1}, Lcom/koushikdutta/async/DataSink;->write(Lcom/koushikdutta/async/ByteBufferList;)V

    .line 43
    invoke-virtual {p1}, Lcom/koushikdutta/async/ByteBufferList;->remaining()I

    move-result v2

    sub-int v1, v0, v2

    .line 44
    .local v1, "wrote":I
    iget v2, p0, Lcom/koushikdutta/ion/RequestBodyUploadObserver$1;->totalWritten:I

    add-int/2addr v2, v1

    iput v2, p0, Lcom/koushikdutta/ion/RequestBodyUploadObserver$1;->totalWritten:I

    .line 45
    iget-object v2, p0, Lcom/koushikdutta/ion/RequestBodyUploadObserver$1;->this$0:Lcom/koushikdutta/ion/RequestBodyUploadObserver;

    iget-object v2, v2, Lcom/koushikdutta/ion/RequestBodyUploadObserver;->callback:Lcom/koushikdutta/ion/ProgressCallback;

    iget v3, p0, Lcom/koushikdutta/ion/RequestBodyUploadObserver$1;->totalWritten:I

    int-to-long v4, v3

    iget v3, p0, Lcom/koushikdutta/ion/RequestBodyUploadObserver$1;->val$length:I

    int-to-long v6, v3

    invoke-interface {v2, v4, v5, v6, v7}, Lcom/koushikdutta/ion/ProgressCallback;->onProgress(JJ)V

    .line 46
    return-void
.end method

.method public write(Ljava/nio/ByteBuffer;)V
    .locals 8
    .param p1, "bb"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 32
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    .line 33
    .local v0, "start":I
    iget-object v2, p0, Lcom/koushikdutta/ion/RequestBodyUploadObserver$1;->val$sink:Lcom/koushikdutta/async/DataSink;

    invoke-interface {v2, p1}, Lcom/koushikdutta/async/DataSink;->write(Ljava/nio/ByteBuffer;)V

    .line 34
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    sub-int v1, v0, v2

    .line 35
    .local v1, "wrote":I
    iget v2, p0, Lcom/koushikdutta/ion/RequestBodyUploadObserver$1;->totalWritten:I

    add-int/2addr v2, v1

    iput v2, p0, Lcom/koushikdutta/ion/RequestBodyUploadObserver$1;->totalWritten:I

    .line 36
    iget-object v2, p0, Lcom/koushikdutta/ion/RequestBodyUploadObserver$1;->this$0:Lcom/koushikdutta/ion/RequestBodyUploadObserver;

    iget-object v2, v2, Lcom/koushikdutta/ion/RequestBodyUploadObserver;->callback:Lcom/koushikdutta/ion/ProgressCallback;

    iget v3, p0, Lcom/koushikdutta/ion/RequestBodyUploadObserver$1;->totalWritten:I

    int-to-long v4, v3

    iget v3, p0, Lcom/koushikdutta/ion/RequestBodyUploadObserver$1;->val$length:I

    int-to-long v6, v3

    invoke-interface {v2, v4, v5, v6, v7}, Lcom/koushikdutta/ion/ProgressCallback;->onProgress(JJ)V

    .line 37
    return-void
.end method
