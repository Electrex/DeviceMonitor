.class public Lcom/koushikdutta/ion/loader/StreamLoader;
.super Lcom/koushikdutta/ion/loader/SimpleLoader;
.source "StreamLoader.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/koushikdutta/ion/loader/SimpleLoader;-><init>()V

    return-void
.end method


# virtual methods
.method protected getInputStream(Landroid/content/Context;Ljava/lang/String;)Ljava/io/InputStream;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 51
    const/4 v0, 0x0

    return-object v0
.end method

.method public loadBitmap(Landroid/content/Context;Lcom/koushikdutta/ion/Ion;Ljava/lang/String;Ljava/lang/String;IIZ)Lcom/koushikdutta/async/future/Future;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ion"    # Lcom/koushikdutta/ion/Ion;
    .param p3, "key"    # Ljava/lang/String;
    .param p4, "uri"    # Ljava/lang/String;
    .param p5, "resizeWidth"    # I
    .param p6, "resizeHeight"    # I
    .param p7, "animateGif"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/koushikdutta/ion/Ion;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IIZ)",
            "Lcom/koushikdutta/async/future/Future",
            "<",
            "Lcom/koushikdutta/ion/bitmap/BitmapInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    new-instance v9, Lcom/koushikdutta/async/future/SimpleFuture;

    invoke-direct {v9}, Lcom/koushikdutta/async/future/SimpleFuture;-><init>()V

    .line 59
    .local v9, "ret":Lcom/koushikdutta/async/future/SimpleFuture;, "Lcom/koushikdutta/async/future/SimpleFuture<Lcom/koushikdutta/ion/bitmap/BitmapInfo;>;"
    invoke-static {}, Lcom/koushikdutta/ion/Ion;->getBitmapLoadExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v10

    new-instance v0, Lcom/koushikdutta/ion/loader/StreamLoader$2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p4

    move-object v4, p2

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object v8, p3

    invoke-direct/range {v0 .. v9}, Lcom/koushikdutta/ion/loader/StreamLoader$2;-><init>(Lcom/koushikdutta/ion/loader/StreamLoader;Landroid/content/Context;Ljava/lang/String;Lcom/koushikdutta/ion/Ion;IIZLjava/lang/String;Lcom/koushikdutta/async/future/SimpleFuture;)V

    invoke-interface {v10, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 96
    return-object v9
.end method

.method protected loadGif(Ljava/lang/String;Landroid/graphics/Point;Ljava/io/InputStream;Landroid/graphics/BitmapFactory$Options;)Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    .locals 8
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "size"    # Landroid/graphics/Point;
    .param p3, "in"    # Ljava/io/InputStream;
    .param p4, "options"    # Landroid/graphics/BitmapFactory$Options;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 27
    new-instance v2, Lcom/koushikdutta/ion/gif/GifDecoder;

    new-instance v6, Lcom/koushikdutta/ion/loader/StreamLoader$1;

    invoke-direct {v6, p0}, Lcom/koushikdutta/ion/loader/StreamLoader$1;-><init>(Lcom/koushikdutta/ion/loader/StreamLoader;)V

    invoke-direct {v2, p3, v6}, Lcom/koushikdutta/ion/gif/GifDecoder;-><init>(Ljava/io/InputStream;Lcom/koushikdutta/ion/gif/GifAction;)V

    .line 33
    .local v2, "decoder":Lcom/koushikdutta/ion/gif/GifDecoder;
    invoke-virtual {v2}, Lcom/koushikdutta/ion/gif/GifDecoder;->run()V

    .line 34
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/io/Closeable;

    const/4 v7, 0x0

    aput-object p3, v6, v7

    invoke-static {v6}, Lcom/koushikdutta/async/util/StreamUtility;->closeQuietly([Ljava/io/Closeable;)V

    .line 35
    invoke-virtual {v2}, Lcom/koushikdutta/ion/gif/GifDecoder;->getFrameCount()I

    move-result v6

    if-nez v6, :cond_0

    .line 36
    new-instance v6, Ljava/lang/Exception;

    const-string v7, "failed to load gif"

    invoke-direct {v6, v7}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v6

    .line 37
    :cond_0
    invoke-virtual {v2}, Lcom/koushikdutta/ion/gif/GifDecoder;->getFrameCount()I

    move-result v6

    new-array v1, v6, [Landroid/graphics/Bitmap;

    .line 38
    .local v1, "bitmaps":[Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Lcom/koushikdutta/ion/gif/GifDecoder;->getDelays()[I

    move-result-object v3

    .line 39
    .local v3, "delays":[I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v2}, Lcom/koushikdutta/ion/gif/GifDecoder;->getFrameCount()I

    move-result v6

    if-ge v4, v6, :cond_2

    .line 40
    invoke-virtual {v2, v4}, Lcom/koushikdutta/ion/gif/GifDecoder;->getFrameImage(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 41
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    .line 42
    new-instance v6, Ljava/lang/Exception;

    const-string v7, "failed to load gif frame"

    invoke-direct {v6, v7}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v6

    .line 43
    :cond_1
    aput-object v0, v1, v4

    .line 39
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 45
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_2
    new-instance v5, Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v6, p4, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    invoke-direct {v5, p1, v6, v1, p2}, Lcom/koushikdutta/ion/bitmap/BitmapInfo;-><init>(Ljava/lang/String;Ljava/lang/String;[Landroid/graphics/Bitmap;Landroid/graphics/Point;)V

    .line 46
    .local v5, "info":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    iput-object v3, v5, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->delays:[I

    .line 47
    return-object v5
.end method
