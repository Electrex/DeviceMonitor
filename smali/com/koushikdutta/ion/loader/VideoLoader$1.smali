.class Lcom/koushikdutta/ion/loader/VideoLoader$1;
.super Ljava/lang/Object;
.source "VideoLoader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/koushikdutta/ion/loader/VideoLoader;->loadBitmap(Landroid/content/Context;Lcom/koushikdutta/ion/Ion;Ljava/lang/String;Ljava/lang/String;IIZ)Lcom/koushikdutta/async/future/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/koushikdutta/ion/loader/VideoLoader;

.field final synthetic val$key:Ljava/lang/String;

.field final synthetic val$ret:Lcom/koushikdutta/async/future/SimpleFuture;

.field final synthetic val$type:Lcom/koushikdutta/ion/loader/MediaFile$MediaFileType;

.field final synthetic val$uri:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/koushikdutta/ion/loader/VideoLoader;Ljava/lang/String;Lcom/koushikdutta/async/future/SimpleFuture;Ljava/lang/String;Lcom/koushikdutta/ion/loader/MediaFile$MediaFileType;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/koushikdutta/ion/loader/VideoLoader$1;->this$0:Lcom/koushikdutta/ion/loader/VideoLoader;

    iput-object p2, p0, Lcom/koushikdutta/ion/loader/VideoLoader$1;->val$uri:Ljava/lang/String;

    iput-object p3, p0, Lcom/koushikdutta/ion/loader/VideoLoader$1;->val$ret:Lcom/koushikdutta/async/future/SimpleFuture;

    iput-object p4, p0, Lcom/koushikdutta/ion/loader/VideoLoader$1;->val$key:Ljava/lang/String;

    iput-object p5, p0, Lcom/koushikdutta/ion/loader/VideoLoader$1;->val$type:Lcom/koushikdutta/ion/loader/MediaFile$MediaFileType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 51
    new-instance v2, Ljava/io/File;

    iget-object v4, p0, Lcom/koushikdutta/ion/loader/VideoLoader$1;->val$uri:Ljava/lang/String;

    invoke-static {v4}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/net/URI;)V

    .line 52
    .local v2, "file":Ljava/io/File;
    iget-object v4, p0, Lcom/koushikdutta/ion/loader/VideoLoader$1;->val$ret:Lcom/koushikdutta/async/future/SimpleFuture;

    invoke-virtual {v4}, Lcom/koushikdutta/async/future/SimpleFuture;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 71
    :goto_0
    return-void

    .line 59
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/koushikdutta/ion/loader/VideoLoader;->mustUseThumbnailUtils()Z

    move-result v4

    if-nez v4, :cond_1

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xa

    if-ge v4, v5, :cond_2

    .line 60
    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 63
    .local v0, "bmp":Landroid/graphics/Bitmap;
    :goto_1
    if-nez v0, :cond_3

    .line 64
    new-instance v4, Ljava/lang/Exception;

    const-string v5, "video bitmap failed to load"

    invoke-direct {v4, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v1

    .line 69
    .local v1, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/koushikdutta/ion/loader/VideoLoader$1;->val$ret:Lcom/koushikdutta/async/future/SimpleFuture;

    invoke-virtual {v4, v1}, Lcom/koushikdutta/async/future/SimpleFuture;->setComplete(Ljava/lang/Exception;)Z

    goto :goto_0

    .line 62
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_1
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/koushikdutta/ion/loader/VideoLoader;->createVideoThumbnail(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .restart local v0    # "bmp":Landroid/graphics/Bitmap;
    goto :goto_1

    .line 65
    :cond_3
    new-instance v3, Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v4, p0, Lcom/koushikdutta/ion/loader/VideoLoader$1;->val$key:Ljava/lang/String;

    iget-object v5, p0, Lcom/koushikdutta/ion/loader/VideoLoader$1;->val$type:Lcom/koushikdutta/ion/loader/MediaFile$MediaFileType;

    iget-object v5, v5, Lcom/koushikdutta/ion/loader/MediaFile$MediaFileType;->mimeType:Ljava/lang/String;

    const/4 v6, 0x1

    new-array v6, v6, [Landroid/graphics/Bitmap;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    new-instance v7, Landroid/graphics/Point;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-direct {v7, v8, v9}, Landroid/graphics/Point;-><init>(II)V

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/koushikdutta/ion/bitmap/BitmapInfo;-><init>(Ljava/lang/String;Ljava/lang/String;[Landroid/graphics/Bitmap;Landroid/graphics/Point;)V

    .line 66
    .local v3, "info":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    const/4 v4, 0x1

    iput v4, v3, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->loadedFrom:I

    .line 67
    iget-object v4, p0, Lcom/koushikdutta/ion/loader/VideoLoader$1;->val$ret:Lcom/koushikdutta/async/future/SimpleFuture;

    invoke-virtual {v4, v3}, Lcom/koushikdutta/async/future/SimpleFuture;->setComplete(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
