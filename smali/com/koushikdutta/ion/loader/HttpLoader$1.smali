.class Lcom/koushikdutta/ion/loader/HttpLoader$1;
.super Ljava/lang/Object;
.source "HttpLoader.java"

# interfaces
.implements Lcom/koushikdutta/async/http/callback/HttpConnectCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/koushikdutta/ion/loader/HttpLoader;->load(Lcom/koushikdutta/ion/Ion;Lcom/koushikdutta/async/http/AsyncHttpRequest;Lcom/koushikdutta/async/future/FutureCallback;)Lcom/koushikdutta/async/future/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/koushikdutta/ion/loader/HttpLoader;

.field final synthetic val$callback:Lcom/koushikdutta/async/future/FutureCallback;


# direct methods
.method constructor <init>(Lcom/koushikdutta/ion/loader/HttpLoader;Lcom/koushikdutta/async/future/FutureCallback;)V
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lcom/koushikdutta/ion/loader/HttpLoader$1;->this$0:Lcom/koushikdutta/ion/loader/HttpLoader;

    iput-object p2, p0, Lcom/koushikdutta/ion/loader/HttpLoader$1;->val$callback:Lcom/koushikdutta/async/future/FutureCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnectCompleted(Ljava/lang/Exception;Lcom/koushikdutta/async/http/AsyncHttpResponse;)V
    .locals 9
    .param p1, "ex"    # Ljava/lang/Exception;
    .param p2, "response"    # Lcom/koushikdutta/async/http/AsyncHttpResponse;

    .prologue
    .line 27
    const-wide/16 v2, -0x1

    .line 28
    .local v2, "length":J
    const/4 v4, 0x3

    .line 29
    .local v4, "loadedFrom":I
    const/4 v5, 0x0

    .line 30
    .local v5, "headers":Lcom/koushikdutta/async/http/libcore/RawHeaders;
    const/4 v6, 0x0

    .line 31
    .local v6, "request":Lcom/koushikdutta/async/http/AsyncHttpRequest;
    if-eqz p2, :cond_0

    .line 32
    invoke-interface {p2}, Lcom/koushikdutta/async/http/AsyncHttpResponse;->getRequest()Lcom/koushikdutta/async/http/AsyncHttpRequest;

    move-result-object v6

    .line 33
    invoke-interface {p2}, Lcom/koushikdutta/async/http/AsyncHttpResponse;->getHeaders()Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    move-result-object v0

    invoke-virtual {v0}, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;->getHeaders()Lcom/koushikdutta/async/http/libcore/RawHeaders;

    move-result-object v5

    .line 34
    invoke-interface {p2}, Lcom/koushikdutta/async/http/AsyncHttpResponse;->getHeaders()Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    move-result-object v0

    invoke-virtual {v0}, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;->getContentLength()J

    move-result-wide v2

    .line 35
    invoke-interface {p2}, Lcom/koushikdutta/async/http/AsyncHttpResponse;->getHeaders()Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    move-result-object v0

    invoke-virtual {v0}, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;->getHeaders()Lcom/koushikdutta/async/http/libcore/RawHeaders;

    move-result-object v0

    const-string v1, "X-Served-From"

    invoke-virtual {v0, v1}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 36
    .local v7, "servedFrom":Ljava/lang/String;
    const-string v0, "cache"

    invoke-static {v7, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 37
    const/4 v4, 0x1

    .line 41
    .end local v7    # "servedFrom":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v8, p0, Lcom/koushikdutta/ion/loader/HttpLoader$1;->val$callback:Lcom/koushikdutta/async/future/FutureCallback;

    new-instance v0, Lcom/koushikdutta/ion/Loader$LoaderEmitter;

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lcom/koushikdutta/ion/Loader$LoaderEmitter;-><init>(Lcom/koushikdutta/async/DataEmitter;JILcom/koushikdutta/async/http/libcore/RawHeaders;Lcom/koushikdutta/async/http/AsyncHttpRequest;)V

    invoke-interface {v8, p1, v0}, Lcom/koushikdutta/async/future/FutureCallback;->onCompleted(Ljava/lang/Exception;Ljava/lang/Object;)V

    .line 42
    return-void

    .line 38
    .restart local v7    # "servedFrom":Ljava/lang/String;
    :cond_1
    const-string v0, "conditional-cache"

    invoke-static {v7, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    const/4 v4, 0x2

    goto :goto_0
.end method
