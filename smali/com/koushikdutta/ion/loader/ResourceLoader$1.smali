.class Lcom/koushikdutta/ion/loader/ResourceLoader$1;
.super Ljava/lang/Object;
.source "ResourceLoader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/koushikdutta/ion/loader/ResourceLoader;->loadBitmap(Landroid/content/Context;Lcom/koushikdutta/ion/Ion;Ljava/lang/String;Ljava/lang/String;IIZ)Lcom/koushikdutta/async/future/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/koushikdutta/ion/loader/ResourceLoader;

.field final synthetic val$animateGif:Z

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$ion:Lcom/koushikdutta/ion/Ion;

.field final synthetic val$key:Ljava/lang/String;

.field final synthetic val$resizeHeight:I

.field final synthetic val$resizeWidth:I

.field final synthetic val$ret:Lcom/koushikdutta/async/future/SimpleFuture;

.field final synthetic val$uri:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/koushikdutta/ion/loader/ResourceLoader;Landroid/content/Context;Ljava/lang/String;Lcom/koushikdutta/ion/Ion;IIZLjava/lang/String;Lcom/koushikdutta/async/future/SimpleFuture;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/koushikdutta/ion/loader/ResourceLoader$1;->this$0:Lcom/koushikdutta/ion/loader/ResourceLoader;

    iput-object p2, p0, Lcom/koushikdutta/ion/loader/ResourceLoader$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/koushikdutta/ion/loader/ResourceLoader$1;->val$uri:Ljava/lang/String;

    iput-object p4, p0, Lcom/koushikdutta/ion/loader/ResourceLoader$1;->val$ion:Lcom/koushikdutta/ion/Ion;

    iput p5, p0, Lcom/koushikdutta/ion/loader/ResourceLoader$1;->val$resizeWidth:I

    iput p6, p0, Lcom/koushikdutta/ion/loader/ResourceLoader$1;->val$resizeHeight:I

    iput-boolean p7, p0, Lcom/koushikdutta/ion/loader/ResourceLoader$1;->val$animateGif:Z

    iput-object p8, p0, Lcom/koushikdutta/ion/loader/ResourceLoader$1;->val$key:Ljava/lang/String;

    iput-object p9, p0, Lcom/koushikdutta/ion/loader/ResourceLoader$1;->val$ret:Lcom/koushikdutta/async/future/SimpleFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 76
    :try_start_0
    iget-object v7, p0, Lcom/koushikdutta/ion/loader/ResourceLoader$1;->val$context:Landroid/content/Context;

    iget-object v8, p0, Lcom/koushikdutta/ion/loader/ResourceLoader$1;->val$uri:Ljava/lang/String;

    # invokes: Lcom/koushikdutta/ion/loader/ResourceLoader;->lookupResource(Landroid/content/Context;Ljava/lang/String;)Lcom/koushikdutta/ion/loader/ResourceLoader$Resource;
    invoke-static {v7, v8}, Lcom/koushikdutta/ion/loader/ResourceLoader;->access$100(Landroid/content/Context;Ljava/lang/String;)Lcom/koushikdutta/ion/loader/ResourceLoader$Resource;

    move-result-object v5

    .line 77
    .local v5, "res":Lcom/koushikdutta/ion/loader/ResourceLoader$Resource;
    iget-object v7, p0, Lcom/koushikdutta/ion/loader/ResourceLoader$1;->val$ion:Lcom/koushikdutta/ion/Ion;

    invoke-virtual {v7}, Lcom/koushikdutta/ion/Ion;->getBitmapCache()Lcom/koushikdutta/ion/bitmap/IonBitmapCache;

    move-result-object v7

    iget-object v8, v5, Lcom/koushikdutta/ion/loader/ResourceLoader$Resource;->res:Landroid/content/res/Resources;

    iget v9, v5, Lcom/koushikdutta/ion/loader/ResourceLoader$Resource;->id:I

    iget v10, p0, Lcom/koushikdutta/ion/loader/ResourceLoader$1;->val$resizeWidth:I

    iget v11, p0, Lcom/koushikdutta/ion/loader/ResourceLoader$1;->val$resizeHeight:I

    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->prepareBitmapOptions(Landroid/content/res/Resources;III)Landroid/graphics/BitmapFactory$Options;

    move-result-object v4

    .line 78
    .local v4, "options":Landroid/graphics/BitmapFactory$Options;
    if-nez v4, :cond_0

    .line 79
    new-instance v7, Ljava/lang/Exception;

    const-string v8, "BitmapFactory.Options failed to load"

    invoke-direct {v7, v8}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 100
    .end local v4    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v5    # "res":Lcom/koushikdutta/ion/loader/ResourceLoader$Resource;
    :catch_0
    move-exception v1

    .line 101
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    iget-object v7, p0, Lcom/koushikdutta/ion/loader/ResourceLoader$1;->val$ret:Lcom/koushikdutta/async/future/SimpleFuture;

    new-instance v8, Ljava/lang/Exception;

    invoke-direct {v8, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Lcom/koushikdutta/async/future/SimpleFuture;->setComplete(Ljava/lang/Exception;Ljava/lang/Object;)Z

    .line 106
    .end local v1    # "e":Ljava/lang/OutOfMemoryError;
    :goto_0
    return-void

    .line 80
    .restart local v4    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v5    # "res":Lcom/koushikdutta/ion/loader/ResourceLoader$Resource;
    :cond_0
    :try_start_1
    new-instance v6, Landroid/graphics/Point;

    iget v7, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v8, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-direct {v6, v7, v8}, Landroid/graphics/Point;-><init>(II)V

    .line 82
    .local v6, "size":Landroid/graphics/Point;
    iget-boolean v7, p0, Lcom/koushikdutta/ion/loader/ResourceLoader$1;->val$animateGif:Z

    if-eqz v7, :cond_1

    const-string v7, "image/gif"

    iget-object v8, v4, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    invoke-static {v7, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 83
    iget-object v7, v5, Lcom/koushikdutta/ion/loader/ResourceLoader$Resource;->res:Landroid/content/res/Resources;

    iget v8, v5, Lcom/koushikdutta/ion/loader/ResourceLoader$Resource;->id:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 85
    .local v2, "in":Ljava/io/InputStream;
    :try_start_2
    iget-object v7, p0, Lcom/koushikdutta/ion/loader/ResourceLoader$1;->this$0:Lcom/koushikdutta/ion/loader/ResourceLoader;

    iget-object v8, p0, Lcom/koushikdutta/ion/loader/ResourceLoader$1;->val$key:Ljava/lang/String;

    invoke-virtual {v7, v8, v6, v2, v4}, Lcom/koushikdutta/ion/loader/ResourceLoader;->loadGif(Ljava/lang/String;Landroid/graphics/Point;Ljava/io/InputStream;Landroid/graphics/BitmapFactory$Options;)Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    .line 88
    .local v3, "info":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    const/4 v7, 0x1

    :try_start_3
    new-array v7, v7, [Ljava/io/Closeable;

    const/4 v8, 0x0

    aput-object v2, v7, v8

    invoke-static {v7}, Lcom/koushikdutta/async/util/StreamUtility;->closeQuietly([Ljava/io/Closeable;)V

    .line 97
    .end local v2    # "in":Ljava/io/InputStream;
    :goto_1
    const/4 v7, 0x1

    iput v7, v3, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->loadedFrom:I

    .line 98
    iget-object v7, p0, Lcom/koushikdutta/ion/loader/ResourceLoader$1;->val$ret:Lcom/koushikdutta/async/future/SimpleFuture;

    invoke-virtual {v7, v3}, Lcom/koushikdutta/async/future/SimpleFuture;->setComplete(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 103
    .end local v3    # "info":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    .end local v4    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v5    # "res":Lcom/koushikdutta/ion/loader/ResourceLoader$Resource;
    .end local v6    # "size":Landroid/graphics/Point;
    :catch_1
    move-exception v1

    .line 104
    .local v1, "e":Ljava/lang/Exception;
    iget-object v7, p0, Lcom/koushikdutta/ion/loader/ResourceLoader$1;->val$ret:Lcom/koushikdutta/async/future/SimpleFuture;

    invoke-virtual {v7, v1}, Lcom/koushikdutta/async/future/SimpleFuture;->setComplete(Ljava/lang/Exception;)Z

    goto :goto_0

    .line 88
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v2    # "in":Ljava/io/InputStream;
    .restart local v4    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v5    # "res":Lcom/koushikdutta/ion/loader/ResourceLoader$Resource;
    .restart local v6    # "size":Landroid/graphics/Point;
    :catchall_0
    move-exception v7

    const/4 v8, 0x1

    :try_start_4
    new-array v8, v8, [Ljava/io/Closeable;

    const/4 v9, 0x0

    aput-object v2, v8, v9

    invoke-static {v8}, Lcom/koushikdutta/async/util/StreamUtility;->closeQuietly([Ljava/io/Closeable;)V

    throw v7

    .line 92
    .end local v2    # "in":Ljava/io/InputStream;
    :cond_1
    iget-object v7, v5, Lcom/koushikdutta/ion/loader/ResourceLoader$Resource;->res:Landroid/content/res/Resources;

    iget v8, v5, Lcom/koushikdutta/ion/loader/ResourceLoader$Resource;->id:I

    invoke-static {v7, v8, v4}, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->loadBitmap(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 93
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_2

    .line 94
    new-instance v7, Ljava/lang/Exception;

    const-string v8, "Bitmap failed to load"

    invoke-direct {v7, v8}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v7

    .line 95
    :cond_2
    new-instance v3, Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v7, p0, Lcom/koushikdutta/ion/loader/ResourceLoader$1;->val$key:Ljava/lang/String;

    iget-object v8, v4, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    const/4 v9, 0x1

    new-array v9, v9, [Landroid/graphics/Bitmap;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    invoke-direct {v3, v7, v8, v9, v6}, Lcom/koushikdutta/ion/bitmap/BitmapInfo;-><init>(Ljava/lang/String;Ljava/lang/String;[Landroid/graphics/Bitmap;Landroid/graphics/Point;)V
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .restart local v3    # "info":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    goto :goto_1
.end method
