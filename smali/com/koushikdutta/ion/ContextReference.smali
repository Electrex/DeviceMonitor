.class abstract Lcom/koushikdutta/ion/ContextReference;
.super Ljava/lang/ref/WeakReference;
.source "ContextReference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/koushikdutta/ion/ContextReference$SupportFragmentContextReference;,
        Lcom/koushikdutta/ion/ContextReference$ActivityContextReference;,
        Lcom/koushikdutta/ion/ContextReference$NormalContextReference;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/ref/WeakReference",
        "<TT;>;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 20
    .local p0, "this":Lcom/koushikdutta/ion/ContextReference;, "Lcom/koushikdutta/ion/ContextReference<TT;>;"
    .local p1, "t":Ljava/lang/Object;, "TT;"
    invoke-direct {p0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 21
    return-void
.end method


# virtual methods
.method public abstract getContext()Landroid/content/Context;
.end method

.method public abstract isAlive()Ljava/lang/String;
.end method
