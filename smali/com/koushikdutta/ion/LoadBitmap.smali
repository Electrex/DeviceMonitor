.class Lcom/koushikdutta/ion/LoadBitmap;
.super Lcom/koushikdutta/ion/LoadBitmapEmitter;
.source "LoadBitmap.java"

# interfaces
.implements Lcom/koushikdutta/async/future/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/koushikdutta/ion/LoadBitmapEmitter;",
        "Lcom/koushikdutta/async/future/FutureCallback",
        "<",
        "Lcom/koushikdutta/async/ByteBufferList;",
        ">;"
    }
.end annotation


# instance fields
.field resizeHeight:I

.field resizeWidth:I


# direct methods
.method public constructor <init>(Lcom/koushikdutta/ion/Ion;Ljava/lang/String;ZIIZLcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;)V
    .locals 6
    .param p1, "ion"    # Lcom/koushikdutta/ion/Ion;
    .param p2, "urlKey"    # Ljava/lang/String;
    .param p3, "put"    # Z
    .param p4, "resizeWidth"    # I
    .param p5, "resizeHeight"    # I
    .param p6, "animateGif"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/koushikdutta/ion/Ion;",
            "Ljava/lang/String;",
            "ZIIZ",
            "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform",
            "<",
            "Lcom/koushikdutta/async/ByteBufferList;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p7, "emitterTransform":Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;, "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform<Lcom/koushikdutta/async/ByteBufferList;>;"
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/koushikdutta/ion/LoadBitmapEmitter;-><init>(Lcom/koushikdutta/ion/Ion;Ljava/lang/String;ZZLcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;)V

    .line 28
    iput p4, p0, Lcom/koushikdutta/ion/LoadBitmap;->resizeWidth:I

    .line 29
    iput p5, p0, Lcom/koushikdutta/ion/LoadBitmap;->resizeHeight:I

    .line 30
    iput-boolean p6, p0, Lcom/koushikdutta/ion/LoadBitmap;->animateGif:Z

    .line 31
    iput-object p7, p0, Lcom/koushikdutta/ion/LoadBitmap;->emitterTransform:Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;

    .line 32
    return-void
.end method


# virtual methods
.method public onCompleted(Ljava/lang/Exception;Lcom/koushikdutta/async/ByteBufferList;)V
    .locals 2
    .param p1, "e"    # Ljava/lang/Exception;
    .param p2, "result"    # Lcom/koushikdutta/async/ByteBufferList;

    .prologue
    .line 36
    if-eqz p1, :cond_0

    .line 37
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/koushikdutta/ion/LoadBitmap;->report(Ljava/lang/Exception;Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V

    .line 109
    :goto_0
    return-void

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/koushikdutta/ion/LoadBitmap;->ion:Lcom/koushikdutta/ion/Ion;

    iget-object v0, v0, Lcom/koushikdutta/ion/Ion;->bitmapsPending:Lcom/koushikdutta/async/util/HashList;

    iget-object v1, p0, Lcom/koushikdutta/ion/LoadBitmap;->key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/koushikdutta/async/util/HashList;->tag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p0, :cond_1

    .line 42
    invoke-virtual {p2}, Lcom/koushikdutta/async/ByteBufferList;->recycle()V

    goto :goto_0

    .line 46
    :cond_1
    invoke-static {}, Lcom/koushikdutta/ion/Ion;->getBitmapLoadExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/koushikdutta/ion/LoadBitmap$1;

    invoke-direct {v1, p0, p2}, Lcom/koushikdutta/ion/LoadBitmap$1;-><init>(Lcom/koushikdutta/ion/LoadBitmap;Lcom/koushikdutta/async/ByteBufferList;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public bridge synthetic onCompleted(Ljava/lang/Exception;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Exception;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 22
    check-cast p2, Lcom/koushikdutta/async/ByteBufferList;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/koushikdutta/ion/LoadBitmap;->onCompleted(Ljava/lang/Exception;Lcom/koushikdutta/async/ByteBufferList;)V

    return-void
.end method
