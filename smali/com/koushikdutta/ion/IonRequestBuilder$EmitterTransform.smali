.class Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;
.super Lcom/koushikdutta/async/future/TransformFuture;
.source "IonRequestBuilder.java"

# interfaces
.implements Lcom/koushikdutta/ion/future/ResponseFuture;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/koushikdutta/ion/IonRequestBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EmitterTransform"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/koushikdutta/async/future/TransformFuture",
        "<TT;",
        "Lcom/koushikdutta/ion/Loader$LoaderEmitter;",
        ">;",
        "Lcom/koushikdutta/ion/future/ResponseFuture",
        "<TT;>;"
    }
.end annotation


# instance fields
.field cancelCallback:Ljava/lang/Runnable;

.field emitter:Lcom/koushikdutta/async/DataEmitter;

.field finalRequest:Lcom/koushikdutta/async/http/AsyncHttpRequest;

.field headers:Lcom/koushikdutta/async/http/libcore/RawHeaders;

.field initialRequest:Lcom/koushikdutta/async/http/AsyncHttpRequest;

.field loadedFrom:I

.field final synthetic this$0:Lcom/koushikdutta/ion/IonRequestBuilder;


# direct methods
.method public constructor <init>(Lcom/koushikdutta/ion/IonRequestBuilder;Ljava/lang/Runnable;)V
    .locals 5
    .param p2, "cancelCallback"    # Ljava/lang/Runnable;

    .prologue
    .line 437
    .local p0, "this":Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;, "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform<TT;>;"
    iput-object p1, p0, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->this$0:Lcom/koushikdutta/ion/IonRequestBuilder;

    invoke-direct {p0}, Lcom/koushikdutta/async/future/TransformFuture;-><init>()V

    .line 438
    iput-object p2, p0, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->cancelCallback:Ljava/lang/Runnable;

    .line 439
    iget-object v3, p1, Lcom/koushikdutta/ion/IonRequestBuilder;->ion:Lcom/koushikdutta/ion/Ion;

    iget-object v4, p1, Lcom/koushikdutta/ion/IonRequestBuilder;->contextReference:Lcom/koushikdutta/ion/ContextReference;

    invoke-virtual {v4}, Lcom/koushikdutta/ion/ContextReference;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, p0, v4}, Lcom/koushikdutta/ion/Ion;->addFutureInFlight(Lcom/koushikdutta/async/future/Future;Ljava/lang/Object;)V

    .line 440
    iget-object v3, p1, Lcom/koushikdutta/ion/IonRequestBuilder;->groups:Ljava/util/ArrayList;

    if-nez v3, :cond_1

    .line 447
    :cond_0
    return-void

    .line 442
    :cond_1
    iget-object v3, p1, Lcom/koushikdutta/ion/IonRequestBuilder;->groups:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 443
    .local v2, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Ljava/lang/Object;>;"
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    .line 444
    .local v0, "group":Ljava/lang/Object;
    if-eqz v0, :cond_2

    .line 445
    iget-object v3, p1, Lcom/koushikdutta/ion/IonRequestBuilder;->ion:Lcom/koushikdutta/ion/Ion;

    invoke-virtual {v3, p0, v0}, Lcom/koushikdutta/ion/Ion;->addFutureInFlight(Lcom/koushikdutta/async/future/Future;Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method protected cancelCleanup()V
    .locals 1

    .prologue
    .line 451
    .local p0, "this":Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;, "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform<TT;>;"
    invoke-super {p0}, Lcom/koushikdutta/async/future/TransformFuture;->cancelCleanup()V

    .line 452
    iget-object v0, p0, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->emitter:Lcom/koushikdutta/async/DataEmitter;

    if-eqz v0, :cond_0

    .line 453
    iget-object v0, p0, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->emitter:Lcom/koushikdutta/async/DataEmitter;

    invoke-interface {v0}, Lcom/koushikdutta/async/DataEmitter;->close()V

    .line 454
    :cond_0
    iget-object v0, p0, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->cancelCallback:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 455
    iget-object v0, p0, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->cancelCallback:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 456
    :cond_1
    return-void
.end method

.method protected error(Ljava/lang/Exception;)V
    .locals 2
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    .line 461
    .local p0, "this":Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;, "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform<TT;>;"
    iget-object v0, p0, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->this$0:Lcom/koushikdutta/ion/IonRequestBuilder;

    const/4 v1, 0x0

    # invokes: Lcom/koushikdutta/ion/IonRequestBuilder;->postExecute(Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;Ljava/lang/Exception;Ljava/lang/Object;)V
    invoke-static {v0, p0, p1, v1}, Lcom/koushikdutta/ion/IonRequestBuilder;->access$000(Lcom/koushikdutta/ion/IonRequestBuilder;Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;Ljava/lang/Exception;Ljava/lang/Object;)V

    .line 462
    return-void
.end method

.method public loadedFrom()I
    .locals 1

    .prologue
    .line 434
    .local p0, "this":Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;, "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform<TT;>;"
    iget v0, p0, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->loadedFrom:I

    return v0
.end method

.method protected transform(Lcom/koushikdutta/ion/Loader$LoaderEmitter;)V
    .locals 6
    .param p1, "emitter"    # Lcom/koushikdutta/ion/Loader$LoaderEmitter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 466
    .local p0, "this":Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;, "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform<TT;>;"
    invoke-virtual {p1}, Lcom/koushikdutta/ion/Loader$LoaderEmitter;->getDataEmitter()Lcom/koushikdutta/async/DataEmitter;

    move-result-object v4

    iput-object v4, p0, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->emitter:Lcom/koushikdutta/async/DataEmitter;

    .line 467
    invoke-virtual {p1}, Lcom/koushikdutta/ion/Loader$LoaderEmitter;->loadedFrom()I

    move-result v4

    iput v4, p0, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->loadedFrom:I

    .line 468
    invoke-virtual {p1}, Lcom/koushikdutta/ion/Loader$LoaderEmitter;->getHeaders()Lcom/koushikdutta/async/http/libcore/RawHeaders;

    move-result-object v4

    iput-object v4, p0, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->headers:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    .line 469
    invoke-virtual {p1}, Lcom/koushikdutta/ion/Loader$LoaderEmitter;->getRequest()Lcom/koushikdutta/async/http/AsyncHttpRequest;

    move-result-object v4

    iput-object v4, p0, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->finalRequest:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    .line 471
    iget-object v4, p0, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->this$0:Lcom/koushikdutta/ion/IonRequestBuilder;

    iget-object v4, v4, Lcom/koushikdutta/ion/IonRequestBuilder;->headersCallback:Lcom/koushikdutta/ion/HeadersCallback;

    if-eqz v4, :cond_0

    .line 472
    invoke-virtual {p1}, Lcom/koushikdutta/ion/Loader$LoaderEmitter;->getHeaders()Lcom/koushikdutta/async/http/libcore/RawHeaders;

    move-result-object v0

    .line 474
    .local v0, "headers":Lcom/koushikdutta/async/http/libcore/RawHeaders;
    iget-object v4, p0, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->this$0:Lcom/koushikdutta/ion/IonRequestBuilder;

    iget-object v4, v4, Lcom/koushikdutta/ion/IonRequestBuilder;->handler:Landroid/os/Handler;

    new-instance v5, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform$2;

    invoke-direct {v5, p0, v0}, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform$2;-><init>(Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;Lcom/koushikdutta/async/http/libcore/RawHeaders;)V

    invoke-static {v4, v5}, Lcom/koushikdutta/async/AsyncServer;->post(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 483
    .end local v0    # "headers":Lcom/koushikdutta/async/http/libcore/RawHeaders;
    :cond_0
    invoke-virtual {p1}, Lcom/koushikdutta/ion/Loader$LoaderEmitter;->length()J

    move-result-wide v2

    .line 485
    .local v2, "total":J
    instance-of v4, p1, Lcom/koushikdutta/async/DataTrackingEmitter;

    if-nez v4, :cond_1

    .line 486
    new-instance v1, Lcom/koushikdutta/async/FilteredDataEmitter;

    invoke-direct {v1}, Lcom/koushikdutta/async/FilteredDataEmitter;-><init>()V

    .line 487
    .local v1, "tracker":Lcom/koushikdutta/async/DataTrackingEmitter;
    iget-object v4, p0, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->emitter:Lcom/koushikdutta/async/DataEmitter;

    invoke-interface {v1, v4}, Lcom/koushikdutta/async/DataTrackingEmitter;->setDataEmitter(Lcom/koushikdutta/async/DataEmitter;)V

    .line 492
    :goto_0
    iput-object v1, p0, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->emitter:Lcom/koushikdutta/async/DataEmitter;

    .line 493
    new-instance v4, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform$3;

    invoke-direct {v4, p0, v2, v3}, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform$3;-><init>(Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;J)V

    invoke-interface {v1, v4}, Lcom/koushikdutta/async/DataTrackingEmitter;->setDataTracker(Lcom/koushikdutta/async/DataTrackingEmitter$DataTracker;)V

    .line 542
    return-void

    .line 490
    .end local v1    # "tracker":Lcom/koushikdutta/async/DataTrackingEmitter;
    :cond_1
    iget-object v1, p0, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->emitter:Lcom/koushikdutta/async/DataEmitter;

    check-cast v1, Lcom/koushikdutta/async/DataTrackingEmitter;

    .restart local v1    # "tracker":Lcom/koushikdutta/async/DataTrackingEmitter;
    goto :goto_0
.end method

.method protected bridge synthetic transform(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 399
    .local p0, "this":Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;, "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform<TT;>;"
    check-cast p1, Lcom/koushikdutta/ion/Loader$LoaderEmitter;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->transform(Lcom/koushikdutta/ion/Loader$LoaderEmitter;)V

    return-void
.end method
