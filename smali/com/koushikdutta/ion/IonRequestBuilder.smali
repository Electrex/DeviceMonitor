.class Lcom/koushikdutta/ion/IonRequestBuilder;
.super Ljava/lang/Object;
.source "IonRequestBuilder.java"

# interfaces
.implements Lcom/koushikdutta/ion/builder/Builders$Any$B;
.implements Lcom/koushikdutta/ion/builder/Builders$Any$F;
.implements Lcom/koushikdutta/ion/builder/Builders$Any$M;
.implements Lcom/koushikdutta/ion/builder/Builders$Any$U;
.implements Lcom/koushikdutta/ion/builder/LoadBuilder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;,
        Lcom/koushikdutta/ion/IonRequestBuilder$LoadRequestCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/koushikdutta/ion/builder/Builders$Any$B;",
        "Lcom/koushikdutta/ion/builder/Builders$Any$F;",
        "Lcom/koushikdutta/ion/builder/Builders$Any$M;",
        "Lcom/koushikdutta/ion/builder/Builders$Any$U;",
        "Lcom/koushikdutta/ion/builder/LoadBuilder",
        "<",
        "Lcom/koushikdutta/ion/builder/Builders$Any$B;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field body:Lcom/koushikdutta/async/http/body/AsyncHttpRequestBody;

.field contextReference:Lcom/koushikdutta/ion/ContextReference;

.field followRedirect:Z

.field groups:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field handler:Landroid/os/Handler;

.field headers:Lcom/koushikdutta/async/http/libcore/RawHeaders;

.field headersCallback:Lcom/koushikdutta/ion/HeadersCallback;

.field ion:Lcom/koushikdutta/ion/Ion;

.field loadRequestCallback:Lcom/koushikdutta/ion/IonRequestBuilder$LoadRequestCallback;

.field logLevel:I

.field logTag:Ljava/lang/String;

.field method:Ljava/lang/String;

.field noCache:Z

.field progress:Lcom/koushikdutta/ion/ProgressCallback;

.field progressBar:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/ProgressBar;",
            ">;"
        }
    .end annotation
.end field

.field progressDialog:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/ProgressDialog;",
            ">;"
        }
    .end annotation
.end field

.field progressHandler:Lcom/koushikdutta/ion/ProgressCallback;

.field proxyHost:Ljava/lang/String;

.field proxyPort:I

.field query:Lcom/koushikdutta/async/http/Multimap;

.field timeoutMilliseconds:I

.field uploadProgress:Lcom/koushikdutta/ion/ProgressCallback;

.field uploadProgressBar:Landroid/widget/ProgressBar;

.field uploadProgressDialog:Landroid/app/ProgressDialog;

.field uploadProgressHandler:Lcom/koushikdutta/ion/ProgressCallback;

.field uri:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    const-class v0, Lcom/koushikdutta/ion/IonRequestBuilder;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/koushikdutta/ion/IonRequestBuilder;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/koushikdutta/ion/ContextReference;Lcom/koushikdutta/ion/Ion;)V
    .locals 4
    .param p1, "contextReference"    # Lcom/koushikdutta/ion/ContextReference;
    .param p2, "ion"    # Lcom/koushikdutta/ion/Ion;

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    sget-object v1, Lcom/koushikdutta/ion/Ion;->mainHandler:Landroid/os/Handler;

    iput-object v1, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->handler:Landroid/os/Handler;

    .line 84
    const-string v1, "GET"

    iput-object v1, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->method:Ljava/lang/String;

    .line 182
    const/16 v1, 0x7530

    iput v1, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->timeoutMilliseconds:I

    .line 218
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->followRedirect:Z

    .line 88
    invoke-virtual {p1}, Lcom/koushikdutta/ion/ContextReference;->isAlive()Ljava/lang/String;

    move-result-object v0

    .line 89
    .local v0, "alive":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 90
    const-string v1, "Ion"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Building request with dead context: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :cond_0
    iput-object p2, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->ion:Lcom/koushikdutta/ion/Ion;

    .line 92
    iput-object p1, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->contextReference:Lcom/koushikdutta/ion/ContextReference;

    .line 93
    return-void
.end method

.method static synthetic access$000(Lcom/koushikdutta/ion/IonRequestBuilder;Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;Ljava/lang/Exception;Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/koushikdutta/ion/IonRequestBuilder;
    .param p1, "x1"    # Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;
    .param p2, "x2"    # Ljava/lang/Exception;
    .param p3, "x3"    # Ljava/lang/Object;

    .prologue
    .line 80
    invoke-direct {p0, p1, p2, p3}, Lcom/koushikdutta/ion/IonRequestBuilder;->postExecute(Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;Ljava/lang/Exception;Ljava/lang/Object;)V

    return-void
.end method

.method private getLoaderEmitter(Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 297
    .local p1, "ret":Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;, "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform<TT;>;"
    invoke-direct {p0}, Lcom/koushikdutta/ion/IonRequestBuilder;->prepareURI()Landroid/net/Uri;

    move-result-object v1

    .line 298
    .local v1, "uri":Landroid/net/Uri;
    if-nez v1, :cond_0

    .line 299
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "Invalid URI"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v3}, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->setComplete(Ljava/lang/Exception;)Z

    .line 338
    :goto_0
    return-void

    .line 303
    :cond_0
    iget-object v2, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->body:Lcom/koushikdutta/async/http/body/AsyncHttpRequestBody;

    .line 304
    .local v2, "wrappedBody":Lcom/koushikdutta/async/http/body/AsyncHttpRequestBody;
    iget-object v3, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->uploadProgressHandler:Lcom/koushikdutta/ion/ProgressCallback;

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->uploadProgressBar:Landroid/widget/ProgressBar;

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->uploadProgress:Lcom/koushikdutta/ion/ProgressCallback;

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->uploadProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v3, :cond_2

    .line 305
    :cond_1
    new-instance v2, Lcom/koushikdutta/ion/RequestBodyUploadObserver;

    .end local v2    # "wrappedBody":Lcom/koushikdutta/async/http/body/AsyncHttpRequestBody;
    iget-object v3, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->body:Lcom/koushikdutta/async/http/body/AsyncHttpRequestBody;

    new-instance v4, Lcom/koushikdutta/ion/IonRequestBuilder$2;

    invoke-direct {v4, p0, p1}, Lcom/koushikdutta/ion/IonRequestBuilder$2;-><init>(Lcom/koushikdutta/ion/IonRequestBuilder;Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;)V

    invoke-direct {v2, v3, v4}, Lcom/koushikdutta/ion/RequestBodyUploadObserver;-><init>(Lcom/koushikdutta/async/http/body/AsyncHttpRequestBody;Lcom/koushikdutta/ion/ProgressCallback;)V

    .line 335
    .restart local v2    # "wrappedBody":Lcom/koushikdutta/async/http/body/AsyncHttpRequestBody;
    :cond_2
    invoke-direct {p0, v1, v2}, Lcom/koushikdutta/ion/IonRequestBuilder;->prepareRequest(Landroid/net/Uri;Lcom/koushikdutta/async/http/body/AsyncHttpRequestBody;)Lcom/koushikdutta/async/http/AsyncHttpRequest;

    move-result-object v0

    .line 336
    .local v0, "request":Lcom/koushikdutta/async/http/AsyncHttpRequest;
    iput-object v0, p1, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->initialRequest:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    .line 337
    invoke-virtual {p0, v0, p1}, Lcom/koushikdutta/ion/IonRequestBuilder;->resolveAndLoadRequest(Lcom/koushikdutta/async/http/AsyncHttpRequest;Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;)V

    goto :goto_0
.end method

.method private loadInternal(Ljava/lang/String;Ljava/lang/String;)Lcom/koushikdutta/ion/IonRequestBuilder;
    .locals 1
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->method:Ljava/lang/String;

    .line 102
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object p2

    .line 104
    :cond_0
    iput-object p2, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->uri:Ljava/lang/String;

    .line 105
    return-object p0
.end method

.method private postExecute(Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;Ljava/lang/Exception;Ljava/lang/Object;)V
    .locals 2
    .param p2, "ex"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform",
            "<TT;>;",
            "Ljava/lang/Exception;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 226
    .local p1, "future":Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;, "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform<TT;>;"
    .local p3, "value":Ljava/lang/Object;, "TT;"
    new-instance v0, Lcom/koushikdutta/ion/IonRequestBuilder$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/koushikdutta/ion/IonRequestBuilder$1;-><init>(Lcom/koushikdutta/ion/IonRequestBuilder;Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;Ljava/lang/Exception;Ljava/lang/Object;)V

    .line 246
    .local v0, "runner":Ljava/lang/Runnable;
    iget-object v1, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->handler:Landroid/os/Handler;

    if-nez v1, :cond_0

    .line 247
    iget-object v1, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->ion:Lcom/koushikdutta/ion/Ion;

    iget-object v1, v1, Lcom/koushikdutta/ion/Ion;->httpClient:Lcom/koushikdutta/async/http/AsyncHttpClient;

    invoke-virtual {v1}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getServer()Lcom/koushikdutta/async/AsyncServer;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/koushikdutta/async/AsyncServer;->post(Ljava/lang/Runnable;)Ljava/lang/Object;

    .line 250
    :goto_0
    return-void

    .line 249
    :cond_0
    iget-object v1, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->handler:Landroid/os/Handler;

    invoke-static {v1, v0}, Lcom/koushikdutta/async/AsyncServer;->post(Landroid/os/Handler;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private prepareRequest(Landroid/net/Uri;Lcom/koushikdutta/async/http/body/AsyncHttpRequestBody;)Lcom/koushikdutta/async/http/AsyncHttpRequest;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "wrappedBody"    # Lcom/koushikdutta/async/http/body/AsyncHttpRequestBody;

    .prologue
    .line 278
    iget-object v1, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->ion:Lcom/koushikdutta/ion/Ion;

    invoke-virtual {v1}, Lcom/koushikdutta/ion/Ion;->configure()Lcom/koushikdutta/ion/Ion$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/koushikdutta/ion/Ion$Config;->getAsyncHttpRequestFactory()Lcom/koushikdutta/ion/loader/AsyncHttpRequestFactory;

    move-result-object v1

    iget-object v2, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->method:Ljava/lang/String;

    iget-object v3, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->headers:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    invoke-interface {v1, p1, v2, v3}, Lcom/koushikdutta/ion/loader/AsyncHttpRequestFactory;->createAsyncHttpRequest(Landroid/net/Uri;Ljava/lang/String;Lcom/koushikdutta/async/http/libcore/RawHeaders;)Lcom/koushikdutta/async/http/AsyncHttpRequest;

    move-result-object v0

    .line 279
    .local v0, "request":Lcom/koushikdutta/async/http/AsyncHttpRequest;
    iget-boolean v1, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->followRedirect:Z

    invoke-virtual {v0, v1}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->setFollowRedirect(Z)Lcom/koushikdutta/async/http/AsyncHttpRequest;

    .line 280
    invoke-virtual {v0, p2}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->setBody(Lcom/koushikdutta/async/http/body/AsyncHttpRequestBody;)V

    .line 281
    iget-object v1, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->ion:Lcom/koushikdutta/ion/Ion;

    iget-object v1, v1, Lcom/koushikdutta/ion/Ion;->logtag:Ljava/lang/String;

    iget-object v2, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->ion:Lcom/koushikdutta/ion/Ion;

    iget v2, v2, Lcom/koushikdutta/ion/Ion;->logLevel:I

    invoke-virtual {v0, v1, v2}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->setLogging(Ljava/lang/String;I)V

    .line 282
    iget-object v1, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->logTag:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 283
    iget-object v1, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->logTag:Ljava/lang/String;

    iget v2, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->logLevel:I

    invoke-virtual {v0, v1, v2}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->setLogging(Ljava/lang/String;I)V

    .line 284
    :cond_0
    iget-object v1, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->proxyHost:Ljava/lang/String;

    iget v2, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->proxyPort:I

    invoke-virtual {v0, v1, v2}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->enableProxy(Ljava/lang/String;I)V

    .line 285
    iget v1, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->timeoutMilliseconds:I

    invoke-virtual {v0, v1}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->setTimeout(I)Lcom/koushikdutta/async/http/AsyncHttpRequest;

    .line 286
    const-string v1, "preparing request"

    invoke-virtual {v0, v1}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->logd(Ljava/lang/String;)V

    .line 287
    return-object v0
.end method

.method private prepareURI()Landroid/net/Uri;
    .locals 8

    .prologue
    .line 255
    :try_start_0
    iget-object v7, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->query:Lcom/koushikdutta/async/http/Multimap;

    if-eqz v7, :cond_4

    .line 256
    iget-object v7, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->uri:Ljava/lang/String;

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 257
    .local v0, "builder":Landroid/net/Uri$Builder;
    iget-object v7, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->query:Lcom/koushikdutta/async/http/Multimap;

    invoke-virtual {v7}, Lcom/koushikdutta/async/http/Multimap;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 258
    .local v4, "key":Ljava/lang/String;
    iget-object v7, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->query:Lcom/koushikdutta/async/http/Multimap;

    invoke-virtual {v7, v4}, Lcom/koushikdutta/async/http/Multimap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 259
    .local v6, "value":Ljava/lang/String;
    invoke-virtual {v0, v4, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    goto :goto_0

    .line 262
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "key":Ljava/lang/String;
    .end local v6    # "value":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 271
    .end local v0    # "builder":Landroid/net/Uri$Builder;
    .local v5, "uri":Landroid/net/Uri;
    :goto_1
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_3

    .line 272
    :cond_2
    const/4 v5, 0x0

    .line 274
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_3
    return-object v5

    .line 265
    :cond_4
    :try_start_1
    iget-object v7, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->uri:Ljava/lang/String;

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v5

    .restart local v5    # "uri":Landroid/net/Uri;
    goto :goto_1

    .line 268
    .end local v5    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v1

    .line 269
    .local v1, "e":Ljava/lang/Exception;
    const/4 v5, 0x0

    .restart local v5    # "uri":Landroid/net/Uri;
    goto :goto_1
.end method


# virtual methods
.method execute(Lcom/koushikdutta/async/DataSink;ZLjava/lang/Object;Ljava/lang/Runnable;)Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;
    .locals 6
    .param p1, "sink"    # Lcom/koushikdutta/async/DataSink;
    .param p2, "close"    # Z
    .param p4, "cancel"    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/koushikdutta/async/DataSink;",
            "ZTT;",
            "Ljava/lang/Runnable;",
            ")",
            "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 580
    .local p3, "result":Ljava/lang/Object;, "TT;"
    new-instance v0, Lcom/koushikdutta/ion/IonRequestBuilder$5;

    move-object v1, p0

    move-object v2, p4

    move v3, p2

    move-object v4, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/koushikdutta/ion/IonRequestBuilder$5;-><init>(Lcom/koushikdutta/ion/IonRequestBuilder;Ljava/lang/Runnable;ZLcom/koushikdutta/async/DataSink;Ljava/lang/Object;)V

    .line 600
    .local v0, "ret":Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;, "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform<TT;>;"
    invoke-direct {p0, v0}, Lcom/koushikdutta/ion/IonRequestBuilder;->getLoaderEmitter(Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;)V

    .line 601
    return-object v0
.end method

.method execute(Lcom/koushikdutta/async/parser/AsyncParser;Ljava/lang/Runnable;)Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;
    .locals 2
    .param p2, "cancel"    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/koushikdutta/async/parser/AsyncParser",
            "<TT;>;",
            "Ljava/lang/Runnable;",
            ")",
            "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 609
    .local p1, "parser":Lcom/koushikdutta/async/parser/AsyncParser;, "Lcom/koushikdutta/async/parser/AsyncParser<TT;>;"
    sget-boolean v1, Lcom/koushikdutta/ion/IonRequestBuilder;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 610
    :cond_0
    new-instance v0, Lcom/koushikdutta/ion/IonRequestBuilder$6;

    invoke-direct {v0, p0, p2, p1}, Lcom/koushikdutta/ion/IonRequestBuilder$6;-><init>(Lcom/koushikdutta/ion/IonRequestBuilder;Ljava/lang/Runnable;Lcom/koushikdutta/async/parser/AsyncParser;)V

    .line 623
    .local v0, "ret":Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;, "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform<TT;>;"
    invoke-direct {p0, v0}, Lcom/koushikdutta/ion/IonRequestBuilder;->getLoaderEmitter(Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;)V

    .line 624
    return-object v0
.end method

.method invokeLoadRequest(Lcom/koushikdutta/async/http/AsyncHttpRequest;Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;)V
    .locals 1
    .param p1, "request"    # Lcom/koushikdutta/async/http/AsyncHttpRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/koushikdutta/async/http/AsyncHttpRequest;",
            "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 369
    .local p2, "ret":Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;, "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform<TT;>;"
    iget-object v0, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->loadRequestCallback:Lcom/koushikdutta/ion/IonRequestBuilder$LoadRequestCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->loadRequestCallback:Lcom/koushikdutta/ion/IonRequestBuilder$LoadRequestCallback;

    invoke-interface {v0, p1}, Lcom/koushikdutta/ion/IonRequestBuilder$LoadRequestCallback;->loadRequest(Lcom/koushikdutta/async/http/AsyncHttpRequest;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 370
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/koushikdutta/ion/IonRequestBuilder;->loadRequest(Lcom/koushikdutta/async/http/AsyncHttpRequest;Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;)V

    .line 371
    :cond_1
    return-void
.end method

.method public load(Ljava/lang/String;)Lcom/koushikdutta/ion/IonRequestBuilder;
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 97
    const-string v0, "GET"

    invoke-direct {p0, v0, p1}, Lcom/koushikdutta/ion/IonRequestBuilder;->loadInternal(Ljava/lang/String;Ljava/lang/String;)Lcom/koushikdutta/ion/IonRequestBuilder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic load(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 80
    invoke-virtual {p0, p1}, Lcom/koushikdutta/ion/IonRequestBuilder;->load(Ljava/lang/String;)Lcom/koushikdutta/ion/IonRequestBuilder;

    move-result-object v0

    return-object v0
.end method

.method loadRequest(Lcom/koushikdutta/async/http/AsyncHttpRequest;Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;)V
    .locals 5
    .param p1, "request"    # Lcom/koushikdutta/async/http/AsyncHttpRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/koushikdutta/async/http/AsyncHttpRequest;",
            "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 375
    .local p2, "ret":Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;, "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform<TT;>;"
    iget-object v3, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->ion:Lcom/koushikdutta/ion/Ion;

    iget-object v3, v3, Lcom/koushikdutta/ion/Ion;->loaders:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/koushikdutta/ion/Loader;

    .line 376
    .local v2, "loader":Lcom/koushikdutta/ion/Loader;
    iget-object v3, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->ion:Lcom/koushikdutta/ion/Ion;

    invoke-interface {v2, v3, p1, p2}, Lcom/koushikdutta/ion/Loader;->load(Lcom/koushikdutta/ion/Ion;Lcom/koushikdutta/async/http/AsyncHttpRequest;Lcom/koushikdutta/async/future/FutureCallback;)Lcom/koushikdutta/async/future/Future;

    move-result-object v0

    .line 377
    .local v0, "emitter":Lcom/koushikdutta/async/future/Future;, "Lcom/koushikdutta/async/future/Future<Lcom/koushikdutta/async/DataEmitter;>;"
    if-eqz v0, :cond_0

    .line 378
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Using loader: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->logi(Ljava/lang/String;)V

    .line 379
    invoke-virtual {p2, v0}, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->setParent(Lcom/koushikdutta/async/future/Cancellable;)Lcom/koushikdutta/async/future/SimpleFuture;

    .line 384
    .end local v0    # "emitter":Lcom/koushikdutta/async/future/Future;, "Lcom/koushikdutta/async/future/Future<Lcom/koushikdutta/async/DataEmitter;>;"
    .end local v2    # "loader":Lcom/koushikdutta/ion/Loader;
    :goto_0
    return-void

    .line 383
    :cond_1
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "Unknown uri scheme"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v3}, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->setComplete(Ljava/lang/Exception;)Z

    goto :goto_0
.end method

.method public progress(Lcom/koushikdutta/ion/ProgressCallback;)Lcom/koushikdutta/ion/IonRequestBuilder;
    .locals 0
    .param p1, "callback"    # Lcom/koushikdutta/ion/ProgressCallback;

    .prologue
    .line 563
    iput-object p1, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->progress:Lcom/koushikdutta/ion/ProgressCallback;

    .line 564
    return-object p0
.end method

.method public bridge synthetic progress(Lcom/koushikdutta/ion/ProgressCallback;)Lcom/koushikdutta/ion/builder/RequestBuilder;
    .locals 1
    .param p1, "x0"    # Lcom/koushikdutta/ion/ProgressCallback;

    .prologue
    .line 80
    invoke-virtual {p0, p1}, Lcom/koushikdutta/ion/IonRequestBuilder;->progress(Lcom/koushikdutta/ion/ProgressCallback;)Lcom/koushikdutta/ion/IonRequestBuilder;

    move-result-object v0

    return-object v0
.end method

.method resolveAndLoadRequest(Lcom/koushikdutta/async/http/AsyncHttpRequest;Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;)V
    .locals 3
    .param p1, "request"    # Lcom/koushikdutta/async/http/AsyncHttpRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/koushikdutta/async/http/AsyncHttpRequest;",
            "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 341
    .local p2, "ret":Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;, "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform<TT;>;"
    invoke-virtual {p0, p1, p2}, Lcom/koushikdutta/ion/IonRequestBuilder;->resolveRequest(Lcom/koushikdutta/async/http/AsyncHttpRequest;Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;)Lcom/koushikdutta/async/future/Future;

    move-result-object v0

    .line 342
    .local v0, "resolved":Lcom/koushikdutta/async/future/Future;, "Lcom/koushikdutta/async/future/Future<Lcom/koushikdutta/async/http/AsyncHttpRequest;>;"
    if-eqz v0, :cond_0

    .line 343
    new-instance v1, Lcom/koushikdutta/ion/IonRequestBuilder$3;

    invoke-direct {v1, p0, p2}, Lcom/koushikdutta/ion/IonRequestBuilder$3;-><init>(Lcom/koushikdutta/ion/IonRequestBuilder;Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;)V

    invoke-interface {v0, v1}, Lcom/koushikdutta/async/future/Future;->setCallback(Lcom/koushikdutta/async/future/FutureCallback;)Lcom/koushikdutta/async/future/Future;

    .line 366
    :goto_0
    return-void

    .line 356
    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    if-eq v1, v2, :cond_1

    .line 357
    sget-object v1, Lcom/koushikdutta/ion/Ion;->mainHandler:Landroid/os/Handler;

    new-instance v2, Lcom/koushikdutta/ion/IonRequestBuilder$4;

    invoke-direct {v2, p0, p1, p2}, Lcom/koushikdutta/ion/IonRequestBuilder$4;-><init>(Lcom/koushikdutta/ion/IonRequestBuilder;Lcom/koushikdutta/async/http/AsyncHttpRequest;Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;)V

    invoke-static {v1, v2}, Lcom/koushikdutta/async/AsyncServer;->post(Landroid/os/Handler;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 365
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/koushikdutta/ion/IonRequestBuilder;->invokeLoadRequest(Lcom/koushikdutta/async/http/AsyncHttpRequest;Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;)V

    goto :goto_0
.end method

.method resolveRequest(Lcom/koushikdutta/async/http/AsyncHttpRequest;Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;)Lcom/koushikdutta/async/future/Future;
    .locals 5
    .param p1, "request"    # Lcom/koushikdutta/async/http/AsyncHttpRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/koushikdutta/async/http/AsyncHttpRequest;",
            "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform",
            "<TT;>;)",
            "Lcom/koushikdutta/async/future/Future",
            "<",
            "Lcom/koushikdutta/async/http/AsyncHttpRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 388
    .local p2, "ret":Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;, "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform<TT;>;"
    iget-object v3, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->ion:Lcom/koushikdutta/ion/Ion;

    iget-object v3, v3, Lcom/koushikdutta/ion/Ion;->loaders:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/koushikdutta/ion/Loader;

    .line 389
    .local v1, "loader":Lcom/koushikdutta/ion/Loader;
    iget-object v3, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->contextReference:Lcom/koushikdutta/ion/ContextReference;

    invoke-virtual {v3}, Lcom/koushikdutta/ion/ContextReference;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->ion:Lcom/koushikdutta/ion/Ion;

    invoke-interface {v1, v3, v4, p1}, Lcom/koushikdutta/ion/Loader;->resolve(Landroid/content/Context;Lcom/koushikdutta/ion/Ion;Lcom/koushikdutta/async/http/AsyncHttpRequest;)Lcom/koushikdutta/async/future/Future;

    move-result-object v2

    .line 390
    .local v2, "resolved":Lcom/koushikdutta/async/future/Future;, "Lcom/koushikdutta/async/future/Future<Lcom/koushikdutta/async/http/AsyncHttpRequest;>;"
    if-eqz v2, :cond_0

    .line 394
    .end local v1    # "loader":Lcom/koushikdutta/ion/Loader;
    .end local v2    # "resolved":Lcom/koushikdutta/async/future/Future;, "Lcom/koushikdutta/async/future/Future<Lcom/koushikdutta/async/http/AsyncHttpRequest;>;"
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setHandler(Landroid/os/Handler;)Lcom/koushikdutta/ion/IonRequestBuilder;
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->handler:Landroid/os/Handler;

    .line 192
    return-object p0
.end method

.method public write(Ljava/io/File;)Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;
    .locals 3
    .param p1, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 700
    new-instance v0, Lcom/koushikdutta/async/stream/FileDataSink;

    iget-object v1, p0, Lcom/koushikdutta/ion/IonRequestBuilder;->ion:Lcom/koushikdutta/ion/Ion;

    invoke-virtual {v1}, Lcom/koushikdutta/ion/Ion;->getServer()Lcom/koushikdutta/async/AsyncServer;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/koushikdutta/async/stream/FileDataSink;-><init>(Lcom/koushikdutta/async/AsyncServer;Ljava/io/File;)V

    const/4 v1, 0x1

    new-instance v2, Lcom/koushikdutta/ion/IonRequestBuilder$8;

    invoke-direct {v2, p0, p1}, Lcom/koushikdutta/ion/IonRequestBuilder$8;-><init>(Lcom/koushikdutta/ion/IonRequestBuilder;Ljava/io/File;)V

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/koushikdutta/ion/IonRequestBuilder;->execute(Lcom/koushikdutta/async/DataSink;ZLjava/lang/Object;Ljava/lang/Runnable;)Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic write(Ljava/io/File;)Lcom/koushikdutta/ion/future/ResponseFuture;
    .locals 1
    .param p1, "x0"    # Ljava/io/File;

    .prologue
    .line 80
    invoke-virtual {p0, p1}, Lcom/koushikdutta/ion/IonRequestBuilder;->write(Ljava/io/File;)Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;

    move-result-object v0

    return-object v0
.end method
