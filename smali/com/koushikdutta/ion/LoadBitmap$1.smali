.class Lcom/koushikdutta/ion/LoadBitmap$1;
.super Ljava/lang/Object;
.source "LoadBitmap.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/koushikdutta/ion/LoadBitmap;->onCompleted(Ljava/lang/Exception;Lcom/koushikdutta/async/ByteBufferList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/koushikdutta/ion/LoadBitmap;

.field final synthetic val$result:Lcom/koushikdutta/async/ByteBufferList;


# direct methods
.method constructor <init>(Lcom/koushikdutta/ion/LoadBitmap;Lcom/koushikdutta/async/ByteBufferList;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/koushikdutta/ion/LoadBitmap$1;->this$0:Lcom/koushikdutta/ion/LoadBitmap;

    iput-object p2, p0, Lcom/koushikdutta/ion/LoadBitmap$1;->val$result:Lcom/koushikdutta/async/ByteBufferList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 17

    .prologue
    .line 49
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/koushikdutta/ion/LoadBitmap$1;->this$0:Lcom/koushikdutta/ion/LoadBitmap;

    iget-object v1, v1, Lcom/koushikdutta/ion/LoadBitmap;->ion:Lcom/koushikdutta/ion/Ion;

    iget-object v1, v1, Lcom/koushikdutta/ion/Ion;->bitmapsPending:Lcom/koushikdutta/async/util/HashList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/koushikdutta/ion/LoadBitmap$1;->this$0:Lcom/koushikdutta/ion/LoadBitmap;

    iget-object v2, v2, Lcom/koushikdutta/ion/LoadBitmap;->key:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/koushikdutta/async/util/HashList;->tag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/koushikdutta/ion/LoadBitmap$1;->this$0:Lcom/koushikdutta/ion/LoadBitmap;

    if-eq v1, v2, :cond_0

    .line 50
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/koushikdutta/ion/LoadBitmap$1;->val$result:Lcom/koushikdutta/async/ByteBufferList;

    invoke-virtual {v1}, Lcom/koushikdutta/async/ByteBufferList;->recycle()V

    .line 107
    :goto_0
    return-void

    .line 54
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/koushikdutta/ion/LoadBitmap$1;->val$result:Lcom/koushikdutta/async/ByteBufferList;

    invoke-virtual {v1}, Lcom/koushikdutta/async/ByteBufferList;->getAll()Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 58
    .local v7, "bb":Ljava/nio/ByteBuffer;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/koushikdutta/ion/LoadBitmap$1;->this$0:Lcom/koushikdutta/ion/LoadBitmap;

    iget-object v1, v1, Lcom/koushikdutta/ion/LoadBitmap;->ion:Lcom/koushikdutta/ion/Ion;

    iget-object v1, v1, Lcom/koushikdutta/ion/Ion;->bitmapCache:Lcom/koushikdutta/ion/bitmap/IonBitmapCache;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v3

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/koushikdutta/ion/LoadBitmap$1;->this$0:Lcom/koushikdutta/ion/LoadBitmap;

    iget v5, v5, Lcom/koushikdutta/ion/LoadBitmap;->resizeWidth:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/koushikdutta/ion/LoadBitmap$1;->this$0:Lcom/koushikdutta/ion/LoadBitmap;

    iget v6, v6, Lcom/koushikdutta/ion/LoadBitmap;->resizeHeight:I

    invoke-virtual/range {v1 .. v6}, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->prepareBitmapOptions([BIIII)Landroid/graphics/BitmapFactory$Options;

    move-result-object v15

    .line 59
    .local v15, "options":Landroid/graphics/BitmapFactory$Options;
    if-nez v15, :cond_1

    .line 60
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "BitmapFactory.Options failed to load"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    .end local v15    # "options":Landroid/graphics/BitmapFactory$Options;
    :catch_0
    move-exception v12

    .line 99
    .local v12, "e":Ljava/lang/OutOfMemoryError;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/koushikdutta/ion/LoadBitmap$1;->this$0:Lcom/koushikdutta/ion/LoadBitmap;

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2, v12}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/koushikdutta/ion/LoadBitmap;->report(Ljava/lang/Exception;Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    invoke-static {v7}, Lcom/koushikdutta/async/ByteBufferList;->reclaim(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    .line 61
    .end local v12    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v15    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_1
    :try_start_2
    new-instance v16, Landroid/graphics/Point;

    iget v1, v15, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v2, v15, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    move-object/from16 v0, v16

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 62
    .local v16, "size":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/koushikdutta/ion/LoadBitmap$1;->this$0:Lcom/koushikdutta/ion/LoadBitmap;

    iget-boolean v1, v1, Lcom/koushikdutta/ion/LoadBitmap;->animateGif:Z

    if-eqz v1, :cond_4

    const-string v1, "image/gif"

    iget-object v2, v15, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 63
    new-instance v10, Lcom/koushikdutta/ion/gif/GifDecoder;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v2

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    new-instance v4, Lcom/koushikdutta/ion/LoadBitmap$1$1;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/koushikdutta/ion/LoadBitmap$1$1;-><init>(Lcom/koushikdutta/ion/LoadBitmap$1;)V

    invoke-direct {v10, v1, v2, v3, v4}, Lcom/koushikdutta/ion/gif/GifDecoder;-><init>([BIILcom/koushikdutta/ion/gif/GifAction;)V

    .line 69
    .local v10, "decoder":Lcom/koushikdutta/ion/gif/GifDecoder;
    invoke-virtual {v10}, Lcom/koushikdutta/ion/gif/GifDecoder;->run()V

    .line 70
    invoke-virtual {v10}, Lcom/koushikdutta/ion/gif/GifDecoder;->getFrameCount()I

    move-result v1

    if-nez v1, :cond_2

    .line 71
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "failed to load gif"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 101
    .end local v10    # "decoder":Lcom/koushikdutta/ion/gif/GifDecoder;
    .end local v15    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v16    # "size":Landroid/graphics/Point;
    :catch_1
    move-exception v12

    .line 102
    .local v12, "e":Ljava/lang/Exception;
    :try_start_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/koushikdutta/ion/LoadBitmap$1;->this$0:Lcom/koushikdutta/ion/LoadBitmap;

    const/4 v2, 0x0

    invoke-virtual {v1, v12, v2}, Lcom/koushikdutta/ion/LoadBitmap;->report(Ljava/lang/Exception;Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 105
    invoke-static {v7}, Lcom/koushikdutta/async/ByteBufferList;->reclaim(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    .line 72
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v10    # "decoder":Lcom/koushikdutta/ion/gif/GifDecoder;
    .restart local v15    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v16    # "size":Landroid/graphics/Point;
    :cond_2
    :try_start_4
    invoke-virtual {v10}, Lcom/koushikdutta/ion/gif/GifDecoder;->getFrameCount()I

    move-result v1

    new-array v9, v1, [Landroid/graphics/Bitmap;

    .line 73
    .local v9, "bitmaps":[Landroid/graphics/Bitmap;
    invoke-virtual {v10}, Lcom/koushikdutta/ion/gif/GifDecoder;->getDelays()[I

    move-result-object v11

    .line 74
    .local v11, "delays":[I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    invoke-virtual {v10}, Lcom/koushikdutta/ion/gif/GifDecoder;->getFrameCount()I

    move-result v1

    if-ge v13, v1, :cond_6

    .line 75
    invoke-virtual {v10, v13}, Lcom/koushikdutta/ion/gif/GifDecoder;->getFrameImage(I)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 76
    .local v8, "bitmap":Landroid/graphics/Bitmap;
    if-nez v8, :cond_3

    .line 77
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "failed to load gif frame"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 105
    .end local v8    # "bitmap":Landroid/graphics/Bitmap;
    .end local v9    # "bitmaps":[Landroid/graphics/Bitmap;
    .end local v10    # "decoder":Lcom/koushikdutta/ion/gif/GifDecoder;
    .end local v11    # "delays":[I
    .end local v13    # "i":I
    .end local v15    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v16    # "size":Landroid/graphics/Point;
    :catchall_0
    move-exception v1

    invoke-static {v7}, Lcom/koushikdutta/async/ByteBufferList;->reclaim(Ljava/nio/ByteBuffer;)V

    throw v1

    .line 78
    .restart local v8    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v9    # "bitmaps":[Landroid/graphics/Bitmap;
    .restart local v10    # "decoder":Lcom/koushikdutta/ion/gif/GifDecoder;
    .restart local v11    # "delays":[I
    .restart local v13    # "i":I
    .restart local v15    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v16    # "size":Landroid/graphics/Point;
    :cond_3
    :try_start_5
    aput-object v8, v9, v13

    .line 74
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 82
    .end local v8    # "bitmap":Landroid/graphics/Bitmap;
    .end local v9    # "bitmaps":[Landroid/graphics/Bitmap;
    .end local v10    # "decoder":Lcom/koushikdutta/ion/gif/GifDecoder;
    .end local v11    # "delays":[I
    .end local v13    # "i":I
    :cond_4
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v2

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    invoke-static {v1, v2, v3, v15}, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->loadBitmap([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 83
    .restart local v8    # "bitmap":Landroid/graphics/Bitmap;
    if-nez v8, :cond_5

    .line 84
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "failed to load bitmap"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 85
    :cond_5
    const/4 v1, 0x1

    new-array v9, v1, [Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    aput-object v8, v9, v1

    .line 86
    .restart local v9    # "bitmaps":[Landroid/graphics/Bitmap;
    const/4 v11, 0x0

    .line 89
    .end local v8    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v11    # "delays":[I
    :cond_6
    new-instance v14, Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/koushikdutta/ion/LoadBitmap$1;->this$0:Lcom/koushikdutta/ion/LoadBitmap;

    iget-object v1, v1, Lcom/koushikdutta/ion/LoadBitmap;->key:Ljava/lang/String;

    iget-object v2, v15, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v14, v1, v2, v9, v0}, Lcom/koushikdutta/ion/bitmap/BitmapInfo;-><init>(Ljava/lang/String;Ljava/lang/String;[Landroid/graphics/Bitmap;Landroid/graphics/Point;)V

    .line 90
    .local v14, "info":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    iput-object v11, v14, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->delays:[I

    .line 91
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/koushikdutta/ion/LoadBitmap$1;->this$0:Lcom/koushikdutta/ion/LoadBitmap;

    iget-object v1, v1, Lcom/koushikdutta/ion/LoadBitmap;->emitterTransform:Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;

    if-eqz v1, :cond_7

    .line 92
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/koushikdutta/ion/LoadBitmap$1;->this$0:Lcom/koushikdutta/ion/LoadBitmap;

    iget-object v1, v1, Lcom/koushikdutta/ion/LoadBitmap;->emitterTransform:Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;

    invoke-virtual {v1}, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->loadedFrom()I

    move-result v1

    iput v1, v14, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->loadedFrom:I

    .line 96
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/koushikdutta/ion/LoadBitmap$1;->this$0:Lcom/koushikdutta/ion/LoadBitmap;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v14}, Lcom/koushikdutta/ion/LoadBitmap;->report(Ljava/lang/Exception;Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V
    :try_end_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 105
    invoke-static {v7}, Lcom/koushikdutta/async/ByteBufferList;->reclaim(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    .line 94
    :cond_7
    const/4 v1, 0x1

    :try_start_6
    iput v1, v14, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->loadedFrom:I
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2
.end method
