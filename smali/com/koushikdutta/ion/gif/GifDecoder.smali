.class public Lcom/koushikdutta/ion/gif/GifDecoder;
.super Ljava/lang/Thread;
.source "GifDecoder.java"


# instance fields
.field private act:[I

.field private action:Lcom/koushikdutta/ion/gif/GifAction;

.field private bgColor:I

.field private bgIndex:I

.field private block:[B

.field private blockSize:I

.field private currentFrame:Lcom/koushikdutta/ion/gif/GifFrame;

.field private delay:I

.field dest:[I

.field private dispose:I

.field private frameCount:I

.field private gct:[I

.field private gctFlag:Z

.field private gctSize:I

.field private gifData:[B

.field private gifDataLength:I

.field private gifDataOffset:I

.field private gifFrame:Lcom/koushikdutta/ion/gif/GifFrame;

.field public height:I

.field private ih:I

.field private in:Ljava/io/InputStream;

.field private interlace:Z

.field private isShow:Z

.field private iw:I

.field private ix:I

.field private iy:I

.field private lastBgColor:I

.field private lastDispose:I

.field lastPixels:[I

.field private lct:[I

.field private lctFlag:Z

.field private lctSize:I

.field private loopCount:I

.field private lrh:I

.field private lrw:I

.field private lrx:I

.field private lry:I

.field private pixelAspect:I

.field private pixelStack:[B

.field private pixels:[B

.field private prefix:[S

.field private status:I

.field private suffix:[B

.field private transIndex:I

.field private transparency:Z

.field public width:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lcom/koushikdutta/ion/gif/GifAction;)V
    .locals 3
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "act"    # Lcom/koushikdutta/ion/gif/GifAction;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 29
    const/4 v0, 0x1

    iput v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->loopCount:I

    .line 46
    iput-object v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->currentFrame:Lcom/koushikdutta/ion/gif/GifFrame;

    .line 48
    iput-boolean v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->isShow:Z

    .line 51
    const/16 v0, 0x100

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->block:[B

    .line 52
    iput v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->blockSize:I

    .line 53
    iput v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->dispose:I

    .line 54
    iput v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->lastDispose:I

    .line 55
    iput-boolean v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->transparency:Z

    .line 56
    iput v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->delay:I

    .line 71
    iput-object v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->action:Lcom/koushikdutta/ion/gif/GifAction;

    .line 74
    iput-object v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gifData:[B

    .line 91
    iput-object p1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->in:Ljava/io/InputStream;

    .line 92
    iput-object p2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->action:Lcom/koushikdutta/ion/gif/GifAction;

    .line 93
    return-void
.end method

.method public constructor <init>([BIILcom/koushikdutta/ion/gif/GifAction;)V
    .locals 3
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .param p4, "act"    # Lcom/koushikdutta/ion/gif/GifAction;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 83
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 29
    const/4 v0, 0x1

    iput v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->loopCount:I

    .line 46
    iput-object v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->currentFrame:Lcom/koushikdutta/ion/gif/GifFrame;

    .line 48
    iput-boolean v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->isShow:Z

    .line 51
    const/16 v0, 0x100

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->block:[B

    .line 52
    iput v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->blockSize:I

    .line 53
    iput v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->dispose:I

    .line 54
    iput v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->lastDispose:I

    .line 55
    iput-boolean v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->transparency:Z

    .line 56
    iput v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->delay:I

    .line 71
    iput-object v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->action:Lcom/koushikdutta/ion/gif/GifAction;

    .line 74
    iput-object v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gifData:[B

    .line 84
    iput-object p1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gifData:[B

    .line 85
    iput-object p4, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->action:Lcom/koushikdutta/ion/gif/GifAction;

    .line 86
    iput p2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gifDataOffset:I

    .line 87
    iput p3, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gifDataLength:I

    .line 88
    return-void
.end method

.method private decodeImageData()V
    .locals 25

    .prologue
    .line 336
    const/4 v2, -0x1

    .line 337
    .local v2, "NullCode":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->iw:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->ih:I

    move/from16 v24, v0

    mul-int v17, v23, v24

    .line 340
    .local v17, "npix":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->pixels:[B

    move-object/from16 v23, v0

    if-eqz v23, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->pixels:[B

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    move/from16 v1, v17

    if-ge v0, v1, :cond_1

    .line 341
    :cond_0
    move/from16 v0, v17

    new-array v0, v0, [B

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/koushikdutta/ion/gif/GifDecoder;->pixels:[B

    .line 343
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->prefix:[S

    move-object/from16 v23, v0

    if-nez v23, :cond_2

    .line 344
    const/16 v23, 0x1000

    move/from16 v0, v23

    new-array v0, v0, [S

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/koushikdutta/ion/gif/GifDecoder;->prefix:[S

    .line 346
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->suffix:[B

    move-object/from16 v23, v0

    if-nez v23, :cond_3

    .line 347
    const/16 v23, 0x1000

    move/from16 v0, v23

    new-array v0, v0, [B

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/koushikdutta/ion/gif/GifDecoder;->suffix:[B

    .line 349
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->pixelStack:[B

    move-object/from16 v23, v0

    if-nez v23, :cond_4

    .line 350
    const/16 v23, 0x1001

    move/from16 v0, v23

    new-array v0, v0, [B

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/koushikdutta/ion/gif/GifDecoder;->pixelStack:[B

    .line 353
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->read()I

    move-result v11

    .line 354
    .local v11, "data_size":I
    const/16 v23, 0x1

    shl-int v6, v23, v11

    .line 355
    .local v6, "clear":I
    add-int/lit8 v13, v6, 0x1

    .line 356
    .local v13, "end_of_information":I
    add-int/lit8 v3, v6, 0x2

    .line 357
    .local v3, "available":I
    move/from16 v18, v2

    .line 358
    .local v18, "old_code":I
    add-int/lit8 v9, v11, 0x1

    .line 359
    .local v9, "code_size":I
    const/16 v23, 0x1

    shl-int v23, v23, v9

    add-int/lit8 v8, v23, -0x1

    .line 360
    .local v8, "code_mask":I
    const/4 v7, 0x0

    .local v7, "code":I
    :goto_0
    if-ge v7, v6, :cond_5

    .line 361
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->prefix:[S

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-short v24, v23, v7

    .line 362
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->suffix:[B

    move-object/from16 v23, v0

    int-to-byte v0, v7

    move/from16 v24, v0

    aput-byte v24, v23, v7

    .line 360
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 366
    :cond_5
    const/4 v4, 0x0

    .local v4, "bi":I
    move/from16 v19, v4

    .local v19, "pi":I
    move/from16 v21, v4

    .local v21, "top":I
    move v14, v4

    .local v14, "first":I
    move v10, v4

    .local v10, "count":I
    move v5, v4

    .local v5, "bits":I
    move v12, v4

    .line 367
    .local v12, "datum":I
    const/4 v15, 0x0

    .local v15, "i":I
    move/from16 v20, v19

    .end local v19    # "pi":I
    .local v20, "pi":I
    move/from16 v22, v21

    .end local v21    # "top":I
    .local v22, "top":I
    :goto_1
    move/from16 v0, v17

    if-ge v15, v0, :cond_11

    .line 368
    if-nez v22, :cond_12

    .line 369
    if-ge v5, v9, :cond_8

    .line 371
    if-nez v10, :cond_7

    .line 373
    invoke-direct/range {p0 .. p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->readBlock()I

    move-result v10

    .line 374
    if-gtz v10, :cond_6

    move/from16 v21, v22

    .line 439
    .end local v22    # "top":I
    .restart local v21    # "top":I
    :goto_2
    move/from16 v15, v20

    :goto_3
    move/from16 v0, v17

    if-ge v15, v0, :cond_10

    .line 440
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->pixels:[B

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-byte v24, v23, v15

    .line 439
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 377
    .end local v21    # "top":I
    .restart local v22    # "top":I
    :cond_6
    const/4 v4, 0x0

    .line 379
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->block:[B

    move-object/from16 v23, v0

    aget-byte v23, v23, v4

    move/from16 v0, v23

    and-int/lit16 v0, v0, 0xff

    move/from16 v23, v0

    shl-int v23, v23, v5

    add-int v12, v12, v23

    .line 380
    add-int/lit8 v5, v5, 0x8

    .line 381
    add-int/lit8 v4, v4, 0x1

    .line 382
    add-int/lit8 v10, v10, -0x1

    .line 383
    goto :goto_1

    .line 386
    :cond_8
    and-int v7, v12, v8

    .line 387
    shr-int/2addr v12, v9

    .line 388
    sub-int/2addr v5, v9

    .line 391
    if-gt v7, v3, :cond_11

    if-ne v7, v13, :cond_9

    move/from16 v21, v22

    .line 392
    .end local v22    # "top":I
    .restart local v21    # "top":I
    goto :goto_2

    .line 394
    .end local v21    # "top":I
    .restart local v22    # "top":I
    :cond_9
    if-ne v7, v6, :cond_a

    .line 396
    add-int/lit8 v9, v11, 0x1

    .line 397
    const/16 v23, 0x1

    shl-int v23, v23, v9

    add-int/lit8 v8, v23, -0x1

    .line 398
    add-int/lit8 v3, v6, 0x2

    .line 399
    move/from16 v18, v2

    .line 400
    goto :goto_1

    .line 402
    :cond_a
    move/from16 v0, v18

    if-ne v0, v2, :cond_b

    .line 403
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->pixelStack:[B

    move-object/from16 v23, v0

    add-int/lit8 v21, v22, 0x1

    .end local v22    # "top":I
    .restart local v21    # "top":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->suffix:[B

    move-object/from16 v24, v0

    aget-byte v24, v24, v7

    aput-byte v24, v23, v22

    .line 404
    move/from16 v18, v7

    .line 405
    move v14, v7

    move/from16 v22, v21

    .line 406
    .end local v21    # "top":I
    .restart local v22    # "top":I
    goto :goto_1

    .line 408
    :cond_b
    move/from16 v16, v7

    .line 409
    .local v16, "in_code":I
    if-ne v7, v3, :cond_c

    .line 410
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->pixelStack:[B

    move-object/from16 v23, v0

    add-int/lit8 v21, v22, 0x1

    .end local v22    # "top":I
    .restart local v21    # "top":I
    int-to-byte v0, v14

    move/from16 v24, v0

    aput-byte v24, v23, v22

    .line 411
    move/from16 v7, v18

    move/from16 v22, v21

    .line 413
    .end local v21    # "top":I
    .restart local v22    # "top":I
    :cond_c
    :goto_4
    if-le v7, v6, :cond_d

    .line 414
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->pixelStack:[B

    move-object/from16 v23, v0

    add-int/lit8 v21, v22, 0x1

    .end local v22    # "top":I
    .restart local v21    # "top":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->suffix:[B

    move-object/from16 v24, v0

    aget-byte v24, v24, v7

    aput-byte v24, v23, v22

    .line 415
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->prefix:[S

    move-object/from16 v23, v0

    aget-short v7, v23, v7

    move/from16 v22, v21

    .end local v21    # "top":I
    .restart local v22    # "top":I
    goto :goto_4

    .line 417
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->suffix:[B

    move-object/from16 v23, v0

    aget-byte v23, v23, v7

    move/from16 v0, v23

    and-int/lit16 v14, v0, 0xff

    .line 419
    const/16 v23, 0x1000

    move/from16 v0, v23

    if-lt v3, v0, :cond_e

    move/from16 v21, v22

    .line 420
    .end local v22    # "top":I
    .restart local v21    # "top":I
    goto/16 :goto_2

    .line 422
    .end local v21    # "top":I
    .restart local v22    # "top":I
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->pixelStack:[B

    move-object/from16 v23, v0

    add-int/lit8 v21, v22, 0x1

    .end local v22    # "top":I
    .restart local v21    # "top":I
    int-to-byte v0, v14

    move/from16 v24, v0

    aput-byte v24, v23, v22

    .line 423
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->prefix:[S

    move-object/from16 v23, v0

    move/from16 v0, v18

    int-to-short v0, v0

    move/from16 v24, v0

    aput-short v24, v23, v3

    .line 424
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->suffix:[B

    move-object/from16 v23, v0

    int-to-byte v0, v14

    move/from16 v24, v0

    aput-byte v24, v23, v3

    .line 425
    add-int/lit8 v3, v3, 0x1

    .line 426
    and-int v23, v3, v8

    if-nez v23, :cond_f

    const/16 v23, 0x1000

    move/from16 v0, v23

    if-ge v3, v0, :cond_f

    .line 428
    add-int/lit8 v9, v9, 0x1

    .line 429
    add-int/2addr v8, v3

    .line 431
    :cond_f
    move/from16 v18, v16

    .line 435
    .end local v16    # "in_code":I
    :goto_5
    add-int/lit8 v21, v21, -0x1

    .line 436
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->pixels:[B

    move-object/from16 v23, v0

    add-int/lit8 v19, v20, 0x1

    .end local v20    # "pi":I
    .restart local v19    # "pi":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->pixelStack:[B

    move-object/from16 v24, v0

    aget-byte v24, v24, v21

    aput-byte v24, v23, v20

    .line 437
    add-int/lit8 v15, v15, 0x1

    move/from16 v20, v19

    .end local v19    # "pi":I
    .restart local v20    # "pi":I
    move/from16 v22, v21

    .end local v21    # "top":I
    .restart local v22    # "top":I
    goto/16 :goto_1

    .line 442
    .end local v22    # "top":I
    .restart local v21    # "top":I
    :cond_10
    return-void

    .end local v21    # "top":I
    .restart local v22    # "top":I
    :cond_11
    move/from16 v21, v22

    .end local v22    # "top":I
    .restart local v21    # "top":I
    goto/16 :goto_2

    .end local v21    # "top":I
    .restart local v22    # "top":I
    :cond_12
    move/from16 v21, v22

    .end local v22    # "top":I
    .restart local v21    # "top":I
    goto :goto_5
.end method

.method private err()Z
    .locals 1

    .prologue
    .line 445
    iget v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->status:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private init()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 449
    iput v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->status:I

    .line 450
    iput v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->frameCount:I

    .line 451
    iput-object v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gifFrame:Lcom/koushikdutta/ion/gif/GifFrame;

    .line 452
    iput-object v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gct:[I

    .line 453
    iput-object v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->lct:[I

    .line 454
    return-void
.end method

.method private read()I
    .locals 3

    .prologue
    .line 457
    const/4 v0, 0x0

    .line 460
    .local v0, "curByte":I
    :try_start_0
    iget-object v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->in:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 464
    :goto_0
    return v0

    .line 461
    :catch_0
    move-exception v1

    .line 462
    .local v1, "e":Ljava/lang/Exception;
    const/4 v2, 0x1

    iput v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->status:I

    goto :goto_0
.end method

.method private readBlock()I
    .locals 6

    .prologue
    .line 468
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->read()I

    move-result v3

    iput v3, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->blockSize:I

    .line 469
    const/4 v2, 0x0

    .line 470
    .local v2, "n":I
    iget v3, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->blockSize:I

    if-lez v3, :cond_1

    .line 472
    const/4 v0, 0x0

    .line 473
    .local v0, "count":I
    :goto_0
    :try_start_0
    iget v3, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->blockSize:I

    if-ge v2, v3, :cond_0

    .line 474
    iget-object v3, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->in:Ljava/io/InputStream;

    iget-object v4, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->block:[B

    iget v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->blockSize:I

    sub-int/2addr v5, v2

    invoke-virtual {v3, v4, v2, v5}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 475
    const/4 v3, -0x1

    if-ne v0, v3, :cond_2

    .line 483
    :cond_0
    :goto_1
    iget v3, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->blockSize:I

    if-ge v2, v3, :cond_1

    .line 484
    const/4 v3, 0x1

    iput v3, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->status:I

    .line 487
    .end local v0    # "count":I
    :cond_1
    return v2

    .line 478
    .restart local v0    # "count":I
    :cond_2
    add-int/2addr v2, v0

    goto :goto_0

    .line 480
    :catch_0
    move-exception v1

    .line 481
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private readByte()I
    .locals 4

    .prologue
    .line 303
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gifData:[B

    iget v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gifDataOffset:I

    iget v3, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gifDataLength:I

    invoke-direct {v0, v1, v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    iput-object v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->in:Ljava/io/InputStream;

    .line 304
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gifData:[B

    .line 305
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->readStream()I

    move-result v0

    return v0
.end method

.method private readColorTable(I)[I
    .locals 14
    .param p1, "ncolors"    # I

    .prologue
    .line 491
    mul-int/lit8 v9, p1, 0x3

    .line 492
    .local v9, "nbytes":I
    const/4 v11, 0x0

    .line 493
    .local v11, "tab":[I
    new-array v1, v9, [B

    .line 494
    .local v1, "c":[B
    const/4 v8, 0x0

    .line 496
    .local v8, "n":I
    :try_start_0
    iget-object v12, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->in:Ljava/io/InputStream;

    invoke-virtual {v12, v1}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    .line 500
    :goto_0
    if-ge v8, v9, :cond_1

    .line 501
    const/4 v12, 0x1

    iput v12, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->status:I

    .line 513
    :cond_0
    return-object v11

    .line 497
    :catch_0
    move-exception v2

    .line 498
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 503
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    const/16 v12, 0x100

    new-array v11, v12, [I

    .line 504
    const/4 v4, 0x0

    .line 505
    .local v4, "i":I
    const/4 v6, 0x0

    .local v6, "j":I
    move v7, v6

    .end local v6    # "j":I
    .local v7, "j":I
    move v5, v4

    .line 506
    .end local v4    # "i":I
    .local v5, "i":I
    :goto_1
    if-ge v5, p1, :cond_0

    .line 507
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "j":I
    .restart local v6    # "j":I
    aget-byte v12, v1, v7

    and-int/lit16 v10, v12, 0xff

    .line 508
    .local v10, "r":I
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "j":I
    .restart local v7    # "j":I
    aget-byte v12, v1, v6

    and-int/lit16 v3, v12, 0xff

    .line 509
    .local v3, "g":I
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "j":I
    .restart local v6    # "j":I
    aget-byte v12, v1, v7

    and-int/lit16 v0, v12, 0xff

    .line 510
    .local v0, "b":I
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    const/high16 v12, -0x1000000

    shl-int/lit8 v13, v10, 0x10

    or-int/2addr v12, v13

    shl-int/lit8 v13, v3, 0x8

    or-int/2addr v12, v13

    or-int/2addr v12, v0

    aput v12, v11, v5

    move v7, v6

    .end local v6    # "j":I
    .restart local v7    # "j":I
    move v5, v4

    .line 511
    .end local v4    # "i":I
    .restart local v5    # "i":I
    goto :goto_1
.end method

.method private readContents()V
    .locals 6

    .prologue
    .line 518
    const/4 v2, 0x0

    .line 519
    .local v2, "done":Z
    :goto_0
    :sswitch_0
    if-nez v2, :cond_2

    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->err()Z

    move-result v4

    if-nez v4, :cond_2

    .line 520
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->read()I

    move-result v1

    .line 521
    .local v1, "code":I
    sparse-switch v1, :sswitch_data_0

    .line 553
    const/4 v4, 0x1

    iput v4, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->status:I

    goto :goto_0

    .line 523
    :sswitch_1
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->readImage()V

    goto :goto_0

    .line 526
    :sswitch_2
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->read()I

    move-result v1

    .line 527
    sparse-switch v1, :sswitch_data_1

    .line 544
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->skip()V

    goto :goto_0

    .line 529
    :sswitch_3
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->readGraphicControlExt()V

    goto :goto_0

    .line 532
    :sswitch_4
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->readBlock()I

    .line 533
    const-string v0, ""

    .line 534
    .local v0, "app":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    const/16 v4, 0xb

    if-ge v3, v4, :cond_0

    .line 535
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->block:[B

    aget-byte v5, v5, v3

    int-to-char v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 534
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 537
    :cond_0
    const-string v4, "NETSCAPE2.0"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 538
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->readNetscapeExt()V

    goto :goto_0

    .line 540
    :cond_1
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->skip()V

    goto :goto_0

    .line 548
    .end local v0    # "app":Ljava/lang/String;
    .end local v3    # "i":I
    :sswitch_5
    const/4 v2, 0x1

    .line 549
    goto :goto_0

    .line 556
    .end local v1    # "code":I
    :cond_2
    return-void

    .line 521
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x21 -> :sswitch_2
        0x2c -> :sswitch_1
        0x3b -> :sswitch_5
    .end sparse-switch

    .line 527
    :sswitch_data_1
    .sparse-switch
        0xf9 -> :sswitch_3
        0xff -> :sswitch_4
    .end sparse-switch
.end method

.method private readGraphicControlExt()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 559
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->read()I

    .line 560
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->read()I

    move-result v0

    .line 561
    .local v0, "packed":I
    and-int/lit8 v2, v0, 0x1c

    shr-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->dispose:I

    .line 562
    iget v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->dispose:I

    if-nez v2, :cond_0

    .line 563
    iput v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->dispose:I

    .line 565
    :cond_0
    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_1

    :goto_0
    iput-boolean v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->transparency:Z

    .line 566
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->readShort()I

    move-result v1

    mul-int/lit8 v1, v1, 0xa

    iput v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->delay:I

    .line 567
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->read()I

    move-result v1

    iput v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->transIndex:I

    .line 568
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->read()I

    .line 569
    return-void

    .line 565
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private readHeader()V
    .locals 4

    .prologue
    .line 572
    const-string v1, ""

    .line 573
    .local v1, "id":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x6

    if-ge v0, v2, :cond_0

    .line 574
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->read()I

    move-result v3

    int-to-char v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 573
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 576
    :cond_0
    const-string v2, "GIF"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 577
    const/4 v2, 0x1

    iput v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->status:I

    .line 585
    :cond_1
    :goto_1
    return-void

    .line 580
    :cond_2
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->readLSD()V

    .line 581
    iget-boolean v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gctFlag:Z

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->err()Z

    move-result v2

    if-nez v2, :cond_1

    .line 582
    iget v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gctSize:I

    invoke-direct {p0, v2}, Lcom/koushikdutta/ion/gif/GifDecoder;->readColorTable(I)[I

    move-result-object v2

    iput-object v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gct:[I

    .line 583
    iget-object v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gct:[I

    iget v3, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->bgIndex:I

    aget v2, v2, v3

    iput v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->bgColor:I

    goto :goto_1
.end method

.method private readImage()V
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 588
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->readShort()I

    move-result v5

    iput v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->ix:I

    .line 589
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->readShort()I

    move-result v5

    iput v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->iy:I

    .line 590
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->readShort()I

    move-result v5

    iput v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->iw:I

    .line 591
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->readShort()I

    move-result v5

    iput v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->ih:I

    .line 592
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->read()I

    move-result v3

    .line 593
    .local v3, "packed":I
    and-int/lit16 v5, v3, 0x80

    if-eqz v5, :cond_4

    move v5, v6

    :goto_0
    iput-boolean v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->lctFlag:Z

    .line 594
    and-int/lit8 v5, v3, 0x40

    if-eqz v5, :cond_5

    move v5, v6

    :goto_1
    iput-boolean v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->interlace:Z

    .line 597
    const/4 v5, 0x2

    and-int/lit8 v8, v3, 0x7

    shl-int/2addr v5, v8

    iput v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->lctSize:I

    .line 598
    iget-boolean v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->lctFlag:Z

    if-eqz v5, :cond_6

    .line 599
    iget v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->lctSize:I

    invoke-direct {p0, v5}, Lcom/koushikdutta/ion/gif/GifDecoder;->readColorTable(I)[I

    move-result-object v5

    iput-object v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->lct:[I

    .line 600
    iget-object v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->lct:[I

    iput-object v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->act:[I

    .line 607
    :cond_0
    :goto_2
    const/4 v4, 0x0

    .line 608
    .local v4, "save":I
    iget-boolean v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->transparency:Z

    if-eqz v5, :cond_1

    .line 609
    iget-object v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->act:[I

    iget v8, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->transIndex:I

    aget v4, v5, v8

    .line 610
    iget-object v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->act:[I

    iget v8, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->transIndex:I

    aput v7, v5, v8

    .line 612
    :cond_1
    iget-object v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->act:[I

    if-nez v5, :cond_2

    .line 613
    iput v6, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->status:I

    .line 615
    :cond_2
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->err()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 652
    :cond_3
    :goto_3
    return-void

    .end local v4    # "save":I
    :cond_4
    move v5, v7

    .line 593
    goto :goto_0

    :cond_5
    move v5, v7

    .line 594
    goto :goto_1

    .line 602
    :cond_6
    iget-object v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gct:[I

    iput-object v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->act:[I

    .line 603
    iget v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->bgIndex:I

    iget v8, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->transIndex:I

    if-ne v5, v8, :cond_0

    .line 604
    iput v7, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->bgColor:I

    goto :goto_2

    .line 619
    .restart local v4    # "save":I
    :cond_7
    :try_start_0
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->decodeImageData()V

    .line 620
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->skip()V

    .line 621
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->err()Z

    move-result v5

    if-nez v5, :cond_3

    .line 624
    iget v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->frameCount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->frameCount:I

    .line 627
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->setPixels()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 628
    .local v2, "image":Landroid/graphics/Bitmap;
    iget-object v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gifFrame:Lcom/koushikdutta/ion/gif/GifFrame;

    if-nez v5, :cond_9

    .line 629
    new-instance v5, Lcom/koushikdutta/ion/gif/GifFrame;

    iget v6, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->delay:I

    invoke-direct {v5, v2, v6}, Lcom/koushikdutta/ion/gif/GifFrame;-><init>(Landroid/graphics/Bitmap;I)V

    iput-object v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gifFrame:Lcom/koushikdutta/ion/gif/GifFrame;

    .line 630
    iget-object v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gifFrame:Lcom/koushikdutta/ion/gif/GifFrame;

    iput-object v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->currentFrame:Lcom/koushikdutta/ion/gif/GifFrame;

    .line 640
    :goto_4
    iget-boolean v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->transparency:Z

    if-eqz v5, :cond_8

    .line 641
    iget-object v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->act:[I

    iget v6, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->transIndex:I

    aput v4, v5, v6

    .line 643
    :cond_8
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->resetFrame()V

    .line 644
    iget-object v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->action:Lcom/koushikdutta/ion/gif/GifAction;

    const/4 v6, 0x1

    iget v7, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->frameCount:I

    invoke-interface {v5, v6, v7}, Lcom/koushikdutta/ion/gif/GifAction;->parseOk(ZI)Z

    move-result v5

    if-nez v5, :cond_3

    .line 645
    const/4 v5, -0x1

    iput v5, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->status:I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 648
    .end local v2    # "image":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v0

    .line 649
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    const-string v5, "GifDecoder"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ">>> log  : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 650
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_3

    .line 632
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v2    # "image":Landroid/graphics/Bitmap;
    :cond_9
    :try_start_1
    iget-object v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gifFrame:Lcom/koushikdutta/ion/gif/GifFrame;

    .line 633
    .local v1, "f":Lcom/koushikdutta/ion/gif/GifFrame;
    :goto_5
    iget-object v5, v1, Lcom/koushikdutta/ion/gif/GifFrame;->nextFrame:Lcom/koushikdutta/ion/gif/GifFrame;

    if-eqz v5, :cond_a

    .line 634
    iget-object v1, v1, Lcom/koushikdutta/ion/gif/GifFrame;->nextFrame:Lcom/koushikdutta/ion/gif/GifFrame;

    goto :goto_5

    .line 636
    :cond_a
    new-instance v5, Lcom/koushikdutta/ion/gif/GifFrame;

    iget v6, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->delay:I

    invoke-direct {v5, v2, v6}, Lcom/koushikdutta/ion/gif/GifFrame;-><init>(Landroid/graphics/Bitmap;I)V

    iput-object v5, v1, Lcom/koushikdutta/ion/gif/GifFrame;->nextFrame:Lcom/koushikdutta/ion/gif/GifFrame;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4
.end method

.method private readLSD()V
    .locals 3

    .prologue
    .line 656
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->readShort()I

    move-result v1

    iput v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->width:I

    .line 657
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->readShort()I

    move-result v1

    iput v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->height:I

    .line 659
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->read()I

    move-result v0

    .line 660
    .local v0, "packed":I
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gctFlag:Z

    .line 663
    const/4 v1, 0x2

    and-int/lit8 v2, v0, 0x7

    shl-int/2addr v1, v2

    iput v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gctSize:I

    .line 664
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->read()I

    move-result v1

    iput v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->bgIndex:I

    .line 665
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->read()I

    move-result v1

    iput v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->pixelAspect:I

    .line 666
    return-void

    .line 660
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private readNetscapeExt()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 670
    :cond_0
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->readBlock()I

    .line 671
    iget-object v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->block:[B

    const/4 v3, 0x0

    aget-byte v2, v2, v3

    if-ne v2, v4, :cond_1

    .line 673
    iget-object v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->block:[B

    aget-byte v2, v2, v4

    and-int/lit16 v0, v2, 0xff

    .line 674
    .local v0, "b1":I
    iget-object v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->block:[B

    const/4 v3, 0x2

    aget-byte v2, v2, v3

    and-int/lit16 v1, v2, 0xff

    .line 675
    .local v1, "b2":I
    shl-int/lit8 v2, v1, 0x8

    or-int/2addr v2, v0

    iput v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->loopCount:I

    .line 677
    .end local v0    # "b1":I
    .end local v1    # "b2":I
    :cond_1
    iget v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->blockSize:I

    if-lez v2, :cond_2

    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->err()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 678
    :cond_2
    return-void
.end method

.method private readShort()I
    .locals 2

    .prologue
    .line 682
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->read()I

    move-result v0

    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->read()I

    move-result v1

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    return v0
.end method

.method private readStream()I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 309
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->init()V

    .line 310
    iget-object v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->in:Ljava/io/InputStream;

    if-eqz v1, :cond_2

    .line 311
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->readHeader()V

    .line 312
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->err()Z

    move-result v1

    if-nez v1, :cond_0

    .line 313
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->readContents()V

    .line 314
    iget v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->frameCount:I

    if-gez v1, :cond_1

    .line 315
    iput v4, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->status:I

    .line 316
    iget-object v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->action:Lcom/koushikdutta/ion/gif/GifAction;

    invoke-interface {v1, v3, v2}, Lcom/koushikdutta/ion/gif/GifAction;->parseOk(ZI)Z

    .line 323
    :cond_0
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->in:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 332
    :goto_1
    iget v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->status:I

    return v1

    .line 318
    :cond_1
    iput v2, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->status:I

    .line 319
    iget-object v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->action:Lcom/koushikdutta/ion/gif/GifAction;

    invoke-interface {v1, v4, v2}, Lcom/koushikdutta/ion/gif/GifAction;->parseOk(ZI)Z

    goto :goto_0

    .line 324
    :catch_0
    move-exception v0

    .line 325
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 329
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    const/4 v1, 0x2

    iput v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->status:I

    .line 330
    iget-object v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->action:Lcom/koushikdutta/ion/gif/GifAction;

    invoke-interface {v1, v3, v2}, Lcom/koushikdutta/ion/gif/GifAction;->parseOk(ZI)Z

    goto :goto_1
.end method

.method private resetFrame()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 686
    iget v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->dispose:I

    iput v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->lastDispose:I

    .line 687
    iget v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->ix:I

    iput v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->lrx:I

    .line 688
    iget v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->iy:I

    iput v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->lry:I

    .line 689
    iget v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->iw:I

    iput v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->lrw:I

    .line 690
    iget v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->ih:I

    iput v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->lrh:I

    .line 691
    iget-object v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->dest:[I

    iput-object v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->lastPixels:[I

    .line 692
    iget v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->bgColor:I

    iput v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->lastBgColor:I

    .line 693
    iput v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->dispose:I

    .line 694
    iput-boolean v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->transparency:Z

    .line 695
    iput v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->delay:I

    .line 696
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->lct:[I

    .line 697
    return-void
.end method

.method private setPixels()Landroid/graphics/Bitmap;
    .locals 25

    .prologue
    .line 168
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->dest:[I

    if-nez v3, :cond_0

    .line 169
    move-object/from16 v0, p0

    iget v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->width:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->height:I

    mul-int/2addr v3, v4

    new-array v3, v3, [I

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->dest:[I

    .line 171
    :cond_0
    move-object/from16 v0, p0

    iget v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->lastDispose:I

    if-lez v3, :cond_6

    .line 172
    move-object/from16 v0, p0

    iget v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->lastDispose:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_2

    .line 174
    move-object/from16 v0, p0

    iget v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->frameCount:I

    add-int/lit8 v19, v3, -0x2

    .line 175
    .local v19, "n":I
    if-lez v19, :cond_4

    .line 176
    add-int/lit8 v3, v19, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/koushikdutta/ion/gif/GifDecoder;->getFrameImage(I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 177
    .local v2, "lastImage":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->lastPixels:[I

    if-nez v3, :cond_1

    .line 178
    move-object/from16 v0, p0

    iget v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->width:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->height:I

    mul-int/2addr v3, v4

    new-array v3, v3, [I

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->lastPixels:[I

    .line 179
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->lastPixels:[I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->width:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->width:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->height:I

    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 185
    .end local v2    # "lastImage":Landroid/graphics/Bitmap;
    .end local v19    # "n":I
    :cond_2
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->lastPixels:[I

    if-eqz v3, :cond_6

    .line 186
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->lastPixels:[I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->lastPixels:[I

    array-length v4, v4

    invoke-static {v3, v4}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->dest:[I

    .line 188
    move-object/from16 v0, p0

    iget v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->lastDispose:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_6

    .line 190
    const/4 v10, 0x0

    .line 191
    .local v10, "c":I
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->transparency:Z

    if-nez v3, :cond_3

    .line 192
    move-object/from16 v0, p0

    iget v10, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->lastBgColor:I

    .line 194
    :cond_3
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->lrh:I

    if-ge v13, v3, :cond_6

    .line 195
    move-object/from16 v0, p0

    iget v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->lry:I

    add-int/2addr v3, v13

    move-object/from16 v0, p0

    iget v4, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->width:I

    mul-int/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->lrx:I

    add-int v20, v3, v4

    .line 196
    .local v20, "n1":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->lrw:I

    add-int v21, v20, v3

    .line 197
    .local v21, "n2":I
    move/from16 v17, v20

    .local v17, "k":I
    :goto_2
    move/from16 v0, v17

    move/from16 v1, v21

    if-ge v0, v1, :cond_5

    .line 198
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->dest:[I

    aput v10, v3, v17

    .line 197
    add-int/lit8 v17, v17, 0x1

    goto :goto_2

    .line 182
    .end local v10    # "c":I
    .end local v13    # "i":I
    .end local v17    # "k":I
    .end local v20    # "n1":I
    .end local v21    # "n2":I
    .restart local v19    # "n":I
    :cond_4
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->lastPixels:[I

    goto :goto_0

    .line 194
    .end local v19    # "n":I
    .restart local v10    # "c":I
    .restart local v13    # "i":I
    .restart local v17    # "k":I
    .restart local v20    # "n1":I
    .restart local v21    # "n2":I
    :cond_5
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 206
    .end local v10    # "c":I
    .end local v13    # "i":I
    .end local v17    # "k":I
    .end local v20    # "n1":I
    .end local v21    # "n2":I
    :cond_6
    const/16 v22, 0x1

    .line 207
    .local v22, "pass":I
    const/16 v15, 0x8

    .line 208
    .local v15, "inc":I
    const/4 v14, 0x0

    .line 209
    .local v14, "iline":I
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->ih:I

    if-ge v13, v3, :cond_c

    .line 210
    move/from16 v18, v13

    .line 211
    .local v18, "line":I
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->interlace:Z

    if-eqz v3, :cond_8

    .line 212
    move-object/from16 v0, p0

    iget v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->ih:I

    if-lt v14, v3, :cond_7

    .line 213
    add-int/lit8 v22, v22, 0x1

    .line 214
    packed-switch v22, :pswitch_data_0

    .line 227
    :cond_7
    :goto_4
    move/from16 v18, v14

    .line 228
    add-int/2addr v14, v15

    .line 230
    :cond_8
    move-object/from16 v0, p0

    iget v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->iy:I

    add-int v18, v18, v3

    .line 231
    move-object/from16 v0, p0

    iget v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->height:I

    move/from16 v0, v18

    if-ge v0, v3, :cond_b

    .line 232
    move-object/from16 v0, p0

    iget v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->width:I

    mul-int v17, v18, v3

    .line 233
    .restart local v17    # "k":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->ix:I

    add-int v12, v17, v3

    .line 234
    .local v12, "dx":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->iw:I

    add-int v11, v12, v3

    .line 235
    .local v11, "dlim":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->width:I

    add-int v3, v3, v17

    if-ge v3, v11, :cond_9

    .line 236
    move-object/from16 v0, p0

    iget v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->width:I

    add-int v11, v17, v3

    .line 238
    :cond_9
    move-object/from16 v0, p0

    iget v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->iw:I

    mul-int v23, v13, v3

    .local v23, "sx":I
    move/from16 v24, v23

    .line 239
    .end local v23    # "sx":I
    .local v24, "sx":I
    :goto_5
    if-ge v12, v11, :cond_b

    .line 241
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->pixels:[B

    add-int/lit8 v23, v24, 0x1

    .end local v24    # "sx":I
    .restart local v23    # "sx":I
    aget-byte v3, v3, v24

    and-int/lit16 v0, v3, 0xff

    move/from16 v16, v0

    .line 242
    .local v16, "index":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->act:[I

    aget v10, v3, v16

    .line 243
    .restart local v10    # "c":I
    if-eqz v10, :cond_a

    .line 244
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->dest:[I

    aput v10, v3, v12

    .line 246
    :cond_a
    add-int/lit8 v12, v12, 0x1

    move/from16 v24, v23

    .line 247
    .end local v23    # "sx":I
    .restart local v24    # "sx":I
    goto :goto_5

    .line 216
    .end local v10    # "c":I
    .end local v11    # "dlim":I
    .end local v12    # "dx":I
    .end local v16    # "index":I
    .end local v17    # "k":I
    .end local v24    # "sx":I
    :pswitch_0
    const/4 v14, 0x4

    .line 217
    goto :goto_4

    .line 219
    :pswitch_1
    const/4 v14, 0x2

    .line 220
    const/4 v15, 0x4

    .line 221
    goto :goto_4

    .line 223
    :pswitch_2
    const/4 v14, 0x1

    .line 224
    const/4 v15, 0x2

    goto :goto_4

    .line 209
    :cond_b
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    .line 250
    .end local v18    # "line":I
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->dest:[I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->width:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/koushikdutta/ion/gif/GifDecoder;->height:I

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    return-object v3

    .line 214
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private skip()V
    .locals 1

    .prologue
    .line 704
    :cond_0
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->readBlock()I

    .line 705
    iget v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->blockSize:I

    if-lez v0, :cond_1

    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->err()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 706
    :cond_1
    return-void
.end method


# virtual methods
.method public getDelays()[I
    .locals 4

    .prologue
    .line 142
    iget-object v1, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gifFrame:Lcom/koushikdutta/ion/gif/GifFrame;

    .line 143
    .local v1, "f":Lcom/koushikdutta/ion/gif/GifFrame;
    iget v3, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->frameCount:I

    new-array v0, v3, [I

    .line 144
    .local v0, "d":[I
    const/4 v2, 0x0

    .line 145
    .local v2, "i":I
    :goto_0
    if-eqz v1, :cond_0

    iget v3, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->frameCount:I

    if-ge v2, v3, :cond_0

    .line 146
    iget v3, v1, Lcom/koushikdutta/ion/gif/GifFrame;->delay:I

    aput v3, v0, v2

    .line 147
    iget-object v1, v1, Lcom/koushikdutta/ion/gif/GifFrame;->nextFrame:Lcom/koushikdutta/ion/gif/GifFrame;

    .line 148
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 150
    :cond_0
    return-object v0
.end method

.method public getFrame(I)Lcom/koushikdutta/ion/gif/GifFrame;
    .locals 2
    .param p1, "n"    # I

    .prologue
    .line 266
    iget-object v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gifFrame:Lcom/koushikdutta/ion/gif/GifFrame;

    .line 267
    .local v0, "frame":Lcom/koushikdutta/ion/gif/GifFrame;
    const/4 v1, 0x0

    .line 268
    .local v1, "i":I
    :goto_0
    if-eqz v0, :cond_1

    .line 269
    if-ne v1, p1, :cond_0

    .line 276
    .end local v0    # "frame":Lcom/koushikdutta/ion/gif/GifFrame;
    :goto_1
    return-object v0

    .line 272
    .restart local v0    # "frame":Lcom/koushikdutta/ion/gif/GifFrame;
    :cond_0
    iget-object v0, v0, Lcom/koushikdutta/ion/gif/GifFrame;->nextFrame:Lcom/koushikdutta/ion/gif/GifFrame;

    .line 274
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 276
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getFrameCount()I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->frameCount:I

    return v0
.end method

.method public getFrameImage(I)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "n"    # I

    .prologue
    .line 254
    invoke-virtual {p0, p1}, Lcom/koushikdutta/ion/gif/GifDecoder;->getFrame(I)Lcom/koushikdutta/ion/gif/GifFrame;

    move-result-object v0

    .line 255
    .local v0, "frame":Lcom/koushikdutta/ion/gif/GifFrame;
    if-nez v0, :cond_0

    .line 256
    const/4 v1, 0x0

    .line 258
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lcom/koushikdutta/ion/gif/GifFrame;->image:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public run()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->in:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    .line 97
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->readStream()I

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/koushikdutta/ion/gif/GifDecoder;->gifData:[B

    if-eqz v0, :cond_0

    .line 99
    invoke-direct {p0}, Lcom/koushikdutta/ion/gif/GifDecoder;->readByte()I

    goto :goto_0
.end method
