.class Lcom/koushikdutta/ion/TransformBitmap$2;
.super Ljava/lang/Object;
.source "TransformBitmap.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/koushikdutta/ion/TransformBitmap;->onCompleted(Ljava/lang/Exception;Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/koushikdutta/ion/TransformBitmap;

.field final synthetic val$result:Lcom/koushikdutta/ion/bitmap/BitmapInfo;


# direct methods
.method constructor <init>(Lcom/koushikdutta/ion/TransformBitmap;Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/koushikdutta/ion/TransformBitmap$2;->this$0:Lcom/koushikdutta/ion/TransformBitmap;

    iput-object p2, p0, Lcom/koushikdutta/ion/TransformBitmap$2;->val$result:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 18

    .prologue
    .line 105
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/koushikdutta/ion/TransformBitmap$2;->this$0:Lcom/koushikdutta/ion/TransformBitmap;

    iget-object v15, v15, Lcom/koushikdutta/ion/TransformBitmap;->ion:Lcom/koushikdutta/ion/Ion;

    iget-object v15, v15, Lcom/koushikdutta/ion/Ion;->bitmapsPending:Lcom/koushikdutta/async/util/HashList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/TransformBitmap$2;->this$0:Lcom/koushikdutta/ion/TransformBitmap;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/koushikdutta/ion/TransformBitmap;->key:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Lcom/koushikdutta/async/util/HashList;->tag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/TransformBitmap$2;->this$0:Lcom/koushikdutta/ion/TransformBitmap;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    const/4 v11, 0x0

    .line 113
    .local v11, "size":Landroid/graphics/Point;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/koushikdutta/ion/TransformBitmap$2;->val$result:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v15, v15, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    array-length v15, v15

    new-array v2, v15, [Landroid/graphics/Bitmap;

    .line 114
    .local v2, "bitmaps":[Landroid/graphics/Bitmap;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/koushikdutta/ion/TransformBitmap$2;->val$result:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v15, v15, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    array-length v15, v15

    if-ge v6, v15, :cond_4

    .line 115
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/koushikdutta/ion/TransformBitmap$2;->val$result:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v15, v15, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    aget-object v15, v15, v6

    aput-object v15, v2, v6

    .line 116
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/koushikdutta/ion/TransformBitmap$2;->this$0:Lcom/koushikdutta/ion/TransformBitmap;

    iget-object v15, v15, Lcom/koushikdutta/ion/TransformBitmap;->transforms:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    move-object v12, v11

    .end local v11    # "size":Landroid/graphics/Point;
    .local v12, "size":Landroid/graphics/Point;
    :goto_2
    :try_start_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/koushikdutta/ion/bitmap/Transform;

    .line 117
    .local v14, "transform":Lcom/koushikdutta/ion/bitmap/Transform;
    aget-object v15, v2, v6

    invoke-interface {v14, v15}, Lcom/koushikdutta/ion/bitmap/Transform;->transform(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 118
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    if-nez v1, :cond_2

    .line 119
    new-instance v15, Ljava/lang/Exception;

    const-string v16, "failed to transform bitmap"

    invoke-direct/range {v15 .. v16}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v15
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    .line 137
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v14    # "transform":Lcom/koushikdutta/ion/bitmap/Transform;
    :catch_0
    move-exception v4

    move-object v11, v12

    .line 138
    .end local v2    # "bitmaps":[Landroid/graphics/Bitmap;
    .end local v6    # "i":I
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v12    # "size":Landroid/graphics/Point;
    .local v4, "e":Ljava/lang/OutOfMemoryError;
    .restart local v11    # "size":Landroid/graphics/Point;
    :goto_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/koushikdutta/ion/TransformBitmap$2;->this$0:Lcom/koushikdutta/ion/TransformBitmap;

    new-instance v16, Ljava/lang/Exception;

    move-object/from16 v0, v16

    invoke-direct {v0, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Lcom/koushikdutta/ion/TransformBitmap;->report(Ljava/lang/Exception;Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V

    goto :goto_0

    .line 120
    .end local v4    # "e":Ljava/lang/OutOfMemoryError;
    .end local v11    # "size":Landroid/graphics/Point;
    .restart local v1    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v2    # "bitmaps":[Landroid/graphics/Bitmap;
    .restart local v6    # "i":I
    .restart local v7    # "i$":Ljava/util/Iterator;
    .restart local v12    # "size":Landroid/graphics/Point;
    .restart local v14    # "transform":Lcom/koushikdutta/ion/bitmap/Transform;
    :cond_2
    :try_start_2
    aput-object v1, v2, v6

    .line 121
    if-nez v12, :cond_7

    .line 122
    new-instance v11, Landroid/graphics/Point;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v15

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v16

    move/from16 v0, v16

    invoke-direct {v11, v15, v0}, Landroid/graphics/Point;-><init>(II)V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .end local v12    # "size":Landroid/graphics/Point;
    .restart local v11    # "size":Landroid/graphics/Point;
    :goto_4
    move-object v12, v11

    .line 123
    .end local v11    # "size":Landroid/graphics/Point;
    .restart local v12    # "size":Landroid/graphics/Point;
    goto :goto_2

    .line 114
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v14    # "transform":Lcom/koushikdutta/ion/bitmap/Transform;
    :cond_3
    add-int/lit8 v6, v6, 0x1

    move-object v11, v12

    .end local v12    # "size":Landroid/graphics/Point;
    .restart local v11    # "size":Landroid/graphics/Point;
    goto :goto_1

    .line 125
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_4
    :try_start_3
    new-instance v8, Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/koushikdutta/ion/TransformBitmap$2;->this$0:Lcom/koushikdutta/ion/TransformBitmap;

    iget-object v15, v15, Lcom/koushikdutta/ion/TransformBitmap;->key:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/TransformBitmap$2;->val$result:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->mimeType:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-direct {v8, v15, v0, v2, v11}, Lcom/koushikdutta/ion/bitmap/BitmapInfo;-><init>(Ljava/lang/String;Ljava/lang/String;[Landroid/graphics/Bitmap;Landroid/graphics/Point;)V

    .line 126
    .local v8, "info":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/koushikdutta/ion/TransformBitmap$2;->val$result:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v15, v15, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->delays:[I

    iput-object v15, v8, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->delays:[I

    .line 127
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/koushikdutta/ion/TransformBitmap$2;->val$result:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget v15, v15, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->loadedFrom:I

    iput v15, v8, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->loadedFrom:I

    .line 129
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/koushikdutta/ion/TransformBitmap$2;->this$0:Lcom/koushikdutta/ion/TransformBitmap;

    iget-object v15, v15, Lcom/koushikdutta/ion/TransformBitmap;->postProcess:Ljava/util/ArrayList;

    if-eqz v15, :cond_5

    .line 130
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/koushikdutta/ion/TransformBitmap$2;->this$0:Lcom/koushikdutta/ion/TransformBitmap;

    iget-object v15, v15, Lcom/koushikdutta/ion/TransformBitmap;->postProcess:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .restart local v7    # "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/koushikdutta/ion/bitmap/PostProcess;

    .line 131
    .local v10, "p":Lcom/koushikdutta/ion/bitmap/PostProcess;
    invoke-interface {v10, v8}, Lcom/koushikdutta/ion/bitmap/PostProcess;->postProcess(Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V

    goto :goto_5

    .line 137
    .end local v2    # "bitmaps":[Landroid/graphics/Bitmap;
    .end local v6    # "i":I
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "info":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    .end local v10    # "p":Lcom/koushikdutta/ion/bitmap/PostProcess;
    :catch_1
    move-exception v4

    goto :goto_3

    .line 135
    .restart local v2    # "bitmaps":[Landroid/graphics/Bitmap;
    .restart local v6    # "i":I
    .restart local v8    # "info":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/koushikdutta/ion/TransformBitmap$2;->this$0:Lcom/koushikdutta/ion/TransformBitmap;

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v8}, Lcom/koushikdutta/ion/TransformBitmap;->report(Ljava/lang/Exception;Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 148
    iget-object v15, v8, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    array-length v15, v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-gt v15, v0, :cond_0

    .line 150
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/koushikdutta/ion/TransformBitmap$2;->this$0:Lcom/koushikdutta/ion/TransformBitmap;

    iget-object v15, v15, Lcom/koushikdutta/ion/TransformBitmap;->ion:Lcom/koushikdutta/ion/Ion;

    iget-object v15, v15, Lcom/koushikdutta/ion/Ion;->responseCache:Lcom/koushikdutta/async/http/ResponseCacheMiddleware;

    invoke-virtual {v15}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->getFileCache()Lcom/koushikdutta/async/util/FileCache;

    move-result-object v3

    .line 151
    .local v3, "cache":Lcom/koushikdutta/async/util/FileCache;
    if-eqz v3, :cond_0

    .line 153
    invoke-virtual {v3}, Lcom/koushikdutta/async/util/FileCache;->getTempFile()Ljava/io/File;

    move-result-object v13

    .line 155
    .local v13, "tempFile":Ljava/io/File;
    :try_start_4
    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 156
    .local v9, "out":Ljava/io/FileOutputStream;
    iget-object v15, v8, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    const/16 v16, 0x0

    aget-object v15, v15, v16

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->hasAlpha()Z

    move-result v15

    if-eqz v15, :cond_6

    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    .line 157
    .local v5, "format":Landroid/graphics/Bitmap$CompressFormat;
    :goto_6
    iget-object v15, v8, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    const/16 v16, 0x0

    aget-object v15, v15, v16

    const/16 v16, 0x64

    move/from16 v0, v16

    invoke-virtual {v15, v5, v0, v9}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 158
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    .line 159
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/koushikdutta/ion/TransformBitmap$2;->this$0:Lcom/koushikdutta/ion/TransformBitmap;

    iget-object v15, v15, Lcom/koushikdutta/ion/TransformBitmap;->key:Ljava/lang/String;

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/io/File;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v13, v16, v17

    move-object/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Lcom/koushikdutta/async/util/FileCache;->commitTempFiles(Ljava/lang/String;[Ljava/io/File;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 164
    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    goto/16 :goto_0

    .line 141
    .end local v2    # "bitmaps":[Landroid/graphics/Bitmap;
    .end local v3    # "cache":Lcom/koushikdutta/async/util/FileCache;
    .end local v5    # "format":Landroid/graphics/Bitmap$CompressFormat;
    .end local v6    # "i":I
    .end local v8    # "info":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    .end local v9    # "out":Ljava/io/FileOutputStream;
    .end local v13    # "tempFile":Ljava/io/File;
    :catch_2
    move-exception v4

    .line 142
    .local v4, "e":Ljava/lang/Exception;
    :goto_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/koushikdutta/ion/TransformBitmap$2;->this$0:Lcom/koushikdutta/ion/TransformBitmap;

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v15, v4, v0}, Lcom/koushikdutta/ion/TransformBitmap;->report(Ljava/lang/Exception;Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V

    goto/16 :goto_0

    .line 156
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v2    # "bitmaps":[Landroid/graphics/Bitmap;
    .restart local v3    # "cache":Lcom/koushikdutta/async/util/FileCache;
    .restart local v6    # "i":I
    .restart local v8    # "info":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    .restart local v9    # "out":Ljava/io/FileOutputStream;
    .restart local v13    # "tempFile":Ljava/io/File;
    :cond_6
    :try_start_5
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_6

    .line 161
    .end local v9    # "out":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v15

    .line 164
    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    goto/16 :goto_0

    :catchall_0
    move-exception v15

    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    throw v15

    .line 141
    .end local v3    # "cache":Lcom/koushikdutta/async/util/FileCache;
    .end local v8    # "info":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    .end local v11    # "size":Landroid/graphics/Point;
    .end local v13    # "tempFile":Ljava/io/File;
    .restart local v7    # "i$":Ljava/util/Iterator;
    .restart local v12    # "size":Landroid/graphics/Point;
    :catch_4
    move-exception v4

    move-object v11, v12

    .end local v12    # "size":Landroid/graphics/Point;
    .restart local v11    # "size":Landroid/graphics/Point;
    goto :goto_7

    .end local v11    # "size":Landroid/graphics/Point;
    .restart local v1    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v12    # "size":Landroid/graphics/Point;
    .restart local v14    # "transform":Lcom/koushikdutta/ion/bitmap/Transform;
    :cond_7
    move-object v11, v12

    .end local v12    # "size":Landroid/graphics/Point;
    .restart local v11    # "size":Landroid/graphics/Point;
    goto/16 :goto_4
.end method
