.class Lcom/koushikdutta/ion/IonDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "IonDrawable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/koushikdutta/ion/IonDrawable$ImageViewFutureImpl;
    }
.end annotation


# static fields
.field private static final LOG_2:D


# instance fields
.field currentFrame:I

.field private disableFadeIn:Z

.field drawableCallback:Landroid/graphics/drawable/Drawable$Callback;

.field private error:Landroid/graphics/drawable/Drawable;

.field private errorResource:I

.field private info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

.field private invalidate:Ljava/lang/Runnable;

.field private invalidateScheduled:Z

.field private ion:Lcom/koushikdutta/ion/Ion;

.field private maxLevel:I

.field private paint:Landroid/graphics/Paint;

.field private placeholder:Landroid/graphics/drawable/Drawable;

.field private placeholderResource:I

.field private resizeHeight:I

.field private resizeWidth:I

.field private resources:Landroid/content/res/Resources;

.field private textureDim:I

.field tileCallback:Lcom/koushikdutta/async/future/FutureCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/koushikdutta/async/future/FutureCallback",
            "<",
            "Lcom/koushikdutta/ion/bitmap/BitmapInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 388
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sput-wide v0, Lcom/koushikdutta/ion/IonDrawable;->LOG_2:D

    return-void
.end method

.method private drawDrawable(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 399
    if-nez p2, :cond_0

    .line 444
    :goto_0
    return-void

    .line 440
    :cond_0
    invoke-virtual {p0}, Lcom/koushikdutta/ion/IonDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 443
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method private tryGetErrorResource()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 312
    iget-object v0, p0, Lcom/koushikdutta/ion/IonDrawable;->error:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/koushikdutta/ion/IonDrawable;->error:Landroid/graphics/drawable/Drawable;

    .line 318
    :goto_0
    return-object v0

    .line 314
    :cond_0
    iget v0, p0, Lcom/koushikdutta/ion/IonDrawable;->errorResource:I

    if-nez v0, :cond_1

    .line 315
    const/4 v0, 0x0

    goto :goto_0

    .line 316
    :cond_1
    iget-object v0, p0, Lcom/koushikdutta/ion/IonDrawable;->resources:Landroid/content/res/Resources;

    iget v1, p0, Lcom/koushikdutta/ion/IonDrawable;->errorResource:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/koushikdutta/ion/IonDrawable;->error:Landroid/graphics/drawable/Drawable;

    .line 317
    iget-object v0, p0, Lcom/koushikdutta/ion/IonDrawable;->error:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/koushikdutta/ion/IonDrawable;->drawableCallback:Landroid/graphics/drawable/Drawable$Callback;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 318
    iget-object v0, p0, Lcom/koushikdutta/ion/IonDrawable;->error:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private tryGetPlaceholderResource()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 322
    iget-object v0, p0, Lcom/koushikdutta/ion/IonDrawable;->placeholder:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/koushikdutta/ion/IonDrawable;->placeholder:Landroid/graphics/drawable/Drawable;

    .line 328
    :goto_0
    return-object v0

    .line 324
    :cond_0
    iget v0, p0, Lcom/koushikdutta/ion/IonDrawable;->placeholderResource:I

    if-nez v0, :cond_1

    .line 325
    const/4 v0, 0x0

    goto :goto_0

    .line 326
    :cond_1
    iget-object v0, p0, Lcom/koushikdutta/ion/IonDrawable;->resources:Landroid/content/res/Resources;

    iget v1, p0, Lcom/koushikdutta/ion/IonDrawable;->placeholderResource:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/koushikdutta/ion/IonDrawable;->placeholder:Landroid/graphics/drawable/Drawable;

    .line 327
    iget-object v0, p0, Lcom/koushikdutta/ion/IonDrawable;->placeholder:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/koushikdutta/ion/IonDrawable;->drawableCallback:Landroid/graphics/drawable/Drawable$Callback;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 328
    iget-object v0, p0, Lcom/koushikdutta/ion/IonDrawable;->placeholder:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 58
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 449
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    if-nez v4, :cond_1

    .line 450
    invoke-direct/range {p0 .. p0}, Lcom/koushikdutta/ion/IonDrawable;->tryGetPlaceholderResource()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4}, Lcom/koushikdutta/ion/IonDrawable;->drawDrawable(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    .line 648
    :cond_0
    :goto_0
    return-void

    .line 454
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-wide v4, v4, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->drawTime:J

    const-wide/16 v54, 0x0

    cmp-long v4, v4, v54

    if-nez v4, :cond_2

    .line 455
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v54

    move-wide/from16 v0, v54

    iput-wide v0, v4, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->drawTime:J

    .line 457
    :cond_2
    const-wide/16 v16, 0xff

    .line 459
    .local v16, "destAlpha":J
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/koushikdutta/ion/IonDrawable;->disableFadeIn:Z

    if-nez v4, :cond_3

    .line 460
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-wide v0, v7, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->drawTime:J

    move-wide/from16 v54, v0

    sub-long v4, v4, v54

    const/16 v7, 0x8

    shl-long/2addr v4, v7

    const-wide/16 v54, 0xc8

    div-long v16, v4, v54

    .line 461
    const-wide/16 v4, 0xff

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v16

    .line 464
    :cond_3
    const-wide/16 v4, 0xff

    cmp-long v4, v16, v4

    if-eqz v4, :cond_4

    .line 465
    invoke-direct/range {p0 .. p0}, Lcom/koushikdutta/ion/IonDrawable;->tryGetPlaceholderResource()Landroid/graphics/drawable/Drawable;

    move-result-object v31

    .line 466
    .local v31, "placeholder":Landroid/graphics/drawable/Drawable;
    if-eqz v31, :cond_4

    .line 467
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/koushikdutta/ion/IonDrawable;->drawDrawable(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    .line 471
    .end local v31    # "placeholder":Landroid/graphics/drawable/Drawable;
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v4, v4, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->decoder:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v4, :cond_16

    .line 483
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v13

    .line 484
    .local v13, "clip":Landroid/graphics/Rect;
    invoke-virtual/range {p0 .. p0}, Lcom/koushikdutta/ion/IonDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v12

    .line 486
    .local v12, "bounds":Landroid/graphics/Rect;
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v13}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    div-float v51, v4, v5

    .line 488
    .local v51, "zoom":F
    invoke-virtual {v12}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    mul-float v53, v51, v4

    .line 489
    .local v53, "zoomWidth":F
    invoke-virtual {v12}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    mul-float v52, v51, v4

    .line 491
    .local v52, "zoomHeight":F
    const/high16 v4, 0x43800000    # 256.0f

    div-float v4, v53, v4

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    sget-wide v54, Lcom/koushikdutta/ion/IonDrawable;->LOG_2:D

    div-double v48, v4, v54

    .line 492
    .local v48, "wlevel":D
    const/high16 v4, 0x43800000    # 256.0f

    div-float v4, v52, v4

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    sget-wide v54, Lcom/koushikdutta/ion/IonDrawable;->LOG_2:D

    div-double v18, v4, v54

    .line 493
    .local v18, "hlevel":D
    move-wide/from16 v0, v48

    move-wide/from16 v2, v18

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v24

    .line 495
    .local v24, "maxLevel":D
    const/4 v4, 0x0

    iget v5, v13, Landroid/graphics/Rect;->left:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v44

    .line 496
    .local v44, "visibleLeft":I
    invoke-virtual {v12}, Landroid/graphics/Rect;->width()I

    move-result v4

    iget v5, v13, Landroid/graphics/Rect;->right:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v45

    .line 497
    .local v45, "visibleRight":I
    const/4 v4, 0x0

    iget v5, v13, Landroid/graphics/Rect;->top:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v46

    .line 498
    .local v46, "visibleTop":I
    invoke-virtual {v12}, Landroid/graphics/Rect;->height()I

    move-result v4

    iget v5, v13, Landroid/graphics/Rect;->bottom:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v43

    .line 499
    .local v43, "visibleBottom":I
    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v0, v4

    move/from16 v21, v0

    .line 500
    .local v21, "level":I
    move-object/from16 v0, p0

    iget v4, v0, Lcom/koushikdutta/ion/IonDrawable;->maxLevel:I

    move/from16 v0, v21

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v21

    .line 501
    const/4 v4, 0x0

    move/from16 v0, v21

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v21

    .line 502
    const/4 v4, 0x1

    shl-int v22, v4, v21

    .line 503
    .local v22, "levelTiles":I
    move-object/from16 v0, p0

    iget v4, v0, Lcom/koushikdutta/ion/IonDrawable;->textureDim:I

    div-int v40, v4, v22

    .line 508
    .local v40, "textureTileDim":I
    const/4 v10, 0x0

    .line 509
    .local v10, "DEBUG_ZOOM":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v4, v4, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v4, v4, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    if-eqz v4, :cond_5

    .line 510
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v4, v4, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/koushikdutta/ion/IonDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/IonDrawable;->paint:Landroid/graphics/Paint;

    move-object/from16 v54, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v54

    invoke-virtual {v0, v4, v5, v7, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 523
    :goto_1
    const/4 v9, 0x1

    .line 524
    .local v9, "sampleSize":I
    :goto_2
    div-int v4, v40, v9

    const/16 v5, 0x100

    if-le v4, v5, :cond_6

    .line 525
    shl-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 519
    .end local v9    # "sampleSize":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/IonDrawable;->paint:Landroid/graphics/Paint;

    const/high16 v5, -0x1000000

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 520
    invoke-virtual/range {p0 .. p0}, Lcom/koushikdutta/ion/IonDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/koushikdutta/ion/IonDrawable;->paint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 527
    .restart local v9    # "sampleSize":I
    :cond_6
    const/16 v50, 0x0

    .local v50, "y":I
    :goto_3
    move/from16 v0, v50

    move/from16 v1, v22

    if-ge v0, v1, :cond_9

    .line 528
    mul-int v42, v40, v50

    .line 529
    .local v42, "top":I
    add-int/lit8 v4, v50, 0x1

    mul-int v11, v40, v4

    .line 530
    .local v11, "bottom":I
    iget v4, v12, Landroid/graphics/Rect;->bottom:I

    invoke-static {v11, v4}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 532
    move/from16 v0, v46

    if-ge v11, v0, :cond_8

    .line 527
    :cond_7
    add-int/lit8 v50, v50, 0x1

    goto :goto_3

    .line 534
    :cond_8
    move/from16 v0, v42

    move/from16 v1, v43

    if-le v0, v1, :cond_a

    .line 644
    .end local v9    # "sampleSize":I
    .end local v10    # "DEBUG_ZOOM":Z
    .end local v11    # "bottom":I
    .end local v12    # "bounds":Landroid/graphics/Rect;
    .end local v13    # "clip":Landroid/graphics/Rect;
    .end local v18    # "hlevel":D
    .end local v21    # "level":I
    .end local v22    # "levelTiles":I
    .end local v24    # "maxLevel":D
    .end local v40    # "textureTileDim":I
    .end local v42    # "top":I
    .end local v43    # "visibleBottom":I
    .end local v44    # "visibleLeft":I
    .end local v45    # "visibleRight":I
    .end local v46    # "visibleTop":I
    .end local v48    # "wlevel":D
    .end local v50    # "y":I
    .end local v51    # "zoom":F
    .end local v52    # "zoomHeight":F
    .end local v53    # "zoomWidth":F
    :cond_9
    :goto_4
    const-wide/16 v4, 0xff

    cmp-long v4, v16, v4

    if-eqz v4, :cond_0

    .line 645
    invoke-virtual/range {p0 .. p0}, Lcom/koushikdutta/ion/IonDrawable;->invalidateSelf()V

    goto/16 :goto_0

    .line 536
    .restart local v9    # "sampleSize":I
    .restart local v10    # "DEBUG_ZOOM":Z
    .restart local v11    # "bottom":I
    .restart local v12    # "bounds":Landroid/graphics/Rect;
    .restart local v13    # "clip":Landroid/graphics/Rect;
    .restart local v18    # "hlevel":D
    .restart local v21    # "level":I
    .restart local v22    # "levelTiles":I
    .restart local v24    # "maxLevel":D
    .restart local v40    # "textureTileDim":I
    .restart local v42    # "top":I
    .restart local v43    # "visibleBottom":I
    .restart local v44    # "visibleLeft":I
    .restart local v45    # "visibleRight":I
    .restart local v46    # "visibleTop":I
    .restart local v48    # "wlevel":D
    .restart local v50    # "y":I
    .restart local v51    # "zoom":F
    .restart local v52    # "zoomHeight":F
    .restart local v53    # "zoomWidth":F
    :cond_a
    const/16 v47, 0x0

    .local v47, "x":I
    :goto_5
    move/from16 v0, v47

    move/from16 v1, v22

    if-ge v0, v1, :cond_7

    .line 537
    mul-int v20, v40, v47

    .line 538
    .local v20, "left":I
    add-int/lit8 v4, v47, 0x1

    mul-int v32, v40, v4

    .line 539
    .local v32, "right":I
    iget v4, v12, Landroid/graphics/Rect;->right:I

    move/from16 v0, v32

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v32

    .line 541
    move/from16 v0, v32

    move/from16 v1, v44

    if-ge v0, v1, :cond_c

    .line 536
    :cond_b
    :goto_6
    add-int/lit8 v47, v47, 0x1

    goto :goto_5

    .line 543
    :cond_c
    move/from16 v0, v20

    move/from16 v1, v45

    if-gt v0, v1, :cond_7

    .line 546
    new-instance v8, Landroid/graphics/Rect;

    move/from16 v0, v20

    move/from16 v1, v42

    move/from16 v2, v32

    invoke-direct {v8, v0, v1, v2, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 550
    .local v8, "texRect":Landroid/graphics/Rect;
    const/4 v4, 0x7

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v7, v7, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->key:Ljava/lang/String;

    aput-object v7, v4, v5

    const/4 v5, 0x1

    const-string v7, ","

    aput-object v7, v4, v5

    const/4 v5, 0x2

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x3

    const-string v7, ","

    aput-object v7, v4, v5

    const/4 v5, 0x4

    invoke-static/range {v47 .. v47}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x5

    const-string v7, ","

    aput-object v7, v4, v5

    const/4 v5, 0x6

    invoke-static/range {v50 .. v50}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    invoke-static {v4}, Lcom/koushikdutta/async/util/FileCache;->toKeyString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 551
    .local v6, "tileKey":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/IonDrawable;->ion:Lcom/koushikdutta/ion/Ion;

    iget-object v4, v4, Lcom/koushikdutta/ion/Ion;->bitmapCache:Lcom/koushikdutta/ion/bitmap/IonBitmapCache;

    invoke-virtual {v4, v6}, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->get(Ljava/lang/String;)Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    move-result-object v41

    .line 552
    .local v41, "tile":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    if-eqz v41, :cond_d

    move-object/from16 v0, v41

    iget-object v4, v0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    if-eqz v4, :cond_d

    .line 555
    move-object/from16 v0, v41

    iget-object v4, v0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/koushikdutta/ion/IonDrawable;->paint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v8, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_6

    .line 560
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/IonDrawable;->ion:Lcom/koushikdutta/ion/Ion;

    iget-object v4, v4, Lcom/koushikdutta/ion/Ion;->bitmapsPending:Lcom/koushikdutta/async/util/HashList;

    invoke-virtual {v4, v6}, Lcom/koushikdutta/async/util/HashList;->tag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_e

    .line 563
    new-instance v4, Lcom/koushikdutta/ion/LoadBitmapRegion;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/koushikdutta/ion/IonDrawable;->ion:Lcom/koushikdutta/ion/Ion;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v7, v7, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->decoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-direct/range {v4 .. v9}, Lcom/koushikdutta/ion/LoadBitmapRegion;-><init>(Lcom/koushikdutta/ion/Ion;Ljava/lang/String;Landroid/graphics/BitmapRegionDecoder;Landroid/graphics/Rect;I)V

    .line 565
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/IonDrawable;->ion:Lcom/koushikdutta/ion/Ion;

    iget-object v4, v4, Lcom/koushikdutta/ion/Ion;->bitmapsPending:Lcom/koushikdutta/async/util/HashList;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/koushikdutta/ion/IonDrawable;->tileCallback:Lcom/koushikdutta/async/future/FutureCallback;

    invoke-virtual {v4, v6, v5}, Lcom/koushikdutta/async/util/HashList;->add(Ljava/lang/String;Ljava/lang/Object;)V

    .line 567
    const/16 v23, 0x0

    .line 568
    .local v23, "parentLeft":I
    const/16 v27, 0x0

    .line 569
    .local v27, "parentTop":I
    const/16 v28, 0x1

    .line 570
    .local v28, "parentUp":I
    sub-int v26, v21, v28

    .line 571
    .local v26, "parentLevel":I
    rem-int/lit8 v4, v47, 0x2

    const/4 v5, 0x1

    if-ne v4, v5, :cond_f

    .line 572
    add-int/lit8 v23, v23, 0x1

    .line 573
    :cond_f
    rem-int/lit8 v4, v50, 0x2

    const/4 v5, 0x1

    if-ne v4, v5, :cond_10

    .line 574
    add-int/lit8 v27, v27, 0x1

    .line 575
    :cond_10
    shr-int/lit8 v29, v47, 0x1

    .line 576
    .local v29, "parentX":I
    shr-int/lit8 v30, v50, 0x1

    .line 578
    .local v30, "parentY":I
    :goto_7
    if-ltz v26, :cond_11

    .line 579
    const/4 v4, 0x7

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v7, v7, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->key:Ljava/lang/String;

    aput-object v7, v4, v5

    const/4 v5, 0x1

    const-string v7, ","

    aput-object v7, v4, v5

    const/4 v5, 0x2

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x3

    const-string v7, ","

    aput-object v7, v4, v5

    const/4 v5, 0x4

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x5

    const-string v7, ","

    aput-object v7, v4, v5

    const/4 v5, 0x6

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    invoke-static {v4}, Lcom/koushikdutta/async/util/FileCache;->toKeyString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 580
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/IonDrawable;->ion:Lcom/koushikdutta/ion/Ion;

    iget-object v4, v4, Lcom/koushikdutta/ion/Ion;->bitmapCache:Lcom/koushikdutta/ion/bitmap/IonBitmapCache;

    invoke-virtual {v4, v6}, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->get(Ljava/lang/String;)Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    move-result-object v41

    .line 581
    if-eqz v41, :cond_12

    move-object/from16 v0, v41

    iget-object v4, v0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    if-eqz v4, :cond_12

    .line 596
    :cond_11
    if-eqz v41, :cond_b

    move-object/from16 v0, v41

    iget-object v4, v0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    if-eqz v4, :cond_b

    .line 600
    const/4 v4, 0x1

    shl-int v36, v4, v26

    .line 601
    .local v36, "subLevelTiles":I
    move-object/from16 v0, p0

    iget v4, v0, Lcom/koushikdutta/ion/IonDrawable;->textureDim:I

    div-int v39, v4, v36

    .line 602
    .local v39, "subtileDim":I
    const/16 v37, 0x1

    .line 603
    .local v37, "subSampleSize":I
    :goto_8
    div-int v4, v39, v37

    const/16 v5, 0x100

    if-le v4, v5, :cond_15

    .line 604
    shl-int/lit8 v37, v37, 0x1

    goto :goto_8

    .line 583
    .end local v36    # "subLevelTiles":I
    .end local v37    # "subSampleSize":I
    .end local v39    # "subtileDim":I
    :cond_12
    rem-int/lit8 v4, v29, 0x2

    const/4 v5, 0x1

    if-ne v4, v5, :cond_13

    .line 584
    const/4 v4, 0x1

    shl-int v4, v4, v28

    add-int v23, v23, v4

    .line 586
    :cond_13
    rem-int/lit8 v4, v30, 0x2

    const/4 v5, 0x1

    if-ne v4, v5, :cond_14

    .line 587
    const/4 v4, 0x1

    shl-int v4, v4, v28

    add-int v27, v27, v4

    .line 589
    :cond_14
    add-int/lit8 v26, v26, -0x1

    .line 590
    add-int/lit8 v28, v28, 0x1

    .line 591
    shr-int/lit8 v29, v29, 0x1

    .line 592
    shr-int/lit8 v30, v30, 0x1

    goto :goto_7

    .line 605
    .restart local v36    # "subLevelTiles":I
    .restart local v37    # "subSampleSize":I
    .restart local v39    # "subtileDim":I
    :cond_15
    div-int v38, v39, v37

    .line 607
    .local v38, "subTextureDim":I
    shr-int v38, v38, v28

    .line 608
    mul-int v33, v38, v23

    .line 609
    .local v33, "sourceLeft":I
    mul-int v35, v38, v27

    .line 610
    .local v35, "sourceTop":I
    new-instance v34, Landroid/graphics/Rect;

    add-int v4, v33, v38

    add-int v5, v35, v38

    move-object/from16 v0, v34

    move/from16 v1, v33

    move/from16 v2, v35

    invoke-direct {v0, v1, v2, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 611
    .local v34, "sourceRect":Landroid/graphics/Rect;
    move-object/from16 v0, v41

    iget-object v4, v0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/koushikdutta/ion/IonDrawable;->paint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v4, v1, v8, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_6

    .line 622
    .end local v6    # "tileKey":Ljava/lang/String;
    .end local v8    # "texRect":Landroid/graphics/Rect;
    .end local v9    # "sampleSize":I
    .end local v10    # "DEBUG_ZOOM":Z
    .end local v11    # "bottom":I
    .end local v12    # "bounds":Landroid/graphics/Rect;
    .end local v13    # "clip":Landroid/graphics/Rect;
    .end local v18    # "hlevel":D
    .end local v20    # "left":I
    .end local v21    # "level":I
    .end local v22    # "levelTiles":I
    .end local v23    # "parentLeft":I
    .end local v24    # "maxLevel":D
    .end local v26    # "parentLevel":I
    .end local v27    # "parentTop":I
    .end local v28    # "parentUp":I
    .end local v29    # "parentX":I
    .end local v30    # "parentY":I
    .end local v32    # "right":I
    .end local v33    # "sourceLeft":I
    .end local v34    # "sourceRect":Landroid/graphics/Rect;
    .end local v35    # "sourceTop":I
    .end local v36    # "subLevelTiles":I
    .end local v37    # "subSampleSize":I
    .end local v38    # "subTextureDim":I
    .end local v39    # "subtileDim":I
    .end local v40    # "textureTileDim":I
    .end local v41    # "tile":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    .end local v42    # "top":I
    .end local v43    # "visibleBottom":I
    .end local v44    # "visibleLeft":I
    .end local v45    # "visibleRight":I
    .end local v46    # "visibleTop":I
    .end local v47    # "x":I
    .end local v48    # "wlevel":D
    .end local v50    # "y":I
    .end local v51    # "zoom":F
    .end local v52    # "zoomHeight":F
    .end local v53    # "zoomWidth":F
    :cond_16
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v4, v4, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    if-eqz v4, :cond_17

    .line 623
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/IonDrawable;->paint:Landroid/graphics/Paint;

    move-wide/from16 v0, v16

    long-to-int v5, v0

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 624
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v4, v4, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/koushikdutta/ion/IonDrawable;->currentFrame:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v7, v7, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    array-length v7, v7

    rem-int/2addr v5, v7

    aget-object v4, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/koushikdutta/ion/IonDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/koushikdutta/ion/IonDrawable;->paint:Landroid/graphics/Paint;

    move-object/from16 v54, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v54

    invoke-virtual {v0, v4, v5, v7, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 625
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/IonDrawable;->paint:Landroid/graphics/Paint;

    const/16 v5, 0xff

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 626
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v4, v4, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->delays:[I

    if-eqz v4, :cond_9

    .line 627
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v4, v4, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->delays:[I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/koushikdutta/ion/IonDrawable;->currentFrame:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v7, v7, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->delays:[I

    array-length v7, v7

    rem-int/2addr v5, v7

    aget v14, v4, v5

    .line 628
    .local v14, "delay":I
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/koushikdutta/ion/IonDrawable;->invalidateScheduled:Z

    if-nez v4, :cond_9

    .line 629
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/koushikdutta/ion/IonDrawable;->invalidateScheduled:Z

    .line 630
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/IonDrawable;->invalidate:Ljava/lang/Runnable;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/koushikdutta/ion/IonDrawable;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 631
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/IonDrawable;->invalidate:Ljava/lang/Runnable;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v54

    const/16 v5, 0x64

    invoke-static {v14, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    int-to-long v0, v5

    move-wide/from16 v56, v0

    add-long v54, v54, v56

    move-object/from16 v0, p0

    move-wide/from16 v1, v54

    invoke-virtual {v0, v4, v1, v2}, Lcom/koushikdutta/ion/IonDrawable;->scheduleSelf(Ljava/lang/Runnable;J)V

    goto/16 :goto_4

    .line 636
    .end local v14    # "delay":I
    :cond_17
    invoke-direct/range {p0 .. p0}, Lcom/koushikdutta/ion/IonDrawable;->tryGetErrorResource()Landroid/graphics/drawable/Drawable;

    move-result-object v15

    .line 637
    .local v15, "error":Landroid/graphics/drawable/Drawable;
    if-eqz v15, :cond_9

    .line 638
    move-wide/from16 v0, v16

    long-to-int v4, v0

    invoke-virtual {v15, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 639
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v15}, Lcom/koushikdutta/ion/IonDrawable;->drawDrawable(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    .line 640
    const/16 v4, 0xff

    invoke-virtual {v15, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto/16 :goto_4
.end method

.method public getIntrinsicHeight()I
    .locals 4

    .prologue
    .line 359
    iget-object v2, p0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    if-eqz v2, :cond_1

    .line 360
    iget-object v2, p0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v2, v2, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->decoder:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v2, :cond_0

    .line 361
    iget-object v2, p0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v2, v2, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->originalSize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    .line 375
    :goto_0
    return v2

    .line 362
    :cond_0
    iget-object v2, p0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v2, v2, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    .line 363
    iget-object v2, p0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v2, v2, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/koushikdutta/ion/IonDrawable;->resources:Landroid/content/res/Resources;

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v2, v3}, Landroid/graphics/Bitmap;->getScaledHeight(I)I

    move-result v2

    goto :goto_0

    .line 365
    :cond_1
    iget v2, p0, Lcom/koushikdutta/ion/IonDrawable;->resizeHeight:I

    if-lez v2, :cond_2

    .line 366
    iget v2, p0, Lcom/koushikdutta/ion/IonDrawable;->resizeHeight:I

    goto :goto_0

    .line 367
    :cond_2
    iget-object v2, p0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    if-eqz v2, :cond_3

    .line 368
    invoke-direct {p0}, Lcom/koushikdutta/ion/IonDrawable;->tryGetErrorResource()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 369
    .local v0, "error":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_3

    .line 370
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    goto :goto_0

    .line 372
    .end local v0    # "error":Landroid/graphics/drawable/Drawable;
    :cond_3
    invoke-direct {p0}, Lcom/koushikdutta/ion/IonDrawable;->tryGetPlaceholderResource()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 373
    .local v1, "placeholder":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_4

    .line 374
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    goto :goto_0

    .line 375
    :cond_4
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public getIntrinsicWidth()I
    .locals 4

    .prologue
    .line 334
    iget-object v2, p0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    if-eqz v2, :cond_1

    .line 335
    iget-object v2, p0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v2, v2, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->decoder:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v2, :cond_0

    .line 336
    iget-object v2, p0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v2, v2, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->originalSize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    .line 354
    :goto_0
    return v2

    .line 337
    :cond_0
    iget-object v2, p0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v2, v2, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    .line 338
    iget-object v2, p0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v2, v2, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/koushikdutta/ion/IonDrawable;->resources:Landroid/content/res/Resources;

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v2, v3}, Landroid/graphics/Bitmap;->getScaledWidth(I)I

    move-result v2

    goto :goto_0

    .line 341
    :cond_1
    iget v2, p0, Lcom/koushikdutta/ion/IonDrawable;->resizeWidth:I

    if-lez v2, :cond_2

    .line 342
    iget v2, p0, Lcom/koushikdutta/ion/IonDrawable;->resizeWidth:I

    goto :goto_0

    .line 344
    :cond_2
    iget-object v2, p0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    if-eqz v2, :cond_3

    .line 345
    invoke-direct {p0}, Lcom/koushikdutta/ion/IonDrawable;->tryGetErrorResource()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 346
    .local v0, "error":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_3

    .line 347
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    goto :goto_0

    .line 350
    .end local v0    # "error":Landroid/graphics/drawable/Drawable;
    :cond_3
    invoke-direct {p0}, Lcom/koushikdutta/ion/IonDrawable;->tryGetPlaceholderResource()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 351
    .local v1, "placeholder":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_4

    .line 352
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    goto :goto_0

    .line 354
    :cond_4
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public getOpacity()I
    .locals 2

    .prologue
    .line 691
    iget-object v0, p0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v0, v0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/koushikdutta/ion/IonDrawable;->info:Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v0, v0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->hasAlpha()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/koushikdutta/ion/IonDrawable;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    const/16 v1, 0xff

    if-ge v0, v1, :cond_1

    :cond_0
    const/4 v0, -0x3

    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public setAlpha(I)V
    .locals 1
    .param p1, "alpha"    # I

    .prologue
    .line 681
    iget-object v0, p0, Lcom/koushikdutta/ion/IonDrawable;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 682
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1
    .param p1, "cf"    # Landroid/graphics/ColorFilter;

    .prologue
    .line 686
    iget-object v0, p0, Lcom/koushikdutta/ion/IonDrawable;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 687
    return-void
.end method

.method public setDither(Z)V
    .locals 1
    .param p1, "dither"    # Z

    .prologue
    .line 290
    iget-object v0, p0, Lcom/koushikdutta/ion/IonDrawable;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 291
    invoke-virtual {p0}, Lcom/koushikdutta/ion/IonDrawable;->invalidateSelf()V

    .line 292
    return-void
.end method

.method public setFilterBitmap(Z)V
    .locals 1
    .param p1, "filter"    # Z

    .prologue
    .line 284
    iget-object v0, p0, Lcom/koushikdutta/ion/IonDrawable;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 285
    invoke-virtual {p0}, Lcom/koushikdutta/ion/IonDrawable;->invalidateSelf()V

    .line 286
    return-void
.end method
