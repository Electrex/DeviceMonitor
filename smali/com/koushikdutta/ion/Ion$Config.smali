.class public Lcom/koushikdutta/ion/Ion$Config;
.super Ljava/lang/Object;
.source "Ion.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/koushikdutta/ion/Ion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Config"
.end annotation


# instance fields
.field asyncHttpRequestFactory:Lcom/koushikdutta/ion/loader/AsyncHttpRequestFactory;

.field final synthetic this$0:Lcom/koushikdutta/ion/Ion;


# direct methods
.method public constructor <init>(Lcom/koushikdutta/ion/Ion;)V
    .locals 1

    .prologue
    .line 479
    iput-object p1, p0, Lcom/koushikdutta/ion/Ion$Config;->this$0:Lcom/koushikdutta/ion/Ion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 572
    new-instance v0, Lcom/koushikdutta/ion/Ion$Config$1;

    invoke-direct {v0, p0}, Lcom/koushikdutta/ion/Ion$Config$1;-><init>(Lcom/koushikdutta/ion/Ion$Config;)V

    iput-object v0, p0, Lcom/koushikdutta/ion/Ion$Config;->asyncHttpRequestFactory:Lcom/koushikdutta/ion/loader/AsyncHttpRequestFactory;

    return-void
.end method


# virtual methods
.method public addLoader(Lcom/koushikdutta/ion/Loader;)Lcom/koushikdutta/ion/Ion$Config;
    .locals 1
    .param p1, "loader"    # Lcom/koushikdutta/ion/Loader;

    .prologue
    .line 609
    iget-object v0, p0, Lcom/koushikdutta/ion/Ion$Config;->this$0:Lcom/koushikdutta/ion/Ion;

    iget-object v0, v0, Lcom/koushikdutta/ion/Ion;->loaders:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 610
    return-object p0
.end method

.method public getAsyncHttpRequestFactory()Lcom/koushikdutta/ion/loader/AsyncHttpRequestFactory;
    .locals 1

    .prologue
    .line 583
    iget-object v0, p0, Lcom/koushikdutta/ion/Ion$Config;->asyncHttpRequestFactory:Lcom/koushikdutta/ion/loader/AsyncHttpRequestFactory;

    return-object v0
.end method

.method public getLoaders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/koushikdutta/ion/Loader;",
            ">;"
        }
    .end annotation

    .prologue
    .line 613
    iget-object v0, p0, Lcom/koushikdutta/ion/Ion$Config;->this$0:Lcom/koushikdutta/ion/Ion;

    iget-object v0, v0, Lcom/koushikdutta/ion/Ion;->loaders:Ljava/util/ArrayList;

    return-object v0
.end method
