.class public Lcom/koushikdutta/ion/Loader$LoaderEmitter;
.super Ljava/lang/Object;
.source "Loader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/koushikdutta/ion/Loader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LoaderEmitter"
.end annotation


# instance fields
.field emitter:Lcom/koushikdutta/async/DataEmitter;

.field headers:Lcom/koushikdutta/async/http/libcore/RawHeaders;

.field length:J

.field loadedFrom:I

.field request:Lcom/koushikdutta/async/http/AsyncHttpRequest;


# direct methods
.method public constructor <init>(Lcom/koushikdutta/async/DataEmitter;JILcom/koushikdutta/async/http/libcore/RawHeaders;Lcom/koushikdutta/async/http/AsyncHttpRequest;)V
    .locals 0
    .param p1, "emitter"    # Lcom/koushikdutta/async/DataEmitter;
    .param p2, "length"    # J
    .param p4, "loadedFrom"    # I
    .param p5, "headers"    # Lcom/koushikdutta/async/http/libcore/RawHeaders;
    .param p6, "request"    # Lcom/koushikdutta/async/http/AsyncHttpRequest;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-wide p2, p0, Lcom/koushikdutta/ion/Loader$LoaderEmitter;->length:J

    .line 26
    iput-object p1, p0, Lcom/koushikdutta/ion/Loader$LoaderEmitter;->emitter:Lcom/koushikdutta/async/DataEmitter;

    .line 27
    iput p4, p0, Lcom/koushikdutta/ion/Loader$LoaderEmitter;->loadedFrom:I

    .line 28
    iput-object p5, p0, Lcom/koushikdutta/ion/Loader$LoaderEmitter;->headers:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    .line 29
    iput-object p6, p0, Lcom/koushikdutta/ion/Loader$LoaderEmitter;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    .line 30
    return-void
.end method


# virtual methods
.method public getDataEmitter()Lcom/koushikdutta/async/DataEmitter;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/koushikdutta/ion/Loader$LoaderEmitter;->emitter:Lcom/koushikdutta/async/DataEmitter;

    return-object v0
.end method

.method public getHeaders()Lcom/koushikdutta/async/http/libcore/RawHeaders;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/koushikdutta/ion/Loader$LoaderEmitter;->headers:Lcom/koushikdutta/async/http/libcore/RawHeaders;

    return-object v0
.end method

.method public getRequest()Lcom/koushikdutta/async/http/AsyncHttpRequest;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/koushikdutta/ion/Loader$LoaderEmitter;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    return-object v0
.end method

.method public length()J
    .locals 2

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/koushikdutta/ion/Loader$LoaderEmitter;->length:J

    return-wide v0
.end method

.method public loadedFrom()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/koushikdutta/ion/Loader$LoaderEmitter;->loadedFrom:I

    return v0
.end method
