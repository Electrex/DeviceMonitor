.class Lcom/koushikdutta/ion/bitmap/LruBitmapCache;
.super Lcom/koushikdutta/async/util/LruCache;
.source "LruBitmapCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/koushikdutta/async/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "Lcom/koushikdutta/ion/bitmap/BitmapInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private soft:Lcom/koushikdutta/ion/bitmap/SoftReferenceHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/koushikdutta/ion/bitmap/SoftReferenceHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/koushikdutta/ion/bitmap/BitmapInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "maxSize"    # I

    .prologue
    .line 9
    int-to-long v0, p1

    invoke-direct {p0, v0, v1}, Lcom/koushikdutta/async/util/LruCache;-><init>(J)V

    .line 6
    new-instance v0, Lcom/koushikdutta/ion/bitmap/SoftReferenceHashtable;

    invoke-direct {v0}, Lcom/koushikdutta/ion/bitmap/SoftReferenceHashtable;-><init>()V

    iput-object v0, p0, Lcom/koushikdutta/ion/bitmap/LruBitmapCache;->soft:Lcom/koushikdutta/ion/bitmap/SoftReferenceHashtable;

    .line 10
    return-void
.end method


# virtual methods
.method protected bridge synthetic entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Z
    .param p2, "x1"    # Ljava/lang/Object;
    .param p3, "x2"    # Ljava/lang/Object;
    .param p4, "x3"    # Ljava/lang/Object;

    .prologue
    .line 5
    check-cast p2, Ljava/lang/String;

    .end local p2    # "x1":Ljava/lang/Object;
    check-cast p3, Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    .end local p3    # "x2":Ljava/lang/Object;
    check-cast p4, Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    .end local p4    # "x3":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/koushikdutta/ion/bitmap/LruBitmapCache;->entryRemoved(ZLjava/lang/String;Lcom/koushikdutta/ion/bitmap/BitmapInfo;Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V

    return-void
.end method

.method protected entryRemoved(ZLjava/lang/String;Lcom/koushikdutta/ion/bitmap/BitmapInfo;Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V
    .locals 1
    .param p1, "evicted"    # Z
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "oldValue"    # Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    .param p4, "newValue"    # Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    .prologue
    .line 44
    invoke-super {p0, p1, p2, p3, p4}, Lcom/koushikdutta/async/util/LruCache;->entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 47
    if-eqz p1, :cond_0

    .line 48
    iget-object v0, p0, Lcom/koushikdutta/ion/bitmap/LruBitmapCache;->soft:Lcom/koushikdutta/ion/bitmap/SoftReferenceHashtable;

    invoke-virtual {v0, p2, p3}, Lcom/koushikdutta/ion/bitmap/SoftReferenceHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    :cond_0
    return-void
.end method

.method public getBitmapInfo(Ljava/lang/String;)Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/koushikdutta/ion/bitmap/LruBitmapCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    .line 19
    .local v0, "ret":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    if-eqz v0, :cond_0

    move-object v1, v0

    .line 26
    .end local v0    # "ret":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    .local v1, "ret":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    :goto_0
    return-object v1

    .line 22
    .end local v1    # "ret":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    .restart local v0    # "ret":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    :cond_0
    iget-object v2, p0, Lcom/koushikdutta/ion/bitmap/LruBitmapCache;->soft:Lcom/koushikdutta/ion/bitmap/SoftReferenceHashtable;

    invoke-virtual {v2, p1}, Lcom/koushikdutta/ion/bitmap/SoftReferenceHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "ret":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    check-cast v0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    .line 23
    .restart local v0    # "ret":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    if-eqz v0, :cond_1

    .line 24
    invoke-virtual {p0, p1, v0}, Lcom/koushikdutta/ion/bitmap/LruBitmapCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    move-object v1, v0

    .line 26
    .end local v0    # "ret":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    .restart local v1    # "ret":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    goto :goto_0
.end method

.method protected bridge synthetic sizeOf(Ljava/lang/Object;Ljava/lang/Object;)J
    .locals 2
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 5
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/koushikdutta/ion/bitmap/LruBitmapCache;->sizeOf(Ljava/lang/String;Lcom/koushikdutta/ion/bitmap/BitmapInfo;)J

    move-result-wide v0

    return-wide v0
.end method

.method protected sizeOf(Ljava/lang/String;Lcom/koushikdutta/ion/bitmap/BitmapInfo;)J
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "info"    # Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    .prologue
    .line 14
    invoke-virtual {p2}, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->sizeOf()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method
