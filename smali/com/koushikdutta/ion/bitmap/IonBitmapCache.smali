.class public Lcom/koushikdutta/ion/bitmap/IonBitmapCache;
.super Ljava/lang/Object;
.source "IonBitmapCache.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field cache:Lcom/koushikdutta/ion/bitmap/LruBitmapCache;

.field errorCacheDuration:J

.field heapRatio:D

.field ion:Lcom/koushikdutta/ion/Ion;

.field metrics:Landroid/util/DisplayMetrics;

.field resources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/koushikdutta/ion/Ion;)V
    .locals 5
    .param p1, "ion"    # Lcom/koushikdutta/ion/Ion;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const-wide/16 v2, 0x7530

    iput-wide v2, p0, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->errorCacheDuration:J

    .line 69
    const-wide v2, 0x3fc2492492492492L    # 0.14285714285714285

    iput-wide v2, p0, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->heapRatio:D

    .line 51
    invoke-virtual {p1}, Lcom/koushikdutta/ion/Ion;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 52
    .local v0, "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->ion:Lcom/koushikdutta/ion/Ion;

    .line 53
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v2, p0, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->metrics:Landroid/util/DisplayMetrics;

    .line 54
    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    iget-object v3, p0, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->metrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v2, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 56
    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    .line 57
    .local v1, "mgr":Landroid/content/res/AssetManager;
    new-instance v2, Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->metrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    invoke-direct {v2, v1, v3, v4}, Landroid/content/res/Resources;-><init>(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;)V

    iput-object v2, p0, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->resources:Landroid/content/res/Resources;

    .line 58
    new-instance v2, Lcom/koushikdutta/ion/bitmap/LruBitmapCache;

    invoke-static {v0}, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->getHeapSize(Landroid/content/Context;)I

    move-result v3

    div-int/lit8 v3, v3, 0x7

    invoke-direct {v2, v3}, Lcom/koushikdutta/ion/bitmap/LruBitmapCache;-><init>(I)V

    iput-object v2, p0, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->cache:Lcom/koushikdutta/ion/bitmap/LruBitmapCache;

    .line 59
    return-void
.end method

.method private computeTarget(II)Landroid/graphics/Point;
    .locals 3
    .param p1, "minx"    # I
    .param p2, "miny"    # I

    .prologue
    .line 111
    move v1, p1

    .line 112
    .local v1, "targetWidth":I
    move v0, p2

    .line 113
    .local v0, "targetHeight":I
    if-nez v1, :cond_0

    .line 114
    iget-object v2, p0, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->metrics:Landroid/util/DisplayMetrics;

    iget v1, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 115
    :cond_0
    if-gtz v1, :cond_1

    .line 116
    const v1, 0x7fffffff

    .line 117
    :cond_1
    if-nez v0, :cond_2

    .line 118
    iget-object v2, p0, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->metrics:Landroid/util/DisplayMetrics;

    iget v0, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 119
    :cond_2
    if-gtz v0, :cond_3

    .line 120
    const v0, 0x7fffffff

    .line 121
    :cond_3
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v1, v0}, Landroid/graphics/Point;-><init>(II)V

    return-object v2
.end method

.method private static getHeapSize(Landroid/content/Context;)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 254
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    mul-int/lit16 v0, v0, 0x400

    mul-int/lit16 v0, v0, 0x400

    return v0
.end method

.method private static getRotatedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "rotation"    # I

    .prologue
    const/4 v1, 0x0

    .line 166
    if-nez p0, :cond_1

    .line 167
    const/4 p0, 0x0

    .line 173
    .end local p0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object p0

    .line 168
    .restart local p0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    if-eqz p1, :cond_0

    .line 171
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 172
    .local v5, "matrix":Landroid/graphics/Matrix;
    int-to-float v0, p1

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 173
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p0

    goto :goto_0
.end method

.method public static loadBitmap(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "id"    # I
    .param p2, "o"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    const/4 v8, 0x0

    .line 194
    sget-boolean v6, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v7

    if-ne v6, v7, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 197
    :cond_0
    const/4 v3, 0x0

    .line 199
    .local v3, "in":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v3

    .line 200
    const v6, 0xc350

    new-array v1, v6, [B

    .line 201
    .local v1, "bytes":[B
    invoke-virtual {v3, v1}, Ljava/io/InputStream;->read([B)I

    move-result v4

    .line 202
    .local v4, "length":I
    const/4 v6, 0x0

    invoke-static {v1, v6, v4}, Lcom/koushikdutta/ion/bitmap/Exif;->getOrientation([BII)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 207
    .end local v1    # "bytes":[B
    .end local v4    # "length":I
    .local v5, "rotation":I
    :goto_0
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/io/Closeable;

    aput-object v3, v6, v8

    invoke-static {v6}, Lcom/koushikdutta/async/util/StreamUtility;->closeQuietly([Ljava/io/Closeable;)V

    .line 209
    invoke-static {p0, p1, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 210
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-static {v0, v5}, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->getRotatedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v6

    return-object v6

    .line 204
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v5    # "rotation":I
    :catch_0
    move-exception v2

    .line 205
    .local v2, "e":Ljava/lang/Exception;
    const/4 v5, 0x0

    .restart local v5    # "rotation":I
    goto :goto_0
.end method

.method public static loadBitmap(Ljava/io/File;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "file"    # Ljava/io/File;
    .param p1, "o"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    const/4 v9, 0x0

    .line 234
    sget-boolean v7, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->$assertionsDisabled:Z

    if-nez v7, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v8

    invoke-virtual {v8}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v8

    if-ne v7, v8, :cond_0

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 237
    :cond_0
    const/4 v3, 0x0

    .line 239
    .local v3, "fin":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 240
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .local v4, "fin":Ljava/io/FileInputStream;
    const v7, 0xc350

    :try_start_1
    new-array v1, v7, [B

    .line 241
    .local v1, "bytes":[B
    invoke-virtual {v4, v1}, Ljava/io/FileInputStream;->read([B)I

    move-result v5

    .line 242
    .local v5, "length":I
    const/4 v7, 0x0

    invoke-static {v1, v7, v5}, Lcom/koushikdutta/ion/bitmap/Exif;->getOrientation([BII)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v6

    .local v6, "rotation":I
    move-object v3, v4

    .line 247
    .end local v1    # "bytes":[B
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .end local v5    # "length":I
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    :goto_0
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/io/Closeable;

    aput-object v3, v7, v9

    invoke-static {v7}, Lcom/koushikdutta/async/util/StreamUtility;->closeQuietly([Ljava/io/Closeable;)V

    .line 249
    invoke-virtual {p0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 250
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-static {v0, v6}, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->getRotatedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v7

    return-object v7

    .line 244
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v6    # "rotation":I
    :catch_0
    move-exception v2

    .line 245
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    const/4 v6, 0x0

    .restart local v6    # "rotation":I
    goto :goto_0

    .line 244
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .end local v6    # "rotation":I
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    :catch_1
    move-exception v2

    move-object v3, v4

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method public static loadBitmap(Ljava/io/InputStream;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "stream"    # Ljava/io/InputStream;
    .param p1, "o"    # Landroid/graphics/BitmapFactory$Options;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v8, 0xc350

    .line 214
    sget-boolean v6, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v7

    if-ne v6, v7, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 217
    :cond_0
    new-instance v3, Lcom/koushikdutta/ion/bitmap/MarkableInputStream;

    invoke-direct {v3, p0}, Lcom/koushikdutta/ion/bitmap/MarkableInputStream;-><init>(Ljava/io/InputStream;)V

    .line 218
    .local v3, "in":Lcom/koushikdutta/ion/bitmap/MarkableInputStream;
    invoke-virtual {v3, v8}, Lcom/koushikdutta/ion/bitmap/MarkableInputStream;->mark(I)V

    .line 220
    const v6, 0xc350

    :try_start_0
    new-array v1, v6, [B

    .line 221
    .local v1, "bytes":[B
    invoke-virtual {v3, v1}, Lcom/koushikdutta/ion/bitmap/MarkableInputStream;->read([B)I

    move-result v4

    .line 222
    .local v4, "length":I
    const/4 v6, 0x0

    invoke-static {v1, v6, v4}, Lcom/koushikdutta/ion/bitmap/Exif;->getOrientation([BII)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 227
    .end local v1    # "bytes":[B
    .end local v4    # "length":I
    .local v5, "rotation":I
    :goto_0
    invoke-virtual {v3}, Lcom/koushikdutta/ion/bitmap/MarkableInputStream;->reset()V

    .line 229
    const/4 v6, 0x0

    invoke-static {v3, v6, p1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 230
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-static {v0, v5}, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->getRotatedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v6

    return-object v6

    .line 224
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v5    # "rotation":I
    :catch_0
    move-exception v2

    .line 225
    .local v2, "e":Ljava/lang/Exception;
    const/4 v5, 0x0

    .restart local v5    # "rotation":I
    goto :goto_0
.end method

.method public static loadBitmap([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "bytes"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "o"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 177
    sget-boolean v2, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v3

    if-ne v2, v3, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 179
    :cond_0
    invoke-static {p0, p1, p2, p3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 180
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    .line 181
    const/4 v2, 0x0

    .line 183
    :goto_0
    return-object v2

    .line 182
    :cond_1
    invoke-static {p0, p1, p2}, Lcom/koushikdutta/ion/bitmap/Exif;->getOrientation([BII)I

    move-result v1

    .line 183
    .local v1, "rotation":I
    invoke-static {v0, v1}, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->getRotatedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0
.end method

.method public static loadRegion(Landroid/graphics/BitmapRegionDecoder;Landroid/graphics/Rect;I)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "decoder"    # Landroid/graphics/BitmapRegionDecoder;
    .param p1, "sourceRect"    # Landroid/graphics/Rect;
    .param p2, "inSampleSize"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0xa
    .end annotation

    .prologue
    .line 188
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 189
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    iput p2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 190
    invoke-virtual {p0, p1, v0}, Landroid/graphics/BitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method private prepareBitmapOptions(Landroid/graphics/BitmapFactory$Options;II)Landroid/graphics/BitmapFactory$Options;
    .locals 6
    .param p1, "o"    # Landroid/graphics/BitmapFactory$Options;
    .param p2, "minx"    # I
    .param p3, "miny"    # I

    .prologue
    .line 125
    iget v3, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-ltz v3, :cond_0

    iget v3, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-gez v3, :cond_1

    .line 126
    :cond_0
    const/4 v0, 0x0

    .line 134
    :goto_0
    return-object v0

    .line 127
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->computeTarget(II)Landroid/graphics/Point;

    move-result-object v2

    .line 128
    .local v2, "target":Landroid/graphics/Point;
    iget v3, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v4, v2, Landroid/graphics/Point;->x:I

    div-int/2addr v3, v4

    iget v4, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iget v5, v2, Landroid/graphics/Point;->y:I

    div-int/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 129
    .local v1, "scale":I
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 130
    .local v0, "ret":Landroid/graphics/BitmapFactory$Options;
    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 131
    iget v3, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 132
    iget v3, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput v3, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 133
    iget-object v3, p1, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    iput-object v3, v0, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public get(Ljava/lang/String;)Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    .locals 6
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 87
    if-nez p1, :cond_1

    move-object v0, v1

    .line 102
    :cond_0
    :goto_0
    return-object v0

    .line 91
    :cond_1
    iget-object v2, p0, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->cache:Lcom/koushikdutta/ion/bitmap/LruBitmapCache;

    invoke-virtual {v2, p1}, Lcom/koushikdutta/ion/bitmap/LruBitmapCache;->getBitmapInfo(Ljava/lang/String;)Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    move-result-object v0

    .line 92
    .local v0, "ret":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    .line 98
    iget-wide v2, v0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->loadTime:J

    iget-wide v4, p0, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->errorCacheDuration:J

    add-long/2addr v2, v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    .line 101
    iget-object v2, p0, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->cache:Lcom/koushikdutta/ion/bitmap/LruBitmapCache;

    invoke-virtual {v2, p1}, Lcom/koushikdutta/ion/bitmap/LruBitmapCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 102
    goto :goto_0
.end method

.method public prepareBitmapOptions(Landroid/content/res/Resources;III)Landroid/graphics/BitmapFactory$Options;
    .locals 2
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "id"    # I
    .param p3, "minx"    # I
    .param p4, "miny"    # I

    .prologue
    .line 152
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 153
    .local v0, "o":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 154
    invoke-static {p1, p2, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 155
    invoke-direct {p0, v0, p3, p4}, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->prepareBitmapOptions(Landroid/graphics/BitmapFactory$Options;II)Landroid/graphics/BitmapFactory$Options;

    move-result-object v1

    return-object v1
.end method

.method public prepareBitmapOptions(Ljava/io/File;II)Landroid/graphics/BitmapFactory$Options;
    .locals 2
    .param p1, "file"    # Ljava/io/File;
    .param p2, "minx"    # I
    .param p3, "miny"    # I

    .prologue
    .line 138
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 139
    .local v0, "o":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 140
    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 141
    invoke-direct {p0, v0, p2, p3}, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->prepareBitmapOptions(Landroid/graphics/BitmapFactory$Options;II)Landroid/graphics/BitmapFactory$Options;

    move-result-object v1

    return-object v1
.end method

.method public prepareBitmapOptions(Ljava/io/InputStream;II)Landroid/graphics/BitmapFactory$Options;
    .locals 2
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "minx"    # I
    .param p3, "miny"    # I

    .prologue
    .line 159
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 160
    .local v0, "o":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 161
    const/4 v1, 0x0

    invoke-static {p1, v1, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 162
    invoke-direct {p0, v0, p2, p3}, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->prepareBitmapOptions(Landroid/graphics/BitmapFactory$Options;II)Landroid/graphics/BitmapFactory$Options;

    move-result-object v1

    return-object v1
.end method

.method public prepareBitmapOptions([BIIII)Landroid/graphics/BitmapFactory$Options;
    .locals 2
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .param p4, "minx"    # I
    .param p5, "miny"    # I

    .prologue
    .line 145
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 146
    .local v0, "o":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 147
    invoke-static {p1, p2, p3, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 148
    invoke-direct {p0, v0, p4, p5}, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->prepareBitmapOptions(Landroid/graphics/BitmapFactory$Options;II)Landroid/graphics/BitmapFactory$Options;

    move-result-object v1

    return-object v1
.end method

.method public put(Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V
    .locals 6
    .param p1, "info"    # Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    .prologue
    .line 79
    sget-boolean v1, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v2

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 80
    :cond_0
    iget-object v1, p0, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->ion:Lcom/koushikdutta/ion/Ion;

    invoke-virtual {v1}, Lcom/koushikdutta/ion/Ion;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->getHeapSize(Landroid/content/Context;)I

    move-result v1

    int-to-double v2, v1

    iget-wide v4, p0, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->heapRatio:D

    mul-double/2addr v2, v4

    double-to-int v0, v2

    .line 81
    .local v0, "maxSize":I
    int-to-long v2, v0

    iget-object v1, p0, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->cache:Lcom/koushikdutta/ion/bitmap/LruBitmapCache;

    invoke-virtual {v1}, Lcom/koushikdutta/ion/bitmap/LruBitmapCache;->maxSize()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 82
    iget-object v1, p0, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->cache:Lcom/koushikdutta/ion/bitmap/LruBitmapCache;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Lcom/koushikdutta/ion/bitmap/LruBitmapCache;->setMaxSize(J)V

    .line 83
    :cond_1
    iget-object v1, p0, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->cache:Lcom/koushikdutta/ion/bitmap/LruBitmapCache;

    iget-object v2, p1, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->key:Ljava/lang/String;

    invoke-virtual {v1, v2, p1}, Lcom/koushikdutta/ion/bitmap/LruBitmapCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    return-void
.end method
