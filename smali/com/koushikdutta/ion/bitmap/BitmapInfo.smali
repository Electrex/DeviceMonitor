.class public Lcom/koushikdutta/ion/bitmap/BitmapInfo;
.super Ljava/lang/Object;
.source "BitmapInfo.java"


# instance fields
.field public final bitmaps:[Landroid/graphics/Bitmap;

.field public decoder:Landroid/graphics/BitmapRegionDecoder;

.field public decoderFile:Ljava/io/File;

.field public delays:[I

.field public drawTime:J

.field public exception:Ljava/lang/Exception;

.field public final extras:Lcom/koushikdutta/async/util/UntypedHashtable;

.field public final key:Ljava/lang/String;

.field public loadTime:J

.field public loadedFrom:I

.field public final mimeType:Ljava/lang/String;

.field public final originalSize:Landroid/graphics/Point;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[Landroid/graphics/Bitmap;Landroid/graphics/Point;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "bitmaps"    # [Landroid/graphics/Bitmap;
    .param p4, "originalSize"    # Landroid/graphics/Point;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->loadTime:J

    .line 33
    new-instance v0, Lcom/koushikdutta/async/util/UntypedHashtable;

    invoke-direct {v0}, Lcom/koushikdutta/async/util/UntypedHashtable;-><init>()V

    iput-object v0, p0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->extras:Lcom/koushikdutta/async/util/UntypedHashtable;

    .line 16
    iput-object p4, p0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->originalSize:Landroid/graphics/Point;

    .line 17
    iput-object p3, p0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    .line 18
    iput-object p1, p0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->key:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->mimeType:Ljava/lang/String;

    .line 20
    return-void
.end method


# virtual methods
.method public sizeOf()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 36
    iget-object v1, p0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    .line 38
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v1

    iget-object v2, p0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    aget-object v0, v2, v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    mul-int/2addr v0, v1

    iget-object v1, p0, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->bitmaps:[Landroid/graphics/Bitmap;

    array-length v1, v1

    mul-int/2addr v0, v1

    goto :goto_0
.end method
