.class final Lcom/koushikdutta/ion/TransformBitmap$1;
.super Ljava/lang/Object;
.source "TransformBitmap.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/koushikdutta/ion/TransformBitmap;->getBitmapSnapshot(Lcom/koushikdutta/ion/Ion;Ljava/lang/String;Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$callback:Lcom/koushikdutta/ion/BitmapCallback;

.field final synthetic val$ion:Lcom/koushikdutta/ion/Ion;

.field final synthetic val$postProcess:Ljava/util/ArrayList;

.field final synthetic val$transformKey:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/koushikdutta/ion/Ion;Ljava/lang/String;Lcom/koushikdutta/ion/BitmapCallback;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/koushikdutta/ion/TransformBitmap$1;->val$ion:Lcom/koushikdutta/ion/Ion;

    iput-object p2, p0, Lcom/koushikdutta/ion/TransformBitmap$1;->val$transformKey:Ljava/lang/String;

    iput-object p3, p0, Lcom/koushikdutta/ion/TransformBitmap$1;->val$callback:Lcom/koushikdutta/ion/BitmapCallback;

    iput-object p4, p0, Lcom/koushikdutta/ion/TransformBitmap$1;->val$postProcess:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 46
    iget-object v7, p0, Lcom/koushikdutta/ion/TransformBitmap$1;->val$ion:Lcom/koushikdutta/ion/Ion;

    iget-object v7, v7, Lcom/koushikdutta/ion/Ion;->bitmapsPending:Lcom/koushikdutta/async/util/HashList;

    iget-object v8, p0, Lcom/koushikdutta/ion/TransformBitmap$1;->val$transformKey:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/koushikdutta/async/util/HashList;->tag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    iget-object v8, p0, Lcom/koushikdutta/ion/TransformBitmap$1;->val$callback:Lcom/koushikdutta/ion/BitmapCallback;

    if-eq v7, v8, :cond_0

    .line 78
    :goto_0
    return-void

    .line 52
    :cond_0
    :try_start_0
    iget-object v7, p0, Lcom/koushikdutta/ion/TransformBitmap$1;->val$ion:Lcom/koushikdutta/ion/Ion;

    iget-object v7, v7, Lcom/koushikdutta/ion/Ion;->responseCache:Lcom/koushikdutta/async/http/ResponseCacheMiddleware;

    invoke-virtual {v7}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->getFileCache()Lcom/koushikdutta/async/util/FileCache;

    move-result-object v7

    iget-object v8, p0, Lcom/koushikdutta/ion/TransformBitmap$1;->val$transformKey:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/koushikdutta/async/util/FileCache;->getFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 53
    .local v2, "file":Ljava/io/File;
    const/4 v7, 0x0

    invoke-static {v2, v7}, Lcom/koushikdutta/ion/bitmap/IonBitmapCache;->loadBitmap(Ljava/io/File;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 54
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    .line 55
    new-instance v7, Ljava/lang/Exception;

    const-string v8, "Bitmap failed to load"

    invoke-direct {v7, v8}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 68
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "file":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 69
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    iget-object v7, p0, Lcom/koushikdutta/ion/TransformBitmap$1;->val$callback:Lcom/koushikdutta/ion/BitmapCallback;

    new-instance v8, Ljava/lang/Exception;

    invoke-direct {v8, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {v7, v8, v11}, Lcom/koushikdutta/ion/BitmapCallback;->report(Ljava/lang/Exception;Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V

    goto :goto_0

    .line 56
    .end local v1    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v2    # "file":Ljava/io/File;
    :cond_1
    :try_start_1
    new-instance v6, Landroid/graphics/Point;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    invoke-direct {v6, v7, v8}, Landroid/graphics/Point;-><init>(II)V

    .line 57
    .local v6, "size":Landroid/graphics/Point;
    new-instance v4, Lcom/koushikdutta/ion/bitmap/BitmapInfo;

    iget-object v7, p0, Lcom/koushikdutta/ion/TransformBitmap$1;->val$transformKey:Ljava/lang/String;

    const-string v8, "image/jpeg"

    const/4 v9, 0x1

    new-array v9, v9, [Landroid/graphics/Bitmap;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    invoke-direct {v4, v7, v8, v9, v6}, Lcom/koushikdutta/ion/bitmap/BitmapInfo;-><init>(Ljava/lang/String;Ljava/lang/String;[Landroid/graphics/Bitmap;Landroid/graphics/Point;)V

    .line 58
    .local v4, "info":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    const/4 v7, 0x1

    iput v7, v4, Lcom/koushikdutta/ion/bitmap/BitmapInfo;->loadedFrom:I

    .line 60
    iget-object v7, p0, Lcom/koushikdutta/ion/TransformBitmap$1;->val$postProcess:Ljava/util/ArrayList;

    if-eqz v7, :cond_2

    .line 61
    iget-object v7, p0, Lcom/koushikdutta/ion/TransformBitmap$1;->val$postProcess:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/koushikdutta/ion/bitmap/PostProcess;

    .line 62
    .local v5, "p":Lcom/koushikdutta/ion/bitmap/PostProcess;
    invoke-interface {v5, v4}, Lcom/koushikdutta/ion/bitmap/PostProcess;->postProcess(Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 71
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "info":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    .end local v5    # "p":Lcom/koushikdutta/ion/bitmap/PostProcess;
    .end local v6    # "size":Landroid/graphics/Point;
    :catch_1
    move-exception v1

    .line 72
    .local v1, "e":Ljava/lang/Exception;
    iget-object v7, p0, Lcom/koushikdutta/ion/TransformBitmap$1;->val$callback:Lcom/koushikdutta/ion/BitmapCallback;

    invoke-virtual {v7, v1, v11}, Lcom/koushikdutta/ion/BitmapCallback;->report(Ljava/lang/Exception;Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V

    .line 74
    :try_start_2
    iget-object v7, p0, Lcom/koushikdutta/ion/TransformBitmap$1;->val$ion:Lcom/koushikdutta/ion/Ion;

    iget-object v7, v7, Lcom/koushikdutta/ion/Ion;->responseCache:Lcom/koushikdutta/async/http/ResponseCacheMiddleware;

    invoke-virtual {v7}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->getFileCache()Lcom/koushikdutta/async/util/FileCache;

    move-result-object v7

    iget-object v8, p0, Lcom/koushikdutta/ion/TransformBitmap$1;->val$transformKey:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/koushikdutta/async/util/FileCache;->remove(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 75
    :catch_2
    move-exception v7

    goto :goto_0

    .line 66
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v4    # "info":Lcom/koushikdutta/ion/bitmap/BitmapInfo;
    .restart local v6    # "size":Landroid/graphics/Point;
    :cond_2
    :try_start_3
    iget-object v7, p0, Lcom/koushikdutta/ion/TransformBitmap$1;->val$callback:Lcom/koushikdutta/ion/BitmapCallback;

    const/4 v8, 0x0

    invoke-virtual {v7, v8, v4}, Lcom/koushikdutta/ion/BitmapCallback;->report(Ljava/lang/Exception;Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0
.end method
