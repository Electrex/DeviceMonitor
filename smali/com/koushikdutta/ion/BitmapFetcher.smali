.class Lcom/koushikdutta/ion/BitmapFetcher;
.super Ljava/lang/Object;
.source "BitmapFetcher.java"

# interfaces
.implements Lcom/koushikdutta/ion/IonRequestBuilder$LoadRequestCallback;


# instance fields
.field animateGif:Z

.field bitmapKey:Ljava/lang/String;

.field builder:Lcom/koushikdutta/ion/IonRequestBuilder;

.field deepZoom:Z

.field downloadKey:Ljava/lang/String;

.field hasTransforms:Z

.field postProcess:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/koushikdutta/ion/bitmap/PostProcess;",
            ">;"
        }
    .end annotation
.end field

.field resizeHeight:I

.field resizeWidth:I

.field transforms:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/koushikdutta/ion/bitmap/Transform;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private executeTransforms(Lcom/koushikdutta/ion/Ion;)V
    .locals 8
    .param p1, "ion"    # Lcom/koushikdutta/ion/Ion;

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/koushikdutta/ion/BitmapFetcher;->hasTransforms:Z

    if-nez v0, :cond_1

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    iget-object v0, p1, Lcom/koushikdutta/ion/Ion;->bitmapsPending:Lcom/koushikdutta/async/util/HashList;

    iget-object v1, p0, Lcom/koushikdutta/ion/BitmapFetcher;->bitmapKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/koushikdutta/async/util/HashList;->tag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 100
    iget-object v6, p1, Lcom/koushikdutta/ion/Ion;->bitmapsPending:Lcom/koushikdutta/async/util/HashList;

    iget-object v7, p0, Lcom/koushikdutta/ion/BitmapFetcher;->downloadKey:Ljava/lang/String;

    new-instance v0, Lcom/koushikdutta/ion/TransformBitmap;

    iget-object v2, p0, Lcom/koushikdutta/ion/BitmapFetcher;->bitmapKey:Ljava/lang/String;

    iget-object v3, p0, Lcom/koushikdutta/ion/BitmapFetcher;->downloadKey:Ljava/lang/String;

    iget-object v4, p0, Lcom/koushikdutta/ion/BitmapFetcher;->transforms:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/koushikdutta/ion/BitmapFetcher;->postProcess:Ljava/util/ArrayList;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/koushikdutta/ion/TransformBitmap;-><init>(Lcom/koushikdutta/ion/Ion;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual {v6, v7, v0}, Lcom/koushikdutta/async/util/HashList;->add(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private fastLoad(Ljava/lang/String;)Z
    .locals 17
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 33
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/BitmapFetcher;->builder:Lcom/koushikdutta/ion/IonRequestBuilder;

    iget-object v2, v4, Lcom/koushikdutta/ion/IonRequestBuilder;->ion:Lcom/koushikdutta/ion/Ion;

    .line 34
    .local v2, "ion":Lcom/koushikdutta/ion/Ion;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/koushikdutta/ion/BitmapFetcher;->deepZoom:Z

    if-eqz v4, :cond_4

    .line 35
    if-eqz p1, :cond_0

    const-string v4, "file:/"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 36
    :cond_0
    const/4 v4, 0x0

    .line 65
    :goto_0
    return v4

    .line 37
    :cond_1
    new-instance v12, Ljava/io/File;

    invoke-static/range {p1 .. p1}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v4

    invoke-direct {v12, v4}, Ljava/io/File;-><init>(Ljava/net/URI;)V

    .line 38
    .local v12, "file":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_2

    .line 39
    const/4 v4, 0x0

    goto :goto_0

    .line 40
    :cond_2
    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/koushikdutta/ion/loader/MediaFile;->getFileType(Ljava/lang/String;)Lcom/koushikdutta/ion/loader/MediaFile$MediaFileType;

    move-result-object v16

    .line 41
    .local v16, "type":Lcom/koushikdutta/ion/loader/MediaFile$MediaFileType;
    if-eqz v16, :cond_3

    move-object/from16 v0, v16

    iget v4, v0, Lcom/koushikdutta/ion/loader/MediaFile$MediaFileType;->fileType:I

    invoke-static {v4}, Lcom/koushikdutta/ion/loader/MediaFile;->isVideoFileType(I)Z

    move-result v4

    if-nez v4, :cond_4

    .line 42
    :cond_3
    new-instance v1, Lcom/koushikdutta/ion/LoadDeepZoom;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/koushikdutta/ion/BitmapFetcher;->downloadKey:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/koushikdutta/ion/BitmapFetcher;->animateGif:Z

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/koushikdutta/ion/LoadDeepZoom;-><init>(Lcom/koushikdutta/ion/Ion;Ljava/lang/String;ZLcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;Lcom/koushikdutta/async/util/FileCache;)V

    .line 43
    .local v1, "loadDeepZoom":Lcom/koushikdutta/ion/LoadDeepZoom;
    const/4 v4, 0x0

    invoke-virtual {v1, v4, v12}, Lcom/koushikdutta/ion/LoadDeepZoom;->onCompleted(Ljava/lang/Exception;Ljava/io/File;)V

    .line 45
    const/4 v4, 0x1

    goto :goto_0

    .line 50
    .end local v1    # "loadDeepZoom":Lcom/koushikdutta/ion/LoadDeepZoom;
    .end local v12    # "file":Ljava/io/File;
    .end local v16    # "type":Lcom/koushikdutta/ion/loader/MediaFile$MediaFileType;
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/koushikdutta/ion/BitmapFetcher;->hasTransforms:Z

    if-nez v4, :cond_6

    const/4 v15, 0x1

    .line 52
    .local v15, "put":Z
    :goto_1
    invoke-virtual {v2}, Lcom/koushikdutta/ion/Ion;->configure()Lcom/koushikdutta/ion/Ion$Config;

    move-result-object v4

    invoke-virtual {v4}, Lcom/koushikdutta/ion/Ion$Config;->getLoaders()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :cond_5
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/koushikdutta/ion/Loader;

    .line 53
    .local v3, "loader":Lcom/koushikdutta/ion/Loader;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/BitmapFetcher;->builder:Lcom/koushikdutta/ion/IonRequestBuilder;

    iget-object v4, v4, Lcom/koushikdutta/ion/IonRequestBuilder;->contextReference:Lcom/koushikdutta/ion/ContextReference;

    invoke-virtual {v4}, Lcom/koushikdutta/ion/ContextReference;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/koushikdutta/ion/BitmapFetcher;->downloadKey:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/koushikdutta/ion/BitmapFetcher;->resizeWidth:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/koushikdutta/ion/BitmapFetcher;->resizeHeight:I

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/koushikdutta/ion/BitmapFetcher;->animateGif:Z

    move-object v5, v2

    move-object/from16 v7, p1

    invoke-interface/range {v3 .. v10}, Lcom/koushikdutta/ion/Loader;->loadBitmap(Landroid/content/Context;Lcom/koushikdutta/ion/Ion;Ljava/lang/String;Ljava/lang/String;IIZ)Lcom/koushikdutta/async/future/Future;

    move-result-object v13

    .line 54
    .local v13, "future":Lcom/koushikdutta/async/future/Future;, "Lcom/koushikdutta/async/future/Future<Lcom/koushikdutta/ion/bitmap/BitmapInfo;>;"
    if-eqz v13, :cond_5

    .line 55
    new-instance v11, Lcom/koushikdutta/ion/LoadBitmapBase;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/koushikdutta/ion/BitmapFetcher;->downloadKey:Ljava/lang/String;

    invoke-direct {v11, v2, v4, v15}, Lcom/koushikdutta/ion/LoadBitmapBase;-><init>(Lcom/koushikdutta/ion/Ion;Ljava/lang/String;Z)V

    .line 56
    .local v11, "callback":Lcom/koushikdutta/ion/BitmapCallback;
    new-instance v4, Lcom/koushikdutta/ion/BitmapFetcher$1;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v11}, Lcom/koushikdutta/ion/BitmapFetcher$1;-><init>(Lcom/koushikdutta/ion/BitmapFetcher;Lcom/koushikdutta/ion/BitmapCallback;)V

    invoke-interface {v13, v4}, Lcom/koushikdutta/async/future/Future;->setCallback(Lcom/koushikdutta/async/future/FutureCallback;)Lcom/koushikdutta/async/future/Future;

    .line 62
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 50
    .end local v3    # "loader":Lcom/koushikdutta/ion/Loader;
    .end local v11    # "callback":Lcom/koushikdutta/ion/BitmapCallback;
    .end local v13    # "future":Lcom/koushikdutta/async/future/Future;, "Lcom/koushikdutta/async/future/Future<Lcom/koushikdutta/ion/bitmap/BitmapInfo;>;"
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v15    # "put":Z
    :cond_6
    const/4 v15, 0x0

    goto :goto_1

    .line 65
    .restart local v14    # "i$":Ljava/util/Iterator;
    .restart local v15    # "put":Z
    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_0
.end method

.method public static shouldDeferImageView(Lcom/koushikdutta/ion/Ion;)Z
    .locals 7
    .param p0, "ion"    # Lcom/koushikdutta/ion/Ion;

    .prologue
    const/4 v6, 0x5

    const/4 v4, 0x0

    .line 71
    iget-object v5, p0, Lcom/koushikdutta/ion/Ion;->bitmapsPending:Lcom/koushikdutta/async/util/HashList;

    invoke-virtual {v5}, Lcom/koushikdutta/async/util/HashList;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    if-gt v5, v6, :cond_1

    .line 82
    :cond_0
    :goto_0
    return v4

    .line 73
    :cond_1
    const/4 v2, 0x0

    .line 74
    .local v2, "loadCount":I
    iget-object v5, p0, Lcom/koushikdutta/ion/Ion;->bitmapsPending:Lcom/koushikdutta/async/util/HashList;

    invoke-virtual {v5}, Lcom/koushikdutta/async/util/HashList;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 75
    .local v1, "key":Ljava/lang/String;
    iget-object v5, p0, Lcom/koushikdutta/ion/Ion;->bitmapsPending:Lcom/koushikdutta/async/util/HashList;

    invoke-virtual {v5, v1}, Lcom/koushikdutta/async/util/HashList;->tag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 76
    .local v3, "owner":Ljava/lang/Object;
    instance-of v5, v3, Lcom/koushikdutta/ion/LoadBitmapBase;

    if-eqz v5, :cond_2

    .line 77
    add-int/lit8 v2, v2, 0x1

    .line 78
    if-le v2, v6, :cond_2

    .line 79
    const/4 v4, 0x1

    goto :goto_0
.end method


# virtual methods
.method public execute()V
    .locals 17

    .prologue
    .line 110
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/koushikdutta/ion/BitmapFetcher;->builder:Lcom/koushikdutta/ion/IonRequestBuilder;

    iget-object v2, v1, Lcom/koushikdutta/ion/IonRequestBuilder;->ion:Lcom/koushikdutta/ion/Ion;

    .line 115
    .local v2, "ion":Lcom/koushikdutta/ion/Ion;
    iget-object v1, v2, Lcom/koushikdutta/ion/Ion;->responseCache:Lcom/koushikdutta/async/http/ResponseCacheMiddleware;

    invoke-virtual {v1}, Lcom/koushikdutta/async/http/ResponseCacheMiddleware;->getFileCache()Lcom/koushikdutta/async/util/FileCache;

    move-result-object v15

    .line 116
    .local v15, "fileCache":Lcom/koushikdutta/async/util/FileCache;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/koushikdutta/ion/BitmapFetcher;->builder:Lcom/koushikdutta/ion/IonRequestBuilder;

    iget-boolean v1, v1, Lcom/koushikdutta/ion/IonRequestBuilder;->noCache:Z

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/koushikdutta/ion/BitmapFetcher;->hasTransforms:Z

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/koushikdutta/ion/BitmapFetcher;->bitmapKey:Ljava/lang/String;

    invoke-virtual {v15, v1}, Lcom/koushikdutta/async/util/FileCache;->exists(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/koushikdutta/ion/BitmapFetcher;->deepZoom:Z

    if-nez v1, :cond_0

    .line 117
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/koushikdutta/ion/BitmapFetcher;->bitmapKey:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/koushikdutta/ion/BitmapFetcher;->postProcess:Ljava/util/ArrayList;

    invoke-static {v2, v1, v3}, Lcom/koushikdutta/ion/TransformBitmap;->getBitmapSnapshot(Lcom/koushikdutta/ion/Ion;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 155
    :goto_0
    return-void

    .line 122
    :cond_0
    iget-object v1, v2, Lcom/koushikdutta/ion/Ion;->bitmapsPending:Lcom/koushikdutta/async/util/HashList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/koushikdutta/ion/BitmapFetcher;->downloadKey:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/koushikdutta/async/util/HashList;->tag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/koushikdutta/ion/BitmapFetcher;->builder:Lcom/koushikdutta/ion/IonRequestBuilder;

    iget-object v1, v1, Lcom/koushikdutta/ion/IonRequestBuilder;->uri:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/koushikdutta/ion/BitmapFetcher;->fastLoad(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 123
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/koushikdutta/ion/BitmapFetcher;->builder:Lcom/koushikdutta/ion/IonRequestBuilder;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/koushikdutta/ion/IonRequestBuilder;->setHandler(Landroid/os/Handler;)Lcom/koushikdutta/ion/IonRequestBuilder;

    .line 124
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/koushikdutta/ion/BitmapFetcher;->builder:Lcom/koushikdutta/ion/IonRequestBuilder;

    move-object/from16 v0, p0

    iput-object v0, v1, Lcom/koushikdutta/ion/IonRequestBuilder;->loadRequestCallback:Lcom/koushikdutta/ion/IonRequestBuilder$LoadRequestCallback;

    .line 126
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/koushikdutta/ion/BitmapFetcher;->deepZoom:Z

    if-nez v1, :cond_3

    .line 127
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/koushikdutta/ion/BitmapFetcher;->builder:Lcom/koushikdutta/ion/IonRequestBuilder;

    new-instance v3, Lcom/koushikdutta/async/parser/ByteBufferListParser;

    invoke-direct {v3}, Lcom/koushikdutta/async/parser/ByteBufferListParser;-><init>()V

    new-instance v4, Lcom/koushikdutta/ion/BitmapFetcher$2;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v2}, Lcom/koushikdutta/ion/BitmapFetcher$2;-><init>(Lcom/koushikdutta/ion/BitmapFetcher;Lcom/koushikdutta/ion/Ion;)V

    invoke-virtual {v1, v3, v4}, Lcom/koushikdutta/ion/IonRequestBuilder;->execute(Lcom/koushikdutta/async/parser/AsyncParser;Ljava/lang/Runnable;)Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;

    move-result-object v8

    .line 138
    .local v8, "emitterTransform":Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;, "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform<Lcom/koushikdutta/async/ByteBufferList;>;"
    new-instance v1, Lcom/koushikdutta/ion/LoadBitmap;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/koushikdutta/ion/BitmapFetcher;->downloadKey:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/koushikdutta/ion/BitmapFetcher;->hasTransforms:Z

    if-nez v4, :cond_2

    const/4 v4, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget v5, v0, Lcom/koushikdutta/ion/BitmapFetcher;->resizeWidth:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/koushikdutta/ion/BitmapFetcher;->resizeHeight:I

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/koushikdutta/ion/BitmapFetcher;->animateGif:Z

    invoke-direct/range {v1 .. v8}, Lcom/koushikdutta/ion/LoadBitmap;-><init>(Lcom/koushikdutta/ion/Ion;Ljava/lang/String;ZIIZLcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;)V

    invoke-virtual {v8, v1}, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->setCallback(Lcom/koushikdutta/async/future/FutureCallback;)Lcom/koushikdutta/async/future/SimpleFuture;

    .line 154
    .end local v8    # "emitterTransform":Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;, "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform<Lcom/koushikdutta/async/ByteBufferList;>;"
    :cond_1
    :goto_2
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/koushikdutta/ion/BitmapFetcher;->executeTransforms(Lcom/koushikdutta/ion/Ion;)V

    goto :goto_0

    .line 138
    .restart local v8    # "emitterTransform":Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;, "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform<Lcom/koushikdutta/async/ByteBufferList;>;"
    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    .line 142
    .end local v8    # "emitterTransform":Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;, "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform<Lcom/koushikdutta/async/ByteBufferList;>;"
    :cond_3
    invoke-virtual {v15}, Lcom/koushikdutta/async/util/FileCache;->getTempFile()Ljava/io/File;

    move-result-object v16

    .line 143
    .local v16, "file":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/koushikdutta/ion/BitmapFetcher;->builder:Lcom/koushikdutta/ion/IonRequestBuilder;

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Lcom/koushikdutta/ion/IonRequestBuilder;->write(Ljava/io/File;)Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;

    move-result-object v14

    .line 144
    .local v14, "emitterTransform":Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;, "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform<Ljava/io/File;>;"
    new-instance v9, Lcom/koushikdutta/ion/BitmapFetcher$3;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/koushikdutta/ion/BitmapFetcher;->downloadKey:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/koushikdutta/ion/BitmapFetcher;->animateGif:Z

    move-object/from16 v10, p0

    move-object v11, v2

    invoke-direct/range {v9 .. v15}, Lcom/koushikdutta/ion/BitmapFetcher$3;-><init>(Lcom/koushikdutta/ion/BitmapFetcher;Lcom/koushikdutta/ion/Ion;Ljava/lang/String;ZLcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;Lcom/koushikdutta/async/util/FileCache;)V

    .line 150
    .local v9, "loadDeepZoom":Lcom/koushikdutta/ion/LoadDeepZoom;
    invoke-virtual {v14, v9}, Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;->setCallback(Lcom/koushikdutta/async/future/FutureCallback;)Lcom/koushikdutta/async/future/SimpleFuture;

    goto :goto_2
.end method

.method public loadRequest(Lcom/koushikdutta/async/http/AsyncHttpRequest;)Z
    .locals 1
    .param p1, "request"    # Lcom/koushikdutta/async/http/AsyncHttpRequest;

    .prologue
    .line 106
    invoke-virtual {p1}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/koushikdutta/ion/BitmapFetcher;->fastLoad(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
