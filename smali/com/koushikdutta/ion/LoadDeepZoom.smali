.class public Lcom/koushikdutta/ion/LoadDeepZoom;
.super Lcom/koushikdutta/ion/LoadBitmapEmitter;
.source "LoadDeepZoom.java"

# interfaces
.implements Lcom/koushikdutta/async/future/FutureCallback;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xa
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/koushikdutta/ion/LoadBitmapEmitter;",
        "Lcom/koushikdutta/async/future/FutureCallback",
        "<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field fileCache:Lcom/koushikdutta/async/util/FileCache;


# direct methods
.method public constructor <init>(Lcom/koushikdutta/ion/Ion;Ljava/lang/String;ZLcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;Lcom/koushikdutta/async/util/FileCache;)V
    .locals 6
    .param p1, "ion"    # Lcom/koushikdutta/ion/Ion;
    .param p2, "urlKey"    # Ljava/lang/String;
    .param p3, "animateGif"    # Z
    .param p5, "fileCache"    # Lcom/koushikdutta/async/util/FileCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/koushikdutta/ion/Ion;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform",
            "<",
            "Ljava/io/File;",
            ">;",
            "Lcom/koushikdutta/async/util/FileCache;",
            ")V"
        }
    .end annotation

    .prologue
    .line 29
    .local p4, "emitterTransform":Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;, "Lcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform<Ljava/io/File;>;"
    const/4 v3, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/koushikdutta/ion/LoadBitmapEmitter;-><init>(Lcom/koushikdutta/ion/Ion;Ljava/lang/String;ZZLcom/koushikdutta/ion/IonRequestBuilder$EmitterTransform;)V

    .line 30
    iput-object p5, p0, Lcom/koushikdutta/ion/LoadDeepZoom;->fileCache:Lcom/koushikdutta/async/util/FileCache;

    .line 31
    return-void
.end method


# virtual methods
.method public onCompleted(Ljava/lang/Exception;Ljava/io/File;)V
    .locals 2
    .param p1, "e"    # Ljava/lang/Exception;
    .param p2, "tempFile"    # Ljava/io/File;

    .prologue
    .line 35
    if-eqz p1, :cond_1

    .line 36
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/koushikdutta/ion/LoadDeepZoom;->report(Ljava/lang/Exception;Lcom/koushikdutta/ion/bitmap/BitmapInfo;)V

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 40
    :cond_1
    iget-object v0, p0, Lcom/koushikdutta/ion/LoadDeepZoom;->ion:Lcom/koushikdutta/ion/Ion;

    iget-object v0, v0, Lcom/koushikdutta/ion/Ion;->bitmapsPending:Lcom/koushikdutta/async/util/HashList;

    iget-object v1, p0, Lcom/koushikdutta/ion/LoadDeepZoom;->key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/koushikdutta/async/util/HashList;->tag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 45
    invoke-static {}, Lcom/koushikdutta/ion/Ion;->getBitmapLoadExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/koushikdutta/ion/LoadDeepZoom$1;

    invoke-direct {v1, p0, p2}, Lcom/koushikdutta/ion/LoadDeepZoom$1;-><init>(Lcom/koushikdutta/ion/LoadDeepZoom;Ljava/io/File;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public bridge synthetic onCompleted(Ljava/lang/Exception;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Exception;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 25
    check-cast p2, Ljava/io/File;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/koushikdutta/ion/LoadDeepZoom;->onCompleted(Ljava/lang/Exception;Ljava/io/File;)V

    return-void
.end method
