.class public Lcom/google/android/gms/internal/ga;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/ey;
.end annotation


# static fields
.field private static final vX:Lcom/google/android/gms/internal/ga;

.field public static final vY:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mH:Ljava/lang/Object;

.field private nE:Lcom/google/android/gms/internal/am;

.field private nF:Lcom/google/android/gms/internal/al;

.field private nG:Lcom/google/android/gms/internal/ex;

.field private qJ:Lcom/google/android/gms/internal/gs;

.field private uV:Z

.field private uW:Z

.field public final vZ:Ljava/lang/String;

.field private final wa:Lcom/google/android/gms/internal/gb;

.field private wc:Ljava/math/BigInteger;

.field private final wd:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final we:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private wf:Z

.field private wg:Z

.field private wh:Lcom/google/android/gms/internal/an;

.field private wi:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field

.field private wj:Z

.field private wk:Landroid/os/Bundle;

.field private wl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/ga;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ga;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ga;->vX:Lcom/google/android/gms/internal/ga;

    sget-object v0, Lcom/google/android/gms/internal/ga;->vX:Lcom/google/android/gms/internal/ga;

    iget-object v0, v0, Lcom/google/android/gms/internal/ga;->vZ:Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/internal/ga;->vY:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/ga;->mH:Ljava/lang/Object;

    sget-object v0, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    iput-object v0, p0, Lcom/google/android/gms/internal/ga;->wc:Ljava/math/BigInteger;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/ga;->wd:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/ga;->we:Ljava/util/HashMap;

    iput-boolean v2, p0, Lcom/google/android/gms/internal/ga;->wf:Z

    iput-boolean v3, p0, Lcom/google/android/gms/internal/ga;->uV:Z

    iput-boolean v2, p0, Lcom/google/android/gms/internal/ga;->wg:Z

    iput-boolean v3, p0, Lcom/google/android/gms/internal/ga;->uW:Z

    iput-object v1, p0, Lcom/google/android/gms/internal/ga;->nE:Lcom/google/android/gms/internal/am;

    iput-object v1, p0, Lcom/google/android/gms/internal/ga;->wh:Lcom/google/android/gms/internal/an;

    iput-object v1, p0, Lcom/google/android/gms/internal/ga;->nF:Lcom/google/android/gms/internal/al;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/ga;->wi:Ljava/util/LinkedList;

    iput-boolean v2, p0, Lcom/google/android/gms/internal/ga;->wj:Z

    invoke-static {}, Lcom/google/android/gms/internal/bn;->by()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/ga;->wk:Landroid/os/Bundle;

    iput-object v1, p0, Lcom/google/android/gms/internal/ga;->nG:Lcom/google/android/gms/internal/ex;

    invoke-static {}, Lcom/google/android/gms/internal/gi;->dx()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/ga;->vZ:Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/internal/gb;

    iget-object v1, p0, Lcom/google/android/gms/internal/ga;->vZ:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/gb;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/ga;->wa:Lcom/google/android/gms/internal/gb;

    return-void
.end method

.method public static bN()Landroid/os/Bundle;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/ga;->vX:Lcom/google/android/gms/internal/ga;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ga;->dp()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public static c(ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/ga;->vX:Lcom/google/android/gms/internal/ga;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/gms/internal/ga;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static dn()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/ga;->vX:Lcom/google/android/gms/internal/ga;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ga;->do()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static e(Ljava/lang/Throwable;)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/ga;->vX:Lcom/google/android/gms/internal/ga;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/ga;->f(Ljava/lang/Throwable;)V

    return-void
.end method


# virtual methods
.method public d(ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ga;->qJ:Lcom/google/android/gms/internal/gs;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/gs;->wV:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/ga;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_1

    :goto_1
    return-object p2

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ga;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->getRemoteResource(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_1
.end method

.method public do()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/ga;->mH:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ga;->wl:Ljava/lang/String;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public dp()Landroid/os/Bundle;
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/ga;->mH:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ga;->wk:Landroid/os/Bundle;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public f(Ljava/lang/Throwable;)V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/google/android/gms/internal/ga;->wg:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/ex;

    iget-object v1, p0, Lcom/google/android/gms/internal/ga;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/internal/ga;->qJ:Lcom/google/android/gms/internal/gs;

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/google/android/gms/internal/ex;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/gs;Ljava/lang/Thread$UncaughtExceptionHandler;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/ex;->b(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method
