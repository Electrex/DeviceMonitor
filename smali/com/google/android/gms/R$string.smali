.class public final Lcom/google/android/gms/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final accept:I = 0x7f0e0013

.field public static final common_android_wear_notification_needs_update_text:I = 0x7f0e0049

.field public static final common_android_wear_update_text:I = 0x7f0e004a

.field public static final common_android_wear_update_title:I = 0x7f0e004b

.field public static final common_google_play_services_enable_button:I = 0x7f0e004c

.field public static final common_google_play_services_enable_text:I = 0x7f0e004d

.field public static final common_google_play_services_enable_title:I = 0x7f0e004e

.field public static final common_google_play_services_error_notification_requested_by_msg:I = 0x7f0e004f

.field public static final common_google_play_services_install_button:I = 0x7f0e0050

.field public static final common_google_play_services_install_text_phone:I = 0x7f0e0051

.field public static final common_google_play_services_install_text_tablet:I = 0x7f0e0052

.field public static final common_google_play_services_install_title:I = 0x7f0e0053

.field public static final common_google_play_services_invalid_account_text:I = 0x7f0e0054

.field public static final common_google_play_services_invalid_account_title:I = 0x7f0e0055

.field public static final common_google_play_services_needs_enabling_title:I = 0x7f0e0056

.field public static final common_google_play_services_network_error_text:I = 0x7f0e0057

.field public static final common_google_play_services_network_error_title:I = 0x7f0e0058

.field public static final common_google_play_services_notification_needs_installation_title:I = 0x7f0e0059

.field public static final common_google_play_services_notification_needs_update_title:I = 0x7f0e005a

.field public static final common_google_play_services_notification_ticker:I = 0x7f0e005b

.field public static final common_google_play_services_unknown_issue:I = 0x7f0e005c

.field public static final common_google_play_services_unsupported_text:I = 0x7f0e005d

.field public static final common_google_play_services_unsupported_title:I = 0x7f0e005e

.field public static final common_google_play_services_update_button:I = 0x7f0e005f

.field public static final common_google_play_services_update_text:I = 0x7f0e0060

.field public static final common_google_play_services_update_title:I = 0x7f0e0061

.field public static final common_open_on_phone:I = 0x7f0e0062

.field public static final common_signin_button_text:I = 0x7f0e0063

.field public static final common_signin_button_text_long:I = 0x7f0e0064

.field public static final create_calendar_message:I = 0x7f0e007e

.field public static final create_calendar_title:I = 0x7f0e007f

.field public static final decline:I = 0x7f0e0088

.field public static final store_picture_message:I = 0x7f0e01fe

.field public static final store_picture_title:I = 0x7f0e01ff

.field public static final wallet_buy_button_place_holder:I = 0x7f0e023e
