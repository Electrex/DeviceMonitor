.class public Lorg/namelessrom/devicecontrol/objects/KernelInfo;
.super Ljava/lang/Object;
.source "KernelInfo.java"


# instance fields
.field public date:Ljava/lang/String;

.field public extras:Ljava/lang/String;

.field public gcc:Ljava/lang/String;

.field public host:Ljava/lang/String;

.field public revision:Ljava/lang/String;

.field public version:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->version:Ljava/lang/String;

    .line 42
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->host:Ljava/lang/String;

    .line 43
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->gcc:Ljava/lang/String;

    .line 44
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->revision:Ljava/lang/String;

    .line 45
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->extras:Ljava/lang/String;

    .line 46
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->date:Ljava/lang/String;

    .line 47
    return-void
.end method


# virtual methods
.method public feedWithInformation()Z
    .locals 8

    .prologue
    const/4 v7, 0x6

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 55
    const-string v5, "/proc/version"

    invoke-static {v5}, Lorg/namelessrom/devicecontrol/utils/Utils;->readFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 56
    .local v2, "rawKernelVersion":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 102
    :goto_0
    return v3

    .line 59
    :cond_0
    const-string v5, "\n"

    const-string v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 66
    const-string v0, "Linux version (\\S+) \\((\\S+?)\\) (\\(gcc.+? \\)) (#\\d+) (.*?)?((Sun|Mon|Tue|Wed|Thu|Fri|Sat).+)"

    .line 74
    .local v0, "PROC_VERSION_REGEX":Ljava/lang/String;
    const-string v5, "Linux version (\\S+) \\((\\S+?)\\) (\\(gcc.+? \\)) (#\\d+) (.*?)?((Sun|Mon|Tue|Wed|Thu|Fri|Sat).+)"

    invoke-static {v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 75
    .local v1, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v5

    if-nez v5, :cond_1

    .line 76
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Regex did not match on /proc/version: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 78
    :cond_1
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v5

    if-ge v5, v7, :cond_2

    .line 79
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Regex match on /proc/version only returned "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " groups"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 84
    :cond_2
    invoke-virtual {v1, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->version:Ljava/lang/String;

    .line 85
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->version:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->version:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->version:Ljava/lang/String;

    .line 87
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->host:Ljava/lang/String;

    .line 88
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->host:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->host:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->host:Ljava/lang/String;

    .line 90
    :cond_4
    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->gcc:Ljava/lang/String;

    .line 91
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->gcc:Ljava/lang/String;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->gcc:Ljava/lang/String;

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->gcc:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->gcc:Ljava/lang/String;

    .line 93
    :cond_5
    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->revision:Ljava/lang/String;

    .line 94
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->revision:Ljava/lang/String;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->revision:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->revision:Ljava/lang/String;

    .line 96
    :cond_6
    const/4 v3, 0x5

    invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->extras:Ljava/lang/String;

    .line 97
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->extras:Ljava/lang/String;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->extras:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->extras:Ljava/lang/String;

    .line 99
    :cond_7
    invoke-virtual {v1, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->date:Ljava/lang/String;

    .line 100
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->date:Ljava/lang/String;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->date:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->date:Ljava/lang/String;

    :cond_8
    move v3, v4

    .line 102
    goto/16 :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 50
    const-string v0, "version: %s, host: %s, gcc: %s, revision: %s, extras: %s, date: %s"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->version:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->host:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->gcc:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->revision:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->extras:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->date:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
