.class public Lorg/namelessrom/devicecontrol/objects/CpuCore;
.super Ljava/lang/Object;
.source "CpuCore.java"


# instance fields
.field public final mCore:Ljava/lang/String;

.field public final mCoreCurrent:I

.field public final mCoreGov:Ljava/lang/String;

.field public final mCoreMax:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "core"    # Ljava/lang/String;
    .param p2, "coreCurrent"    # Ljava/lang/String;
    .param p3, "coreMax"    # Ljava/lang/String;
    .param p4, "coreGov"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .end local p1    # "core":Ljava/lang/String;
    :goto_0
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/objects/CpuCore;->mCore:Ljava/lang/String;

    .line 32
    invoke-static {p3, v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->tryParse(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lorg/namelessrom/devicecontrol/objects/CpuCore;->mCoreMax:I

    .line 33
    invoke-static {p2, v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->tryParse(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lorg/namelessrom/devicecontrol/objects/CpuCore;->mCoreCurrent:I

    .line 34
    if-eqz p4, :cond_1

    invoke-virtual {p4}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .end local p4    # "coreGov":Ljava/lang/String;
    :goto_1
    iput-object p4, p0, Lorg/namelessrom/devicecontrol/objects/CpuCore;->mCoreGov:Ljava/lang/String;

    .line 35
    return-void

    .line 31
    .restart local p1    # "core":Ljava/lang/String;
    .restart local p4    # "coreGov":Ljava/lang/String;
    :cond_0
    const-string p1, "0"

    goto :goto_0

    .line 34
    .end local p1    # "core":Ljava/lang/String;
    :cond_1
    const-string p4, "0"

    goto :goto_1
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 38
    const-string v0, "core: %s | max: %s | current: %s | gov: %s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/CpuCore;->mCore:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lorg/namelessrom/devicecontrol/objects/CpuCore;->mCoreMax:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lorg/namelessrom/devicecontrol/objects/CpuCore;->mCoreCurrent:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/CpuCore;->mCoreGov:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
