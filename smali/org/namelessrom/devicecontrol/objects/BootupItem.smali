.class public Lorg/namelessrom/devicecontrol/objects/BootupItem;
.super Ljava/lang/Object;
.source "BootupItem.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/namelessrom/devicecontrol/objects/BootupItem;",
        ">;"
    }
.end annotation


# instance fields
.field public category:Ljava/lang/String;

.field public enabled:Z

.field public filename:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "category"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "filename"    # Ljava/lang/String;
    .param p4, "value"    # Ljava/lang/String;
    .param p5, "enabled"    # Z

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/objects/BootupItem;->category:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/objects/BootupItem;->name:Ljava/lang/String;

    .line 35
    iput-object p4, p0, Lorg/namelessrom/devicecontrol/objects/BootupItem;->value:Ljava/lang/String;

    .line 36
    iput-object p3, p0, Lorg/namelessrom/devicecontrol/objects/BootupItem;->filename:Ljava/lang/String;

    .line 37
    iput-boolean p5, p0, Lorg/namelessrom/devicecontrol/objects/BootupItem;->enabled:Z

    .line 38
    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 22
    check-cast p1, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/objects/BootupItem;->compareTo(Lorg/namelessrom/devicecontrol/objects/BootupItem;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/namelessrom/devicecontrol/objects/BootupItem;)I
    .locals 3
    .param p1, "another"    # Lorg/namelessrom/devicecontrol/objects/BootupItem;

    .prologue
    .line 58
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/objects/BootupItem;->category:Ljava/lang/String;

    iget-object v2, p1, Lorg/namelessrom/devicecontrol/objects/BootupItem;->category:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    .line 59
    .local v0, "i":I
    if-eqz v0, :cond_0

    .line 61
    .end local v0    # "i":I
    :goto_0
    return v0

    .restart local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/objects/BootupItem;->name:Ljava/lang/String;

    iget-object v2, p1, Lorg/namelessrom/devicecontrol/objects/BootupItem;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "otherItem"    # Ljava/lang/Object;

    .prologue
    .line 41
    instance-of v0, p1, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/objects/BootupItem;->category:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;->category:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/objects/BootupItem;->name:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/objects/BootupItem;->filename:Ljava/lang/String;

    check-cast p1, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    .end local p1    # "otherItem":Ljava/lang/Object;
    iget-object v1, p1, Lorg/namelessrom/devicecontrol/objects/BootupItem;->filename:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 51
    const-string v0, "category: %s | name: %s | value: %s | filename: %s | enabled: %s"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/BootupItem;->category:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/BootupItem;->name:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/BootupItem;->value:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/BootupItem;->filename:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-boolean v3, p0, Lorg/namelessrom/devicecontrol/objects/BootupItem;->enabled:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
