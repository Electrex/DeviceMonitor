.class public Lorg/namelessrom/devicecontrol/objects/MemoryInfo;
.super Ljava/lang/Object;
.source "MemoryInfo.java"


# static fields
.field public static memoryCached:J

.field public static memoryFree:J

.field public static memoryTotal:J

.field private static sInstance:Lorg/namelessrom/devicecontrol/objects/MemoryInfo;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private checkMemory(Ljava/lang/String;)J
    .locals 2
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 94
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 95
    const-string v0, "kB"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 97
    const-string v0, "MemTotal:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    const-string v0, "MemTotal:"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 99
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    sput-wide v0, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryTotal:J

    .line 100
    sget-wide v0, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryTotal:J

    .line 112
    :goto_0
    return-wide v0

    .line 101
    :cond_0
    const-string v0, "MemFree:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 102
    const-string v0, "MemFree:"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 103
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    sput-wide v0, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryFree:J

    .line 104
    sget-wide v0, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryFree:J

    goto :goto_0

    .line 105
    :cond_1
    const-string v0, "Cached:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 106
    const-string v0, "Cached:"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 107
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    sput-wide v0, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryCached:J

    .line 108
    sget-wide v0, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryCached:J

    goto :goto_0

    .line 112
    :cond_2
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public static getInstance()Lorg/namelessrom/devicecontrol/objects/MemoryInfo;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->sInstance:Lorg/namelessrom/devicecontrol/objects/MemoryInfo;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->sInstance:Lorg/namelessrom/devicecontrol/objects/MemoryInfo;

    .line 45
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->sInstance:Lorg/namelessrom/devicecontrol/objects/MemoryInfo;

    return-object v0
.end method

.method private parseLong(Ljava/lang/String;)J
    .locals 4
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 117
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 119
    :goto_0
    return-wide v2

    .line 118
    :catch_0
    move-exception v0

    .line 119
    .local v0, "exc":Ljava/lang/Exception;
    const-wide/16 v2, -0x1

    goto :goto_0
.end method


# virtual methods
.method public readMemory(I)[J
    .locals 12
    .param p1, "type"    # I

    .prologue
    const-wide/16 v10, 0x400

    const-wide/16 v8, 0x0

    .line 58
    const-string v6, "/proc/meminfo"

    invoke-static {v6}, Lorg/namelessrom/devicecontrol/utils/Utils;->readFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 59
    .local v2, "input":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 60
    const-string v6, "\n"

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 61
    .local v4, "parts":[Ljava/lang/String;
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v5, v0, v1

    .local v5, "s":Ljava/lang/String;
    invoke-direct {p0, v5}, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->checkMemory(Ljava/lang/String;)J

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 63
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "parts":[Ljava/lang/String;
    .end local v5    # "s":Ljava/lang/String;
    :cond_0
    sput-wide v8, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryTotal:J

    .line 64
    sput-wide v8, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryFree:J

    .line 65
    sput-wide v8, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryCached:J

    .line 69
    :cond_1
    sget-wide v6, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryTotal:J

    cmp-long v6, v6, v8

    if-gez v6, :cond_2

    sput-wide v8, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryTotal:J

    .line 70
    :cond_2
    sget-wide v6, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryFree:J

    cmp-long v6, v6, v8

    if-gez v6, :cond_3

    sput-wide v8, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryFree:J

    .line 71
    :cond_3
    sget-wide v6, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryCached:J

    cmp-long v6, v6, v8

    if-gez v6, :cond_4

    sput-wide v8, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryCached:J

    .line 74
    :cond_4
    packed-switch p1, :pswitch_data_0

    .line 90
    :goto_1
    :pswitch_0
    const/4 v6, 0x3

    new-array v6, v6, [J

    const/4 v7, 0x0

    sget-wide v8, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryTotal:J

    aput-wide v8, v6, v7

    const/4 v7, 0x1

    sget-wide v8, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryFree:J

    aput-wide v8, v6, v7

    const/4 v7, 0x2

    sget-wide v8, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryCached:J

    aput-wide v8, v6, v7

    return-object v6

    .line 79
    :pswitch_1
    sget-wide v6, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryTotal:J

    mul-long/2addr v6, v10

    sput-wide v6, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryTotal:J

    .line 80
    sget-wide v6, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryFree:J

    mul-long/2addr v6, v10

    sput-wide v6, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryFree:J

    .line 81
    sget-wide v6, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryCached:J

    mul-long/2addr v6, v10

    sput-wide v6, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryCached:J

    goto :goto_1

    .line 84
    :pswitch_2
    sget-wide v6, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryTotal:J

    div-long/2addr v6, v10

    sput-wide v6, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryTotal:J

    .line 85
    sget-wide v6, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryFree:J

    div-long/2addr v6, v10

    sput-wide v6, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryFree:J

    .line 86
    sget-wide v6, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryCached:J

    div-long/2addr v6, v10

    sput-wide v6, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryCached:J

    goto :goto_1

    .line 74
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 49
    const-string v0, "memoryTotal: %s, memoryFree: %s, memoryCached: %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-wide v4, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryTotal:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-wide v4, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryFree:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-wide v4, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->memoryCached:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
