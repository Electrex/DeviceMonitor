.class public Lorg/namelessrom/devicecontrol/objects/CpuInfo;
.super Ljava/lang/Object;
.source "CpuInfo.java"


# instance fields
.field public bogomips:Ljava/lang/String;

.field public features:Ljava/lang/String;

.field public hardware:Ljava/lang/String;

.field public processor:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->processor:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->bogomips:Ljava/lang/String;

    .line 38
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->features:Ljava/lang/String;

    .line 39
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->hardware:Ljava/lang/String;

    .line 40
    return-void
.end method

.method private getData(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 70
    if-nez p1, :cond_0

    const-string v1, ""

    .line 75
    :goto_0
    return-object v1

    .line 72
    :cond_0
    const-string v1, ":"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "splitted":[Ljava/lang/String;
    array-length v1, v0

    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    const-string v1, ""

    goto :goto_0

    .line 75
    :cond_1
    aget-object v1, v0, v3

    if-eqz v1, :cond_2

    aget-object v1, v0, v3

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    const-string v1, ""

    goto :goto_0
.end method


# virtual methods
.method public feedWithInformation()Z
    .locals 7

    .prologue
    .line 48
    const-string v6, "/proc/cpuinfo"

    invoke-static {v6}, Lorg/namelessrom/devicecontrol/utils/Utils;->readFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 49
    .local v1, "cpuinfo":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v6, 0x0

    .line 66
    :goto_0
    return v6

    .line 51
    :cond_0
    const-string v6, "\n"

    invoke-virtual {v1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 53
    .local v4, "list":[Ljava/lang/String;
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_5

    aget-object v5, v0, v2

    .line 54
    .local v5, "s":Ljava/lang/String;
    invoke-static {p0, v5}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    const-string v6, "Processor"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 56
    invoke-direct {p0, v5}, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->getData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->processor:Ljava/lang/String;

    .line 53
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 57
    :cond_2
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->bogomips:Ljava/lang/String;

    if-nez v6, :cond_3

    const-string v6, "BogoMIPS"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 58
    invoke-direct {p0, v5}, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->getData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->bogomips:Ljava/lang/String;

    goto :goto_2

    .line 59
    :cond_3
    const-string v6, "Features"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 60
    invoke-direct {p0, v5}, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->getData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->features:Ljava/lang/String;

    goto :goto_2

    .line 61
    :cond_4
    const-string v6, "Hardware"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 62
    invoke-direct {p0, v5}, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->getData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->hardware:Ljava/lang/String;

    goto :goto_2

    .line 66
    .end local v5    # "s":Ljava/lang/String;
    :cond_5
    const/4 v6, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 43
    const-string v0, "processor: %s, bogomips: %s, features: %s, hardware: %s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->processor:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->bogomips:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->features:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->hardware:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
