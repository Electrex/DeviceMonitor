.class public Lorg/namelessrom/devicecontrol/objects/PackageStatsObserver;
.super Landroid/content/pm/IPackageStatsObserver$Stub;
.source "PackageStatsObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/objects/PackageStatsObserver$OnPackageStatsListener;
    }
.end annotation


# instance fields
.field private packageStatsListener:Lorg/namelessrom/devicecontrol/objects/PackageStatsObserver$OnPackageStatsListener;


# direct methods
.method public constructor <init>(Lorg/namelessrom/devicecontrol/objects/PackageStatsObserver$OnPackageStatsListener;)V
    .locals 0
    .param p1, "listener"    # Lorg/namelessrom/devicecontrol/objects/PackageStatsObserver$OnPackageStatsListener;

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/content/pm/IPackageStatsObserver$Stub;-><init>()V

    .line 24
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/objects/PackageStatsObserver;->packageStatsListener:Lorg/namelessrom/devicecontrol/objects/PackageStatsObserver$OnPackageStatsListener;

    .line 25
    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/objects/PackageStatsObserver;)Lorg/namelessrom/devicecontrol/objects/PackageStatsObserver$OnPackageStatsListener;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/objects/PackageStatsObserver;

    .prologue
    .line 17
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/objects/PackageStatsObserver;->packageStatsListener:Lorg/namelessrom/devicecontrol/objects/PackageStatsObserver$OnPackageStatsListener;

    return-object v0
.end method


# virtual methods
.method public onGetStatsCompleted(Landroid/content/pm/PackageStats;Z)V
    .locals 4
    .param p1, "pStats"    # Landroid/content/pm/PackageStats;
    .param p2, "success"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 52
    const-string v0, "onGetStatsCompleted(): %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 53
    sget-object v0, Lorg/namelessrom/devicecontrol/Application;->HANDLER:Landroid/os/Handler;

    new-instance v1, Lorg/namelessrom/devicecontrol/objects/PackageStatsObserver$1;

    invoke-direct {v1, p0, p1}, Lorg/namelessrom/devicecontrol/objects/PackageStatsObserver$1;-><init>(Lorg/namelessrom/devicecontrol/objects/PackageStatsObserver;Landroid/content/pm/PackageStats;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 59
    return-void
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 29
    sparse-switch p1, :sswitch_data_0

    .line 47
    :goto_0
    return v2

    .line 31
    :sswitch_0
    const-string v3, "android.content.pm.IPackageStatsObserver"

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 35
    :sswitch_1
    const-string v3, "android.content.pm.IPackageStatsObserver"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 37
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 38
    sget-object v3, Landroid/content/pm/PackageStats;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageStats;

    .line 42
    .local v0, "_arg0":Landroid/content/pm/PackageStats;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1

    move v1, v2

    .line 43
    .local v1, "_arg1":Z
    :goto_2
    invoke-virtual {p0, v0, v1}, Lorg/namelessrom/devicecontrol/objects/PackageStatsObserver;->onGetStatsCompleted(Landroid/content/pm/PackageStats;Z)V

    goto :goto_0

    .line 40
    .end local v0    # "_arg0":Landroid/content/pm/PackageStats;
    .end local v1    # "_arg1":Z
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/content/pm/PackageStats;
    goto :goto_1

    .line 42
    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    .line 29
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
