.class public Lorg/namelessrom/devicecontrol/objects/Prop;
.super Ljava/lang/Object;
.source "Prop.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/namelessrom/devicecontrol/objects/Prop;",
        ">;"
    }
.end annotation


# instance fields
.field private mData:Ljava/lang/String;

.field private mName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "n"    # Ljava/lang/String;
    .param p2, "d"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/objects/Prop;->mName:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/objects/Prop;->mData:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 23
    check-cast p1, Lorg/namelessrom/devicecontrol/objects/Prop;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/objects/Prop;->compareTo(Lorg/namelessrom/devicecontrol/objects/Prop;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/namelessrom/devicecontrol/objects/Prop;)I
    .locals 2
    .param p1, "o"    # Lorg/namelessrom/devicecontrol/objects/Prop;

    .prologue
    .line 42
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/objects/Prop;->mName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/objects/Prop;->mName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/objects/Prop;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0

    .line 45
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/objects/Prop;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getVal()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/objects/Prop;->mData:Ljava/lang/String;

    return-object v0
.end method

.method public setVal(Ljava/lang/String;)V
    .locals 0
    .param p1, "d"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/objects/Prop;->mData:Ljava/lang/String;

    return-void
.end method
