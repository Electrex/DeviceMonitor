.class public Lorg/namelessrom/devicecontrol/objects/AppItem;
.super Ljava/lang/Object;
.source "AppItem.java"


# instance fields
.field private final appInfo:Landroid/content/pm/ApplicationInfo;

.field private enabled:Z

.field private final icon:Landroid/graphics/drawable/Drawable;

.field private final label:Ljava/lang/String;

.field private final pkgInfo:Landroid/content/pm/PackageInfo;


# direct methods
.method public constructor <init>(Landroid/content/pm/PackageInfo;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1, "info"    # Landroid/content/pm/PackageInfo;
    .param p2, "label"    # Ljava/lang/String;
    .param p3, "icon"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/objects/AppItem;->enabled:Z

    .line 35
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/objects/AppItem;->pkgInfo:Landroid/content/pm/PackageInfo;

    .line 36
    iget-object v1, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/objects/AppItem;->appInfo:Landroid/content/pm/ApplicationInfo;

    .line 38
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/objects/AppItem;->label:Ljava/lang/String;

    .line 39
    iput-object p3, p0, Lorg/namelessrom/devicecontrol/objects/AppItem;->icon:Landroid/graphics/drawable/Drawable;

    .line 41
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/objects/AppItem;->appInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/objects/AppItem;->appInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v1, v1, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/objects/AppItem;->enabled:Z

    .line 42
    return-void
.end method


# virtual methods
.method public getApplicationInfo()Landroid/content/pm/ApplicationInfo;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/objects/AppItem;->appInfo:Landroid/content/pm/ApplicationInfo;

    return-object v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/objects/AppItem;->icon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/objects/AppItem;->label:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageInfo()Landroid/content/pm/PackageInfo;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/objects/AppItem;->pkgInfo:Landroid/content/pm/PackageInfo;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/objects/AppItem;->pkgInfo:Landroid/content/pm/PackageInfo;

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/objects/AppItem;->enabled:Z

    return v0
.end method

.method public isSystemApp()Z
    .locals 2

    .prologue
    .line 55
    const/16 v0, 0x81

    .line 56
    .local v0, "mask":I
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/objects/AppItem;->appInfo:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v1, v1, 0x81

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lorg/namelessrom/devicecontrol/objects/AppItem;->enabled:Z

    return-void
.end method
