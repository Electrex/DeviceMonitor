.class public Lorg/namelessrom/devicecontrol/about/AboutFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment;
.source "AboutFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 30
    const v0, 0x7f0e0010

    return v0
.end method

.method public getPagerAdapter()Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment$ViewPagerAdapter;
    .locals 4

    .prologue
    const/4 v2, 0x3

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 34
    .local v0, "fragments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/v4/app/Fragment;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 36
    .local v1, "titles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/CharSequence;>;"
    const v2, 0x7f0e0244

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/about/AboutFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    new-instance v2, Lorg/namelessrom/devicecontrol/about/WelcomeFragment;

    invoke-direct {v2}, Lorg/namelessrom/devicecontrol/about/WelcomeFragment;-><init>()V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    const v2, 0x7f0e0153

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/about/AboutFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    new-instance v2, Lorg/namelessrom/devicecontrol/about/LicenseFragment;

    invoke-direct {v2}, Lorg/namelessrom/devicecontrol/about/LicenseFragment;-><init>()V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    const v2, 0x7f0e01a3

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/about/AboutFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    new-instance v2, Lorg/namelessrom/devicecontrol/about/PrivacyFragment;

    invoke-direct {v2}, Lorg/namelessrom/devicecontrol/about/PrivacyFragment;-><init>()V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    new-instance v2, Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment$ViewPagerAdapter;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/about/AboutFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-direct {v2, p0, v3, v0, v1}, Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment$ViewPagerAdapter;-><init>(Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment;Landroid/support/v4/app/FragmentManager;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-object v2
.end method
