.class public Lorg/namelessrom/devicecontrol/about/WelcomeFragment;
.super Landroid/support/v4/app/Fragment;
.source "WelcomeFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 38
    const v5, 0x7f04002a

    invoke-virtual {p1, v5, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 40
    .local v4, "view":Landroid/view/View;
    const v5, 0x7f0c0092

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 41
    .local v0, "details":Landroid/widget/TextView;
    const v5, 0x7f0e0245

    new-array v6, v10, [Ljava/lang/Object;

    const v7, 0x7f0e0026

    invoke-virtual {p0, v7}, Lorg/namelessrom/devicecontrol/about/WelcomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p0, v5, v6}, Lorg/namelessrom/devicecontrol/about/WelcomeFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/about/WelcomeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f02007e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 44
    .local v3, "translateDrawable":Landroid/graphics/drawable/Drawable;
    const v5, 0x7f0c0093

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 45
    .local v2, "translateButton":Landroid/widget/Button;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/utils/DrawableHelper;->applyAccentColorFilter(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v2, v5, v9, v9, v9}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 47
    new-instance v5, Lorg/namelessrom/devicecontrol/about/WelcomeFragment$1;

    invoke-direct {v5, p0}, Lorg/namelessrom/devicecontrol/about/WelcomeFragment$1;-><init>(Lorg/namelessrom/devicecontrol/about/WelcomeFragment;)V

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    const v5, 0x7f0c0094

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 54
    .local v1, "donateButton":Landroid/widget/Button;
    const-string v5, "%s %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const v7, 0x7f0e00a3

    invoke-virtual {p0, v7}, Lorg/namelessrom/devicecontrol/about/WelcomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    const v7, 0x7f0e0132

    invoke-virtual {p0, v7}, Lorg/namelessrom/devicecontrol/about/WelcomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 56
    new-instance v5, Lorg/namelessrom/devicecontrol/about/WelcomeFragment$2;

    invoke-direct {v5, p0}, Lorg/namelessrom/devicecontrol/about/WelcomeFragment$2;-><init>(Lorg/namelessrom/devicecontrol/about/WelcomeFragment;)V

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    return-object v4
.end method
