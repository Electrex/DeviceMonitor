.class public Lorg/namelessrom/devicecontrol/DummyLauncher;
.super Lorg/namelessrom/devicecontrol/activities/BaseActivity;
.source "DummyLauncher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;
    }
.end annotation


# instance fields
.field private mAction:Landroid/widget/Button;

.field private final mHandler:Landroid/os/Handler;

.field private mLauncher:Landroid/widget/LinearLayout;

.field private mProgressLayout:Landroid/widget/RelativeLayout;

.field private mProgressStatus:Landroid/widget/TextView;

.field private mStatus:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;-><init>()V

    .line 51
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/DummyLauncher;->mHandler:Landroid/os/Handler;

    .line 102
    return-void
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/DummyLauncher;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/DummyLauncher;

    .prologue
    .line 42
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/DummyLauncher;->mProgressStatus:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lorg/namelessrom/devicecontrol/DummyLauncher;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/DummyLauncher;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/DummyLauncher;->updateStatus(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lorg/namelessrom/devicecontrol/DummyLauncher;)V
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/DummyLauncher;

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/DummyLauncher;->startActivity()V

    return-void
.end method

.method static synthetic access$400(Lorg/namelessrom/devicecontrol/DummyLauncher;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/DummyLauncher;

    .prologue
    .line 42
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/DummyLauncher;->mProgressLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$500(Lorg/namelessrom/devicecontrol/DummyLauncher;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/DummyLauncher;

    .prologue
    .line 42
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/DummyLauncher;->mLauncher:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$600(Lorg/namelessrom/devicecontrol/DummyLauncher;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/DummyLauncher;

    .prologue
    .line 42
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/DummyLauncher;->mStatus:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lorg/namelessrom/devicecontrol/DummyLauncher;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/DummyLauncher;

    .prologue
    .line 42
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/DummyLauncher;->mAction:Landroid/widget/Button;

    return-object v0
.end method

.method private startActivity()V
    .locals 2

    .prologue
    .line 97
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lorg/namelessrom/devicecontrol/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/DummyLauncher;->startActivity(Landroid/content/Intent;)V

    .line 99
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/DummyLauncher;->finish()V

    .line 100
    return-void
.end method

.method private updateStatus(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 88
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/DummyLauncher;->mHandler:Landroid/os/Handler;

    new-instance v1, Lorg/namelessrom/devicecontrol/DummyLauncher$3;

    invoke-direct {v1, p0, p1}, Lorg/namelessrom/devicecontrol/DummyLauncher$3;-><init>(Lorg/namelessrom/devicecontrol/DummyLauncher;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 94
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 54
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    const v0, 0x7f04001a

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/DummyLauncher;->setContentView(I)V

    .line 57
    const v0, 0x7f0c006e

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/DummyLauncher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/DummyLauncher;->mProgressLayout:Landroid/widget/RelativeLayout;

    .line 58
    const v0, 0x7f0c006f

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/DummyLauncher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/DummyLauncher;->mProgressStatus:Landroid/widget/TextView;

    .line 60
    const v0, 0x7f0c0070

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/DummyLauncher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/DummyLauncher;->mLauncher:Landroid/widget/LinearLayout;

    .line 61
    const v0, 0x7f0c0071

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/DummyLauncher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/DummyLauncher;->mStatus:Landroid/widget/TextView;

    .line 62
    const v0, 0x7f0c00b4

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/DummyLauncher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/DummyLauncher;->mAction:Landroid/widget/Button;

    .line 63
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/DummyLauncher;->mAction:Landroid/widget/Button;

    new-instance v1, Lorg/namelessrom/devicecontrol/DummyLauncher$1;

    invoke-direct {v1, p0}, Lorg/namelessrom/devicecontrol/DummyLauncher$1;-><init>(Lorg/namelessrom/devicecontrol/DummyLauncher;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    const v0, 0x7f0c00b5

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/DummyLauncher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lorg/namelessrom/devicecontrol/DummyLauncher$2;

    invoke-direct {v1, p0}, Lorg/namelessrom/devicecontrol/DummyLauncher$2;-><init>(Lorg/namelessrom/devicecontrol/DummyLauncher;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    iget-boolean v0, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->skipChecks:Z

    if-eqz v0, :cond_0

    .line 81
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/DummyLauncher;->startActivity()V

    .line 85
    :goto_0
    return-void

    .line 83
    :cond_0
    new-instance v0, Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;-><init>(Lorg/namelessrom/devicecontrol/DummyLauncher;Lorg/namelessrom/devicecontrol/DummyLauncher$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
