.class public Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;
.source "GovernorFragment.java"

# interfaces
.implements Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$GovernorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$1;,
        Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;
    }
.end annotation


# instance fields
.field private mCategory:Landroid/preference/PreferenceCategory;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;-><init>()V

    .line 95
    return-void
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;)Landroid/preference/PreferenceCategory;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->mCategory:Landroid/preference/PreferenceCategory;

    return-object v0
.end method

.method static synthetic access$200(Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Landroid/preference/Preference;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Landroid/preference/Preference;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-static {p0, p1}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->updateBootupListDb(Landroid/preference/Preference;Ljava/lang/String;)V

    return-void
.end method

.method private static updateBootupListDb(Landroid/preference/Preference;Ljava/lang/String;)V
    .locals 2
    .param p0, "p"    # Landroid/preference/Preference;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 185
    new-instance v0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$1updateListDb;

    invoke-direct {v0, p0, p1}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$1updateListDb;-><init>(Landroid/preference/Preference;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$1updateListDb;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 186
    return-void
.end method


# virtual methods
.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 56
    const v0, 0x7f0e0465

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 59
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 60
    const v0, 0x7f06000f

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->addPreferencesFromResource(I)V

    .line 62
    const-string v0, "key_gov_category"

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->mCategory:Landroid/preference/PreferenceCategory;

    .line 63
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->mContext:Landroid/content/Context;

    .line 65
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->getGovernor(Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$GovernorListener;)V

    .line 66
    return-void
.end method

.method public onGovernor(Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$Governor;)V
    .locals 6
    .param p1, "governor"    # Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$Governor;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 69
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/sys/devices/system/cpu/cpufreq/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$Governor;->current:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->mCategory:Landroid/preference/PreferenceCategory;

    const v1, 0x7f0e011f

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p1, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$Governor;->current:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 71
    new-instance v0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;-><init>(Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$1;)V

    new-array v1, v5, [Ljava/lang/String;

    iget-object v2, p1, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$Governor;->current:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 76
    :goto_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f0e0173

    invoke-virtual {p0, v0, v1, v2}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->isSupported(Landroid/preference/PreferenceScreen;Landroid/content/Context;I)V

    .line 77
    return-void

    .line 73
    :cond_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 80
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 81
    .local v1, "id":I
    packed-switch v1, :pswitch_data_0

    .line 92
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 83
    :pswitch_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 84
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 85
    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    .line 87
    :cond_0
    const/4 v2, 0x1

    goto :goto_0

    .line 81
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
