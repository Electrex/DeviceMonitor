.class Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$1updateListDb;
.super Landroid/os/AsyncTask;
.source "GovernorFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->updateBootupListDb(Landroid/preference/Preference;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "updateListDb"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$p:Landroid/preference/Preference;

.field final synthetic val$value:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/preference/Preference;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 173
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$1updateListDb;->val$p:Landroid/preference/Preference;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$1updateListDb;->val$value:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 173
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$1updateListDb;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 7
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 176
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$1updateListDb;->val$p:Landroid/preference/Preference;

    invoke-virtual {v0}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 177
    .local v2, "name":Ljava/lang/String;
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$1updateListDb;->val$p:Landroid/preference/Preference;

    invoke-virtual {v0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    .line 178
    .local v3, "key":Ljava/lang/String;
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v6

    new-instance v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    const-string v1, "cpu"

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$1updateListDb;->val$value:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/objects/BootupItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v6, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->setBootup(Landroid/content/Context;Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 181
    const/4 v0, 0x0

    return-object v0
.end method
