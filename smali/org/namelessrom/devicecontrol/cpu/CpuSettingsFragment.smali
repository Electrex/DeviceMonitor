.class public Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;
.source "CpuSettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lorg/namelessrom/devicecontrol/cpu/CpuUtils$CoreListener;
.implements Lorg/namelessrom/devicecontrol/cpu/CpuUtils$FrequencyListener;
.implements Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$GovernorListener;
.implements Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;


# instance fields
.field private mCpuGovLock:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

.field private mCpuInfo:Landroid/widget/LinearLayout;

.field private mCpuLock:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

.field private mCpuQuietGov:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

.field private mGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

.field private mGovernorTuning:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

.field private mMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

.field private mMpDecision:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

.field private mStatusHide:Landroid/support/v7/widget/SwitchCompat;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuInfo:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private setupHotpluggingPreferences()V
    .locals 26

    .prologue
    .line 154
    const-string v2, "mpdecision"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMpDecision:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    .line 155
    const-string v2, "/system/bin/mpdecision"

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 156
    const/16 v2, 0xc8

    const-string v3, "pgrep mpdecision 2> /dev/null;"

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Lorg/namelessrom/devicecontrol/utils/Utils;->getCommandResult(Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;ILjava/lang/String;)V

    .line 164
    :goto_0
    const-string v2, "intelli_plug"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    check-cast v8, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;

    .line 165
    .local v8, "awCategory":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;
    invoke-virtual {v8}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->isSupported()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 166
    invoke-virtual {v8}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 169
    .local v6, "path":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "intelli_plug_active"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 170
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string v3, "intelli_plug_"

    const-string v4, ""

    const-string v5, "extras"

    const-string v7, "intelli_plug_active"

    move-object/from16 v9, p0

    invoke-static/range {v2 .. v9}, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->addAwesomeTogglePreference(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/preference/PreferenceCategory;Landroid/preference/Preference$OnPreferenceChangeListener;)Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    move-result-object v24

    .line 173
    .local v24, "togglePref":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    if-eqz v24, :cond_0

    .line 174
    invoke-virtual/range {v24 .. v24}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setupTitle()V

    .line 178
    .end local v24    # "togglePref":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "touch_boost_active"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 179
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string v3, "intelli_plug_"

    const-string v4, ""

    const-string v5, "extras"

    const-string v7, "touch_boost_active"

    move-object/from16 v9, p0

    invoke-static/range {v2 .. v9}, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->addAwesomeTogglePreference(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/preference/PreferenceCategory;Landroid/preference/Preference$OnPreferenceChangeListener;)Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    move-result-object v24

    .line 182
    .restart local v24    # "togglePref":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    if-eqz v24, :cond_1

    .line 183
    invoke-virtual/range {v24 .. v24}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setupTitle()V

    .line 187
    .end local v24    # "togglePref":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    :cond_1
    const/4 v2, 0x0

    invoke-static {v6, v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->listFiles(Ljava/lang/String;Z)[Ljava/lang/String;

    move-result-object v19

    .line 188
    .local v19, "files":[Ljava/lang/String;
    move-object/from16 v17, v19

    .local v17, "arr$":[Ljava/lang/String;
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v23, v0

    .local v23, "len$":I
    const/16 v22, 0x0

    .local v22, "i$":I
    :goto_1
    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_4

    aget-object v7, v17, v22

    .line 189
    .local v7, "file":Ljava/lang/String;
    invoke-static {v7}, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->getType(Ljava/lang/String;)I

    move-result v25

    .line 190
    .local v25, "type":I
    if-nez v25, :cond_2

    .line 191
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-string v4, "intelli_plug_"

    const-string v5, "extras"

    move-object/from16 v9, p0

    invoke-static/range {v3 .. v9}, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->addAwesomeEditTextPreference(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/preference/PreferenceCategory;Landroid/preference/Preference$OnPreferenceChangeListener;)Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    .line 188
    :cond_2
    add-int/lit8 v22, v22, 0x1

    goto :goto_1

    .line 158
    .end local v6    # "path":Ljava/lang/String;
    .end local v7    # "file":Ljava/lang/String;
    .end local v8    # "awCategory":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;
    .end local v17    # "arr$":[Ljava/lang/String;
    .end local v19    # "files":[Ljava/lang/String;
    .end local v22    # "i$":I
    .end local v23    # "len$":I
    .end local v25    # "type":I
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMpDecision:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    .line 196
    .restart local v8    # "awCategory":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v8}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->removeIfEmpty(Landroid/preference/PreferenceScreen;Landroid/preference/PreferenceGroup;)V

    .line 198
    const-string v2, "mako_hotplug"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    .end local v8    # "awCategory":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;
    check-cast v8, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;

    .line 199
    .restart local v8    # "awCategory":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;
    invoke-virtual {v8}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->isSupported()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 200
    invoke-virtual {v8}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 202
    .restart local v6    # "path":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "enabled"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 203
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    const-string v10, "mako_"

    const-string v11, ""

    const-string v12, "extras"

    const-string v14, "enabled"

    move-object v13, v6

    move-object v15, v8

    move-object/from16 v16, p0

    invoke-static/range {v9 .. v16}, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->addAwesomeTogglePreference(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/preference/PreferenceCategory;Landroid/preference/Preference$OnPreferenceChangeListener;)Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    move-result-object v24

    .line 205
    .restart local v24    # "togglePref":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    if-eqz v24, :cond_5

    .line 206
    invoke-virtual/range {v24 .. v24}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setupTitle()V

    .line 209
    .end local v24    # "togglePref":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    :cond_5
    const/4 v2, 0x1

    invoke-static {v6, v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->listFiles(Ljava/lang/String;Z)[Ljava/lang/String;

    move-result-object v19

    .line 210
    .restart local v19    # "files":[Ljava/lang/String;
    move-object/from16 v17, v19

    .restart local v17    # "arr$":[Ljava/lang/String;
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v23, v0

    .restart local v23    # "len$":I
    const/16 v22, 0x0

    .restart local v22    # "i$":I
    :goto_2
    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_7

    aget-object v7, v17, v22

    .line 211
    .restart local v7    # "file":Ljava/lang/String;
    invoke-static {v7}, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->getType(Ljava/lang/String;)I

    move-result v25

    .line 212
    .restart local v25    # "type":I
    if-nez v25, :cond_6

    .line 213
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-string v4, "mako_"

    const-string v5, "extras"

    move-object/from16 v9, p0

    invoke-static/range {v3 .. v9}, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->addAwesomeEditTextPreference(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/preference/PreferenceCategory;Landroid/preference/Preference$OnPreferenceChangeListener;)Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    .line 210
    :cond_6
    add-int/lit8 v22, v22, 0x1

    goto :goto_2

    .line 218
    .end local v6    # "path":Ljava/lang/String;
    .end local v7    # "file":Ljava/lang/String;
    .end local v17    # "arr$":[Ljava/lang/String;
    .end local v19    # "files":[Ljava/lang/String;
    .end local v22    # "i$":I
    .end local v23    # "len$":I
    .end local v25    # "type":I
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v8}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->removeIfEmpty(Landroid/preference/PreferenceScreen;Landroid/preference/PreferenceGroup;)V

    .line 223
    const-string v2, "cpu_quiet"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v18

    check-cast v18, Landroid/preference/PreferenceCategory;

    .line 224
    .local v18, "category":Landroid/preference/PreferenceCategory;
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e00dd

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e00dc

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e00de

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 227
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e00dc

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v21

    .line 229
    .local v21, "govs":[Ljava/lang/String;
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e00de

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 231
    .local v20, "gov":Ljava/lang/String;
    new-instance v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuQuietGov:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    .line 232
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuQuietGov:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    const-string v3, "pref_cpu_quiet_governor"

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setKey(Ljava/lang/String;)V

    .line 233
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuQuietGov:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    const v3, 0x7f0e0120

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setTitle(I)V

    .line 234
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuQuietGov:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 235
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuQuietGov:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 236
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuQuietGov:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 237
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuQuietGov:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 238
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuQuietGov:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 239
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuQuietGov:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 241
    .end local v20    # "gov":Ljava/lang/String;
    .end local v21    # "govs":[Ljava/lang/String;
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v2, v1}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->removeIfEmpty(Landroid/preference/PreferenceScreen;Landroid/preference/PreferenceGroup;)V

    .line 243
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->isSupported(Landroid/preference/PreferenceScreen;Landroid/content/Context;)V

    .line 244
    return-void
.end method


# virtual methods
.method public generateRow(ILorg/namelessrom/devicecontrol/objects/CpuCore;)Landroid/view/View;
    .locals 6
    .param p1, "core"    # I
    .param p2, "cpuCore"    # Lorg/namelessrom/devicecontrol/objects/CpuCore;

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 407
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuInfo:Landroid/widget/LinearLayout;

    if-nez v3, :cond_2

    :cond_0
    const/4 v1, 0x0

    .line 429
    :cond_1
    :goto_0
    return-object v1

    .line 408
    :cond_2
    const-string v3, "generateRow(%s);"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-virtual {p2}, Lorg/namelessrom/devicecontrol/objects/CpuCore;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 410
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v3, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 411
    .local v1, "rowView":Landroid/view/View;
    if-nez v1, :cond_3

    .line 412
    new-instance v1, Lorg/namelessrom/devicecontrol/ui/views/CpuCoreView;

    .end local v1    # "rowView":Landroid/view/View;
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v1, v3}, Lorg/namelessrom/devicecontrol/ui/views/CpuCoreView;-><init>(Landroid/content/Context;)V

    .line 413
    .restart local v1    # "rowView":Landroid/view/View;
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 416
    :cond_3
    instance-of v3, v1, Lorg/namelessrom/devicecontrol/ui/views/CpuCoreView;

    if-eqz v3, :cond_1

    .line 417
    iget v3, p2, Lorg/namelessrom/devicecontrol/objects/CpuCore;->mCoreCurrent:I

    if-nez v3, :cond_4

    .local v0, "isOffline":Z
    :goto_1
    move-object v2, v1

    .line 419
    check-cast v2, Lorg/namelessrom/devicecontrol/ui/views/CpuCoreView;

    iget-object v2, v2, Lorg/namelessrom/devicecontrol/ui/views/CpuCoreView;->core:Landroid/widget/TextView;

    iget-object v3, p2, Lorg/namelessrom/devicecontrol/objects/CpuCore;->mCore:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v2, v1

    .line 420
    check-cast v2, Lorg/namelessrom/devicecontrol/ui/views/CpuCoreView;

    iget-object v3, v2, Lorg/namelessrom/devicecontrol/ui/views/CpuCoreView;->freq:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    const v2, 0x7f0e006e

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v2, v1

    .line 425
    check-cast v2, Lorg/namelessrom/devicecontrol/ui/views/CpuCoreView;

    iget-object v2, v2, Lorg/namelessrom/devicecontrol/ui/views/CpuCoreView;->bar:Lcom/daimajia/numberprogressbar/NumberProgressBar;

    iget v3, p2, Lorg/namelessrom/devicecontrol/objects/CpuCore;->mCoreMax:I

    invoke-virtual {v2, v3}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->setMax(I)V

    move-object v2, v1

    .line 426
    check-cast v2, Lorg/namelessrom/devicecontrol/ui/views/CpuCoreView;

    iget-object v2, v2, Lorg/namelessrom/devicecontrol/ui/views/CpuCoreView;->bar:Lcom/daimajia/numberprogressbar/NumberProgressBar;

    iget v3, p2, Lorg/namelessrom/devicecontrol/objects/CpuCore;->mCoreCurrent:I

    invoke-virtual {v2, v3}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->setProgress(I)V

    goto :goto_0

    .end local v0    # "isOffline":Z
    :cond_4
    move v0, v2

    .line 417
    goto :goto_1

    .line 420
    .restart local v0    # "isOffline":Z
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p2, Lorg/namelessrom/devicecontrol/objects/CpuCore;->mCoreCurrent:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->toMhz(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " / "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p2, Lorg/namelessrom/devicecontrol/objects/CpuCore;->mCoreMax:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->toMhz(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " ["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p2, Lorg/namelessrom/devicecontrol/objects/CpuCore;->mCoreGov:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v4, 0x5d

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method

.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 84
    const v0, 0x7f0e007d

    return v0
.end method

.method public onCores(Lorg/namelessrom/devicecontrol/cpu/CpuUtils$Cores;)V
    .locals 4
    .param p1, "cores"    # Lorg/namelessrom/devicecontrol/cpu/CpuUtils$Cores;

    .prologue
    .line 101
    iget-object v0, p1, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$Cores;->list:Ljava/util/List;

    .line 102
    .local v0, "coreList":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/objects/CpuCore;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 103
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 104
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 105
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/namelessrom/devicecontrol/objects/CpuCore;

    invoke-virtual {p0, v2, v3}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->generateRow(ILorg/namelessrom/devicecontrol/objects/CpuCore;)Landroid/view/View;

    .line 104
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 108
    .end local v1    # "count":I
    .end local v2    # "i":I
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 111
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 112
    const v1, 0x7f060001

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->addPreferencesFromResource(I)V

    .line 114
    const-string v1, "pref_max"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    .line 115
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 117
    const-string v1, "pref_min"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    .line 118
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 120
    const-string v1, "cpu_lock_freq"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuLock:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    .line 121
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuLock:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v2

    iget-boolean v2, v2, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->perfCpuLock:Z

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 122
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuLock:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 124
    const-string v1, "pref_governor"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    .line 125
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 127
    const-string v1, "pref_governor_tuning"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mGovernorTuning:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 129
    const-string v1, "cpu_lock_gov"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuGovLock:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    .line 130
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuGovLock:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v2

    iget-boolean v2, v2, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->perfCpuGovLock:Z

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 131
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuGovLock:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 134
    const-string v1, "hotplugging"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 135
    .local v0, "hotplugging":Landroid/preference/PreferenceCategory;
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 137
    const v1, 0x7f0e0095

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x7f0e0096

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x7f0e00dd

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "/system/bin/mpdecision"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 141
    :cond_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 142
    const v1, 0x7f060002

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->addPreferencesFromResource(I)V

    .line 143
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->setupHotpluggingPreferences()V

    .line 145
    :cond_1
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;
    .param p3, "savedState"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 331
    invoke-virtual {p0, v10}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->setHasOptionsMenu(Z)V

    .line 332
    const v5, 0x7f04002c

    invoke-virtual {p1, v5, p2, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 334
    .local v4, "view":Landroid/view/View;
    const v5, 0x7f0c0097

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuInfo:Landroid/widget/LinearLayout;

    .line 336
    const v5, 0x7f0c0098

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/support/v7/widget/SwitchCompat;

    iput-object v5, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mStatusHide:Landroid/support/v7/widget/SwitchCompat;

    .line 337
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mStatusHide:Landroid/support/v7/widget/SwitchCompat;

    new-instance v6, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment$1;

    invoke-direct {v6, p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment$1;-><init>(Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;)V

    invoke-virtual {v5, v6}, Landroid/support/v7/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 350
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mStatusHide:Landroid/support/v7/widget/SwitchCompat;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-static {v6}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v6

    iget-boolean v6, v6, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->perfCpuInfo:Z

    invoke-virtual {v5, v6}, Landroid/support/v7/widget/SwitchCompat;->setChecked(Z)V

    .line 351
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mStatusHide:Landroid/support/v7/widget/SwitchCompat;

    invoke-virtual {v5}, Landroid/support/v7/widget/SwitchCompat;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 352
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 358
    :goto_0
    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->get()Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    move-result-object v5

    invoke-virtual {v5}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->getNumOfCpus()I

    move-result v2

    .line 359
    .local v2, "mCpuNum":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f0e0068

    invoke-virtual {p0, v6}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " %s:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 360
    .local v0, "format":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_1

    .line 361
    new-instance v3, Lorg/namelessrom/devicecontrol/objects/CpuCore;

    new-array v5, v10, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "0"

    const-string v7, "0"

    const-string v8, "0"

    invoke-direct {v3, v5, v6, v7, v8}, Lorg/namelessrom/devicecontrol/objects/CpuCore;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    .local v3, "tmpCore":Lorg/namelessrom/devicecontrol/objects/CpuCore;
    invoke-virtual {p0, v1, v3}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->generateRow(ILorg/namelessrom/devicecontrol/objects/CpuCore;)Landroid/view/View;

    .line 360
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 354
    .end local v0    # "format":Ljava/lang/String;
    .end local v1    # "i":I
    .end local v2    # "mCpuNum":I
    .end local v3    # "tmpCore":Lorg/namelessrom/devicecontrol/objects/CpuCore;
    :cond_0
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuInfo:Landroid/widget/LinearLayout;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 365
    .restart local v0    # "format":Ljava/lang/String;
    .restart local v1    # "i":I
    .restart local v2    # "mCpuNum":I
    :cond_1
    return-object v4
.end method

.method public onFrequency(Lorg/namelessrom/devicecontrol/cpu/CpuUtils$Frequency;)V
    .locals 9
    .param p1, "cpuFreq"    # Lorg/namelessrom/devicecontrol/cpu/CpuUtils$Frequency;

    .prologue
    const/4 v8, 0x1

    .line 369
    iget-object v5, p1, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$Frequency;->available:[Ljava/lang/String;

    .line 370
    .local v5, "mAvailableFrequencies":[Ljava/lang/String;
    new-instance v6, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment$2;

    invoke-direct {v6, p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment$2;-><init>(Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;)V

    invoke-static {v5, v6}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 376
    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 378
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 379
    .local v1, "entries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object v0, v5

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 380
    .local v4, "mAvailableFreq":Ljava/lang/String;
    invoke-static {v4}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->toMhz(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 379
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 383
    .end local v4    # "mAvailableFreq":Ljava/lang/String;
    :cond_0
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/CharSequence;

    invoke-virtual {v7, v6}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 384
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v6, v5}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 385
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iget-object v7, p1, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$Frequency;->maximum:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 386
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iget-object v7, p1, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$Frequency;->maximum:Ljava/lang/String;

    invoke-static {v7}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->toMhz(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 387
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v6, v8}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEnabled(Z)V

    .line 389
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/CharSequence;

    invoke-virtual {v7, v6}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 390
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v6, v5}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 391
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iget-object v7, p1, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$Frequency;->minimum:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 392
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iget-object v7, p1, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$Frequency;->minimum:Ljava/lang/String;

    invoke-static {v7}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->toMhz(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 393
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v6, v8}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEnabled(Z)V

    .line 395
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 396
    return-void
.end method

.method public onGovernor(Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$Governor;)V
    .locals 2
    .param p1, "governor"    # Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$Governor;

    .prologue
    .line 399
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iget-object v1, p1, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$Governor;->available:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 400
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iget-object v1, p1, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$Governor;->available:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 401
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iget-object v1, p1, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$Governor;->current:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 402
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iget-object v1, p1, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$Governor;->current:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 403
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEnabled(Z)V

    .line 404
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 96
    invoke-super {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onPause()V

    .line 97
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->getInstance(Landroid/app/Activity;)Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->stop()V

    .line 98
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 10
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x1

    .line 247
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    if-ne p1, v0, :cond_2

    .line 248
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 249
    .local v7, "selected":Ljava/lang/String;
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 250
    .local v6, "other":Ljava/lang/String;
    invoke-static {v7}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v6}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-ge v0, v1, :cond_0

    move v8, v5

    .line 251
    .local v8, "updateOther":Z
    :cond_0
    if-eqz v8, :cond_1

    .line 252
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v0, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 253
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-static {v7}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->toMhz(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 255
    :cond_1
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v0, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 256
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-static {v7}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->toMhz(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 258
    const-string v0, "cpu_frequency_max"

    invoke-static {v0, v7, v5}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor;->processAction(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 303
    .end local v6    # "other":Ljava/lang/String;
    .end local v7    # "selected":Ljava/lang/String;
    .end local v8    # "updateOther":Z
    .end local p2    # "o":Ljava/lang/Object;
    :goto_0
    return v5

    .line 260
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_2
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    if-ne p1, v0, :cond_5

    .line 261
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 262
    .restart local v7    # "selected":Ljava/lang/String;
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 263
    .restart local v6    # "other":Ljava/lang/String;
    invoke-static {v7}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v6}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-le v0, v1, :cond_3

    move v8, v5

    .line 264
    .restart local v8    # "updateOther":Z
    :cond_3
    if-eqz v8, :cond_4

    .line 265
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v0, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 266
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-static {v7}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->toMhz(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 268
    :cond_4
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v0, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 269
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-static {v7}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->toMhz(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 271
    const-string v0, "cpu_frequency_min"

    invoke-static {v0, v7, v5}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor;->processAction(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 273
    .end local v6    # "other":Ljava/lang/String;
    .end local v7    # "selected":Ljava/lang/String;
    .end local v8    # "updateOther":Z
    :cond_5
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuLock:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-ne p1, v0, :cond_6

    .line 274
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->perfCpuLock:Z

    .line 275
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    goto :goto_0

    .line 277
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_6
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    if-ne p1, v0, :cond_7

    .line 278
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 279
    .restart local v7    # "selected":Ljava/lang/String;
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v0, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 280
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v0, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 282
    const-string v0, "cpu_governor"

    invoke-static {v0, v7, v5}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor;->processAction(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 284
    .end local v7    # "selected":Ljava/lang/String;
    :cond_7
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuGovLock:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-ne p1, v0, :cond_8

    .line 285
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->perfCpuGovLock:Z

    .line 286
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    goto/16 :goto_0

    .line 288
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_8
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMpDecision:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-ne p1, v0, :cond_a

    .line 289
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 290
    .local v4, "value":Z
    new-instance v1, Lorg/namelessrom/devicecontrol/actions/extras/MpDecisionAction;

    if-eqz v4, :cond_9

    const-string v0, "1"

    :goto_1
    invoke-direct {v1, v0, v5}, Lorg/namelessrom/devicecontrol/actions/extras/MpDecisionAction;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/actions/extras/MpDecisionAction;->triggerAction()V

    goto/16 :goto_0

    :cond_9
    const-string v0, "0"

    goto :goto_1

    .line 292
    .end local v4    # "value":Z
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_a
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuQuietGov:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    if-ne p1, v0, :cond_b

    .line 293
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    const v1, 0x7f0e00de

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 294
    .local v3, "path":Ljava/lang/String;
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 295
    .local v4, "value":Ljava/lang/String;
    invoke-static {v3, v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;)V

    .line 296
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    new-instance v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    const-string v1, "extras"

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuQuietGov:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/objects/BootupItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v9, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->setBootup(Landroid/content/Context;Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 299
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuQuietGov:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v0, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 303
    .end local v3    # "path":Ljava/lang/String;
    .end local v4    # "value":Ljava/lang/String;
    :cond_b
    invoke-super {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z

    move-result v5

    goto/16 :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 2
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 308
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mGovernorTuning:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne p2, v0, :cond_0

    .line 309
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0e0465

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/MainActivity;->loadFragment(Landroid/app/Activity;I)V

    .line 310
    const/4 v0, 0x1

    .line 312
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 87
    invoke-super {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onResume()V

    .line 88
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mStatusHide:Landroid/support/v7/widget/SwitchCompat;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mStatusHide:Landroid/support/v7/widget/SwitchCompat;

    invoke-virtual {v0}, Landroid/support/v7/widget/SwitchCompat;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->getInstance(Landroid/app/Activity;)Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;

    move-result-object v0

    const/16 v1, 0x3e8

    invoke-virtual {v0, p0, v1}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->start(Lorg/namelessrom/devicecontrol/cpu/CpuUtils$CoreListener;I)V

    .line 91
    :cond_0
    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->get()Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->getCpuFreq(Lorg/namelessrom/devicecontrol/cpu/CpuUtils$FrequencyListener;)V

    .line 92
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->getGovernor(Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$GovernorListener;)V

    .line 93
    return-void
.end method

.method public onShellOutput(Lorg/namelessrom/devicecontrol/objects/ShellOutput;)V
    .locals 2
    .param p1, "shellOutput"    # Lorg/namelessrom/devicecontrol/objects/ShellOutput;

    .prologue
    .line 316
    if-eqz p1, :cond_0

    .line 317
    iget v0, p1, Lorg/namelessrom/devicecontrol/objects/ShellOutput;->id:I

    packed-switch v0, :pswitch_data_0

    .line 328
    :cond_0
    :goto_0
    return-void

    .line 319
    :pswitch_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMpDecision:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-eqz v0, :cond_0

    .line 320
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMpDecision:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iget-object v0, p1, Lorg/namelessrom/devicecontrol/objects/ShellOutput;->output:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 321
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mMpDecision:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v0, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_0

    .line 320
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 317
    :pswitch_data_0
    .packed-switch 0xc8
        :pswitch_0
    .end packed-switch
.end method
