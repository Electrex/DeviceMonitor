.class public Lorg/namelessrom/devicecontrol/cpu/CpuUtils$Frequency;
.super Ljava/lang/Object;
.source "CpuUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/cpu/CpuUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Frequency"
.end annotation


# instance fields
.field public final available:[Ljava/lang/String;

.field public final maximum:Ljava/lang/String;

.field public final minimum:Ljava/lang/String;


# direct methods
.method public constructor <init>([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "avail"    # [Ljava/lang/String;
    .param p2, "max"    # Ljava/lang/String;
    .param p3, "min"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$Frequency;->available:[Ljava/lang/String;

    .line 68
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$Frequency;->maximum:Ljava/lang/String;

    .line 69
    iput-object p3, p0, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$Frequency;->minimum:Ljava/lang/String;

    .line 70
    return-void
.end method
