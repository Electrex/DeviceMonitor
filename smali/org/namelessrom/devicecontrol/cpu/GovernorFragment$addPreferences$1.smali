.class Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences$1;
.super Ljava/lang/Object;
.source "GovernorFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences$1;->this$1:Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 12
    .param p1, "p"    # Landroid/preference/Preference;

    .prologue
    const/4 v11, 0x1

    const/4 v10, -0x1

    const/16 v8, 0x28

    const/4 v9, -0x2

    .line 125
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v7, p0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences$1;->this$1:Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;

    iget-object v7, v7, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;->this$0:Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->access$200(Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;)Landroid/content/Context;

    move-result-object v7

    invoke-direct {v0, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 126
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    new-instance v3, Landroid/widget/LinearLayout;

    iget-object v7, p0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences$1;->this$1:Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;

    iget-object v7, v7, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;->this$0:Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->access$200(Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;)Landroid/content/Context;

    move-result-object v7

    invoke-direct {v3, v7}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 127
    .local v3, "ll":Landroid/widget/LinearLayout;
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v7, v9, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 130
    new-instance v2, Landroid/widget/EditText;

    iget-object v7, p0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences$1;->this$1:Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;

    iget-object v7, v7, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;->this$0:Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->access$200(Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;)Landroid/content/Context;

    move-result-object v7

    invoke-direct {v2, v7}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 131
    .local v2, "et":Landroid/widget/EditText;
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v4, v10, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 134
    .local v4, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v4, v8, v8, v8, v8}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 135
    const/16 v7, 0x11

    iput v7, v4, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 136
    invoke-virtual {p1}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 137
    .local v5, "val":Ljava/lang/String;
    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 138
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, Landroid/widget/EditText;->setRawInputType(I)V

    .line 139
    invoke-virtual {v2, v11}, Landroid/widget/EditText;->setGravity(I)V

    .line 140
    invoke-virtual {v2, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 141
    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 142
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 143
    const v7, 0x104000a

    new-instance v8, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences$1$1;

    invoke-direct {v8, p0, v2, p1}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences$1$1;-><init>(Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences$1;Landroid/widget/EditText;Landroid/preference/Preference;)V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 155
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 156
    .local v1, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 157
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    .line 158
    .local v6, "window":Landroid/view/Window;
    invoke-virtual {v6, v9, v9}, Landroid/view/Window;->setLayout(II)V

    .line 159
    return v11
.end method
