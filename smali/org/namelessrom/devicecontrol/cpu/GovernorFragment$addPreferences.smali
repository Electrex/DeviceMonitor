.class Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;
.super Landroid/os/AsyncTask;
.source "GovernorFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "addPreferences"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;


# direct methods
.method private constructor <init>(Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;->this$0:Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;
    .param p2, "x1"    # Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$1;

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;-><init>(Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 95
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 14
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 98
    iget-object v11, p0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;->this$0:Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->mCategory:Landroid/preference/PreferenceCategory;
    invoke-static {v11}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->access$100(Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;)Landroid/preference/PreferenceCategory;

    move-result-object v11

    invoke-virtual {v11}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v11

    if-eqz v11, :cond_0

    .line 99
    iget-object v11, p0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;->this$0:Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->mCategory:Landroid/preference/PreferenceCategory;
    invoke-static {v11}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->access$100(Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;)Landroid/preference/PreferenceCategory;

    move-result-object v11

    invoke-virtual {v11}, Landroid/preference/PreferenceCategory;->removeAll()V

    .line 101
    :cond_0
    const/4 v11, 0x0

    aget-object v1, p1, v11

    .line 102
    .local v1, "currentGovernor":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "/sys/devices/system/cpu/cpufreq/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v2, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 103
    .local v2, "f":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 104
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v7

    .line 105
    .local v7, "files":[Ljava/io/File;
    move-object v0, v7

    .local v0, "arr$":[Ljava/io/File;
    array-length v9, v0

    .local v9, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_0
    if-ge v8, v9, :cond_2

    aget-object v3, v0, v8

    .line 106
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    .line 109
    .local v5, "fileName":Ljava/lang/String;
    const-string v11, "boostpulse"

    invoke-virtual {v11, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 105
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 113
    :cond_1
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    .line 114
    .local v6, "filePath":Ljava/lang/String;
    invoke-static {v6}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    const-string v12, "\n"

    const-string v13, ""

    invoke-virtual {v11, v12, v13}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 116
    .local v4, "fileContent":Ljava/lang/String;
    new-instance v10, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iget-object v11, p0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;->this$0:Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->access$200(Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;)Landroid/content/Context;

    move-result-object v11

    invoke-direct {v10, v11}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;-><init>(Landroid/content/Context;)V

    .line 117
    .local v10, "pref":Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-virtual {v10, v5}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 118
    invoke-virtual {v10, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 119
    invoke-virtual {v10, v6}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setKey(Ljava/lang/String;)V

    .line 120
    iget-object v11, p0, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;->this$0:Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->mCategory:Landroid/preference/PreferenceCategory;
    invoke-static {v11}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;->access$100(Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;)Landroid/preference/PreferenceCategory;

    move-result-object v11

    invoke-virtual {v11, v10}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 121
    new-instance v11, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences$1;

    invoke-direct {v11, p0}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences$1;-><init>(Lorg/namelessrom/devicecontrol/cpu/GovernorFragment$addPreferences;)V

    invoke-virtual {v10, v11}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_1

    .line 166
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "fileContent":Ljava/lang/String;
    .end local v5    # "fileName":Ljava/lang/String;
    .end local v6    # "filePath":Ljava/lang/String;
    .end local v7    # "files":[Ljava/io/File;
    .end local v8    # "i$":I
    .end local v9    # "len$":I
    .end local v10    # "pref":Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    :cond_2
    const/4 v11, 0x0

    return-object v11
.end method
