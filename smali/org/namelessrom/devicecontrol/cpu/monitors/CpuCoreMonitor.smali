.class public Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;
.super Ljava/lang/Object;
.source "CpuCoreMonitor.java"


# static fields
.field private static final CPU_COUNT:I

.field private static cpuFrequencyMonitor:Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;

.field private static isStarted:Z

.field private static mActivity:Landroid/app/Activity;

.field private static mInterval:I

.field private static mListener:Lorg/namelessrom/devicecontrol/cpu/CpuUtils$CoreListener;

.field private static final mLock:Ljava/lang/Object;

.field private static mShell:Lcom/stericson/roottools/execution/Shell;


# instance fields
.field private final mUpdater:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->get()Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->getNumOfCpus()I

    move-result v0

    sput v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->CPU_COUNT:I

    .line 45
    const/4 v0, 0x0

    sput-boolean v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->isStarted:Z

    .line 47
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mLock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    new-instance v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor$1;

    invoke-direct {v0, p0}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor$1;-><init>(Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;)V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mUpdater:Ljava/lang/Runnable;

    .line 52
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->openShell()V

    .line 53
    sput-object p1, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mActivity:Landroid/app/Activity;

    .line 54
    return-void
.end method

.method static synthetic access$000()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;)V
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->updateStates()V

    return-void
.end method

.method static synthetic access$200()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$300()I
    .locals 1

    .prologue
    .line 36
    sget v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->CPU_COUNT:I

    return v0
.end method

.method static synthetic access$400()Lorg/namelessrom/devicecontrol/cpu/CpuUtils$CoreListener;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mListener:Lorg/namelessrom/devicecontrol/cpu/CpuUtils$CoreListener;

    return-object v0
.end method

.method static synthetic access$500(Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;

    .prologue
    .line 36
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mUpdater:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$600()I
    .locals 1

    .prologue
    .line 36
    sget v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mInterval:I

    return v0
.end method

.method public static getInstance(Landroid/app/Activity;)Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 57
    sget-object v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->cpuFrequencyMonitor:Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;

    invoke-direct {v0, p0}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;-><init>(Landroid/app/Activity;)V

    sput-object v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->cpuFrequencyMonitor:Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;

    .line 60
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->cpuFrequencyMonitor:Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;

    return-object v0
.end method

.method private openShell()V
    .locals 3

    .prologue
    .line 93
    sget-object v1, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mShell:Lcom/stericson/roottools/execution/Shell;

    if-eqz v1, :cond_0

    sget-object v1, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mShell:Lcom/stericson/roottools/execution/Shell;

    invoke-virtual {v1}, Lcom/stericson/roottools/execution/Shell;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 95
    :cond_0
    const/4 v1, 0x1

    :try_start_0
    invoke-static {v1}, Lcom/stericson/roottools/RootTools;->getShell(Z)Lcom/stericson/roottools/execution/Shell;

    move-result-object v1

    sput-object v1, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mShell:Lcom/stericson/roottools/execution/Shell;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :cond_1
    :goto_0
    return-void

    .line 96
    :catch_0
    move-exception v0

    .line 97
    .local v0, "exc":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CpuCoreMonitor: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateStates()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 103
    const-string v0, " 2> /dev/null;"

    .line 104
    .local v0, "END":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    .local v5, "sb":Ljava/lang/StringBuilder;
    const-string v6, "command=$("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    sget v6, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->CPU_COUNT:I

    if-ge v3, v6, :cond_0

    .line 109
    const-string v6, "if [ -d \"/sys/devices/system/cpu/cpu"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/cpufreq\" ]; then "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    const-string v6, "busybox cat "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->get()Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    move-result-object v7

    invoke-virtual {v7, v3}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->getCpuFrequencyPath(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " 2> /dev/null;"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    const-string v6, "busybox cat "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->get()Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    move-result-object v7

    invoke-virtual {v7, v3}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->getMaxCpuFrequencyPath(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " 2> /dev/null;"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    const-string v6, "busybox cat "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;

    move-result-object v7

    invoke-virtual {v7, v3}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->getGovernorPath(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " 2> /dev/null;"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    const-string v6, "else busybox echo \"0 0 0\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " 2> /dev/null;"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " fi;"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 121
    :cond_0
    const-string v6, ");"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "echo $command | busybox tr -d \"\\n\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 124
    .local v1, "cmd":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "cmd: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 127
    .local v4, "outputCollector":Ljava/lang/StringBuilder;
    new-instance v2, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor$2;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    aput-object v1, v6, v8

    invoke-direct {v2, p0, v8, v6, v4}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor$2;-><init>(Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;I[Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 183
    .local v2, "commandCapture":Lcom/stericson/roottools/execution/CommandCapture;
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->openShell()V

    .line 184
    sget-object v6, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mShell:Lcom/stericson/roottools/execution/Shell;

    if-eqz v6, :cond_1

    sget-object v6, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mShell:Lcom/stericson/roottools/execution/Shell;

    invoke-virtual {v6}, Lcom/stericson/roottools/execution/Shell;->isClosed()Z

    move-result v6

    if-nez v6, :cond_1

    sget-boolean v6, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->isStarted:Z

    if-eqz v6, :cond_1

    .line 185
    sget-object v6, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mShell:Lcom/stericson/roottools/execution/Shell;

    invoke-virtual {v6, v2}, Lcom/stericson/roottools/execution/Shell;->add(Lcom/stericson/roottools/execution/Command;)Lcom/stericson/roottools/execution/Command;

    .line 187
    :cond_1
    return-void
.end method


# virtual methods
.method public start(Lorg/namelessrom/devicecontrol/cpu/CpuUtils$CoreListener;I)V
    .locals 2
    .param p1, "listener"    # Lorg/namelessrom/devicecontrol/cpu/CpuUtils$CoreListener;
    .param p2, "interval"    # I

    .prologue
    .line 66
    sput-object p1, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mListener:Lorg/namelessrom/devicecontrol/cpu/CpuUtils$CoreListener;

    .line 67
    sput p2, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mInterval:I

    .line 68
    sget-boolean v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->isStarted:Z

    if-nez v0, :cond_0

    .line 69
    sget-object v0, Lorg/namelessrom/devicecontrol/Application;->HANDLER:Landroid/os/Handler;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mUpdater:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 70
    const/4 v0, 0x1

    sput-boolean v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->isStarted:Z

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "started, interval: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mInterval:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    :goto_0
    return-void

    .line 73
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "updated interval: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mInterval:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 78
    const/4 v0, 0x0

    sput-object v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mListener:Lorg/namelessrom/devicecontrol/cpu/CpuUtils$CoreListener;

    .line 79
    const/4 v0, 0x0

    sput-boolean v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->isStarted:Z

    .line 80
    sget-object v0, Lorg/namelessrom/devicecontrol/Application;->HANDLER:Landroid/os/Handler;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mUpdater:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 81
    const-string v0, "stopped!"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    return-void
.end method
