.class public Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;
.super Ljava/lang/Object;
.source "CpuStateMonitor.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CpuState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;",
        ">;"
    }
.end annotation


# instance fields
.field public final duration:J

.field public final freq:I


# direct methods
.method public constructor <init>(IJ)V
    .locals 0
    .param p1, "freq"    # I
    .param p2, "duration"    # J

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput p1, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;->freq:I

    .line 58
    iput-wide p2, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;->duration:J

    .line 59
    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 52
    check-cast p1, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;->compareTo(Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;)I
    .locals 3
    .param p1, "state"    # Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;

    .prologue
    .line 62
    iget v2, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;->freq:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 63
    .local v0, "a":Ljava/lang/Integer;
    iget v2, p1, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;->freq:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 64
    .local v1, "b":Ljava/lang/Integer;
    invoke-virtual {v0, v1}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v2

    return v2
.end method
