.class Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$1;
.super Ljava/lang/Object;
.source "CpuStateMonitor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;->updateStates(Lorg/namelessrom/devicecontrol/cpu/CpuUtils$StateListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;

.field final synthetic val$listener:Lorg/namelessrom/devicecontrol/cpu/CpuUtils$StateListener;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;Lorg/namelessrom/devicecontrol/cpu/CpuUtils$StateListener;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$1;->this$0:Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$1;->val$listener:Lorg/namelessrom/devicecontrol/cpu/CpuUtils$StateListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 100
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$1;->val$listener:Lorg/namelessrom/devicecontrol/cpu/CpuUtils$StateListener;

    new-instance v1, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$State;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$1;->this$0:Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;

    # getter for: Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;->mStates:Ljava/util/ArrayList;
    invoke-static {v2}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;->access$000(Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$1;->this$0:Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$1;->this$0:Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;

    # getter for: Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;->mStates:Ljava/util/ArrayList;
    invoke-static {v4}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;->access$000(Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;)Ljava/util/ArrayList;

    move-result-object v4

    # invokes: Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;->getTotalStateTime(Ljava/util/ArrayList;)J
    invoke-static {v3, v4}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;->access$100(Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;Ljava/util/ArrayList;)J

    move-result-wide v4

    invoke-direct {v1, v2, v4, v5}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$State;-><init>(Ljava/util/List;J)V

    invoke-interface {v0, v1}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$StateListener;->onStates(Lorg/namelessrom/devicecontrol/cpu/CpuUtils$State;)V

    .line 101
    return-void
.end method
