.class public Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;
.super Ljava/lang/Object;
.source "CpuStateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;
    }
.end annotation


# static fields
.field private static mCpuStateMonitor:Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;


# instance fields
.field private final mStates:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;->mStates:Ljava/util/ArrayList;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;

    .prologue
    .line 37
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;->mStates:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;Ljava/util/ArrayList;)J
    .locals 2
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;->getTotalStateTime(Ljava/util/ArrayList;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getInstance()Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;->mCpuStateMonitor:Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;

    if-nez v0, :cond_0

    .line 47
    new-instance v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;->mCpuStateMonitor:Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;

    .line 49
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;->mCpuStateMonitor:Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;

    return-object v0
.end method

.method private getTotalStateTime(Ljava/util/ArrayList;)J
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;",
            ">;)J"
        }
    .end annotation

    .prologue
    .local p1, "states":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;>;"
    const-wide/16 v4, 0x0

    .line 69
    const-wide/16 v2, 0x0

    .line 70
    .local v2, "sum":J
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;

    .line 71
    .local v1, "state":Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;
    iget-wide v6, v1, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;->duration:J

    add-long/2addr v2, v6

    .line 72
    goto :goto_0

    .line 73
    .end local v1    # "state":Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;
    :cond_0
    cmp-long v6, v2, v4

    if-gez v6, :cond_1

    move-wide v2, v4

    .end local v2    # "sum":J
    :cond_1
    return-wide v2
.end method

.method private readInStates(Ljava/io/BufferedReader;)V
    .locals 8
    .param p1, "br"    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .local v0, "line":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 109
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 111
    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 112
    .local v1, "nums":[Ljava/lang/String;
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;->mStates:Ljava/util/ArrayList;

    new-instance v3, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;

    const/4 v4, 0x0

    aget-object v4, v1, v4

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x1

    aget-object v5, v1, v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-direct {v3, v4, v6, v7}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;-><init>(IJ)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 115
    .end local v1    # "nums":[Ljava/lang/String;
    :cond_1
    return-void
.end method


# virtual methods
.method public updateStates(Lorg/namelessrom/devicecontrol/cpu/CpuUtils$StateListener;)V
    .locals 14
    .param p1, "listener"    # Lorg/namelessrom/devicecontrol/cpu/CpuUtils$StateListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x0

    .line 77
    const/4 v6, 0x0

    .line 78
    .local v6, "is":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 79
    .local v4, "ir":Ljava/io/InputStreamReader;
    const/4 v0, 0x0

    .line 81
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v7, Ljava/io/FileInputStream;

    const-string v10, "/sys/devices/system/cpu/cpu0/cpufreq/stats/time_in_state"

    invoke-direct {v7, v10}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    .end local v6    # "is":Ljava/io/InputStream;
    .local v7, "is":Ljava/io/InputStream;
    :try_start_1
    new-instance v5, Ljava/io/InputStreamReader;

    invoke-direct {v5, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 83
    .end local v4    # "ir":Ljava/io/InputStreamReader;
    .local v5, "ir":Ljava/io/InputStreamReader;
    :try_start_2
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 84
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    :try_start_3
    iget-object v10, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;->mStates:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 85
    invoke-direct {p0, v1}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;->readInStates(Ljava/io/BufferedReader;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 87
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 88
    :cond_0
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/io/InputStreamReader;->close()V

    .line 89
    :cond_1
    if-eqz v7, :cond_2

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    .line 92
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v12

    sub-long/2addr v10, v12

    const-wide/16 v12, 0xa

    div-long v2, v10, v12

    .line 93
    .local v2, "deepSleep":J
    iget-object v10, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;->mStates:Ljava/util/ArrayList;

    new-instance v11, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;

    const/4 v12, 0x0

    cmp-long v13, v2, v8

    if-gez v13, :cond_3

    move-wide v2, v8

    .end local v2    # "deepSleep":J
    :cond_3
    invoke-direct {v11, v12, v2, v3}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;-><init>(IJ)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;->mStates:Ljava/util/ArrayList;

    invoke-static {}, Ljava/util/Collections;->reverseOrder()Ljava/util/Comparator;

    move-result-object v9

    invoke-static {v8, v9}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 97
    sget-object v8, Lorg/namelessrom/devicecontrol/Application;->HANDLER:Landroid/os/Handler;

    new-instance v9, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$1;

    invoke-direct {v9, p0, p1}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$1;-><init>(Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;Lorg/namelessrom/devicecontrol/cpu/CpuUtils$StateListener;)V

    invoke-virtual {v8, v9}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 103
    return-void

    .line 87
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v5    # "ir":Ljava/io/InputStreamReader;
    .end local v7    # "is":Ljava/io/InputStream;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "ir":Ljava/io/InputStreamReader;
    .restart local v6    # "is":Ljava/io/InputStream;
    :catchall_0
    move-exception v8

    :goto_0
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 88
    :cond_4
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Ljava/io/InputStreamReader;->close()V

    .line 89
    :cond_5
    if-eqz v6, :cond_6

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    :cond_6
    throw v8

    .line 87
    .end local v6    # "is":Ljava/io/InputStream;
    .restart local v7    # "is":Ljava/io/InputStream;
    :catchall_1
    move-exception v8

    move-object v6, v7

    .end local v7    # "is":Ljava/io/InputStream;
    .restart local v6    # "is":Ljava/io/InputStream;
    goto :goto_0

    .end local v4    # "ir":Ljava/io/InputStreamReader;
    .end local v6    # "is":Ljava/io/InputStream;
    .restart local v5    # "ir":Ljava/io/InputStreamReader;
    .restart local v7    # "is":Ljava/io/InputStream;
    :catchall_2
    move-exception v8

    move-object v4, v5

    .end local v5    # "ir":Ljava/io/InputStreamReader;
    .restart local v4    # "ir":Ljava/io/InputStreamReader;
    move-object v6, v7

    .end local v7    # "is":Ljava/io/InputStream;
    .restart local v6    # "is":Ljava/io/InputStream;
    goto :goto_0

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v4    # "ir":Ljava/io/InputStreamReader;
    .end local v6    # "is":Ljava/io/InputStream;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v5    # "ir":Ljava/io/InputStreamReader;
    .restart local v7    # "is":Ljava/io/InputStream;
    :catchall_3
    move-exception v8

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v4, v5

    .end local v5    # "ir":Ljava/io/InputStreamReader;
    .restart local v4    # "ir":Ljava/io/InputStreamReader;
    move-object v6, v7

    .end local v7    # "is":Ljava/io/InputStream;
    .restart local v6    # "is":Ljava/io/InputStream;
    goto :goto_0
.end method
