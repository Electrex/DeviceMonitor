.class Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor$2;
.super Lcom/stericson/roottools/execution/CommandCapture;
.source "CpuCoreMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->updateStates()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;

.field final synthetic val$outputCollector:Ljava/lang/StringBuilder;


# direct methods
.method varargs constructor <init>(Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;I[Ljava/lang/String;Ljava/lang/StringBuilder;)V
    .locals 0
    .param p2, "x0"    # I
    .param p3, "x1"    # [Ljava/lang/String;

    .prologue
    .line 127
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor$2;->this$0:Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;

    iput-object p4, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor$2;->val$outputCollector:Ljava/lang/StringBuilder;

    invoke-direct {p0, p2, p3}, Lcom/stericson/roottools/execution/CommandCapture;-><init>(I[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public commandCompleted(II)V
    .locals 14
    .param p1, "id"    # I
    .param p2, "exitcode"    # I

    .prologue
    .line 135
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor$2;->val$outputCollector:Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 136
    .local v6, "output":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "output: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    # getter for: Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mActivity:Landroid/app/Activity;
    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->access$200()Landroid/app/Activity;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 139
    const-string v9, " "

    invoke-virtual {v6, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 140
    .local v7, "parts":[Ljava/lang/String;
    new-instance v3, Ljava/util/ArrayList;

    # getter for: Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->CPU_COUNT:I
    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->access$300()I

    move-result v9

    invoke-direct {v3, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 141
    .local v3, "mCoreList":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/objects/CpuCore;>;"
    const/4 v5, 0x0

    .line 144
    .local v5, "mult":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    # getter for: Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->CPU_COUNT:I
    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->access$300()I

    move-result v9

    if-ge v1, v9, :cond_0

    .line 146
    add-int v9, v1, v5

    :try_start_0
    aget-object v4, v7, v9

    .line 147
    .local v4, "max":Ljava/lang/String;
    add-int v9, v1, v5

    add-int/lit8 v9, v9, 0x1

    aget-object v0, v7, v9

    .line 148
    .local v0, "current":Ljava/lang/String;
    new-instance v8, Lorg/namelessrom/devicecontrol/objects/CpuCore;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mActivity:Landroid/app/Activity;
    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->access$200()Landroid/app/Activity;

    move-result-object v10

    const v11, 0x7f0e0068

    invoke-virtual {v10, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/16 v10, 0x20

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/16 v10, 0x3a

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    add-int v10, v1, v5

    add-int/lit8 v10, v10, 0x2

    aget-object v10, v7, v10

    invoke-direct {v8, v9, v4, v0, v10}, Lorg/namelessrom/devicecontrol/objects/CpuCore;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    .end local v0    # "current":Ljava/lang/String;
    .end local v4    # "max":Ljava/lang/String;
    .local v8, "tmp":Lorg/namelessrom/devicecontrol/objects/CpuCore;
    :goto_1
    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    add-int/lit8 v5, v5, 0x2

    .line 144
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 155
    .end local v8    # "tmp":Lorg/namelessrom/devicecontrol/objects/CpuCore;
    :catch_0
    move-exception v2

    .line 156
    .local v2, "iob":Ljava/lang/IndexOutOfBoundsException;
    new-instance v8, Lorg/namelessrom/devicecontrol/objects/CpuCore;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mActivity:Landroid/app/Activity;
    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->access$200()Landroid/app/Activity;

    move-result-object v10

    const v11, 0x7f0e0068

    invoke-virtual {v10, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/16 v10, 0x20

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/16 v10, 0x3a

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "0"

    const-string v11, "0"

    const-string v12, "0"

    invoke-direct {v8, v9, v10, v11, v12}, Lorg/namelessrom/devicecontrol/objects/CpuCore;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .restart local v8    # "tmp":Lorg/namelessrom/devicecontrol/objects/CpuCore;
    goto :goto_1

    .line 167
    .end local v2    # "iob":Ljava/lang/IndexOutOfBoundsException;
    .end local v8    # "tmp":Lorg/namelessrom/devicecontrol/objects/CpuCore;
    :cond_0
    # getter for: Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mListener:Lorg/namelessrom/devicecontrol/cpu/CpuUtils$CoreListener;
    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->access$400()Lorg/namelessrom/devicecontrol/cpu/CpuUtils$CoreListener;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 168
    sget-object v9, Lorg/namelessrom/devicecontrol/Application;->HANDLER:Landroid/os/Handler;

    new-instance v10, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor$2$1;

    invoke-direct {v10, p0, v3}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor$2$1;-><init>(Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor$2;Ljava/util/List;)V

    invoke-virtual {v9, v10}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 178
    .end local v1    # "i":I
    .end local v3    # "mCoreList":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/objects/CpuCore;>;"
    .end local v5    # "mult":I
    .end local v7    # "parts":[Ljava/lang/String;
    :cond_1
    sget-object v9, Lorg/namelessrom/devicecontrol/Application;->HANDLER:Landroid/os/Handler;

    iget-object v10, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor$2;->this$0:Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;

    # getter for: Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mUpdater:Ljava/lang/Runnable;
    invoke-static {v10}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->access$500(Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;)Ljava/lang/Runnable;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 179
    sget-object v9, Lorg/namelessrom/devicecontrol/Application;->HANDLER:Landroid/os/Handler;

    iget-object v10, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor$2;->this$0:Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;

    # getter for: Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mUpdater:Ljava/lang/Runnable;
    invoke-static {v10}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->access$500(Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;)Ljava/lang/Runnable;

    move-result-object v10

    # getter for: Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->mInterval:I
    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->access$600()I

    move-result v11

    int-to-long v12, v11

    invoke-virtual {v9, v10, v12, v13}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 180
    return-void
.end method

.method public commandOutput(ILjava/lang/String;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "line"    # Ljava/lang/String;

    .prologue
    .line 130
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor$2;->val$outputCollector:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    return-void
.end method
