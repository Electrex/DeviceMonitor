.class public Lorg/namelessrom/devicecontrol/cpu/CpuUtils$State;
.super Ljava/lang/Object;
.source "CpuUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/cpu/CpuUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "State"
.end annotation


# instance fields
.field public final states:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;",
            ">;"
        }
    .end annotation
.end field

.field public final totalTime:J


# direct methods
.method public constructor <init>(Ljava/util/List;J)V
    .locals 0
    .param p2, "totalStateTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 83
    .local p1, "stateList":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$State;->states:Ljava/util/List;

    .line 85
    iput-wide p2, p0, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$State;->totalTime:J

    .line 86
    return-void
.end method
