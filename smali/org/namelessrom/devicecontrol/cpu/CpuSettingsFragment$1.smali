.class Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment$1;
.super Ljava/lang/Object;
.source "CpuSettingsFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;)V
    .locals 0

    .prologue
    .line 337
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment$1;->this$0:Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "button"    # Landroid/widget/CompoundButton;
    .param p2, "b"    # Z

    .prologue
    .line 339
    if-eqz p2, :cond_0

    .line 340
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment$1;->this$0:Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;

    # getter for: Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuInfo:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->access$000(Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 341
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment$1;->this$0:Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->getInstance(Landroid/app/Activity;)Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;

    move-result-object v0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment$1;->this$0:Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;

    const/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->start(Lorg/namelessrom/devicecontrol/cpu/CpuUtils$CoreListener;I)V

    .line 346
    :goto_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment$1;->this$0:Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    iput-boolean p2, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->perfCpuInfo:Z

    .line 347
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment$1;->this$0:Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment$1;->this$0:Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    .line 348
    return-void

    .line 343
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment$1;->this$0:Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->getInstance(Landroid/app/Activity;)Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuCoreMonitor;->stop()V

    .line 344
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment$1;->this$0:Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;

    # getter for: Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->mCpuInfo:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;->access$000(Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method
