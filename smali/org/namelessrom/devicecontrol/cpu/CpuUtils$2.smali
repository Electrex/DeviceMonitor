.class Lorg/namelessrom/devicecontrol/cpu/CpuUtils$2;
.super Lcom/stericson/roottools/execution/CommandCapture;
.source "CpuUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->getCpuFreq(Lorg/namelessrom/devicecontrol/cpu/CpuUtils$FrequencyListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

.field final synthetic val$listener:Lorg/namelessrom/devicecontrol/cpu/CpuUtils$FrequencyListener;

.field final synthetic val$outputCollector:Ljava/lang/StringBuilder;


# direct methods
.method varargs constructor <init>(Lorg/namelessrom/devicecontrol/cpu/CpuUtils;I[Ljava/lang/String;Ljava/lang/StringBuilder;Lorg/namelessrom/devicecontrol/cpu/CpuUtils$FrequencyListener;)V
    .locals 0
    .param p2, "x0"    # I
    .param p3, "x1"    # [Ljava/lang/String;

    .prologue
    .line 236
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$2;->this$0:Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    iput-object p4, p0, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$2;->val$outputCollector:Ljava/lang/StringBuilder;

    iput-object p5, p0, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$2;->val$listener:Lorg/namelessrom/devicecontrol/cpu/CpuUtils$FrequencyListener;

    invoke-direct {p0, p2, p3}, Lcom/stericson/roottools/execution/CommandCapture;-><init>(I[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public commandCompleted(II)V
    .locals 13
    .param p1, "id"    # I
    .param p2, "exitcode"    # I

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 245
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$2;->val$outputCollector:Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    .line 247
    .local v4, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 248
    .local v6, "tmpList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v7, ""

    .local v7, "tmpMax":Ljava/lang/String;
    const-string v8, ""

    .line 250
    .local v8, "tmpMin":Ljava/lang/String;
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v9

    if-gtz v9, :cond_0

    .line 276
    :goto_0
    return-void

    .line 254
    :cond_0
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 255
    .local v5, "s":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_1

    .line 258
    invoke-virtual {v5, v11}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x5b

    if-ne v9, v10, :cond_2

    .line 259
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v5, v12, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 260
    :cond_2
    invoke-virtual {v5, v11}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x5d

    if-ne v9, v10, :cond_3

    .line 261
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v5, v12, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    goto :goto_1

    .line 263
    :cond_3
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 267
    .end local v5    # "s":Ljava/lang/String;
    :cond_4
    move-object v2, v7

    .line 268
    .local v2, "max":Ljava/lang/String;
    move-object v3, v8

    .line 269
    .local v3, "min":Ljava/lang/String;
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    new-array v9, v9, [Ljava/lang/String;

    invoke-interface {v6, v9}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 270
    .local v0, "avail":[Ljava/lang/String;
    sget-object v9, Lorg/namelessrom/devicecontrol/Application;->HANDLER:Landroid/os/Handler;

    new-instance v10, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$2$1;

    invoke-direct {v10, p0, v0, v2, v3}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$2$1;-><init>(Lorg/namelessrom/devicecontrol/cpu/CpuUtils$2;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public commandOutput(ILjava/lang/String;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "line"    # Ljava/lang/String;

    .prologue
    .line 239
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$2;->val$outputCollector:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    const-class v0, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    invoke-static {v0, p2}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 241
    return-void
.end method
