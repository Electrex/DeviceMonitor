.class public Lorg/namelessrom/devicecontrol/cpu/CpuUtils;
.super Ljava/lang/Object;
.source "CpuUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/cpu/CpuUtils$StateListener;,
        Lorg/namelessrom/devicecontrol/cpu/CpuUtils$CoreListener;,
        Lorg/namelessrom/devicecontrol/cpu/CpuUtils$FrequencyListener;,
        Lorg/namelessrom/devicecontrol/cpu/CpuUtils$State;,
        Lorg/namelessrom/devicecontrol/cpu/CpuUtils$Cores;,
        Lorg/namelessrom/devicecontrol/cpu/CpuUtils$Frequency;
    }
.end annotation


# static fields
.field private static sInstance:Lorg/namelessrom/devicecontrol/cpu/CpuUtils;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get()Lorg/namelessrom/devicecontrol/cpu/CpuUtils;
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->sInstance:Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    if-nez v0, :cond_0

    .line 107
    new-instance v0, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->sInstance:Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    .line 109
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->sInstance:Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    return-object v0
.end method

.method public static toMhz(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "mhzString"    # Ljava/lang/String;

    .prologue
    .line 303
    const/4 v1, -0x1

    .line 304
    .local v1, "value":I
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 306
    :try_start_0
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v2

    div-int/lit16 v1, v2, 0x3e8
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    :cond_0
    :goto_0
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 314
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " MHz"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 316
    :goto_1
    return-object v2

    .line 307
    :catch_0
    move-exception v0

    .line 308
    .local v0, "exc":Ljava/lang/NumberFormatException;
    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->get()Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    move-result-object v2

    const-string v3, "toMhz"

    invoke-static {v2, v3, v0}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 309
    const/4 v1, -0x1

    goto :goto_0

    .line 316
    .end local v0    # "exc":Ljava/lang/NumberFormatException;
    :cond_1
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e006e

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method


# virtual methods
.method public getAvailableFrequencies(Z)[Ljava/lang/String;
    .locals 3
    .param p1, "sorted"    # Z

    .prologue
    .line 146
    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies"

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 147
    .local v1, "freqsRaw":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 148
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 149
    .local v0, "freqs":[Ljava/lang/String;
    if-nez p1, :cond_0

    .line 161
    .end local v0    # "freqs":[Ljava/lang/String;
    :goto_0
    return-object v0

    .line 152
    .restart local v0    # "freqs":[Ljava/lang/String;
    :cond_0
    new-instance v2, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$1;

    invoke-direct {v2, p0}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$1;-><init>(Lorg/namelessrom/devicecontrol/cpu/CpuUtils;)V

    invoke-static {v0, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 158
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    goto :goto_0

    .line 161
    .end local v0    # "freqs":[Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCpuFreq(Lorg/namelessrom/devicecontrol/cpu/CpuUtils$FrequencyListener;)V
    .locals 9
    .param p1, "listener"    # Lorg/namelessrom/devicecontrol/cpu/CpuUtils$FrequencyListener;

    .prologue
    .line 222
    const/4 v1, 0x1

    :try_start_0
    invoke-static {v1}, Lcom/stericson/roottools/RootTools;->getShell(Z)Lcom/stericson/roottools/execution/Shell;

    move-result-object v8

    .line 223
    .local v8, "mShell":Lcom/stericson/roottools/execution/Shell;
    if-nez v8, :cond_0

    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Shell is null"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 281
    .end local v8    # "mShell":Lcom/stericson/roottools/execution/Shell;
    :catch_0
    move-exception v7

    .line 282
    .local v7, "exc":Ljava/lang/Exception;
    const-class v1, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    .line 284
    .end local v7    # "exc":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 225
    .restart local v8    # "mShell":Lcom/stericson/roottools/execution/Shell;
    :cond_0
    :try_start_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 226
    .local v6, "cmd":Ljava/lang/StringBuilder;
    const-string v1, "command=$("

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    const-string v1, "cat "

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " 2> /dev/null;"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    const-string v1, "echo -n \"[\";"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    const-string v1, "cat "

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->getMaxCpuFrequencyPath(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " 2> /dev/null;"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    const-string v1, "echo -n \"]\";"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    const-string v1, "cat "

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->getMinCpuFrequencyPath(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " 2> /dev/null;"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    const-string v1, ");"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "echo $command | busybox tr -d \"\\n\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    const-class v1, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 235
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 236
    .local v4, "outputCollector":Ljava/lang/StringBuilder;
    new-instance v0, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$2;

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v1

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$2;-><init>(Lorg/namelessrom/devicecontrol/cpu/CpuUtils;I[Ljava/lang/String;Ljava/lang/StringBuilder;Lorg/namelessrom/devicecontrol/cpu/CpuUtils$FrequencyListener;)V

    .line 279
    .local v0, "cmdCapture":Lcom/stericson/roottools/execution/CommandCapture;
    invoke-virtual {v8}, Lcom/stericson/roottools/execution/Shell;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Shell is closed"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 280
    :cond_1
    invoke-virtual {v8, v0}, Lcom/stericson/roottools/execution/Shell;->add(Lcom/stericson/roottools/execution/Command;)Lcom/stericson/roottools/execution/Command;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public getCpuFrequencyPath(I)Ljava/lang/String;
    .locals 4
    .param p1, "cpu"    # I

    .prologue
    .line 113
    const-string v0, "/sys/devices/system/cpu/cpu%s/cpufreq/scaling_cur_freq"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCpuTemperature()I
    .locals 6

    .prologue
    const/16 v3, 0x64

    const/4 v1, -0x1

    .line 129
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v4

    const v5, 0x7f0e00f8

    invoke-virtual {v4, v5}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 130
    .local v2, "tmp":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 133
    :try_start_0
    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 138
    .local v1, "temp":I
    if-gez v1, :cond_0

    const/4 v1, 0x0

    .line 139
    :cond_0
    if-le v1, v3, :cond_1

    move v1, v3

    .line 142
    .end local v1    # "temp":I
    :cond_1
    :goto_0
    return v1

    .line 134
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "could not read cpu temperature"

    invoke-static {p0, v3, v0}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public getMaxCpuFrequencyPath(I)Ljava/lang/String;
    .locals 4
    .param p1, "cpu"    # I

    .prologue
    .line 117
    const-string v0, "/sys/devices/system/cpu/cpu%s/cpufreq/scaling_max_freq"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMinCpuFrequencyPath(I)Ljava/lang/String;
    .locals 4
    .param p1, "cpu"    # I

    .prologue
    .line 121
    const-string v0, "/sys/devices/system/cpu/cpu%s/cpufreq/scaling_min_freq"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNumOfCpus()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 170
    const/4 v2, 0x1

    .line 171
    .local v2, "numOfCpu":I
    const-string v4, "/sys/devices/system/cpu/present"

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 172
    .local v3, "numOfCpus":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 173
    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 174
    .local v0, "cpuCount":[Ljava/lang/String;
    array-length v4, v0

    if-le v4, v5, :cond_0

    .line 176
    const/4 v4, 0x1

    :try_start_0
    aget-object v4, v0, v4

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x0

    aget-object v5, v0, v5

    invoke-static {v5}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    sub-int/2addr v4, v5

    add-int/lit8 v2, v4, 0x1

    .line 177
    if-gez v2, :cond_0

    .line 178
    const/4 v2, 0x1

    .line 185
    .end local v0    # "cpuCount":[Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 180
    .restart local v0    # "cpuCount":[Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 181
    .local v1, "ex":Ljava/lang/NumberFormatException;
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public getOnlinePath(I)Ljava/lang/String;
    .locals 4
    .param p1, "cpu"    # I

    .prologue
    .line 125
    const-string v0, "/sys/devices/system/cpu/cpu%s/online"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onlineCpu(I)Ljava/lang/String;
    .locals 3
    .param p1, "cpu"    # I

    .prologue
    .line 287
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 288
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->getOnlinePath(I)Ljava/lang/String;

    move-result-object v0

    .line 289
    .local v0, "pathOnline":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 290
    const-string v2, "0"

    invoke-static {v0, v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    const-string v2, "1"

    invoke-static {v0, v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 293
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public restore(Landroid/content/Context;)Ljava/lang/String;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 189
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 191
    .local v5, "sbCmd":Ljava/lang/StringBuilder;
    invoke-static {p1}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    move-result-object v8

    const-string v9, "cpu"

    invoke-virtual {v8, v9}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->getItemsByCategory(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 196
    .local v3, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/namelessrom/devicecontrol/objects/BootupItem;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    .line 197
    .local v2, "item":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    const/4 v6, -0x1

    .line 198
    .local v6, "tmpInt":I
    iget-object v7, v2, Lorg/namelessrom/devicecontrol/objects/BootupItem;->name:Ljava/lang/String;

    .line 199
    .local v7, "tmpString":Ljava/lang/String;
    if-eqz v7, :cond_0

    const-string v8, "io"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 201
    :try_start_0
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v7, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 207
    :cond_0
    :goto_1
    const/4 v8, -0x1

    if-eq v6, v8, :cond_1

    .line 208
    invoke-virtual {p0, v6}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->getOnlinePath(I)Ljava/lang/String;

    move-result-object v4

    .line 209
    .local v4, "path":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 210
    const-string v8, "0"

    invoke-static {v4, v8}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    const-string v8, "1"

    invoke-static {v4, v8}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    .end local v4    # "path":Ljava/lang/String;
    :cond_1
    iget-object v8, v2, Lorg/namelessrom/devicecontrol/objects/BootupItem;->filename:Ljava/lang/String;

    iget-object v9, v2, Lorg/namelessrom/devicecontrol/objects/BootupItem;->value:Ljava/lang/String;

    invoke-static {v8, v9}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 203
    :catch_0
    move-exception v0

    .line 204
    .local v0, "exc":Ljava/lang/Exception;
    const/4 v6, -0x1

    goto :goto_1

    .line 217
    .end local v0    # "exc":Ljava/lang/Exception;
    .end local v2    # "item":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    .end local v6    # "tmpInt":I
    .end local v7    # "tmpString":Ljava/lang/String;
    :cond_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method
