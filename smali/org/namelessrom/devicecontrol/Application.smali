.class public Lorg/namelessrom/devicecontrol/Application;
.super Landroid/app/Application;
.source "Application.java"


# static fields
.field public static final HANDLER:Landroid/os/Handler;

.field public static IS_NAMELESS:Z

.field private static sAccentColor:I

.field private static sInstance:Lorg/namelessrom/devicecontrol/Application;

.field private static sIsDarkTheme:I

.field private static sPrimaryColor:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 44
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/Application;->HANDLER:Landroid/os/Handler;

    .line 46
    const/4 v0, 0x0

    sput-boolean v0, Lorg/namelessrom/devicecontrol/Application;->IS_NAMELESS:Z

    .line 50
    sput v1, Lorg/namelessrom/devicecontrol/Application;->sAccentColor:I

    .line 51
    sput v1, Lorg/namelessrom/devicecontrol/Application;->sPrimaryColor:I

    .line 52
    sput v1, Lorg/namelessrom/devicecontrol/Application;->sIsDarkTheme:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method public static get()Lorg/namelessrom/devicecontrol/Application;
    .locals 1

    .prologue
    .line 122
    sget-object v0, Lorg/namelessrom/devicecontrol/Application;->sInstance:Lorg/namelessrom/devicecontrol/Application;

    return-object v0
.end method


# virtual methods
.method public getAccentColor()I
    .locals 2

    .prologue
    .line 168
    sget v0, Lorg/namelessrom/devicecontrol/Application;->sAccentColor:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 169
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/Application;->isDarkTheme()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0a0004

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/Application;->getColor(I)I

    move-result v0

    :goto_0
    sput v0, Lorg/namelessrom/devicecontrol/Application;->sAccentColor:I

    .line 173
    :cond_0
    sget v0, Lorg/namelessrom/devicecontrol/Application;->sAccentColor:I

    return v0

    .line 169
    :cond_1
    const v0, 0x7f0a0005

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/Application;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method public getColor(I)I
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 160
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public getFilesDirectory()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SdCardPath"
        }
    .end annotation

    .prologue
    .line 125
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/Application;->getFilesDir()Ljava/io/File;

    move-result-object v0

    .line 126
    .local v0, "tmp":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 127
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 129
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/data/data/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/Application;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getPrimaryColor()I
    .locals 2

    .prologue
    .line 177
    sget v0, Lorg/namelessrom/devicecontrol/Application;->sPrimaryColor:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 178
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/Application;->isDarkTheme()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0a0029

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/Application;->getColor(I)I

    move-result v0

    :goto_0
    sput v0, Lorg/namelessrom/devicecontrol/Application;->sPrimaryColor:I

    .line 182
    :cond_0
    sget v0, Lorg/namelessrom/devicecontrol/Application;->sPrimaryColor:I

    return v0

    .line 178
    :cond_1
    const v0, 0x7f0a0042

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/Application;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method public getStringArray(I)[Ljava/lang/String;
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 164
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isDarkTheme()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 191
    sget v0, Lorg/namelessrom/devicecontrol/Application;->sIsDarkTheme:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    .line 192
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    iget-boolean v0, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->darkTheme:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    sput v0, Lorg/namelessrom/devicecontrol/Application;->sIsDarkTheme:I

    .line 194
    :cond_0
    sget v0, Lorg/namelessrom/devicecontrol/Application;->sIsDarkTheme:I

    if-ne v0, v1, :cond_2

    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 192
    goto :goto_0

    :cond_2
    move v1, v2

    .line 194
    goto :goto_1
.end method

.method public onCreate()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 55
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 56
    sput-object p0, Lorg/namelessrom/devicecontrol/Application;->sInstance:Lorg/namelessrom/devicecontrol/Application;

    .line 59
    invoke-static {v2}, Lorg/namelessrom/devicecontrol/Logger;->setEnabled(Z)V

    .line 61
    const-string v3, "/system/build.prop"

    const-string v4, "ro.nameless.debug=1"

    invoke-static {v3, v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->existsInFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 63
    new-instance v3, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v3}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    invoke-virtual {v3}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectAll()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectCustomSlowCalls()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectDiskReads()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectDiskWrites()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectNetwork()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyFlashScreen()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v3

    invoke-static {v3}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 74
    new-instance v1, Landroid/os/StrictMode$VmPolicy$Builder;

    invoke-direct {v1}, Landroid/os/StrictMode$VmPolicy$Builder;-><init>()V

    .line 75
    .local v1, "vmpolicy":Landroid/os/StrictMode$VmPolicy$Builder;
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-lt v3, v4, :cond_0

    .line 76
    invoke-virtual {v1}, Landroid/os/StrictMode$VmPolicy$Builder;->detectLeakedRegistrationObjects()Landroid/os/StrictMode$VmPolicy$Builder;

    .line 77
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x12

    if-lt v3, v4, :cond_0

    .line 78
    invoke-virtual {v1}, Landroid/os/StrictMode$VmPolicy$Builder;->detectFileUriExposure()Landroid/os/StrictMode$VmPolicy$Builder;

    .line 81
    :cond_0
    invoke-virtual {v1}, Landroid/os/StrictMode$VmPolicy$Builder;->detectAll()Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/StrictMode$VmPolicy$Builder;->detectActivityLeaks()Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/StrictMode$VmPolicy$Builder;->detectLeakedClosableObjects()Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/StrictMode$VmPolicy$Builder;->detectLeakedSqlLiteObjects()Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v3

    const-class v4, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    const/16 v5, 0x64

    invoke-virtual {v3, v4, v5}, Landroid/os/StrictMode$VmPolicy$Builder;->setClassInstanceLimit(Ljava/lang/Class;I)Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/StrictMode$VmPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$VmPolicy$Builder;

    .line 88
    invoke-virtual {v1}, Landroid/os/StrictMode$VmPolicy$Builder;->build()Landroid/os/StrictMode$VmPolicy;

    move-result-object v3

    invoke-static {v3}, Landroid/os/StrictMode;->setVmPolicy(Landroid/os/StrictMode$VmPolicy;)V

    .line 91
    sput-boolean v2, Lcom/stericson/roottools/RootTools;->debugMode:Z

    .line 95
    .end local v1    # "vmpolicy":Landroid/os/StrictMode$VmPolicy$Builder;
    :cond_1
    invoke-static {}, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->getInstance()Lorg/namelessrom/devicecontrol/database/DatabaseHandler;

    .line 98
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 99
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    .line 100
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    .line 101
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;

    .line 102
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    .line 103
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    .line 105
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v3

    iget-boolean v3, v3, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->extensiveLogging:Z

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/Logger;->setEnabled(Z)V

    .line 107
    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/Utils;->isNameless()Z

    move-result v3

    sput-boolean v3, Lorg/namelessrom/devicecontrol/Application;->IS_NAMELESS:Z

    .line 108
    const-string v3, "is nameless: %s"

    new-array v4, v2, [Ljava/lang/Object;

    sget-boolean v5, Lorg/namelessrom/devicecontrol/Application;->IS_NAMELESS:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    sget-boolean v3, Lorg/namelessrom/devicecontrol/Application;->IS_NAMELESS:Z

    if-eqz v3, :cond_2

    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v3

    iget-boolean v3, v3, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->showLauncher:Z

    if-eqz v3, :cond_3

    :cond_2
    move v0, v2

    .line 112
    .local v0, "showLauncher":Z
    :cond_3
    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/Application;->toggleLauncherIcon(Z)V

    .line 113
    return-void
.end method

.method public onTerminate()V
    .locals 0

    .prologue
    .line 118
    invoke-static {}, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->tearDown()V

    .line 119
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 120
    return-void
.end method

.method public setAccentColor(I)Lorg/namelessrom/devicecontrol/Application;
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 186
    sput p1, Lorg/namelessrom/devicecontrol/Application;->sAccentColor:I

    .line 187
    return-object p0
.end method

.method public setDarkTheme(Z)Lorg/namelessrom/devicecontrol/Application;
    .locals 2
    .param p1, "isDark"    # Z

    .prologue
    const/4 v1, -0x1

    .line 198
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput v0, Lorg/namelessrom/devicecontrol/Application;->sIsDarkTheme:I

    .line 199
    sput v1, Lorg/namelessrom/devicecontrol/Application;->sAccentColor:I

    .line 200
    sput v1, Lorg/namelessrom/devicecontrol/Application;->sPrimaryColor:I

    .line 202
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    iput-boolean p1, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->darkTheme:Z

    .line 203
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    .line 204
    return-object p0

    .line 198
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toggleLauncherIcon(Z)V
    .locals 6
    .param p1, "showLauncher"    # Z

    .prologue
    .line 134
    sget-boolean v3, Lorg/namelessrom/devicecontrol/Application;->IS_NAMELESS:Z

    if-eqz v3, :cond_1

    .line 135
    const-string v1, "com.android.settings"

    .line 138
    .local v1, "pkg":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.android.settings"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 145
    .local v2, "res":Landroid/content/res/Resources;
    if-nez p1, :cond_0

    if-eqz v2, :cond_0

    const-string v3, "device_control"

    const-string v4, "string"

    const-string v5, "com.android.settings"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_0

    .line 147
    const-string v3, "Implemented into system and showLauncher is not set!"

    invoke-static {p0, v3}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/Application;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lorg/namelessrom/devicecontrol/DummyLauncher;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->disableComponent(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    .end local v1    # "pkg":Ljava/lang/String;
    .end local v2    # "res":Landroid/content/res/Resources;
    :goto_0
    return-void

    .line 139
    .restart local v1    # "pkg":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 140
    .local v0, "exc":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "You dont have settings? That\'s weird."

    invoke-static {p0, v3, v0}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 141
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/Application;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lorg/namelessrom/devicecontrol/DummyLauncher;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->enableComponent(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 150
    .end local v0    # "exc":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v2    # "res":Landroid/content/res/Resources;
    :cond_0
    const-string v3, "Implemented into system and showLauncher is set!"

    invoke-static {p0, v3}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/Application;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lorg/namelessrom/devicecontrol/DummyLauncher;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->enableComponent(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 154
    .end local v1    # "pkg":Ljava/lang/String;
    .end local v2    # "res":Landroid/content/res/Resources;
    :cond_1
    const-string v3, "Not implemented into system!"

    invoke-static {p0, v3}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/Application;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lorg/namelessrom/devicecontrol/DummyLauncher;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->enableComponent(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
