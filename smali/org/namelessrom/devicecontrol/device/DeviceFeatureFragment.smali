.class public Lorg/namelessrom/devicecontrol/device/DeviceFeatureFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment;
.source "DeviceFeatureFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment;-><init>()V

    return-void
.end method

.method public static restore(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->restore(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 31
    const v0, 0x7f0e00db

    return v0
.end method

.method public getPagerAdapter()Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment$ViewPagerAdapter;
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 35
    .local v0, "fragments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/v4/app/Fragment;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 37
    .local v1, "titles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/CharSequence;>;"
    new-instance v2, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;

    invoke-direct {v2}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;-><init>()V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    const v2, 0x7f0e0116

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    new-instance v2, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;

    invoke-direct {v2}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;-><init>()V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    const v2, 0x7f0e014b

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    new-instance v2, Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment$ViewPagerAdapter;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-direct {v2, p0, v3, v0, v1}, Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment$ViewPagerAdapter;-><init>(Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment;Landroid/support/v4/app/FragmentManager;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-object v2
.end method
