.class Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;
.super Landroid/os/AsyncTask;
.source "DeviceInformationGeneralFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CpuInfoTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final category:Landroid/preference/PreferenceCategory;

.field private final cpuInfo:Lorg/namelessrom/devicecontrol/objects/CpuInfo;

.field final synthetic this$0:Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;


# direct methods
.method public constructor <init>(Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;Landroid/preference/PreferenceCategory;)V
    .locals 1
    .param p2, "category"    # Landroid/preference/PreferenceCategory;

    .prologue
    .line 178
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;->this$0:Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 179
    new-instance v0, Lorg/namelessrom/devicecontrol/objects/CpuInfo;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/objects/CpuInfo;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;->cpuInfo:Lorg/namelessrom/devicecontrol/objects/CpuInfo;

    .line 180
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;->category:Landroid/preference/PreferenceCategory;

    .line 181
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 1
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 184
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;->cpuInfo:Lorg/namelessrom/devicecontrol/objects/CpuInfo;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->feedWithInformation()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 174
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 5
    .param p1, "success"    # Ljava/lang/Boolean;

    .prologue
    .line 188
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;->category:Landroid/preference/PreferenceCategory;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;->cpuInfo:Lorg/namelessrom/devicecontrol/objects/CpuInfo;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;->this$0:Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;->category:Landroid/preference/PreferenceCategory;

    const-string v2, "cpu_hardware"

    const v3, 0x7f0e012d

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;->cpuInfo:Lorg/namelessrom/devicecontrol/objects/CpuInfo;

    iget-object v4, v4, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->hardware:Ljava/lang/String;

    # invokes: Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v0, v1, v2, v3, v4}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->access$000(Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 191
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;->this$0:Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;->category:Landroid/preference/PreferenceCategory;

    const-string v2, "cpu_processor"

    const v3, 0x7f0e01a4

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;->cpuInfo:Lorg/namelessrom/devicecontrol/objects/CpuInfo;

    iget-object v4, v4, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->processor:Ljava/lang/String;

    # invokes: Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v0, v1, v2, v3, v4}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->access$000(Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 192
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;->this$0:Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;->category:Landroid/preference/PreferenceCategory;

    const-string v2, "cpu_features"

    const v3, 0x7f0e00db

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;->cpuInfo:Lorg/namelessrom/devicecontrol/objects/CpuInfo;

    iget-object v4, v4, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->features:Ljava/lang/String;

    # invokes: Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v0, v1, v2, v3, v4}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->access$000(Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 193
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;->this$0:Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;->category:Landroid/preference/PreferenceCategory;

    const-string v2, "cpu_bogomips"

    const v3, 0x7f0e0039

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;->cpuInfo:Lorg/namelessrom/devicecontrol/objects/CpuInfo;

    iget-object v4, v4, Lorg/namelessrom/devicecontrol/objects/CpuInfo;->bogomips:Ljava/lang/String;

    # invokes: Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v0, v1, v2, v3, v4}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->access$000(Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 195
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 174
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
