.class public Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;
.super Landroid/support/v4/preference/PreferenceFragment;
.source "DeviceInformationGeneralFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$MemoryInfoTask;,
        Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;,
        Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;
    }
.end annotation


# instance fields
.field private mEasterEggStarted:Z

.field private mHits:[J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/support/v4/preference/PreferenceFragment;-><init>()V

    .line 47
    const/4 v0, 0x3

    new-array v0, v0, [J

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->mHits:[J

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->mEasterEggStarted:Z

    .line 224
    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;
    .param p1, "x1"    # Landroid/preference/PreferenceCategory;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I
    .param p4, "x4"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v0

    return-object v0
.end method

.method private addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    .locals 1
    .param p1, "category"    # Landroid/preference/PreferenceCategory;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "titleResId"    # I
    .param p4, "summary"    # Ljava/lang/String;

    .prologue
    .line 142
    invoke-virtual {p0, p3}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, p4}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v0

    return-object v0
.end method

.method private addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    .locals 2
    .param p1, "category"    # Landroid/preference/PreferenceCategory;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "summary"    # Ljava/lang/String;

    .prologue
    .line 147
    new-instance v0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;-><init>(Landroid/content/Context;)V

    .line 148
    .local v0, "preference":Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-virtual {v0, p2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setKey(Ljava/lang/String;)V

    .line 149
    invoke-virtual {v0, p3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 150
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0e022e

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->getString(I)Ljava/lang/String;

    move-result-object p4

    .end local p4    # "summary":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0, p4}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 151
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 152
    return-object v0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v12, 0x7f0e0235

    const v11, 0x7f0e0226

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 60
    invoke-super {p0, p1}, Landroid/support/v4/preference/PreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 61
    const v6, 0x7f060007

    invoke-virtual {p0, v6}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreferencesFromResource(I)V

    .line 63
    invoke-static {}, Lorg/namelessrom/devicecontrol/Device;->get()Lorg/namelessrom/devicecontrol/Device;

    move-result-object v3

    .line 66
    .local v3, "device":Lorg/namelessrom/devicecontrol/Device;
    const-string v6, "platform"

    invoke-virtual {p0, v6}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceCategory;

    .line 68
    .local v1, "category":Landroid/preference/PreferenceCategory;
    const-string v6, "platform_version"

    iget-object v7, v3, Lorg/namelessrom/devicecontrol/Device;->platformVersion:Ljava/lang/String;

    invoke-direct {p0, v1, v6, v12, v7}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v6

    invoke-virtual {v6, v10}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSelectable(Z)V

    .line 70
    const-string v6, "platform_id"

    const v7, 0x7f0e003e

    iget-object v8, v3, Lorg/namelessrom/devicecontrol/Device;->platformId:Ljava/lang/String;

    invoke-direct {p0, v1, v6, v7, v8}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 71
    const-string v6, "platform_type"

    iget-object v7, v3, Lorg/namelessrom/devicecontrol/Device;->platformType:Ljava/lang/String;

    invoke-direct {p0, v1, v6, v11, v7}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 72
    const-string v6, "platform_tags"

    const v7, 0x7f0e020d

    iget-object v8, v3, Lorg/namelessrom/devicecontrol/Device;->platformTags:Ljava/lang/String;

    invoke-direct {p0, v1, v6, v7, v8}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 73
    const-string v6, "platform_build_date"

    const v7, 0x7f0e003d

    iget-object v8, v3, Lorg/namelessrom/devicecontrol/Device;->platformBuildType:Ljava/lang/String;

    invoke-direct {p0, v1, v6, v7, v8}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 77
    const-string v6, "runtime"

    invoke-virtual {p0, v6}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .end local v1    # "category":Landroid/preference/PreferenceCategory;
    check-cast v1, Landroid/preference/PreferenceCategory;

    .line 79
    .restart local v1    # "category":Landroid/preference/PreferenceCategory;
    const-string v6, "vm_library"

    iget-object v7, v3, Lorg/namelessrom/devicecontrol/Device;->vmLibrary:Ljava/lang/String;

    invoke-direct {p0, v1, v6, v11, v7}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 80
    const-string v6, "vm_version"

    iget-object v7, v3, Lorg/namelessrom/devicecontrol/Device;->vmVersion:Ljava/lang/String;

    invoke-direct {p0, v1, v6, v12, v7}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 83
    const-string v6, "device_information"

    invoke-virtual {p0, v6}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .end local v1    # "category":Landroid/preference/PreferenceCategory;
    check-cast v1, Landroid/preference/PreferenceCategory;

    .line 86
    .restart local v1    # "category":Landroid/preference/PreferenceCategory;
    const-string v6, "android_id"

    const v7, 0x7f0e0020

    iget-object v8, v3, Lorg/namelessrom/devicecontrol/Device;->androidId:Ljava/lang/String;

    invoke-direct {p0, v1, v6, v7, v8}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 87
    const-string v6, "device_manufacturer"

    const v7, 0x7f0e0158

    iget-object v8, v3, Lorg/namelessrom/devicecontrol/Device;->manufacturer:Ljava/lang/String;

    invoke-direct {p0, v1, v6, v7, v8}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 88
    const-string v6, "device_model"

    const v7, 0x7f0e0164

    iget-object v8, v3, Lorg/namelessrom/devicecontrol/Device;->model:Ljava/lang/String;

    invoke-direct {p0, v1, v6, v7, v8}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 89
    const-string v6, "device_product"

    const v7, 0x7f0e01a5

    iget-object v8, v3, Lorg/namelessrom/devicecontrol/Device;->product:Ljava/lang/String;

    invoke-direct {p0, v1, v6, v7, v8}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 90
    const-string v6, "device_board"

    const v7, 0x7f0e0038

    iget-object v8, v3, Lorg/namelessrom/devicecontrol/Device;->board:Ljava/lang/String;

    invoke-direct {p0, v1, v6, v7, v8}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 91
    const-string v6, "device_bootloader"

    const v7, 0x7f0e003b

    iget-object v8, v3, Lorg/namelessrom/devicecontrol/Device;->bootloader:Ljava/lang/String;

    invoke-direct {p0, v1, v6, v7, v8}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 92
    const-string v6, "device_radio_version"

    const v7, 0x7f0e01a6

    iget-object v8, v3, Lorg/namelessrom/devicecontrol/Device;->radio:Ljava/lang/String;

    invoke-direct {p0, v1, v6, v7, v8}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 95
    const-string v6, "emmc"

    invoke-virtual {p0, v6}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .end local v1    # "category":Landroid/preference/PreferenceCategory;
    check-cast v1, Landroid/preference/PreferenceCategory;

    .line 96
    .restart local v1    # "category":Landroid/preference/PreferenceCategory;
    const-string v6, "emmc_name"

    const v7, 0x7f0e016e

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/Emmc;->get()Lorg/namelessrom/devicecontrol/hardware/Emmc;

    move-result-object v8

    invoke-virtual {v8}, Lorg/namelessrom/devicecontrol/hardware/Emmc;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v1, v6, v7, v8}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 97
    const-string v6, "emmc_cid"

    const v7, 0x7f0e00b4

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/Emmc;->get()Lorg/namelessrom/devicecontrol/hardware/Emmc;

    move-result-object v8

    invoke-virtual {v8}, Lorg/namelessrom/devicecontrol/hardware/Emmc;->getCid()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v1, v6, v7, v8}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 98
    const-string v6, "emmc_mid"

    const v7, 0x7f0e00b6

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/Emmc;->get()Lorg/namelessrom/devicecontrol/hardware/Emmc;

    move-result-object v8

    invoke-virtual {v8}, Lorg/namelessrom/devicecontrol/hardware/Emmc;->getMid()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v1, v6, v7, v8}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 99
    const-string v6, "emmc_rev"

    const v7, 0x7f0e00b7

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/Emmc;->get()Lorg/namelessrom/devicecontrol/hardware/Emmc;

    move-result-object v8

    invoke-virtual {v8}, Lorg/namelessrom/devicecontrol/hardware/Emmc;->getRev()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v1, v6, v7, v8}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 100
    const-string v6, "emmc_date"

    const v7, 0x7f0e00b5

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/Emmc;->get()Lorg/namelessrom/devicecontrol/hardware/Emmc;

    move-result-object v8

    invoke-virtual {v8}, Lorg/namelessrom/devicecontrol/hardware/Emmc;->getDate()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v1, v6, v7, v8}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 101
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/Emmc;->get()Lorg/namelessrom/devicecontrol/hardware/Emmc;

    move-result-object v6

    invoke-virtual {v6}, Lorg/namelessrom/devicecontrol/hardware/Emmc;->canBrick()Z

    move-result v6

    if-eqz v6, :cond_0

    const v6, 0x7f0e00b3

    invoke-virtual {p0, v6}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 104
    .local v5, "tmp":Ljava/lang/String;
    :goto_0
    const-string v6, "%s\n%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v5, v7, v9

    const v8, 0x7f0e01a1

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 105
    const-string v6, "emmc_can_brick"

    const v7, 0x7f0e00b1

    invoke-direct {p0, v1, v6, v7, v5}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v6

    invoke-virtual {v6, v10}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSelectable(Z)V

    .line 108
    const-string v6, "processor"

    invoke-virtual {p0, v6}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .end local v1    # "category":Landroid/preference/PreferenceCategory;
    check-cast v1, Landroid/preference/PreferenceCategory;

    .line 110
    .restart local v1    # "category":Landroid/preference/PreferenceCategory;
    const v6, 0x7f0e0072

    invoke-virtual {p0, v6}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 111
    .local v2, "cpuAbi":Ljava/lang/String;
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x15

    if-lt v6, v7, :cond_2

    .line 112
    sget-object v6, Landroid/os/Build;->SUPPORTED_64_BIT_ABIS:[Ljava/lang/String;

    array-length v6, v6

    if-nez v6, :cond_1

    const v0, 0x7f0e0035

    .line 114
    .local v0, "bitResId":I
    :goto_1
    const-string v6, "cpu_bit"

    const v7, 0x7f0e002f

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v1, v6, v7, v8}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 115
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    sget-object v6, Landroid/os/Build;->SUPPORTED_ABIS:[Ljava/lang/String;

    array-length v6, v6

    if-ge v4, v6, :cond_3

    .line 116
    const-string v6, "cpu_abi%s"

    new-array v7, v10, [Ljava/lang/Object;

    add-int/lit8 v8, v4, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    add-int/lit8 v8, v4, 0x1

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Landroid/os/Build;->SUPPORTED_ABIS:[Ljava/lang/String;

    aget-object v8, v8, v4

    invoke-direct {p0, v1, v6, v7, v8}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 115
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 101
    .end local v0    # "bitResId":I
    .end local v2    # "cpuAbi":Ljava/lang/String;
    .end local v4    # "i":I
    .end local v5    # "tmp":Ljava/lang/String;
    :cond_0
    const v6, 0x7f0e00b2

    invoke-virtual {p0, v6}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 112
    .restart local v2    # "cpuAbi":Ljava/lang/String;
    .restart local v5    # "tmp":Ljava/lang/String;
    :cond_1
    const v0, 0x7f0e0036

    goto :goto_1

    .line 121
    :cond_2
    const-string v6, "cpu_abi"

    sget-object v7, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-direct {p0, v1, v6, v2, v7}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 123
    const-string v6, "cpu_abi2"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "2"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-direct {p0, v1, v6, v7, v8}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 125
    :cond_3
    new-instance v6, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;

    invoke-direct {v6, p0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;-><init>(Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;Landroid/preference/PreferenceCategory;)V

    new-array v7, v9, [Ljava/lang/Void;

    invoke-virtual {v6, v7}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$CpuInfoTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 128
    const-string v6, "kernel"

    invoke-virtual {p0, v6}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .end local v1    # "category":Landroid/preference/PreferenceCategory;
    check-cast v1, Landroid/preference/PreferenceCategory;

    .line 129
    .restart local v1    # "category":Landroid/preference/PreferenceCategory;
    new-instance v6, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;

    invoke-direct {v6, p0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;-><init>(Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;Landroid/preference/PreferenceCategory;)V

    new-array v7, v9, [Ljava/lang/Void;

    invoke-virtual {v6, v7}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 132
    const-string v6, "memory"

    invoke-virtual {p0, v6}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .end local v1    # "category":Landroid/preference/PreferenceCategory;
    check-cast v1, Landroid/preference/PreferenceCategory;

    .line 133
    .restart local v1    # "category":Landroid/preference/PreferenceCategory;
    new-instance v6, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$MemoryInfoTask;

    invoke-direct {v6, p0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$MemoryInfoTask;-><init>(Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;Landroid/preference/PreferenceCategory;)V

    new-array v7, v9, [Ljava/lang/Void;

    invoke-virtual {v6, v7}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$MemoryInfoTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 135
    invoke-virtual {v1}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v6

    if-nez v6, :cond_4

    .line 136
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 138
    :cond_4
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 8
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 157
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 158
    .local v0, "key":Ljava/lang/String;
    iget-boolean v2, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->mEasterEggStarted:Z

    if-nez v2, :cond_1

    const-string v2, "platform_version"

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 159
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->mHits:[J

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->mHits:[J

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->mHits:[J

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v2, v1, v3, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 160
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->mHits:[J

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->mHits:[J

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    aput-wide v4, v2, v3

    .line 161
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->mHits:[J

    aget-wide v2, v2, v6

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x1f4

    sub-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    .line 162
    const-string v2, "am start android/com.android.internal.app.PlatLogoActivity"

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;)V

    .line 163
    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->mEasterEggStarted:Z

    .line 171
    :cond_0
    :goto_0
    return v1

    .line 166
    :cond_1
    const-string v2, "emmc_can_brick"

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 167
    const-string v2, "http://wiki.cyanogenmod.org/w/EMMC_Bugs"

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->viewInBrowser(Ljava/lang/String;)V

    goto :goto_0

    .line 171
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/support/v4/preference/PreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v1

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 55
    invoke-super {p0}, Landroid/support/v4/preference/PreferenceFragment;->onResume()V

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->mEasterEggStarted:Z

    .line 57
    return-void
.end method
