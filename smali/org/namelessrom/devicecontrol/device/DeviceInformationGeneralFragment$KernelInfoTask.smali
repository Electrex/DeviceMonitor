.class Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;
.super Landroid/os/AsyncTask;
.source "DeviceInformationGeneralFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "KernelInfoTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final category:Landroid/preference/PreferenceCategory;

.field private final kernelInfo:Lorg/namelessrom/devicecontrol/objects/KernelInfo;

.field final synthetic this$0:Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;


# direct methods
.method public constructor <init>(Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;Landroid/preference/PreferenceCategory;)V
    .locals 1
    .param p2, "category"    # Landroid/preference/PreferenceCategory;

    .prologue
    .line 202
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->this$0:Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 203
    new-instance v0, Lorg/namelessrom/devicecontrol/objects/KernelInfo;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/objects/KernelInfo;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->kernelInfo:Lorg/namelessrom/devicecontrol/objects/KernelInfo;

    .line 204
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->category:Landroid/preference/PreferenceCategory;

    .line 205
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 1
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 208
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->kernelInfo:Lorg/namelessrom/devicecontrol/objects/KernelInfo;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->feedWithInformation()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 198
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 8
    .param p1, "success"    # Ljava/lang/Boolean;

    .prologue
    .line 212
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->category:Landroid/preference/PreferenceCategory;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->kernelInfo:Lorg/namelessrom/devicecontrol/objects/KernelInfo;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->this$0:Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->category:Landroid/preference/PreferenceCategory;

    const-string v2, "kernel_version"

    const v3, 0x7f0e0235

    const-string v4, "%s %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->kernelInfo:Lorg/namelessrom/devicecontrol/objects/KernelInfo;

    iget-object v7, v7, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->version:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->kernelInfo:Lorg/namelessrom/devicecontrol/objects/KernelInfo;

    iget-object v7, v7, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->revision:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    # invokes: Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v0, v1, v2, v3, v4}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->access$000(Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 216
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->this$0:Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->category:Landroid/preference/PreferenceCategory;

    const-string v2, "kernel_extras"

    const v3, 0x7f0e00d2

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->kernelInfo:Lorg/namelessrom/devicecontrol/objects/KernelInfo;

    iget-object v4, v4, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->extras:Ljava/lang/String;

    # invokes: Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v0, v1, v2, v3, v4}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->access$000(Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 217
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->this$0:Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->category:Landroid/preference/PreferenceCategory;

    const-string v2, "kernel_gcc"

    const v3, 0x7f0e021c

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->kernelInfo:Lorg/namelessrom/devicecontrol/objects/KernelInfo;

    iget-object v4, v4, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->gcc:Ljava/lang/String;

    # invokes: Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v0, v1, v2, v3, v4}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->access$000(Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 218
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->this$0:Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->category:Landroid/preference/PreferenceCategory;

    const-string v2, "kernel_date"

    const v3, 0x7f0e003d

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->kernelInfo:Lorg/namelessrom/devicecontrol/objects/KernelInfo;

    iget-object v4, v4, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->date:Ljava/lang/String;

    # invokes: Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v0, v1, v2, v3, v4}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->access$000(Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 219
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->this$0:Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->category:Landroid/preference/PreferenceCategory;

    const-string v2, "kernel_host"

    const v3, 0x7f0e0136

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->kernelInfo:Lorg/namelessrom/devicecontrol/objects/KernelInfo;

    iget-object v4, v4, Lorg/namelessrom/devicecontrol/objects/KernelInfo;->host:Ljava/lang/String;

    # invokes: Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v0, v1, v2, v3, v4}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->access$000(Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 221
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 198
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$KernelInfoTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
