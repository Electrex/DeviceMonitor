.class public Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/CustomPreferenceFragment;
.source "DeviceFeatureKernelFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private mEntropy:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mKsm:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mMcPowerScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

.field private mMsmDcvs:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

.field private mPowerEfficientWork:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

.field private mRoot:Landroid/preference/PreferenceScreen;

.field private mTcpCongestion:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

.field private mUksm:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mVoltageControl:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/CustomPreferenceFragment;-><init>()V

    return-void
.end method

.method private buildExtraCategory(Landroid/preference/PreferenceCategory;)V
    .locals 4
    .param p1, "category"    # Landroid/preference/PreferenceCategory;

    .prologue
    .line 162
    const-string v2, "tcp_congestion_control"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mTcpCongestion:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    .line 164
    const-string v2, "/proc/sys/net/ipv4/tcp_available_congestion_control"

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->readFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 165
    .local v1, "tmp":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 167
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 169
    .local v0, "tcp_avail":[Ljava/lang/String;
    const-string v2, "/proc/sys/net/ipv4/tcp_congestion_control"

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->readFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 170
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 171
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 172
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mTcpCongestion:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v2, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 173
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mTcpCongestion:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v2, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 174
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mTcpCongestion:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v2, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mTcpCongestion:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v2, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 176
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mTcpCongestion:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 181
    .end local v0    # "tcp_avail":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 179
    :cond_1
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mTcpCongestion:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {p1, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method private removeIfEmpty(Landroid/preference/PreferenceGroup;)V
    .locals 1
    .param p1, "preferenceGroup"    # Landroid/preference/PreferenceGroup;

    .prologue
    .line 184
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mRoot:Landroid/preference/PreferenceScreen;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 185
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mRoot:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 187
    :cond_0
    return-void
.end method

.method public static restore(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 241
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 243
    .local v3, "sbCmd":Ljava/lang/StringBuilder;
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    move-result-object v4

    const-string v5, "extras"

    invoke-virtual {v4, v5}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->getItemsByCategory(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 246
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/namelessrom/devicecontrol/objects/BootupItem;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    .line 247
    .local v1, "item":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    const-string v4, "/system/bin/mpdecision"

    iget-object v5, v1, Lorg/namelessrom/devicecontrol/objects/BootupItem;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 248
    new-instance v4, Lorg/namelessrom/devicecontrol/actions/extras/MpDecisionAction;

    iget-object v5, v1, Lorg/namelessrom/devicecontrol/objects/BootupItem;->value:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lorg/namelessrom/devicecontrol/actions/extras/MpDecisionAction;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v4}, Lorg/namelessrom/devicecontrol/actions/extras/MpDecisionAction;->triggerAction()V

    goto :goto_0

    .line 250
    :cond_0
    iget-object v4, v1, Lorg/namelessrom/devicecontrol/objects/BootupItem;->name:Ljava/lang/String;

    iget-object v5, v1, Lorg/namelessrom/devicecontrol/objects/BootupItem;->value:Ljava/lang/String;

    invoke-static {v4, v5}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 254
    .end local v1    # "item":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 80
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/CustomPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 81
    const v1, 0x7f060006

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->addPreferencesFromResource(I)V

    .line 82
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mRoot:Landroid/preference/PreferenceScreen;

    .line 87
    const-string v1, "kernel_features"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 89
    .local v0, "category":Landroid/preference/PreferenceCategory;
    const-string v1, "entropy"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mEntropy:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 90
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mEntropy:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 92
    const-string v1, "ksm"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mKsm:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 93
    const-string v1, "/sys/kernel/mm/ksm/"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 94
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mKsm:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 99
    :goto_0
    const-string v1, "uksm"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mUksm:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 100
    const-string v1, "/sys/kernel/mm/uksm/"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 101
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mUksm:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 106
    :goto_1
    invoke-direct {p0, v0}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->removeIfEmpty(Landroid/preference/PreferenceGroup;)V

    .line 111
    const-string v1, "powersaving"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .end local v0    # "category":Landroid/preference/PreferenceCategory;
    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 112
    .restart local v0    # "category":Landroid/preference/PreferenceCategory;
    const-string v1, "power_efficient_work"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mPowerEfficientWork:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 113
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mPowerEfficientWork:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 114
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mPowerEfficientWork:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 115
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mPowerEfficientWork:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 120
    :goto_2
    const-string v1, "sched_mc_power_savings"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mMcPowerScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

    .line 121
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mMcPowerScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->isSupported()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 122
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mMcPowerScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->initValue()V

    .line 123
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mMcPowerScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mMcPowerScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mMcPowerScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 128
    :goto_3
    invoke-direct {p0, v0}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->removeIfEmpty(Landroid/preference/PreferenceGroup;)V

    .line 133
    const-string v1, "voltage"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .end local v0    # "category":Landroid/preference/PreferenceCategory;
    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 134
    .restart local v0    # "category":Landroid/preference/PreferenceCategory;
    const-string v1, "msm_dcvs"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mMsmDcvs:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 135
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mMsmDcvs:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 136
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mMsmDcvs:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 137
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mMsmDcvs:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 142
    :goto_4
    const-string v1, "voltage_control"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mVoltageControl:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 143
    const-string v1, "/sys/devices/system/cpu/cpufreq/vdd_table/vdd_levels"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 145
    :cond_0
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mVoltageControl:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 149
    :goto_5
    invoke-direct {p0, v0}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->removeIfEmpty(Landroid/preference/PreferenceGroup;)V

    .line 154
    const-string v1, "extras"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .end local v0    # "category":Landroid/preference/PreferenceCategory;
    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 155
    .restart local v0    # "category":Landroid/preference/PreferenceCategory;
    invoke-direct {p0, v0}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->buildExtraCategory(Landroid/preference/PreferenceCategory;)V

    .line 156
    invoke-direct {p0, v0}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->removeIfEmpty(Landroid/preference/PreferenceGroup;)V

    .line 158
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mRoot:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->isSupported(Landroid/preference/PreferenceScreen;Landroid/content/Context;)V

    .line 159
    return-void

    .line 96
    :cond_1
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mKsm:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    .line 103
    :cond_2
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mUksm:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_1

    .line 117
    :cond_3
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mPowerEfficientWork:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_2

    .line 126
    :cond_4
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mMcPowerScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_3

    .line 139
    :cond_5
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mMsmDcvs:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_4

    .line 147
    :cond_6
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mVoltageControl:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_5
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 8
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    .line 208
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mPowerEfficientWork:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    if-ne p1, v0, :cond_1

    .line 209
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mPowerEfficientWork:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->writeValue(Z)V

    .line 233
    :cond_0
    :goto_0
    return v5

    .line 211
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_1
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mMcPowerScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

    if-ne p1, v0, :cond_2

    .line 212
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 213
    .local v4, "value":Ljava/lang/String;
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mMcPowerScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

    invoke-virtual {v0, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->writeValue(Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mMcPowerScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mMcPowerScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v1

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 217
    .local v6, "summary":Ljava/lang/String;
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mMcPowerScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

    invoke-virtual {v0, v6}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 220
    .end local v4    # "value":Ljava/lang/String;
    .end local v6    # "summary":Ljava/lang/String;
    :cond_2
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mMsmDcvs:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    if-ne p1, v0, :cond_3

    .line 221
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mMsmDcvs:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->writeValue(Z)V

    goto :goto_0

    .line 223
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_3
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mTcpCongestion:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    if-ne p1, v0, :cond_4

    .line 224
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 225
    .restart local v4    # "value":Ljava/lang/String;
    const-string v0, "/proc/sys/net/ipv4/tcp_congestion_control"

    invoke-static {v0, v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->writeValue(Ljava/lang/String;Ljava/lang/String;)Z

    .line 226
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    new-instance v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    const-string v1, "extras"

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mTcpCongestion:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "/proc/sys/net/ipv4/tcp_congestion_control"

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/objects/BootupItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v7, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->setBootup(Landroid/content/Context;Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 229
    invoke-virtual {p1, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 233
    .end local v4    # "value":Ljava/lang/String;
    :cond_4
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v0, 0x1

    .line 190
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mVoltageControl:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne v1, p1, :cond_0

    .line 191
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0e0653

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/MainActivity;->loadFragment(Landroid/app/Activity;I)V

    .line 204
    :goto_0
    return v0

    .line 193
    :cond_0
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mKsm:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne v1, p1, :cond_1

    .line 194
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0e058b

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/MainActivity;->loadFragment(Landroid/app/Activity;I)V

    goto :goto_0

    .line 196
    :cond_1
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mUksm:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne v1, p1, :cond_2

    .line 197
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0e05ef

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/MainActivity;->loadFragment(Landroid/app/Activity;I)V

    goto :goto_0

    .line 199
    :cond_2
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->mEntropy:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne v1, p1, :cond_3

    .line 200
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0e06b7

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/MainActivity;->loadFragment(Landroid/app/Activity;I)V

    goto :goto_0

    .line 204
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
