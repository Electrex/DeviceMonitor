.class public Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/CustomPreferenceFragment;
.source "DeviceFeatureGeneralFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;


# static fields
.field private static final SOUND_CONTROL_PATHS:[Ljava/lang/String;


# instance fields
.field private mFastCharge:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mGloveMode:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

.field private mPanelColor:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

.field private mSoundControl:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 56
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "/sys/devices/virtual/misc/soundcontrol"

    aput-object v2, v0, v1

    sput-object v0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->SOUND_CONTROL_PATHS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/CustomPreferenceFragment;-><init>()V

    return-void
.end method

.method private enableHts(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 340
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mGloveMode:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mGloveMode:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setEnabled(Z)V

    .line 341
    :cond_0
    if-eqz p1, :cond_1

    const-string v0, "glove_mode,1"

    .line 342
    .local v0, "mode":Ljava/lang/String;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/sys/class/sec/tsp/cmd"

    invoke-static {v2, v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/sys/class/sec/tsp/cmd_result"

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->getReadCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->getCommandResult(Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;Ljava/lang/String;)V

    .line 344
    return-void

    .line 341
    .end local v0    # "mode":Ljava/lang/String;
    :cond_1
    const-string v0, "glove_mode,0"

    goto :goto_0
.end method

.method private isHtsSupported()Z
    .locals 6

    .prologue
    .line 359
    new-instance v1, Ljava/io/File;

    const-string v4, "/sys/class/sec/tsp/cmd"

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 362
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 363
    const/4 v2, 0x0

    .line 365
    .local v2, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    const-string v5, "/sys/class/sec/tsp/cmd_list"

    invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 367
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .local v3, "reader":Ljava/io/BufferedReader;
    :cond_0
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .local v0, "currentLine":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 368
    const-string v4, "glove_mode"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 369
    const-class v4, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;

    const-string v5, "Glove mode / high touch sensitivity supported"

    invoke-static {v4, v5}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 371
    const/4 v4, 0x1

    .line 378
    if-eqz v3, :cond_1

    .line 379
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 390
    .end local v0    # "currentLine":Ljava/lang/String;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    return v4

    .line 378
    .restart local v0    # "currentLine":Ljava/lang/String;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :cond_2
    if-eqz v3, :cond_3

    .line 379
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 387
    .end local v0    # "currentLine":Ljava/lang/String;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    :cond_3
    :goto_1
    const-class v4, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;

    const-string v5, "Glove mode / high touch sensitivity NOT supported"

    invoke-static {v4, v5}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 390
    const/4 v4, 0x0

    goto :goto_0

    .line 374
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v4

    .line 378
    :goto_2
    if-eqz v2, :cond_3

    .line 379
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 381
    :catch_1
    move-exception v4

    goto :goto_1

    .line 377
    :catchall_0
    move-exception v4

    .line 378
    :goto_3
    if-eqz v2, :cond_4

    .line 379
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 383
    :cond_4
    :goto_4
    throw v4

    .line 381
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v0    # "currentLine":Ljava/lang/String;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_2
    move-exception v5

    goto :goto_0

    :catch_3
    move-exception v4

    goto :goto_1

    .end local v0    # "currentLine":Ljava/lang/String;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v5

    goto :goto_4

    .line 377
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_3

    .line 374
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_5
    move-exception v4

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_2
.end method

.method public static restore(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 317
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 319
    .local v4, "sbCmd":Ljava/lang/StringBuilder;
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    move-result-object v5

    const-string v6, "device"

    invoke-virtual {v5, v6}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->getItemsByCategory(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 322
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/namelessrom/devicecontrol/objects/BootupItem;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    .line 323
    .local v1, "item":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    const-string v5, "input_glove_mode"

    iget-object v6, v1, Lorg/namelessrom/devicecontrol/objects/BootupItem;->filename:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 324
    const-string v5, "1"

    iget-object v6, v1, Lorg/namelessrom/devicecontrol/objects/BootupItem;->value:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v3, "glove_mode,1"

    .line 325
    .local v3, "mode":Ljava/lang/String;
    :goto_1
    const-string v5, "/sys/class/sec/tsp/cmd"

    invoke-static {v5, v3}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 324
    .end local v3    # "mode":Ljava/lang/String;
    :cond_0
    const-string v3, "glove_mode,0"

    goto :goto_1

    .line 327
    :cond_1
    iget-object v5, v1, Lorg/namelessrom/devicecontrol/objects/BootupItem;->filename:Ljava/lang/String;

    iget-object v6, v1, Lorg/namelessrom/devicecontrol/objects/BootupItem;->value:Ljava/lang/String;

    invoke-static {v5, v6}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 331
    .end local v1    # "item":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 26
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    invoke-super/range {p0 .. p1}, Lorg/namelessrom/devicecontrol/ui/views/CustomPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 77
    const v24, 0x7f060005

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->addPreferencesFromResource(I)V

    .line 79
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v19

    .line 81
    .local v19, "preferenceScreen":Landroid/preference/PreferenceScreen;
    const-string v24, "input_gestures"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceCategory;

    .line 82
    .local v7, "category":Landroid/preference/PreferenceCategory;
    const-string v24, "knockon_gesture_enable"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v17

    check-cast v17, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 84
    .local v17, "mKnockOn":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    invoke-virtual/range {v17 .. v17}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v24

    if-eqz v24, :cond_9

    .line 86
    :try_start_0
    invoke-virtual/range {v17 .. v17}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 88
    :goto_0
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 93
    :goto_1
    const-string v24, "sweep_to_wake"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v22

    check-cast v22, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 95
    .local v22, "sweepToWake":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    invoke-virtual/range {v22 .. v22}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v24

    if-eqz v24, :cond_a

    .line 96
    invoke-virtual/range {v22 .. v22}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 97
    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 102
    :goto_2
    const-string v24, "sweep_to_volume"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v21

    check-cast v21, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 104
    .local v21, "sweepToVolume":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    invoke-virtual/range {v21 .. v21}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v24

    if-eqz v24, :cond_b

    .line 105
    invoke-virtual/range {v21 .. v21}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 106
    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 111
    :goto_3
    invoke-virtual {v7}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v24

    if-nez v24, :cond_0

    .line 112
    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 115
    :cond_0
    const-string v24, "input_others"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    .end local v7    # "category":Landroid/preference/PreferenceCategory;
    check-cast v7, Landroid/preference/PreferenceCategory;

    .line 116
    .restart local v7    # "category":Landroid/preference/PreferenceCategory;
    const-string v24, "vibrator_tuning"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v18

    check-cast v18, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;

    .line 117
    .local v18, "pref":Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;
    invoke-static {}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->isSupported()Z

    move-result v24

    if-nez v24, :cond_1

    .line 118
    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 121
    :cond_1
    const-string v24, "input_glove_mode_aw"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 123
    .local v3, "awesomeGloveMode":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v24

    if-eqz v24, :cond_c

    .line 124
    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 125
    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 131
    :goto_4
    const-string v24, "input_glove_mode"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v24

    check-cast v24, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mGloveMode:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    .line 134
    if-nez v3, :cond_2

    :try_start_1
    invoke-direct/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->isHtsSupported()Z

    move-result v24

    if-nez v24, :cond_d

    .line 135
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mGloveMode:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 147
    :goto_5
    const-string v24, "input_reset_on_suspend"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v20

    check-cast v20, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 149
    .local v20, "resetOnSuspend":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    invoke-virtual/range {v20 .. v20}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v24

    if-eqz v24, :cond_10

    .line 150
    invoke-virtual/range {v20 .. v20}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 151
    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 156
    :goto_6
    invoke-virtual {v7}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v24

    if-nez v24, :cond_3

    .line 157
    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 162
    :cond_3
    const-string v24, "touchkey"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    .end local v7    # "category":Landroid/preference/PreferenceCategory;
    check-cast v7, Landroid/preference/PreferenceCategory;

    .line 163
    .restart local v7    # "category":Landroid/preference/PreferenceCategory;
    const-string v24, "touchkey_light"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 165
    .local v4, "backlightKey":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    invoke-virtual {v4}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v24

    if-eqz v24, :cond_11

    .line 166
    invoke-virtual {v4}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 167
    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 172
    :goto_7
    const-string v24, "touchkey_bln"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 174
    .local v5, "backlightNotification":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    invoke-virtual {v5}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v24

    if-eqz v24, :cond_12

    .line 175
    invoke-virtual {v5}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 176
    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 181
    :goto_8
    const-string v24, "keyboard_light"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v12

    check-cast v12, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 183
    .local v12, "keyboardBacklight":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    invoke-virtual {v12}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v24

    if-eqz v24, :cond_13

    .line 184
    invoke-virtual {v12}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 185
    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 190
    :goto_9
    invoke-virtual {v7}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v24

    if-nez v24, :cond_4

    .line 191
    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 196
    :cond_4
    const-string v24, "graphics"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    .end local v7    # "category":Landroid/preference/PreferenceCategory;
    check-cast v7, Landroid/preference/PreferenceCategory;

    .line 197
    .restart local v7    # "category":Landroid/preference/PreferenceCategory;
    const-string v24, "display_color_calibration"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    check-cast v8, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;

    .line 199
    .local v8, "displayColor":Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;
    invoke-static {}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->isSupported()Z

    move-result v24

    if-nez v24, :cond_5

    .line 200
    invoke-virtual {v7, v8}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 203
    :cond_5
    const-string v24, "display_gamma_calibration"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    check-cast v9, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;

    .line 205
    .local v9, "displayGamma":Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;
    invoke-static {}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->isSupported()Z

    move-result v24

    if-nez v24, :cond_6

    .line 206
    invoke-virtual {v7, v9}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 209
    :cond_6
    const-string v24, "panel_color_temperature"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v24

    check-cast v24, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mPanelColor:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

    .line 210
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mPanelColor:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->isSupported()Z

    move-result v24

    if-eqz v24, :cond_14

    .line 211
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mPanelColor:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->initValue()V

    .line 212
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mPanelColor:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 217
    :goto_a
    const-string v24, "lcd_power_reduce"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    check-cast v14, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 219
    .local v14, "lcdPowerReduce":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    invoke-virtual {v14}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v24

    if-eqz v24, :cond_15

    .line 220
    invoke-virtual {v14}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 221
    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 226
    :goto_b
    const-string v24, "lcd_sunlight_enhancement"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v15

    check-cast v15, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 228
    .local v15, "lcdSunlightEnhancement":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    invoke-virtual {v15}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v24

    if-eqz v24, :cond_16

    .line 229
    invoke-virtual {v15}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 230
    move-object/from16 v0, p0

    invoke-virtual {v15, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 235
    :goto_c
    const-string v24, "lcd_color_enhancement"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v13

    check-cast v13, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 237
    .local v13, "lcdColorEnhancement":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    invoke-virtual {v13}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v24

    if-eqz v24, :cond_17

    .line 238
    invoke-virtual {v13}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 239
    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 244
    :goto_d
    invoke-virtual {v7}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v24

    if-nez v24, :cond_7

    .line 245
    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 248
    :cond_7
    const-string v24, "extras"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    .end local v7    # "category":Landroid/preference/PreferenceCategory;
    check-cast v7, Landroid/preference/PreferenceCategory;

    .line 249
    .restart local v7    # "category":Landroid/preference/PreferenceCategory;
    const-string v24, "logger_mode"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v16

    check-cast v16, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 251
    .local v16, "loggerMode":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    invoke-virtual/range {v16 .. v16}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v24

    if-eqz v24, :cond_18

    .line 252
    const/16 v24, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue(Z)V

    .line 253
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 258
    :goto_e
    invoke-virtual {v7}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v24

    if-nez v24, :cond_8

    .line 259
    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 262
    :cond_8
    const-string v24, "fast_charge"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v24

    check-cast v24, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mFastCharge:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 263
    const-string v24, "/sys/kernel/fast_charge"

    invoke-static/range {v24 .. v24}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_19

    .line 264
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mFastCharge:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 269
    :goto_f
    const-string v24, "sound_control"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v24

    check-cast v24, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mSoundControl:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 270
    sget-object v24, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->SOUND_CONTROL_PATHS:[Ljava/lang/String;

    invoke-static/range {v24 .. v24}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists([Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_1a

    .line 271
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mSoundControl:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 276
    :goto_10
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->isSupported(Landroid/preference/PreferenceScreen;Landroid/content/Context;)V

    .line 277
    return-void

    .line 90
    .end local v3    # "awesomeGloveMode":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .end local v4    # "backlightKey":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .end local v5    # "backlightNotification":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .end local v8    # "displayColor":Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;
    .end local v9    # "displayGamma":Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;
    .end local v12    # "keyboardBacklight":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .end local v13    # "lcdColorEnhancement":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .end local v14    # "lcdPowerReduce":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .end local v15    # "lcdSunlightEnhancement":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .end local v16    # "loggerMode":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .end local v18    # "pref":Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;
    .end local v20    # "resetOnSuspend":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .end local v21    # "sweepToVolume":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .end local v22    # "sweepToWake":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    :cond_9
    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_1

    .line 99
    .restart local v22    # "sweepToWake":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    :cond_a
    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_2

    .line 108
    .restart local v21    # "sweepToVolume":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    :cond_b
    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_3

    .line 127
    .restart local v3    # "awesomeGloveMode":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .restart local v18    # "pref":Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;
    :cond_c
    invoke-virtual {v7, v3}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 128
    const/4 v3, 0x0

    goto/16 :goto_4

    .line 137
    :cond_d
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mGloveMode:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->getKey()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->getItemByName(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/objects/BootupItem;

    move-result-object v6

    .line 139
    .local v6, "bootupItem":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    if-eqz v6, :cond_e

    iget-object v0, v6, Lorg/namelessrom/devicecontrol/objects/BootupItem;->value:Ljava/lang/String;

    move-object/from16 v23, v0

    .line 140
    .local v23, "value":Ljava/lang/String;
    :goto_11
    if-eqz v23, :cond_f

    const-string v24, "1"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_f

    const/4 v10, 0x1

    .line 142
    .local v10, "enableGlove":Z
    :goto_12
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->enableHts(Z)V

    .line 143
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mGloveMode:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_5

    .line 145
    .end local v6    # "bootupItem":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    .end local v10    # "enableGlove":Z
    .end local v23    # "value":Ljava/lang/String;
    :catch_0
    move-exception v11

    .local v11, "exc":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mGloveMode:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_5

    .line 139
    .end local v11    # "exc":Ljava/lang/Exception;
    .restart local v6    # "bootupItem":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    :cond_e
    const/16 v23, 0x0

    goto :goto_11

    .line 140
    .restart local v23    # "value":Ljava/lang/String;
    :cond_f
    const/4 v10, 0x0

    goto :goto_12

    .line 153
    .end local v6    # "bootupItem":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    .end local v23    # "value":Ljava/lang/String;
    .restart local v20    # "resetOnSuspend":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    :cond_10
    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_6

    .line 169
    .restart local v4    # "backlightKey":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    :cond_11
    invoke-virtual {v7, v4}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_7

    .line 178
    .restart local v5    # "backlightNotification":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    :cond_12
    invoke-virtual {v7, v5}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_8

    .line 187
    .restart local v12    # "keyboardBacklight":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    :cond_13
    invoke-virtual {v7, v12}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_9

    .line 214
    .restart local v8    # "displayColor":Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;
    .restart local v9    # "displayGamma":Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mPanelColor:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_a

    .line 223
    .restart local v14    # "lcdPowerReduce":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    :cond_15
    invoke-virtual {v7, v14}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_b

    .line 232
    .restart local v15    # "lcdSunlightEnhancement":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    :cond_16
    invoke-virtual {v7, v15}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_c

    .line 241
    .restart local v13    # "lcdColorEnhancement":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    :cond_17
    invoke-virtual {v7, v13}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_d

    .line 255
    .restart local v16    # "loggerMode":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    :cond_18
    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_e

    .line 266
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mFastCharge:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-object/from16 v24, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_f

    .line 273
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mSoundControl:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-object/from16 v24, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_10

    .line 87
    .end local v3    # "awesomeGloveMode":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .end local v4    # "backlightKey":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .end local v5    # "backlightNotification":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .end local v8    # "displayColor":Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;
    .end local v9    # "displayGamma":Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;
    .end local v12    # "keyboardBacklight":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .end local v13    # "lcdColorEnhancement":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .end local v14    # "lcdPowerReduce":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .end local v15    # "lcdSunlightEnhancement":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .end local v16    # "loggerMode":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .end local v18    # "pref":Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;
    .end local v20    # "resetOnSuspend":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .end local v21    # "sweepToVolume":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .end local v22    # "sweepToWake":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    :catch_1
    move-exception v24

    goto/16 :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 8
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    .line 281
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mGloveMode:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mGloveMode:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 282
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 283
    .local v6, "value":Z
    invoke-direct {p0, v6}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->enableHts(Z)V

    .line 284
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    new-instance v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    const-string v1, "device"

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mGloveMode:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->getKey()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mGloveMode:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->getKey()Ljava/lang/String;

    move-result-object v3

    if-eqz v6, :cond_0

    const-string v4, "1"

    :goto_0
    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/objects/BootupItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v7, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->setBootup(Landroid/content/Context;Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 296
    .end local v6    # "value":Z
    .end local p1    # "preference":Landroid/preference/Preference;
    :goto_1
    return v5

    .line 284
    .restart local v6    # "value":Z
    .restart local p1    # "preference":Landroid/preference/Preference;
    :cond_0
    const-string v4, "0"

    goto :goto_0

    .line 288
    .end local v6    # "value":Z
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_1
    instance-of v0, p1, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    if-eqz v0, :cond_2

    .line 289
    check-cast p1, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->writeValue(Z)V

    goto :goto_1

    .line 291
    .restart local p1    # "preference":Landroid/preference/Preference;
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_2
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mPanelColor:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

    if-ne p1, v0, :cond_3

    .line 292
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mPanelColor:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->writeValue(Ljava/lang/String;)V

    goto :goto_1

    .line 296
    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v0, 0x1

    .line 301
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mFastCharge:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne v1, p1, :cond_0

    .line 302
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0e04c3

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/MainActivity;->loadFragment(Landroid/app/Activity;I)V

    .line 309
    :goto_0
    return v0

    .line 304
    :cond_0
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mSoundControl:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne v1, p1, :cond_1

    .line 305
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0e0527

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/MainActivity;->loadFragment(Landroid/app/Activity;I)V

    goto :goto_0

    .line 309
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onShellOutput(Lorg/namelessrom/devicecontrol/objects/ShellOutput;)V
    .locals 3
    .param p1, "output"    # Lorg/namelessrom/devicecontrol/objects/ShellOutput;

    .prologue
    .line 347
    if-eqz p1, :cond_0

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mGloveMode:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-nez v0, :cond_1

    .line 351
    :cond_0
    :goto_0
    return-void

    .line 349
    :cond_1
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mGloveMode:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iget-object v1, p1, Lorg/namelessrom/devicecontrol/objects/ShellOutput;->output:Ljava/lang/String;

    const-string v2, "glove_mode,1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 350
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceFeatureGeneralFragment;->mGloveMode:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setEnabled(Z)V

    goto :goto_0
.end method
