.class public Lorg/namelessrom/devicecontrol/device/SensorActivity;
.super Lorg/namelessrom/devicecontrol/activities/BaseActivity;
.source "SensorActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/device/SensorActivity$ViewPagerAdapter;
    }
.end annotation


# instance fields
.field private final mSensorList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;",
            ">;"
        }
    .end annotation
.end field

.field private final mTitleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;-><init>()V

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mSensorList:Ljava/util/ArrayList;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mTitleList:Ljava/util/ArrayList;

    .line 270
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 34
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 65
    invoke-super/range {p0 .. p1}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 66
    const v32, 0x7f04001c

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/SensorActivity;->setContentView(I)V

    .line 69
    const v32, 0x7f0c0061

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/SensorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v30

    check-cast v30, Landroid/support/v7/widget/Toolbar;

    .line 70
    .local v30, "toolbar":Landroid/support/v7/widget/Toolbar;
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/SensorActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 71
    new-instance v32, Lorg/namelessrom/devicecontrol/device/SensorActivity$1;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/namelessrom/devicecontrol/device/SensorActivity$1;-><init>(Lorg/namelessrom/devicecontrol/device/SensorActivity;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    new-instance v19, Lorg/namelessrom/devicecontrol/device/SensorActivity$2;

    const/16 v32, -0x1

    sget-object v33, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;->THIN:Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, p0

    move/from16 v3, v32

    move-object/from16 v4, v33

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/namelessrom/devicecontrol/device/SensorActivity$2;-><init>(Lorg/namelessrom/devicecontrol/device/SensorActivity;Landroid/app/Activity;ILcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;)V

    .line 78
    .local v19, "materialMenu":Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;
    sget-object v32, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->ARROW:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    move-object/from16 v0, v19

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;->setState(Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;)V

    .line 79
    sget v32, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v33, 0x15

    move/from16 v0, v32

    move/from16 v1, v33

    if-lt v0, v1, :cond_0

    .line 80
    const/16 v32, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;->setNeverDrawTouch(Z)V

    .line 83
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mTitleList:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    const v33, 0x7f0e00c7

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/SensorActivity;->getString(I)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mTitleList:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    const v33, 0x7f0e0167

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/SensorActivity;->getString(I)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mTitleList:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    const v33, 0x7f0e0196

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/SensorActivity;->getString(I)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    new-instance v7, Lorg/namelessrom/devicecontrol/device/SensorActivity$ViewPagerAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mTitleList:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-direct {v7, v0, v1}, Lorg/namelessrom/devicecontrol/device/SensorActivity$ViewPagerAdapter;-><init>(Lorg/namelessrom/devicecontrol/device/SensorActivity;Ljava/util/ArrayList;)V

    .line 88
    .local v7, "adapter":Lorg/namelessrom/devicecontrol/device/SensorActivity$ViewPagerAdapter;
    const v32, 0x7f0c0073

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/SensorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/support/v4/view/ViewPager;

    .line 89
    .local v31, "viewPager":Landroid/support/v4/view/ViewPager;
    move-object/from16 v0, v31

    invoke-virtual {v0, v7}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 90
    const/16 v32, 0x3

    invoke-virtual/range {v31 .. v32}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 92
    const v32, 0x7f0c0062

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/SensorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v29

    check-cast v29, Lcom/astuetz/PagerSlidingTabStrip;

    .line 93
    .local v29, "tabHost":Lcom/astuetz/PagerSlidingTabStrip;
    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/astuetz/PagerSlidingTabStrip;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 96
    const v32, 0x7f0c0074

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/SensorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 97
    .local v10, "environmentRoot":Landroid/view/View;
    const v32, 0x7f0c00d3

    move/from16 v0, v32

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    .line 101
    .local v9, "environmentContainer":Landroid/widget/LinearLayout;
    const/16 v32, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->isSupported(Landroid/content/Context;I)Z

    move-result v32

    if-eqz v32, :cond_1

    .line 102
    new-instance v8, Lorg/namelessrom/devicecontrol/device/sensors/environment/AmbientTemperatureSensor;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lorg/namelessrom/devicecontrol/device/sensors/environment/AmbientTemperatureSensor;-><init>(Landroid/content/Context;)V

    .line 103
    .local v8, "amTemperatureSensor":Lorg/namelessrom/devicecontrol/device/sensors/environment/AmbientTemperatureSensor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mSensorList:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    invoke-virtual {v9, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 108
    .end local v8    # "amTemperatureSensor":Lorg/namelessrom/devicecontrol/device/sensors/environment/AmbientTemperatureSensor;
    :cond_1
    const/16 v32, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->isSupported(Landroid/content/Context;I)Z

    move-result v32

    if-eqz v32, :cond_2

    .line 109
    new-instance v16, Lorg/namelessrom/devicecontrol/device/sensors/environment/LightSensor;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/environment/LightSensor;-><init>(Landroid/content/Context;)V

    .line 110
    .local v16, "lightSensor":Lorg/namelessrom/devicecontrol/device/sensors/environment/LightSensor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mSensorList:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 115
    .end local v16    # "lightSensor":Lorg/namelessrom/devicecontrol/device/sensors/environment/LightSensor;
    :cond_2
    const/16 v32, 0x6

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->isSupported(Landroid/content/Context;I)Z

    move-result v32

    if-eqz v32, :cond_3

    .line 116
    new-instance v24, Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;-><init>(Landroid/content/Context;)V

    .line 117
    .local v24, "pressureSensor":Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mSensorList:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 122
    .end local v24    # "pressureSensor":Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;
    :cond_3
    const/16 v32, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->isSupported(Landroid/content/Context;I)Z

    move-result v32

    if-eqz v32, :cond_4

    .line 123
    new-instance v26, Lorg/namelessrom/devicecontrol/device/sensors/environment/RelativeHumiditySensor;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/environment/RelativeHumiditySensor;-><init>(Landroid/content/Context;)V

    .line 124
    .local v26, "relativeHumiditySensor":Lorg/namelessrom/devicecontrol/device/sensors/environment/RelativeHumiditySensor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mSensorList:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    move-object/from16 v0, v26

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 129
    .end local v26    # "relativeHumiditySensor":Lorg/namelessrom/devicecontrol/device/sensors/environment/RelativeHumiditySensor;
    :cond_4
    const v32, 0x7f0c0075

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/SensorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v21

    .line 130
    .local v21, "motionRoot":Landroid/view/View;
    const v32, 0x7f0c00d3

    move-object/from16 v0, v21

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/LinearLayout;

    .line 134
    .local v20, "motionContainer":Landroid/widget/LinearLayout;
    sget v32, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v33, 0x13

    move/from16 v0, v32

    move/from16 v1, v33

    if-lt v0, v1, :cond_5

    .line 135
    const/16 v32, 0x13

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->isSupported(Landroid/content/Context;I)Z

    move-result v32

    if-eqz v32, :cond_5

    .line 136
    new-instance v28, Lorg/namelessrom/devicecontrol/device/sensors/motion/StepSensor;

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/motion/StepSensor;-><init>(Landroid/content/Context;)V

    .line 137
    .local v28, "stepSensor":Lorg/namelessrom/devicecontrol/device/sensors/motion/StepSensor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mSensorList:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    move-object/from16 v0, v20

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 143
    .end local v28    # "stepSensor":Lorg/namelessrom/devicecontrol/device/sensors/motion/StepSensor;
    :cond_5
    const/16 v32, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->isSupported(Landroid/content/Context;I)Z

    move-result v32

    if-eqz v32, :cond_6

    .line 144
    new-instance v6, Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerSensor;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerSensor;-><init>(Landroid/content/Context;)V

    .line 145
    .local v6, "accelerometerSensor":Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerSensor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mSensorList:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 150
    .end local v6    # "accelerometerSensor":Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerSensor;
    :cond_6
    const/16 v32, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->isSupported(Landroid/content/Context;I)Z

    move-result v32

    if-eqz v32, :cond_7

    .line 151
    new-instance v5, Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerLinearSensor;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerLinearSensor;-><init>(Landroid/content/Context;)V

    .line 152
    .local v5, "accLinearSensor":Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerLinearSensor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mSensorList:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 157
    .end local v5    # "accLinearSensor":Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerLinearSensor;
    :cond_7
    const/16 v32, 0x9

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->isSupported(Landroid/content/Context;I)Z

    move-result v32

    if-eqz v32, :cond_8

    .line 158
    new-instance v13, Lorg/namelessrom/devicecontrol/device/sensors/motion/GravitySensor;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/GravitySensor;-><init>(Landroid/content/Context;)V

    .line 159
    .local v13, "gravitySensor":Lorg/namelessrom/devicecontrol/device/sensors/motion/GravitySensor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mSensorList:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 164
    .end local v13    # "gravitySensor":Lorg/namelessrom/devicecontrol/device/sensors/motion/GravitySensor;
    :cond_8
    const/16 v32, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->isSupported(Landroid/content/Context;I)Z

    move-result v32

    if-eqz v32, :cond_9

    .line 165
    new-instance v27, Lorg/namelessrom/devicecontrol/device/sensors/motion/RotationVectorSensor;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/motion/RotationVectorSensor;-><init>(Landroid/content/Context;)V

    .line 166
    .local v27, "rotationVectorSensor":Lorg/namelessrom/devicecontrol/device/sensors/motion/RotationVectorSensor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mSensorList:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 167
    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 171
    .end local v27    # "rotationVectorSensor":Lorg/namelessrom/devicecontrol/device/sensors/motion/RotationVectorSensor;
    :cond_9
    const/16 v32, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->isSupported(Landroid/content/Context;I)Z

    move-result v32

    if-eqz v32, :cond_a

    .line 172
    new-instance v14, Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeSensor;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeSensor;-><init>(Landroid/content/Context;)V

    .line 173
    .local v14, "gyroscopeSensor":Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeSensor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mSensorList:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 178
    .end local v14    # "gyroscopeSensor":Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeSensor;
    :cond_a
    sget v32, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v33, 0x12

    move/from16 v0, v32

    move/from16 v1, v33

    if-lt v0, v1, :cond_b

    .line 179
    const/16 v32, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->isSupported(Landroid/content/Context;I)Z

    move-result v32

    if-eqz v32, :cond_b

    .line 180
    new-instance v15, Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeUncalibratedSensor;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeUncalibratedSensor;-><init>(Landroid/content/Context;)V

    .line 182
    .local v15, "gyroscopeUncalibratedSensor":Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeUncalibratedSensor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mSensorList:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 188
    .end local v15    # "gyroscopeUncalibratedSensor":Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeUncalibratedSensor;
    :cond_b
    const v32, 0x7f0c0076

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/device/SensorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v23

    .line 189
    .local v23, "positionRoot":Landroid/view/View;
    const v32, 0x7f0c00d3

    move-object/from16 v0, v23

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/LinearLayout;

    .line 193
    .local v22, "positionContainer":Landroid/widget/LinearLayout;
    const/16 v32, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->isSupported(Landroid/content/Context;I)Z

    move-result v32

    if-eqz v32, :cond_c

    .line 194
    new-instance v25, Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;-><init>(Landroid/content/Context;)V

    .line 195
    .local v25, "proximitySensor":Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mSensorList:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 196
    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 200
    .end local v25    # "proximitySensor":Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;
    :cond_c
    const/16 v32, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->isSupported(Landroid/content/Context;I)Z

    move-result v32

    if-eqz v32, :cond_d

    .line 201
    new-instance v17, Lorg/namelessrom/devicecontrol/device/sensors/position/MagneticFieldSensor;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/position/MagneticFieldSensor;-><init>(Landroid/content/Context;)V

    .line 202
    .local v17, "magneticFieldSensor":Lorg/namelessrom/devicecontrol/device/sensors/position/MagneticFieldSensor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mSensorList:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 203
    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 207
    .end local v17    # "magneticFieldSensor":Lorg/namelessrom/devicecontrol/device/sensors/position/MagneticFieldSensor;
    :cond_d
    sget v32, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v33, 0x12

    move/from16 v0, v32

    move/from16 v1, v33

    if-lt v0, v1, :cond_e

    .line 208
    const/16 v32, 0xe

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->isSupported(Landroid/content/Context;I)Z

    move-result v32

    if-eqz v32, :cond_e

    .line 209
    new-instance v18, Lorg/namelessrom/devicecontrol/device/sensors/position/MagneticFieldUncalibratedSensor;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/position/MagneticFieldUncalibratedSensor;-><init>(Landroid/content/Context;)V

    .line 211
    .local v18, "magneticFieldUncalibratedSensor":Lorg/namelessrom/devicecontrol/device/sensors/position/MagneticFieldUncalibratedSensor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mSensorList:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 217
    .end local v18    # "magneticFieldUncalibratedSensor":Lorg/namelessrom/devicecontrol/device/sensors/position/MagneticFieldUncalibratedSensor;
    :cond_e
    sget v32, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v33, 0x13

    move/from16 v0, v32

    move/from16 v1, v33

    if-lt v0, v1, :cond_f

    .line 218
    const/16 v32, 0x14

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->isSupported(Landroid/content/Context;I)Z

    move-result v32

    if-eqz v32, :cond_f

    .line 219
    new-instance v12, Lorg/namelessrom/devicecontrol/device/sensors/position/GeomagneticRotationVectorSensor;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lorg/namelessrom/devicecontrol/device/sensors/position/GeomagneticRotationVectorSensor;-><init>(Landroid/content/Context;)V

    .line 221
    .local v12, "geomagneticRotationVectorSensor":Lorg/namelessrom/devicecontrol/device/sensors/position/GeomagneticRotationVectorSensor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mSensorList:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 227
    .end local v12    # "geomagneticRotationVectorSensor":Lorg/namelessrom/devicecontrol/device/sensors/position/GeomagneticRotationVectorSensor;
    :cond_f
    sget v32, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v33, 0x12

    move/from16 v0, v32

    move/from16 v1, v33

    if-lt v0, v1, :cond_10

    .line 228
    const/16 v32, 0xf

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->isSupported(Landroid/content/Context;I)Z

    move-result v32

    if-eqz v32, :cond_10

    .line 229
    new-instance v11, Lorg/namelessrom/devicecontrol/device/sensors/position/GameRotationVectorSensor;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lorg/namelessrom/devicecontrol/device/sensors/position/GameRotationVectorSensor;-><init>(Landroid/content/Context;)V

    .line 231
    .local v11, "gameRotationVectorSensor":Lorg/namelessrom/devicecontrol/device/sensors/position/GameRotationVectorSensor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mSensorList:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 232
    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 235
    .end local v11    # "gameRotationVectorSensor":Lorg/namelessrom/devicecontrol/device/sensors/position/GameRotationVectorSensor;
    :cond_10
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 238
    invoke-super {p0}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onPause()V

    .line 239
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v2

    iget-boolean v2, v2, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->swipeOnContent:Z

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/MainActivity;->setSwipeOnContent(Z)V

    .line 242
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mSensorList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;

    .line 243
    .local v1, "sensor":Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;
    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->onPause()V

    goto :goto_0

    .line 245
    .end local v1    # "sensor":Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 248
    invoke-super {p0}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onResume()V

    .line 250
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/MainActivity;->setSwipeOnContent(Z)V

    .line 253
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/SensorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    packed-switch v2, :pswitch_data_0

    .line 265
    :goto_0
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/device/SensorActivity;->mSensorList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;

    .line 266
    .local v1, "sensor":Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;
    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->onResume()V

    goto :goto_1

    .line 255
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "sensor":Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;
    :pswitch_0
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/device/SensorActivity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 259
    :pswitch_1
    invoke-virtual {p0, v3}, Lorg/namelessrom/devicecontrol/device/SensorActivity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 268
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    return-void

    .line 253
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
