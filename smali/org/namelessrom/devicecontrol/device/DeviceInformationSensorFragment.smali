.class public Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment;
.super Landroid/support/v4/preference/PreferenceFragment;
.source "DeviceInformationSensorFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment$1;,
        Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment$SortIgnoreCase;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/support/v4/preference/PreferenceFragment;-><init>()V

    .line 88
    return-void
.end method

.method private addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    .locals 2
    .param p1, "category"    # Landroid/preference/PreferenceCategory;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "summary"    # Ljava/lang/String;

    .prologue
    .line 80
    new-instance v0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;-><init>(Landroid/content/Context;)V

    .line 81
    .local v0, "preference":Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-virtual {v0, p2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setKey(Ljava/lang/String;)V

    .line 82
    invoke-virtual {v0, p3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 83
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0e022e

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment;->getString(I)Ljava/lang/String;

    move-result-object p4

    .end local p4    # "summary":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0, p4}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 84
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 85
    return-object v0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 42
    invoke-super {p0, p1}, Landroid/support/v4/preference/PreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 43
    const v5, 0x7f060008

    invoke-virtual {p0, v5}, Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment;->addPreferencesFromResource(I)V

    .line 45
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v5

    const-string v6, "sensor"

    invoke-virtual {v5, v6}, Lorg/namelessrom/devicecontrol/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/hardware/SensorManager;

    .line 49
    .local v4, "sensorManager":Landroid/hardware/SensorManager;
    const-string v5, "sensors"

    invoke-virtual {p0, v5}, Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 53
    .local v0, "category":Landroid/preference/PreferenceCategory;
    new-instance v3, Ljava/util/ArrayList;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 56
    .local v3, "sensorList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/hardware/Sensor;>;"
    new-instance v5, Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment$SortIgnoreCase;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment$SortIgnoreCase;-><init>(Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment;Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment$1;)V

    invoke-static {v3, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 58
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/Sensor;

    .line 59
    .local v2, "s":Landroid/hardware/Sensor;
    const-string v5, ""

    invoke-virtual {v2}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Landroid/hardware/Sensor;->getVendor()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v0, v5, v6, v7}, Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    goto :goto_0

    .line 62
    .end local v2    # "s":Landroid/hardware/Sensor;
    :cond_0
    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v5

    if-nez v5, :cond_1

    .line 63
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 65
    :cond_1
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 4
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 69
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    .line 70
    .local v1, "key":Ljava/lang/String;
    const-string v2, "sensor_test"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 71
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lorg/namelessrom/devicecontrol/device/SensorActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 72
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment;->startActivity(Landroid/content/Intent;)V

    .line 75
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/preference/PreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v2

    return v2
.end method
