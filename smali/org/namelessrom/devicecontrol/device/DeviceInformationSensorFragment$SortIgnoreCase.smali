.class Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment$SortIgnoreCase;
.super Ljava/lang/Object;
.source "DeviceInformationSensorFragment.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SortIgnoreCase"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/hardware/Sensor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment;


# direct methods
.method private constructor <init>(Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment$SortIgnoreCase;->this$0:Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment;Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment;
    .param p2, "x1"    # Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment$1;

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment$SortIgnoreCase;-><init>(Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment;)V

    return-void
.end method


# virtual methods
.method public compare(Landroid/hardware/Sensor;Landroid/hardware/Sensor;)I
    .locals 3
    .param p1, "sensor1"    # Landroid/hardware/Sensor;
    .param p2, "sensor2"    # Landroid/hardware/Sensor;

    .prologue
    .line 90
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, "s1":Ljava/lang/String;
    :goto_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v1

    .line 92
    .local v1, "s2":Ljava/lang/String;
    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    return v2

    .line 90
    .end local v0    # "s1":Ljava/lang/String;
    .end local v1    # "s2":Ljava/lang/String;
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 91
    .restart local v0    # "s1":Ljava/lang/String;
    :cond_1
    const-string v1, ""

    goto :goto_1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 88
    check-cast p1, Landroid/hardware/Sensor;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/hardware/Sensor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/namelessrom/devicecontrol/device/DeviceInformationSensorFragment$SortIgnoreCase;->compare(Landroid/hardware/Sensor;Landroid/hardware/Sensor;)I

    move-result v0

    return v0
.end method
