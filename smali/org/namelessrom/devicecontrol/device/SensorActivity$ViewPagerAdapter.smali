.class public Lorg/namelessrom/devicecontrol/device/SensorActivity$ViewPagerAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "SensorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/device/SensorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ViewPagerAdapter"
.end annotation


# instance fields
.field private final mTitles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/namelessrom/devicecontrol/device/SensorActivity;


# direct methods
.method public constructor <init>(Lorg/namelessrom/devicecontrol/device/SensorActivity;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 273
    .local p2, "titles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/device/SensorActivity$ViewPagerAdapter;->this$0:Lorg/namelessrom/devicecontrol/device/SensorActivity;

    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 274
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/device/SensorActivity$ViewPagerAdapter;->mTitles:Ljava/util/ArrayList;

    .line 275
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/SensorActivity$ViewPagerAdapter;->mTitles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getPageTitle(I)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 270
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/device/SensorActivity$ViewPagerAdapter;->getPageTitle(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPageTitle(I)Ljava/lang/String;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 279
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/SensorActivity$ViewPagerAdapter;->mTitles:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 284
    packed-switch p2, :pswitch_data_0

    .line 287
    const v0, 0x7f0c0074

    .line 299
    .local v0, "resId":I
    :goto_0
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/SensorActivity$ViewPagerAdapter;->this$0:Lorg/namelessrom/devicecontrol/device/SensorActivity;

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/device/SensorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    return-object v1

    .line 291
    .end local v0    # "resId":I
    :pswitch_0
    const v0, 0x7f0c0075

    .line 292
    .restart local v0    # "resId":I
    goto :goto_0

    .line 295
    .end local v0    # "resId":I
    :pswitch_1
    const v0, 0x7f0c0076

    .restart local v0    # "resId":I
    goto :goto_0

    .line 284
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 307
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
