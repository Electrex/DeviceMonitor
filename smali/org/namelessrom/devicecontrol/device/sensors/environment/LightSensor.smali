.class public Lorg/namelessrom/devicecontrol/device/sensors/environment/LightSensor;
.super Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;
.source "LightSensor.java"


# instance fields
.field private mSensor:Landroid/hardware/Sensor;

.field private mValue:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;-><init>(Landroid/content/Context;)V

    .line 45
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/environment/LightSensor;->getInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040042

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/environment/LightSensor;->getDataContainer()Landroid/widget/LinearLayout;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 47
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/environment/LightSensor;->getSensorManager()Landroid/hardware/SensorManager;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/LightSensor;->mSensor:Landroid/hardware/Sensor;

    .line 49
    const v0, 0x7f0e01cc

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/device/sensors/environment/LightSensor;->setup(I)V

    .line 51
    const v0, 0x7f0c00d4

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/device/sensors/environment/LightSensor;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/LightSensor;->mValue:Landroid/widget/TextView;

    .line 52
    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/device/sensors/environment/LightSensor;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/device/sensors/environment/LightSensor;

    .prologue
    .line 28
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/LightSensor;->mValue:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public getImageResourceId()I
    .locals 1

    .prologue
    .line 36
    const v0, 0x7f02005d

    return v0
.end method

.method public getSensor()Landroid/hardware/Sensor;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/LightSensor;->mSensor:Landroid/hardware/Sensor;

    return-object v0
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v3, 0x0

    .line 55
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/LightSensor;->mValue:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v3

    const/high16 v2, 0x4f000000

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v1, v3

    .line 60
    .local v0, "value":F
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/LightSensor;->mValue:Landroid/widget/TextView;

    new-instance v2, Lorg/namelessrom/devicecontrol/device/sensors/environment/LightSensor$1;

    invoke-direct {v2, p0, v0}, Lorg/namelessrom/devicecontrol/device/sensors/environment/LightSensor$1;-><init>(Lorg/namelessrom/devicecontrol/device/sensors/environment/LightSensor;F)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
