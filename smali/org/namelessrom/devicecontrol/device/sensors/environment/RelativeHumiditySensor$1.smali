.class Lorg/namelessrom/devicecontrol/device/sensors/environment/RelativeHumiditySensor$1;
.super Ljava/lang/Object;
.source "RelativeHumiditySensor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/device/sensors/environment/RelativeHumiditySensor;->onSensorChanged(Landroid/hardware/SensorEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/device/sensors/environment/RelativeHumiditySensor;

.field final synthetic val$value:F


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/device/sensors/environment/RelativeHumiditySensor;F)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/RelativeHumiditySensor$1;->this$0:Lorg/namelessrom/devicecontrol/device/sensors/environment/RelativeHumiditySensor;

    iput p2, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/RelativeHumiditySensor$1;->val$value:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 62
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/RelativeHumiditySensor$1;->this$0:Lorg/namelessrom/devicecontrol/device/sensors/environment/RelativeHumiditySensor;

    # getter for: Lorg/namelessrom/devicecontrol/device/sensors/environment/RelativeHumiditySensor;->mValue:Landroid/widget/TextView;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/device/sensors/environment/RelativeHumiditySensor;->access$000(Lorg/namelessrom/devicecontrol/device/sensors/environment/RelativeHumiditySensor;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/RelativeHumiditySensor$1;->val$value:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " %"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    return-void
.end method
