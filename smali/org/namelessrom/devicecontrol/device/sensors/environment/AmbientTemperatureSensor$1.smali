.class Lorg/namelessrom/devicecontrol/device/sensors/environment/AmbientTemperatureSensor$1;
.super Ljava/lang/Object;
.source "AmbientTemperatureSensor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/device/sensors/environment/AmbientTemperatureSensor;->onSensorChanged(Landroid/hardware/SensorEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/device/sensors/environment/AmbientTemperatureSensor;

.field final synthetic val$fahrenheit:F

.field final synthetic val$value:F


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/device/sensors/environment/AmbientTemperatureSensor;FF)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/AmbientTemperatureSensor$1;->this$0:Lorg/namelessrom/devicecontrol/device/sensors/environment/AmbientTemperatureSensor;

    iput p2, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/AmbientTemperatureSensor$1;->val$value:F

    iput p3, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/AmbientTemperatureSensor$1;->val$fahrenheit:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 63
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/AmbientTemperatureSensor$1;->this$0:Lorg/namelessrom/devicecontrol/device/sensors/environment/AmbientTemperatureSensor;

    # getter for: Lorg/namelessrom/devicecontrol/device/sensors/environment/AmbientTemperatureSensor;->mValue:Landroid/widget/TextView;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/device/sensors/environment/AmbientTemperatureSensor;->access$000(Lorg/namelessrom/devicecontrol/device/sensors/environment/AmbientTemperatureSensor;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "%s \u00b0C\n%s \u00b0F"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/AmbientTemperatureSensor$1;->val$value:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/AmbientTemperatureSensor$1;->val$fahrenheit:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    return-void
.end method
