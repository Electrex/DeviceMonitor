.class public Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;
.super Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;
.source "PressureSensor.java"


# instance fields
.field private mNote:Ljava/lang/String;

.field private mSensor:Landroid/hardware/Sensor;

.field private mValue:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    .line 45
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;-><init>(Landroid/content/Context;)V

    .line 46
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;->getInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040042

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;->getDataContainer()Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 48
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;->getSensorManager()Landroid/hardware/SensorManager;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;->mSensor:Landroid/hardware/Sensor;

    .line 50
    const v0, 0x7f0e01cf

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;->setup(I)V

    .line 52
    const v0, 0x7f0c00d4

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;->mValue:Landroid/widget/TextView;

    .line 53
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e01d0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "hPa"

    aput-object v4, v2, v3

    const-string v3, "mbar"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;->mNote:Ljava/lang/String;

    .line 54
    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;

    .prologue
    .line 28
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;->mNote:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;

    .prologue
    .line 28
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;->mValue:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public getImageResourceId()I
    .locals 1

    .prologue
    .line 37
    const v0, 0x7f020078

    return v0
.end method

.method public getSensor()Landroid/hardware/Sensor;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;->mSensor:Landroid/hardware/Sensor;

    return-object v0
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v3, 0x0

    .line 57
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;->mValue:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v3

    const/high16 v2, 0x4f000000

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v1, v3

    .line 62
    .local v0, "value":F
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;->mValue:Landroid/widget/TextView;

    new-instance v2, Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor$1;

    invoke-direct {v2, p0, v0}, Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor$1;-><init>(Lorg/namelessrom/devicecontrol/device/sensors/environment/PressureSensor;F)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
