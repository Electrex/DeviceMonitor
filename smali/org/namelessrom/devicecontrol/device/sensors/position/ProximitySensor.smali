.class public Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;
.super Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;
.source "ProximitySensor.java"


# instance fields
.field private mMaxRange:F

.field private mSensor:Landroid/hardware/Sensor;

.field private mState:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;-><init>(Landroid/content/Context;)V

    .line 47
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;->getInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040042

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;->getDataContainer()Landroid/widget/LinearLayout;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 49
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;->getSensorManager()Landroid/hardware/SensorManager;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;->mSensor:Landroid/hardware/Sensor;

    .line 50
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;->mSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getMaximumRange()F

    move-result v0

    iput v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;->mMaxRange:F

    .line 52
    const v0, 0x7f0e01d1

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;->setup(I)V

    .line 54
    const v0, 0x7f0c00d4

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;->mState:Landroid/widget/TextView;

    .line 55
    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;

    .prologue
    .line 28
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;->mState:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public getImageResourceId()I
    .locals 1

    .prologue
    .line 38
    const v0, 0x7f020055

    return v0
.end method

.method public getSensor()Landroid/hardware/Sensor;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;->mSensor:Landroid/hardware/Sensor;

    return-object v0
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v0, 0x0

    .line 58
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;->mState:Landroid/widget/TextView;

    if-eqz v3, :cond_0

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v0

    const/high16 v4, 0x4f000000

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v3, v0

    .line 63
    .local v1, "state":F
    iget v3, p0, Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;->mMaxRange:F

    cmpl-float v3, v1, v3

    if-ltz v3, :cond_2

    const/4 v0, 0x1

    .line 64
    .local v0, "isFar":Z
    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e00d4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 67
    .local v2, "stateString":Ljava/lang/String;
    :goto_1
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;->mState:Landroid/widget/TextView;

    new-instance v4, Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor$1;

    invoke-direct {v4, p0, v2, v1}, Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor$1;-><init>(Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;Ljava/lang/String;F)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 64
    .end local v2    # "stateString":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/position/ProximitySensor;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e016f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method
