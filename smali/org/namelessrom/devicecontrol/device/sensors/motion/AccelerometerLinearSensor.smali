.class public Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerLinearSensor;
.super Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;
.source "AccelerometerLinearSensor.java"


# instance fields
.field private mSensor:Landroid/hardware/Sensor;

.field private mValue:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;-><init>(Landroid/content/Context;)V

    .line 45
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerLinearSensor;->getInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040042

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerLinearSensor;->getDataContainer()Landroid/widget/LinearLayout;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 47
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerLinearSensor;->getSensorManager()Landroid/hardware/SensorManager;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerLinearSensor;->mSensor:Landroid/hardware/Sensor;

    .line 49
    const v0, 0x7f0e01c4

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerLinearSensor;->setup(I)V

    .line 51
    const v0, 0x7f0c00d4

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerLinearSensor;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerLinearSensor;->mValue:Landroid/widget/TextView;

    .line 52
    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerLinearSensor;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerLinearSensor;

    .prologue
    .line 28
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerLinearSensor;->mValue:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public getImageResourceId()I
    .locals 1

    .prologue
    .line 36
    const v0, 0x7f02007a

    return v0
.end method

.method public getSensor()Landroid/hardware/Sensor;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerLinearSensor;->mSensor:Landroid/hardware/Sensor;

    return-object v0
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v5, 0x0

    .line 55
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerLinearSensor;->mValue:Landroid/widget/TextView;

    if-eqz v3, :cond_0

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v5

    const/high16 v4, 0x4f000000

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v3, v5

    .line 60
    .local v0, "x":F
    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x1

    aget v1, v3, v4

    .line 61
    .local v1, "y":F
    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x2

    aget v2, v3, v4

    .line 62
    .local v2, "z":F
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerLinearSensor;->mValue:Landroid/widget/TextView;

    new-instance v4, Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerLinearSensor$1;

    invoke-direct {v4, p0, v0, v1, v2}, Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerLinearSensor$1;-><init>(Lorg/namelessrom/devicecontrol/device/sensors/motion/AccelerometerLinearSensor;FFF)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
