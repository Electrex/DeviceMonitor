.class public Lorg/namelessrom/devicecontrol/device/sensors/motion/RotationVectorSensor;
.super Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;
.source "RotationVectorSensor.java"


# instance fields
.field private mSensor:Landroid/hardware/Sensor;

.field private mValue:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;-><init>(Landroid/content/Context;)V

    .line 45
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/RotationVectorSensor;->getInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040042

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/RotationVectorSensor;->getDataContainer()Landroid/widget/LinearLayout;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 47
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/RotationVectorSensor;->getSensorManager()Landroid/hardware/SensorManager;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/RotationVectorSensor;->mSensor:Landroid/hardware/Sensor;

    .line 49
    const v0, 0x7f0e01d3

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/RotationVectorSensor;->setup(I)V

    .line 51
    const v0, 0x7f0c00d4

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/RotationVectorSensor;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/RotationVectorSensor;->mValue:Landroid/widget/TextView;

    .line 52
    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/device/sensors/motion/RotationVectorSensor;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/device/sensors/motion/RotationVectorSensor;

    .prologue
    .line 28
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/RotationVectorSensor;->mValue:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public getImageResourceId()I
    .locals 1

    .prologue
    .line 36
    const v0, 0x7f02007b

    return v0
.end method

.method public getSensor()Landroid/hardware/Sensor;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/RotationVectorSensor;->mSensor:Landroid/hardware/Sensor;

    return-object v0
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 8
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x0

    .line 55
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/RotationVectorSensor;->mValue:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    array-length v0, v0

    if-lt v0, v7, :cond_0

    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v6

    const/high16 v1, 0x4f000000

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v2, v0, v6

    .line 60
    .local v2, "x":F
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x1

    aget v3, v0, v1

    .line 61
    .local v3, "y":F
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x2

    aget v4, v0, v1

    .line 65
    .local v4, "z":F
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    array-length v0, v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_2

    .line 66
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v5, v0, v7

    .line 70
    .local v5, "scalar":F
    :goto_1
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/RotationVectorSensor;->mValue:Landroid/widget/TextView;

    new-instance v0, Lorg/namelessrom/devicecontrol/device/sensors/motion/RotationVectorSensor$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/device/sensors/motion/RotationVectorSensor$1;-><init>(Lorg/namelessrom/devicecontrol/device/sensors/motion/RotationVectorSensor;FFFF)V

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 68
    .end local v5    # "scalar":F
    :cond_2
    const/4 v5, 0x0

    .restart local v5    # "scalar":F
    goto :goto_1
.end method
