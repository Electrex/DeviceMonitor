.class public Lorg/namelessrom/devicecontrol/device/sensors/motion/StepSensor;
.super Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;
.source "StepSensor.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field private mSensor:Landroid/hardware/Sensor;

.field private mSteps:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;-><init>(Landroid/content/Context;)V

    .line 51
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/StepSensor;->getInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040042

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/StepSensor;->getDataContainer()Landroid/widget/LinearLayout;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 53
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/StepSensor;->getSensorManager()Landroid/hardware/SensorManager;

    move-result-object v0

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/StepSensor;->mSensor:Landroid/hardware/Sensor;

    .line 55
    const v0, 0x7f0e01d4

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/StepSensor;->setup(I)V

    .line 57
    const v0, 0x7f0c00d4

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/StepSensor;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/StepSensor;->mSteps:Landroid/widget/TextView;

    .line 58
    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/device/sensors/motion/StepSensor;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/device/sensors/motion/StepSensor;

    .prologue
    .line 34
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/StepSensor;->mSteps:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public getImageResourceId()I
    .locals 1

    .prologue
    .line 42
    const v0, 0x7f02007f

    return v0
.end method

.method public getSensor()Landroid/hardware/Sensor;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/StepSensor;->mSensor:Landroid/hardware/Sensor;

    return-object v0
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v3, 0x0

    .line 61
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/StepSensor;->mSteps:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v3

    const/high16 v2, 0x4f000000

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v3

    float-to-int v0, v1

    .line 66
    .local v0, "steps":I
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/StepSensor;->mSteps:Landroid/widget/TextView;

    new-instance v2, Lorg/namelessrom/devicecontrol/device/sensors/motion/StepSensor$1;

    invoke-direct {v2, p0, v0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/StepSensor$1;-><init>(Lorg/namelessrom/devicecontrol/device/sensors/motion/StepSensor;I)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
