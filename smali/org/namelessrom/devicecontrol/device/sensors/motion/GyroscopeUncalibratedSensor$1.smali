.class Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeUncalibratedSensor$1;
.super Ljava/lang/Object;
.source "GyroscopeUncalibratedSensor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeUncalibratedSensor;->onSensorChanged(Landroid/hardware/SensorEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeUncalibratedSensor;

.field final synthetic val$x:F

.field final synthetic val$y:F

.field final synthetic val$z:F


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeUncalibratedSensor;FFF)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeUncalibratedSensor$1;->this$0:Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeUncalibratedSensor;

    iput p2, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeUncalibratedSensor$1;->val$x:F

    iput p3, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeUncalibratedSensor$1;->val$y:F

    iput p4, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeUncalibratedSensor$1;->val$z:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 70
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeUncalibratedSensor$1;->this$0:Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeUncalibratedSensor;

    # getter for: Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeUncalibratedSensor;->mValue:Landroid/widget/TextView;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeUncalibratedSensor;->access$000(Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeUncalibratedSensor;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "x: %s rad/s\ny: %s rad/s\nz: %s rad/s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeUncalibratedSensor$1;->val$x:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeUncalibratedSensor$1;->val$y:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lorg/namelessrom/devicecontrol/device/sensors/motion/GyroscopeUncalibratedSensor$1;->val$z:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    return-void
.end method
