.class public abstract Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;
.super Landroid/widget/LinearLayout;
.source "BaseSensor.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field private mDataContainer:Landroid/widget/LinearLayout;

.field private mIcon:Landroid/widget/ImageView;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mName:Landroid/widget/TextView;

.field private mPowerUsage:Landroid/widget/TextView;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mTitle:Landroid/widget/TextView;

.field private mVendor:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    .line 74
    const/4 v2, 0x0

    invoke-direct {p0, p1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 75
    const-string v2, "sensor"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/SensorManager;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->mSensorManager:Landroid/hardware/SensorManager;

    .line 76
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->mInflater:Landroid/view/LayoutInflater;

    .line 79
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/Application;->isDarkTheme()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 80
    const v1, 0x7f040022

    .line 85
    .local v1, "resId":I
    :goto_0
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {v2, v1, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 86
    const v2, 0x7f0c007e

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 88
    .local v0, "container":Landroid/widget/FrameLayout;
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f040036

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 90
    const v2, 0x7f0c00b6

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->mIcon:Landroid/widget/ImageView;

    .line 91
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->getSensorImage()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 93
    const v2, 0x7f0c00b7

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->mTitle:Landroid/widget/TextView;

    .line 94
    const v2, 0x7f0c00b8

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->mName:Landroid/widget/TextView;

    .line 95
    const v2, 0x7f0c00b9

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->mVendor:Landroid/widget/TextView;

    .line 96
    const v2, 0x7f0c00ba

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->mPowerUsage:Landroid/widget/TextView;

    .line 98
    const v2, 0x7f0c00bb

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->mDataContainer:Landroid/widget/LinearLayout;

    .line 99
    return-void

    .line 82
    .end local v0    # "container":Landroid/widget/FrameLayout;
    .end local v1    # "resId":I
    :cond_0
    const v1, 0x7f040023

    .restart local v1    # "resId":I
    goto :goto_0
.end method

.method public static isSupported(Landroid/content/Context;I)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # I

    .prologue
    .line 51
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    invoke-virtual {v0, p1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getDataContainer()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->mDataContainer:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getIcon()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->mIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getImageResourceId()I
    .locals 1

    .prologue
    .line 56
    const v0, 0x7f02007f

    return v0
.end method

.method public getInflater()Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method public getName()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->mName:Landroid/widget/TextView;

    return-object v0
.end method

.method public getPowerUsage()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->mPowerUsage:Landroid/widget/TextView;

    return-object v0
.end method

.method public getPowerUsageString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 152
    const-string v0, "%s: %s mA"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0199

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->getSensor()Landroid/hardware/Sensor;

    move-result-object v3

    invoke-virtual {v3}, Landroid/hardware/Sensor;->getPower()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract getSensor()Landroid/hardware/Sensor;
.end method

.method public getSensorDelay()I
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x2

    return v0
.end method

.method public getSensorImage()Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    .line 60
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/Application;->isDarkTheme()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, -0x1

    .line 61
    .local v0, "color":I
    :goto_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->getImageResourceId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 62
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    invoke-static {v1, v0}, Lorg/namelessrom/devicecontrol/utils/DrawableHelper;->applyColorFilter(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    return-object v2

    .line 60
    .end local v0    # "color":I
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    const/high16 v0, -0x1000000

    goto :goto_0
.end method

.method public getSensorManager()Landroid/hardware/SensorManager;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->mSensorManager:Landroid/hardware/SensorManager;

    return-object v0
.end method

.method public getTitle()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->mTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method public getVendor()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->mVendor:Landroid/widget/TextView;

    return-object v0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 4
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 161
    const-string v0, "onAccuracyChanged: %s (%s), %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/hardware/Sensor;->getVendor()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Lorg/namelessrom/devicecontrol/Logger;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 163
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->unregisterSensor()V

    .line 117
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 112
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->registerSensor()V

    .line 113
    return-void
.end method

.method public registerSensor()V
    .locals 3

    .prologue
    .line 66
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->getSensorManager()Landroid/hardware/SensorManager;

    move-result-object v0

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->getSensor()Landroid/hardware/Sensor;

    move-result-object v1

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->getSensorDelay()I

    move-result v2

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 67
    return-void
.end method

.method public setup(I)V
    .locals 5
    .param p1, "titleResId"    # I

    .prologue
    .line 102
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->getTitle()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 104
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->getName()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->getSensor()Landroid/hardware/Sensor;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->getVendor()Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "(%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->getSensor()Landroid/hardware/Sensor;

    move-result-object v4

    invoke-virtual {v4}, Landroid/hardware/Sensor;->getVendor()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->getPowerUsage()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->getPowerUsageString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    return-void
.end method

.method public unregisterSensor()V
    .locals 2

    .prologue
    .line 70
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->getSensorManager()Landroid/hardware/SensorManager;

    move-result-object v0

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sensors/BaseSensor;->getSensor()Landroid/hardware/Sensor;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 71
    return-void
.end method
