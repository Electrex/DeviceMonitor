.class Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$MemoryInfoTask;
.super Landroid/os/AsyncTask;
.source "DeviceInformationGeneralFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MemoryInfoTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "[J>;"
    }
.end annotation


# instance fields
.field private final category:Landroid/preference/PreferenceCategory;

.field final synthetic this$0:Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;


# direct methods
.method public constructor <init>(Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;Landroid/preference/PreferenceCategory;)V
    .locals 0
    .param p2, "category"    # Landroid/preference/PreferenceCategory;

    .prologue
    .line 227
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$MemoryInfoTask;->this$0:Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 228
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$MemoryInfoTask;->category:Landroid/preference/PreferenceCategory;

    .line 229
    return-void
.end method

.method private get(J)Ljava/lang/String;
    .locals 5
    .param p1, "data"    # J

    .prologue
    .line 246
    const-string v0, "%s MB"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 224
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$MemoryInfoTask;->doInBackground([Ljava/lang/Void;)[J

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)[J
    .locals 2
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 233
    invoke-static {}, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->getInstance()Lorg/namelessrom/devicecontrol/objects/MemoryInfo;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->readMemory(I)[J

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 224
    check-cast p1, [J

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$MemoryInfoTask;->onPostExecute([J)V

    return-void
.end method

.method protected onPostExecute([J)V
    .locals 6
    .param p1, "result"    # [J

    .prologue
    .line 237
    array-length v0, p1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$MemoryInfoTask;->category:Landroid/preference/PreferenceCategory;

    if-eqz v0, :cond_0

    .line 238
    invoke-static {}, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->getInstance()Lorg/namelessrom/devicecontrol/objects/MemoryInfo;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/objects/MemoryInfo;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 239
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$MemoryInfoTask;->this$0:Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$MemoryInfoTask;->category:Landroid/preference/PreferenceCategory;

    const-string v2, "memory_total"

    const v3, 0x7f0e021e

    const/4 v4, 0x0

    aget-wide v4, p1, v4

    invoke-direct {p0, v4, v5}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$MemoryInfoTask;->get(J)Ljava/lang/String;

    move-result-object v4

    # invokes: Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v0, v1, v2, v3, v4}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->access$000(Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 240
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$MemoryInfoTask;->this$0:Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$MemoryInfoTask;->category:Landroid/preference/PreferenceCategory;

    const-string v2, "memory_free"

    const v3, 0x7f0e010a

    const/4 v4, 0x1

    aget-wide v4, p1, v4

    invoke-direct {p0, v4, v5}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$MemoryInfoTask;->get(J)Ljava/lang/String;

    move-result-object v4

    # invokes: Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v0, v1, v2, v3, v4}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->access$000(Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 241
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$MemoryInfoTask;->this$0:Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$MemoryInfoTask;->category:Landroid/preference/PreferenceCategory;

    const-string v2, "memory_cached"

    const v3, 0x7f0e0041

    const/4 v4, 0x2

    aget-wide v4, p1, v4

    invoke-direct {p0, v4, v5}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment$MemoryInfoTask;->get(J)Ljava/lang/String;

    move-result-object v4

    # invokes: Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->addPreference(Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v0, v1, v2, v3, v4}, Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;->access$000(Lorg/namelessrom/devicecontrol/device/DeviceInformationGeneralFragment;Landroid/preference/PreferenceCategory;Ljava/lang/String;ILjava/lang/String;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 243
    :cond_0
    return-void
.end method
