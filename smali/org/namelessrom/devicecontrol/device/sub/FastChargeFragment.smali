.class public Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;
.source "FastChargeFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private isNewVersion:Z

.field private mAcLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

.field private mAcLevelsValid:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mFailsafe:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

.field private mForceFastCharge:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

.field private mUsbLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

.field private mUsbLevelsValid:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->isNewVersion:Z

    return-void
.end method

.method private getForceSummary(Ljava/lang/String;)I
    .locals 3
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const v0, 0x7f0e022e

    .line 219
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 233
    :cond_0
    :goto_0
    return v0

    .line 221
    :cond_1
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_2
    :goto_1
    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 223
    :pswitch_0
    const v0, 0x7f0e00d6

    goto :goto_0

    .line 221
    :pswitch_1
    const-string v2, "0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x0

    goto :goto_1

    :pswitch_2
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    :pswitch_3
    const-string v2, "2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x2

    goto :goto_1

    .line 225
    :pswitch_4
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->isNewVersion:Z

    if-eqz v0, :cond_3

    .line 226
    const v0, 0x7f0e00d7

    goto :goto_0

    .line 228
    :cond_3
    const v0, 0x7f0e00d8

    goto :goto_0

    .line 231
    :pswitch_5
    const v0, 0x7f0e00d9

    goto :goto_0

    .line 221
    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 55
    const v0, 0x7f0e04c3

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v9, 0x7f0e0022

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 59
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 60
    const v4, 0x7f060003

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->addPreferencesFromResource(I)V

    .line 62
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 65
    .local v0, "mRoot":Landroid/preference/PreferenceScreen;
    const-string v4, "version"

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 66
    .local v1, "mVersion":Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    if-eqz v1, :cond_0

    .line 67
    const-string v4, "/sys/kernel/fast_charge/version"

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 68
    const-string v4, "/sys/kernel/fast_charge/version"

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 69
    .local v2, "tmp":Ljava/lang/String;
    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 70
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "by paul reioux"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    iput-boolean v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->isNewVersion:Z

    .line 77
    .end local v2    # "tmp":Ljava/lang/String;
    :cond_0
    :goto_0
    const-string v4, "force_fast_charge"

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mForceFastCharge:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    .line 78
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mForceFastCharge:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    if-eqz v4, :cond_1

    .line 79
    const-string v4, "/sys/kernel/fast_charge/force_fast_charge"

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 80
    iget-boolean v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->isNewVersion:Z

    if-eqz v4, :cond_8

    const/4 v4, 0x3

    new-array v3, v4, [Ljava/lang/String;

    const-string v4, "0"

    aput-object v4, v3, v6

    const-string v4, "1"

    aput-object v4, v3, v7

    const-string v4, "2"

    aput-object v4, v3, v8

    .line 82
    .local v3, "values":[Ljava/lang/String;
    :goto_1
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mForceFastCharge:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v4, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 83
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mForceFastCharge:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v4, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 84
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mForceFastCharge:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    const-string v5, "/sys/kernel/fast_charge/force_fast_charge"

    invoke-static {v5}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 85
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mForceFastCharge:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mForceFastCharge:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v5}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->getForceSummary(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(I)V

    .line 86
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mForceFastCharge:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v4, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 92
    .end local v3    # "values":[Ljava/lang/String;
    :cond_1
    :goto_2
    const-string v4, "failsafe"

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mFailsafe:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 93
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mFailsafe:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    if-eqz v4, :cond_2

    .line 94
    iget-boolean v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->isNewVersion:Z

    if-eqz v4, :cond_a

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mFailsafe:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v4}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 95
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mFailsafe:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v4}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 96
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mFailsafe:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v4, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 102
    :cond_2
    :goto_3
    const-string v4, "ac_levels_valid"

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mAcLevelsValid:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 103
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mAcLevelsValid:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-eqz v4, :cond_3

    .line 104
    iget-boolean v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->isNewVersion:Z

    if-eqz v4, :cond_c

    const-string v4, "/sys/kernel/fast_charge/ac_levels"

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 105
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mFailsafe:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    if-eqz v4, :cond_b

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mFailsafe:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v4}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 106
    const-string v4, "/sys/kernel/fast_charge/ac_levels"

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 107
    .restart local v2    # "tmp":Ljava/lang/String;
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mAcLevelsValid:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v4, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 116
    .end local v2    # "tmp":Ljava/lang/String;
    :cond_3
    :goto_4
    const-string v4, "ac_level"

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mAcLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    .line 117
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mAcLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    if-eqz v4, :cond_4

    .line 118
    iget-boolean v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->isNewVersion:Z

    if-eqz v4, :cond_d

    const-string v4, "/sys/kernel/fast_charge/ac_charge_level"

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 119
    const-string v4, "/sys/kernel/fast_charge/ac_charge_level"

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 120
    .restart local v2    # "tmp":Ljava/lang/String;
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mAcLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v4, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setText(Ljava/lang/String;)V

    .line 121
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mAcLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v4, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mAcLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v4, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 128
    .end local v2    # "tmp":Ljava/lang/String;
    :cond_4
    :goto_5
    const-string v4, "usb_levels_valid"

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mUsbLevelsValid:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 129
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mUsbLevelsValid:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-eqz v4, :cond_5

    .line 130
    iget-boolean v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->isNewVersion:Z

    if-eqz v4, :cond_f

    const-string v4, "/sys/kernel/fast_charge/usb_levels"

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 131
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mFailsafe:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    if-eqz v4, :cond_e

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mFailsafe:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v4}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 132
    const-string v4, "/sys/kernel/fast_charge/usb_levels"

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 133
    .restart local v2    # "tmp":Ljava/lang/String;
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mUsbLevelsValid:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v4, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 142
    .end local v2    # "tmp":Ljava/lang/String;
    :cond_5
    :goto_6
    const-string v4, "usb_level"

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mUsbLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    .line 143
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mUsbLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    if-eqz v4, :cond_6

    .line 144
    iget-boolean v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->isNewVersion:Z

    if-eqz v4, :cond_10

    const-string v4, "/sys/kernel/fast_charge/usb_charge_level"

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 145
    const-string v4, "/sys/kernel/fast_charge/usb_charge_level"

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 146
    .restart local v2    # "tmp":Ljava/lang/String;
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mUsbLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v4, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setText(Ljava/lang/String;)V

    .line 147
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mUsbLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v4, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 148
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mUsbLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v4, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 154
    .end local v2    # "tmp":Ljava/lang/String;
    :cond_6
    :goto_7
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->isSupported(Landroid/preference/PreferenceScreen;Landroid/content/Context;)V

    .line 155
    return-void

    .line 72
    :cond_7
    iput-boolean v6, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->isNewVersion:Z

    .line 73
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    .line 80
    :cond_8
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "0"

    aput-object v4, v3, v6

    const-string v4, "1"

    aput-object v4, v3, v7

    goto/16 :goto_1

    .line 88
    :cond_9
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mForceFastCharge:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_2

    .line 98
    :cond_a
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mFailsafe:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_3

    .line 109
    :cond_b
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mAcLevelsValid:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v4, v9}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(I)V

    goto/16 :goto_4

    .line 112
    :cond_c
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mAcLevelsValid:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_4

    .line 124
    :cond_d
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mAcLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_5

    .line 135
    :cond_e
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mUsbLevelsValid:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v4, v9}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(I)V

    goto :goto_6

    .line 138
    :cond_f
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mUsbLevelsValid:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_6

    .line 150
    :cond_10
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mUsbLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_7
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 9
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const v2, 0x7f0e0022

    const/4 v5, 0x1

    .line 159
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mForceFastCharge:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    if-ne v0, p1, :cond_1

    .line 160
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 161
    .local v4, "value":Ljava/lang/String;
    const-string v0, "/sys/kernel/fast_charge/force_fast_charge"

    invoke-static {v0, v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->writeValue(Ljava/lang/String;Ljava/lang/String;)Z

    .line 162
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mForceFastCharge:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    const-string v1, "/sys/kernel/fast_charge/force_fast_charge"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->getForceSummary(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(I)V

    .line 163
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    new-instance v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    const-string v1, "device"

    const-string v2, "force_fast_charge"

    const-string v3, "/sys/kernel/fast_charge/force_fast_charge"

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/objects/BootupItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v8, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->setBootup(Landroid/content/Context;Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 215
    .end local v4    # "value":Ljava/lang/String;
    .end local p2    # "newValue":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v5

    .line 166
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_1
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mFailsafe:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    if-ne v0, p1, :cond_7

    .line 167
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 168
    .local v4, "value":Z
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mFailsafe:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v0, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->writeValue(Z)V

    .line 169
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mAcLevelsValid:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-eqz v0, :cond_2

    .line 170
    if-nez v4, :cond_5

    .line 171
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mAcLevelsValid:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(I)V

    .line 176
    :cond_2
    :goto_1
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mUsbLevelsValid:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-eqz v0, :cond_3

    .line 177
    if-nez v4, :cond_6

    .line 178
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mUsbLevelsValid:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(I)V

    .line 184
    :cond_3
    :goto_2
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mAcLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    if-eqz v0, :cond_4

    .line 185
    const-string v0, "/sys/kernel/fast_charge/ac_charge_level"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 186
    .local v7, "tmp":Ljava/lang/String;
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mAcLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v0, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setText(Ljava/lang/String;)V

    .line 187
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mAcLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v0, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 189
    .end local v7    # "tmp":Ljava/lang/String;
    :cond_4
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mUsbLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    if-eqz v0, :cond_0

    .line 190
    const-string v0, "/sys/kernel/fast_charge/usb_charge_level"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 191
    .restart local v7    # "tmp":Ljava/lang/String;
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mUsbLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v0, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setText(Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mUsbLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v0, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 173
    .end local v7    # "tmp":Ljava/lang/String;
    :cond_5
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mAcLevelsValid:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    const-string v1, "/sys/kernel/fast_charge/ac_levels"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 180
    :cond_6
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mUsbLevelsValid:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    const-string v1, "/sys/kernel/fast_charge/usb_levels"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 195
    .end local v4    # "value":Z
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_7
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mAcLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    if-ne v0, p1, :cond_8

    .line 196
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 197
    .local v4, "value":Ljava/lang/String;
    const-string v0, "/sys/kernel/fast_charge/ac_charge_level"

    invoke-static {v0, v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->writeValue(Ljava/lang/String;Ljava/lang/String;)Z

    .line 198
    const-string v0, "/sys/kernel/fast_charge/ac_charge_level"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 199
    .local v6, "currentValue":Ljava/lang/String;
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mAcLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v0, v6}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 200
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mAcLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v0, v6}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setText(Ljava/lang/String;)V

    .line 201
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    new-instance v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    const-string v1, "device"

    const-string v2, "ac_level"

    const-string v3, "/sys/kernel/fast_charge/ac_charge_level"

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/objects/BootupItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v8, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->setBootup(Landroid/content/Context;Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    goto/16 :goto_0

    .line 204
    .end local v4    # "value":Ljava/lang/String;
    .end local v6    # "currentValue":Ljava/lang/String;
    :cond_8
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mUsbLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    if-ne v0, p1, :cond_9

    .line 205
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 206
    .restart local v4    # "value":Ljava/lang/String;
    const-string v0, "/sys/kernel/fast_charge/usb_charge_level"

    invoke-static {v0, v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->writeValue(Ljava/lang/String;Ljava/lang/String;)Z

    .line 207
    const-string v0, "/sys/kernel/fast_charge/usb_charge_level"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 208
    .restart local v6    # "currentValue":Ljava/lang/String;
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mUsbLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v0, v6}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 209
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->mUsbLevel:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v0, v6}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setText(Ljava/lang/String;)V

    .line 210
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    new-instance v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    const-string v1, "device"

    const-string v2, "usb_level"

    const-string v3, "/sys/kernel/fast_charge/usb_charge_level"

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/objects/BootupItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v8, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->setBootup(Landroid/content/Context;Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    goto/16 :goto_0

    .line 215
    .end local v4    # "value":Ljava/lang/String;
    .end local v6    # "currentValue":Ljava/lang/String;
    :cond_9
    const/4 v5, 0x0

    goto/16 :goto_0
.end method
