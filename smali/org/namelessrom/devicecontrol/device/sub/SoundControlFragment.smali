.class public Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;
.source "SoundControlFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private mMicrophone:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

.field private mSpeaker:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

.field private mVolume:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 35
    const v0, 0x7f0e0527

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 38
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const v1, 0x7f060004

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->addPreferencesFromResource(I)V

    .line 42
    const-string v1, "sc_version"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 43
    .local v0, "mVersion":Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    const-string v1, "Franciso Franco"

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 45
    const-string v1, "microphone_gain"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->mMicrophone:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    .line 46
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->mMicrophone:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;->isSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 47
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->mMicrophone:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;->initValue()V

    .line 48
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->mMicrophone:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 53
    :goto_0
    const-string v1, "speaker_gain"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->mSpeaker:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    .line 54
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->mSpeaker:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;->isSupported()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 55
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->mSpeaker:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;->initValue()V

    .line 56
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->mSpeaker:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 61
    :goto_1
    const-string v1, "volume_gain"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->mVolume:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    .line 62
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->mVolume:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;->isSupported()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 63
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->mVolume:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;->initValue()V

    .line 64
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->mVolume:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 70
    :goto_2
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->isSupported(Landroid/preference/PreferenceScreen;Landroid/content/Context;)V

    .line 71
    return-void

    .line 50
    :cond_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->mMicrophone:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 58
    :cond_1
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->mSpeaker:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1

    .line 66
    :cond_2
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->mVolume:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_2
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    .line 74
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->mMicrophone:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    if-ne v1, p1, :cond_0

    .line 75
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->mMicrophone:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;->writeValue(I)V

    .line 85
    :goto_0
    return v0

    .line 77
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_0
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->mSpeaker:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    if-ne v1, p1, :cond_1

    .line 78
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->mSpeaker:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;->writeValue(I)V

    goto :goto_0

    .line 80
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_1
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->mVolume:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    if-ne v1, p1, :cond_2

    .line 81
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;->mVolume:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;

    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeSeekBarPreference;->writeValue(I)V

    goto :goto_0

    .line 85
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
