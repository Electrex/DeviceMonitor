.class public Lorg/namelessrom/devicecontrol/hardware/IoUtils;
.super Ljava/lang/Object;
.source "IoUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/hardware/IoUtils$IoSchedulerListener;,
        Lorg/namelessrom/devicecontrol/hardware/IoUtils$IoScheduler;
    }
.end annotation


# static fields
.field public static final IO_SCHEDULER_PATH:[Ljava/lang/String;

.field public static final READ_AHEAD_PATH:[Ljava/lang/String;

.field private static sInstance:Lorg/namelessrom/devicecontrol/hardware/IoUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "/sys/block/mmcblk0/queue/scheduler"

    aput-object v1, v0, v2

    const-string v1, "/sys/block/mmcblk1/queue/scheduler"

    aput-object v1, v0, v3

    sput-object v0, Lorg/namelessrom/devicecontrol/hardware/IoUtils;->IO_SCHEDULER_PATH:[Ljava/lang/String;

    .line 41
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "/sys/block/mmcblk0/queue/read_ahead_kb"

    aput-object v1, v0, v2

    const-string v1, "/sys/block/mmcblk1/queue/read_ahead_kb"

    aput-object v1, v0, v3

    sput-object v0, Lorg/namelessrom/devicecontrol/hardware/IoUtils;->READ_AHEAD_PATH:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get()Lorg/namelessrom/devicecontrol/hardware/IoUtils;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/IoUtils;->sInstance:Lorg/namelessrom/devicecontrol/hardware/IoUtils;

    if-nez v0, :cond_0

    .line 66
    new-instance v0, Lorg/namelessrom/devicecontrol/hardware/IoUtils;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/hardware/IoUtils;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/hardware/IoUtils;->sInstance:Lorg/namelessrom/devicecontrol/hardware/IoUtils;

    .line 68
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/IoUtils;->sInstance:Lorg/namelessrom/devicecontrol/hardware/IoUtils;

    return-object v0
.end method


# virtual methods
.method public getAvailableIoSchedulers()[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 77
    const/4 v2, 0x0

    .line 78
    .local v2, "schedulers":[Ljava/lang/String;
    sget-object v3, Lorg/namelessrom/devicecontrol/hardware/IoUtils;->IO_SCHEDULER_PATH:[Ljava/lang/String;

    aget-object v3, v3, v6

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/utils/Utils;->readStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "aux":[Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 80
    array-length v3, v0

    new-array v2, v3, [Ljava/lang/String;

    .line 81
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    .line 82
    aget-object v3, v0, v1

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x5b

    if-ne v3, v4, :cond_0

    .line 83
    aget-object v3, v0, v1

    const/4 v4, 0x1

    aget-object v5, v0, v1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 81
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 85
    :cond_0
    aget-object v3, v0, v1

    aput-object v3, v2, v1

    goto :goto_1

    .line 89
    .end local v1    # "i":I
    :cond_1
    return-object v2
.end method

.method public getIoScheduler(Lorg/namelessrom/devicecontrol/hardware/IoUtils$IoSchedulerListener;)V
    .locals 9
    .param p1, "listener"    # Lorg/namelessrom/devicecontrol/hardware/IoUtils$IoSchedulerListener;

    .prologue
    .line 94
    const/4 v1, 0x1

    :try_start_0
    invoke-static {v1}, Lcom/stericson/roottools/RootTools;->getShell(Z)Lcom/stericson/roottools/execution/Shell;

    move-result-object v8

    .line 95
    .local v8, "mShell":Lcom/stericson/roottools/execution/Shell;
    if-nez v8, :cond_0

    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Shell is null"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    .end local v8    # "mShell":Lcom/stericson/roottools/execution/Shell;
    :catch_0
    move-exception v7

    .line 138
    .local v7, "exc":Ljava/lang/Exception;
    const-class v1, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    .end local v7    # "exc":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 97
    .restart local v8    # "mShell":Lcom/stericson/roottools/execution/Shell;
    :cond_0
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cat "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lorg/namelessrom/devicecontrol/hardware/IoUtils;->IO_SCHEDULER_PATH:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " 2> /dev/null;"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 99
    .local v6, "cmd":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    .local v4, "outputCollector":Ljava/lang/StringBuilder;
    new-instance v0, Lorg/namelessrom/devicecontrol/hardware/IoUtils$1;

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v6, v3, v1

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/hardware/IoUtils$1;-><init>(Lorg/namelessrom/devicecontrol/hardware/IoUtils;I[Ljava/lang/String;Ljava/lang/StringBuilder;Lorg/namelessrom/devicecontrol/hardware/IoUtils$IoSchedulerListener;)V

    .line 135
    .local v0, "cmdCapture":Lcom/stericson/roottools/execution/CommandCapture;
    invoke-virtual {v8}, Lcom/stericson/roottools/execution/Shell;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Shell is closed"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 136
    :cond_1
    invoke-virtual {v8, v0}, Lcom/stericson/roottools/execution/Shell;->add(Lcom/stericson/roottools/execution/Command;)Lcom/stericson/roottools/execution/Command;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
