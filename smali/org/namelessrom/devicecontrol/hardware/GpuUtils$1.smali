.class Lorg/namelessrom/devicecontrol/hardware/GpuUtils$1;
.super Ljava/lang/Object;
.source "GpuUtils.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getAvailableFrequencies(Z)[Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/hardware/GpuUtils;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/hardware/GpuUtils;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/hardware/GpuUtils$1;->this$0:Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 213
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/String;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils$1;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public compare(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p1, "object1"    # Ljava/lang/String;
    .param p2, "object2"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 216
    invoke-static {p1, v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->tryValueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p2, v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->tryValueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method
