.class public Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;
.super Ljava/lang/Object;
.source "Emmc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/hardware/Emmc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EmmcBugged"
.end annotation


# instance fields
.field public impact:I

.field public final mid:Ljava/lang/String;

.field public final name:Ljava/lang/String;

.field public final rev:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "mid"    # Ljava/lang/String;
    .param p3, "rev"    # Ljava/lang/String;

    .prologue
    .line 130
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 131
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "mid"    # Ljava/lang/String;
    .param p3, "rev"    # Ljava/lang/String;
    .param p4, "impact"    # I

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;->name:Ljava/lang/String;

    .line 135
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;->mid:Ljava/lang/String;

    .line 136
    iput-object p3, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;->rev:Ljava/lang/String;

    .line 137
    iput p4, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;->impact:I

    .line 138
    return-void
.end method
