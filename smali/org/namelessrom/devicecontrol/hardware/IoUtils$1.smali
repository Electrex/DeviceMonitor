.class Lorg/namelessrom/devicecontrol/hardware/IoUtils$1;
.super Lcom/stericson/roottools/execution/CommandCapture;
.source "IoUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/hardware/IoUtils;->getIoScheduler(Lorg/namelessrom/devicecontrol/hardware/IoUtils$IoSchedulerListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/hardware/IoUtils;

.field final synthetic val$listener:Lorg/namelessrom/devicecontrol/hardware/IoUtils$IoSchedulerListener;

.field final synthetic val$outputCollector:Ljava/lang/StringBuilder;


# direct methods
.method varargs constructor <init>(Lorg/namelessrom/devicecontrol/hardware/IoUtils;I[Ljava/lang/String;Ljava/lang/StringBuilder;Lorg/namelessrom/devicecontrol/hardware/IoUtils$IoSchedulerListener;)V
    .locals 0
    .param p2, "x0"    # I
    .param p3, "x1"    # [Ljava/lang/String;

    .prologue
    .line 100
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/hardware/IoUtils$1;->this$0:Lorg/namelessrom/devicecontrol/hardware/IoUtils;

    iput-object p4, p0, Lorg/namelessrom/devicecontrol/hardware/IoUtils$1;->val$outputCollector:Ljava/lang/StringBuilder;

    iput-object p5, p0, Lorg/namelessrom/devicecontrol/hardware/IoUtils$1;->val$listener:Lorg/namelessrom/devicecontrol/hardware/IoUtils$IoSchedulerListener;

    invoke-direct {p0, p2, p3}, Lcom/stericson/roottools/execution/CommandCapture;-><init>(I[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public commandCompleted(II)V
    .locals 9
    .param p1, "id"    # I
    .param p2, "exitcode"    # I

    .prologue
    .line 106
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/hardware/IoUtils$1;->val$outputCollector:Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 108
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 109
    .local v5, "tmpList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v6, ""

    .line 111
    .local v6, "tmpString":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    if-gtz v7, :cond_0

    .line 132
    :goto_0
    return-void

    .line 113
    :cond_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 114
    .local v3, "s":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_1

    .line 115
    const/4 v7, 0x0

    invoke-virtual {v3, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x5b

    if-ne v7, v8, :cond_2

    .line 116
    const/4 v7, 0x1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v3, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 117
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 119
    :cond_2
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 123
    .end local v3    # "s":Ljava/lang/String;
    :cond_3
    move-object v4, v6

    .line 124
    .local v4, "scheduler":Ljava/lang/String;
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    new-array v7, v7, [Ljava/lang/String;

    invoke-interface {v5, v7}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 126
    .local v0, "availableSchedulers":[Ljava/lang/String;
    sget-object v7, Lorg/namelessrom/devicecontrol/Application;->HANDLER:Landroid/os/Handler;

    new-instance v8, Lorg/namelessrom/devicecontrol/hardware/IoUtils$1$1;

    invoke-direct {v8, p0, v0, v4}, Lorg/namelessrom/devicecontrol/hardware/IoUtils$1$1;-><init>(Lorg/namelessrom/devicecontrol/hardware/IoUtils$1;[Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public commandOutput(ILjava/lang/String;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "line"    # Ljava/lang/String;

    .prologue
    .line 102
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/IoUtils$1;->val$outputCollector:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    return-void
.end method
