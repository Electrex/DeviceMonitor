.class public Lorg/namelessrom/devicecontrol/hardware/VoltageUtils;
.super Ljava/lang/Object;
.source "VoltageUtils.java"


# static fields
.field private static sInstance:Lorg/namelessrom/devicecontrol/hardware/VoltageUtils;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get()Lorg/namelessrom/devicecontrol/hardware/VoltageUtils;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/VoltageUtils;->sInstance:Lorg/namelessrom/devicecontrol/hardware/VoltageUtils;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lorg/namelessrom/devicecontrol/hardware/VoltageUtils;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/hardware/VoltageUtils;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/hardware/VoltageUtils;->sInstance:Lorg/namelessrom/devicecontrol/hardware/VoltageUtils;

    .line 43
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/VoltageUtils;->sInstance:Lorg/namelessrom/devicecontrol/hardware/VoltageUtils;

    return-object v0
.end method


# virtual methods
.method public getUvValues(Z)[Ljava/lang/String;
    .locals 12
    .param p1, "getName"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 51
    .local v8, "valueList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 52
    .local v3, "fstream":Ljava/io/FileInputStream;
    const/4 v5, 0x0

    .line 53
    .local v5, "in":Ljava/io/DataInputStream;
    const/4 v0, 0x0

    .line 55
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v2, Ljava/io/File;

    const-string v10, "/sys/devices/system/cpu/cpufreq/vdd_table/vdd_levels"

    invoke-direct {v2, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 56
    .local v2, "f":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 57
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .end local v3    # "fstream":Ljava/io/FileInputStream;
    .local v4, "fstream":Ljava/io/FileInputStream;
    move-object v3, v4

    .line 65
    .end local v4    # "fstream":Ljava/io/FileInputStream;
    .restart local v3    # "fstream":Ljava/io/FileInputStream;
    :cond_0
    :goto_0
    if-eqz v3, :cond_8

    .line 66
    new-instance v6, Ljava/io/DataInputStream;

    invoke-direct {v6, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 67
    .end local v5    # "in":Ljava/io/DataInputStream;
    .local v6, "in":Ljava/io/DataInputStream;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/InputStreamReader;

    invoke-direct {v10, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 70
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    :cond_1
    :goto_1
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    .local v7, "strLine":Ljava/lang/String;
    if-eqz v7, :cond_7

    .line 71
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 72
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v10

    if-eqz v10, :cond_1

    .line 73
    if-eqz p1, :cond_6

    .line 74
    const-string v10, ":"

    const-string v11, ""

    invoke-virtual {v7, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "\\s+"

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 75
    .local v9, "values":[Ljava/lang/String;
    const/4 v10, 0x0

    aget-object v10, v9, v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 84
    .end local v7    # "strLine":Ljava/lang/String;
    .end local v9    # "values":[Ljava/lang/String;
    :catchall_0
    move-exception v10

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v5, v6

    .end local v2    # "f":Ljava/io/File;
    .end local v6    # "in":Ljava/io/DataInputStream;
    .restart local v5    # "in":Ljava/io/DataInputStream;
    :goto_2
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 85
    :cond_2
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Ljava/io/DataInputStream;->close()V

    .line 86
    :cond_3
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    :cond_4
    throw v10

    .line 59
    .restart local v2    # "f":Ljava/io/File;
    :cond_5
    :try_start_3
    new-instance v2, Ljava/io/File;

    .end local v2    # "f":Ljava/io/File;
    const-string v10, "/sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table"

    invoke-direct {v2, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 60
    .restart local v2    # "f":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 61
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .end local v3    # "fstream":Ljava/io/FileInputStream;
    .restart local v4    # "fstream":Ljava/io/FileInputStream;
    move-object v3, v4

    .end local v4    # "fstream":Ljava/io/FileInputStream;
    .restart local v3    # "fstream":Ljava/io/FileInputStream;
    goto :goto_0

    .line 77
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v5    # "in":Ljava/io/DataInputStream;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v6    # "in":Ljava/io/DataInputStream;
    .restart local v7    # "strLine":Ljava/lang/String;
    :cond_6
    :try_start_4
    const-string v10, "\\s+"

    invoke-virtual {v7, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 78
    .restart local v9    # "values":[Ljava/lang/String;
    const/4 v10, 0x1

    aget-object v10, v9, v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .end local v9    # "values":[Ljava/lang/String;
    :cond_7
    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v5, v6

    .line 84
    .end local v6    # "in":Ljava/io/DataInputStream;
    .end local v7    # "strLine":Ljava/lang/String;
    .restart local v5    # "in":Ljava/io/DataInputStream;
    :cond_8
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 85
    :cond_9
    if-eqz v5, :cond_a

    invoke-virtual {v5}, Ljava/io/DataInputStream;->close()V

    .line 86
    :cond_a
    if-eqz v3, :cond_b

    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 89
    :cond_b
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    new-array v10, v10, [Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/String;

    return-object v10

    .line 84
    .end local v2    # "f":Ljava/io/File;
    :catchall_1
    move-exception v10

    goto :goto_2

    .end local v5    # "in":Ljava/io/DataInputStream;
    .restart local v2    # "f":Ljava/io/File;
    .restart local v6    # "in":Ljava/io/DataInputStream;
    :catchall_2
    move-exception v10

    move-object v5, v6

    .end local v6    # "in":Ljava/io/DataInputStream;
    .restart local v5    # "in":Ljava/io/DataInputStream;
    goto :goto_2
.end method
