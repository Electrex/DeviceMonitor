.class public Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;
.super Ljava/lang/Object;
.source "DisplayGammaCalibration.java"


# static fields
.field private static sInstance:Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;


# instance fields
.field private descriptors:[Ljava/lang/String;

.field private max:I

.field private min:I

.field private paths:[Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 15

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v13

    invoke-virtual {v13}, Lorg/namelessrom/devicecontrol/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 37
    .local v11, "res":Landroid/content/res/Resources;
    const v13, 0x7f08001d

    invoke-virtual {v11, v13}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v10

    .line 38
    .local v10, "paths":[Ljava/lang/String;
    const v13, 0x7f08001a

    invoke-virtual {v11, v13}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 39
    .local v1, "descs":[Ljava/lang/String;
    const v13, 0x7f08001b

    invoke-virtual {v11, v13}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    .line 40
    .local v7, "maxs":[Ljava/lang/String;
    const v13, 0x7f08001c

    invoke-virtual {v11, v13}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v8

    .line 43
    .local v8, "mins":[Ljava/lang/String;
    array-length v6, v10

    .line 44
    .local v6, "length":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v6, :cond_1

    .line 46
    aget-object v13, v10, v3

    const-string v14, ","

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 47
    .local v12, "splitted":[Ljava/lang/String;
    const/4 v2, 0x0

    .line 48
    .local v2, "exists":Z
    move-object v0, v12

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_0

    aget-object v9, v0, v4

    .line 50
    .local v9, "path":Ljava/lang/String;
    invoke-static {v9}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 52
    const/4 v2, 0x1

    .line 58
    .end local v9    # "path":Ljava/lang/String;
    :cond_0
    if-eqz v2, :cond_3

    .line 59
    iput-object v12, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->paths:[Ljava/lang/String;

    .line 61
    aget-object v13, v7, v3

    invoke-static {v13}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v13

    iput v13, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->max:I

    .line 62
    aget-object v13, v8, v3

    invoke-static {v13}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v13

    iput v13, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->min:I

    .line 64
    aget-object v13, v1, v3

    const-string v14, ","

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    iput-object v13, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->descriptors:[Ljava/lang/String;

    .line 69
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "exists":Z
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v12    # "splitted":[Ljava/lang/String;
    :cond_1
    return-void

    .line 48
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v2    # "exists":Z
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    .restart local v9    # "path":Ljava/lang/String;
    .restart local v12    # "splitted":[Ljava/lang/String;
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 44
    .end local v9    # "path":Ljava/lang/String;
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public static get()Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->sInstance:Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->sInstance:Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;

    .line 75
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->sInstance:Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;

    return-object v0
.end method


# virtual methods
.method public getCurGamma(I)Ljava/lang/String;
    .locals 3
    .param p1, "control"    # I

    .prologue
    .line 85
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 86
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->paths:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 87
    if-lez v0, :cond_0

    .line 88
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    :cond_0
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->paths:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 92
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getDescriptors()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->descriptors:[Ljava/lang/String;

    return-object v0
.end method

.method public getMaxValue(I)I
    .locals 1
    .param p1, "control"    # I

    .prologue
    .line 80
    iget v0, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->max:I

    return v0
.end method

.method public getMinValue(I)I
    .locals 1
    .param p1, "control"    # I

    .prologue
    .line 82
    iget v0, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->min:I

    return v0
.end method

.method public getNumberOfControls()I
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x1

    return v0
.end method

.method public getPaths(I)[Ljava/lang/String;
    .locals 1
    .param p1, "control"    # I

    .prologue
    .line 109
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->paths:[Ljava/lang/String;

    return-object v0
.end method

.method public isSupported()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 78
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->paths:[Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->paths:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public setGamma(ILjava/lang/String;)V
    .locals 4
    .param p1, "control"    # I
    .param p2, "gamma"    # Ljava/lang/String;

    .prologue
    .line 96
    const-string v2, " "

    invoke-virtual {p2, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 97
    .local v1, "split":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->paths:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 98
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->paths:[Ljava/lang/String;

    aget-object v2, v2, v0

    aget-object v3, v1, v0

    invoke-static {v2, v3}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;)V

    .line 97
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 100
    :cond_0
    return-void
.end method
