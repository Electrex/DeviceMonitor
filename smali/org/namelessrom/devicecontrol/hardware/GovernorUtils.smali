.class public Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;
.super Ljava/lang/Object;
.source "GovernorUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$GovernorListener;,
        Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$Governor;
    }
.end annotation


# static fields
.field public static final GPU_GOVS:[Ljava/lang/String;

.field private static sInstance:Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 57
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "performance"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "ondemand"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "simple"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "conservative"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "interactive"

    aput-object v2, v0, v1

    sput-object v0, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->GPU_GOVS:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get()Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->sInstance:Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->sInstance:Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;

    .line 82
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->sInstance:Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;

    return-object v0
.end method


# virtual methods
.method public getAvailableCpuGovernors()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->getAvailableGovernors(Z)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAvailableGovernors(Z)[Ljava/lang/String;
    .locals 3
    .param p1, "isGpu"    # Z

    .prologue
    .line 100
    const/4 v0, 0x0

    .line 101
    .local v0, "govArray":[Ljava/lang/String;
    if-eqz p1, :cond_1

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    move-result-object v2

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getGpuGovsAvailablePath()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 104
    .local v1, "govs":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 105
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 108
    :cond_0
    return-object v0

    .line 101
    .end local v1    # "govs":Ljava/lang/String;
    :cond_1
    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors"

    goto :goto_0
.end method

.method public getAvailableGpuGovernors()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getGpuGovsAvailablePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->GPU_GOVS:[Ljava/lang/String;

    .line 117
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->getAvailableGovernors(Z)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getGovernor(Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$GovernorListener;)V
    .locals 9
    .param p1, "listener"    # Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$GovernorListener;

    .prologue
    .line 122
    const/4 v1, 0x1

    :try_start_0
    invoke-static {v1}, Lcom/stericson/roottools/RootTools;->getShell(Z)Lcom/stericson/roottools/execution/Shell;

    move-result-object v8

    .line 123
    .local v8, "mShell":Lcom/stericson/roottools/execution/Shell;
    if-nez v8, :cond_0

    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Shell is null"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    .end local v8    # "mShell":Lcom/stericson/roottools/execution/Shell;
    :catch_0
    move-exception v7

    .line 171
    .local v7, "exc":Ljava/lang/Exception;
    const-class v1, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    .end local v7    # "exc":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 125
    .restart local v8    # "mShell":Lcom/stericson/roottools/execution/Shell;
    :cond_0
    :try_start_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 126
    .local v6, "cmd":Ljava/lang/StringBuilder;
    const-string v1, "command=$("

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    const-string v1, "cat "

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " 2> /dev/null;"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    const-string v1, "echo -n \"[\";"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    const-string v1, "cat "

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " 2> /dev/null;"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    const-string v1, ");"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "echo $command | busybox tr -d \"\\n\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    const-class v1, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 134
    .local v4, "outputCollector":Ljava/lang/StringBuilder;
    new-instance v0, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$1;

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v1

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$1;-><init>(Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;I[Ljava/lang/String;Ljava/lang/StringBuilder;Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$GovernorListener;)V

    .line 168
    .local v0, "cmdCapture":Lcom/stericson/roottools/execution/CommandCapture;
    invoke-virtual {v8}, Lcom/stericson/roottools/execution/Shell;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Shell is closed"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 169
    :cond_1
    invoke-virtual {v8, v0}, Lcom/stericson/roottools/execution/Shell;->add(Lcom/stericson/roottools/execution/Command;)Lcom/stericson/roottools/execution/Command;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public getGovernorPath(I)Ljava/lang/String;
    .locals 1
    .param p1, "cpu"    # I

    .prologue
    .line 86
    packed-switch p1, :pswitch_data_0

    .line 89
    const-string v0, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    .line 95
    :goto_0
    return-object v0

    .line 91
    :pswitch_0
    const-string v0, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_governor"

    goto :goto_0

    .line 93
    :pswitch_1
    const-string v0, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_governor"

    goto :goto_0

    .line 95
    :pswitch_2
    const-string v0, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_governor"

    goto :goto_0

    .line 86
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
