.class public Lorg/namelessrom/devicecontrol/hardware/UksmUtils;
.super Ljava/lang/Object;
.source "UksmUtils.java"


# static fields
.field private static sInstance:Lorg/namelessrom/devicecontrol/hardware/UksmUtils;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get()Lorg/namelessrom/devicecontrol/hardware/UksmUtils;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/UksmUtils;->sInstance:Lorg/namelessrom/devicecontrol/hardware/UksmUtils;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lorg/namelessrom/devicecontrol/hardware/UksmUtils;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/hardware/UksmUtils;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/hardware/UksmUtils;->sInstance:Lorg/namelessrom/devicecontrol/hardware/UksmUtils;

    .line 48
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/UksmUtils;->sInstance:Lorg/namelessrom/devicecontrol/hardware/UksmUtils;

    return-object v0
.end method


# virtual methods
.method public getAvailableCpuGovernors()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 57
    const/4 v2, 0x0

    .line 58
    .local v2, "schedulers":[Ljava/lang/String;
    const-string v3, "/sys/kernel/mm/uksm/cpu_governor"

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/utils/Utils;->readStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, "aux":[Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 60
    array-length v3, v0

    new-array v2, v3, [Ljava/lang/String;

    .line 61
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    .line 62
    aget-object v3, v0, v1

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x5b

    if-ne v3, v4, :cond_0

    .line 63
    aget-object v3, v0, v1

    const/4 v4, 0x1

    aget-object v5, v0, v1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 61
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 65
    :cond_0
    aget-object v3, v0, v1

    aput-object v3, v2, v1

    goto :goto_1

    .line 69
    .end local v1    # "i":I
    :cond_1
    return-object v2
.end method

.method public getCurrentCpuGovernor()Ljava/lang/String;
    .locals 7

    .prologue
    .line 78
    const-string v5, "/sys/kernel/mm/uksm/cpu_governor"

    invoke-static {v5}, Lorg/namelessrom/devicecontrol/utils/Utils;->readStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 79
    .local v2, "aux":[Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 80
    move-object v1, v2

    .local v1, "arr$":[Ljava/lang/String;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v1, v3

    .line 81
    .local v0, "anAux":Ljava/lang/String;
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x5b

    if-ne v5, v6, :cond_0

    .line 82
    const/4 v5, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 86
    .end local v0    # "anAux":Ljava/lang/String;
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :goto_1
    return-object v5

    .line 80
    .restart local v0    # "anAux":Ljava/lang/String;
    .restart local v1    # "arr$":[Ljava/lang/String;
    .restart local v3    # "i$":I
    .restart local v4    # "len$":I
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 86
    .end local v0    # "anAux":Ljava/lang/String;
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_1
    const-string v5, "full"

    goto :goto_1
.end method
