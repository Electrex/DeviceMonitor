.class public Lorg/namelessrom/devicecontrol/hardware/GpuUtils$Gpu;
.super Ljava/lang/Object;
.source "GpuUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/hardware/GpuUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Gpu"
.end annotation


# instance fields
.field public final available:[Ljava/lang/String;

.field public final governor:Ljava/lang/String;

.field public final max:Ljava/lang/String;

.field public final min:Ljava/lang/String;


# direct methods
.method public constructor <init>([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "availFreqs"    # [Ljava/lang/String;
    .param p2, "maxFreq"    # Ljava/lang/String;
    .param p3, "minFreq"    # Ljava/lang/String;
    .param p4, "gov"    # Ljava/lang/String;

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/hardware/GpuUtils$Gpu;->available:[Ljava/lang/String;

    .line 88
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/hardware/GpuUtils$Gpu;->max:Ljava/lang/String;

    .line 89
    iput-object p3, p0, Lorg/namelessrom/devicecontrol/hardware/GpuUtils$Gpu;->min:Ljava/lang/String;

    .line 90
    iput-object p4, p0, Lorg/namelessrom/devicecontrol/hardware/GpuUtils$Gpu;->governor:Ljava/lang/String;

    .line 91
    return-void
.end method
