.class public Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$Governor;
.super Ljava/lang/Object;
.source "GovernorUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Governor"
.end annotation


# instance fields
.field public final available:[Ljava/lang/String;

.field public final current:Ljava/lang/String;


# direct methods
.method public constructor <init>([Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "availableGovernors"    # [Ljava/lang/String;
    .param p2, "governor"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$Governor;->available:[Ljava/lang/String;

    .line 66
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils$Governor;->current:Ljava/lang/String;

    .line 67
    return-void
.end method
