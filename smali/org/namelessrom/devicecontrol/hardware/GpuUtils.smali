.class public Lorg/namelessrom/devicecontrol/hardware/GpuUtils;
.super Ljava/lang/Object;
.source "GpuUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/hardware/GpuUtils$Gpu;
    }
.end annotation


# static fields
.field public static final GL_INFO:[I

.field public static final GL_STRINGS:[I

.field private static gpuBasePath:Ljava/lang/String;

.field private static gpuFreqMaxPath:Ljava/lang/String;

.field private static gpuFreqMinPath:Ljava/lang/String;

.field private static gpuFreqsAvailPath:Ljava/lang/String;

.field private static gpuGovPath:Ljava/lang/String;

.field private static gpuGovsAvailablePath:Ljava/lang/String;

.field private static sInstance:Lorg/namelessrom/devicecontrol/hardware/GpuUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x5

    const/4 v1, 0x0

    .line 54
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->GL_INFO:[I

    .line 62
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    sput-object v0, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->GL_STRINGS:[I

    .line 70
    sput-object v1, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuBasePath:Ljava/lang/String;

    .line 71
    sput-object v1, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuGovPath:Ljava/lang/String;

    .line 72
    sput-object v1, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuGovsAvailablePath:Ljava/lang/String;

    .line 73
    sput-object v1, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuFreqsAvailPath:Ljava/lang/String;

    .line 74
    sput-object v1, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuFreqMaxPath:Ljava/lang/String;

    .line 75
    sput-object v1, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuFreqMinPath:Ljava/lang/String;

    return-void

    .line 54
    nop

    :array_0
    .array-data 4
        0x1f00
        0x1f01
        0x1f02
        0x1f03
        0x8b8c
    .end array-data

    .line 62
    :array_1
    .array-data 4
        0x7f0e0129
        0x7f0e0128
        0x7f0e0180
        0x7f0e017f
        0x7f0e01df
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static freqsToMhz([Ljava/lang/String;)[Ljava/lang/String;
    .locals 3
    .param p0, "frequencies"    # [Ljava/lang/String;

    .prologue
    .line 290
    if-nez p0, :cond_1

    const/4 v1, 0x0

    .line 297
    :cond_0
    return-object v1

    .line 291
    :cond_1
    array-length v2, p0

    new-array v1, v2, [Ljava/lang/String;

    .line 293
    .local v1, "names":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 294
    aget-object v2, p0, v0

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->toMhz(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 293
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static fromMHz(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "mhzString"    # Ljava/lang/String;

    .prologue
    .line 279
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 281
    :try_start_0
    const-string v1, " MHz"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v1

    const v2, 0xf4240

    mul-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 286
    :goto_0
    return-object v1

    .line 282
    :catch_0
    move-exception v0

    .line 283
    .local v0, "exc":Ljava/lang/Exception;
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    .line 286
    .end local v0    # "exc":Ljava/lang/Exception;
    :cond_0
    const-string v1, "0"

    goto :goto_0
.end method

.method public static get()Lorg/namelessrom/devicecontrol/hardware/GpuUtils;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->sInstance:Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    if-nez v0, :cond_0

    .line 100
    new-instance v0, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->sInstance:Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    .line 102
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->sInstance:Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    return-object v0
.end method

.method public static getOpenGLESInformation()Ljava/util/ArrayList;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 315
    new-instance v15, Ljava/util/ArrayList;

    sget-object v4, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->GL_INFO:[I

    array-length v4, v4

    invoke-direct {v15, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 316
    .local v15, "glesInformation":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x15

    if-lt v4, v6, :cond_3

    .line 318
    const/4 v4, 0x0

    invoke-static {v4}, Landroid/opengl/EGL14;->eglGetDisplay(I)Landroid/opengl/EGLDisplay;

    move-result-object v2

    .line 319
    .local v2, "dpy":Landroid/opengl/EGLDisplay;
    const/4 v4, 0x2

    new-array v0, v4, [I

    move-object/from16 v20, v0

    .line 320
    .local v20, "vers":[I
    const/4 v4, 0x0

    const/4 v6, 0x1

    move-object/from16 v0, v20

    move-object/from16 v1, v20

    invoke-static {v2, v0, v4, v1, v6}, Landroid/opengl/EGL14;->eglInitialize(Landroid/opengl/EGLDisplay;[II[II)Z

    .line 324
    const/16 v4, 0x9

    new-array v3, v4, [I

    fill-array-data v3, :array_0

    .line 331
    .local v3, "configAttr":[I
    const/4 v4, 0x1

    new-array v5, v4, [Landroid/opengl/EGLConfig;

    .line 332
    .local v5, "configs":[Landroid/opengl/EGLConfig;
    const/4 v4, 0x1

    new-array v8, v4, [I

    .line 333
    .local v8, "numConfig":[I
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Landroid/opengl/EGL14;->eglChooseConfig(Landroid/opengl/EGLDisplay;[II[Landroid/opengl/EGLConfig;II[II)Z

    .line 334
    const/4 v4, 0x0

    aget v4, v8, v4

    if-nez v4, :cond_0

    .line 335
    const-string v4, "getOpenGLESInformation"

    const-string v6, "no config found! PANIC!"

    invoke-static {v4, v6}, Lorg/namelessrom/devicecontrol/Logger;->w(Ljava/lang/Object;Ljava/lang/String;)V

    .line 337
    :cond_0
    const/4 v4, 0x0

    aget-object v12, v5, v4

    .line 341
    .local v12, "config":Landroid/opengl/EGLConfig;
    const/4 v4, 0x5

    new-array v0, v4, [I

    move-object/from16 v19, v0

    fill-array-data v19, :array_1

    .line 346
    .local v19, "surfAttr":[I
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-static {v2, v12, v0, v4}, Landroid/opengl/EGL14;->eglCreatePbufferSurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;[II)Landroid/opengl/EGLSurface;

    move-result-object v18

    .line 349
    .local v18, "surf":Landroid/opengl/EGLSurface;
    const/4 v4, 0x3

    new-array v14, v4, [I

    fill-array-data v14, :array_2

    .line 350
    .local v14, "ctxAttrib":[I
    sget-object v4, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    const/4 v6, 0x0

    invoke-static {v2, v12, v4, v14, v6}, Landroid/opengl/EGL14;->eglCreateContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Landroid/opengl/EGLContext;[II)Landroid/opengl/EGLContext;

    move-result-object v13

    .line 354
    .local v13, "ctx":Landroid/opengl/EGLContext;
    move-object/from16 v0, v18

    move-object/from16 v1, v18

    invoke-static {v2, v0, v1, v13}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    .line 357
    sget-object v11, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->GL_INFO:[I

    .local v11, "arr$":[I
    array-length v0, v11

    move/from16 v17, v0

    .local v17, "len$":I
    const/16 v16, 0x0

    .local v16, "i$":I
    :goto_0
    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_1

    aget v10, v11, v16

    .line 358
    .local v10, "aGL_INFO":I
    invoke-static {v10}, Landroid/opengl/GLES20;->glGetString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 357
    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    .line 362
    .end local v10    # "aGL_INFO":I
    :cond_1
    sget-object v4, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v6, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v7, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v2, v4, v6, v7}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    .line 364
    move-object/from16 v0, v18

    invoke-static {v2, v0}, Landroid/opengl/EGL14;->eglDestroySurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 365
    invoke-static {v2, v13}, Landroid/opengl/EGL14;->eglDestroyContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLContext;)Z

    .line 366
    invoke-static {v2}, Landroid/opengl/EGL14;->eglTerminate(Landroid/opengl/EGLDisplay;)Z

    .line 374
    .end local v2    # "dpy":Landroid/opengl/EGLDisplay;
    .end local v3    # "configAttr":[I
    .end local v5    # "configs":[Landroid/opengl/EGLConfig;
    .end local v8    # "numConfig":[I
    .end local v12    # "config":Landroid/opengl/EGLConfig;
    .end local v13    # "ctx":Landroid/opengl/EGLContext;
    .end local v14    # "ctxAttrib":[I
    .end local v18    # "surf":Landroid/opengl/EGLSurface;
    .end local v19    # "surfAttr":[I
    .end local v20    # "vers":[I
    :cond_2
    return-object v15

    .line 369
    .end local v11    # "arr$":[I
    .end local v16    # "i$":I
    .end local v17    # "len$":I
    :cond_3
    sget-object v11, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->GL_INFO:[I

    .restart local v11    # "arr$":[I
    array-length v0, v11

    move/from16 v17, v0

    .restart local v17    # "len$":I
    const/16 v16, 0x0

    .restart local v16    # "i$":I
    :goto_1
    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_2

    aget v10, v11, v16

    .line 370
    .restart local v10    # "aGL_INFO":I
    invoke-static {v10}, Landroid/opengl/GLES20;->glGetString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 369
    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    .line 324
    :array_0
    .array-data 4
        0x303f
        0x308e
        0x3029
        0x0
        0x3040
        0x4
        0x3033
        0x1
        0x3038
    .end array-data

    .line 341
    :array_1
    .array-data 4
        0x3057
        0x40
        0x3056
        0x40
        0x3038
    .end array-data

    .line 349
    :array_2
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data
.end method

.method public static isOpenGLES20Supported()Z
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 301
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v5

    const-string v6, "activity"

    invoke-virtual {v5, v6}, Lorg/namelessrom/devicecontrol/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 303
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getDeviceConfigurationInfo()Landroid/content/pm/ConfigurationInfo;

    move-result-object v2

    .line 304
    .local v2, "info":Landroid/content/pm/ConfigurationInfo;
    if-nez v2, :cond_0

    .line 311
    :goto_0
    return v4

    .line 308
    :cond_0
    iget v5, v2, Landroid/content/pm/ConfigurationInfo;->reqGlEsVersion:I

    const/high16 v6, -0x10000

    and-int/2addr v5, v6

    shr-int/lit8 v1, v5, 0x10

    .line 309
    .local v1, "glEsVersion":I
    const-string v5, "isOpenGLES20Supported"

    const-string v6, "glEsVersion: %s (%s)"

    new-array v7, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-virtual {v2}, Landroid/content/pm/ConfigurationInfo;->getGlEsVersion()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-static {v5, v6, v7}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 311
    if-lt v1, v9, :cond_1

    :goto_1
    move v4, v3

    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_1
.end method

.method public static toMhz(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "mhz"    # Ljava/lang/String;

    .prologue
    .line 270
    :try_start_0
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 275
    .local v1, "mhzInt":I
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0xf4240

    div-int v3, v1, v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " MHz"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 271
    .end local v1    # "mhzInt":I
    :catch_0
    move-exception v0

    .line 272
    .local v0, "exc":Ljava/lang/Exception;
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    .line 273
    const/4 v1, 0x0

    .restart local v1    # "mhzInt":I
    goto :goto_0
.end method


# virtual methods
.method public containsGov(Ljava/lang/String;)Z
    .locals 8
    .param p1, "gov"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 246
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;

    move-result-object v6

    invoke-virtual {v6}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->getAvailableGpuGovernors()[Ljava/lang/String;

    move-result-object v1

    .line 247
    .local v1, "governors":[Ljava/lang/String;
    if-nez v1, :cond_1

    .line 251
    :cond_0
    :goto_0
    return v5

    .line 248
    :cond_1
    move-object v0, v1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 249
    .local v4, "s":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x1

    goto :goto_0

    .line 248
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public getAvailableFrequencies(Z)[Ljava/lang/String;
    .locals 3
    .param p1, "sorted"    # Z

    .prologue
    .line 207
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getGpuFreqsAvailPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 208
    .local v1, "freqsRaw":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 209
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 210
    .local v0, "freqs":[Ljava/lang/String;
    if-nez p1, :cond_0

    .line 222
    .end local v0    # "freqs":[Ljava/lang/String;
    :goto_0
    return-object v0

    .line 213
    .restart local v0    # "freqs":[Ljava/lang/String;
    :cond_0
    new-instance v2, Lorg/namelessrom/devicecontrol/hardware/GpuUtils$1;

    invoke-direct {v2, p0}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils$1;-><init>(Lorg/namelessrom/devicecontrol/hardware/GpuUtils;)V

    invoke-static {v0, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 219
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    goto :goto_0

    .line 222
    .end local v0    # "freqs":[Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGovernor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getGpuGovPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGpu()Lorg/namelessrom/devicecontrol/hardware/GpuUtils$Gpu;
    .locals 5

    .prologue
    .line 238
    new-instance v0, Lorg/namelessrom/devicecontrol/hardware/GpuUtils$Gpu;

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getAvailableFrequencies(Z)[Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    move-result-object v2

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getMaxFreq()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    move-result-object v3

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getMinFreq()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    move-result-object v4

    invoke-virtual {v4}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getGovernor()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils$Gpu;-><init>([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public getGpuBasePath()Ljava/lang/String;
    .locals 7

    .prologue
    .line 106
    sget-object v5, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuBasePath:Ljava/lang/String;

    if-nez v5, :cond_2

    .line 107
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v5

    const v6, 0x7f08000f

    invoke-virtual {v5, v6}, Lorg/namelessrom/devicecontrol/Application;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 108
    .local v3, "paths":[Ljava/lang/String;
    move-object v0, v3

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 109
    .local v4, "s":Ljava/lang/String;
    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 110
    sput-object v4, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuBasePath:Ljava/lang/String;

    .line 114
    .end local v4    # "s":Ljava/lang/String;
    :cond_0
    sget-object v5, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuBasePath:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 115
    const-string v5, ""

    .line 118
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "paths":[Ljava/lang/String;
    :goto_1
    return-object v5

    .line 108
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    .restart local v3    # "paths":[Ljava/lang/String;
    .restart local v4    # "s":Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 118
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "paths":[Ljava/lang/String;
    .end local v4    # "s":Ljava/lang/String;
    :cond_2
    sget-object v5, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuBasePath:Ljava/lang/String;

    goto :goto_1
.end method

.method public getGpuFreqMaxPath()Ljava/lang/String;
    .locals 8

    .prologue
    .line 173
    sget-object v6, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuFreqMaxPath:Ljava/lang/String;

    if-nez v6, :cond_2

    .line 174
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getGpuBasePath()Ljava/lang/String;

    move-result-object v1

    .line 175
    .local v1, "base":Ljava/lang/String;
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v6

    const v7, 0x7f080011

    invoke-virtual {v6, v7}, Lorg/namelessrom/devicecontrol/Application;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 176
    .local v4, "paths":[Ljava/lang/String;
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v5, v0, v2

    .line 177
    .local v5, "s":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 178
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuFreqMaxPath:Ljava/lang/String;

    .line 182
    .end local v5    # "s":Ljava/lang/String;
    :cond_0
    sget-object v6, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuFreqMaxPath:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 183
    const/4 v6, 0x0

    .line 186
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "base":Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "paths":[Ljava/lang/String;
    :goto_1
    return-object v6

    .line 176
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "base":Ljava/lang/String;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    .restart local v4    # "paths":[Ljava/lang/String;
    .restart local v5    # "s":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 186
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "base":Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "paths":[Ljava/lang/String;
    .end local v5    # "s":Ljava/lang/String;
    :cond_2
    sget-object v6, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuFreqMaxPath:Ljava/lang/String;

    goto :goto_1
.end method

.method public getGpuFreqMinPath()Ljava/lang/String;
    .locals 8

    .prologue
    .line 190
    sget-object v6, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuFreqMinPath:Ljava/lang/String;

    if-nez v6, :cond_2

    .line 191
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getGpuBasePath()Ljava/lang/String;

    move-result-object v1

    .line 192
    .local v1, "base":Ljava/lang/String;
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v6

    const v7, 0x7f080012

    invoke-virtual {v6, v7}, Lorg/namelessrom/devicecontrol/Application;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 193
    .local v4, "paths":[Ljava/lang/String;
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v5, v0, v2

    .line 194
    .local v5, "s":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 195
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuFreqMinPath:Ljava/lang/String;

    .line 199
    .end local v5    # "s":Ljava/lang/String;
    :cond_0
    sget-object v6, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuFreqMinPath:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 200
    const/4 v6, 0x0

    .line 203
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "base":Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "paths":[Ljava/lang/String;
    :goto_1
    return-object v6

    .line 193
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "base":Ljava/lang/String;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    .restart local v4    # "paths":[Ljava/lang/String;
    .restart local v5    # "s":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 203
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "base":Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "paths":[Ljava/lang/String;
    .end local v5    # "s":Ljava/lang/String;
    :cond_2
    sget-object v6, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuFreqMinPath:Ljava/lang/String;

    goto :goto_1
.end method

.method public getGpuFreqsAvailPath()Ljava/lang/String;
    .locals 8

    .prologue
    .line 156
    sget-object v6, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuFreqsAvailPath:Ljava/lang/String;

    if-nez v6, :cond_2

    .line 157
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getGpuBasePath()Ljava/lang/String;

    move-result-object v1

    .line 158
    .local v1, "base":Ljava/lang/String;
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v6

    const v7, 0x7f080010

    invoke-virtual {v6, v7}, Lorg/namelessrom/devicecontrol/Application;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 159
    .local v4, "paths":[Ljava/lang/String;
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v5, v0, v2

    .line 160
    .local v5, "s":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 161
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuFreqsAvailPath:Ljava/lang/String;

    .line 165
    .end local v5    # "s":Ljava/lang/String;
    :cond_0
    sget-object v6, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuFreqsAvailPath:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 166
    const/4 v6, 0x0

    .line 169
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "base":Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "paths":[Ljava/lang/String;
    :goto_1
    return-object v6

    .line 159
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "base":Ljava/lang/String;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    .restart local v4    # "paths":[Ljava/lang/String;
    .restart local v5    # "s":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 169
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "base":Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "paths":[Ljava/lang/String;
    .end local v5    # "s":Ljava/lang/String;
    :cond_2
    sget-object v6, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuFreqsAvailPath:Ljava/lang/String;

    goto :goto_1
.end method

.method public getGpuGovPath()Ljava/lang/String;
    .locals 8

    .prologue
    .line 122
    sget-object v6, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuGovPath:Ljava/lang/String;

    if-nez v6, :cond_2

    .line 123
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getGpuBasePath()Ljava/lang/String;

    move-result-object v1

    .line 124
    .local v1, "base":Ljava/lang/String;
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v6

    const v7, 0x7f080013

    invoke-virtual {v6, v7}, Lorg/namelessrom/devicecontrol/Application;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 125
    .local v4, "paths":[Ljava/lang/String;
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v5, v0, v2

    .line 126
    .local v5, "s":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 127
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuGovPath:Ljava/lang/String;

    .line 131
    .end local v5    # "s":Ljava/lang/String;
    :cond_0
    sget-object v6, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuGovPath:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 132
    const-string v6, ""

    .line 135
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "base":Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "paths":[Ljava/lang/String;
    :goto_1
    return-object v6

    .line 125
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "base":Ljava/lang/String;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    .restart local v4    # "paths":[Ljava/lang/String;
    .restart local v5    # "s":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 135
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "base":Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "paths":[Ljava/lang/String;
    .end local v5    # "s":Ljava/lang/String;
    :cond_2
    sget-object v6, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuGovPath:Ljava/lang/String;

    goto :goto_1
.end method

.method public getGpuGovsAvailablePath()Ljava/lang/String;
    .locals 8

    .prologue
    .line 139
    sget-object v6, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuGovsAvailablePath:Ljava/lang/String;

    if-nez v6, :cond_2

    .line 140
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getGpuBasePath()Ljava/lang/String;

    move-result-object v1

    .line 141
    .local v1, "base":Ljava/lang/String;
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v6

    const v7, 0x7f080014

    invoke-virtual {v6, v7}, Lorg/namelessrom/devicecontrol/Application;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 142
    .local v4, "paths":[Ljava/lang/String;
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v5, v0, v2

    .line 143
    .local v5, "s":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 144
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuGovsAvailablePath:Ljava/lang/String;

    .line 148
    .end local v5    # "s":Ljava/lang/String;
    :cond_0
    sget-object v6, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuGovsAvailablePath:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 149
    const/4 v6, 0x0

    .line 152
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "base":Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "paths":[Ljava/lang/String;
    :goto_1
    return-object v6

    .line 142
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "base":Ljava/lang/String;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    .restart local v4    # "paths":[Ljava/lang/String;
    .restart local v5    # "s":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 152
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "base":Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "paths":[Ljava/lang/String;
    .end local v5    # "s":Ljava/lang/String;
    :cond_2
    sget-object v6, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->gpuGovsAvailablePath:Ljava/lang/String;

    goto :goto_1
.end method

.method public getMaxFreq()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getGpuFreqMaxPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMinFreq()Ljava/lang/String;
    .locals 1

    .prologue
    .line 230
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getGpuFreqMinPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public restore(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 255
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 257
    .local v3, "sbCmd":Ljava/lang/StringBuilder;
    invoke-static {p1}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    move-result-object v4

    const-string v5, "gpu"

    invoke-virtual {v4, v5}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->getItemsByCategory(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 260
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/namelessrom/devicecontrol/objects/BootupItem;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    .line 261
    .local v1, "item":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    iget-object v4, v1, Lorg/namelessrom/devicecontrol/objects/BootupItem;->filename:Ljava/lang/String;

    iget-object v5, v1, Lorg/namelessrom/devicecontrol/objects/BootupItem;->value:Ljava/lang/String;

    invoke-static {v4, v5}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 264
    .end local v1    # "item":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method
