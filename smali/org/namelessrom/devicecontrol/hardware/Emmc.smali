.class public Lorg/namelessrom/devicecontrol/hardware/Emmc;
.super Ljava/lang/Object;
.source "Emmc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;
    }
.end annotation


# static fields
.field private static final EMMC_BUGGED_LIST:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;",
            ">;"
        }
    .end annotation
.end field

.field private static sInstance:Lorg/namelessrom/devicecontrol/hardware/Emmc;


# instance fields
.field private cid:Ljava/lang/String;

.field private date:Ljava/lang/String;

.field private mid:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private rev:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->EMMC_BUGGED_LIST:Ljava/util/ArrayList;

    .line 42
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->EMMC_BUGGED_LIST:Ljava/util/ArrayList;

    new-instance v1, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;

    const-string v2, "KYL00M"

    const-string v3, "15"

    const-string v4, "25"

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->EMMC_BUGGED_LIST:Ljava/util/ArrayList;

    new-instance v1, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;

    const-string v2, "M8G2FA"

    const-string v3, "15"

    const-string v4, "0"

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->EMMC_BUGGED_LIST:Ljava/util/ArrayList;

    new-instance v1, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;

    const-string v2, "MAG2GA"

    const-string v3, "15"

    const-string v4, "0"

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->EMMC_BUGGED_LIST:Ljava/util/ArrayList;

    new-instance v1, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;

    const-string v2, "MAG4FA"

    const-string v3, "15"

    const-string v4, "25"

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->EMMC_BUGGED_LIST:Ljava/util/ArrayList;

    new-instance v1, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;

    const-string v2, "MBG8FA"

    const-string v3, "15"

    const-string v4, "0"

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 47
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->EMMC_BUGGED_LIST:Ljava/util/ArrayList;

    new-instance v1, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;

    const-string v2, "MCGAFA"

    const-string v3, "15"

    const-string v4, "0"

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->EMMC_BUGGED_LIST:Ljava/util/ArrayList;

    new-instance v1, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;

    const-string v2, "VAL00M"

    const-string v3, "15"

    const-string v4, "0"

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->EMMC_BUGGED_LIST:Ljava/util/ArrayList;

    new-instance v1, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;

    const-string v2, "VTU001"

    const-string v3, "15"

    const-string v4, "f1"

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 50
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->EMMC_BUGGED_LIST:Ljava/util/ArrayList;

    new-instance v1, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;

    const-string v2, "VYL00M"

    const-string v3, "15"

    const-string v4, "0"

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->EMMC_BUGGED_LIST:Ljava/util/ArrayList;

    new-instance v1, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;

    const-string v2, "VZL00M"

    const-string v3, "15"

    const-string v4, "0"

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->cid:Ljava/lang/String;

    .line 34
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->date:Ljava/lang/String;

    .line 35
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->mid:Ljava/lang/String;

    .line 36
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->name:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->rev:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public static get()Lorg/namelessrom/devicecontrol/hardware/Emmc;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->sInstance:Lorg/namelessrom/devicecontrol/hardware/Emmc;

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Lorg/namelessrom/devicecontrol/hardware/Emmc;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/hardware/Emmc;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->sInstance:Lorg/namelessrom/devicecontrol/hardware/Emmc;

    .line 62
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->sInstance:Lorg/namelessrom/devicecontrol/hardware/Emmc;

    return-object v0
.end method


# virtual methods
.method public canBrick()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 103
    new-instance v1, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/hardware/Emmc;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/hardware/Emmc;->getMid()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/hardware/Emmc;->getRev()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v4, v5, v6}, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    .local v1, "emmc":Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;
    sget-object v4, Lorg/namelessrom/devicecontrol/hardware/Emmc;->EMMC_BUGGED_LIST:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;

    .line 105
    .local v0, "bugged":Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;
    if-eqz v0, :cond_0

    iget-object v4, v1, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;->name:Ljava/lang/String;

    iget-object v5, v0, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;->name:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v1, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;->mid:Ljava/lang/String;

    iget-object v5, v0, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;->mid:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v0, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;->rev:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, v0, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;->rev:Ljava/lang/String;

    const-string v5, "0"

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, v1, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;->rev:Ljava/lang/String;

    iget-object v5, v0, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;->rev:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 111
    :cond_1
    iget v4, v0, Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;->impact:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    const/4 v3, 0x1

    .line 115
    .end local v0    # "bugged":Lorg/namelessrom/devicecontrol/hardware/Emmc$EmmcBugged;
    :cond_2
    return v3
.end method

.method public getCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->cid:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 67
    const-string v0, "/sys/class/block/mmcblk0/device/cid"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->cid:Ljava/lang/String;

    .line 69
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->cid:Ljava/lang/String;

    return-object v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->date:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 74
    const-string v0, "/sys/class/block/mmcblk0/device/date"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->date:Ljava/lang/String;

    .line 76
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->date:Ljava/lang/String;

    return-object v0
.end method

.method public getMid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->mid:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 81
    const-string v0, "/sys/class/block/mmcblk0/device/manfid"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->mid:Ljava/lang/String;

    .line 83
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->mid:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->name:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 88
    const-string v0, "/sys/class/block/mmcblk0/device/name"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->name:Ljava/lang/String;

    .line 90
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getRev()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x14

    .line 94
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->rev:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 95
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/hardware/Emmc;->getCid()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/hardware/Emmc;->getCid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v2, :cond_1

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/hardware/Emmc;->getCid()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x12

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->rev:Ljava/lang/String;

    .line 99
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/Emmc;->rev:Ljava/lang/String;

    return-object v0

    .line 95
    :cond_1
    const-string v0, "-"

    goto :goto_0
.end method
