.class public Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;
.super Ljava/lang/Object;
.source "DisplayColorCalibration.java"


# static fields
.field private static sInstance:Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;


# instance fields
.field private ctrl:Ljava/lang/String;

.field private def:Ljava/lang/String;

.field private max:I

.field private min:I

.field private path:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v8

    invoke-virtual {v8}, Lorg/namelessrom/devicecontrol/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 40
    .local v7, "res":Landroid/content/res/Resources;
    const v8, 0x7f080019

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    .line 41
    .local v6, "paths":[Ljava/lang/String;
    const v8, 0x7f080015

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "ctrls":[Ljava/lang/String;
    const v8, 0x7f080016

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 43
    .local v1, "defs":[Ljava/lang/String;
    const v8, 0x7f080017

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 44
    .local v4, "maxs":[Ljava/lang/String;
    const v8, 0x7f080018

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    .line 46
    .local v5, "mins":[Ljava/lang/String;
    array-length v3, v6

    .line 47
    .local v3, "length":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_2

    .line 49
    aget-object v8, v6, v2

    invoke-static {v8}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 51
    aget-object v8, v6, v2

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->path:Ljava/lang/String;

    .line 54
    aget-object v8, v0, v2

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->ctrl:Ljava/lang/String;

    .line 55
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->ctrl:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    iget-object v8, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->ctrl:Ljava/lang/String;

    const-string v9, "-"

    invoke-static {v8, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    iget-object v8, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->ctrl:Ljava/lang/String;

    invoke-static {v8}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 60
    :cond_0
    const/4 v8, 0x0

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->ctrl:Ljava/lang/String;

    .line 64
    :cond_1
    aget-object v8, v4, v2

    invoke-static {v8}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v8

    iput v8, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->max:I

    .line 65
    const-string v8, "max --> %s"

    new-array v9, v12, [Ljava/lang/Object;

    iget v10, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->max:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {p0, v8, v9}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    aget-object v8, v5, v2

    invoke-static {v8}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v8

    iput v8, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->min:I

    .line 69
    const-string v8, "min --> %s"

    new-array v9, v12, [Ljava/lang/Object;

    iget v10, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->min:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {p0, v8, v9}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    aget-object v8, v1, v2

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->def:Ljava/lang/String;

    .line 73
    const-string v8, "max"

    iget-object v9, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->def:Ljava/lang/String;

    invoke-static {v8, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 74
    iget v8, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->max:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->def:Ljava/lang/String;

    .line 83
    :cond_2
    :goto_1
    return-void

    .line 75
    :cond_3
    const-string v8, "min"

    iget-object v9, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->def:Ljava/lang/String;

    invoke-static {v8, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 76
    iget v8, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->min:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->def:Ljava/lang/String;

    goto :goto_1

    .line 47
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0
.end method

.method public static get()Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->sInstance:Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;

    if-nez v0, :cond_0

    .line 87
    new-instance v0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->sInstance:Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;

    .line 89
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->sInstance:Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;

    return-object v0
.end method


# virtual methods
.method public getCurColors()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->path:Ljava/lang/String;

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDefValue()I
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->def:Ljava/lang/String;

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getMaxValue()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->max:I

    return v0
.end method

.method public getMinValue()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->min:I

    return v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->path:Ljava/lang/String;

    return-object v0
.end method

.method public isSupported()Z
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->path:Ljava/lang/String;

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setColors(Ljava/lang/String;)V
    .locals 3
    .param p1, "colors"    # Ljava/lang/String;

    .prologue
    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->path:Ljava/lang/String;

    invoke-static {v1, p1}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->ctrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 106
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->ctrl:Ljava/lang/String;

    const-string v2, "1"

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;)V

    .line 109
    return-void
.end method
