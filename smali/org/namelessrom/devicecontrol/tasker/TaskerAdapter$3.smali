.class Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;
.super Ljava/lang/Object;
.source "TaskerAdapter.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->onBindViewHolder(Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;

.field final synthetic val$item:Lorg/namelessrom/devicecontrol/tasker/TaskerItem;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;Lorg/namelessrom/devicecontrol/tasker/TaskerItem;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;->val$item:Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 98
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;

    # getter for: Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->access$000(Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 99
    .local v0, "alert":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f020060

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/DrawableHelper;->applyAccentColorFilter(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    .line 101
    const v1, 0x7f0e008f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 102
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;

    # getter for: Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->access$000(Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;)Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0e0090

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 103
    const/high16 v1, 0x1040000

    new-instance v2, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3$1;

    invoke-direct {v2, p0}, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3$1;-><init>(Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 109
    const v1, 0x1040013

    new-instance v2, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3$2;

    invoke-direct {v2, p0}, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3$2;-><init>(Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 120
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 121
    const/4 v1, 0x1

    return v1
.end method
