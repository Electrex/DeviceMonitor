.class Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$UpdateTaskerCardList;
.super Landroid/os/AsyncTask;
.source "TaskerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateTaskerCardList"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lorg/namelessrom/devicecontrol/tasker/TaskerItem;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;


# direct methods
.method private constructor <init>(Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$UpdateTaskerCardList;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;
    .param p2, "x1"    # Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$1;

    .prologue
    .line 127
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$UpdateTaskerCardList;-><init>(Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 127
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$UpdateTaskerCardList;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 2
    .param p1, "voids"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/tasker/TaskerItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$UpdateTaskerCardList;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 136
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->loadConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v1

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->items:Ljava/util/ArrayList;

    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 127
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$UpdateTaskerCardList;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/tasker/TaskerItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/tasker/TaskerItem;>;"
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 141
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$UpdateTaskerCardList;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$UpdateTaskerCardList;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;

    # getter for: Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->mEmptyView:Landroid/view/View;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->access$100(Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 143
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$UpdateTaskerCardList;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;

    # getter for: Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->access$200(Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$UpdateTaskerCardList;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;

    # getter for: Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->access$200(Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v0

    new-instance v1, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$UpdateTaskerCardList;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;-><init>(Landroid/app/Activity;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 149
    :goto_0
    return-void

    .line 146
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$UpdateTaskerCardList;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;

    # getter for: Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->mEmptyView:Landroid/view/View;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->access$100(Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 147
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$UpdateTaskerCardList;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;

    # getter for: Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->access$200(Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 130
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$UpdateTaskerCardList;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;

    # getter for: Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->mEmptyView:Landroid/view/View;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->access$100(Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 131
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$UpdateTaskerCardList;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;

    # getter for: Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->access$200(Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 132
    return-void
.end method
