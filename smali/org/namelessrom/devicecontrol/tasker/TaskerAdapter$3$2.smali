.class Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3$2;
.super Ljava/lang/Object;
.source "TaskerAdapter.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;->onLongClick(Landroid/view/View;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3$2;->this$1:Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "d"    # Landroid/content/DialogInterface;
    .param p2, "b"    # I

    .prologue
    .line 112
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3$2;->this$1:Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;

    # getter for: Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->access$000(Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3$2;->this$1:Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;->val$item:Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->deleteItem(Lorg/namelessrom/devicecontrol/tasker/TaskerItem;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3$2;->this$1:Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;

    # getter for: Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->access$000(Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    .line 115
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3$2;->this$1:Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;

    # getter for: Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->mTasks:Ljava/util/List;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->access$100(Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3$2;->this$1:Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;->val$item:Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 116
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 117
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3$2;->this$1:Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->notifyDataSetChanged()V

    .line 118
    return-void
.end method
