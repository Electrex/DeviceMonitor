.class public Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "TaskerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mTasks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/tasker/TaskerItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/util/List;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/tasker/TaskerItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p2, "tasks":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/tasker/TaskerItem;>;"
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 50
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->mActivity:Landroid/app/Activity;

    .line 51
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->mTasks:Ljava/util/List;

    .line 53
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->mTasks:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 54
    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;

    .prologue
    .line 45
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;

    .prologue
    .line 45
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->mTasks:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->mTasks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "x1"    # I

    .prologue
    .line 45
    check-cast p1, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;

    .end local p1    # "x0":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p0, p1, p2}, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->onBindViewHolder(Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;I)V
    .locals 3
    .param p1, "holder"    # Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 72
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->mTasks:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    .line 74
    .local v0, "item":Lorg/namelessrom/devicecontrol/tasker/TaskerItem;
    iget-object v1, p1, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;->image:Landroid/widget/ImageView;

    iget-object v2, v0, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->category:Ljava/lang/String;

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor;->getImageForCategory(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 75
    iget-object v1, p1, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;->trigger:Landroid/widget/TextView;

    iget-object v2, v0, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->trigger:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v1, p1, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;->action:Landroid/widget/TextView;

    iget-object v2, v0, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v1, p1, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;->value:Landroid/widget/TextView;

    iget-object v2, v0, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->value:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v1, p1, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;->enabled:Landroid/widget/Switch;

    iget-boolean v2, v0, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->enabled:Z

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setChecked(Z)V

    .line 79
    iget-object v1, p1, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;->enabled:Landroid/widget/Switch;

    new-instance v2, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$1;

    invoke-direct {v2, p0, v0}, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$1;-><init>(Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;Lorg/namelessrom/devicecontrol/tasker/TaskerItem;)V

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 88
    iget-object v1, p1, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;->cardView:Landroid/support/v7/widget/CardView;

    new-instance v2, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$2;

    invoke-direct {v2, p0, v0}, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$2;-><init>(Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;Lorg/namelessrom/devicecontrol/tasker/TaskerItem;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/CardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    iget-object v1, p1, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;->cardView:Landroid/support/v7/widget/CardView;

    new-instance v2, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;

    invoke-direct {v2, p0, v0}, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$3;-><init>(Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;Lorg/namelessrom/devicecontrol/tasker/TaskerItem;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/CardView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 124
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1
    .param p1, "x0"    # Landroid/view/ViewGroup;
    .param p2, "x1"    # I

    .prologue
    .line 45
    invoke-virtual {p0, p1, p2}, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 62
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/Application;->isDarkTheme()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 63
    const v0, 0x7f040024

    .line 67
    .local v0, "resId":I
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 68
    .local v1, "v":Landroid/view/View;
    new-instance v2, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;

    invoke-direct {v2, v1}, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;-><init>(Landroid/view/View;)V

    return-object v2

    .line 65
    .end local v0    # "resId":I
    .end local v1    # "v":Landroid/view/View;
    :cond_0
    const v0, 0x7f040025

    .restart local v0    # "resId":I
    goto :goto_0
.end method
