.class public Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;
.source "TaskerFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$UpdateTaskerCardList;
    }
.end annotation


# instance fields
.field private mEmptyView:Landroid/view/View;

.field private mRecyclerView:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;-><init>()V

    .line 127
    return-void
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->mEmptyView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;)Landroid/support/v7/widget/RecyclerView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method private refreshListView()V
    .locals 2

    .prologue
    .line 96
    new-instance v0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$UpdateTaskerCardList;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$UpdateTaskerCardList;-><init>(Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$UpdateTaskerCardList;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 97
    return-void
.end method


# virtual methods
.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 55
    const v0, 0x7f0e020f

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 91
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 92
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->refreshListView()V

    .line 93
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 100
    invoke-super {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 101
    const v3, 0x7f100004

    invoke-virtual {p2, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 103
    const v3, 0x7f0c0116

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 105
    .local v1, "toggle":Landroid/view/MenuItem;
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v2

    .local v2, "v":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 106
    const v3, 0x7f0c003f

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SwitchCompat;

    .line 107
    .local v0, "sw":Landroid/support/v7/widget/SwitchCompat;
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v3

    iget-boolean v3, v3, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->enabled:Z

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/SwitchCompat;->setChecked(Z)V

    .line 108
    new-instance v3, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$2;

    invoke-direct {v3, p0}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$2;-><init>(Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 125
    .end local v0    # "sw":Landroid/support/v7/widget/SwitchCompat;
    .end local v2    # "v":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 64
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->setHasOptionsMenu(Z)V

    .line 65
    const v3, 0x7f04002f

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 67
    .local v2, "v":Landroid/view/View;
    const v3, 0x102000a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/RecyclerView;

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 68
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v4, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 69
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v4, Landroid/support/v7/widget/DefaultItemAnimator;

    invoke-direct {v4}, Landroid/support/v7/widget/DefaultItemAnimator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 71
    const v3, 0x1020004

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->mEmptyView:Landroid/view/View;

    .line 72
    const v3, 0x7f0c009b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/melnykov/fab/FloatingActionButton;

    .line 73
    .local v0, "fabAdd":Lcom/melnykov/fab/FloatingActionButton;
    new-instance v3, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$1;

    invoke-direct {v3, p0}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$1;-><init>(Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;)V

    invoke-virtual {v0, v3}, Lcom/melnykov/fab/FloatingActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    const v3, 0x7f0c0099

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/melnykov/fab/ObservableScrollView;

    .line 83
    .local v1, "scrollView":Lcom/melnykov/fab/ObservableScrollView;
    if-eqz v1, :cond_0

    .line 84
    invoke-virtual {v0, v1}, Lcom/melnykov/fab/FloatingActionButton;->attachToScrollView(Lcom/melnykov/fab/ObservableScrollView;)V

    .line 87
    :cond_0
    return-object v2
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 58
    invoke-super {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;->onResume()V

    .line 59
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->refreshListView()V

    .line 60
    return-void
.end method
