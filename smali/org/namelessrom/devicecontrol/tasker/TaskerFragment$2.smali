.class Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$2;
.super Ljava/lang/Object;
.source "TaskerFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$2;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "b"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 112
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$2;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v0

    iput-boolean p2, v0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->enabled:Z

    .line 113
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$2;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$2;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    .line 114
    new-instance v1, Landroid/content/ComponentName;

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment$2;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-class v2, Lorg/namelessrom/devicecontrol/services/TaskerService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->toggleComponent(Landroid/content/ComponentName;Z)V

    .line 116
    if-eqz p2, :cond_1

    .line 117
    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/Utils;->startTaskerService()Z

    .line 121
    :goto_1
    return-void

    .line 114
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 119
    :cond_1
    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/Utils;->stopTaskerService()V

    goto :goto_1
.end method
