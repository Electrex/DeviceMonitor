.class Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$1;
.super Ljava/lang/Object;
.source "TaskerAdapter.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->onBindViewHolder(Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;

.field final synthetic val$item:Lorg/namelessrom/devicecontrol/tasker/TaskerItem;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;Lorg/namelessrom/devicecontrol/tasker/TaskerItem;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$1;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$1;->val$item:Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 81
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$1;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;

    # getter for: Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->access$000(Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v0

    .line 82
    .local v0, "configuration":Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$1;->val$item:Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->deleteItem(Lorg/namelessrom/devicecontrol/tasker/TaskerItem;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    .line 83
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$1;->val$item:Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    iput-boolean p2, v1, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->enabled:Z

    .line 84
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$1;->val$item:Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->addItem(Lorg/namelessrom/devicecontrol/tasker/TaskerItem;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v1

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$1;->this$0:Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;

    # getter for: Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;->access$000(Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    .line 85
    return-void
.end method
