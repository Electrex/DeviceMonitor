.class public Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "TaskerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TaskerViewHolder"
.end annotation


# instance fields
.field public final action:Landroid/widget/TextView;

.field public final cardView:Landroid/support/v7/widget/CardView;

.field public final enabled:Landroid/widget/Switch;

.field public final image:Landroid/widget/ImageView;

.field public final trigger:Landroid/widget/TextView;

.field public final value:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 135
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 136
    check-cast p1, Landroid/support/v7/widget/CardView;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;->cardView:Landroid/support/v7/widget/CardView;

    .line 137
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;->cardView:Landroid/support/v7/widget/CardView;

    const v1, 0x7f0c00ce

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;->image:Landroid/widget/ImageView;

    .line 138
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;->cardView:Landroid/support/v7/widget/CardView;

    const v1, 0x7f0c00cf

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;->trigger:Landroid/widget/TextView;

    .line 139
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;->cardView:Landroid/support/v7/widget/CardView;

    const v1, 0x7f0c00d0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;->action:Landroid/widget/TextView;

    .line 140
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;->cardView:Landroid/support/v7/widget/CardView;

    const v1, 0x7f0c00d1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;->value:Landroid/widget/TextView;

    .line 141
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;->cardView:Landroid/support/v7/widget/CardView;

    const v1, 0x7f0c00d2

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/tasker/TaskerAdapter$TaskerViewHolder;->enabled:Landroid/widget/Switch;

    .line 142
    return-void
.end method
