.class public Lorg/namelessrom/devicecontrol/MainActivity;
.super Lorg/namelessrom/devicecontrol/activities/BaseActivity;
.source "MainActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final lockObject:Ljava/lang/Object;

.field private static mBackPressed:J

.field public static sDisableFragmentAnimations:Z

.field public static sMaterialMenu:Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;

.field public static sSlidingMenu:Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;


# instance fields
.field private mCurrentFragment:Landroid/support/v4/app/Fragment;

.field private mFragmentTitle:I

.field private final mMenuEntries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mMenuIcons:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSubFragmentTitle:I

.field private mTitle:I

.field private mToast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/MainActivity;->lockObject:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const v0, 0x7f0e0135

    .line 83
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;-><init>()V

    .line 100
    iput v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    .line 101
    iput v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mFragmentTitle:I

    .line 102
    const/4 v0, -0x1

    iput v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuEntries:Ljava/util/ArrayList;

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuIcons:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/MainActivity;)I
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/MainActivity;

    .prologue
    .line 83
    iget v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    return v0
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/MainActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/MainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/MainActivity;->onCustomBackPressed(Z)V

    return-void
.end method

.method public static loadFragment(Landroid/app/Activity;I)V
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "id"    # I

    .prologue
    .line 377
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/namelessrom/devicecontrol/MainActivity;->loadFragment(Landroid/app/Activity;IZ)V

    .line 378
    return-void
.end method

.method public static loadFragment(Landroid/app/Activity;IZ)V
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "id"    # I
    .param p2, "onResume"    # Z

    .prologue
    .line 381
    instance-of v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;

    if-eqz v0, :cond_0

    .line 382
    check-cast p0, Lorg/namelessrom/devicecontrol/MainActivity;

    .end local p0    # "activity":Landroid/app/Activity;
    invoke-direct {p0, p1, p2}, Lorg/namelessrom/devicecontrol/MainActivity;->loadFragmentPrivate(IZ)V

    .line 384
    :cond_0
    return-void
.end method

.method private loadFragmentPrivate(IZ)V
    .locals 10
    .param p1, "i"    # I
    .param p2, "onResume"    # Z

    .prologue
    const/4 v9, 0x0

    const v8, 0x7f0e020b

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v7, -0x1

    .line 387
    sparse-switch p1, :sswitch_data_0

    .line 391
    if-nez p2, :cond_0

    new-instance v6, Lorg/namelessrom/devicecontrol/about/AboutFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/about/AboutFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 392
    :cond_0
    const v6, 0x7f0e0026

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    .line 393
    iput v7, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    .line 515
    :goto_0
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/MainActivity;->restoreActionBar()V

    .line 517
    if-eqz p2, :cond_19

    .line 553
    :goto_1
    return-void

    .line 397
    :sswitch_0
    if-nez p2, :cond_1

    new-instance v6, Lorg/namelessrom/devicecontrol/device/DeviceInformationFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/device/DeviceInformationFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 398
    :cond_1
    const v6, 0x7f0e0091

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    .line 399
    iput v7, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    goto :goto_0

    .line 403
    :sswitch_1
    if-nez p2, :cond_2

    new-instance v6, Lorg/namelessrom/devicecontrol/device/DeviceFeatureFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 404
    :cond_2
    const v6, 0x7f0e00db

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    .line 405
    iput v7, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    goto :goto_0

    .line 408
    :sswitch_2
    if-nez p2, :cond_3

    new-instance v6, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/device/sub/FastChargeFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 409
    :cond_3
    const v6, 0x7f0e00d5

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    goto :goto_0

    .line 412
    :sswitch_3
    if-nez p2, :cond_4

    new-instance v6, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/device/sub/SoundControlFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 413
    :cond_4
    const v6, 0x7f0e01f9

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    goto :goto_0

    .line 416
    :sswitch_4
    if-nez p2, :cond_5

    new-instance v6, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 417
    :cond_5
    const v6, 0x7f0e014f

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    goto :goto_0

    .line 420
    :sswitch_5
    if-nez p2, :cond_6

    new-instance v6, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 421
    :cond_6
    const v6, 0x7f0e0227

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    goto :goto_0

    .line 424
    :sswitch_6
    if-nez p2, :cond_7

    new-instance v6, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 425
    :cond_7
    const v6, 0x7f0e023c

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    goto :goto_0

    .line 428
    :sswitch_7
    if-nez p2, :cond_8

    new-instance v6, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 429
    :cond_8
    const v6, 0x7f0e00c3

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    goto/16 :goto_0

    .line 433
    :sswitch_8
    if-nez p2, :cond_9

    new-instance v6, Lorg/namelessrom/devicecontrol/ui/fragments/performance/InformationFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/InformationFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 434
    :cond_9
    const v6, 0x7f0e013a

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    .line 435
    iput v7, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    goto/16 :goto_0

    .line 439
    :sswitch_9
    if-nez p2, :cond_a

    new-instance v6, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/cpu/CpuSettingsFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 440
    :cond_a
    const v6, 0x7f0e007d

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    .line 441
    iput v7, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    goto/16 :goto_0

    .line 444
    :sswitch_a
    if-nez p2, :cond_b

    new-instance v6, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/cpu/GovernorFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 445
    :cond_b
    const v6, 0x7f0e0076

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    goto/16 :goto_0

    .line 449
    :sswitch_b
    if-nez p2, :cond_c

    new-instance v6, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 450
    :cond_c
    const v6, 0x7f0e012a

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    .line 451
    iput v7, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    goto/16 :goto_0

    .line 455
    :sswitch_c
    if-nez p2, :cond_d

    new-instance v6, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 456
    :cond_d
    const v6, 0x7f0e00fc

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    .line 457
    iput v7, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    goto/16 :goto_0

    .line 460
    :sswitch_d
    if-nez p2, :cond_e

    new-instance v6, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/IoSchedConfigFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/IoSchedConfigFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 461
    :cond_e
    const v6, 0x7f0e0145

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    goto/16 :goto_0

    .line 465
    :sswitch_e
    if-nez p2, :cond_f

    new-instance v6, Lorg/namelessrom/devicecontrol/ui/fragments/performance/ThermalFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/ThermalFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 466
    :cond_f
    const v6, 0x7f0e0216

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    .line 467
    iput v7, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    goto/16 :goto_0

    .line 471
    :sswitch_f
    if-nez p2, :cond_10

    new-instance v6, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/tasker/TaskerFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 472
    :cond_10
    const v6, 0x7f0e020f

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    .line 473
    iput v7, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    goto/16 :goto_0

    .line 477
    :sswitch_10
    if-nez p2, :cond_11

    new-instance v6, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 478
    :cond_11
    const v6, 0x7f0e0101

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    .line 479
    iput v7, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    goto/16 :goto_0

    .line 483
    :sswitch_11
    if-nez p2, :cond_12

    new-instance v6, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 484
    :cond_12
    const v6, 0x7f0e0165

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    .line 485
    iput v7, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    goto/16 :goto_0

    .line 488
    :sswitch_12
    if-nez p2, :cond_13

    new-instance v6, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 489
    :cond_13
    iput v8, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    iput v8, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    goto/16 :goto_0

    .line 492
    :sswitch_13
    if-nez p2, :cond_14

    new-instance v6, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 493
    :cond_14
    iput v8, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    iput v8, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    goto/16 :goto_0

    .line 496
    :sswitch_14
    if-nez p2, :cond_15

    new-instance v6, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 497
    :cond_15
    const v6, 0x7f0e003f

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    goto/16 :goto_0

    .line 500
    :sswitch_15
    if-nez p2, :cond_16

    new-instance v6, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 501
    :cond_16
    const v6, 0x7f0e0025

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    goto/16 :goto_0

    .line 504
    :sswitch_16
    if-nez p2, :cond_17

    new-instance v6, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 505
    :cond_17
    const v6, 0x7f0e024c

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    goto/16 :goto_0

    .line 509
    :sswitch_17
    if-nez p2, :cond_18

    new-instance v6, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/PreferencesFragment;

    invoke-direct {v6}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/PreferencesFragment;-><init>()V

    iput-object v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 510
    :cond_18
    const v6, 0x7f0e019f

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mFragmentTitle:I

    iput v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    .line 511
    iput v7, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    goto/16 :goto_0

    .line 521
    :cond_19
    iget v6, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    if-eq v6, v7, :cond_1c

    move v3, v4

    .line 523
    .local v3, "isSubFragment":Z
    :goto_2
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 524
    .local v0, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    if-nez v3, :cond_1a

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v6

    if-lez v6, :cond_1a

    .line 526
    sput-boolean v4, Lorg/namelessrom/devicecontrol/utils/AppHelper;->preventOnResume:Z

    .line 527
    sput-boolean v4, Lorg/namelessrom/devicecontrol/MainActivity;->sDisableFragmentAnimations:Z

    .line 528
    invoke-virtual {v0, v9, v4}, Landroid/support/v4/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z

    .line 529
    sput-boolean v5, Lorg/namelessrom/devicecontrol/MainActivity;->sDisableFragmentAnimations:Z

    .line 531
    sput-boolean v5, Lorg/namelessrom/devicecontrol/utils/AppHelper;->preventOnResume:Z

    .line 534
    :cond_1a
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 536
    .local v1, "ft":Landroid/support/v4/app/FragmentTransaction;
    if-eqz v3, :cond_1b

    .line 537
    const v4, 0x7f050009

    const v5, 0x7f05000b

    const v6, 0x7f050008

    const v7, 0x7f05000a

    invoke-virtual {v1, v4, v5, v6, v7}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(IIII)Landroid/support/v4/app/FragmentTransaction;

    .line 539
    invoke-virtual {v1, v9}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 542
    :cond_1b
    const v4, 0x7f0c0072

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v4, v5}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 543
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 546
    if-eqz v3, :cond_1d

    .line 547
    sget-object v2, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->ARROW:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    .line 552
    .local v2, "iconState":Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;
    :goto_3
    sget-object v4, Lorg/namelessrom/devicecontrol/MainActivity;->sMaterialMenu:Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;

    invoke-virtual {v4, v2}, Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;->animateState(Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;)V

    goto/16 :goto_1

    .end local v0    # "fragmentManager":Landroid/support/v4/app/FragmentManager;
    .end local v1    # "ft":Landroid/support/v4/app/FragmentTransaction;
    .end local v2    # "iconState":Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;
    .end local v3    # "isSubFragment":Z
    :cond_1c
    move v3, v5

    .line 521
    goto :goto_2

    .line 549
    .restart local v0    # "fragmentManager":Landroid/support/v4/app/FragmentManager;
    .restart local v1    # "ft":Landroid/support/v4/app/FragmentTransaction;
    .restart local v3    # "isSubFragment":Z
    :cond_1d
    sget-object v2, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->BURGER:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    .restart local v2    # "iconState":Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;
    goto :goto_3

    .line 387
    :sswitch_data_0
    .sparse-switch
        0x7f0e007d -> :sswitch_9
        0x7f0e0092 -> :sswitch_0
        0x7f0e00db -> :sswitch_1
        0x7f0e00fc -> :sswitch_c
        0x7f0e0101 -> :sswitch_10
        0x7f0e012a -> :sswitch_b
        0x7f0e013a -> :sswitch_8
        0x7f0e0165 -> :sswitch_11
        0x7f0e019f -> :sswitch_17
        0x7f0e020f -> :sswitch_f
        0x7f0e0216 -> :sswitch_e
        0x7f0e0465 -> :sswitch_a
        0x7f0e04c3 -> :sswitch_2
        0x7f0e04e4 -> :sswitch_d
        0x7f0e0527 -> :sswitch_3
        0x7f0e054d -> :sswitch_12
        0x7f0e0561 -> :sswitch_13
        0x7f0e058b -> :sswitch_4
        0x7f0e05b1 -> :sswitch_14
        0x7f0e05ef -> :sswitch_5
        0x7f0e0615 -> :sswitch_15
        0x7f0e0653 -> :sswitch_6
        0x7f0e0679 -> :sswitch_16
        0x7f0e06b7 -> :sswitch_7
    .end sparse-switch
.end method

.method private onCustomBackPressed(Z)V
    .locals 7
    .param p1, "animatePressed"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 282
    const-string v2, "onCustomBackPressed(%s)"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {p0, v2, v3}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 285
    sget-object v2, Lorg/namelessrom/devicecontrol/MainActivity;->sSlidingMenu:Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;

    invoke-virtual {v2}, Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;->isMenuShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 286
    sget-object v2, Lorg/namelessrom/devicecontrol/MainActivity;->sSlidingMenu:Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;

    invoke-virtual {v2, v5}, Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;->toggle(Z)V

    .line 347
    :goto_0
    return-void

    .line 291
    :cond_0
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    instance-of v2, v2, Lorg/namelessrom/devicecontrol/listeners/OnBackPressedListener;

    if-eqz v2, :cond_2

    .line 292
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    check-cast v1, Lorg/namelessrom/devicecontrol/listeners/OnBackPressedListener;

    .line 295
    .local v1, "listener":Lorg/namelessrom/devicecontrol/listeners/OnBackPressedListener;
    invoke-interface {v1}, Lorg/namelessrom/devicecontrol/listeners/OnBackPressedListener;->onBackPressed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 296
    const-string v2, "onBackPressed()"

    invoke-static {p0, v2}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 303
    :cond_1
    invoke-interface {v1}, Lorg/namelessrom/devicecontrol/listeners/OnBackPressedListener;->showBurger()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 304
    sget-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->BURGER:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    .line 309
    .local v0, "iconState":Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;
    :goto_1
    const-string v2, "iconState: %s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v6

    invoke-static {p0, v2, v3}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 312
    if-eqz p1, :cond_4

    .line 313
    sget-object v2, Lorg/namelessrom/devicecontrol/MainActivity;->sMaterialMenu:Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;

    invoke-virtual {v2, v0}, Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;->animatePressedState(Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;)V

    .line 322
    .end local v0    # "iconState":Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;
    .end local v1    # "listener":Lorg/namelessrom/devicecontrol/listeners/OnBackPressedListener;
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v2

    if-lez v2, :cond_6

    .line 323
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->popBackStack()V

    .line 326
    iget v2, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_5

    .line 327
    iget v2, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mSubFragmentTitle:I

    iput v2, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    .line 331
    :goto_3
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/MainActivity;->restoreActionBar()V

    goto :goto_0

    .line 306
    .restart local v1    # "listener":Lorg/namelessrom/devicecontrol/listeners/OnBackPressedListener;
    :cond_3
    sget-object v0, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->ARROW:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    .restart local v0    # "iconState":Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;
    goto :goto_1

    .line 315
    :cond_4
    sget-object v2, Lorg/namelessrom/devicecontrol/MainActivity;->sMaterialMenu:Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;

    invoke-virtual {v2, v0}, Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;->animateState(Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;)V

    goto :goto_2

    .line 329
    .end local v0    # "iconState":Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;
    .end local v1    # "listener":Lorg/namelessrom/devicecontrol/listeners/OnBackPressedListener;
    :cond_5
    iget v2, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mFragmentTitle:I

    iput v2, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    goto :goto_3

    .line 338
    :cond_6
    sget-wide v2, Lorg/namelessrom/devicecontrol/MainActivity;->mBackPressed:J

    const-wide/16 v4, 0x7d0

    add-long/2addr v2, v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_8

    .line 339
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mToast:Landroid/widget/Toast;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->cancel()V

    .line 340
    :cond_7
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/MainActivity;->finish()V

    .line 346
    :goto_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sput-wide v2, Lorg/namelessrom/devicecontrol/MainActivity;->mBackPressed:J

    goto :goto_0

    .line 342
    :cond_8
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/MainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e0015

    invoke-virtual {p0, v3}, Lorg/namelessrom/devicecontrol/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mToast:Landroid/widget/Toast;

    .line 344
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_4
.end method

.method private restoreActionBar()V
    .locals 2

    .prologue
    .line 556
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/MainActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 557
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v0, :cond_0

    .line 558
    iget v1, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mTitle:I

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(I)V

    .line 560
    :cond_0
    return-void
.end method

.method public static setSwipeOnContent(Z)V
    .locals 2
    .param p0, "swipeOnContent"    # Z

    .prologue
    .line 563
    sget-object v0, Lorg/namelessrom/devicecontrol/MainActivity;->sSlidingMenu:Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;

    if-nez v0, :cond_0

    .line 567
    :goto_0
    return-void

    .line 565
    :cond_0
    sget-object v1, Lorg/namelessrom/devicecontrol/MainActivity;->sSlidingMenu:Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;->setTouchModeAbove(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private setupMenuLists()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 109
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuEntries:Ljava/util/ArrayList;

    const v1, 0x7f0e0091

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuIcons:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuEntries:Ljava/util/ArrayList;

    const v1, 0x7f0e0092

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuIcons:Ljava/util/ArrayList;

    const v1, 0x7f020062

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuEntries:Ljava/util/ArrayList;

    const v1, 0x7f0e00db

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuIcons:Ljava/util/ArrayList;

    const v1, 0x7f020061

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuEntries:Ljava/util/ArrayList;

    const v1, 0x7f0e018f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuIcons:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuEntries:Ljava/util/ArrayList;

    const v1, 0x7f0e013a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuIcons:Ljava/util/ArrayList;

    const v1, 0x7f020070

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuEntries:Ljava/util/ArrayList;

    const v1, 0x7f0e007d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuIcons:Ljava/util/ArrayList;

    const v1, 0x7f02006f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuEntries:Ljava/util/ArrayList;

    const v1, 0x7f0e012a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuIcons:Ljava/util/ArrayList;

    const v1, 0x7f020063

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuEntries:Ljava/util/ArrayList;

    const v1, 0x7f0e00fc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 132
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuIcons:Ljava/util/ArrayList;

    const v1, 0x7f02007d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    const v0, 0x7f0e0097

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0e00e8

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuEntries:Ljava/util/ArrayList;

    const v1, 0x7f0e0216

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuIcons:Ljava/util/ArrayList;

    const v1, 0x7f02006b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    :cond_1
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuEntries:Ljava/util/ArrayList;

    const v1, 0x7f0e021d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuIcons:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuEntries:Ljava/util/ArrayList;

    const v1, 0x7f0e020f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuIcons:Ljava/util/ArrayList;

    const v1, 0x7f020064

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuEntries:Ljava/util/ArrayList;

    const v1, 0x7f0e0101

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuIcons:Ljava/util/ArrayList;

    const v1, 0x7f020066

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuEntries:Ljava/util/ArrayList;

    const v1, 0x7f0e0165

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuIcons:Ljava/util/ArrayList;

    const v1, 0x7f020080

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 350
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/namelessrom/devicecontrol/MainActivity;->onCustomBackPressed(Z)V

    .line 351
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 251
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 252
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    .line 260
    :goto_0
    return-void

    .line 254
    :pswitch_0
    const v1, 0x7f0e019f

    invoke-direct {p0, v1, v2}, Lorg/namelessrom/devicecontrol/MainActivity;->loadFragmentPrivate(IZ)V

    goto :goto_0

    .line 257
    :pswitch_1
    const v1, 0x7f0e0010

    invoke-direct {p0, v1, v2}, Lorg/namelessrom/devicecontrol/MainActivity;->loadFragmentPrivate(IZ)V

    goto :goto_0

    .line 252
    nop

    :pswitch_data_0
    .packed-switch 0x7f0c00c5
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 168
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 169
    const v7, 0x7f04001b

    invoke-virtual {p0, v7}, Lorg/namelessrom/devicecontrol/MainActivity;->setContentView(I)V

    .line 172
    const v7, 0x7f0c0061

    invoke-virtual {p0, v7}, Lorg/namelessrom/devicecontrol/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/support/v7/widget/Toolbar;

    .line 173
    .local v5, "toolbar":Landroid/support/v7/widget/Toolbar;
    invoke-virtual {p0, v5}, Lorg/namelessrom/devicecontrol/MainActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 174
    new-instance v7, Lorg/namelessrom/devicecontrol/MainActivity$1;

    invoke-direct {v7, p0}, Lorg/namelessrom/devicecontrol/MainActivity$1;-><init>(Lorg/namelessrom/devicecontrol/MainActivity;)V

    invoke-virtual {v5, v7}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    new-instance v7, Lorg/namelessrom/devicecontrol/MainActivity$2;

    const/4 v8, -0x1

    sget-object v9, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;->THIN:Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    invoke-direct {v7, p0, p0, v8, v9}, Lorg/namelessrom/devicecontrol/MainActivity$2;-><init>(Lorg/namelessrom/devicecontrol/MainActivity;Landroid/app/Activity;ILcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;)V

    sput-object v7, Lorg/namelessrom/devicecontrol/MainActivity;->sMaterialMenu:Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;

    .line 189
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x15

    if-lt v7, v8, :cond_0

    .line 190
    sget-object v7, Lorg/namelessrom/devicecontrol/MainActivity;->sMaterialMenu:Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;

    invoke-virtual {v7, v10}, Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;->setNeverDrawTouch(Z)V

    .line 193
    :cond_0
    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/Utils;->setupDirectories()V

    .line 195
    const v7, 0x7f0c0072

    invoke-virtual {p0, v7}, Lorg/namelessrom/devicecontrol/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 196
    .local v0, "container":Landroid/widget/FrameLayout;
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x10

    if-lt v7, v8, :cond_4

    .line 197
    invoke-virtual {v0, v12}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 203
    :goto_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/MainActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f04003b

    invoke-virtual {v7, v8, v0, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 204
    .local v6, "v":Landroid/view/View;
    const v7, 0x7f0c00c3

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ListView;

    .line 205
    .local v4, "menuList":Landroid/widget/ListView;
    const v7, 0x7f0c00c4

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 207
    .local v3, "menuContainer":Landroid/widget/LinearLayout;
    const v7, 0x7f0c00c5

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 208
    const v7, 0x7f0c00c6

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    new-instance v7, Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;

    invoke-direct {v7, p0}, Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;-><init>(Landroid/content/Context;)V

    sput-object v7, Lorg/namelessrom/devicecontrol/MainActivity;->sSlidingMenu:Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;

    .line 211
    sget-object v7, Lorg/namelessrom/devicecontrol/MainActivity;->sSlidingMenu:Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;

    invoke-virtual {v7, v11}, Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;->setMode(I)V

    .line 212
    sget-object v7, Lorg/namelessrom/devicecontrol/MainActivity;->sSlidingMenu:Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;

    const v8, 0x7f0b0047

    invoke-virtual {v7, v8}, Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;->setShadowWidthRes(I)V

    .line 213
    sget-object v7, Lorg/namelessrom/devicecontrol/MainActivity;->sSlidingMenu:Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;

    const v8, 0x7f020087

    invoke-virtual {v7, v8}, Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;->setShadowDrawable(I)V

    .line 214
    sget-object v7, Lorg/namelessrom/devicecontrol/MainActivity;->sSlidingMenu:Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;

    const v8, 0x7f0b0048

    invoke-virtual {v7, v8}, Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;->setBehindWidthRes(I)V

    .line 215
    sget-object v7, Lorg/namelessrom/devicecontrol/MainActivity;->sSlidingMenu:Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;

    invoke-virtual {v7, v10}, Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;->setFadeEnabled(Z)V

    .line 216
    sget-object v7, Lorg/namelessrom/devicecontrol/MainActivity;->sSlidingMenu:Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;

    const v8, 0x3ee66666    # 0.45f

    invoke-virtual {v7, v8}, Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;->setFadeDegree(F)V

    .line 217
    sget-object v7, Lorg/namelessrom/devicecontrol/MainActivity;->sSlidingMenu:Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;

    invoke-virtual {v7, p0, v10}, Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;->attachToActivity(Landroid/app/Activity;I)V

    .line 218
    sget-object v7, Lorg/namelessrom/devicecontrol/MainActivity;->sSlidingMenu:Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;

    invoke-virtual {v7, v6}, Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;->setMenu(Landroid/view/View;)V

    .line 221
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v7

    iget-boolean v7, v7, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->swipeOnContent:Z

    invoke-static {v7}, Lorg/namelessrom/devicecontrol/MainActivity;->setSwipeOnContent(Z)V

    .line 224
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/MainActivity;->setupMenuLists()V

    .line 225
    new-instance v2, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;

    const v7, 0x7f04003c

    iget-object v8, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuEntries:Ljava/util/ArrayList;

    iget-object v9, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mMenuIcons:Ljava/util/ArrayList;

    invoke-direct {v2, p0, v7, v8, v9}, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 227
    .local v2, "mAdapter":Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;
    invoke-virtual {v4, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 228
    invoke-virtual {v4, v10}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 229
    invoke-virtual {v4, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 231
    const v7, 0x7f0e0010

    invoke-direct {p0, v7, v11}, Lorg/namelessrom/devicecontrol/MainActivity;->loadFragmentPrivate(IZ)V

    .line 232
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 234
    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/Utils;->startTaskerService()Z

    .line 236
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/MainActivity;->getFilesDir()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lorg/namelessrom/devicecontrol/DeviceConstants;->DC_DOWNGRADE:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 237
    .local v1, "downgradePath":Ljava/lang/String;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 238
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    move-result v7

    if-nez v7, :cond_1

    .line 239
    const-string v7, "Could not delete downgrade indicator file!"

    invoke-static {p0, v7}, Lorg/namelessrom/devicecontrol/Logger;->wtf(Ljava/lang/Object;Ljava/lang/String;)V

    .line 241
    :cond_1
    const v7, 0x7f0e00a9

    invoke-static {p0, v7, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    .line 244
    :cond_2
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v7

    iget-boolean v7, v7, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->dcFirstStart:Z

    if-eqz v7, :cond_3

    .line 245
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v7

    iput-boolean v11, v7, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->dcFirstStart:Z

    .line 246
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v7

    invoke-virtual {v7, p0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    .line 248
    :cond_3
    return-void

    .line 200
    .end local v1    # "downgradePath":Ljava/lang/String;
    .end local v2    # "mAdapter":Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;
    .end local v3    # "menuContainer":Landroid/widget/LinearLayout;
    .end local v4    # "menuList":Landroid/widget/ListView;
    .end local v6    # "v":Landroid/view/View;
    :cond_4
    invoke-virtual {v0, v12}, Landroid/widget/FrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 267
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/MainActivity;->restoreActionBar()V

    .line 268
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 5

    .prologue
    .line 354
    invoke-static {}, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->tearDown()V

    .line 355
    sget-object v2, Lorg/namelessrom/devicecontrol/MainActivity;->lockObject:Ljava/lang/Object;

    monitor-enter v2

    .line 356
    :try_start_0
    const-string v1, "closing shells"

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 358
    :try_start_1
    invoke-static {}, Lcom/stericson/roottools/RootTools;->closeAllShells()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 362
    :goto_0
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 363
    invoke-super {p0}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onDestroy()V

    .line 364
    return-void

    .line 359
    :catch_0
    move-exception v0

    .line 360
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v1, "onDestroy(): %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 362
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "i"    # I
    .param p4, "l"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 263
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/namelessrom/devicecontrol/MainActivity;->loadFragmentPrivate(IZ)V

    .line 264
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 272
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 273
    sget-object v0, Lorg/namelessrom/devicecontrol/MainActivity;->sMaterialMenu:Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;

    invoke-virtual {v0, p1}, Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;->syncState(Landroid/os/Bundle;)V

    .line 274
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 159
    invoke-super {p0}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onResume()V

    .line 160
    invoke-static {}, Lorg/namelessrom/proprietary/Configuration;->getPollfishApiKeyDc()Ljava/lang/String;

    move-result-object v0

    .line 161
    .local v0, "pfApiKey":Ljava/lang/String;
    const-string v1, "---"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v1

    iget-boolean v1, v1, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->showPollfish:Z

    if-eqz v1, :cond_0

    .line 162
    const-string v1, "PollFish.init()"

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    sget-object v1, Lcom/pollfish/constants/Position;->BOTTOM_RIGHT:Lcom/pollfish/constants/Position;

    const/16 v2, 0x1e

    invoke-static {p0, v0, v1, v2}, Lcom/pollfish/main/PollFish;->init(Landroid/app/Activity;Ljava/lang/String;Lcom/pollfish/constants/Position;I)V

    .line 165
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 277
    sget-object v0, Lorg/namelessrom/devicecontrol/MainActivity;->sMaterialMenu:Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;

    invoke-virtual {v0, p1}, Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 278
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 279
    return-void
.end method

.method public setFragment(Landroid/support/v4/app/Fragment;)V
    .locals 4
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 371
    if-nez p1, :cond_0

    .line 374
    :goto_0
    return-void

    .line 372
    :cond_0
    const-string v0, "setFragment: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 373
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/MainActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    goto :goto_0
.end method
