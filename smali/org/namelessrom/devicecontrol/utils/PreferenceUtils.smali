.class public Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;
.super Ljava/lang/Object;
.source "PreferenceUtils.java"


# static fields
.field private static final CONTENT_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final MAP_SUMMARY:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final MAP_TITLE:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const v4, 0x7f0e00b9

    const/4 v3, 0x1

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->CONTENT_MAP:Ljava/util/HashMap;

    .line 42
    sget-object v0, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->CONTENT_MAP:Ljava/util/HashMap;

    const-string v1, "enable"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->CONTENT_MAP:Ljava/util/HashMap;

    const-string v1, "enabled"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->CONTENT_MAP:Ljava/util/HashMap;

    const-string v1, "intelli_plug_active"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->CONTENT_MAP:Ljava/util/HashMap;

    const-string v1, "touch_boost_active"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->MAP_TITLE:Ljava/util/HashMap;

    .line 53
    sget-object v0, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->MAP_TITLE:Ljava/util/HashMap;

    const-string v1, "enable"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->MAP_TITLE:Ljava/util/HashMap;

    const-string v1, "enabled"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->MAP_TITLE:Ljava/util/HashMap;

    const-string v1, "intelli_plug_active"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->MAP_TITLE:Ljava/util/HashMap;

    const-string v1, "touch_boost_active"

    const v2, 0x7f0e0220

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->MAP_SUMMARY:Ljava/util/HashMap;

    .line 65
    return-void
.end method

.method public static addAwesomeEditTextPreference(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/preference/PreferenceCategory;Landroid/preference/Preference$OnPreferenceChangeListener;)Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "category"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "fileName"    # Ljava/lang/String;
    .param p5, "prefCat"    # Landroid/preference/PreferenceCategory;
    .param p6, "listener"    # Landroid/preference/Preference$OnPreferenceChangeListener;

    .prologue
    const/4 v3, 0x0

    .line 93
    new-instance v0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v1, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;-><init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 95
    .local v0, "preference":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;
    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;->isSupported()Z

    move-result v1

    if-nez v1, :cond_0

    .line 104
    :goto_0
    return-object v3

    .line 99
    :cond_0
    invoke-virtual {p5, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 100
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;->setKey(Ljava/lang/String;)V

    .line 101
    invoke-virtual {v0, p4}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 102
    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;->initValue()V

    .line 103
    invoke-virtual {v0, p6}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    move-object v3, v0

    .line 104
    goto :goto_0
.end method

.method public static addAwesomeTogglePreference(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/preference/PreferenceCategory;Landroid/preference/Preference$OnPreferenceChangeListener;)Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "summary"    # Ljava/lang/String;
    .param p3, "category"    # Ljava/lang/String;
    .param p4, "path"    # Ljava/lang/String;
    .param p5, "fileName"    # Ljava/lang/String;
    .param p6, "prefCat"    # Landroid/preference/PreferenceCategory;
    .param p7, "listener"    # Landroid/preference/Preference$OnPreferenceChangeListener;

    .prologue
    const/4 v3, 0x0

    .line 111
    new-instance v0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v1, p0

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;-><init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 113
    .local v0, "preference":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v1

    if-nez v1, :cond_0

    .line 125
    :goto_0
    return-object v3

    .line 117
    :cond_0
    invoke-virtual {p6, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 118
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setKey(Ljava/lang/String;)V

    .line 119
    invoke-virtual {v0, p5}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 120
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 121
    invoke-virtual {v0, p2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 123
    :cond_1
    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 124
    invoke-virtual {v0, p7}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    move-object v3, v0

    .line 125
    goto :goto_0
.end method

.method public static getTitle(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 82
    sget-object v0, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->MAP_TITLE:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public static getType(Ljava/lang/String;)I
    .locals 1
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 68
    sget-object v0, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->CONTENT_MAP:Ljava/util/HashMap;

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->getType(Ljava/lang/String;Ljava/util/HashMap;)I

    move-result v0

    return v0
.end method

.method public static getType(Ljava/lang/String;Ljava/util/HashMap;)I
    .locals 4
    .param p0, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    .local p1, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    .line 72
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    .line 78
    :goto_0
    return v2

    .line 74
    :cond_0
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 75
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_0

    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_2
    move v2, v3

    .line 78
    goto :goto_0
.end method
