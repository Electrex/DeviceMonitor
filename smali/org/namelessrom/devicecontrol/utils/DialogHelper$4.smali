.class final Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;
.super Ljava/lang/Object;
.source "DialogHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/utils/DialogHelper;->openSeekbarDialog(Landroid/app/Activity;ILjava/lang/String;IILandroid/preference/Preference;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$category:Ljava/lang/String;

.field final synthetic val$min:I

.field final synthetic val$path:Ljava/lang/String;

.field final synthetic val$pref:Landroid/preference/Preference;

.field final synthetic val$seekbar:Landroid/widget/SeekBar;

.field final synthetic val$settingText:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Landroid/widget/EditText;ILandroid/widget/SeekBar;Landroid/preference/Preference;Ljava/lang/String;Landroid/app/Activity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;->val$settingText:Landroid/widget/EditText;

    iput p2, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;->val$min:I

    iput-object p3, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;->val$seekbar:Landroid/widget/SeekBar;

    iput-object p4, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;->val$pref:Landroid/preference/Preference;

    iput-object p5, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;->val$path:Ljava/lang/String;

    iput-object p6, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;->val$activity:Landroid/app/Activity;

    iput-object p7, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;->val$category:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 126
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;->val$settingText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 139
    :goto_0
    return-void

    .line 127
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;->val$settingText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 128
    .local v6, "val":I
    iget v0, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;->val$min:I

    if-ge v6, v0, :cond_1

    .line 129
    iget v6, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;->val$min:I

    .line 131
    :cond_1
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;->val$seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v6}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 133
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;->val$seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 134
    .local v4, "newProgress":Ljava/lang/String;
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;->val$pref:Landroid/preference/Preference;

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;->val$path:Ljava/lang/String;

    invoke-static {v0, v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->writeValue(Ljava/lang/String;Ljava/lang/String;)Z

    .line 137
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;->val$activity:Landroid/app/Activity;

    new-instance v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;->val$category:Ljava/lang/String;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;->val$pref:Landroid/preference/Preference;

    invoke-virtual {v2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;->val$path:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/objects/BootupItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v7, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->setBootup(Landroid/content/Context;Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    goto :goto_0
.end method
