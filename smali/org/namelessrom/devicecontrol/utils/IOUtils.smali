.class public Lorg/namelessrom/devicecontrol/utils/IOUtils;
.super Ljava/lang/Object;
.source "IOUtils.java"


# static fields
.field public static final SDCARD:Ljava/lang/String;

.field private static sInstance:Lorg/namelessrom/devicecontrol/utils/IOUtils;

.field private static sSdcardsChecked:Z


# instance fields
.field private sPrimarySdcard:Ljava/lang/String;

.field private sSecondarySdcard:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/namelessrom/devicecontrol/utils/IOUtils;->SDCARD:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->readMounts()V

    .line 46
    return-void
.end method

.method private findFstab()Ljava/io/File;
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 157
    new-instance v1, Ljava/io/File;

    const-string v8, "/system/etc/vold.fstab"

    invoke-direct {v1, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 158
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_1

    move-object v7, v1

    .line 174
    :cond_0
    :goto_0
    return-object v7

    .line 162
    :cond_1
    const-string v8, "grep -ls \"/dev/block/\" * --include=fstab.* --exclude=fstab.goldfish"

    invoke-static {v8, v7}, Lorg/namelessrom/devicecontrol/utils/Utils;->getCommandResult(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 164
    .local v3, "fstab":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 165
    const-string v8, "\n"

    invoke-virtual {v3, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 166
    .local v2, "files":[Ljava/lang/String;
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_0

    aget-object v6, v0, v4

    .line 167
    .local v6, "s":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    .end local v1    # "file":Ljava/io/File;
    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 168
    .restart local v1    # "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_2

    move-object v7, v1

    .line 169
    goto :goto_0

    .line 166
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method public static get()Lorg/namelessrom/devicecontrol/utils/IOUtils;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lorg/namelessrom/devicecontrol/utils/IOUtils;->sInstance:Lorg/namelessrom/devicecontrol/utils/IOUtils;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Lorg/namelessrom/devicecontrol/utils/IOUtils;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/utils/IOUtils;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/utils/IOUtils;->sInstance:Lorg/namelessrom/devicecontrol/utils/IOUtils;

    .line 52
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/utils/IOUtils;->sInstance:Lorg/namelessrom/devicecontrol/utils/IOUtils;

    return-object v0
.end method

.method private readMounts()V
    .locals 21
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SdCardPath"
        }
    .end annotation

    .prologue
    .line 68
    sget-boolean v17, Lorg/namelessrom/devicecontrol/utils/IOUtils;->sSdcardsChecked:Z

    if-eqz v17, :cond_0

    .line 154
    :goto_0
    return-void

    .line 72
    :cond_0
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 73
    .local v12, "mounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 75
    .local v16, "vold":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v17, "cat /proc/mounts;\n"

    const-string v18, ""

    invoke-static/range {v17 .. v18}, Lorg/namelessrom/devicecontrol/utils/Utils;->getCommandResult(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    const-string v18, "\n"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 76
    .local v13, "output":[Ljava/lang/String;
    move-object v3, v13

    .local v3, "arr$":[Ljava/lang/String;
    array-length v9, v3

    .local v9, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_1
    if-ge v8, v9, :cond_3

    aget-object v15, v3, v8

    .line 77
    .local v15, "s":Ljava/lang/String;
    const-string v17, "/dev/block/vold/"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 78
    const-string v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 79
    .local v10, "lineElements":[Ljava/lang/String;
    const/16 v17, 0x1

    aget-object v17, v10, v17

    if-nez v17, :cond_2

    .line 76
    .end local v10    # "lineElements":[Ljava/lang/String;
    :cond_1
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 80
    .restart local v10    # "lineElements":[Ljava/lang/String;
    :cond_2
    const/16 v17, 0x1

    aget-object v4, v10, v17

    .line 81
    .local v4, "element":Ljava/lang/String;
    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 85
    .end local v4    # "element":Ljava/lang/String;
    .end local v10    # "lineElements":[Ljava/lang/String;
    .end local v15    # "s":Ljava/lang/String;
    :cond_3
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_6

    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->isExternalStorageAvailable()Z

    move-result v17

    if-eqz v17, :cond_6

    const/4 v2, 0x1

    .line 86
    .local v2, "addExternal":Z
    :goto_3
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v17

    if-nez v17, :cond_4

    if-eqz v2, :cond_4

    .line 87
    const-string v17, "/mnt/sdcard"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    :cond_4
    invoke-direct/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->findFstab()Ljava/io/File;

    move-result-object v5

    .line 91
    .local v5, "fstab":Ljava/io/File;
    if-eqz v5, :cond_b

    .line 92
    const-string v17, "cat %s;\n"

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    const-string v18, ""

    invoke-static/range {v17 .. v18}, Lorg/namelessrom/devicecontrol/utils/Utils;->getCommandResult(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    const-string v18, "\n"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 94
    move-object v3, v13

    array-length v9, v3

    const/4 v8, 0x0

    :goto_4
    if-ge v8, v9, :cond_b

    aget-object v15, v3, v8

    .line 95
    .restart local v15    # "s":Ljava/lang/String;
    const-string v17, "dev_mount"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 96
    const-string v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 97
    .restart local v10    # "lineElements":[Ljava/lang/String;
    const/16 v17, 0x2

    aget-object v17, v10, v17

    if-nez v17, :cond_7

    .line 94
    .end local v10    # "lineElements":[Ljava/lang/String;
    :cond_5
    :goto_5
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    .line 85
    .end local v2    # "addExternal":Z
    .end local v5    # "fstab":Ljava/io/File;
    .end local v15    # "s":Ljava/lang/String;
    :cond_6
    const/4 v2, 0x0

    goto :goto_3

    .line 98
    .restart local v2    # "addExternal":Z
    .restart local v5    # "fstab":Ljava/io/File;
    .restart local v10    # "lineElements":[Ljava/lang/String;
    .restart local v15    # "s":Ljava/lang/String;
    :cond_7
    const/16 v17, 0x2

    aget-object v4, v10, v17

    .line 100
    .restart local v4    # "element":Ljava/lang/String;
    const-string v17, ":"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 101
    const/16 v17, 0x0

    const-string v18, ":"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v4, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 104
    :cond_8
    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v17

    const-string v18, "usb"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_5

    .line 105
    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 107
    .end local v4    # "element":Ljava/lang/String;
    .end local v10    # "lineElements":[Ljava/lang/String;
    :cond_9
    const-string v17, "/devices/platform"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 108
    const-string v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 109
    .restart local v10    # "lineElements":[Ljava/lang/String;
    const/16 v17, 0x1

    aget-object v17, v10, v17

    if-eqz v17, :cond_5

    .line 110
    const/16 v17, 0x1

    aget-object v4, v10, v17

    .line 112
    .restart local v4    # "element":Ljava/lang/String;
    const-string v17, ":"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 113
    const/16 v17, 0x0

    const-string v18, ":"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v4, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 116
    :cond_a
    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v17

    const-string v18, "usb"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_5

    .line 117
    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 123
    .end local v4    # "element":Ljava/lang/String;
    .end local v10    # "lineElements":[Ljava/lang/String;
    .end local v15    # "s":Ljava/lang/String;
    :cond_b
    if-eqz v2, :cond_c

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_c

    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->isExternalStorageAvailable()Z

    move-result v17

    if-eqz v17, :cond_c

    .line 124
    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    :cond_c
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v17

    if-nez v17, :cond_d

    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->isExternalStorageAvailable()Z

    move-result v17

    if-eqz v17, :cond_d

    .line 128
    const-string v17, "/mnt/sdcard"

    invoke-virtual/range {v16 .. v17}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    :cond_d
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_6
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v6, v0, :cond_10

    .line 132
    invoke-virtual {v12, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 133
    .local v11, "mount":Ljava/lang/String;
    new-instance v14, Ljava/io/File;

    invoke-direct {v14, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 134
    .local v14, "root":Ljava/io/File;
    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_e

    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_e

    invoke-virtual {v14}, Ljava/io/File;->isDirectory()Z

    move-result v17

    if-eqz v17, :cond_e

    invoke-virtual {v14}, Ljava/io/File;->canWrite()Z

    move-result v17

    if-nez v17, :cond_f

    .line 136
    :cond_e
    add-int/lit8 v7, v6, -0x1

    .end local v6    # "i":I
    .local v7, "i":I
    invoke-virtual {v12, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v6, v7

    .line 131
    .end local v7    # "i":I
    .restart local v6    # "i":I
    :cond_f
    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    .line 140
    .end local v11    # "mount":Ljava/lang/String;
    .end local v14    # "root":Ljava/io/File;
    :cond_10
    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_7
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_12

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 141
    .restart local v11    # "mount":Ljava/lang/String;
    const-string v17, "sdcard0"

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_11

    const-string v17, "/mnt/sdcard"

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_11

    const-string v17, "/sdcard"

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_11

    .line 143
    move-object/from16 v0, p0

    iput-object v11, v0, Lorg/namelessrom/devicecontrol/utils/IOUtils;->sSecondarySdcard:Ljava/lang/String;

    goto :goto_7

    .line 145
    :cond_11
    move-object/from16 v0, p0

    iput-object v11, v0, Lorg/namelessrom/devicecontrol/utils/IOUtils;->sPrimarySdcard:Ljava/lang/String;

    goto :goto_7

    .line 149
    .end local v11    # "mount":Ljava/lang/String;
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/utils/IOUtils;->sPrimarySdcard:Ljava/lang/String;

    move-object/from16 v17, v0

    if-nez v17, :cond_13

    .line 150
    const-string v17, "/sdcard"

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/namelessrom/devicecontrol/utils/IOUtils;->sPrimarySdcard:Ljava/lang/String;

    .line 153
    :cond_13
    const/16 v17, 0x1

    sput-boolean v17, Lorg/namelessrom/devicecontrol/utils/IOUtils;->sSdcardsChecked:Z

    goto/16 :goto_0
.end method


# virtual methods
.method public folderExists(Ljava/lang/String;)Z
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 199
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 200
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPrimarySdCard()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/IOUtils;->sPrimarySdcard:Ljava/lang/String;

    return-object v0
.end method

.method public getSecondarySdCard()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/IOUtils;->sSecondarySdcard:Ljava/lang/String;

    return-object v0
.end method

.method public getSpaceLeft()D
    .locals 8

    .prologue
    .line 178
    new-instance v2, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 180
    .local v2, "stat":Landroid/os/StatFs;
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x12

    if-lt v3, v4, :cond_0

    .line 181
    invoke-virtual {v2}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v4

    long-to-double v4, v4

    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v6

    long-to-double v6, v6

    mul-double v0, v4, v6

    .line 187
    .local v0, "sdAvailSize":D
    :goto_0
    const-wide/high16 v4, 0x41d0000000000000L    # 1.073741824E9

    div-double v4, v0, v4

    return-wide v4

    .line 184
    .end local v0    # "sdAvailSize":D
    :cond_0
    invoke-virtual {v2}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v3

    int-to-double v4, v3

    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I

    move-result v3

    int-to-double v6, v3

    mul-double v0, v4, v6

    .restart local v0    # "sdAvailSize":D
    goto :goto_0
.end method

.method public hasAndroidSecure()Z
    .locals 2

    .prologue
    .line 191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lorg/namelessrom/devicecontrol/utils/IOUtils;->SDCARD:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/.android-secure"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->folderExists(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasSdExt()Z
    .locals 1

    .prologue
    .line 195
    const-string v0, "/sd-ext"

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->folderExists(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isExternalStorageAvailable()Z
    .locals 2

    .prologue
    .line 56
    const-string v0, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
