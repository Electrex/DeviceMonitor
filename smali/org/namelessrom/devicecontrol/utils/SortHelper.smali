.class public Lorg/namelessrom/devicecontrol/utils/SortHelper;
.super Ljava/lang/Object;
.source "SortHelper.java"


# static fields
.field public static final sAppComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/namelessrom/devicecontrol/objects/AppItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final sFileComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lorg/namelessrom/devicecontrol/utils/SortHelper$1;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/utils/SortHelper$1;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/utils/SortHelper;->sAppComparator:Ljava/util/Comparator;

    .line 36
    new-instance v0, Lorg/namelessrom/devicecontrol/utils/SortHelper$2;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/utils/SortHelper$2;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/utils/SortHelper;->sFileComparator:Ljava/util/Comparator;

    return-void
.end method
