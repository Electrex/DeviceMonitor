.class public Lorg/namelessrom/devicecontrol/utils/DialogHelper;
.super Ljava/lang/Object;
.source "DialogHelper.java"


# direct methods
.method public static openSeekbarDialog(Landroid/app/Activity;ILjava/lang/String;IILandroid/preference/Preference;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "currentProgress"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "min"    # I
    .param p4, "max"    # I
    .param p5, "pref"    # Landroid/preference/Preference;
    .param p6, "path"    # Ljava/lang/String;
    .param p7, "category"    # Ljava/lang/String;

    .prologue
    .line 44
    if-nez p0, :cond_0

    .line 141
    :goto_0
    return-void

    .line 46
    :cond_0
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    const/high16 v3, 0x1040000

    invoke-virtual {v1, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 47
    .local v10, "cancel":Ljava/lang/String;
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    const v3, 0x104000a

    invoke-virtual {v1, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 48
    .local v11, "ok":Ljava/lang/String;
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v3, 0x7f040029

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 51
    .local v9, "alphaDialog":Landroid/view/View;
    const v1, 0x7f0c008f

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/SeekBar;

    .line 53
    .local v4, "seekbar":Landroid/widget/SeekBar;
    move/from16 v0, p4

    invoke-virtual {v4, v0}, Landroid/widget/SeekBar;->setMax(I)V

    .line 54
    invoke-virtual {v4, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 56
    const v1, 0x7f0c0090

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 57
    .local v2, "settingText":Landroid/widget/EditText;
    new-instance v1, Lorg/namelessrom/devicecontrol/utils/DialogHelper$1;

    invoke-direct {v1, v2, v4}, Lorg/namelessrom/devicecontrol/utils/DialogHelper$1;-><init>(Landroid/widget/EditText;Landroid/widget/SeekBar;)V

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 73
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 74
    new-instance v1, Lorg/namelessrom/devicecontrol/utils/DialogHelper$2;

    move/from16 v0, p4

    invoke-direct {v1, v0, v4}, Lorg/namelessrom/devicecontrol/utils/DialogHelper$2;-><init>(ILandroid/widget/SeekBar;)V

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 96
    new-instance v1, Lorg/namelessrom/devicecontrol/utils/DialogHelper$3;

    invoke-direct {v1, v2}, Lorg/namelessrom/devicecontrol/utils/DialogHelper$3;-><init>(Landroid/widget/EditText;)V

    invoke-virtual {v4, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 112
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v3, Lorg/namelessrom/devicecontrol/utils/DialogHelper$5;

    invoke-direct {v3}, Lorg/namelessrom/devicecontrol/utils/DialogHelper$5;-><init>()V

    invoke-virtual {v1, v10, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v12

    new-instance v1, Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;

    move/from16 v3, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object v7, p0

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lorg/namelessrom/devicecontrol/utils/DialogHelper$4;-><init>(Landroid/widget/EditText;ILandroid/widget/SeekBar;Landroid/preference/Preference;Ljava/lang/String;Landroid/app/Activity;Ljava/lang/String;)V

    invoke-virtual {v12, v11, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0
.end method
