.class public Lorg/namelessrom/devicecontrol/utils/HtmlHelper;
.super Ljava/lang/Object;
.source "HtmlHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static cleanupPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    const-string p0, ""

    .line 68
    .end local p0    # "path":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 62
    .restart local p0    # "path":Ljava/lang/String;
    :cond_1
    const-string v0, "./"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 63
    const-string v0, "./"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 65
    :cond_2
    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    const-string v0, "/"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static loadPath(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 1
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 72
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/utils/HtmlHelper;->cleanupPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 73
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/utils/HtmlHelper;->loadPathInternal(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public static loadPathAsString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 77
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/utils/HtmlHelper;->cleanupPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 78
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/utils/HtmlHelper;->loadPathAsStringInternal(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static loadPathAsStringInternal(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 92
    :try_start_0
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/utils/Utils;->loadFromAssets(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 96
    :goto_0
    return-object v1

    .line 93
    :catch_0
    move-exception v0

    .line 94
    .local v0, "exc":Ljava/lang/Exception;
    const-class v1, Lorg/namelessrom/devicecontrol/utils/HtmlHelper;

    const-string v2, "loadPath"

    invoke-static {v1, v2, v0}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 96
    const-string v1, ""

    goto :goto_0
.end method

.method private static loadPathInternal(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 83
    :try_start_0
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/Application;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 87
    :goto_0
    return-object v1

    .line 84
    :catch_0
    move-exception v0

    .line 85
    .local v0, "exc":Ljava/lang/Exception;
    const-class v1, Lorg/namelessrom/devicecontrol/utils/HtmlHelper;

    const-string v2, "loadPath"

    invoke-static {v1, v2, v0}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 87
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static urlDecode(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 39
    :try_start_0
    const-string v2, "UTF-8"

    invoke-static {p0, v2}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 44
    .local v0, "decoded":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 40
    .end local v0    # "decoded":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 42
    .local v1, "ignored":Ljava/lang/Exception;
    invoke-static {p0}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "decoded":Ljava/lang/String;
    goto :goto_0
.end method
