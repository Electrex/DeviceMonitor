.class final Lorg/namelessrom/devicecontrol/utils/Utils$1;
.super Lcom/stericson/roottools/execution/CommandCapture;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/utils/Utils;->getCommandResult(Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;ILjava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$ID:I

.field final synthetic val$NEWLINE:Z

.field final synthetic val$listener:Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;

.field final synthetic val$sb:Ljava/lang/StringBuilder;


# direct methods
.method varargs constructor <init>(I[Ljava/lang/String;Ljava/lang/StringBuilder;ZLorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;I)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # [Ljava/lang/String;

    .prologue
    .line 406
    iput-object p3, p0, Lorg/namelessrom/devicecontrol/utils/Utils$1;->val$sb:Ljava/lang/StringBuilder;

    iput-boolean p4, p0, Lorg/namelessrom/devicecontrol/utils/Utils$1;->val$NEWLINE:Z

    iput-object p5, p0, Lorg/namelessrom/devicecontrol/utils/Utils$1;->val$listener:Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;

    iput p6, p0, Lorg/namelessrom/devicecontrol/utils/Utils$1;->val$ID:I

    invoke-direct {p0, p1, p2}, Lcom/stericson/roottools/execution/CommandCapture;-><init>(I[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public commandCompleted(II)V
    .locals 5
    .param p1, "id"    # I
    .param p2, "exitcode"    # I

    .prologue
    .line 416
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/utils/Utils$1;->val$sb:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 417
    .local v0, "result":Ljava/lang/String;
    const-class v1, Lorg/namelessrom/devicecontrol/utils/Utils;

    const-string v2, "Shell output: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 419
    sget-object v1, Lorg/namelessrom/devicecontrol/Application;->HANDLER:Landroid/os/Handler;

    new-instance v2, Lorg/namelessrom/devicecontrol/utils/Utils$1$1;

    invoke-direct {v2, p0}, Lorg/namelessrom/devicecontrol/utils/Utils$1$1;-><init>(Lorg/namelessrom/devicecontrol/utils/Utils$1;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 424
    return-void
.end method

.method public commandOutput(ILjava/lang/String;)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "line"    # Ljava/lang/String;

    .prologue
    .line 408
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/Utils$1;->val$sb:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 409
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/utils/Utils$1;->val$NEWLINE:Z

    if-eqz v0, :cond_0

    .line 410
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/Utils$1;->val$sb:Ljava/lang/StringBuilder;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 412
    :cond_0
    return-void
.end method
