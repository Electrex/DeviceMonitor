.class final Lorg/namelessrom/devicecontrol/utils/DialogHelper$2;
.super Ljava/lang/Object;
.source "DialogHelper.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/utils/DialogHelper;->openSeekbarDialog(Landroid/app/Activity;ILjava/lang/String;IILandroid/preference/Preference;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$max:I

.field final synthetic val$seekbar:Landroid/widget/SeekBar;


# direct methods
.method constructor <init>(ILandroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 74
    iput p1, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$2;->val$max:I

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$2;->val$seekbar:Landroid/widget/SeekBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 86
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 87
    .local v0, "val":I
    iget v1, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$2;->val$max:I

    if-le v0, v1, :cond_0

    .line 88
    const/4 v1, 0x0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    iget v3, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$2;->val$max:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 89
    iget v0, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$2;->val$max:I

    .line 91
    :cond_0
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$2;->val$seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    .end local v0    # "val":I
    :goto_0
    return-void

    .line 92
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 81
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 77
    return-void
.end method
