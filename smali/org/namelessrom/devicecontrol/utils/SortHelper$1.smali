.class final Lorg/namelessrom/devicecontrol/utils/SortHelper$1;
.super Ljava/lang/Object;
.source "SortHelper.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/utils/SortHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/namelessrom/devicecontrol/objects/AppItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final collator:Ljava/text/Collator;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/utils/SortHelper$1;->collator:Ljava/text/Collator;

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 28
    check-cast p1, Lorg/namelessrom/devicecontrol/objects/AppItem;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/namelessrom/devicecontrol/objects/AppItem;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/namelessrom/devicecontrol/utils/SortHelper$1;->compare(Lorg/namelessrom/devicecontrol/objects/AppItem;Lorg/namelessrom/devicecontrol/objects/AppItem;)I

    move-result v0

    return v0
.end method

.method public final compare(Lorg/namelessrom/devicecontrol/objects/AppItem;Lorg/namelessrom/devicecontrol/objects/AppItem;)I
    .locals 3
    .param p1, "a"    # Lorg/namelessrom/devicecontrol/objects/AppItem;
    .param p2, "b"    # Lorg/namelessrom/devicecontrol/objects/AppItem;

    .prologue
    .line 30
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/SortHelper$1;->collator:Ljava/text/Collator;

    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method
