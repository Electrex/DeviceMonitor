.class final Lorg/namelessrom/devicecontrol/utils/DialogHelper$1;
.super Ljava/lang/Object;
.source "DialogHelper.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/utils/DialogHelper;->openSeekbarDialog(Landroid/app/Activity;ILjava/lang/String;IILandroid/preference/Preference;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$seekbar:Landroid/widget/SeekBar;

.field final synthetic val$settingText:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Landroid/widget/EditText;Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$1;->val$settingText:Landroid/widget/EditText;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$1;->val$seekbar:Landroid/widget/SeekBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 60
    const/4 v2, 0x6

    if-ne p2, v2, :cond_1

    .line 61
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$1;->val$settingText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 62
    .local v0, "text":Landroid/text/Editable;
    if-eqz v0, :cond_0

    .line 64
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 65
    .local v1, "val":I
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/utils/DialogHelper$1;->val$seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v1}, Landroid/widget/SeekBar;->setProgress(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    .end local v1    # "val":I
    :cond_0
    :goto_0
    const/4 v2, 0x1

    .line 70
    .end local v0    # "text":Landroid/text/Editable;
    :goto_1
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 66
    .restart local v0    # "text":Landroid/text/Editable;
    :catch_0
    move-exception v2

    goto :goto_0
.end method
