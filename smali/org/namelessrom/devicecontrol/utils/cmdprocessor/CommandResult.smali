.class public Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;
.super Ljava/lang/Object;
.source "CommandResult.java"

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field private final mEndTime:J

.field private final mExitValue:I

.field private final mStartTime:J

.field private final mStderr:Ljava/lang/String;

.field private final mStdout:Ljava/lang/String;


# direct methods
.method public constructor <init>(JILjava/lang/String;Ljava/lang/String;J)V
    .locals 6
    .param p1, "startTime"    # J
    .param p3, "exitValue"    # I
    .param p4, "stdout"    # Ljava/lang/String;
    .param p5, "stderr"    # Ljava/lang/String;
    .param p6, "endTime"    # J

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-wide p1, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStartTime:J

    .line 19
    iput p3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mExitValue:I

    .line 20
    iput-object p4, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStdout:Ljava/lang/String;

    .line 21
    iput-object p5, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStderr:Ljava/lang/String;

    .line 22
    iput-wide p6, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mEndTime:J

    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Time to execute: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mEndTime:J

    iget-wide v4, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStartTime:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ns (nanoseconds)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 86
    if-ne p0, p1, :cond_1

    .line 91
    :cond_0
    :goto_0
    return v1

    .line 87
    :cond_1
    instance-of v3, p1, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 89
    check-cast v0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;

    .line 91
    .local v0, "that":Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;
    iget-wide v4, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStartTime:J

    iget-wide v6, v0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStartTime:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget v3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mExitValue:I

    iget v4, v0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mExitValue:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStdout:Ljava/lang/String;

    iget-object v4, v0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStdout:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStderr:Ljava/lang/String;

    iget-object v4, v0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStderr:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-wide v4, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mEndTime:J

    iget-wide v6, v0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mEndTime:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getStdout()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStdout:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/16 v8, 0x20

    const/4 v2, 0x0

    .line 99
    const/4 v0, 0x0

    .line 100
    .local v0, "result":I
    iget-wide v4, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStartTime:J

    iget-wide v6, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStartTime:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v1, v4

    add-int/lit8 v0, v1, 0x0

    .line 101
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mExitValue:I

    add-int v0, v1, v3

    .line 102
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStdout:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStdout:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_0
    add-int v0, v3, v1

    .line 103
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStderr:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStderr:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_0
    add-int v0, v1, v2

    .line 104
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mEndTime:J

    iget-wide v4, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mEndTime:J

    ushr-long/2addr v4, v8

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 105
    return v0

    :cond_1
    move v1, v2

    .line 102
    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x27

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CommandResult{, mStartTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStartTime:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mExitValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mExitValue:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", stdout=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStdout:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", stderr=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStderr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mEndTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mEndTime:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 68
    iget-wide v0, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStartTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 69
    iget v0, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mExitValue:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStdout:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mStderr:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 72
    iget-wide v0, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->mEndTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 73
    return-void
.end method
