.class public Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$sh;
.super Ljava/lang/Object;
.source "CMDProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "sh"
.end annotation


# instance fields
.field private SHELL:Ljava/lang/String;

.field final synthetic this$0:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor;


# direct methods
.method public constructor <init>(Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor;Ljava/lang/String;)V
    .locals 1
    .param p2, "SHELL_in"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$sh;->this$0:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    const-string v0, "sh"

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$sh;->SHELL:Ljava/lang/String;

    .line 74
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$sh;->SHELL:Ljava/lang/String;

    .line 75
    return-void
.end method

.method private getStreamLines(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 7
    .param p1, "is"    # Ljava/io/InputStream;

    .prologue
    .line 78
    const/4 v4, 0x0

    .line 79
    .local v4, "out":Ljava/lang/String;
    const/4 v0, 0x0

    .line 80
    .local v0, "buffer":Ljava/lang/StringBuffer;
    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 83
    .local v2, "dis":Ljava/io/DataInputStream;
    :try_start_0
    invoke-virtual {v2}, Ljava/io/DataInputStream;->available()I

    move-result v5

    if-lez v5, :cond_2

    .line 85
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readLine()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    .end local v0    # "buffer":Ljava/lang/StringBuffer;
    .local v1, "buffer":Ljava/lang/StringBuffer;
    :goto_0
    :try_start_1
    invoke-virtual {v2}, Ljava/io/DataInputStream;->available()I

    move-result v5

    if-lez v5, :cond_1

    .line 88
    const/16 v5, 0xa

    invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readLine()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 92
    :catch_0
    move-exception v3

    move-object v0, v1

    .line 93
    .end local v1    # "buffer":Ljava/lang/StringBuffer;
    .restart local v0    # "buffer":Ljava/lang/StringBuffer;
    .local v3, "ex":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    const-string v5, "aCMDProcessor"

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 96
    :try_start_3
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 99
    .end local v3    # "ex":Ljava/lang/Exception;
    :goto_2
    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    .line 102
    :cond_0
    return-object v4

    .end local v0    # "buffer":Ljava/lang/StringBuffer;
    .restart local v1    # "buffer":Ljava/lang/StringBuffer;
    :cond_1
    move-object v0, v1

    .line 91
    .end local v1    # "buffer":Ljava/lang/StringBuffer;
    .restart local v0    # "buffer":Ljava/lang/StringBuffer;
    :cond_2
    :try_start_4
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 96
    :try_start_5
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2

    .line 97
    :catch_1
    move-exception v5

    goto :goto_2

    .line 95
    :catchall_0
    move-exception v5

    .line 96
    :goto_3
    :try_start_6
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    .line 97
    :goto_4
    throw v5

    .restart local v3    # "ex":Ljava/lang/Exception;
    :catch_2
    move-exception v5

    goto :goto_2

    .end local v3    # "ex":Ljava/lang/Exception;
    :catch_3
    move-exception v6

    goto :goto_4

    .line 95
    .end local v0    # "buffer":Ljava/lang/StringBuffer;
    .restart local v1    # "buffer":Ljava/lang/StringBuffer;
    :catchall_1
    move-exception v5

    move-object v0, v1

    .end local v1    # "buffer":Ljava/lang/StringBuffer;
    .restart local v0    # "buffer":Ljava/lang/StringBuffer;
    goto :goto_3

    .line 92
    :catch_4
    move-exception v3

    goto :goto_1
.end method


# virtual methods
.method public run(Ljava/lang/String;)Ljava/lang/Process;
    .locals 6
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 108
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v3

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$sh;->SHELL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v1

    .line 109
    .local v1, "process":Ljava/lang/Process;
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-virtual {v1}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 110
    .local v2, "toProcess":Ljava/io/DataOutputStream;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exec "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 111
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    .end local v2    # "toProcess":Ljava/io/DataOutputStream;
    :goto_0
    return-object v1

    .line 112
    .end local v1    # "process":Ljava/lang/Process;
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "aCMDProcessor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception while trying to run: \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    const/4 v1, 0x0

    .restart local v1    # "process":Ljava/lang/Process;
    goto :goto_0
.end method

.method public runWaitFor(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$CommandResult2;
    .locals 8
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 120
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$sh;->run(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v2

    .line 121
    .local v2, "process":Ljava/lang/Process;
    const/4 v1, 0x0

    .line 122
    .local v1, "exit_value":Ljava/lang/Integer;
    const/4 v4, 0x0

    .line 123
    .local v4, "stdout":Ljava/lang/String;
    const/4 v3, 0x0

    .line 124
    .local v3, "stderr":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 126
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Process;->waitFor()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 128
    invoke-virtual {v2}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {p0, v5}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$sh;->getStreamLines(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v4

    .line 129
    invoke-virtual {v2}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {p0, v5}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$sh;->getStreamLines(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 134
    :cond_0
    :goto_0
    new-instance v5, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$CommandResult2;

    iget-object v6, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$sh;->this$0:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor;

    invoke-direct {v5, v6, v1, v4, v3}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$CommandResult2;-><init>(Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V

    return-object v5

    .line 130
    :catch_0
    move-exception v0

    .line 131
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "aCMDProcessor"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "runWaitFor "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method
