.class public Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;
.super Ljava/lang/Object;
.source "ChildProcess.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;,
        Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;
    }
.end annotation


# instance fields
.field private mChildProc:Ljava/lang/Process;

.field private mChildStderr:Ljava/lang/StringBuilder;

.field private mChildStderrReader:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;

.field private mChildStdinWriter:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;

.field private mChildStdout:Ljava/lang/StringBuilder;

.field private mChildStdoutReader:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;

.field private mEndTime:J

.field private mExitValue:I

.field private final mStartTime:J


# direct methods
.method public constructor <init>([Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "cmdarray"    # [Ljava/lang/String;
    .param p2, "childStdin"    # Ljava/lang/String;

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mStartTime:J

    .line 88
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v1

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildProc:Ljava/lang/Process;

    .line 89
    if-eqz p2, :cond_0

    .line 90
    new-instance v1, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildProc:Ljava/lang/Process;

    invoke-virtual {v2}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-direct {v1, p0, v2, p2}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;-><init>(Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;Ljava/io/OutputStream;Ljava/lang/String;)V

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStdinWriter:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;

    .line 91
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStdinWriter:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;->start()V

    .line 93
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStdout:Ljava/lang/StringBuilder;

    .line 94
    new-instance v1, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildProc:Ljava/lang/Process;

    invoke-virtual {v2}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStdout:Ljava/lang/StringBuilder;

    invoke-direct {v1, p0, v2, v3}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;-><init>(Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;Ljava/io/InputStream;Ljava/lang/StringBuilder;)V

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStdoutReader:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;

    .line 95
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStdoutReader:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;->start()V

    .line 96
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStderr:Ljava/lang/StringBuilder;

    .line 97
    new-instance v1, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildProc:Ljava/lang/Process;

    invoke-virtual {v2}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v2

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStderr:Ljava/lang/StringBuilder;

    invoke-direct {v1, p0, v2, v3}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;-><init>(Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;Ljava/io/InputStream;Ljava/lang/StringBuilder;)V

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStderrReader:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;

    .line 98
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStderrReader:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;->start()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    :goto_0
    return-void

    .line 99
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getResult()Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;
    .locals 8

    .prologue
    .line 149
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 150
    new-instance v0, Ljava/lang/IllegalThreadStateException;

    const-string v1, "Child process running"

    invoke-direct {v0, v1}, Ljava/lang/IllegalThreadStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 153
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStdout:Ljava/lang/StringBuilder;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStdout:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 154
    .local v4, "childStdOut":Ljava/lang/String;
    :goto_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStderr:Ljava/lang/StringBuilder;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStderr:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 156
    .local v5, "childStdErr":Ljava/lang/String;
    :goto_1
    new-instance v0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;

    iget-wide v1, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mStartTime:J

    iget v3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mExitValue:I

    iget-wide v6, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mEndTime:J

    invoke-direct/range {v0 .. v7}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;-><init>(JILjava/lang/String;Ljava/lang/String;J)V

    return-object v0

    .line 153
    .end local v4    # "childStdOut":Ljava/lang/String;
    .end local v5    # "childStdErr":Ljava/lang/String;
    :cond_1
    const-string v4, ""

    goto :goto_0

    .line 154
    .restart local v4    # "childStdOut":Ljava/lang/String;
    :cond_2
    const-string v5, ""

    goto :goto_1
.end method

.method public isFinished()Z
    .locals 3

    .prologue
    .line 105
    const/4 v1, 0x1

    .line 106
    .local v1, "finished":Z
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildProc:Ljava/lang/Process;

    if-eqz v2, :cond_0

    .line 108
    :try_start_0
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildProc:Ljava/lang/Process;

    invoke-virtual {v2}, Ljava/lang/Process;->exitValue()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :cond_0
    :goto_0
    return v1

    .line 109
    :catch_0
    move-exception v0

    .line 110
    .local v0, "e":Ljava/lang/IllegalStateException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public waitFinished()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 117
    :goto_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildProc:Ljava/lang/Process;

    if-eqz v0, :cond_1

    .line 119
    :try_start_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildProc:Ljava/lang/Process;

    invoke-virtual {v0}, Ljava/lang/Process;->waitFor()I

    move-result v0

    iput v0, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mExitValue:I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_3

    .line 121
    :goto_1
    iput-object v2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildProc:Ljava/lang/Process;

    .line 124
    :try_start_1
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStderrReader:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 126
    iput-object v2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStderrReader:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;

    .line 130
    :goto_2
    :try_start_2
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStdoutReader:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;->join()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 132
    iput-object v2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStdoutReader:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;

    .line 135
    :goto_3
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStdinWriter:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;

    if-eqz v0, :cond_0

    .line 137
    :try_start_3
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStdinWriter:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;->join()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 139
    iput-object v2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStdinWriter:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;

    .line 143
    :cond_0
    :goto_4
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mEndTime:J

    goto :goto_0

    .line 125
    :catch_0
    move-exception v0

    .line 126
    iput-object v2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStderrReader:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;

    goto :goto_2

    :catchall_0
    move-exception v0

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStderrReader:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;

    throw v0

    .line 131
    :catch_1
    move-exception v0

    .line 132
    iput-object v2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStdoutReader:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;

    goto :goto_3

    :catchall_1
    move-exception v0

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStdoutReader:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;

    throw v0

    .line 138
    :catch_2
    move-exception v0

    .line 139
    iput-object v2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStdinWriter:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;

    goto :goto_4

    :catchall_2
    move-exception v0

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mChildStdinWriter:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;

    throw v0

    .line 145
    :cond_1
    iget v0, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->mExitValue:I

    return v0

    .line 120
    :catch_3
    move-exception v0

    goto :goto_1
.end method
