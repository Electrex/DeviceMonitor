.class Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;
.super Ljava/lang/Thread;
.source "ChildProcess.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChildWriter"
.end annotation


# instance fields
.field final mBuffer:Ljava/lang/String;

.field final mStream:Ljava/io/OutputStream;

.field final synthetic this$0:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;Ljava/io/OutputStream;Ljava/lang/String;)V
    .locals 0
    .param p2, "os"    # Ljava/io/OutputStream;
    .param p3, "buf"    # Ljava/lang/String;

    .prologue
    .line 48
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;->this$0:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 49
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;->mStream:Ljava/io/OutputStream;

    .line 50
    iput-object p3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;->mBuffer:Ljava/lang/String;

    .line 51
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 54
    const/4 v2, 0x0

    .line 55
    .local v2, "off":I
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;->mBuffer:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 57
    .local v0, "buf":[B
    :goto_0
    :try_start_0
    array-length v3, v0

    if-ge v2, v3, :cond_0

    .line 58
    const/16 v3, 0x400

    array-length v4, v0

    sub-int/2addr v4, v2

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 59
    .local v1, "len":I
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;->mStream:Ljava/io/OutputStream;

    invoke-virtual {v3, v0, v2, v1}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60
    add-int/2addr v2, v1

    .line 61
    goto :goto_0

    .line 66
    .end local v1    # "len":I
    :cond_0
    :try_start_1
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;->mStream:Ljava/io/OutputStream;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;->mStream:Ljava/io/OutputStream;

    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 71
    :cond_1
    :goto_1
    return-void

    .line 62
    :catch_0
    move-exception v3

    .line 66
    :try_start_2
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;->mStream:Ljava/io/OutputStream;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;->mStream:Ljava/io/OutputStream;

    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 67
    :catch_1
    move-exception v3

    goto :goto_1

    .line 65
    :catchall_0
    move-exception v3

    .line 66
    :try_start_3
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;->mStream:Ljava/io/OutputStream;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildWriter;->mStream:Ljava/io/OutputStream;

    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 69
    :cond_2
    :goto_2
    throw v3

    .line 67
    :catch_2
    move-exception v4

    goto :goto_2

    :catch_3
    move-exception v3

    goto :goto_1
.end method
