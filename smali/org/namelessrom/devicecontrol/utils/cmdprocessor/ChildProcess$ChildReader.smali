.class Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;
.super Ljava/lang/Thread;
.source "ChildProcess.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChildReader"
.end annotation


# instance fields
.field final mBuffer:Ljava/lang/StringBuilder;

.field final mStream:Ljava/io/InputStream;

.field final synthetic this$0:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;Ljava/io/InputStream;Ljava/lang/StringBuilder;)V
    .locals 0
    .param p2, "is"    # Ljava/io/InputStream;
    .param p3, "buf"    # Ljava/lang/StringBuilder;

    .prologue
    .line 18
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;->this$0:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 19
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;->mStream:Ljava/io/InputStream;

    .line 20
    iput-object p3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;->mBuffer:Ljava/lang/StringBuilder;

    .line 21
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 24
    const/16 v3, 0x400

    new-array v0, v3, [B

    .line 28
    .local v0, "buf":[B
    :goto_0
    :try_start_0
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;->mStream:Ljava/io/InputStream;

    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "len":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_1

    .line 29
    new-instance v2, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3, v1}, Ljava/lang/String;-><init>([BII)V

    .line 30
    .local v2, "s":Ljava/lang/String;
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;->mBuffer:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 32
    .end local v1    # "len":I
    .end local v2    # "s":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 36
    :try_start_1
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;->mStream:Ljava/io/InputStream;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;->mStream:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 41
    :cond_0
    :goto_1
    return-void

    .line 36
    .restart local v1    # "len":I
    :cond_1
    :try_start_2
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;->mStream:Ljava/io/InputStream;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;->mStream:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 37
    :catch_1
    move-exception v3

    goto :goto_1

    .line 35
    .end local v1    # "len":I
    :catchall_0
    move-exception v3

    .line 36
    :try_start_3
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;->mStream:Ljava/io/InputStream;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess$ChildReader;->mStream:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 39
    :cond_2
    :goto_2
    throw v3

    .line 37
    :catch_2
    move-exception v4

    goto :goto_2

    :catch_3
    move-exception v3

    goto :goto_1
.end method
