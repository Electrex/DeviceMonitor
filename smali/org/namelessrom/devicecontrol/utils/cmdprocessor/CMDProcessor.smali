.class public final Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor;
.super Ljava/lang/Object;
.source "CMDProcessor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$sh;,
        Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$CommandResult2;
    }
.end annotation


# instance fields
.field public final su:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$sh;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$sh;

    const-string v1, "su"

    invoke-direct {v0, p0, v1}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$sh;-><init>(Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor;->su:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$sh;

    .line 18
    return-void
.end method

.method public static runShellCommand(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;
    .locals 2
    .param p0, "cmd"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor;->startShellCommand(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;

    move-result-object v0

    .line 35
    .local v0, "proc":Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;
    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->waitFinished()I

    .line 36
    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->getResult()Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;

    move-result-object v1

    return-object v1
.end method

.method public static runSuCommand(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;
    .locals 2
    .param p0, "cmd"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor;->startSuCommand(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;

    move-result-object v0

    .line 49
    .local v0, "proc":Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;
    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->waitFinished()I

    .line 50
    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;->getResult()Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;

    move-result-object v1

    return-object v1
.end method

.method public static startShellCommand(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;
    .locals 3
    .param p0, "cmd"    # Ljava/lang/String;

    .prologue
    .line 26
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/String;

    .line 27
    .local v0, "cmdarray":[Ljava/lang/String;
    const/4 v1, 0x0

    const-string v2, "sh"

    aput-object v2, v0, v1

    .line 28
    const/4 v1, 0x1

    const-string v2, "-c"

    aput-object v2, v0, v1

    .line 29
    const/4 v1, 0x2

    aput-object p0, v0, v1

    .line 30
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor;->startSysCmd([Ljava/lang/String;Ljava/lang/String;)Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;

    move-result-object v1

    return-object v1
.end method

.method public static startSuCommand(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;
    .locals 3
    .param p0, "cmd"    # Ljava/lang/String;

    .prologue
    .line 40
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/String;

    .line 41
    .local v0, "cmdarray":[Ljava/lang/String;
    const/4 v1, 0x0

    const-string v2, "su"

    aput-object v2, v0, v1

    .line 42
    const/4 v1, 0x1

    const-string v2, "-c"

    aput-object v2, v0, v1

    .line 43
    const/4 v1, 0x2

    aput-object p0, v0, v1

    .line 44
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor;->startSysCmd([Ljava/lang/String;Ljava/lang/String;)Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;

    move-result-object v1

    return-object v1
.end method

.method public static startSysCmd([Ljava/lang/String;Ljava/lang/String;)Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;
    .locals 1
    .param p0, "cmdarray"    # [Ljava/lang/String;
    .param p1, "childStdin"    # Ljava/lang/String;

    .prologue
    .line 22
    new-instance v0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;

    invoke-direct {v0, p0, p1}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/ChildProcess;-><init>([Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
