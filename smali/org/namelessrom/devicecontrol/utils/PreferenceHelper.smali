.class public Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;
.super Ljava/lang/Object;
.source "PreferenceHelper.java"


# direct methods
.method public static getBoolean(Ljava/lang/String;Z)Z
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Z

    .prologue
    .line 41
    invoke-static {}, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->getInstance()Lorg/namelessrom/devicecontrol/database/DatabaseHandler;

    move-result-object v1

    const-string v2, "devicecontrol"

    invoke-virtual {v1, p0, v2}, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->getValueByName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 43
    .local v0, "value":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .end local p1    # "defaultValue":Z
    :goto_0
    return p1

    .restart local p1    # "defaultValue":Z
    :cond_0
    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    goto :goto_0
.end method

.method public static getInt(Ljava/lang/String;I)I
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "defaultValue"    # I

    .prologue
    .line 32
    invoke-static {}, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->getInstance()Lorg/namelessrom/devicecontrol/database/DatabaseHandler;

    move-result-object v1

    const-string v2, "devicecontrol"

    invoke-virtual {v1, p0, v2}, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->getValueByName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "value":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .end local p1    # "defaultValue":I
    :goto_0
    return p1

    .restart local p1    # "defaultValue":I
    :cond_0
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result p1

    goto :goto_0
.end method
