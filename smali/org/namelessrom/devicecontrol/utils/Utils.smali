.class public Lorg/namelessrom/devicecontrol/utils/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field private static final BLACKLIST:[Ljava/lang/String;

.field private static final ENABLED_STATES:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 68
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    const/high16 v1, 0x7f080000

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/Application;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/namelessrom/devicecontrol/utils/Utils;->BLACKLIST:[Ljava/lang/String;

    .line 70
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "TRUE"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "1"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "255"

    aput-object v2, v0, v1

    sput-object v0, Lorg/namelessrom/devicecontrol/utils/Utils;->ENABLED_STATES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 285
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    .end local p0    # "path":Ljava/lang/String;
    :goto_0
    return-object p0

    .restart local p0    # "path":Ljava/lang/String;
    :cond_0
    const-string p0, ""

    goto :goto_0
.end method

.method public static checkPaths([Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "paths"    # [Ljava/lang/String;

    .prologue
    .line 272
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 273
    .local v3, "s":Ljava/lang/String;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 275
    .end local v3    # "s":Ljava/lang/String;
    :goto_1
    return-object v3

    .line 272
    .restart local v3    # "s":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 275
    .end local v3    # "s":Ljava/lang/String;
    :cond_1
    const-string v3, ""

    goto :goto_1
.end method

.method public static closeQuietly(Ljava/io/Closeable;)V
    .locals 1
    .param p0, "closeable"    # Ljava/io/Closeable;

    .prologue
    .line 628
    if-nez p0, :cond_0

    .line 632
    :goto_0
    return-void

    .line 630
    :cond_0
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 631
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static disableComponent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "componentName"    # Ljava/lang/String;

    .prologue
    .line 447
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->toggleComponent(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 448
    return-void
.end method

.method public static enableComponent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "componentName"    # Ljava/lang/String;

    .prologue
    .line 451
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->toggleComponent(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 452
    return-void
.end method

.method public static execute(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "command"    # Ljava/lang/String;

    .prologue
    .line 356
    new-instance v0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor;-><init>()V

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor;->su:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$sh;

    invoke-virtual {v0, p0}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$sh;->runWaitFor(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$CommandResult2;

    move-result-object v0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$CommandResult2;->stdout:Ljava/lang/String;

    return-object v0
.end method

.method public static existsInFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "file"    # Ljava/lang/String;
    .param p1, "prop"    # Ljava/lang/String;

    .prologue
    .line 78
    invoke-static {p0, p1}, Lorg/namelessrom/devicecontrol/utils/Utils;->findPropValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static fileExists(Ljava/lang/String;)Z
    .locals 1
    .param p0, "filename"    # Ljava/lang/String;

    .prologue
    .line 248
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "-"

    invoke-static {v0, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 249
    :cond_0
    const/4 v0, 0x0

    .line 251
    :goto_0
    return v0

    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    goto :goto_0
.end method

.method public static fileExists([Ljava/lang/String;)Z
    .locals 5
    .param p0, "files"    # [Ljava/lang/String;

    .prologue
    .line 261
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .local v3, "s":Ljava/lang/String;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    .line 262
    .end local v3    # "s":Ljava/lang/String;
    :goto_1
    return v4

    .line 261
    .restart local v3    # "s":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 262
    .end local v3    # "s":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static findPropValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "file"    # Ljava/lang/String;
    .param p1, "prop"    # Ljava/lang/String;

    .prologue
    .line 83
    :try_start_0
    invoke-static {p0, p1}, Lorg/namelessrom/devicecontrol/utils/Utils;->findPropValueOf(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 84
    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    const-string v1, ""

    goto :goto_0
.end method

.method private static findPropValueOf(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "file"    # Ljava/lang/String;
    .param p1, "prop"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 88
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 89
    .local v2, "f":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->canRead()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 90
    const/4 v3, 0x0

    .line 91
    .local v3, "fis":Ljava/io/FileInputStream;
    const/4 v5, 0x0

    .line 92
    .local v5, "isr":Ljava/io/InputStreamReader;
    const/4 v0, 0x0

    .line 95
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .local v4, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v6, Ljava/io/InputStreamReader;

    invoke-direct {v6, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 97
    .end local v5    # "isr":Ljava/io/InputStreamReader;
    .local v6, "isr":Ljava/io/InputStreamReader;
    :try_start_2
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 100
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    :cond_0
    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    .local v7, "s":Ljava/lang/String;
    if-eqz v7, :cond_1

    .line 101
    invoke-virtual {v7, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x3d

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result-object v8

    .line 104
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 105
    invoke-static {v6}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 106
    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 110
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "isr":Ljava/io/InputStreamReader;
    .end local v7    # "s":Ljava/lang/String;
    :goto_0
    return-object v8

    .line 104
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "isr":Ljava/io/InputStreamReader;
    .restart local v7    # "s":Ljava/lang/String;
    :cond_1
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 105
    invoke-static {v6}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 106
    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 110
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "isr":Ljava/io/InputStreamReader;
    .end local v7    # "s":Ljava/lang/String;
    :cond_2
    const-string v8, ""

    goto :goto_0

    .line 104
    .restart local v0    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "isr":Ljava/io/InputStreamReader;
    :catchall_0
    move-exception v8

    :goto_1
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 105
    invoke-static {v5}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 106
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    throw v8

    .line 104
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v8

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_1

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .end local v5    # "isr":Ljava/io/InputStreamReader;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "isr":Ljava/io/InputStreamReader;
    :catchall_2
    move-exception v8

    move-object v5, v6

    .end local v6    # "isr":Ljava/io/InputStreamReader;
    .restart local v5    # "isr":Ljava/io/InputStreamReader;
    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_1

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .end local v5    # "isr":Ljava/io/InputStreamReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "isr":Ljava/io/InputStreamReader;
    :catchall_3
    move-exception v8

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v5, v6

    .end local v6    # "isr":Ljava/io/InputStreamReader;
    .restart local v5    # "isr":Ljava/io/InputStreamReader;
    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method public static getAndroidId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 583
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getBatteryHealth(I)Ljava/lang/String;
    .locals 2
    .param p0, "healthInt"    # I

    .prologue
    .line 517
    packed-switch p0, :pswitch_data_0

    .line 536
    :pswitch_0
    const v0, 0x7f0e022e

    .line 540
    .local v0, "health":I
    :goto_0
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 519
    .end local v0    # "health":I
    :pswitch_1
    const v0, 0x7f0e0047

    .line 520
    .restart local v0    # "health":I
    goto :goto_0

    .line 522
    .end local v0    # "health":I
    :pswitch_2
    const v0, 0x7f0e011c

    .line 523
    .restart local v0    # "health":I
    goto :goto_0

    .line 525
    .end local v0    # "health":I
    :pswitch_3
    const v0, 0x7f0e0086

    .line 526
    .restart local v0    # "health":I
    goto :goto_0

    .line 528
    .end local v0    # "health":I
    :pswitch_4
    const v0, 0x7f0e0184

    .line 529
    .restart local v0    # "health":I
    goto :goto_0

    .line 531
    .end local v0    # "health":I
    :pswitch_5
    const v0, 0x7f0e0186

    .line 532
    .restart local v0    # "health":I
    goto :goto_0

    .line 517
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getCommandResult(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "command"    # Ljava/lang/String;
    .param p1, "def"    # Ljava/lang/String;

    .prologue
    .line 360
    new-instance v1, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor;

    invoke-direct {v1}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor;-><init>()V

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor;->su:Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$sh;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$sh;->runWaitFor(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$CommandResult2;

    move-result-object v1

    iget-object v0, v1, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor$CommandResult2;->stdout:Ljava/lang/String;

    .line 361
    .local v0, "result":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 365
    .end local p1    # "def":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 364
    .restart local p1    # "def":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 365
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object p1, v0

    goto :goto_0
.end method

.method public static getCommandResult(Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;ILjava/lang/String;)V
    .locals 1
    .param p0, "listener"    # Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;
    .param p1, "id"    # I
    .param p2, "cmd"    # Ljava/lang/String;

    .prologue
    .line 400
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->getCommandResult(Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;ILjava/lang/String;Z)V

    .line 401
    return-void
.end method

.method public static getCommandResult(Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;ILjava/lang/String;Z)V
    .locals 8
    .param p0, "listener"    # Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;
    .param p1, "ID"    # I
    .param p2, "COMMAND"    # Ljava/lang/String;
    .param p3, "NEWLINE"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 405
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 406
    .local v3, "sb":Ljava/lang/StringBuilder;
    new-instance v0, Lorg/namelessrom/devicecontrol/utils/Utils$1;

    new-array v2, v2, [Ljava/lang/String;

    aput-object p2, v2, v1

    move v4, p3

    move-object v5, p0

    move v6, p1

    invoke-direct/range {v0 .. v6}, Lorg/namelessrom/devicecontrol/utils/Utils$1;-><init>(I[Ljava/lang/String;Ljava/lang/StringBuilder;ZLorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;I)V

    .line 427
    .local v0, "comm":Lcom/stericson/roottools/execution/CommandCapture;
    const/4 v1, 0x1

    :try_start_0
    invoke-static {v1}, Lcom/stericson/roottools/RootTools;->getShell(Z)Lcom/stericson/roottools/execution/Shell;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/stericson/roottools/execution/Shell;->add(Lcom/stericson/roottools/execution/Command;)Lcom/stericson/roottools/execution/Command;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 431
    :goto_0
    return-void

    .line 428
    :catch_0
    move-exception v7

    .line 429
    .local v7, "e":Ljava/lang/Exception;
    const-class v1, Lorg/namelessrom/devicecontrol/utils/Utils;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "runRootCommand: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getCommandResult(Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;Ljava/lang/String;)V
    .locals 1
    .param p0, "listener"    # Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 395
    const/4 v0, -0x1

    invoke-static {p0, v0, p1}, Lorg/namelessrom/devicecontrol/utils/Utils;->getCommandResult(Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;ILjava/lang/String;)V

    .line 396
    return-void
.end method

.method public static getDate(J)Ljava/lang/String;
    .locals 2
    .param p0, "time"    # J

    .prologue
    .line 622
    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-static {v1}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 623
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 624
    const-string v1, "dd-MM-yyyy"

    invoke-static {v1, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getDateAndTime()Ljava/lang/String;
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    .prologue
    .line 601
    new-instance v0, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 602
    .local v0, "date":Ljava/util/Date;
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd.HH.mm.ss"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 603
    .local v1, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    const-string v2, "UTC"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 604
    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 349
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, ""

    .line 352
    :goto_0
    return-object v1

    .line 350
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 351
    .local v0, "splitted":[Ljava/lang/String;
    const-class v1, Lorg/namelessrom/devicecontrol/utils/Utils;

    const-string v2, "getFileName(%s) --> %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    array-length v5, v0

    add-int/lit8 v5, v5, -0x1

    aget-object v5, v0, v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 352
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v0, v1

    goto :goto_0
.end method

.method public static getReadCommand(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 434
    const-string v0, "cat %s 2> /dev/null"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 438
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "busybox chmod 644 %s;\n"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "busybox echo \"%s\" > %s;\n"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    aput-object p0, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isEnabled(Ljava/lang/String;Z)Z
    .locals 6
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "contains"    # Z

    .prologue
    const/4 v4, 0x1

    .line 501
    if-eqz p0, :cond_3

    .line 502
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p0

    .line 503
    sget-object v0, Lorg/namelessrom/devicecontrol/utils/Utils;->ENABLED_STATES:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_3

    aget-object v3, v0, v1

    .line 504
    .local v3, "state":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 505
    invoke-virtual {p0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 511
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "state":Ljava/lang/String;
    :cond_0
    :goto_1
    return v4

    .line 507
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    .restart local v3    # "state":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 503
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 511
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "state":Ljava/lang/String;
    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static isFileBlacklisted(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 5
    .param p0, "file"    # Ljava/lang/String;
    .param p1, "blacklist"    # [Ljava/lang/String;

    .prologue
    .line 342
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 343
    .local v3, "s":Ljava/lang/String;
    invoke-static {v3, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    .line 345
    .end local v3    # "s":Ljava/lang/String;
    :goto_1
    return v4

    .line 342
    .restart local v3    # "s":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 345
    .end local v3    # "s":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static isNameless()Z
    .locals 2

    .prologue
    .line 73
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "org.namelessrom.android"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/system/build.prop"

    const-string v1, "ro.nameless.version"

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->existsInFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static listFiles(Ljava/lang/String;Z)[Ljava/lang/String;
    .locals 1
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "blacklist"    # Z

    .prologue
    .line 320
    if-eqz p1, :cond_0

    sget-object v0, Lorg/namelessrom/devicecontrol/utils/Utils;->BLACKLIST:[Ljava/lang/String;

    :goto_0
    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->listFiles(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static listFiles(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 11
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "blacklist"    # [Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 324
    const-string v7, "ls %s"

    new-array v8, v9, [Ljava/lang/Object;

    aput-object p0, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lorg/namelessrom/devicecontrol/utils/Utils;->execute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 325
    .local v5, "output":Ljava/lang/String;
    const-class v7, Lorg/namelessrom/devicecontrol/utils/Utils;

    const-string v8, "listFiles --> output: %s"

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v5, v9, v10

    invoke-static {v7, v8, v9}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 326
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    new-array v7, v10, [Ljava/lang/String;

    .line 338
    :goto_0
    return-object v7

    .line 328
    :cond_0
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 329
    .local v1, "files":[Ljava/lang/String;
    if-eqz p1, :cond_3

    .line 330
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 331
    .local v2, "filtered":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object v0, v1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_2

    aget-object v6, v0, v3

    .line 332
    .local v6, "s":Ljava/lang/String;
    invoke-static {v6, p1}, Lorg/namelessrom/devicecontrol/utils/Utils;->isFileBlacklisted(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 333
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 331
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 336
    .end local v6    # "s":Ljava/lang/String;
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v7, v7, [Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    goto :goto_0

    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "filtered":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_3
    move-object v7, v1

    .line 338
    goto :goto_0
.end method

.method public static loadFromAssets(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 114
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 116
    .local v6, "sb":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .line 117
    .local v2, "htmlStream":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 118
    .local v4, "reader":Ljava/io/InputStreamReader;
    const/4 v0, 0x0

    .line 120
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v7

    invoke-virtual {v7}, Lorg/namelessrom/devicecontrol/Application;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v7

    invoke-virtual {v7, p0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 121
    new-instance v5, Ljava/io/InputStreamReader;

    invoke-direct {v5, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 122
    .end local v4    # "reader":Ljava/io/InputStreamReader;
    .local v5, "reader":Ljava/io/InputStreamReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 124
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    :goto_0
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .local v3, "line":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 125
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0xa

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 128
    .end local v3    # "line":Ljava/lang/String;
    :catchall_0
    move-exception v7

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v4, v5

    .end local v5    # "reader":Ljava/io/InputStreamReader;
    .restart local v4    # "reader":Ljava/io/InputStreamReader;
    :goto_1
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 129
    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 130
    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    throw v7

    .line 128
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v4    # "reader":Ljava/io/InputStreamReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "line":Ljava/lang/String;
    .restart local v5    # "reader":Ljava/io/InputStreamReader;
    :cond_0
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 129
    invoke-static {v5}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 130
    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 133
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7

    .line 128
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v3    # "line":Ljava/lang/String;
    .end local v5    # "reader":Ljava/io/InputStreamReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/InputStreamReader;
    :catchall_1
    move-exception v7

    goto :goto_1

    .end local v4    # "reader":Ljava/io/InputStreamReader;
    .restart local v5    # "reader":Ljava/io/InputStreamReader;
    :catchall_2
    move-exception v7

    move-object v4, v5

    .end local v5    # "reader":Ljava/io/InputStreamReader;
    .restart local v4    # "reader":Ljava/io/InputStreamReader;
    goto :goto_1
.end method

.method public static lockFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 443
    const-string v0, "busybox chmod 444 %s;"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static parseInt(Ljava/lang/String;)I
    .locals 1
    .param p0, "integer"    # Ljava/lang/String;

    .prologue
    .line 608
    const/4 v0, -0x1

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static parseInt(Ljava/lang/String;I)I
    .locals 6
    .param p0, "integer"    # Ljava/lang/String;
    .param p1, "def"    # I

    .prologue
    .line 613
    if-eqz p0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 614
    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 617
    .end local p1    # "def":I
    :goto_0
    return p1

    .line 615
    .restart local p1    # "def":I
    :catch_0
    move-exception v0

    .line 616
    .local v0, "exc":Ljava/lang/NumberFormatException;
    const-string v1, "Utils"

    const-string v2, "parseInt(%s, %s)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static readFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "sFile"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    .line 174
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 175
    const/4 v3, 0x0

    .line 176
    .local v3, "reader":Ljava/io/FileReader;
    const/4 v0, 0x0

    .line 178
    .local v0, "brBuffer":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, p0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    .end local v3    # "reader":Ljava/io/FileReader;
    .local v4, "reader":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    const/16 v7, 0x200

    invoke-direct {v1, v4, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 181
    .end local v0    # "brBuffer":Ljava/io/BufferedReader;
    .local v1, "brBuffer":Ljava/io/BufferedReader;
    :try_start_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 183
    .local v6, "sb":Ljava/lang/StringBuilder;
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .local v5, "s":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 184
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0xa

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    .line 187
    .end local v5    # "s":Ljava/lang/String;
    .end local v6    # "sb":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v2

    move-object v0, v1

    .end local v1    # "brBuffer":Ljava/io/BufferedReader;
    .restart local v0    # "brBuffer":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 188
    .end local v4    # "reader":Ljava/io/FileReader;
    .local v2, "e":Ljava/lang/Exception;
    .restart local v3    # "reader":Ljava/io/FileReader;
    :goto_1
    const/4 v7, 0x1

    :try_start_3
    invoke-static {p0, v7}, Lorg/namelessrom/devicecontrol/utils/Utils;->readFileViaShell(Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v7

    .line 190
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 191
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 196
    .end local v0    # "brBuffer":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "reader":Ljava/io/FileReader;
    :goto_2
    return-object v7

    .line 186
    .restart local v1    # "brBuffer":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/FileReader;
    .restart local v5    # "s":Ljava/lang/String;
    .restart local v6    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    :try_start_4
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-object v7

    .line 190
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 191
    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_2

    .line 190
    .end local v1    # "brBuffer":Ljava/io/BufferedReader;
    .end local v4    # "reader":Ljava/io/FileReader;
    .end local v5    # "s":Ljava/lang/String;
    .end local v6    # "sb":Ljava/lang/StringBuilder;
    .restart local v0    # "brBuffer":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/FileReader;
    :catchall_0
    move-exception v7

    :goto_3
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 191
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    throw v7

    .line 194
    .end local v0    # "brBuffer":Ljava/io/BufferedReader;
    .end local v3    # "reader":Ljava/io/FileReader;
    :cond_1
    const-string v7, "Utils"

    const-string v8, "File does not exist or is not readable -> %s"

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p0, v9, v10

    invoke-static {v7, v8, v9}, Lorg/namelessrom/devicecontrol/Logger;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 196
    const/4 v7, 0x0

    goto :goto_2

    .line 190
    .restart local v0    # "brBuffer":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/FileReader;
    :catchall_1
    move-exception v7

    move-object v3, v4

    .end local v4    # "reader":Ljava/io/FileReader;
    .restart local v3    # "reader":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "brBuffer":Ljava/io/BufferedReader;
    .end local v3    # "reader":Ljava/io/FileReader;
    .restart local v1    # "brBuffer":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/FileReader;
    :catchall_2
    move-exception v7

    move-object v0, v1

    .end local v1    # "brBuffer":Ljava/io/BufferedReader;
    .restart local v0    # "brBuffer":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "reader":Ljava/io/FileReader;
    .restart local v3    # "reader":Ljava/io/FileReader;
    goto :goto_3

    .line 187
    :catch_1
    move-exception v2

    goto :goto_1

    .end local v3    # "reader":Ljava/io/FileReader;
    .restart local v4    # "reader":Ljava/io/FileReader;
    :catch_2
    move-exception v2

    move-object v3, v4

    .end local v4    # "reader":Ljava/io/FileReader;
    .restart local v3    # "reader":Ljava/io/FileReader;
    goto :goto_1
.end method

.method public static readFileViaShell(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 4
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "useSu"    # Z

    .prologue
    .line 200
    const-string v1, "cat %s;"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 201
    .local v0, "command":Ljava/lang/String;
    if-eqz p1, :cond_0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor;->runSuCommand(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->getStdout()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CMDProcessor;->runShellCommand(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/utils/cmdprocessor/CommandResult;->getStdout()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static readOneLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "sFile"    # Ljava/lang/String;

    .prologue
    .line 143
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static readOneLine(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 7
    .param p0, "sFile"    # Ljava/lang/String;
    .param p1, "trim"    # Z

    .prologue
    const/4 v6, 0x1

    .line 147
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 150
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, p0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    const/16 v4, 0x200

    invoke-direct {v0, v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    .local v0, "brBuffer":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 153
    .local v2, "value":Ljava/lang/String;
    if-eqz p1, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 155
    .end local v2    # "value":Ljava/lang/String;
    :cond_0
    :try_start_2
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 164
    .end local v0    # "brBuffer":Ljava/io/BufferedReader;
    :goto_0
    return-object v2

    .line 155
    .restart local v0    # "brBuffer":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v3

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    throw v3
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 157
    .end local v0    # "brBuffer":Ljava/io/BufferedReader;
    :catch_0
    move-exception v1

    .line 158
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "Utils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "could not read file: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 159
    invoke-static {p0, v6}, Lorg/namelessrom/devicecontrol/utils/Utils;->readFileViaShell(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 162
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    const-string v3, "Utils"

    const-string v4, "File does not exist or is not readable -> %s"

    new-array v5, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    invoke-static {v3, v4, v5}, Lorg/namelessrom/devicecontrol/Logger;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 164
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static readStringArray(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 312
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 313
    .local v0, "line":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 314
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 316
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static remount(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "mode"    # Ljava/lang/String;

    .prologue
    .line 544
    const-string v0, "busybox mount -o %s,remount %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;)V

    .line 545
    return-void
.end method

.method public static restartActivity(Landroid/app/Activity;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    const v1, 0x10a0001

    const/high16 v0, 0x10a0000

    .line 554
    if-nez p0, :cond_0

    .line 559
    :goto_0
    return-void

    .line 555
    :cond_0
    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 556
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 557
    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 558
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static runRootCommand(Ljava/lang/String;)V
    .locals 1
    .param p0, "command"    # Ljava/lang/String;

    .prologue
    .line 369
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;Z)V

    .line 370
    return-void
.end method

.method public static runRootCommand(Ljava/lang/String;Z)V
    .locals 7
    .param p0, "command"    # Ljava/lang/String;
    .param p1, "wait"    # Z

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 379
    const-string v2, "runRootCommand"

    const-string v3, "executing -> %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object p0, v4, v5

    invoke-static {v2, v3, v4}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 380
    new-instance v0, Lcom/stericson/roottools/execution/CommandCapture;

    new-array v2, v6, [Ljava/lang/String;

    aput-object p0, v2, v5

    invoke-direct {v0, v5, v2}, Lcom/stericson/roottools/execution/CommandCapture;-><init>(I[Ljava/lang/String;)V

    .line 382
    .local v0, "comm":Lcom/stericson/roottools/execution/CommandCapture;
    const/4 v2, 0x1

    :try_start_0
    invoke-static {v2}, Lcom/stericson/roottools/RootTools;->getShell(Z)Lcom/stericson/roottools/execution/Shell;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/stericson/roottools/execution/Shell;->add(Lcom/stericson/roottools/execution/Command;)Lcom/stericson/roottools/execution/Command;

    .line 383
    if-eqz p1, :cond_1

    .line 385
    :cond_0
    invoke-virtual {v0}, Lcom/stericson/roottools/execution/CommandCapture;->isExecuting()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_0

    .line 392
    :cond_1
    :goto_0
    return-void

    .line 389
    :catch_0
    move-exception v1

    .line 390
    .local v1, "e":Ljava/lang/Exception;
    const-class v2, Lorg/namelessrom/devicecontrol/utils/Utils;

    const-string v3, "runRootCommand"

    invoke-static {v2, v3, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static setPermissions(Ljava/lang/String;Ljava/lang/String;II)Ljava/lang/String;
    .locals 4
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "mask"    # Ljava/lang/String;
    .param p2, "user"    # I
    .param p3, "group"    # I

    .prologue
    .line 549
    const-string v0, "busybox chown %s.%s %s;busybox chmod %s %s;"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object p0, v1, v2

    const/4 v2, 0x3

    aput-object p1, v1, v2

    const/4 v2, 0x4

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static setupDirectories()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 293
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v7

    invoke-virtual {v7}, Lorg/namelessrom/devicecontrol/Application;->getFilesDirectory()Ljava/lang/String;

    move-result-object v1

    .line 294
    .local v1, "basePath":Ljava/lang/String;
    new-array v3, v12, [Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lorg/namelessrom/devicecontrol/DeviceConstants;->DC_LOG_DIR:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v11

    .line 296
    .local v3, "dirList":[Ljava/lang/String;
    move-object v0, v3

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v6, v0, v4

    .line 297
    .local v6, "s":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 298
    .local v2, "dir":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    .line 299
    const-class v7, Lorg/namelessrom/devicecontrol/utils/Utils;

    const-string v8, "setupDirectories: creating %s -> %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v6, v9, v11

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 296
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 303
    .end local v2    # "dir":Ljava/io/File;
    .end local v6    # "s":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public static showToast(Landroid/content/Context;I)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resourceId"    # I

    .prologue
    .line 588
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 589
    return-void
.end method

.method public static showToast(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 592
    sget-object v0, Lorg/namelessrom/devicecontrol/Application;->HANDLER:Landroid/os/Handler;

    new-instance v1, Lorg/namelessrom/devicecontrol/utils/Utils$2;

    invoke-direct {v1, p0, p1}, Lorg/namelessrom/devicecontrol/utils/Utils$2;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 597
    return-void
.end method

.method public static startTaskerService()Z
    .locals 7

    .prologue
    .line 472
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v5

    invoke-static {v5}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v5

    iget-boolean v5, v5, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->enabled:Z

    if-nez v5, :cond_0

    const/4 v0, 0x0

    .line 491
    .local v0, "enabled":Z
    .local v1, "i$":Ljava/util/Iterator;
    .local v3, "tasker":Landroid/content/Intent;
    .local v4, "taskerItemList":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/tasker/TaskerItem;>;"
    :goto_0
    return v0

    .line 474
    .end local v0    # "enabled":Z
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "tasker":Landroid/content/Intent;
    .end local v4    # "taskerItemList":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/tasker/TaskerItem;>;"
    :cond_0
    const/4 v0, 0x0

    .line 475
    .restart local v0    # "enabled":Z
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v5

    invoke-static {v5}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v5

    iget-object v4, v5, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->items:Ljava/util/ArrayList;

    .line 476
    .restart local v4    # "taskerItemList":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/tasker/TaskerItem;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    .line 477
    .local v2, "item":Lorg/namelessrom/devicecontrol/tasker/TaskerItem;
    iget-boolean v5, v2, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->enabled:Z

    if-eqz v5, :cond_1

    .line 478
    const/4 v0, 0x1

    .line 483
    .end local v2    # "item":Lorg/namelessrom/devicecontrol/tasker/TaskerItem;
    :cond_2
    new-instance v3, Landroid/content/Intent;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v5

    const-class v6, Lorg/namelessrom/devicecontrol/services/TaskerService;

    invoke-direct {v3, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 484
    .restart local v3    # "tasker":Landroid/content/Intent;
    if-eqz v0, :cond_3

    .line 485
    const-string v5, "action_start"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 489
    :goto_1
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v5

    invoke-virtual {v5, v3}, Lorg/namelessrom/devicecontrol/Application;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 487
    :cond_3
    const-string v5, "action_stop"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method

.method public static stopTaskerService()V
    .locals 3

    .prologue
    .line 495
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    const-class v2, Lorg/namelessrom/devicecontrol/services/TaskerService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 496
    .local v0, "tasker":Landroid/content/Intent;
    const-string v1, "action_stop"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 497
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/Application;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 498
    return-void
.end method

.method public static toggleComponent(Landroid/content/ComponentName;Z)V
    .locals 3
    .param p0, "component"    # Landroid/content/ComponentName;
    .param p1, "disable"    # Z

    .prologue
    const/4 v2, 0x1

    .line 460
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 461
    .local v0, "pm":Landroid/content/pm/PackageManager;
    if-eqz v0, :cond_0

    .line 462
    if-eqz p1, :cond_1

    const/4 v1, 0x2

    :goto_0
    invoke-virtual {v0, p0, v1, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 469
    :cond_0
    return-void

    :cond_1
    move v1, v2

    .line 462
    goto :goto_0
.end method

.method public static toggleComponent(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "componentName"    # Ljava/lang/String;
    .param p2, "disable"    # Z

    .prologue
    .line 456
    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, p0, p1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, p2}, Lorg/namelessrom/devicecontrol/utils/Utils;->toggleComponent(Landroid/content/ComponentName;Z)V

    .line 457
    return-void
.end method

.method public static tryParse(Ljava/lang/String;I)I
    .locals 1
    .param p0, "parse"    # Ljava/lang/String;
    .param p1, "def"    # I

    .prologue
    .line 562
    :try_start_0
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .end local p1    # "def":I
    :goto_0
    return p1

    .restart local p1    # "def":I
    :catch_0
    move-exception v0

    .local v0, "exc":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static tryValueOf(Ljava/lang/String;I)Ljava/lang/Integer;
    .locals 2
    .param p0, "value"    # Ljava/lang/String;
    .param p1, "def"    # I

    .prologue
    .line 566
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    .local v0, "exc":Ljava/lang/Exception;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0
.end method

.method public static writeToFile(Ljava/io/File;Ljava/lang/String;)Z
    .locals 6
    .param p0, "file"    # Ljava/io/File;
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 635
    const/4 v0, 0x0

    .line 637
    .local v0, "fw":Ljava/io/FileWriter;
    :try_start_0
    new-instance v1, Ljava/io/FileWriter;

    const/4 v4, 0x0

    invoke-direct {v1, p0, v4}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 638
    .end local v0    # "fw":Ljava/io/FileWriter;
    .local v1, "fw":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v1, p1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 639
    invoke-virtual {v1}, Ljava/io/FileWriter;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 640
    const/4 v3, 0x1

    .line 645
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    move-object v0, v1

    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v0    # "fw":Ljava/io/FileWriter;
    :goto_0
    return v3

    .line 641
    :catch_0
    move-exception v2

    .line 642
    .local v2, "ioe":Ljava/io/IOException;
    :goto_1
    :try_start_2
    const-string v4, "Utils"

    const-string v5, "could not write to file"

    invoke-static {v4, v5, v2}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 645
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v2    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    :goto_2
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    throw v3

    .end local v0    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    :catchall_1
    move-exception v3

    move-object v0, v1

    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v0    # "fw":Ljava/io/FileWriter;
    goto :goto_2

    .line 641
    .end local v0    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    :catch_1
    move-exception v2

    move-object v0, v1

    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v0    # "fw":Ljava/io/FileWriter;
    goto :goto_1
.end method

.method public static writeValue(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 213
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 215
    :try_start_0
    new-instance v0, Ljava/io/FileWriter;

    invoke-direct {v0, p0}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    .local v0, "fw":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v0, p1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 220
    :try_start_2
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 227
    .end local v0    # "fw":Ljava/io/FileWriter;
    :cond_0
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 220
    .restart local v0    # "fw":Ljava/io/FileWriter;
    :catchall_0
    move-exception v2

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->closeQuietly(Ljava/io/Closeable;)V

    throw v2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 222
    .end local v0    # "fw":Ljava/io/FileWriter;
    :catch_0
    move-exception v1

    .line 223
    .local v1, "ignored":Ljava/io/IOException;
    invoke-static {p0, p1}, Lorg/namelessrom/devicecontrol/utils/Utils;->writeValueViaShell(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static writeValueViaShell(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 237
    invoke-static {p0, p1}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;)V

    .line 238
    return-void
.end method
