.class public Lorg/namelessrom/devicecontrol/utils/ContentTypes;
.super Ljava/lang/Object;
.source "ContentTypes.java"


# static fields
.field private static contentTypes:Lorg/namelessrom/devicecontrol/utils/ContentTypes;


# instance fields
.field private final mContentTypes:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    .line 35
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "3gp"

    const-string v2, "video/3gp"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "7z"

    const-string v2, "application/x-7z-compressed"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "aac"

    const-string v2, "audio/x-aac"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "apk"

    const-string v2, "application/vnd.android.package-archive"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "avi"

    const-string v2, "video/avi"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "bin"

    const-string v2, "application/octet-stream"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "bmp"

    const-string v2, "image/bmp"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "bz"

    const-string v2, "application/x-bzip"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "bz2"

    const-string v2, "application/x-bzip2"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "css"

    const-string v2, "text/css"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "deb"

    const-string v2, "application/x-debian-package"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "doc"

    const-string v2, "application/msword"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "dot"

    const-string v2, "application/msword"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "exe"

    const-string v2, "application/octet-stream"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "flv"

    const-string v2, "video/x-flv"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "gif"

    const-string v2, "image/gif"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "gz"

    const-string v2, "application/x-gzip"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "gzip"

    const-string v2, "application/x-gzip"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "htm"

    const-string v2, "text/html"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "html"

    const-string v2, "text/html"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "htmls"

    const-string v2, "text/html"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "ico"

    const-string v2, "image/x-icon"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "jpe"

    const-string v2, "image/jpeg"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "jpeg"

    const-string v2, "image/jpeg"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "jpg"

    const-string v2, "image/jpeg"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "js"

    const-string v2, "application/javascript"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "json"

    const-string v2, "application/json"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "m4v"

    const-string v2, "video/x-m4v"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "mov"

    const-string v2, "video/quicktime"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "mp3"

    const-string v2, "audio/mpeg3"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "mp4"

    const-string v2, "video/mp4"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "mpeg"

    const-string v2, "video/mpeg"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "ogg"

    const-string v2, "audio/ogg"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "pdf"

    const-string v2, "application/pdf"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "png"

    const-string v2, "image/png"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "ppt"

    const-string v2, "application/powerpoint"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "rar"

    const-string v2, "application/x-rar-compressed"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "rss"

    const-string v2, "application/rss+xml"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "rtf"

    const-string v2, "application/rtf"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "shtml"

    const-string v2, "text/html"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "swf"

    const-string v2, "application/x-shockwave-flash"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "tar"

    const-string v2, "application/x-tar"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "tgz"

    const-string v2, "application/x-compressed"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "torrent"

    const-string v2, "application/x-bittorrent"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "ttf"

    const-string v2, "application/x-font-ttf"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "txt"

    const-string v2, "text/plain"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "wav"

    const-string v2, "audio/wav"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "webm"

    const-string v2, "video/webm"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "wmv"

    const-string v2, "video/x-ms-wmv"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "xhtml"

    const-string v2, "application/xhtml+xml"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "xml"

    const-string v2, "application/rss+xml"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    const-string v1, "zip"

    const-string v2, "application/zip"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    return-void
.end method

.method public static getInstance()Lorg/namelessrom/devicecontrol/utils/ContentTypes;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->contentTypes:Lorg/namelessrom/devicecontrol/utils/ContentTypes;

    if-nez v0, :cond_0

    new-instance v0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/utils/ContentTypes;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->contentTypes:Lorg/namelessrom/devicecontrol/utils/ContentTypes;

    .line 91
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->contentTypes:Lorg/namelessrom/devicecontrol/utils/ContentTypes;

    return-object v0
.end method

.method public static isFiletypeMatching(Ljava/io/File;Ljava/lang/String;)Z
    .locals 1
    .param p0, "file"    # Ljava/io/File;
    .param p1, "fileType"    # Ljava/lang/String;

    .prologue
    .line 112
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->isFiletypeMatching(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isFiletypeMatching(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "fileType"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 116
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 119
    :cond_0
    :goto_0
    return v2

    .line 117
    :cond_1
    const-string v3, "\\."

    invoke-virtual {p0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 118
    .local v1, "tmpString":[Ljava/lang/String;
    array-length v3, v1

    if-lez v3, :cond_3

    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    aget-object v0, v1, v3

    .line 119
    .local v0, "tmp":Ljava/lang/String;
    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 118
    .end local v0    # "tmp":Ljava/lang/String;
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private tryGetContentType(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 101
    const-string v3, "."

    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 102
    .local v2, "index":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 103
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 104
    .local v1, "fileExtension":Ljava/lang/String;
    const-string v3, "fileExtension: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->mContentTypes:Ljava/util/Hashtable;

    invoke-virtual {v3, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 106
    .local v0, "ct":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 108
    .end local v0    # "ct":Ljava/lang/String;
    .end local v1    # "fileExtension":Ljava/lang/String;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getContentType(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->tryGetContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 96
    .local v0, "type":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 97
    .end local v0    # "type":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "type":Ljava/lang/String;
    :cond_0
    const-string v0, "text/plain"

    goto :goto_0
.end method
