.class public Lorg/namelessrom/devicecontrol/utils/DrawableHelper;
.super Ljava/lang/Object;
.source "DrawableHelper.java"


# direct methods
.method public static applyAccentColorFilter(I)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p0, "drawableRes"    # I

    .prologue
    .line 44
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/Application;->getAccentColor()I

    move-result v1

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/DrawableHelper;->applyColorFilter(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public static applyAccentColorFilter(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 49
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/Application;->getAccentColor()I

    move-result v0

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/utils/DrawableHelper;->applyColorFilter(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public static applyColorFilter(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p0, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p1, "color"    # I

    .prologue
    .line 34
    if-nez p0, :cond_0

    .line 35
    const-string v1, "DrawableHelper"

    const-string v2, "drawable is null!"

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/Logger;->w(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    const/4 p0, 0x0

    .line 40
    .end local p0    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_0
    return-object p0

    .line 38
    .restart local p0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    new-instance v0, Landroid/graphics/LightingColorFilter;

    const/high16 v1, -0x1000000

    invoke-direct {v0, v1, p1}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    .line 39
    .local v0, "lightingColorFilter":Landroid/graphics/LightingColorFilter;
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0
.end method
