.class public Lorg/namelessrom/devicecontrol/database/DataItem;
.super Ljava/lang/Object;
.source "DataItem.java"


# instance fields
.field private _category:Ljava/lang/String;

.field private _filename:Ljava/lang/String;

.field private _id:I

.field private _name:Ljava/lang/String;

.field private _value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/database/DataItem;->_category:Ljava/lang/String;

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/database/DataItem;->_filename:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/database/DataItem;->_name:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/database/DataItem;->_value:Ljava/lang/String;

    return-object v0
.end method

.method public setCategory(Ljava/lang/String;)V
    .locals 0
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/database/DataItem;->_category:Ljava/lang/String;

    return-void
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 0
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/database/DataItem;->_filename:Ljava/lang/String;

    return-void
.end method

.method public setID(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 50
    iput p1, p0, Lorg/namelessrom/devicecontrol/database/DataItem;->_id:I

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/database/DataItem;->_name:Ljava/lang/String;

    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/database/DataItem;->_value:Ljava/lang/String;

    return-void
.end method
