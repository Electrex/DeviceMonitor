.class public Lorg/namelessrom/devicecontrol/database/DatabaseHandler;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "DatabaseHandler.java"


# static fields
.field private static sDatabaseHandler:Lorg/namelessrom/devicecontrol/database/DatabaseHandler;

.field private static sDb:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    sput-object v0, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->sDatabaseHandler:Lorg/namelessrom/devicecontrol/database/DatabaseHandler;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    const-string v1, "DeviceControl.db"

    const/4 v2, 0x0

    const/16 v3, 0x9

    invoke-direct {p0, p1, v1, v2, v3}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 81
    :try_start_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sput-object v1, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->sDb:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    :goto_0
    return-void

    .line 82
    :catch_0
    move-exception v0

    .line 83
    .local v0, "sqle":Landroid/database/sqlite/SQLiteException;
    const-string v1, "Could not get writable database."

    invoke-static {p0, v1, v0}, Lorg/namelessrom/devicecontrol/Logger;->wtf(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static declared-synchronized getInstance()Lorg/namelessrom/devicecontrol/database/DatabaseHandler;
    .locals 3

    .prologue
    .line 88
    const-class v1, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->sDatabaseHandler:Lorg/namelessrom/devicecontrol/database/DatabaseHandler;

    if-nez v0, :cond_0

    .line 89
    new-instance v0, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    invoke-direct {v0, v2}, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;-><init>(Landroid/content/Context;)V

    sput-object v0, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->sDatabaseHandler:Lorg/namelessrom/devicecontrol/database/DatabaseHandler;

    .line 91
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->sDatabaseHandler:Lorg/namelessrom/devicecontrol/database/DatabaseHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized tearDown()V
    .locals 3

    .prologue
    .line 95
    const-class v1, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;

    monitor-enter v1

    :try_start_0
    const-class v0, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;

    const-string v2, "tearDown()"

    invoke-static {v0, v2}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    sget-object v0, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->sDb:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    .line 97
    sget-object v0, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->sDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 98
    const/4 v0, 0x0

    sput-object v0, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->sDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 100
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->sDatabaseHandler:Lorg/namelessrom/devicecontrol/database/DatabaseHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    monitor-exit v1

    return-void

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private wipeDb(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 171
    const-string v0, "DROP TABLE IF EXISTS boot_up"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 172
    const-string v0, "DROP TABLE IF EXISTS devicecontrol"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 173
    const-string v0, "DROP TABLE IF EXISTS tasker"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 174
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 175
    return-void
.end method


# virtual methods
.method public getAllItems(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1, "tableName"    # Ljava/lang/String;
    .param p2, "category"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/database/DataItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 204
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 205
    .local v2, "itemList":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/database/DataItem;>;"
    sget-object v4, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->sDb:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v4, :cond_0

    .line 227
    :goto_0
    return-object v2

    .line 207
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SELECT * FROM "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, ""

    :goto_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 211
    .local v3, "selectQuery":Ljava/lang/String;
    sget-object v4, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->sDb:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 213
    .local v0, "cursor":Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 216
    :cond_1
    new-instance v1, Lorg/namelessrom/devicecontrol/database/DataItem;

    invoke-direct {v1}, Lorg/namelessrom/devicecontrol/database/DataItem;-><init>()V

    .line 217
    .local v1, "item":Lorg/namelessrom/devicecontrol/database/DataItem;
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v4}, Lorg/namelessrom/devicecontrol/database/DataItem;->setID(I)V

    .line 218
    const/4 v4, 0x1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lorg/namelessrom/devicecontrol/database/DataItem;->setCategory(Ljava/lang/String;)V

    .line 219
    const/4 v4, 0x2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lorg/namelessrom/devicecontrol/database/DataItem;->setName(Ljava/lang/String;)V

    .line 220
    const/4 v4, 0x3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lorg/namelessrom/devicecontrol/database/DataItem;->setFileName(Ljava/lang/String;)V

    .line 221
    const/4 v4, 0x4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lorg/namelessrom/devicecontrol/database/DataItem;->setValue(Ljava/lang/String;)V

    .line 222
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 226
    .end local v1    # "item":Lorg/namelessrom/devicecontrol/database/DataItem;
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 207
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v3    # "selectQuery":Ljava/lang/String;
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " WHERE category = \'"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v6, 0x27

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method public getAllTaskerItems(Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .param p1, "category"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/tasker/TaskerItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 238
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 239
    .local v3, "itemList":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/tasker/TaskerItem;>;"
    sget-object v5, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->sDb:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v5, :cond_0

    .line 264
    :goto_0
    return-object v3

    .line 241
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SELECT * FROM tasker"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, ""

    :goto_1
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 245
    .local v4, "selectQuery":Ljava/lang/String;
    sget-object v5, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->sDb:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v7, 0x0

    invoke-virtual {v5, v4, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 247
    .local v0, "cursor":Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 250
    :cond_1
    new-instance v2, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    invoke-direct {v2}, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;-><init>()V

    .line 253
    .local v2, "item":Lorg/namelessrom/devicecontrol/tasker/TaskerItem;
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->category:Ljava/lang/String;

    .line 254
    const/4 v5, 0x2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->name:Ljava/lang/String;

    .line 255
    const/4 v5, 0x3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->trigger:Ljava/lang/String;

    .line 256
    const/4 v5, 0x4

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->value:Ljava/lang/String;

    .line 257
    const/4 v5, 0x5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 258
    .local v1, "enabled":Ljava/lang/String;
    if-eqz v1, :cond_4

    const-string v5, "1"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    move v5, v6

    :goto_2
    iput-boolean v5, v2, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->enabled:Z

    .line 259
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 260
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 263
    .end local v1    # "enabled":Ljava/lang/String;
    .end local v2    # "item":Lorg/namelessrom/devicecontrol/tasker/TaskerItem;
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 241
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v4    # "selectQuery":Ljava/lang/String;
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " WHERE category = \'"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v8, 0x27

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 258
    .restart local v0    # "cursor":Landroid/database/Cursor;
    .restart local v1    # "enabled":Ljava/lang/String;
    .restart local v2    # "item":Lorg/namelessrom/devicecontrol/tasker/TaskerItem;
    .restart local v4    # "selectQuery":Ljava/lang/String;
    :cond_4
    const/4 v5, 0x0

    goto :goto_2
.end method

.method public getValueByName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "tableName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 185
    sget-object v0, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->sDb:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_1

    .line 197
    :cond_0
    :goto_0
    return-object v5

    .line 187
    :cond_1
    sget-object v0, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->sDb:Landroid/database/sqlite/SQLiteDatabase;

    new-array v2, v4, [Ljava/lang/String;

    const-string v1, "value"

    aput-object v1, v2, v6

    const-string v3, "name=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v1, p2

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 190
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_2

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 191
    :cond_2
    if-eqz v9, :cond_0

    .line 193
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_3

    move-object v10, v5

    .line 196
    .local v10, "result":Ljava/lang/String;
    :goto_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    move-object v5, v10

    .line 197
    goto :goto_0

    .line 193
    .end local v10    # "result":Ljava/lang/String;
    :cond_3
    const-string v0, "value"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto :goto_1
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 105
    const-string v0, "CREATE TABLE boot_up(id INTEGER PRIMARY KEY,category TEXT,name TEXT,filename TEXT,value TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 106
    const-string v0, "CREATE TABLE devicecontrol(id INTEGER PRIMARY KEY,name TEXT,value TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 107
    const-string v0, "CREATE TABLE tasker(id INTEGER PRIMARY KEY,category TEXT,name TEXT,trigger TEXT,value TEXT,enabled TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 108
    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 157
    const-string v2, "DeviceControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onDowngrade | oldVersion: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " | newVersion: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->wipeDb(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 162
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/Application;->getFilesDirectory()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lorg/namelessrom/devicecontrol/DeviceConstants;->DC_DOWNGRADE:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    .local v0, "downgradeFile":Ljava/io/File;
    const-string v2, "creating downgrade file -> %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p0, v2, v3}, Lorg/namelessrom/devicecontrol/Logger;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    .end local v0    # "downgradeFile":Ljava/io/File;
    :goto_0
    return-void

    .line 165
    :catch_0
    move-exception v1

    .line 166
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "could not create downgrade file"

    invoke-static {p0, v2, v1}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 9
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/16 v5, 0x9

    const/4 v4, 0x1

    .line 112
    const-string v1, "DeviceControl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onUpgrade | oldVersion: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | newVersion: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    move v0, p2

    .line 117
    .local v0, "currentVersion":I
    if-ge v0, v4, :cond_0

    .line 118
    const-string v1, "DROP TABLE IF EXISTS boot_up"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 119
    const-string v1, "CREATE TABLE boot_up(id INTEGER PRIMARY KEY,category TEXT,name TEXT,filename TEXT,value TEXT)"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 120
    const/4 v0, 0x1

    .line 123
    :cond_0
    if-ge v0, v7, :cond_1

    .line 124
    const-string v1, "DROP TABLE IF EXISTS boot_up"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 125
    const-string v1, "CREATE TABLE boot_up(id INTEGER PRIMARY KEY,category TEXT,name TEXT,filename TEXT,value TEXT)"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 126
    const-string v1, "DROP TABLE IF EXISTS tasker"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 127
    const-string v1, "CREATE TABLE tasker(id INTEGER PRIMARY KEY,category TEXT,name TEXT,trigger TEXT,value TEXT,enabled TEXT)"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 128
    const/4 v0, 0x3

    .line 131
    :cond_1
    if-ge v0, v8, :cond_2

    .line 132
    const-string v1, "DROP TABLE IF EXISTS boot_up"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 133
    const-string v1, "CREATE TABLE boot_up(id INTEGER PRIMARY KEY,category TEXT,name TEXT,filename TEXT,value TEXT)"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 134
    const-string v1, "DROP TABLE IF EXISTS devicecontrol"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 135
    const-string v1, "CREATE TABLE devicecontrol(id INTEGER PRIMARY KEY,name TEXT,value TEXT)"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 136
    const/16 v0, 0x8

    .line 139
    :cond_2
    if-ge v0, v5, :cond_3

    .line 140
    const-string v1, "ALTER TABLE %s RENAME TO tmp;"

    new-array v2, v4, [Ljava/lang/Object;

    const-string v3, "tasker"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 141
    const-string v1, "CREATE TABLE tasker(id INTEGER PRIMARY KEY,category TEXT,name TEXT,trigger TEXT,value TEXT,enabled TEXT)"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 142
    const-string v1, "INSERT INTO %s(%s,%s,%s,%s,%s) SELECT %s,%s,%s,%s,%s FROM tmp;"

    const/16 v2, 0xb

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "tasker"

    aput-object v3, v2, v6

    const-string v3, "id"

    aput-object v3, v2, v4

    const/4 v3, 0x2

    const-string v4, "category"

    aput-object v4, v2, v3

    const-string v3, "name"

    aput-object v3, v2, v7

    const/4 v3, 0x4

    const-string v4, "value"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "enabled"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "id"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "category"

    aput-object v4, v2, v3

    const-string v3, "name"

    aput-object v3, v2, v8

    const-string v3, "value"

    aput-object v3, v2, v5

    const/16 v3, 0xa

    const-string v4, "enabled"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 147
    const-string v1, "DROP TABLE tmp;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 150
    :cond_3
    if-eq v0, v5, :cond_4

    .line 151
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->wipeDb(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 153
    :cond_4
    return-void
.end method
