.class public Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ScreenReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver$Worker;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 47
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 36
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 37
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 38
    const-string v1, "action: %s"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 40
    new-instance v1, Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver$Worker;

    invoke-direct {v1, p0, p1}, Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver$Worker;-><init>(Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver;Landroid/content/Context;)V

    new-array v2, v3, [Ljava/lang/String;

    const-string v3, "screen_on"

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver$Worker;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 45
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    new-instance v1, Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver$Worker;

    invoke-direct {v1, p0, p1}, Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver$Worker;-><init>(Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver;Landroid/content/Context;)V

    new-array v2, v3, [Ljava/lang/String;

    const-string v3, "screen_off"

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver$Worker;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
