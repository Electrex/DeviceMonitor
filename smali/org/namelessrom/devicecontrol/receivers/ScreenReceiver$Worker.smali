.class Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver$Worker;
.super Landroid/os/AsyncTask;
.source "ScreenReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Worker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver;


# direct methods
.method public constructor <init>(Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver$Worker;->this$0:Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 51
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver$Worker;->mContext:Landroid/content/Context;

    .line 52
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 47
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver$Worker;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 10
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 56
    aget-object v3, p1, v8

    .line 57
    .local v3, "trigger":Ljava/lang/String;
    const-string v4, "Trigger: %s"

    new-array v5, v9, [Ljava/lang/Object;

    aput-object v3, v5, v8

    invoke-static {p0, v4, v5}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 59
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver$Worker;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v4

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver$Worker;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->loadConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v4

    invoke-virtual {v4, v3}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->getItemsByTrigger(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 63
    .local v2, "itemList":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/tasker/TaskerItem;>;"
    const-string v4, "Items: %s"

    new-array v5, v9, [Ljava/lang/Object;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {p0, v4, v5}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 65
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    .line 66
    .local v1, "item":Lorg/namelessrom/devicecontrol/tasker/TaskerItem;
    const-string v4, "Processing: %s | %s | %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, v1, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->name:Ljava/lang/String;

    aput-object v6, v5, v8

    iget-object v6, v1, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->value:Ljava/lang/String;

    aput-object v6, v5, v9

    const/4 v6, 0x2

    iget-boolean v7, v1, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->enabled:Z

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {p0, v4, v5}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    iget-boolean v4, v1, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->enabled:Z

    if-eqz v4, :cond_0

    .line 68
    iget-object v4, v1, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->name:Ljava/lang/String;

    iget-object v5, v1, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->value:Ljava/lang/String;

    invoke-static {v4, v5, v8}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor;->getProcessAction(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 72
    .end local v1    # "item":Lorg/namelessrom/devicecontrol/tasker/TaskerItem;
    :cond_1
    const/4 v4, 0x0

    return-object v4
.end method
