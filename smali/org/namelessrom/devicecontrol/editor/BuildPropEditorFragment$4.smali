.class Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;
.super Ljava/lang/Object;
.source "BuildPropEditorFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->editBuildPropDialog(Lorg/namelessrom/devicecontrol/objects/Prop;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

.field final synthetic val$etName:Landroid/widget/EditText;

.field final synthetic val$etValue:Landroid/widget/EditText;

.field final synthetic val$p:Lorg/namelessrom/devicecontrol/objects/Prop;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;Lorg/namelessrom/devicecontrol/objects/Prop;Landroid/widget/EditText;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 311
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;->this$0:Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;->val$p:Lorg/namelessrom/devicecontrol/objects/Prop;

    iput-object p3, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;->val$etValue:Landroid/widget/EditText;

    iput-object p4, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;->val$etName:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/16 v4, 0xc8

    .line 314
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;->val$p:Lorg/namelessrom/devicecontrol/objects/Prop;

    if-eqz v2, :cond_2

    .line 315
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;->val$etValue:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 316
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;->val$p:Lorg/namelessrom/devicecontrol/objects/Prop;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/objects/Prop;->getName()Ljava/lang/String;

    move-result-object v0

    .line 317
    .local v0, "name":Ljava/lang/String;
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;->val$etValue:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 318
    .local v1, "value":Ljava/lang/String;
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;->val$p:Lorg/namelessrom/devicecontrol/objects/Prop;

    invoke-virtual {v2, v1}, Lorg/namelessrom/devicecontrol/objects/Prop;->setVal(Ljava/lang/String;)V

    .line 319
    const-string v2, "/system"

    const-string v3, "rw"

    invoke-static {v2, v3}, Lorg/namelessrom/devicecontrol/utils/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;->this$0:Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/Scripts;->addOrUpdate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v4, v3}, Lorg/namelessrom/devicecontrol/utils/Utils;->getCommandResult(Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;ILjava/lang/String;)V

    .line 336
    .end local v0    # "name":Ljava/lang/String;
    .end local v1    # "value":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;->this$0:Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mProps:Ljava/util/List;
    invoke-static {v2}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->access$300(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 337
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;->this$0:Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;
    invoke-static {v2}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->access$100(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;)Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;->this$0:Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;
    invoke-static {v2}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->access$100(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;)Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->notifyDataSetChanged()V

    .line 338
    :cond_1
    return-void

    .line 324
    :cond_2
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;->val$etValue:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;->val$etName:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 325
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;->val$etName:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 326
    .restart local v0    # "name":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 327
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;->val$etValue:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 328
    .restart local v1    # "value":Ljava/lang/String;
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;->this$0:Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mProps:Ljava/util/List;
    invoke-static {v2}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->access$300(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lorg/namelessrom/devicecontrol/objects/Prop;

    invoke-direct {v3, v0, v1}, Lorg/namelessrom/devicecontrol/objects/Prop;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 329
    const-string v2, "/system"

    const-string v3, "rw"

    invoke-static {v2, v3}, Lorg/namelessrom/devicecontrol/utils/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;->this$0:Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/Scripts;->addOrUpdate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v4, v3}, Lorg/namelessrom/devicecontrol/utils/Utils;->getCommandResult(Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;ILjava/lang/String;)V

    goto :goto_0
.end method
