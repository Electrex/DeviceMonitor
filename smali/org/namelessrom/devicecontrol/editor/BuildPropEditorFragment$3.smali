.class Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$3;
.super Ljava/lang/Object;
.source "BuildPropEditorFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->editBuildPropDialog(Lorg/namelessrom/devicecontrol/objects/Prop;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

.field final synthetic val$etValue:Landroid/widget/EditText;

.field final synthetic val$sp:Landroid/widget/Spinner;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;Landroid/widget/Spinner;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$3;->this$0:Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$3;->val$sp:Landroid/widget/Spinner;

    iput-object p3, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$3;->val$etValue:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "selectedItemView"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 292
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$3;->val$sp:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$3;->val$etValue:Landroid/widget/EditText;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$3;->val$sp:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 295
    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 297
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
