.class Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$6;
.super Ljava/lang/Object;
.source "BuildPropEditorFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->makeDialog(ILjava/lang/String;Lorg/namelessrom/devicecontrol/objects/Prop;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

.field final synthetic val$prop:Lorg/namelessrom/devicecontrol/objects/Prop;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;Lorg/namelessrom/devicecontrol/objects/Prop;)V
    .locals 0

    .prologue
    .line 357
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$6;->this$0:Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$6;->val$prop:Lorg/namelessrom/devicecontrol/objects/Prop;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 360
    const-string v0, "/system"

    const-string v1, "rw"

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$6;->this$0:Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

    const/16 v1, 0x64

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$6;->val$prop:Lorg/namelessrom/devicecontrol/objects/Prop;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/objects/Prop;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Scripts;->removeProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->getCommandResult(Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;ILjava/lang/String;)V

    .line 363
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$6;->this$0:Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->access$100(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;)Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$6;->this$0:Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->access$100(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;)Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    move-result-object v0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$6;->val$prop:Lorg/namelessrom/devicecontrol/objects/Prop;

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->remove(Ljava/lang/Object;)V

    .line 364
    :cond_0
    return-void
.end method
