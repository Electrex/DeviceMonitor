.class Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;
.super Ljava/lang/Object;
.source "SysctlEditorFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->editPropDialog(Lorg/namelessrom/devicecontrol/objects/Prop;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

.field final synthetic val$dn:Ljava/lang/String;

.field final synthetic val$p:Lorg/namelessrom/devicecontrol/objects/Prop;

.field final synthetic val$tn:Landroid/widget/TextView;

.field final synthetic val$tv:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;Lorg/namelessrom/devicecontrol/objects/Prop;Landroid/widget/EditText;Ljava/lang/String;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 333
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;->this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;->val$p:Lorg/namelessrom/devicecontrol/objects/Prop;

    iput-object p3, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;->val$tv:Landroid/widget/EditText;

    iput-object p4, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;->val$dn:Ljava/lang/String;

    iput-object p5, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;->val$tn:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/16 v5, 0xc9

    .line 336
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;->val$p:Lorg/namelessrom/devicecontrol/objects/Prop;

    if-eqz v2, :cond_1

    .line 337
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;->val$tv:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 338
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;->val$p:Lorg/namelessrom/devicecontrol/objects/Prop;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/objects/Prop;->getName()Ljava/lang/String;

    move-result-object v0

    .line 339
    .local v0, "name":Ljava/lang/String;
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;->val$tv:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 340
    .local v1, "value":Ljava/lang/String;
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;->val$p:Lorg/namelessrom/devicecontrol/objects/Prop;

    invoke-virtual {v2, v1}, Lorg/namelessrom/devicecontrol/objects/Prop;->setVal(Ljava/lang/String;)V

    .line 341
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;->this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;->val$dn:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/sysctl.conf"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lorg/namelessrom/devicecontrol/utils/Scripts;->addOrUpdate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v5, v3}, Lorg/namelessrom/devicecontrol/utils/Utils;->getCommandResult(Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;ILjava/lang/String;)V

    .line 355
    .end local v0    # "name":Ljava/lang/String;
    .end local v1    # "value":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;->this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mProps:Ljava/util/List;
    invoke-static {v2}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->access$800(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 356
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;->this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;
    invoke-static {v2}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->access$100(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->notifyDataSetChanged()V

    .line 357
    return-void

    .line 345
    :cond_1
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;->val$tv:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;->val$tn:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 346
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;->val$tn:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 347
    .restart local v0    # "name":Ljava/lang/String;
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;->val$tv:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 348
    .restart local v1    # "value":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 349
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;->this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mProps:Ljava/util/List;
    invoke-static {v2}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->access$800(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lorg/namelessrom/devicecontrol/objects/Prop;

    invoke-direct {v3, v0, v1}, Lorg/namelessrom/devicecontrol/objects/Prop;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 350
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;->this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;->val$dn:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/sysctl.conf"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lorg/namelessrom/devicecontrol/utils/Scripts;->addOrUpdate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v5, v3}, Lorg/namelessrom/devicecontrol/utils/Utils;->getCommandResult(Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;ILjava/lang/String;)V

    goto :goto_0
.end method
