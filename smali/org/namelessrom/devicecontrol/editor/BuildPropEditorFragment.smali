.class public Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;
.source "BuildPropEditorFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;


# instance fields
.field private mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

.field private mEmptyView:Landroid/widget/LinearLayout;

.field private mFilter:Landroid/widget/EditText;

.field private mListView:Landroid/widget/ListView;

.field private mLoadingView:Landroid/widget/LinearLayout;

.field private final mProps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/objects/Prop;",
            ">;"
        }
    .end annotation
.end field

.field private mTools:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;-><init>()V

    .line 69
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    .line 70
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mFilter:Landroid/widget/EditText;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mProps:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;ILjava/lang/String;Lorg/namelessrom/devicecontrol/objects/Prop;)V
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lorg/namelessrom/devicecontrol/objects/Prop;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->makeDialog(ILjava/lang/String;Lorg/namelessrom/devicecontrol/objects/Prop;)V

    return-void
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;)Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

    .prologue
    .line 56
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

    .prologue
    .line 56
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mFilter:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$300(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

    .prologue
    .line 56
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mProps:Ljava/util/List;

    return-object v0
.end method

.method private editBuildPropDialog(Lorg/namelessrom/devicecontrol/objects/Prop;)V
    .locals 14
    .param p1, "p"    # Lorg/namelessrom/devicecontrol/objects/Prop;

    .prologue
    .line 231
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 232
    .local v0, "activity":Landroid/app/Activity;
    if-nez v0, :cond_0

    .line 340
    :goto_0
    return-void

    .line 236
    :cond_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-static {v10}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v10

    const v11, 0x7f040027

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v10, v11, v12, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 238
    .local v1, "editDialog":Landroid/view/View;
    const v10, 0x7f0c0089

    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 239
    .local v7, "tvName":Landroid/widget/TextView;
    const v10, 0x7f0c008a

    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 240
    .local v2, "etName":Landroid/widget/EditText;
    const v10, 0x7f0c008b

    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 241
    .local v3, "etValue":Landroid/widget/EditText;
    const v10, 0x7f0c008d

    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Spinner;

    .line 242
    .local v5, "sp":Landroid/widget/Spinner;
    const v10, 0x7f0c008c

    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 243
    .local v4, "lpresets":Landroid/widget/LinearLayout;
    new-instance v9, Landroid/widget/ArrayAdapter;

    const v10, 0x1090008

    invoke-direct {v9, v0, v10}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 245
    .local v9, "vAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v10, 0x1090009

    invoke-virtual {v9, v10}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 246
    invoke-virtual {v9}, Landroid/widget/ArrayAdapter;->clear()V

    .line 248
    if-eqz p1, :cond_5

    .line 249
    const v10, 0x7f0e00ac

    invoke-virtual {p0, v10}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 250
    .local v6, "title":Ljava/lang/String;
    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/objects/Prop;->getVal()Ljava/lang/String;

    move-result-object v8

    .line 252
    .local v8, "v":Ljava/lang/String;
    const/16 v10, 0x8

    invoke-virtual {v4, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 253
    const-string v10, "0"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 254
    const-string v10, "0"

    invoke-virtual {v9, v10}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 255
    const-string v10, "1"

    invoke-virtual {v9, v10}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 256
    const/4 v10, 0x0

    invoke-virtual {v4, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 257
    invoke-virtual {v5, v9}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 274
    :cond_1
    :goto_1
    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/objects/Prop;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 275
    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/objects/Prop;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 276
    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Landroid/widget/EditText;->setVisibility(I)V

    .line 277
    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/objects/Prop;->getVal()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 289
    .end local v8    # "v":Ljava/lang/String;
    :goto_2
    new-instance v10, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$3;

    invoke-direct {v10, p0, v5, v3}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$3;-><init>(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;Landroid/widget/Spinner;Landroid/widget/EditText;)V

    invoke-virtual {v5, v10}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 299
    new-instance v10, Landroid/app/AlertDialog$Builder;

    invoke-direct {v10, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v10, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    invoke-virtual {v10, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    const/high16 v11, 0x1040000

    invoke-virtual {p0, v11}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    new-instance v12, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$5;

    invoke-direct {v12, p0}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$5;-><init>(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;)V

    invoke-virtual {v10, v11, v12}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    const v11, 0x7f0e01b7

    invoke-virtual {p0, v11}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    new-instance v12, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;

    invoke-direct {v12, p0, p1, v3, v2}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$4;-><init>(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;Lorg/namelessrom/devicecontrol/objects/Prop;Landroid/widget/EditText;Landroid/widget/EditText;)V

    invoke-virtual {v10, v11, v12}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 258
    .restart local v8    # "v":Ljava/lang/String;
    :cond_2
    const-string v10, "1"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 259
    const-string v10, "1"

    invoke-virtual {v9, v10}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 260
    const-string v10, "0"

    invoke-virtual {v9, v10}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 261
    const/4 v10, 0x0

    invoke-virtual {v4, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 262
    invoke-virtual {v5, v9}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    goto :goto_1

    .line 263
    :cond_3
    const-string v10, "true"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 264
    const-string v10, "true"

    invoke-virtual {v9, v10}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 265
    const-string v10, "false"

    invoke-virtual {v9, v10}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 266
    const/4 v10, 0x0

    invoke-virtual {v4, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 267
    invoke-virtual {v5, v9}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    goto/16 :goto_1

    .line 268
    :cond_4
    const-string v10, "false"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 269
    const-string v10, "false"

    invoke-virtual {v9, v10}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 270
    const-string v10, "true"

    invoke-virtual {v9, v10}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 271
    const/4 v10, 0x0

    invoke-virtual {v4, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 272
    invoke-virtual {v5, v9}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    goto/16 :goto_1

    .line 279
    .end local v6    # "title":Ljava/lang/String;
    .end local v8    # "v":Ljava/lang/String;
    :cond_5
    const v10, 0x7f0e0017

    invoke-virtual {p0, v10}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 280
    .restart local v6    # "title":Ljava/lang/String;
    const-string v10, ""

    invoke-virtual {v9, v10}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 281
    const-string v10, "0"

    invoke-virtual {v9, v10}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 282
    const-string v10, "1"

    invoke-virtual {v9, v10}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 283
    const-string v10, "true"

    invoke-virtual {v9, v10}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 284
    const-string v10, "false"

    invoke-virtual {v9, v10}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 285
    invoke-virtual {v5, v9}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 286
    const/4 v10, 0x0

    invoke-virtual {v4, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 287
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Landroid/widget/EditText;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method private loadBuildProp(Ljava/lang/String;)V
    .locals 14
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 193
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 195
    .local v1, "activity":Landroid/app/Activity;
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mProps:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->clear()V

    .line 196
    const-string v9, "\n"

    invoke-virtual {p1, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 197
    .local v6, "p":[Ljava/lang/String;
    move-object v2, v6

    .local v2, "arr$":[Ljava/lang/String;
    array-length v5, v2

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_3

    aget-object v0, v2, v4

    .line 198
    .local v0, "aP":Ljava/lang/String;
    const-string v9, "#"

    invoke-virtual {v0, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_1

    const-string v9, "="

    invoke-virtual {v0, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 199
    const-string v9, "["

    const-string v10, ""

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "]"

    const-string v11, ""

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 200
    const-string v9, "="

    invoke-virtual {v0, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 201
    .local v7, "pp":[Ljava/lang/String;
    array-length v9, v7

    const/4 v10, 0x2

    if-lt v9, v10, :cond_2

    .line 202
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 203
    .local v8, "sb":Ljava/lang/StringBuilder;
    const/4 v3, 0x2

    .local v3, "i":I
    :goto_1
    array-length v9, v7

    if-ge v3, v9, :cond_0

    .line 204
    const/16 v9, 0x3d

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    aget-object v10, v7, v3

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 206
    :cond_0
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mProps:Ljava/util/List;

    new-instance v10, Lorg/namelessrom/devicecontrol/objects/Prop;

    const/4 v11, 0x0

    aget-object v11, v7, v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x1

    aget-object v13, v7, v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Lorg/namelessrom/devicecontrol/objects/Prop;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    .end local v3    # "i":I
    .end local v7    # "pp":[Ljava/lang/String;
    .end local v8    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 208
    .restart local v7    # "pp":[Ljava/lang/String;
    :cond_2
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mProps:Ljava/util/List;

    new-instance v10, Lorg/namelessrom/devicecontrol/objects/Prop;

    const/4 v11, 0x0

    aget-object v11, v7, v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    const-string v12, ""

    invoke-direct {v10, v11, v12}, Lorg/namelessrom/devicecontrol/objects/Prop;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 212
    .end local v0    # "aP":Ljava/lang/String;
    .end local v7    # "pp":[Ljava/lang/String;
    :cond_3
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mProps:Ljava/util/List;

    invoke-static {v9}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 214
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mLoadingView:Landroid/widget/LinearLayout;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 215
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mProps:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 216
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mEmptyView:Landroid/widget/LinearLayout;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 224
    :goto_3
    return-void

    .line 218
    :cond_4
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mEmptyView:Landroid/widget/LinearLayout;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 219
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mListView:Landroid/widget/ListView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setVisibility(I)V

    .line 220
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mTools:Landroid/widget/RelativeLayout;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 221
    new-instance v9, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    iget-object v10, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mProps:Ljava/util/List;

    invoke-direct {v9, v1, v10}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v9, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    .line 222
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mListView:Landroid/widget/ListView;

    iget-object v10, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_3
.end method

.method private makeDialog(ILjava/lang/String;Lorg/namelessrom/devicecontrol/objects/Prop;)V
    .locals 4
    .param p1, "title"    # I
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "prop"    # Lorg/namelessrom/devicecontrol/objects/Prop;

    .prologue
    .line 343
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 344
    .local v0, "activity":Landroid/app/Activity;
    if-nez v0, :cond_0

    .line 367
    :goto_0
    return-void

    .line 346
    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$7;

    invoke-direct {v3, p0}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$7;-><init>(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040013

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$6;

    invoke-direct {v3, p0, p3}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$6;-><init>(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;Lorg/namelessrom/devicecontrol/objects/Prop;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0
.end method


# virtual methods
.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 77
    const v0, 0x7f0e05b1

    return v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 134
    const v0, 0x7f100001

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 136
    const v0, 0x7f0c0115

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 137
    const v0, 0x7f0c0116

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 139
    invoke-super {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 140
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    .line 86
    invoke-super {p0, p3}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;->onCreate(Landroid/os/Bundle;)V

    .line 87
    invoke-virtual {p0, v3}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->setHasOptionsMenu(Z)V

    .line 89
    const v1, 0x7f040054

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 91
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f0c0102

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mListView:Landroid/widget/ListView;

    .line 92
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 93
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 94
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setFastScrollAlwaysVisible(Z)V

    .line 95
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mListView:Landroid/widget/ListView;

    new-instance v2, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$1;

    invoke-direct {v2, p0}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$1;-><init>(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 108
    const v1, 0x7f0c00ff

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mLoadingView:Landroid/widget/LinearLayout;

    .line 109
    const v1, 0x7f0c0100

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mEmptyView:Landroid/widget/LinearLayout;

    .line 110
    const v1, 0x7f0c000b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mTools:Landroid/widget/RelativeLayout;

    .line 111
    const v1, 0x7f0c0103

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mFilter:Landroid/widget/EditText;

    .line 112
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mFilter:Landroid/widget/EditText;

    new-instance v2, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$2;

    invoke-direct {v2, p0}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$2;-><init>(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 127
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setVisibility(I)V

    .line 128
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mTools:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 130
    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "row"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 158
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    if-nez v1, :cond_1

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    invoke-virtual {v1, p3}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->getItem(I)Lorg/namelessrom/devicecontrol/objects/Prop;

    move-result-object v0

    .line 161
    .local v0, "p":Lorg/namelessrom/devicecontrol/objects/Prop;
    if-eqz v0, :cond_0

    .line 162
    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/objects/Prop;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "fingerprint"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 163
    invoke-direct {p0, v0}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->editBuildPropDialog(Lorg/namelessrom/devicecontrol/objects/Prop;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 143
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 144
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    .line 151
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 146
    :pswitch_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->editBuildPropDialog(Lorg/namelessrom/devicecontrol/objects/Prop;)V

    .line 147
    const/4 v1, 0x1

    goto :goto_0

    .line 144
    nop

    :pswitch_data_0
    .packed-switch 0x7f0c0114
        :pswitch_0
    .end packed-switch
.end method

.method public onShellOutput(Lorg/namelessrom/devicecontrol/objects/ShellOutput;)V
    .locals 2
    .param p1, "shellOutput"    # Lorg/namelessrom/devicecontrol/objects/ShellOutput;

    .prologue
    .line 169
    iget v0, p1, Lorg/namelessrom/devicecontrol/objects/ShellOutput;->id:I

    sparse-switch v0, :sswitch_data_0

    .line 178
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onReadPropsCompleted: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lorg/namelessrom/devicecontrol/objects/ShellOutput;->output:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 180
    iget-object v0, p1, Lorg/namelessrom/devicecontrol/objects/ShellOutput;->output:Ljava/lang/String;

    invoke-direct {p0, v0}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->loadBuildProp(Ljava/lang/String;)V

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 171
    :sswitch_0
    const-string v0, "/system"

    const-string v1, "ro"

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 174
    :sswitch_1
    const-string v0, "/system"

    const-string v1, "ro"

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 182
    :cond_1
    const-string v0, "Not attached!"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->w(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 169
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_1
        0xc8 -> :sswitch_0
    .end sparse-switch
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 80
    invoke-super {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 81
    const/4 v0, -0x1

    const-string v1, "cat /system/build.prop"

    const/4 v2, 0x1

    invoke-static {p0, v0, v1, v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->getCommandResult(Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;ILjava/lang/String;Z)V

    .line 82
    return-void
.end method

.method public showBurger()Z
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x0

    return v0
.end method
