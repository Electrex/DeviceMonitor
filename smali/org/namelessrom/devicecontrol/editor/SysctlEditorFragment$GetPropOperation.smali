.class Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$GetPropOperation;
.super Landroid/os/AsyncTask;
.source "SysctlEditorFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetPropOperation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;


# direct methods
.method private constructor <init>(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$GetPropOperation;->this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;
    .param p2, "x1"    # Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$1;

    .prologue
    .line 207
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$GetPropOperation;-><init>(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 207
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$GetPropOperation;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 5
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 216
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 217
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/Application;->getFilesDirectory()Ljava/lang/String;

    move-result-object v0

    .line 219
    .local v0, "dn":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    const-string v3, "/system/etc/sysctl.conf"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 220
    const-string v2, "/system/etc/sysctl.conf"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/sysctl.conf"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lorg/namelessrom/devicecontrol/utils/Scripts;->copyFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    :goto_0
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$GetPropOperation;->this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mLoadFull:Z
    invoke-static {v2}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->access$700(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 227
    const-string v2, "busybox echo `busybox find /proc/sys/* -type f -perm -644 |"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " grep -v \"vm.\"`;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 234
    :goto_1
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$GetPropOperation;->this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    const/4 v3, -0x1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->getCommandResult(Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;ILjava/lang/String;)V

    .line 236
    const/4 v2, 0x0

    return-object v2

    .line 222
    :cond_0
    const-string v2, "busybox echo \"# created by Device Control\n\" > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/sysctl.conf;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 230
    :cond_1
    const-string v2, "busybox echo `busybox find /proc/sys/vm/* -type f "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-prune -perm -644`;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 209
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$GetPropOperation;->this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mLoadingView:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->access$300(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 210
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$GetPropOperation;->this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->access$400(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 211
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$GetPropOperation;->this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mEmptyView:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->access$500(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 212
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$GetPropOperation;->this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mTools:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->access$600(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 213
    return-void
.end method
