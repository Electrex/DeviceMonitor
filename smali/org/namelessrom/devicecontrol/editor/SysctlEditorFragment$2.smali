.class Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$2;
.super Ljava/lang/Object;
.source "SysctlEditorFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$2;->this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 114
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 117
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 121
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$2;->this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->access$100(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 122
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$2;->this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mFilter:Landroid/widget/EditText;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->access$200(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 123
    .local v0, "filter":Landroid/text/Editable;
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$2;->this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    # getter for: Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->access$100(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v2

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 125
    .end local v0    # "filter":Landroid/text/Editable;
    :cond_0
    return-void

    .line 123
    .restart local v0    # "filter":Landroid/text/Editable;
    :cond_1
    const-string v1, ""

    goto :goto_0
.end method
