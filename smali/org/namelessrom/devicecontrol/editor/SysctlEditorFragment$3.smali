.class Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$3;
.super Ljava/lang/Object;
.source "SysctlEditorFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->makeApplyDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$3;->this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$3;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialogInterface"    # Landroid/content/DialogInterface;
    .param p2, "i"    # I

    .prologue
    .line 187
    const-string v0, "/system"

    const-string v1, "rw"

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$3;->this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    const/16 v1, 0xc8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v3

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/Application;->getFilesDirectory()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/sysctl.conf"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "/system/etc/sysctl.conf"

    invoke-static {v2, v3}, Lorg/namelessrom/devicecontrol/utils/Scripts;->copyFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->getCommandResult(Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;ILjava/lang/String;)V

    .line 192
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 193
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$3;->val$activity:Landroid/app/Activity;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$3;->this$0:Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    const v2, 0x7f0e021a

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 196
    return-void
.end method
