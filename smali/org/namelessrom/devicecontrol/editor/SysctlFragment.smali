.class public Lorg/namelessrom/devicecontrol/editor/SysctlFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;
.source "SysctlFragment.java"


# instance fields
.field private mDirtyBackground:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mDirtyExpireCentisecs:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mDirtyRatio:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mDirtyWriteback:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mFullEditor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mMinFreeK:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mOvercommit:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mSwappiness:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mVfs:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;-><init>()V

    return-void
.end method

.method public static restore(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 156
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 158
    .local v3, "sbCmd":Ljava/lang/StringBuilder;
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    move-result-object v4

    const-string v5, "sysctl"

    invoke-virtual {v4, v5}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->getItemsByCategory(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 161
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/namelessrom/devicecontrol/objects/BootupItem;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    .line 162
    .local v1, "item":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    iget-object v4, v1, Lorg/namelessrom/devicecontrol/objects/BootupItem;->name:Ljava/lang/String;

    iget-object v5, v1, Lorg/namelessrom/devicecontrol/objects/BootupItem;->value:Ljava/lang/String;

    invoke-static {v4, v5}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 165
    .end local v1    # "item":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method


# virtual methods
.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 69
    const v0, 0x7f0e054d

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 73
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 74
    const v0, 0x7f060013

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->addPreferencesFromResource(I)V

    .line 76
    const-string v0, "pref_full_editor"

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mFullEditor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 77
    const-string v0, "pref_dirty_ratio"

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mDirtyRatio:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 78
    const-string v0, "pref_dirty_background"

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mDirtyBackground:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 79
    const-string v0, "pref_dirty_expire"

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mDirtyExpireCentisecs:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 80
    const-string v0, "pref_dirty_writeback"

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mDirtyWriteback:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 81
    const-string v0, "pref_min_free_kb"

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mMinFreeK:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 82
    const-string v0, "pref_overcommit"

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mOvercommit:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 83
    const-string v0, "pref_swappiness"

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mSwappiness:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 84
    const-string v0, "pref_vfs"

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mVfs:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 86
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mDirtyRatio:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    const-string v1, "/proc/sys/vm/dirty_ratio"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mDirtyBackground:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    const-string v1, "/proc/sys/vm/dirty_background_ratio"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mDirtyExpireCentisecs:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    const-string v1, "/proc/sys/vm/dirty_expire_centisecs"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mDirtyWriteback:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    const-string v1, "/proc/sys/vm/dirty_writeback_centisecs"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mMinFreeK:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    const-string v1, "/proc/sys/vm/min_free_kbytes"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mOvercommit:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    const-string v1, "/proc/sys/vm/overcommit_ratio"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mSwappiness:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    const-string v1, "/proc/sys/vm/swappiness"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mVfs:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    const-string v1, "/proc/sys/vm/vfs_cache_pressure"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 94
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 9
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    const/16 v5, 0x1388

    const/16 v4, 0x64

    const/4 v3, 0x0

    const/4 v8, 0x1

    .line 98
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mFullEditor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne p2, v0, :cond_0

    .line 99
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v3, 0x7f0e0561

    invoke-static {v0, v3}, Lorg/namelessrom/devicecontrol/MainActivity;->loadFragment(Landroid/app/Activity;I)V

    move v0, v8

    .line 152
    :goto_0
    return v0

    .line 101
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mDirtyRatio:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne p2, v0, :cond_1

    .line 102
    const v0, 0x7f0e009a

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 103
    .local v2, "title":Ljava/lang/String;
    const-string v0, "/proc/sys/vm/dirty_ratio"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 104
    .local v1, "currentProgress":I
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v6, "/proc/sys/vm/dirty_ratio"

    const-string v7, "sysctl"

    move-object v5, p2

    invoke-static/range {v0 .. v7}, Lorg/namelessrom/devicecontrol/utils/DialogHelper;->openSeekbarDialog(Landroid/app/Activity;ILjava/lang/String;IILandroid/preference/Preference;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    .line 106
    goto :goto_0

    .line 107
    .end local v1    # "currentProgress":I
    .end local v2    # "title":Ljava/lang/String;
    :cond_1
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mDirtyBackground:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne p2, v0, :cond_2

    .line 108
    const v0, 0x7f0e0098

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 109
    .restart local v2    # "title":Ljava/lang/String;
    const-string v0, "/proc/sys/vm/dirty_background_ratio"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 110
    .restart local v1    # "currentProgress":I
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v6, "/proc/sys/vm/dirty_background_ratio"

    const-string v7, "sysctl"

    move-object v5, p2

    invoke-static/range {v0 .. v7}, Lorg/namelessrom/devicecontrol/utils/DialogHelper;->openSeekbarDialog(Landroid/app/Activity;ILjava/lang/String;IILandroid/preference/Preference;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    .line 112
    goto :goto_0

    .line 113
    .end local v1    # "currentProgress":I
    .end local v2    # "title":Ljava/lang/String;
    :cond_2
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mDirtyExpireCentisecs:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne p2, v0, :cond_3

    .line 114
    const v0, 0x7f0e0099

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 115
    .restart local v2    # "title":Ljava/lang/String;
    const-string v0, "/proc/sys/vm/dirty_expire_centisecs"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 116
    .restart local v1    # "currentProgress":I
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v6, "/proc/sys/vm/dirty_expire_centisecs"

    const-string v7, "sysctl"

    move v4, v5

    move-object v5, p2

    invoke-static/range {v0 .. v7}, Lorg/namelessrom/devicecontrol/utils/DialogHelper;->openSeekbarDialog(Landroid/app/Activity;ILjava/lang/String;IILandroid/preference/Preference;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    .line 118
    goto :goto_0

    .line 119
    .end local v1    # "currentProgress":I
    .end local v2    # "title":Ljava/lang/String;
    :cond_3
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mDirtyWriteback:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne p2, v0, :cond_4

    .line 120
    const v0, 0x7f0e009b

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 121
    .restart local v2    # "title":Ljava/lang/String;
    const-string v0, "/proc/sys/vm/dirty_writeback_centisecs"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 122
    .restart local v1    # "currentProgress":I
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v6, "/proc/sys/vm/dirty_writeback_centisecs"

    const-string v7, "sysctl"

    move v4, v5

    move-object v5, p2

    invoke-static/range {v0 .. v7}, Lorg/namelessrom/devicecontrol/utils/DialogHelper;->openSeekbarDialog(Landroid/app/Activity;ILjava/lang/String;IILandroid/preference/Preference;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    .line 124
    goto/16 :goto_0

    .line 125
    .end local v1    # "currentProgress":I
    .end local v2    # "title":Ljava/lang/String;
    :cond_4
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mMinFreeK:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne p2, v0, :cond_5

    .line 126
    const v0, 0x7f0e0162

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 127
    .restart local v2    # "title":Ljava/lang/String;
    const-string v0, "/proc/sys/vm/min_free_kbytes"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 128
    .restart local v1    # "currentProgress":I
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/16 v4, 0x2000

    const-string v6, "/proc/sys/vm/min_free_kbytes"

    const-string v7, "sysctl"

    move-object v5, p2

    invoke-static/range {v0 .. v7}, Lorg/namelessrom/devicecontrol/utils/DialogHelper;->openSeekbarDialog(Landroid/app/Activity;ILjava/lang/String;IILandroid/preference/Preference;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    .line 130
    goto/16 :goto_0

    .line 131
    .end local v1    # "currentProgress":I
    .end local v2    # "title":Ljava/lang/String;
    :cond_5
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mOvercommit:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne p2, v0, :cond_6

    .line 132
    const v0, 0x7f0e0185

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 133
    .restart local v2    # "title":Ljava/lang/String;
    const-string v0, "/proc/sys/vm/overcommit_ratio"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 134
    .restart local v1    # "currentProgress":I
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v6, "/proc/sys/vm/overcommit_ratio"

    const-string v7, "sysctl"

    move-object v5, p2

    invoke-static/range {v0 .. v7}, Lorg/namelessrom/devicecontrol/utils/DialogHelper;->openSeekbarDialog(Landroid/app/Activity;ILjava/lang/String;IILandroid/preference/Preference;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    .line 136
    goto/16 :goto_0

    .line 137
    .end local v1    # "currentProgress":I
    .end local v2    # "title":Ljava/lang/String;
    :cond_6
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mSwappiness:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne p2, v0, :cond_7

    .line 138
    const v0, 0x7f0e0206

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 139
    .restart local v2    # "title":Ljava/lang/String;
    const-string v0, "/proc/sys/vm/swappiness"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 140
    .restart local v1    # "currentProgress":I
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v6, "/proc/sys/vm/swappiness"

    const-string v7, "sysctl"

    move-object v5, p2

    invoke-static/range {v0 .. v7}, Lorg/namelessrom/devicecontrol/utils/DialogHelper;->openSeekbarDialog(Landroid/app/Activity;ILjava/lang/String;IILandroid/preference/Preference;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    .line 142
    goto/16 :goto_0

    .line 143
    .end local v1    # "currentProgress":I
    .end local v2    # "title":Ljava/lang/String;
    :cond_7
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->mVfs:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne p2, v0, :cond_8

    .line 144
    const v0, 0x7f0e0236

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 145
    .restart local v2    # "title":Ljava/lang/String;
    const-string v0, "/proc/sys/vm/vfs_cache_pressure"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 147
    .restart local v1    # "currentProgress":I
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/16 v4, 0xc8

    const-string v6, "/proc/sys/vm/vfs_cache_pressure"

    const-string v7, "sysctl"

    move-object v5, p2

    invoke-static/range {v0 .. v7}, Lorg/namelessrom/devicecontrol/utils/DialogHelper;->openSeekbarDialog(Landroid/app/Activity;ILjava/lang/String;IILandroid/preference/Preference;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    .line 149
    goto/16 :goto_0

    .line 152
    .end local v1    # "currentProgress":I
    .end local v2    # "title":Ljava/lang/String;
    :cond_8
    invoke-super {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    goto/16 :goto_0
.end method
