.class Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$1;
.super Ljava/lang/Object;
.source "BuildPropEditorFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$1;->this$0:Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 9
    .param p2, "view"    # Landroid/view/View;
    .param p3, "i"    # I
    .param p4, "l"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v8, 0x1

    .line 99
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/objects/Prop;

    .line 100
    .local v0, "p":Lorg/namelessrom/devicecontrol/objects/Prop;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/objects/Prop;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "fingerprint"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 101
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$1;->this$0:Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

    const v2, 0x7f0e008d

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment$1;->this$0:Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;

    const v4, 0x7f0e008e

    new-array v5, v8, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/objects/Prop;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->makeDialog(ILjava/lang/String;Lorg/namelessrom/devicecontrol/objects/Prop;)V
    invoke-static {v1, v2, v3, v0}, Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;->access$000(Lorg/namelessrom/devicecontrol/editor/BuildPropEditorFragment;ILjava/lang/String;Lorg/namelessrom/devicecontrol/objects/Prop;)V

    .line 104
    :cond_0
    return v8
.end method
