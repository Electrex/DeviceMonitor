.class public Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;
.source "SysctlEditorFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$GetPropOperation;
    }
.end annotation


# instance fields
.field private mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

.field private mEmptyView:Landroid/widget/LinearLayout;

.field private mFilter:Landroid/widget/EditText;

.field private mListView:Landroid/widget/ListView;

.field private mLoadFull:Z

.field private mLoadingView:Landroid/widget/LinearLayout;

.field private final mProps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/objects/Prop;",
            ">;"
        }
    .end annotation
.end field

.field private mTools:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;-><init>()V

    .line 74
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    .line 75
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mFilter:Landroid/widget/EditText;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mProps:Ljava/util/List;

    .line 78
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mLoadFull:Z

    .line 207
    return-void
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mFilter:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$300(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mLoadingView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$400(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$500(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mEmptyView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$600(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mTools:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$700(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)Z
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    .prologue
    .line 59
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mLoadFull:Z

    return v0
.end method

.method static synthetic access$800(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mProps:Ljava/util/List;

    return-object v0
.end method

.method private editPropDialog(Lorg/namelessrom/devicecontrol/objects/Prop;)V
    .locals 11
    .param p1, "p"    # Lorg/namelessrom/devicecontrol/objects/Prop;

    .prologue
    .line 304
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    .line 305
    .local v6, "activity":Landroid/app/Activity;
    if-nez v6, :cond_0

    .line 359
    :goto_0
    return-void

    .line 307
    :cond_0
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/Application;->getFilesDirectory()Ljava/lang/String;

    move-result-object v4

    .line 310
    .local v4, "dn":Ljava/lang/String;
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040028

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 312
    .local v7, "editDialog":Landroid/view/View;
    const v0, 0x7f0c008b

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 313
    .local v3, "tv":Landroid/widget/EditText;
    const v0, 0x7f0c0089

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 315
    .local v5, "tn":Landroid/widget/TextView;
    if-eqz p1, :cond_1

    .line 316
    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/objects/Prop;->getVal()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 317
    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/objects/Prop;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 318
    const v0, 0x7f0e00ac

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 323
    .local v8, "title":Ljava/lang/String;
    :goto_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$6;

    invoke-direct {v2, p0}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$6;-><init>(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    const v0, 0x7f0e01b7

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    new-instance v0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$5;-><init>(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;Lorg/namelessrom/devicecontrol/objects/Prop;Landroid/widget/EditText;Ljava/lang/String;Landroid/widget/TextView;)V

    invoke-virtual {v9, v10, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 320
    .end local v8    # "title":Ljava/lang/String;
    :cond_1
    const v0, 0x7f0e0017

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "title":Ljava/lang/String;
    goto :goto_1
.end method

.method private loadProp(Ljava/lang/String;)V
    .locals 14
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    const/16 v13, 0xa

    const/16 v12, 0x8

    const/4 v11, 0x0

    .line 264
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 265
    .local v1, "activity":Landroid/app/Activity;
    if-eqz v1, :cond_4

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_4

    .line 266
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mProps:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->clear()V

    .line 267
    const-string v9, " "

    invoke-virtual {p1, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 268
    .local v6, "p":[Ljava/lang/String;
    move-object v2, v6

    .local v2, "arr$":[Ljava/lang/String;
    array-length v4, v2

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_3

    aget-object v0, v2, v3

    .line 269
    .local v0, "aP":Ljava/lang/String;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_2

    .line 270
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 271
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    .line 272
    .local v5, "length":I
    if-lez v5, :cond_2

    .line 273
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 274
    .local v8, "pv":Ljava/lang/String;
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    .line 275
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 277
    :cond_0
    const-string v9, "/"

    const-string v10, "."

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    .line 278
    .local v7, "pn":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    if-le v9, v13, :cond_1

    .line 279
    invoke-virtual {v7, v13, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 281
    :cond_1
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mProps:Ljava/util/List;

    new-instance v10, Lorg/namelessrom/devicecontrol/objects/Prop;

    invoke-direct {v10, v7, v8}, Lorg/namelessrom/devicecontrol/objects/Prop;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 268
    .end local v5    # "length":I
    .end local v7    # "pn":Ljava/lang/String;
    .end local v8    # "pv":Ljava/lang/String;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 285
    .end local v0    # "aP":Ljava/lang/String;
    :cond_3
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mProps:Ljava/util/List;

    invoke-static {v9}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 286
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mLoadingView:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v12}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 287
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mProps:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 288
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mEmptyView:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 297
    .end local v2    # "arr$":[Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v6    # "p":[Ljava/lang/String;
    :cond_4
    :goto_1
    return-void

    .line 290
    .restart local v2    # "arr$":[Ljava/lang/String;
    .restart local v3    # "i$":I
    .restart local v4    # "len$":I
    .restart local v6    # "p":[Ljava/lang/String;
    :cond_5
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mEmptyView:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v12}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 291
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v9, v11}, Landroid/widget/ListView;->setVisibility(I)V

    .line 292
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mTools:Landroid/widget/RelativeLayout;

    invoke-virtual {v9, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 293
    new-instance v9, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    iget-object v10, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mProps:Ljava/util/List;

    invoke-direct {v9, v1, v10}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v9, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    .line 294
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mListView:Landroid/widget/ListView;

    iget-object v10, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_1
.end method

.method private makeApplyDialog()V
    .locals 4

    .prologue
    .line 169
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 170
    .local v0, "activity":Landroid/app/Activity;
    if-nez v0, :cond_0

    .line 200
    :goto_0
    return-void

    .line 172
    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0e0093

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e0094

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$4;

    invoke-direct {v3, p0}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$4;-><init>(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040013

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$3;

    invoke-direct {v3, p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$3;-><init>(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;Landroid/app/Activity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0
.end method


# virtual methods
.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 84
    const v0, 0x7f0e0561

    return v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 134
    const v0, 0x7f100001

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 136
    const v0, 0x7f0c0114

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 138
    invoke-super {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 139
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 99
    invoke-super {p0, p3}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;->onCreate(Landroid/os/Bundle;)V

    .line 100
    invoke-virtual {p0, v3}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->setHasOptionsMenu(Z)V

    .line 102
    const v1, 0x7f040054

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 104
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f0c0102

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mListView:Landroid/widget/ListView;

    .line 105
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 106
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 107
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setFastScrollAlwaysVisible(Z)V

    .line 109
    const v1, 0x7f0c00ff

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mLoadingView:Landroid/widget/LinearLayout;

    .line 110
    const v1, 0x7f0c0100

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mEmptyView:Landroid/widget/LinearLayout;

    .line 111
    const v1, 0x7f0c000b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mTools:Landroid/widget/RelativeLayout;

    .line 112
    const v1, 0x7f0c0103

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mFilter:Landroid/widget/EditText;

    .line 113
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mFilter:Landroid/widget/EditText;

    new-instance v2, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$2;

    invoke-direct {v2, p0}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$2;-><init>(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 128
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mTools:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 130
    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "row"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 162
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mAdapter:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    invoke-virtual {v1, p3}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->getItem(I)Lorg/namelessrom/devicecontrol/objects/Prop;

    move-result-object v0

    .line 163
    .local v0, "p":Lorg/namelessrom/devicecontrol/objects/Prop;
    if-eqz v0, :cond_0

    .line 164
    invoke-direct {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->editPropDialog(Lorg/namelessrom/devicecontrol/objects/Prop;)V

    .line 166
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 142
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 143
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    move v2, v3

    .line 155
    :goto_0
    return v2

    .line 145
    :pswitch_0
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->makeApplyDialog()V

    goto :goto_0

    .line 149
    :pswitch_1
    iget-boolean v1, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mLoadFull:Z

    if-nez v1, :cond_0

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->mLoadFull:Z

    .line 150
    new-instance v1, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$GetPropOperation;

    const/4 v4, 0x0

    invoke-direct {v1, p0, v4}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$GetPropOperation;-><init>(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$1;)V

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v1, v3}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$GetPropOperation;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_0
    move v1, v3

    .line 149
    goto :goto_1

    .line 143
    :pswitch_data_0
    .packed-switch 0x7f0c0115
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onShellOutput(Lorg/namelessrom/devicecontrol/objects/ShellOutput;)V
    .locals 2
    .param p1, "shellOutput"    # Lorg/namelessrom/devicecontrol/objects/ShellOutput;

    .prologue
    .line 241
    iget v0, p1, Lorg/namelessrom/devicecontrol/objects/ShellOutput;->id:I

    packed-switch v0, :pswitch_data_0

    .line 249
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onReadPropsCompleted: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lorg/namelessrom/devicecontrol/objects/ShellOutput;->output:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 250
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p1, Lorg/namelessrom/devicecontrol/objects/ShellOutput;->output:Ljava/lang/String;

    invoke-direct {p0, v0}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;->loadProp(Ljava/lang/String;)V

    .line 257
    :goto_0
    return-void

    .line 243
    :pswitch_0
    const-string v0, "/system"

    const-string v1, "ro"

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    :pswitch_1
    const-string v0, "busybox chmod 644 /system/etc/sysctl.conf;busybox sysctl -p /system/etc/sysctl.conf;"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;)V

    goto :goto_0

    .line 253
    :cond_0
    const-string v0, "Not attached!"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->w(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 241
    :pswitch_data_0
    .packed-switch 0xc8
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 87
    invoke-super {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 89
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$1;

    invoke-direct {v1, p0}, Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment$1;-><init>(Lorg/namelessrom/devicecontrol/editor/SysctlEditorFragment;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 95
    return-void
.end method

.method public showBurger()Z
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x0

    return v0
.end method
