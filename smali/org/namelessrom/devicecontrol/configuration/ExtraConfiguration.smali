.class public Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;
.super Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration;
.source "ExtraConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration",
        "<",
        "Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;",
        ">;"
    }
.end annotation


# static fields
.field private static sInstance:Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;


# instance fields
.field public migrationLevel:I

.field public rngStartup:Z

.field public uv:Ljava/lang/String;

.field public vdd:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration;-><init>()V

    .line 44
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->loadConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    .line 45
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->migrateFromDatabase(Landroid/content/Context;)Z

    .line 46
    return-void
.end method

.method public static get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    sget-object v0, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->sInstance:Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    invoke-direct {v0, p0}, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;-><init>(Landroid/content/Context;)V

    sput-object v0, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->sInstance:Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    .line 52
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->sInstance:Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    return-object v0
.end method


# virtual methods
.method protected getConfigurationFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    const-string v0, "extra_configuration.json"

    return-object v0
.end method

.method public loadConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 78
    const-class v1, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    invoke-virtual {p0, p1, v1}, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->loadRawConfiguration(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    .line 79
    .local v0, "config":Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;
    if-nez v0, :cond_0

    .line 90
    :goto_0
    return-object p0

    .line 83
    :cond_0
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->rngStartup:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->rngStartup:Z

    .line 85
    iget-object v1, v0, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->uv:Ljava/lang/String;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->uv:Ljava/lang/String;

    .line 86
    iget-object v1, v0, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->vdd:Ljava/lang/String;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->vdd:Ljava/lang/String;

    .line 88
    iget v1, v0, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->migrationLevel:I

    iput v1, p0, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->migrationLevel:I

    goto :goto_0
.end method

.method protected migrateFromDatabase(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 60
    iget v2, p0, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->migrationLevel:I

    if-ne v1, v2, :cond_0

    .line 61
    const-string v1, "already up to date :)"

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    :goto_0
    return v0

    .line 65
    :cond_0
    const-string v2, "rng_startup"

    invoke-static {v2, v0}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->rngStartup:Z

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->uv:Ljava/lang/String;

    .line 68
    const-string v0, ""

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->vdd:Ljava/lang/String;

    .line 71
    iput v1, p0, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->migrationLevel:I

    .line 73
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    move v0, v1

    .line 74
    goto :goto_0
.end method

.method public saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 94
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->saveConfigurationInternal(Landroid/content/Context;)V

    .line 95
    return-object p0
.end method
