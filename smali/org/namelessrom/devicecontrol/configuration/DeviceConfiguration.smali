.class public Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;
.super Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration;
.source "DeviceConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration",
        "<",
        "Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;",
        ">;"
    }
.end annotation


# static fields
.field private static sInstance:Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;


# instance fields
.field public darkTheme:Z

.field public dcFirstStart:Z

.field public extensiveLogging:Z

.field public migrationLevel:I

.field public monkey:Z

.field public perfCpuGovLock:Z

.field public perfCpuInfo:Z

.field public perfCpuLock:Z

.field public showLauncher:Z

.field public showPollfish:Z

.field public skipChecks:Z

.field public sobCpu:Z

.field public sobDevice:Z

.field public sobExtras:Z

.field public sobGpu:Z

.field public sobSysctl:Z

.field public sobVoltage:Z

.field public swipeOnContent:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 73
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration;-><init>()V

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->showLauncher:Z

    .line 74
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->loadConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    .line 75
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->migrateFromDatabase(Landroid/content/Context;)Z

    .line 76
    return-void
.end method

.method public static get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    sget-object v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sInstance:Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    invoke-direct {v0, p0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;-><init>(Landroid/content/Context;)V

    sput-object v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sInstance:Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    .line 82
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sInstance:Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    return-object v0
.end method


# virtual methods
.method protected getConfigurationFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    const-string v0, "device_configuration.json"

    return-object v0
.end method

.method public loadConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 125
    const-class v1, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    invoke-virtual {p0, p1, v1}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->loadRawConfiguration(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    .line 126
    .local v0, "config":Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;
    if-nez v0, :cond_0

    .line 155
    :goto_0
    return-object p0

    .line 130
    :cond_0
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->dcFirstStart:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->dcFirstStart:Z

    .line 131
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->swipeOnContent:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->swipeOnContent:Z

    .line 133
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobDevice:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobDevice:Z

    .line 134
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobCpu:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobCpu:Z

    .line 135
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobGpu:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobGpu:Z

    .line 136
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobExtras:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobExtras:Z

    .line 137
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobSysctl:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobSysctl:Z

    .line 138
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobVoltage:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobVoltage:Z

    .line 140
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->darkTheme:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->darkTheme:Z

    .line 141
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->showPollfish:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->showPollfish:Z

    .line 143
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->showLauncher:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->showLauncher:Z

    .line 144
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->skipChecks:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->skipChecks:Z

    .line 145
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->extensiveLogging:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->extensiveLogging:Z

    .line 147
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->perfCpuLock:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->perfCpuLock:Z

    .line 148
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->perfCpuGovLock:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->perfCpuGovLock:Z

    .line 149
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->perfCpuInfo:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->perfCpuInfo:Z

    .line 151
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->monkey:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->monkey:Z

    .line 153
    iget v1, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->migrationLevel:I

    iput v1, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->migrationLevel:I

    goto :goto_0
.end method

.method protected migrateFromDatabase(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 90
    iget v2, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->migrationLevel:I

    if-ne v1, v2, :cond_0

    .line 91
    const-string v1, "already up to date :)"

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    :goto_0
    return v0

    .line 95
    :cond_0
    const-string v2, "dc_first_start"

    invoke-static {v2, v0}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->dcFirstStart:Z

    .line 96
    const-string v2, "swipe_on_content"

    invoke-static {v2, v0}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->swipeOnContent:Z

    .line 98
    const-string v2, "sob_device"

    invoke-static {v2, v0}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobDevice:Z

    .line 99
    const-string v2, "sob_cpu"

    invoke-static {v2, v0}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobCpu:Z

    .line 100
    const-string v2, "sob_gpu"

    invoke-static {v2, v0}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobGpu:Z

    .line 101
    const-string v2, "sob_extras"

    invoke-static {v2, v0}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobExtras:Z

    .line 102
    const-string v2, "sob_sysctl"

    invoke-static {v2, v0}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobSysctl:Z

    .line 103
    const-string v2, "sob_voltage"

    invoke-static {v2, v0}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobVoltage:Z

    .line 105
    const-string v2, "dark_theme"

    invoke-static {v2, v0}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->darkTheme:Z

    .line 106
    const-string v2, "show_pollfish"

    invoke-static {v2, v0}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->showPollfish:Z

    .line 108
    const-string v2, "show_launcher"

    invoke-static {v2, v1}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->showLauncher:Z

    .line 109
    const-string v2, "skip_checks"

    invoke-static {v2, v0}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->skipChecks:Z

    .line 110
    const-string v2, "extensive_logging"

    invoke-static {v2, v0}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->extensiveLogging:Z

    .line 112
    const-string v2, "cpu_lock_freq"

    invoke-static {v2, v0}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->perfCpuLock:Z

    .line 113
    const-string v2, "pref_show_cpu_info"

    invoke-static {v2, v0}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->perfCpuInfo:Z

    .line 115
    const-string v2, "monkey"

    invoke-static {v2, v0}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->monkey:Z

    .line 118
    iput v1, p0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->migrationLevel:I

    .line 120
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move v0, v1

    .line 121
    goto/16 :goto_0
.end method

.method public saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 159
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->saveConfigurationInternal(Landroid/content/Context;)V

    .line 160
    return-object p0
.end method
