.class public Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;
.super Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration;
.source "TaskerConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration",
        "<",
        "Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;",
        ">;"
    }
.end annotation


# static fields
.field private static sInstance:Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;


# instance fields
.field public enabled:Z

.field public fstrimEnabled:Z

.field public fstrimInterval:I

.field public items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/namelessrom/devicecontrol/tasker/TaskerItem;",
            ">;"
        }
    .end annotation
.end field

.field public migrationLevel:I


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration;-><init>()V

    .line 44
    const/16 v0, 0x1e0

    iput v0, p0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->fstrimInterval:I

    .line 55
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->loadConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    .line 56
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->migrateFromDatabase(Landroid/content/Context;)Z

    .line 57
    return-void
.end method

.method public static get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    sget-object v0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->sInstance:Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    invoke-direct {v0, p0}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;-><init>(Landroid/content/Context;)V

    sput-object v0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->sInstance:Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    .line 63
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->sInstance:Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized addItem(Lorg/namelessrom/devicecontrol/tasker/TaskerItem;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;
    .locals 4
    .param p1, "taskerItem"    # Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    .prologue
    .line 132
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->items:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    const-string v0, "added item -> %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Lorg/namelessrom/devicecontrol/Logger;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    monitor-exit p0

    return-object p0

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public deleteItem(Lorg/namelessrom/devicecontrol/tasker/TaskerItem;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;
    .locals 6
    .param p1, "taskerItem"    # Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    .prologue
    .line 139
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->items:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 140
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/namelessrom/devicecontrol/tasker/TaskerItem;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 141
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    .line 142
    .local v0, "item":Lorg/namelessrom/devicecontrol/tasker/TaskerItem;
    invoke-virtual {p1, v0}, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 143
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 144
    const-string v2, "removed item -> %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p0, v2, v3}, Lorg/namelessrom/devicecontrol/Logger;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 148
    .end local v0    # "item":Lorg/namelessrom/devicecontrol/tasker/TaskerItem;
    :cond_1
    return-object p0
.end method

.method protected getConfigurationFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    const-string v0, "tasker_configuration.json"

    return-object v0
.end method

.method public getItemsByTrigger(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 4
    .param p1, "trigger"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/namelessrom/devicecontrol/tasker/TaskerItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 122
    .local v0, "filteredItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/namelessrom/devicecontrol/tasker/TaskerItem;>;"
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->items:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    .line 123
    .local v2, "item":Lorg/namelessrom/devicecontrol/tasker/TaskerItem;
    if-eqz v2, :cond_0

    iget-object v3, v2, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->trigger:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 124
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 128
    .end local v2    # "item":Lorg/namelessrom/devicecontrol/tasker/TaskerItem;
    :cond_1
    return-object v0
.end method

.method public loadConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 97
    const-class v1, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    invoke-virtual {p0, p1, v1}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->loadRawConfiguration(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    .line 98
    .local v0, "config":Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;
    if-nez v0, :cond_0

    .line 111
    :goto_0
    return-object p0

    .line 102
    :cond_0
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->enabled:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->enabled:Z

    .line 104
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->fstrimEnabled:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->fstrimEnabled:Z

    .line 105
    iget v1, v0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->fstrimInterval:I

    iput v1, p0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->fstrimInterval:I

    .line 107
    iget v1, v0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->migrationLevel:I

    iput v1, p0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->migrationLevel:I

    .line 109
    iget-object v1, v0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->items:Ljava/util/ArrayList;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->items:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method protected migrateFromDatabase(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 71
    iget v2, p0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->migrationLevel:I

    if-ne v4, v2, :cond_0

    .line 72
    const-string v1, "already up to date :)"

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    :goto_0
    return v0

    .line 76
    :cond_0
    iget v2, p0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->migrationLevel:I

    if-ge v2, v1, :cond_1

    .line 77
    const-string v2, "use_tasker"

    invoke-static {v2, v0}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->enabled:Z

    .line 79
    const-string v2, "fstrim"

    invoke-static {v2, v0}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->fstrimEnabled:Z

    .line 80
    const-string v0, "fstrim_interval"

    const/16 v2, 0x1e0

    invoke-static {v0, v2}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->fstrimInterval:I

    .line 82
    iput v1, p0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->migrationLevel:I

    .line 85
    :cond_1
    iget v0, p0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->migrationLevel:I

    if-ge v0, v4, :cond_2

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {}, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->getInstance()Lorg/namelessrom/devicecontrol/database/DatabaseHandler;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->getAllTaskerItems(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->items:Ljava/util/ArrayList;

    .line 89
    iput v4, p0, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->migrationLevel:I

    .line 92
    :cond_2
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move v0, v1

    .line 93
    goto :goto_0
.end method

.method public saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 115
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->saveConfigurationInternal(Landroid/content/Context;)V

    .line 116
    return-object p0
.end method
