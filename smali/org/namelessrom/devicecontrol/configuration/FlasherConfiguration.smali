.class public Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;
.super Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration;
.source "FlasherConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration",
        "<",
        "Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;",
        ">;"
    }
.end annotation


# static fields
.field private static sInstance:Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;


# instance fields
.field public migrationLevel:I

.field public recoveryType:I


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration;-><init>()V

    .line 44
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->loadConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;

    .line 45
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->migrateFromDatabase(Landroid/content/Context;)Z

    .line 46
    return-void
.end method

.method public static get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    sget-object v0, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->sInstance:Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;

    invoke-direct {v0, p0}, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;-><init>(Landroid/content/Context;)V

    sput-object v0, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->sInstance:Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;

    .line 52
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->sInstance:Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;

    return-object v0
.end method


# virtual methods
.method protected getConfigurationFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    const-string v0, "flasher_configuration.json"

    return-object v0
.end method

.method public loadConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 75
    const-class v1, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;

    invoke-virtual {p0, p1, v1}, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->loadRawConfiguration(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;

    .line 77
    .local v0, "config":Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;
    if-nez v0, :cond_0

    .line 85
    :goto_0
    return-object p0

    .line 81
    :cond_0
    iget v1, v0, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->recoveryType:I

    iput v1, p0, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->recoveryType:I

    .line 83
    iget v1, v0, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->migrationLevel:I

    iput v1, p0, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->migrationLevel:I

    goto :goto_0
.end method

.method protected migrateFromDatabase(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 60
    iget v1, p0, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->migrationLevel:I

    if-ne v0, v1, :cond_0

    .line 61
    const-string v0, "already up to date :)"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    const/4 v0, 0x0

    .line 71
    :goto_0
    return v0

    .line 65
    :cond_0
    const-string v1, "pref_recovery_type"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->recoveryType:I

    .line 68
    iput v0, p0, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->migrationLevel:I

    .line 70
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;

    goto :goto_0
.end method

.method public saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 89
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->saveConfigurationInternal(Landroid/content/Context;)V

    .line 90
    return-object p0
.end method
