.class public Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;
.super Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration;
.source "BootupConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration",
        "<",
        "Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;",
        ">;"
    }
.end annotation


# static fields
.field private static sInstance:Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;


# instance fields
.field public items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/namelessrom/devicecontrol/objects/BootupItem;",
            ">;"
        }
    .end annotation
.end field

.field public migrationLevel:I


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration;-><init>()V

    .line 46
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->loadConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 47
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->migrateFromDatabase(Landroid/content/Context;)Z

    .line 48
    return-void
.end method

.method public static get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    sget-object v0, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->sInstance:Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    invoke-direct {v0, p0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;-><init>(Landroid/content/Context;)V

    sput-object v0, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->sInstance:Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 54
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->sInstance:Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    return-object v0
.end method

.method public static declared-synchronized setBootup(Landroid/content/Context;Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "item"    # Lorg/namelessrom/devicecontrol/objects/BootupItem;

    .prologue
    .line 149
    const-class v2, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    monitor-enter v2

    :try_start_0
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    move-result-object v0

    .line 150
    .local v0, "config":Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;
    invoke-virtual {v0, p1}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->deleteItem(Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 151
    invoke-virtual {v0, p1}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->addItem(Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 152
    invoke-virtual {v0, p0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    monitor-exit v2

    return-object v0

    .line 149
    .end local v0    # "config":Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method


# virtual methods
.method public declared-synchronized addItem(Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;
    .locals 4
    .param p1, "bootupItem"    # Lorg/namelessrom/devicecontrol/objects/BootupItem;

    .prologue
    .line 127
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->deleteItem(Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 128
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->items:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    const-string v0, "added item -> %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/objects/BootupItem;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Lorg/namelessrom/devicecontrol/Logger;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    monitor-exit p0

    return-object p0

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public deleteItem(Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;
    .locals 6
    .param p1, "bootupItem"    # Lorg/namelessrom/devicecontrol/objects/BootupItem;

    .prologue
    .line 135
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->items:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 136
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/namelessrom/devicecontrol/objects/BootupItem;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 137
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    .line 138
    .local v0, "item":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    invoke-virtual {p1, v0}, Lorg/namelessrom/devicecontrol/objects/BootupItem;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 139
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 140
    const-string v2, "removed item -> %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/objects/BootupItem;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p0, v2, v3}, Lorg/namelessrom/devicecontrol/Logger;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 144
    .end local v0    # "item":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    :cond_1
    return-object p0
.end method

.method protected getConfigurationFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    const-string v0, "bootup_configuration.json"

    return-object v0
.end method

.method public getItemByName(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/objects/BootupItem;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 117
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->items:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    .line 118
    .local v1, "item":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    if-eqz v1, :cond_0

    iget-object v2, v1, Lorg/namelessrom/devicecontrol/objects/BootupItem;->name:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 123
    .end local v1    # "item":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getItemsByCategory(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 4
    .param p1, "category"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/namelessrom/devicecontrol/objects/BootupItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 107
    .local v0, "filteredItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/namelessrom/devicecontrol/objects/BootupItem;>;"
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->items:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    .line 108
    .local v2, "item":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    if-eqz v2, :cond_0

    iget-object v3, v2, Lorg/namelessrom/devicecontrol/objects/BootupItem;->category:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 109
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 113
    .end local v2    # "item":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    :cond_1
    return-object v0
.end method

.method public loadConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    const-class v1, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    invoke-virtual {p0, p1, v1}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->loadRawConfiguration(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 88
    .local v0, "config":Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;
    if-nez v0, :cond_0

    .line 96
    :goto_0
    return-object p0

    .line 92
    :cond_0
    iget-object v1, v0, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->items:Ljava/util/ArrayList;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->items:Ljava/util/ArrayList;

    .line 94
    iget v1, v0, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->migrationLevel:I

    iput v1, p0, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->migrationLevel:I

    goto :goto_0
.end method

.method protected migrateFromDatabase(Landroid/content/Context;)Z
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x1

    .line 62
    iget v0, p0, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->migrationLevel:I

    if-ne v9, v0, :cond_0

    .line 63
    const-string v0, "already up to date :)"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    const/4 v0, 0x0

    .line 83
    :goto_0
    return v0

    .line 67
    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-static {}, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->getInstance()Lorg/namelessrom/devicecontrol/database/DatabaseHandler;

    move-result-object v0

    const-string v1, "boot_up"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lorg/namelessrom/devicecontrol/database/DatabaseHandler;->getAllItems(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 70
    .local v6, "bootupItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/namelessrom/devicecontrol/database/DataItem;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->items:Ljava/util/ArrayList;

    .line 72
    monitor-enter p0

    .line 73
    :try_start_0
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/namelessrom/devicecontrol/database/DataItem;

    .line 74
    .local v8, "item":Lorg/namelessrom/devicecontrol/database/DataItem;
    iget-object v10, p0, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->items:Ljava/util/ArrayList;

    new-instance v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    invoke-virtual {v8}, Lorg/namelessrom/devicecontrol/database/DataItem;->getCategory()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8}, Lorg/namelessrom/devicecontrol/database/DataItem;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8}, Lorg/namelessrom/devicecontrol/database/DataItem;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8}, Lorg/namelessrom/devicecontrol/database/DataItem;->getValue()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/objects/BootupItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 77
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "item":Lorg/namelessrom/devicecontrol/database/DataItem;
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .restart local v7    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80
    iput v9, p0, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->migrationLevel:I

    .line 82
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    move v0, v9

    .line 83
    goto :goto_0
.end method

.method public saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 100
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->saveConfigurationInternal(Landroid/content/Context;)V

    .line 101
    return-object p0
.end method
