.class public abstract Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration;
.super Ljava/lang/Object;
.source "BaseConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    .local p0, "this":Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration;, "Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract getConfigurationFile()Ljava/lang/String;
.end method

.method protected loadRawConfiguration(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration;, "Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration<TT;>;"
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const/4 v4, 0x0

    .line 54
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, "base":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration;->getConfigurationFile()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .local v2, "configurationFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 58
    const-string v5, "config does not exist, return"

    invoke-static {p0, v5}, Lorg/namelessrom/devicecontrol/Logger;->w(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    :goto_0
    return-object v4

    .line 62
    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/namelessrom/devicecontrol/utils/Utils;->readFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 63
    .local v1, "config":Ljava/lang/String;
    const-string v5, "config -> %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    invoke-static {p0, v5, v6}, Lorg/namelessrom/devicecontrol/Logger;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 65
    const-string v5, "config is empty or null, return"

    invoke-static {p0, v5}, Lorg/namelessrom/devicecontrol/Logger;->w(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 70
    :cond_1
    :try_start_0
    new-instance v5, Lcom/google/gson/Gson;

    invoke-direct {v5}, Lcom/google/gson/Gson;-><init>()V

    invoke-virtual {v5, v1, p2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 71
    :catch_0
    move-exception v3

    .line 72
    .local v3, "jse":Lcom/google/gson/JsonSyntaxException;
    const-string v5, "could not convert file into configuration"

    invoke-static {p0, v5, v3}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method protected abstract migrateFromDatabase(Landroid/content/Context;)Z
.end method

.method protected saveConfigurationInternal(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .local p0, "this":Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration;, "Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration<TT;>;"
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, "base":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration;->getConfigurationFile()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .local v2, "configurationFile":Ljava/io/File;
    const-string v3, "deleting configuration before writing -> %s"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {p0, v3, v4}, Lorg/namelessrom/devicecontrol/Logger;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 48
    new-instance v3, Lcom/google/gson/Gson;

    invoke-direct {v3}, Lcom/google/gson/Gson;-><init>()V

    invoke-virtual {v3, p0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 49
    .local v1, "config":Ljava/lang/String;
    const-string v3, "config -> %s"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-static {p0, v3, v4}, Lorg/namelessrom/devicecontrol/Logger;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 50
    invoke-static {v2, v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->writeToFile(Ljava/io/File;Ljava/lang/String;)Z

    .line 51
    return-void
.end method
