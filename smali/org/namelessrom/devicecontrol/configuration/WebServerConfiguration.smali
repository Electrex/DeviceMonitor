.class public Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;
.super Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration;
.source "WebServerConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration",
        "<",
        "Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;",
        ">;"
    }
.end annotation


# static fields
.field private static sInstance:Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;


# instance fields
.field public migrationLevel:I

.field public password:Ljava/lang/String;

.field public port:I

.field public root:Z

.field public useAuth:Z

.field public username:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/configuration/BaseConfiguration;-><init>()V

    .line 38
    const/16 v0, 0x1f90

    iput v0, p0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->port:I

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->useAuth:Z

    .line 41
    const-string v0, "root"

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->username:Ljava/lang/String;

    .line 42
    const-string v0, "toor"

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->password:Ljava/lang/String;

    .line 51
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->loadConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    .line 52
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->migrateFromDatabase(Landroid/content/Context;)Z

    .line 53
    return-void
.end method

.method public static get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    sget-object v0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->sInstance:Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    invoke-direct {v0, p0}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;-><init>(Landroid/content/Context;)V

    sput-object v0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->sInstance:Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    .line 59
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->sInstance:Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    return-object v0
.end method


# virtual methods
.method protected getConfigurationFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    const-string v0, "webserver_configuration.json"

    return-object v0
.end method

.method public loadConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    const-class v1, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    invoke-virtual {p0, p1, v1}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->loadRawConfiguration(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    .line 89
    .local v0, "config":Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;
    if-nez v0, :cond_0

    .line 102
    :goto_0
    return-object p0

    .line 93
    :cond_0
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->root:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->root:Z

    .line 94
    iget v1, v0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->port:I

    iput v1, p0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->port:I

    .line 96
    iget-boolean v1, v0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->useAuth:Z

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->useAuth:Z

    .line 97
    iget-object v1, v0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->username:Ljava/lang/String;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->username:Ljava/lang/String;

    .line 98
    iget-object v1, v0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->password:Ljava/lang/String;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->password:Ljava/lang/String;

    .line 100
    iget v1, v0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->migrationLevel:I

    iput v1, p0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->migrationLevel:I

    goto :goto_0
.end method

.method protected migrateFromDatabase(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 67
    iget v2, p0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->migrationLevel:I

    if-ne v1, v2, :cond_0

    .line 68
    const-string v1, "already up to date :)"

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    :goto_0
    return v0

    .line 72
    :cond_0
    const-string v2, "wfm_root"

    invoke-static {v2, v0}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->root:Z

    .line 73
    const-string v0, "wfm_port"

    const/16 v2, 0x1f90

    invoke-static {v0, v2}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->port:I

    .line 75
    const-string v0, "wfm_auth"

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/PreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->useAuth:Z

    .line 76
    const-string v0, "root"

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->username:Ljava/lang/String;

    .line 77
    const-string v0, "toor"

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->password:Ljava/lang/String;

    .line 80
    iput v1, p0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->migrationLevel:I

    .line 82
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    move v0, v1

    .line 83
    goto :goto_0
.end method

.method public saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 106
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->saveConfigurationInternal(Landroid/content/Context;)V

    .line 107
    return-object p0
.end method
