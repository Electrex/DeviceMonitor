.class Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;
.super Ljava/lang/Object;
.source "RebootHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/flasher/RebootHelper;->reallyShowBackupDialog(Landroid/content/Context;[Ljava/lang/String;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/flasher/RebootHelper;

.field final synthetic val$cbBoot:Landroid/widget/CheckBox;

.field final synthetic val$cbCache:Landroid/widget/CheckBox;

.field final synthetic val$cbData:Landroid/widget/CheckBox;

.field final synthetic val$cbRecovery:Landroid/widget/CheckBox;

.field final synthetic val$cbSdext:Landroid/widget/CheckBox;

.field final synthetic val$cbSecure:Landroid/widget/CheckBox;

.field final synthetic val$cbSystem:Landroid/widget/CheckBox;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$input:Landroid/widget/EditText;

.field final synthetic val$items:[Ljava/lang/String;

.field final synthetic val$wipeCaches:Z

.field final synthetic val$wipeData:Z


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/flasher/RebootHelper;Landroid/widget/EditText;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/content/Context;[Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->this$0:Lorg/namelessrom/devicecontrol/flasher/RebootHelper;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$input:Landroid/widget/EditText;

    iput-object p3, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$cbSystem:Landroid/widget/CheckBox;

    iput-object p4, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$cbData:Landroid/widget/CheckBox;

    iput-object p5, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$cbCache:Landroid/widget/CheckBox;

    iput-object p6, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$cbRecovery:Landroid/widget/CheckBox;

    iput-object p7, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$cbBoot:Landroid/widget/CheckBox;

    iput-object p8, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$cbSecure:Landroid/widget/CheckBox;

    iput-object p9, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$cbSdext:Landroid/widget/CheckBox;

    iput-object p10, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$context:Landroid/content/Context;

    iput-object p11, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$items:[Ljava/lang/String;

    iput-boolean p12, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$wipeData:Z

    iput-boolean p13, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$wipeCaches:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 111
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 113
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$input:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 114
    .local v5, "text":Ljava/lang/String;
    const-string v0, "[^a-zA-Z0-9.-]"

    const-string v1, ""

    invoke-virtual {v5, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 116
    const-string v6, ""

    .line 117
    .local v6, "backupOptions":Ljava/lang/String;
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$cbSystem:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "S"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 120
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$cbData:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "D"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 123
    :cond_1
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$cbCache:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "C"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 126
    :cond_2
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$cbRecovery:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 127
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "R"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 129
    :cond_3
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$cbBoot:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "B"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 132
    :cond_4
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$cbSecure:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "A"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 135
    :cond_5
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$cbSdext:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "E"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 139
    :cond_6
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->this$0:Lorg/namelessrom/devicecontrol/flasher/RebootHelper;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$items:[Ljava/lang/String;

    iget-boolean v3, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$wipeData:Z

    iget-boolean v4, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;->val$wipeCaches:Z

    # invokes: Lorg/namelessrom/devicecontrol/flasher/RebootHelper;->reboot(Landroid/content/Context;[Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V
    invoke-static/range {v0 .. v6}, Lorg/namelessrom/devicecontrol/flasher/RebootHelper;->access$100(Lorg/namelessrom/devicecontrol/flasher/RebootHelper;Landroid/content/Context;[Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    .line 140
    return-void
.end method
