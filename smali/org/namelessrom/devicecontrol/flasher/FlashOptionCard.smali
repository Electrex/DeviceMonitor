.class public Lorg/namelessrom/devicecontrol/flasher/FlashOptionCard;
.super Lorg/namelessrom/devicecontrol/flasher/BaseCard;
.source "FlashOptionCard.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/flasher/BaseCard;-><init>(Landroid/content/Context;)V

    .line 31
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f04003f

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/flasher/FlashOptionCard;->getContainer()Landroid/widget/FrameLayout;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 34
    invoke-static {p1}, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;

    move-result-object v2

    iget v1, v2, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->recoveryType:I

    .line 35
    .local v1, "recoveryType":I
    const v2, 0x7f0c00cb

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/flasher/FlashOptionCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 36
    .local v0, "radioGroup":Landroid/widget/RadioGroup;
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const v2, 0x7f0c00cd

    :goto_0
    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->check(I)V

    .line 38
    new-instance v2, Lorg/namelessrom/devicecontrol/flasher/FlashOptionCard$1;

    invoke-direct {v2, p0, p1}, Lorg/namelessrom/devicecontrol/flasher/FlashOptionCard$1;-><init>(Lorg/namelessrom/devicecontrol/flasher/FlashOptionCard;Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 57
    return-void

    .line 36
    :cond_0
    const v2, 0x7f0c00cc

    goto :goto_0
.end method
