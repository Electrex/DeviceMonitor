.class public Lorg/namelessrom/devicecontrol/flasher/RebootHelper;
.super Ljava/lang/Object;
.source "RebootHelper.java"


# instance fields
.field private mRecoveryHelper:Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;


# direct methods
.method public constructor <init>(Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;)V
    .locals 0
    .param p1, "recoveryHelper"    # Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper;->mRecoveryHelper:Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;

    .line 47
    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/flasher/RebootHelper;Landroid/content/Context;[Ljava/lang/String;ZZ)V
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/flasher/RebootHelper;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # [Ljava/lang/String;
    .param p3, "x3"    # Z
    .param p4, "x4"    # Z

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/namelessrom/devicecontrol/flasher/RebootHelper;->reallyShowBackupDialog(Landroid/content/Context;[Ljava/lang/String;ZZ)V

    return-void
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/flasher/RebootHelper;Landroid/content/Context;[Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/flasher/RebootHelper;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # [Ljava/lang/String;
    .param p3, "x3"    # Z
    .param p4, "x4"    # Z
    .param p5, "x5"    # Ljava/lang/String;
    .param p6, "x6"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-direct/range {p0 .. p6}, Lorg/namelessrom/devicecontrol/flasher/RebootHelper;->reboot(Landroid/content/Context;[Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lorg/namelessrom/devicecontrol/flasher/RebootHelper;Landroid/content/Context;[Ljava/lang/String;ZZ)V
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/flasher/RebootHelper;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # [Ljava/lang/String;
    .param p3, "x3"    # Z
    .param p4, "x4"    # Z

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/namelessrom/devicecontrol/flasher/RebootHelper;->showBackupDialog(Landroid/content/Context;[Ljava/lang/String;ZZ)V

    return-void
.end method

.method private reallyShowBackupDialog(Landroid/content/Context;[Ljava/lang/String;ZZ)V
    .locals 18
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "wipeData"    # Z
    .param p4, "wipeCaches"    # Z

    .prologue
    .line 83
    new-instance v15, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p1

    invoke-direct {v15, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 84
    .local v15, "alert":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0e001c

    invoke-virtual {v15, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 85
    invoke-static/range {p1 .. p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v11, 0x7f040026

    move-object/from16 v1, p1

    check-cast v1, Landroid/app/Activity;

    const v12, 0x7f0c007f

    invoke-virtual {v1, v12}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v2, v11, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v16

    .line 87
    .local v16, "view":Landroid/view/View;
    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 89
    const v1, 0x7f0c0081

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 90
    .local v4, "cbSystem":Landroid/widget/CheckBox;
    const v1, 0x7f0c0082

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    .line 91
    .local v5, "cbData":Landroid/widget/CheckBox;
    const v1, 0x7f0c0083

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    .line 92
    .local v6, "cbCache":Landroid/widget/CheckBox;
    const v1, 0x7f0c0084

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    .line 93
    .local v7, "cbRecovery":Landroid/widget/CheckBox;
    const v1, 0x7f0c0085

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/CheckBox;

    .line 94
    .local v8, "cbBoot":Landroid/widget/CheckBox;
    const v1, 0x7f0c0086

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/CheckBox;

    .line 95
    .local v9, "cbSecure":Landroid/widget/CheckBox;
    const v1, 0x7f0c0087

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/CheckBox;

    .line 96
    .local v10, "cbSdext":Landroid/widget/CheckBox;
    const v1, 0x7f0c0088

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 98
    .local v3, "input":Landroid/widget/EditText;
    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/Utils;->getDateAndTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 99
    invoke-virtual {v3}, Landroid/widget/EditText;->selectAll()V

    .line 101
    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->get()Lorg/namelessrom/devicecontrol/utils/IOUtils;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->hasAndroidSecure()Z

    move-result v1

    if-nez v1, :cond_0

    .line 102
    const/16 v1, 0x8

    invoke-virtual {v9, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 104
    :cond_0
    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->get()Lorg/namelessrom/devicecontrol/utils/IOUtils;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->hasSdExt()Z

    move-result v1

    if-nez v1, :cond_1

    .line 105
    const/16 v1, 0x8

    invoke-virtual {v10, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 108
    :cond_1
    const v17, 0x104000a

    new-instance v1, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;

    move-object/from16 v2, p0

    move-object/from16 v11, p1

    move-object/from16 v12, p2

    move/from16 v13, p3

    move/from16 v14, p4

    invoke-direct/range {v1 .. v14}, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$3;-><init>(Lorg/namelessrom/devicecontrol/flasher/RebootHelper;Landroid/widget/EditText;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/content/Context;[Ljava/lang/String;ZZ)V

    move/from16 v0, v17

    invoke-virtual {v15, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 143
    const/high16 v1, 0x1040000

    new-instance v2, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$4;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$4;-><init>(Lorg/namelessrom/devicecontrol/flasher/RebootHelper;)V

    invoke-virtual {v15, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 149
    invoke-virtual {v15}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 150
    return-void
.end method

.method private reboot(Landroid/content/Context;[Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 24
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "wipeData"    # Z
    .param p4, "wipeCaches"    # Z
    .param p5, "backupFolder"    # Ljava/lang/String;
    .param p6, "backupOptions"    # Ljava/lang/String;

    .prologue
    .line 193
    invoke-static/range {p1 .. p1}, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;

    move-result-object v2

    iget v15, v2, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->recoveryType:I

    .line 194
    .local v15, "flashType":I
    const/4 v2, 0x1

    if-ne v2, v15, :cond_0

    .line 195
    const/4 v2, 0x1

    new-array v0, v2, [I

    move-object/from16 v21, v0

    const/4 v2, 0x0

    const/4 v5, 0x0

    aput v5, v21, v2

    .line 203
    .local v21, "recoveries":[I
    :goto_0
    move-object/from16 v9, v21

    .local v9, "arr$":[I
    array-length v0, v9

    move/from16 v19, v0

    .local v19, "len$":I
    const/16 v17, 0x0

    .local v17, "i$":I
    move/from16 v18, v17

    .end local v9    # "arr$":[I
    .end local v17    # "i$":I
    .end local v19    # "len$":I
    .local v18, "i$":I
    :goto_1
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_4

    aget v3, v9, v18

    .line 204
    .local v3, "recovery":I
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    .line 205
    .local v23, "sb":Ljava/lang/StringBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/cache/recovery/"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper;->mRecoveryHelper:Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;

    invoke-virtual {v5, v3}, Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;->getCommandsFile(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 207
    .local v14, "file":Ljava/lang/String;
    move-object/from16 v0, p2

    array-length v2, v0

    new-array v4, v2, [Ljava/lang/String;

    .line 208
    .local v4, "files":[Ljava/lang/String;
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_2
    array-length v2, v4

    move/from16 v0, v16

    if-ge v0, v2, :cond_2

    .line 209
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper;->mRecoveryHelper:Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;

    aget-object v5, p2, v16

    invoke-virtual {v2, v3, v5}, Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;->getRecoveryFilePath(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v16

    .line 208
    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    .line 196
    .end local v3    # "recovery":I
    .end local v4    # "files":[Ljava/lang/String;
    .end local v14    # "file":Ljava/lang/String;
    .end local v16    # "i":I
    .end local v18    # "i$":I
    .end local v21    # "recoveries":[I
    .end local v23    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    const/4 v2, 0x2

    if-ne v2, v15, :cond_1

    .line 197
    const/4 v2, 0x1

    new-array v0, v2, [I

    move-object/from16 v21, v0

    const/4 v2, 0x0

    const/4 v5, 0x1

    aput v5, v21, v2

    .restart local v21    # "recoveries":[I
    goto :goto_0

    .line 199
    .end local v21    # "recoveries":[I
    :cond_1
    const/4 v2, 0x2

    new-array v0, v2, [I

    move-object/from16 v21, v0

    fill-array-data v21, :array_0

    .restart local v21    # "recoveries":[I
    goto :goto_0

    .line 212
    .restart local v3    # "recovery":I
    .restart local v4    # "files":[Ljava/lang/String;
    .restart local v14    # "file":Ljava/lang/String;
    .restart local v16    # "i":I
    .restart local v18    # "i$":I
    .restart local v23    # "sb":Ljava/lang/StringBuilder;
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper;->mRecoveryHelper:Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;

    move/from16 v5, p3

    move/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-virtual/range {v2 .. v8}, Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;->getCommands(I[Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 214
    .local v12, "commands":[Ljava/lang/String;
    if-eqz v12, :cond_3

    .line 215
    move-object v10, v12

    .local v10, "arr$":[Ljava/lang/String;
    array-length v0, v10

    move/from16 v20, v0

    .local v20, "len$":I
    const/16 v17, 0x0

    .end local v18    # "i$":I
    .restart local v17    # "i$":I
    :goto_3
    move/from16 v0, v17

    move/from16 v1, v20

    if-ge v0, v1, :cond_3

    aget-object v22, v10, v17

    .line 216
    .local v22, "s":Ljava/lang/String;
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "\n"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    add-int/lit8 v17, v17, 0x1

    goto :goto_3

    .line 220
    .end local v10    # "arr$":[Ljava/lang/String;
    .end local v17    # "i$":I
    .end local v20    # "len$":I
    .end local v22    # "s":Ljava/lang/String;
    :cond_3
    const-string v2, "mkdir -p /cache/recovery/;\necho \'%s\' > \'%s\';\n"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v14, v5, v6

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 222
    .local v11, "cmd":Ljava/lang/String;
    const/4 v2, 0x1

    invoke-static {v11, v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;Z)V

    .line 223
    const-string v2, "0644"

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    const/16 v6, 0x7d1

    invoke-static {v14, v2, v5, v6}, Lorg/namelessrom/devicecontrol/utils/Utils;->setPermissions(Ljava/lang/String;Ljava/lang/String;II)Ljava/lang/String;

    .line 203
    add-int/lit8 v17, v18, 0x1

    .restart local v17    # "i$":I
    move/from16 v18, v17

    .end local v17    # "i$":I
    .restart local v18    # "i$":I
    goto/16 :goto_1

    .line 227
    .end local v3    # "recovery":I
    .end local v4    # "files":[Ljava/lang/String;
    .end local v11    # "cmd":Ljava/lang/String;
    .end local v12    # "commands":[Ljava/lang/String;
    .end local v14    # "file":Ljava/lang/String;
    .end local v16    # "i":I
    .end local v23    # "sb":Ljava/lang/StringBuilder;
    :cond_4
    :try_start_0
    const-string v2, "power"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    const-string v5, "recovery"

    invoke-virtual {v2, v5}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    :goto_4
    return-void

    .line 228
    :catch_0
    move-exception v13

    .line 229
    .local v13, "exc":Ljava/lang/Exception;
    const-string v2, "can not reboot using power manager"

    move-object/from16 v0, p0

    invoke-static {v0, v2, v13}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 230
    const-string v2, "reboot recovery;\n"

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;)V

    goto :goto_4

    .line 199
    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method private showBackupDialog(Landroid/content/Context;[Ljava/lang/String;ZZ)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "wipeData"    # Z
    .param p4, "wipeCaches"    # Z

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 51
    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->get()Lorg/namelessrom/devicecontrol/utils/IOUtils;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->getSpaceLeft()D

    move-result-wide v8

    .line 52
    .local v8, "spaceLeft":D
    cmpg-double v0, v8, v4

    if-gez v0, :cond_0

    .line 53
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 54
    .local v6, "alert":Landroid/app/AlertDialog$Builder;
    const v0, 0x7f0e001b

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 55
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e001a

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 58
    const v7, 0x104000a

    new-instance v0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$1;-><init>(Lorg/namelessrom/devicecontrol/flasher/RebootHelper;Landroid/content/Context;[Ljava/lang/String;ZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 67
    const/high16 v0, 0x1040000

    new-instance v1, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$2;

    invoke-direct {v1, p0}, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$2;-><init>(Lorg/namelessrom/devicecontrol/flasher/RebootHelper;)V

    invoke-virtual {v6, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 74
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 78
    .end local v6    # "alert":Landroid/app/AlertDialog$Builder;
    :goto_0
    return-void

    .line 76
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/namelessrom/devicecontrol/flasher/RebootHelper;->reallyShowBackupDialog(Landroid/content/Context;[Ljava/lang/String;ZZ)V

    goto :goto_0
.end method


# virtual methods
.method public showRebootDialog(Landroid/content/Context;[Ljava/lang/String;ZZZ)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "backup"    # Z
    .param p4, "wipeData"    # Z
    .param p5, "wipeCaches"    # Z

    .prologue
    .line 155
    if-eqz p2, :cond_0

    array-length v0, p2

    if-nez v0, :cond_1

    .line 188
    :cond_0
    :goto_0
    return-void

    .line 159
    :cond_1
    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-direct {v7, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 160
    .local v7, "alert":Landroid/app/AlertDialog$Builder;
    const v0, 0x7f0e001d

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 162
    array-length v0, p2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 163
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e001f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 168
    :goto_1
    const v8, 0x7f0e013e

    new-instance v0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$5;

    move-object v1, p0

    move v2, p3

    move-object v3, p1

    move-object v4, p2

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$5;-><init>(Lorg/namelessrom/devicecontrol/flasher/RebootHelper;ZLandroid/content/Context;[Ljava/lang/String;ZZ)V

    invoke-virtual {v7, v8, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 180
    const/high16 v0, 0x1040000

    new-instance v1, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$6;

    invoke-direct {v1, p0}, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$6;-><init>(Lorg/namelessrom/devicecontrol/flasher/RebootHelper;)V

    invoke-virtual {v7, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 187
    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 165
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e001e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method
