.class public Lorg/namelessrom/devicecontrol/flasher/FlashCard;
.super Lorg/namelessrom/devicecontrol/flasher/BaseCard;
.source "FlashCard.java"


# instance fields
.field public install:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "flasherFragment"    # Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/flasher/BaseCard;-><init>(Landroid/content/Context;)V

    .line 33
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04003e

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/flasher/FlashCard;->getContainer()Landroid/widget/FrameLayout;

    move-result-object v2

    const/4 v8, 0x1

    invoke-virtual {v0, v1, v2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 36
    new-instance v7, Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;

    invoke-virtual {p2}, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v7, v0}, Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;-><init>(Landroid/content/Context;)V

    .line 37
    .local v7, "recoveryHelper":Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;
    new-instance v3, Lorg/namelessrom/devicecontrol/flasher/RebootHelper;

    invoke-direct {v3, v7}, Lorg/namelessrom/devicecontrol/flasher/RebootHelper;-><init>(Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;)V

    .line 39
    .local v3, "rebootHelper":Lorg/namelessrom/devicecontrol/flasher/RebootHelper;
    const v0, 0x7f0c00ca

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/flasher/FlashCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/FlashCard;->install:Landroid/widget/Button;

    .line 40
    const v0, 0x7f0c00c7

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/flasher/FlashCard;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 41
    .local v4, "backup":Landroid/widget/CheckBox;
    const v0, 0x7f0c00c9

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/flasher/FlashCard;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    .line 42
    .local v6, "wipeCaches":Landroid/widget/CheckBox;
    const v0, 0x7f0c00c8

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/flasher/FlashCard;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    .line 44
    .local v5, "wipeData":Landroid/widget/CheckBox;
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/FlashCard;->install:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 45
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/flasher/FlashCard;->install:Landroid/widget/Button;

    new-instance v0, Lorg/namelessrom/devicecontrol/flasher/FlashCard$1;

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lorg/namelessrom/devicecontrol/flasher/FlashCard$1;-><init>(Lorg/namelessrom/devicecontrol/flasher/FlashCard;Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;Lorg/namelessrom/devicecontrol/flasher/RebootHelper;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;)V

    invoke-virtual {v8, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    return-void
.end method
