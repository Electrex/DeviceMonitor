.class Lorg/namelessrom/devicecontrol/flasher/FlashOptionCard$1;
.super Ljava/lang/Object;
.source "FlashOptionCard.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/flasher/FlashOptionCard;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/flasher/FlashOptionCard;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/flasher/FlashOptionCard;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/flasher/FlashOptionCard$1;->this$0:Lorg/namelessrom/devicecontrol/flasher/FlashOptionCard;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/flasher/FlashOptionCard$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 3
    .param p1, "group"    # Landroid/widget/RadioGroup;
    .param p2, "checkedId"    # I

    .prologue
    .line 40
    invoke-virtual {p1}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    .line 41
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    .line 55
    :goto_0
    return-void

    .line 43
    :pswitch_0
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/flasher/FlashOptionCard$1;->val$context:Landroid/content/Context;

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;

    move-result-object v1

    const/4 v2, 0x1

    iput v2, v1, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->recoveryType:I

    .line 45
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/flasher/FlashOptionCard$1;->val$context:Landroid/content/Context;

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;

    move-result-object v1

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/flasher/FlashOptionCard$1;->val$context:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;

    goto :goto_0

    .line 49
    :pswitch_1
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/flasher/FlashOptionCard$1;->val$context:Landroid/content/Context;

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;

    move-result-object v1

    const/4 v2, 0x2

    iput v2, v1, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->recoveryType:I

    .line 51
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/flasher/FlashOptionCard$1;->val$context:Landroid/content/Context;

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;

    move-result-object v1

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/flasher/FlashOptionCard$1;->val$context:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/FlasherConfiguration;

    goto :goto_0

    .line 41
    :pswitch_data_0
    .packed-switch 0x7f0c00cc
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
