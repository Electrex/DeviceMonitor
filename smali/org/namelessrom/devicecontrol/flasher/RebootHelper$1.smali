.class Lorg/namelessrom/devicecontrol/flasher/RebootHelper$1;
.super Ljava/lang/Object;
.source "RebootHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/flasher/RebootHelper;->showBackupDialog(Landroid/content/Context;[Ljava/lang/String;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/flasher/RebootHelper;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$items:[Ljava/lang/String;

.field final synthetic val$wipeCaches:Z

.field final synthetic val$wipeData:Z


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/flasher/RebootHelper;Landroid/content/Context;[Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$1;->this$0:Lorg/namelessrom/devicecontrol/flasher/RebootHelper;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$1;->val$items:[Ljava/lang/String;

    iput-boolean p4, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$1;->val$wipeData:Z

    iput-boolean p5, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$1;->val$wipeCaches:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 61
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 63
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$1;->this$0:Lorg/namelessrom/devicecontrol/flasher/RebootHelper;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$1;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$1;->val$items:[Ljava/lang/String;

    iget-boolean v3, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$1;->val$wipeData:Z

    iget-boolean v4, p0, Lorg/namelessrom/devicecontrol/flasher/RebootHelper$1;->val$wipeCaches:Z

    # invokes: Lorg/namelessrom/devicecontrol/flasher/RebootHelper;->reallyShowBackupDialog(Landroid/content/Context;[Ljava/lang/String;ZZ)V
    invoke-static {v0, v1, v2, v3, v4}, Lorg/namelessrom/devicecontrol/flasher/RebootHelper;->access$000(Lorg/namelessrom/devicecontrol/flasher/RebootHelper;Landroid/content/Context;[Ljava/lang/String;ZZ)V

    .line 64
    return-void
.end method
