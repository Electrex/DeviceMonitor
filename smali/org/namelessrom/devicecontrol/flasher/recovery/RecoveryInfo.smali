.class public abstract Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;
.super Ljava/lang/Object;
.source "RecoveryInfo.java"


# instance fields
.field private externalSdcard:Ljava/lang/String;

.field private id:I

.field private internalSdcard:Ljava/lang/String;

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;->name:Ljava/lang/String;

    .line 30
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;->internalSdcard:Ljava/lang/String;

    .line 31
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;->externalSdcard:Ljava/lang/String;

    .line 34
    return-void
.end method


# virtual methods
.method public abstract getCommands([Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
.end method

.method public abstract getCommandsFile()Ljava/lang/String;
.end method

.method public getExternalSdcard()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;->externalSdcard:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;->id:I

    return v0
.end method

.method public getInternalSdcard()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;->internalSdcard:Ljava/lang/String;

    return-object v0
.end method

.method public setExternalSdcard(Ljava/lang/String;)V
    .locals 0
    .param p1, "sdcard"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;->externalSdcard:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 41
    iput p1, p0, Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;->id:I

    .line 42
    return-void
.end method

.method public setInternalSdcard(Ljava/lang/String;)V
    .locals 0
    .param p1, "sdcard"    # Ljava/lang/String;

    .prologue
    .line 57
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;->internalSdcard:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;->name:Ljava/lang/String;

    .line 50
    return-void
.end method
