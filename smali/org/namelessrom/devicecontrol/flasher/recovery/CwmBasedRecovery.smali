.class public Lorg/namelessrom/devicecontrol/flasher/recovery/CwmBasedRecovery;
.super Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;
.source "CwmBasedRecovery.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;-><init>()V

    .line 40
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/flasher/recovery/CwmBasedRecovery;->setId(I)V

    .line 41
    const-string v0, "cwmbased"

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/flasher/recovery/CwmBasedRecovery;->setName(Ljava/lang/String;)V

    .line 42
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/flasher/recovery/CwmBasedRecovery;->internalStorage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/flasher/recovery/CwmBasedRecovery;->setInternalSdcard(Ljava/lang/String;)V

    .line 43
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/flasher/recovery/CwmBasedRecovery;->externalStorage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/flasher/recovery/CwmBasedRecovery;->setExternalSdcard(Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method private externalStorage(Landroid/content/Context;)Ljava/lang/String;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    .line 115
    const-string v9, "storage"

    invoke-virtual {p1, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/storage/StorageManager;

    .line 118
    .local v4, "storageManager":Landroid/os/storage/StorageManager;
    invoke-direct {p0, v4}, Lorg/namelessrom/devicecontrol/flasher/recovery/CwmBasedRecovery;->primaryVolumePath(Landroid/os/storage/StorageManager;)Ljava/lang/String;

    move-result-object v3

    .line 119
    .local v3, "primaryVolumePath":Ljava/lang/String;
    invoke-direct {p0, v4}, Lorg/namelessrom/devicecontrol/flasher/recovery/CwmBasedRecovery;->volumePaths(Landroid/os/storage/StorageManager;)[Ljava/lang/String;

    move-result-object v6

    .line 120
    .local v6, "volumePaths":[Ljava/lang/String;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 121
    .local v7, "volumePathsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 123
    .local v2, "path":Ljava/lang/String;
    if-nez v6, :cond_1

    move v0, v8

    .line 124
    .local v0, "i":I
    :goto_0
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    if-ge v1, v0, :cond_3

    .line 125
    aget-object v5, v6, v1

    .line 127
    .local v5, "volumePath":Ljava/lang/String;
    const-string v9, "EMULATED_STORAGE_SOURCE"

    invoke-static {v9}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "EXTERNAL_STORAGE"

    invoke-static {v9}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    const-string v10, "usb"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 124
    :cond_0
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 123
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v5    # "volumePath":Ljava/lang/String;
    :cond_1
    array-length v0, v6

    goto :goto_0

    .line 134
    .restart local v0    # "i":I
    .restart local v1    # "j":I
    .restart local v5    # "volumePath":Ljava/lang/String;
    :cond_2
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 136
    .end local v5    # "volumePath":Ljava/lang/String;
    :cond_3
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_4

    .line 137
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 139
    :goto_3
    return-object v8

    :cond_4
    const/4 v8, 0x0

    goto :goto_3
.end method

.method private internalStorage()Ljava/lang/String;
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SdCardPath"
        }
    .end annotation

    .prologue
    .line 87
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    if-nez v6, :cond_1

    .line 88
    const-string v0, "sdcard"

    .line 111
    :cond_0
    :goto_0
    return-object v0

    .line 90
    :cond_1
    const-string v5, "/sdcard"

    .line 92
    .local v5, "sdcard":Ljava/lang/String;
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    .local v4, "path":Ljava/lang/String;
    move-object v0, v4

    .line 93
    .local v0, "dirPath":Ljava/lang/String;
    const-string v6, "/mnt/sdcard"

    const-string v7, "/sdcard"

    invoke-direct {p0, v0, v6, v7}, Lorg/namelessrom/devicecontrol/flasher/recovery/CwmBasedRecovery;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "/mnt/emmc"

    const-string v8, "/emmc"

    invoke-direct {p0, v6, v7, v8}, Lorg/namelessrom/devicecontrol/flasher/recovery/CwmBasedRecovery;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "/sdcard"

    invoke-direct {p0, v6, v4, v7}, Lorg/namelessrom/devicecontrol/flasher/recovery/CwmBasedRecovery;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 96
    const-string v6, "EMULATED_STORAGE_TARGET"

    invoke-static {v6}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 97
    .local v2, "emulatedStorageTarget":Ljava/lang/String;
    if-eqz v2, :cond_2

    invoke-virtual {v4, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 98
    const-string v6, ""

    invoke-virtual {v4, v2, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 99
    .local v3, "number":Ljava/lang/String;
    const-string v6, "/sdcard"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/sdcard"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v0, v6, v7}, Lorg/namelessrom/devicecontrol/flasher/recovery/CwmBasedRecovery;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 102
    .end local v3    # "number":Ljava/lang/String;
    :cond_2
    const-string v6, "EMULATED_STORAGE_SOURCE"

    invoke-static {v6}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 103
    .local v1, "emulatedStorageSource":Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 104
    const-string v6, "/data/media"

    invoke-direct {p0, v0, v1, v6}, Lorg/namelessrom/devicecontrol/flasher/recovery/CwmBasedRecovery;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 107
    :cond_3
    if-nez v2, :cond_0

    if-nez v1, :cond_0

    const-string v6, "/storage/sdcard0"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "/sdcard"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 109
    move-object v0, v4

    goto :goto_0
.end method

.method private primaryVolumePath(Landroid/os/storage/StorageManager;)Ljava/lang/String;
    .locals 5
    .param p1, "storageManager"    # Landroid/os/storage/StorageManager;

    .prologue
    .line 155
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getPrimaryVolume"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 158
    .local v1, "localObject":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getPath"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    .end local v1    # "localObject":Ljava/lang/Object;
    :goto_0
    return-object v2

    .line 160
    :catch_0
    move-exception v0

    .line 161
    .local v0, "ex":Ljava/lang/Exception;
    const-string v2, "error getting primary volume path"

    invoke-static {p0, v2, v0}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 162
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "original"    # Ljava/lang/String;
    .param p2, "starts"    # Ljava/lang/String;
    .param p3, "replace"    # Ljava/lang/String;

    .prologue
    .line 167
    invoke-virtual {p1, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .end local p1    # "original":Ljava/lang/String;
    :goto_0
    return-object p1

    .restart local p1    # "original":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private volumePaths(Landroid/os/storage/StorageManager;)[Ljava/lang/String;
    .locals 4
    .param p1, "storageManager"    # Landroid/os/storage/StorageManager;

    .prologue
    .line 144
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "getVolumePaths"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    check-cast v1, [Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    :goto_0
    return-object v1

    .line 147
    :catch_0
    move-exception v0

    .line 148
    .local v0, "ex":Ljava/lang/Exception;
    const-string v1, "error getting volume paths"

    invoke-static {p0, v1, v0}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 149
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCommands([Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 9
    .param p1, "items"    # [Ljava/lang/String;
    .param p2, "wipeData"    # Z
    .param p3, "wipeCaches"    # Z
    .param p4, "backupFolder"    # Ljava/lang/String;
    .param p5, "backupOptions"    # Ljava/lang/String;

    .prologue
    .line 54
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 55
    .local v1, "commands":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/flasher/recovery/CwmBasedRecovery;->getInternalSdcard()Ljava/lang/String;

    move-result-object v3

    .line 57
    .local v3, "internalStorage":Ljava/lang/String;
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 58
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "assert(backup_rom(\"/data/media/clockworkmod/backup/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\"));"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    :cond_0
    if-eqz p2, :cond_1

    .line 63
    const-string v6, "format(\"/data\");"

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    const-string v6, "format(\"%s/.android_secure\");"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v3, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    :cond_1
    if-eqz p3, :cond_2

    .line 67
    const-string v6, "format(\"/cache\");"

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    const-string v6, "format(\"/data/dalvik-cache\");"

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    const-string v6, "format(\"/cache/dalvik-cache\");"

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    const-string v6, "format(\"/sd-ext/dalvik-cache\");"

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    :cond_2
    array-length v6, p1

    if-lez v6, :cond_4

    .line 74
    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->get()Lorg/namelessrom/devicecontrol/utils/IOUtils;

    move-result-object v6

    invoke-virtual {v6}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->isExternalStorageAvailable()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 75
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "run_program(\"/sbin/mount\", \""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/flasher/recovery/CwmBasedRecovery;->getExternalSdcard()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\");"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    :cond_3
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v5, :cond_4

    aget-object v4, v0, v2

    .line 78
    .local v4, "item":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "assert(install_zip(\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\"));"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 82
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v4    # "item":Ljava/lang/String;
    .end local v5    # "len$":I
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-interface {v1, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    return-object v6
.end method

.method public getCommandsFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    const-string v0, "extendedcommand"

    return-object v0
.end method
