.class Lorg/namelessrom/devicecontrol/flasher/FlashCard$1;
.super Ljava/lang/Object;
.source "FlashCard.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/flasher/FlashCard;-><init>(Landroid/content/Context;Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/flasher/FlashCard;

.field final synthetic val$backup:Landroid/widget/CheckBox;

.field final synthetic val$flasherFragment:Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;

.field final synthetic val$rebootHelper:Lorg/namelessrom/devicecontrol/flasher/RebootHelper;

.field final synthetic val$wipeCaches:Landroid/widget/CheckBox;

.field final synthetic val$wipeData:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/flasher/FlashCard;Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;Lorg/namelessrom/devicecontrol/flasher/RebootHelper;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/flasher/FlashCard$1;->this$0:Lorg/namelessrom/devicecontrol/flasher/FlashCard;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/flasher/FlashCard$1;->val$flasherFragment:Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;

    iput-object p3, p0, Lorg/namelessrom/devicecontrol/flasher/FlashCard$1;->val$rebootHelper:Lorg/namelessrom/devicecontrol/flasher/RebootHelper;

    iput-object p4, p0, Lorg/namelessrom/devicecontrol/flasher/FlashCard$1;->val$backup:Landroid/widget/CheckBox;

    iput-object p5, p0, Lorg/namelessrom/devicecontrol/flasher/FlashCard$1;->val$wipeData:Landroid/widget/CheckBox;

    iput-object p6, p0, Lorg/namelessrom/devicecontrol/flasher/FlashCard$1;->val$wipeCaches:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 47
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/FlashCard$1;->val$flasherFragment:Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    .line 48
    .local v2, "items":[Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/FlashCard$1;->val$flasherFragment:Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_0

    .line 49
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/FlashCard$1;->val$flasherFragment:Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    .line 48
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 51
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/FlashCard$1;->val$rebootHelper:Lorg/namelessrom/devicecontrol/flasher/RebootHelper;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/flasher/FlashCard$1;->this$0:Lorg/namelessrom/devicecontrol/flasher/FlashCard;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/flasher/FlashCard;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/flasher/FlashCard$1;->val$backup:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/flasher/FlashCard$1;->val$wipeData:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/flasher/FlashCard$1;->val$wipeCaches:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/flasher/RebootHelper;->showRebootDialog(Landroid/content/Context;[Ljava/lang/String;ZZZ)V

    .line 53
    return-void
.end method
