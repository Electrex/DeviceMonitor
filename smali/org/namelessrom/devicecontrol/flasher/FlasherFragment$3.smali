.class Lorg/namelessrom/devicecontrol/flasher/FlasherFragment$3;
.super Ljava/lang/Object;
.source "FlasherFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->showRemoveDialog(Ljava/io/File;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;

.field final synthetic val$file:Ljava/io/File;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment$3;->this$0:Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment$3;->val$file:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 138
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 140
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment$3;->this$0:Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mFiles:Ljava/util/ArrayList;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment$3;->val$file:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 141
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment$3;->this$0:Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;

    # getter for: Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->access$000(Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v1

    new-instance v2, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter;

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment$3;->this$0:Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment$3;->this$0:Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;

    iget-object v4, v4, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mFiles:Ljava/util/ArrayList;

    invoke-direct {v2, v3, v4}, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter;-><init>(Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 142
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment$3;->this$0:Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    .line 143
    .local v0, "isEnabled":Z
    :goto_0
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment$3;->this$0:Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;

    # getter for: Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mFlashCard:Lorg/namelessrom/devicecontrol/flasher/FlashCard;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->access$100(Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;)Lorg/namelessrom/devicecontrol/flasher/FlashCard;

    move-result-object v1

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/flasher/FlashCard;->install:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 144
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment$3;->this$0:Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;

    # getter for: Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mFlashCard:Lorg/namelessrom/devicecontrol/flasher/FlashCard;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->access$100(Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;)Lorg/namelessrom/devicecontrol/flasher/FlashCard;

    move-result-object v1

    iget-object v2, v1, Lorg/namelessrom/devicecontrol/flasher/FlashCard;->install:Landroid/widget/Button;

    if-eqz v0, :cond_1

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/Application;->getAccentColor()I

    move-result v1

    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 147
    return-void

    .line 142
    .end local v0    # "isEnabled":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 144
    .restart local v0    # "isEnabled":Z
    :cond_1
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    const/high16 v3, 0x1060000

    invoke-virtual {v1, v3}, Lorg/namelessrom/devicecontrol/Application;->getColor(I)I

    move-result v1

    goto :goto_1
.end method
