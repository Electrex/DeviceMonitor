.class public Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;
.source "FlasherFragment.java"

# interfaces
.implements Lorg/namelessrom/devicecontrol/activities/RequestFileActivity$RequestFileCallback;


# instance fields
.field public mFiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mFlashCard:Lorg/namelessrom/devicecontrol/flasher/FlashCard;

.field private mRecyclerView:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;-><init>()V

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mFiles:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;)Landroid/support/v7/widget/RecyclerView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;)Lorg/namelessrom/devicecontrol/flasher/FlashCard;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mFlashCard:Lorg/namelessrom/devicecontrol/flasher/FlashCard;

    return-object v0
.end method

.method private addFile(Ljava/io/File;)V
    .locals 3
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 103
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 111
    :goto_0
    return-void

    .line 107
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mFiles:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v2}, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter;-><init>(Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 109
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mFlashCard:Lorg/namelessrom/devicecontrol/flasher/FlashCard;

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/flasher/FlashCard;->install:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 110
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mFlashCard:Lorg/namelessrom/devicecontrol/flasher/FlashCard;

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/flasher/FlashCard;->install:Landroid/widget/Button;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/Application;->getAccentColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_0
.end method


# virtual methods
.method public fileRequested(Ljava/lang/String;)V
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 114
    const-string v0, "fileRequested --> %s"

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0e00f3

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->showToast(Landroid/content/Context;I)V

    .line 124
    :goto_0
    return-void

    .line 118
    :cond_0
    const-string v0, "null"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 119
    const-string v0, "null"

    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->get()Lorg/namelessrom/devicecontrol/utils/IOUtils;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->getPrimarySdCard()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 120
    const-string v0, "filePath started with null! modified -> %s"

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 122
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->addFile(Ljava/io/File;)V

    goto :goto_0
.end method

.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 56
    const v0, 0x7f0e0101

    return v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 60
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->setHasOptionsMenu(Z)V

    .line 61
    const v4, 0x7f04002d

    const/4 v5, 0x0

    invoke-virtual {p1, v4, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 63
    .local v3, "v":Landroid/view/View;
    const v4, 0x102000a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/support/v7/widget/RecyclerView;

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 64
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v5, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 65
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v5, Landroid/support/v7/widget/DefaultItemAnimator;

    invoke-direct {v5}, Landroid/support/v7/widget/DefaultItemAnimator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 67
    new-instance v0, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter;

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mFiles:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v4}, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter;-><init>(Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;Ljava/util/List;)V

    .line 68
    .local v0, "adapter":Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter;
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 70
    const v4, 0x7f0c009b

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/melnykov/fab/FloatingActionButton;

    .line 71
    .local v1, "fabAdd":Lcom/melnykov/fab/FloatingActionButton;
    new-instance v4, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment$1;

    invoke-direct {v4, p0}, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment$1;-><init>(Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;)V

    invoke-virtual {v1, v4}, Lcom/melnykov/fab/FloatingActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    const v4, 0x7f0c0099

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/melnykov/fab/ObservableScrollView;

    .line 83
    .local v2, "scrollView":Lcom/melnykov/fab/ObservableScrollView;
    if-eqz v2, :cond_0

    .line 84
    invoke-virtual {v1, v2}, Lcom/melnykov/fab/FloatingActionButton;->attachToScrollView(Lcom/melnykov/fab/ObservableScrollView;)V

    .line 87
    :cond_0
    return-object v3
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 91
    invoke-super {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 92
    const v1, 0x7f0c009a

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 94
    .local v0, "container":Landroid/widget/LinearLayout;
    new-instance v1, Lorg/namelessrom/devicecontrol/flasher/FlashCard;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Lorg/namelessrom/devicecontrol/flasher/FlashCard;-><init>(Landroid/content/Context;Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;)V

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mFlashCard:Lorg/namelessrom/devicecontrol/flasher/FlashCard;

    .line 95
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->mFlashCard:Lorg/namelessrom/devicecontrol/flasher/FlashCard;

    invoke-virtual {v0, v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 97
    new-instance v1, Lorg/namelessrom/devicecontrol/flasher/FlashOptionCard;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/namelessrom/devicecontrol/flasher/FlashOptionCard;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 99
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/activities/RequestFileActivity;->setRequestFileCallback(Lorg/namelessrom/devicecontrol/activities/RequestFileActivity$RequestFileCallback;)V

    .line 100
    return-void
.end method

.method public showRemoveDialog(Ljava/io/File;)V
    .locals 6
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 127
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 128
    .local v0, "alert":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0e01b1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 129
    const v2, 0x7f0e01b0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 130
    .local v1, "message":Ljava/lang/String;
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 131
    const/high16 v2, 0x1040000

    new-instance v3, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment$2;

    invoke-direct {v3, p0}, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment$2;-><init>(Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 136
    const v2, 0x104000a

    new-instance v3, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment$3;

    invoke-direct {v3, p0, p1}, Lorg/namelessrom/devicecontrol/flasher/FlasherFragment$3;-><init>(Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;Ljava/io/File;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 149
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 150
    return-void
.end method
