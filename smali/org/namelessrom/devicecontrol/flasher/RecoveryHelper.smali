.class public Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;
.super Ljava/lang/Object;
.source "RecoveryHelper.java"


# instance fields
.field private mRecoveries:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;->mRecoveries:Landroid/util/SparseArray;

    .line 37
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;->mRecoveries:Landroid/util/SparseArray;

    const/4 v1, 0x0

    new-instance v2, Lorg/namelessrom/devicecontrol/flasher/recovery/CwmBasedRecovery;

    invoke-direct {v2, p1}, Lorg/namelessrom/devicecontrol/flasher/recovery/CwmBasedRecovery;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 38
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;->mRecoveries:Landroid/util/SparseArray;

    const/4 v1, 0x1

    new-instance v2, Lorg/namelessrom/devicecontrol/flasher/recovery/TwrpRecovery;

    invoke-direct {v2}, Lorg/namelessrom/devicecontrol/flasher/recovery/TwrpRecovery;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 39
    return-void
.end method


# virtual methods
.method public getCommands(I[Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 6
    .param p1, "id"    # I
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "wipeData"    # Z
    .param p4, "wipeCache"    # Z
    .param p5, "backupFolder"    # Ljava/lang/String;
    .param p6, "backupOptions"    # Ljava/lang/String;

    .prologue
    .line 103
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;->getRecovery(I)Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;

    move-result-object v0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;->getCommands([Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCommandsFile(I)Ljava/lang/String;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;->getRecovery(I)Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;->getCommandsFile()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRecovery(I)Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 42
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;->mRecoveries:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 43
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;->mRecoveries:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    .line 44
    .local v2, "key":I
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;->mRecoveries:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;

    .line 45
    .local v1, "info":Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;
    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;->getId()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 49
    .end local v1    # "info":Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;
    .end local v2    # "key":I
    :goto_1
    return-object v1

    .line 42
    .restart local v1    # "info":Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;
    .restart local v2    # "key":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    .end local v1    # "info":Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;
    .end local v2    # "key":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getRecoveryFilePath(ILjava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "id"    # I
    .param p2, "filePath"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SdCardPath"
        }
    .end annotation

    .prologue
    .line 58
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/flasher/RecoveryHelper;->getRecovery(I)Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;

    move-result-object v4

    .line 60
    .local v4, "info":Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;
    invoke-virtual {v4}, Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;->getInternalSdcard()Ljava/lang/String;

    move-result-object v7

    .line 61
    .local v7, "internalStorage":Ljava/lang/String;
    invoke-virtual {v4}, Lorg/namelessrom/devicecontrol/flasher/recovery/RecoveryInfo;->getExternalSdcard()Ljava/lang/String;

    move-result-object v2

    .line 63
    .local v2, "externalStorage":Ljava/lang/String;
    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->get()Lorg/namelessrom/devicecontrol/utils/IOUtils;

    move-result-object v10

    invoke-virtual {v10}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->getPrimarySdCard()Ljava/lang/String;

    move-result-object v8

    .line 64
    .local v8, "primarySdcard":Ljava/lang/String;
    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->get()Lorg/namelessrom/devicecontrol/utils/IOUtils;

    move-result-object v10

    invoke-virtual {v10}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->getSecondarySdCard()Ljava/lang/String;

    move-result-object v9

    .line 66
    .local v9, "secondarySdcard":Ljava/lang/String;
    const/4 v10, 0x6

    new-array v6, v10, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v8, v6, v10

    const/4 v10, 0x1

    const-string v11, "/mnt/sdcard"

    aput-object v11, v6, v10

    const/4 v10, 0x2

    const-string v11, "/storage/sdcard/"

    aput-object v11, v6, v10

    const/4 v10, 0x3

    const-string v11, "/sdcard"

    aput-object v11, v6, v10

    const/4 v10, 0x4

    const-string v11, "/storage/sdcard0"

    aput-object v11, v6, v10

    const/4 v10, 0x5

    const-string v11, "/storage/emulated/0"

    aput-object v11, v6, v10

    .line 74
    .local v6, "internalNames":[Ljava/lang/String;
    const/4 v10, 0x6

    new-array v1, v10, [Ljava/lang/String;

    const/4 v10, 0x0

    if-nez v9, :cond_0

    const-string v9, " "

    .end local v9    # "secondarySdcard":Ljava/lang/String;
    :cond_0
    aput-object v9, v1, v10

    const/4 v10, 0x1

    const-string v11, "/mnt/extSdCard"

    aput-object v11, v1, v10

    const/4 v10, 0x2

    const-string v11, "/storage/extSdCard/"

    aput-object v11, v1, v10

    const/4 v10, 0x3

    const-string v11, "/extSdCard"

    aput-object v11, v1, v10

    const/4 v10, 0x4

    const-string v11, "/storage/sdcard1"

    aput-object v11, v1, v10

    const/4 v10, 0x5

    const-string v11, "/storage/emulated/1"

    aput-object v11, v1, v10

    .line 82
    .local v1, "externalNames":[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v10, v6

    if-ge v3, v10, :cond_1

    .line 83
    aget-object v5, v6, v3

    .line 84
    .local v5, "internalName":Ljava/lang/String;
    aget-object v0, v1, v3

    .line 85
    .local v0, "externalName":Ljava/lang/String;
    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 86
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p2, v0, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 94
    .end local v0    # "externalName":Ljava/lang/String;
    .end local v5    # "internalName":Ljava/lang/String;
    :cond_1
    :goto_1
    const-string v10, "//"

    invoke-virtual {p2, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 95
    const/4 v10, 0x1

    invoke-virtual {p2, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 88
    .restart local v0    # "externalName":Ljava/lang/String;
    .restart local v5    # "internalName":Ljava/lang/String;
    :cond_2
    invoke-virtual {p2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 89
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p2, v5, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 90
    goto :goto_1

    .line 82
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 98
    .end local v0    # "externalName":Ljava/lang/String;
    .end local v5    # "internalName":Ljava/lang/String;
    :cond_4
    return-object p2
.end method
