.class public abstract Lorg/namelessrom/devicecontrol/flasher/BaseCard;
.super Landroid/widget/LinearLayout;
.source "BaseCard.java"


# instance fields
.field private mContainer:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/Application;->isDarkTheme()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 35
    const v0, 0x7f040022

    .line 39
    .local v0, "resId":I
    :goto_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 40
    const v1, 0x7f0c007e

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/flasher/BaseCard;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/flasher/BaseCard;->mContainer:Landroid/widget/FrameLayout;

    .line 41
    return-void

    .line 37
    .end local v0    # "resId":I
    :cond_0
    const v0, 0x7f040023

    .restart local v0    # "resId":I
    goto :goto_0
.end method


# virtual methods
.method public getContainer()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/flasher/BaseCard;->mContainer:Landroid/widget/FrameLayout;

    return-object v0
.end method
