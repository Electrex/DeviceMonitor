.class Lorg/namelessrom/devicecontrol/DummyLauncher$1;
.super Ljava/lang/Object;
.source "DummyLauncher.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/DummyLauncher;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/DummyLauncher;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/DummyLauncher;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/DummyLauncher$1;->this$0:Lorg/namelessrom/devicecontrol/DummyLauncher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 65
    invoke-static {}, Lorg/namelessrom/devicecontrol/Device;->get()Lorg/namelessrom/devicecontrol/Device;

    move-result-object v1

    iget-boolean v1, v1, Lorg/namelessrom/devicecontrol/Device;->hasRoot:Z

    if-nez v1, :cond_1

    .line 66
    const-string v1, "https://www.google.com/#q=how+to+root+%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Lorg/namelessrom/devicecontrol/Device;->get()Lorg/namelessrom/devicecontrol/Device;

    move-result-object v4

    iget-object v4, v4, Lorg/namelessrom/devicecontrol/Device;->model:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 68
    .local v0, "url":Ljava/lang/String;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->viewInBrowser(Ljava/lang/String;)V

    .line 72
    .end local v0    # "url":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    invoke-static {}, Lorg/namelessrom/devicecontrol/Device;->get()Lorg/namelessrom/devicecontrol/Device;

    move-result-object v1

    iget-boolean v1, v1, Lorg/namelessrom/devicecontrol/Device;->hasBusyBox:Z

    if-nez v1, :cond_0

    .line 70
    invoke-static {}, Lcom/stericson/roottools/RootTools;->offerBusyBox()V

    goto :goto_0
.end method
