.class Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;
.super Landroid/os/AsyncTask;
.source "DummyLauncher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/DummyLauncher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckTools"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lorg/namelessrom/devicecontrol/Device;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/DummyLauncher;


# direct methods
.method private constructor <init>(Lorg/namelessrom/devicecontrol/DummyLauncher;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;->this$0:Lorg/namelessrom/devicecontrol/DummyLauncher;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/namelessrom/devicecontrol/DummyLauncher;Lorg/namelessrom/devicecontrol/DummyLauncher$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/namelessrom/devicecontrol/DummyLauncher;
    .param p2, "x1"    # Lorg/namelessrom/devicecontrol/DummyLauncher$1;

    .prologue
    .line 102
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;-><init>(Lorg/namelessrom/devicecontrol/DummyLauncher;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 102
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;->doInBackground([Ljava/lang/Void;)Lorg/namelessrom/devicecontrol/Device;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Lorg/namelessrom/devicecontrol/Device;
    .locals 6
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 105
    invoke-static {}, Lorg/namelessrom/devicecontrol/Device;->get()Lorg/namelessrom/devicecontrol/Device;

    move-result-object v0

    .line 107
    .local v0, "device":Lorg/namelessrom/devicecontrol/Device;
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;->this$0:Lorg/namelessrom/devicecontrol/DummyLauncher;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;->this$0:Lorg/namelessrom/devicecontrol/DummyLauncher;

    const v3, 0x7f0e0043

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/DummyLauncher;->getString(I)Ljava/lang/String;

    move-result-object v2

    # invokes: Lorg/namelessrom/devicecontrol/DummyLauncher;->updateStatus(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/DummyLauncher;->access$200(Lorg/namelessrom/devicecontrol/DummyLauncher;Ljava/lang/String;)V

    .line 108
    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/Device;->update()V

    .line 110
    const-string v1, "hasRoot -> %s"

    new-array v2, v5, [Ljava/lang/Object;

    iget-boolean v3, v0, Lorg/namelessrom/devicecontrol/Device;->hasRoot:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p0, v1, v2}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    const-string v1, "suVersion -> %s"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, v0, Lorg/namelessrom/devicecontrol/Device;->suVersion:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {p0, v1, v2}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 112
    const-string v1, "hasBusyBox -> %s"

    new-array v2, v5, [Ljava/lang/Object;

    iget-boolean v3, v0, Lorg/namelessrom/devicecontrol/Device;->hasBusyBox:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p0, v1, v2}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 114
    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 102
    check-cast p1, Lorg/namelessrom/devicecontrol/Device;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;->onPostExecute(Lorg/namelessrom/devicecontrol/Device;)V

    return-void
.end method

.method protected onPostExecute(Lorg/namelessrom/devicecontrol/Device;)V
    .locals 8
    .param p1, "device"    # Lorg/namelessrom/devicecontrol/Device;

    .prologue
    const v7, 0x7f0e0026

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 118
    iget-boolean v1, p1, Lorg/namelessrom/devicecontrol/Device;->hasRoot:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p1, Lorg/namelessrom/devicecontrol/Device;->hasBusyBox:Z

    if-eqz v1, :cond_0

    .line 119
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;->this$0:Lorg/namelessrom/devicecontrol/DummyLauncher;

    # invokes: Lorg/namelessrom/devicecontrol/DummyLauncher;->startActivity()V
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/DummyLauncher;->access$300(Lorg/namelessrom/devicecontrol/DummyLauncher;)V

    .line 136
    :goto_0
    return-void

    .line 123
    :cond_0
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;->this$0:Lorg/namelessrom/devicecontrol/DummyLauncher;

    # getter for: Lorg/namelessrom/devicecontrol/DummyLauncher;->mProgressLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/DummyLauncher;->access$400(Lorg/namelessrom/devicecontrol/DummyLauncher;)Landroid/widget/RelativeLayout;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 124
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;->this$0:Lorg/namelessrom/devicecontrol/DummyLauncher;

    # getter for: Lorg/namelessrom/devicecontrol/DummyLauncher;->mLauncher:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/DummyLauncher;->access$500(Lorg/namelessrom/devicecontrol/DummyLauncher;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 126
    iget-boolean v1, p1, Lorg/namelessrom/devicecontrol/Device;->hasRoot:Z

    if-eqz v1, :cond_1

    .line 127
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;->this$0:Lorg/namelessrom/devicecontrol/DummyLauncher;

    const v3, 0x7f0e002a

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;->this$0:Lorg/namelessrom/devicecontrol/DummyLauncher;

    invoke-virtual {v5, v7}, Lorg/namelessrom/devicecontrol/DummyLauncher;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Lorg/namelessrom/devicecontrol/DummyLauncher;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;->this$0:Lorg/namelessrom/devicecontrol/DummyLauncher;

    const v3, 0x7f0e002b

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/DummyLauncher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 130
    .local v0, "status":Ljava/lang/String;
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;->this$0:Lorg/namelessrom/devicecontrol/DummyLauncher;

    # getter for: Lorg/namelessrom/devicecontrol/DummyLauncher;->mStatus:Landroid/widget/TextView;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/DummyLauncher;->access$600(Lorg/namelessrom/devicecontrol/DummyLauncher;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;->this$0:Lorg/namelessrom/devicecontrol/DummyLauncher;

    # getter for: Lorg/namelessrom/devicecontrol/DummyLauncher;->mAction:Landroid/widget/Button;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/DummyLauncher;->access$700(Lorg/namelessrom/devicecontrol/DummyLauncher;)Landroid/widget/Button;

    move-result-object v1

    const v2, 0x7f0e0118

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    .line 133
    .end local v0    # "status":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;->this$0:Lorg/namelessrom/devicecontrol/DummyLauncher;

    # getter for: Lorg/namelessrom/devicecontrol/DummyLauncher;->mStatus:Landroid/widget/TextView;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/DummyLauncher;->access$600(Lorg/namelessrom/devicecontrol/DummyLauncher;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;->this$0:Lorg/namelessrom/devicecontrol/DummyLauncher;

    const v3, 0x7f0e002c

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;->this$0:Lorg/namelessrom/devicecontrol/DummyLauncher;

    invoke-virtual {v5, v7}, Lorg/namelessrom/devicecontrol/DummyLauncher;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Lorg/namelessrom/devicecontrol/DummyLauncher;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/DummyLauncher$CheckTools;->this$0:Lorg/namelessrom/devicecontrol/DummyLauncher;

    # getter for: Lorg/namelessrom/devicecontrol/DummyLauncher;->mAction:Landroid/widget/Button;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/DummyLauncher;->access$700(Lorg/namelessrom/devicecontrol/DummyLauncher;)Landroid/widget/Button;

    move-result-object v1

    const v2, 0x7f0e0166

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_0
.end method
