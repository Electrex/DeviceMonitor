.class public Lorg/namelessrom/devicecontrol/actions/ActionProcessor;
.super Ljava/lang/Object;
.source "ActionProcessor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;
    }
.end annotation


# direct methods
.method private static addValuesOnOff(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 384
    .local p0, "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;>;"
    new-instance v0, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    const v2, 0x7f0e017c

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-direct {v0, v1, v2}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 385
    new-instance v0, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    const v2, 0x7f0e017b

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "0"

    invoke-direct {v0, v1, v2}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 386
    return-void
.end method

.method public static getActions(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 4
    .param p0, "category"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 155
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 156
    .local v0, "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;>;"
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 218
    :cond_0
    :goto_0
    return-object v0

    .line 159
    :cond_1
    const-string v1, "cpu"

    invoke-static {v1, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 160
    new-instance v1, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e0073

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "cpu_frequency_max"

    invoke-direct {v1, v2, v3}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    new-instance v1, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e0074

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "cpu_frequency_min"

    invoke-direct {v1, v2, v3}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    new-instance v1, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e0075

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "cpu_governor"

    invoke-direct {v1, v2, v3}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    :cond_2
    const-string v1, "gpu"

    invoke-static {v1, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 169
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getGpuFreqMaxPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 170
    new-instance v1, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e0124

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "gpu_frequency_max"

    invoke-direct {v1, v2, v3}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    :cond_3
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getGpuFreqMinPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 174
    new-instance v1, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e0125

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "gpu_frequency_min"

    invoke-direct {v1, v2, v3}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    :cond_4
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getGpuGovPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 178
    new-instance v1, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e0126

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "gpu_governor"

    invoke-direct {v1, v2, v3}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    :cond_5
    const-string v1, "/sys/devices/gr3d/enable_3d_scaling"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 182
    new-instance v1, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e0122

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "3d_scaling"

    invoke-direct {v1, v2, v3}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    :cond_6
    const-string v1, "extras"

    invoke-static {v1, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 188
    const-string v1, "/sys/kernel/mm/ksm/"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 189
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    const v2, 0x7f0e00ec

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 190
    new-instance v1, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e00c0

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ksm_enabled"

    invoke-direct {v1, v2, v3}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 193
    :cond_7
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    const v2, 0x7f0e00eb

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 194
    new-instance v1, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e008b

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ksm_deferred"

    invoke-direct {v1, v2, v3}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 197
    :cond_8
    const-string v1, "/sys/kernel/mm/ksm/pages_to_scan"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 198
    new-instance v1, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e018a

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ksm_pages"

    invoke-direct {v1, v2, v3}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 201
    :cond_9
    const-string v1, "/sys/kernel/mm/ksm/sleep_millisecs"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 202
    new-instance v1, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e01f5

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ksm_sleep"

    invoke-direct {v1, v2, v3}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 206
    :cond_a
    const-string v1, "/system/bin/mpdecision"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 207
    new-instance v1, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e0168

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "mpdecision_enabled"

    invoke-direct {v1, v2, v3}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    :cond_b
    const-string v1, "fs"

    invoke-static {v1, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 213
    new-instance v1, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e0145

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "io_scheduler"

    invoke-direct {v1, v2, v3}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    new-instance v1, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e01a7

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "read_ahead"

    invoke-direct {v1, v2, v3}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public static getCategories()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 143
    .local v0, "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;>;"
    new-instance v1, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e0071

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "cpu"

    invoke-direct {v1, v2, v3}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    new-instance v1, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e0121

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "gpu"

    invoke-direct {v1, v2, v3}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    new-instance v1, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e00d2

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "extras"

    invoke-direct {v1, v2, v3}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    new-instance v1, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e00fc

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "fs"

    invoke-direct {v1, v2, v3}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    return-object v0
.end method

.method public static getImageForCategory(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p0, "category"    # Ljava/lang/String;

    .prologue
    const v1, 0x7f020061

    .line 101
    const-string v0, "cpu"

    invoke-static {v0, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    const v0, 0x7f02006f

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/DrawableHelper;->applyAccentColorFilter(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 115
    :goto_0
    return-object v0

    .line 104
    :cond_0
    const-string v0, "gpu"

    invoke-static {v0, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    const v0, 0x7f020063

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/DrawableHelper;->applyAccentColorFilter(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 107
    :cond_1
    const-string v0, "extras"

    invoke-static {v0, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 108
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/DrawableHelper;->applyAccentColorFilter(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 110
    :cond_2
    const-string v0, "fs"

    invoke-static {v0, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 111
    const v0, 0x7f02007d

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/DrawableHelper;->applyAccentColorFilter(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 115
    :cond_3
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/DrawableHelper;->applyAccentColorFilter(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public static getProcessAction(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p0, "action"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "boot"    # Z

    .prologue
    .line 314
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 381
    :cond_0
    :goto_0
    return-void

    .line 321
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_2
    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 323
    :pswitch_0
    new-instance v0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuFreqMaxAction;

    invoke-direct {v0, p1, p2}, Lorg/namelessrom/devicecontrol/actions/cpu/CpuFreqMaxAction;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/actions/cpu/CpuFreqMaxAction;->triggerAction()V

    goto :goto_0

    .line 321
    :sswitch_0
    const-string v1, "cpu_frequency_max"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v1, "cpu_frequency_min"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v1, "cpu_governor"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_3
    const-string v1, "gpu_frequency_max"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_4
    const-string v1, "gpu_frequency_min"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_5
    const-string v1, "gpu_governor"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_6
    const-string v1, "3d_scaling"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x6

    goto :goto_1

    :sswitch_7
    const-string v1, "io_scheduler"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x7

    goto :goto_1

    :sswitch_8
    const-string v1, "read_ahead"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x8

    goto :goto_1

    :sswitch_9
    const-string v1, "ksm_enabled"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x9

    goto :goto_1

    :sswitch_a
    const-string v1, "ksm_deferred"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0xa

    goto :goto_1

    :sswitch_b
    const-string v1, "ksm_pages"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0xb

    goto/16 :goto_1

    :sswitch_c
    const-string v1, "ksm_sleep"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0xc

    goto/16 :goto_1

    :sswitch_d
    const-string v1, "uksm_sleep"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0xd

    goto/16 :goto_1

    :sswitch_e
    const-string v1, "uksm_enabled"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0xe

    goto/16 :goto_1

    :sswitch_f
    const-string v1, "uksm_governor"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0xf

    goto/16 :goto_1

    .line 326
    :pswitch_1
    new-instance v0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuFreqMinAction;

    invoke-direct {v0, p1, p2}, Lorg/namelessrom/devicecontrol/actions/cpu/CpuFreqMinAction;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/actions/cpu/CpuFreqMinAction;->triggerAction()V

    goto/16 :goto_0

    .line 329
    :pswitch_2
    new-instance v0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuGovAction;

    invoke-direct {v0, p1, p2}, Lorg/namelessrom/devicecontrol/actions/cpu/CpuGovAction;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/actions/cpu/CpuGovAction;->triggerAction()V

    goto/16 :goto_0

    .line 335
    :pswitch_3
    new-instance v0, Lorg/namelessrom/devicecontrol/actions/gpu/GpuFreqMaxAction;

    invoke-direct {v0, p1, p2}, Lorg/namelessrom/devicecontrol/actions/gpu/GpuFreqMaxAction;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/actions/gpu/GpuFreqMaxAction;->triggerAction()V

    goto/16 :goto_0

    .line 338
    :pswitch_4
    new-instance v0, Lorg/namelessrom/devicecontrol/actions/gpu/GpuFreqMinAction;

    invoke-direct {v0, p1, p2}, Lorg/namelessrom/devicecontrol/actions/gpu/GpuFreqMinAction;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/actions/gpu/GpuFreqMinAction;->triggerAction()V

    goto/16 :goto_0

    .line 341
    :pswitch_5
    new-instance v0, Lorg/namelessrom/devicecontrol/actions/gpu/GpuGovAction;

    invoke-direct {v0, p1, p2}, Lorg/namelessrom/devicecontrol/actions/gpu/GpuGovAction;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/actions/gpu/GpuGovAction;->triggerAction()V

    goto/16 :goto_0

    .line 344
    :pswitch_6
    new-instance v0, Lorg/namelessrom/devicecontrol/actions/gpu/Gpu3dScalingAction;

    invoke-direct {v0, p1, p2}, Lorg/namelessrom/devicecontrol/actions/gpu/Gpu3dScalingAction;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/actions/gpu/Gpu3dScalingAction;->triggerAction()V

    goto/16 :goto_0

    .line 350
    :pswitch_7
    new-instance v0, Lorg/namelessrom/devicecontrol/actions/fs/IoSchedulerAction;

    invoke-direct {v0, p1, p2}, Lorg/namelessrom/devicecontrol/actions/fs/IoSchedulerAction;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/actions/fs/IoSchedulerAction;->triggerAction()V

    goto/16 :goto_0

    .line 354
    :pswitch_8
    new-instance v0, Lorg/namelessrom/devicecontrol/actions/fs/ReadAheadAction;

    invoke-direct {v0, p1, p2}, Lorg/namelessrom/devicecontrol/actions/fs/ReadAheadAction;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/actions/fs/ReadAheadAction;->triggerAction()V

    goto/16 :goto_0

    .line 360
    :pswitch_9
    new-instance v0, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmEnableAction;

    invoke-direct {v0, p1, p2}, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmEnableAction;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmEnableAction;->triggerAction()V

    goto/16 :goto_0

    .line 363
    :pswitch_a
    new-instance v0, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmDeferredAction;

    invoke-direct {v0, p1, p2}, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmDeferredAction;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmDeferredAction;->triggerAction()V

    goto/16 :goto_0

    .line 366
    :pswitch_b
    new-instance v0, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmPagesAction;

    invoke-direct {v0, p1, p2}, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmPagesAction;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmPagesAction;->triggerAction()V

    goto/16 :goto_0

    .line 369
    :pswitch_c
    new-instance v0, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmSleepAction;

    invoke-direct {v0, p1, p2}, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmSleepAction;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmSleepAction;->triggerAction()V

    goto/16 :goto_0

    .line 372
    :pswitch_d
    new-instance v0, Lorg/namelessrom/devicecontrol/actions/extras/uksm/UksmSleepAction;

    invoke-direct {v0, p1, p2}, Lorg/namelessrom/devicecontrol/actions/extras/uksm/UksmSleepAction;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/actions/extras/uksm/UksmSleepAction;->triggerAction()V

    goto/16 :goto_0

    .line 375
    :pswitch_e
    new-instance v0, Lorg/namelessrom/devicecontrol/actions/extras/uksm/UksmEnableAction;

    invoke-direct {v0, p1, p2}, Lorg/namelessrom/devicecontrol/actions/extras/uksm/UksmEnableAction;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/actions/extras/uksm/UksmEnableAction;->triggerAction()V

    goto/16 :goto_0

    .line 378
    :pswitch_f
    new-instance v0, Lorg/namelessrom/devicecontrol/actions/extras/uksm/UksmGovernorAction;

    invoke-direct {v0, p1, p2}, Lorg/namelessrom/devicecontrol/actions/extras/uksm/UksmGovernorAction;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/actions/extras/uksm/UksmGovernorAction;->triggerAction()V

    goto/16 :goto_0

    .line 321
    nop

    :sswitch_data_0
    .sparse-switch
        -0x6c2d7827 -> :sswitch_6
        -0x50e9e4db -> :sswitch_f
        -0x35786e8e -> :sswitch_e
        -0x2ec2e4f6 -> :sswitch_b
        -0x2e93a5e3 -> :sswitch_c
        -0xa669628 -> :sswitch_8
        -0xa226656 -> :sswitch_0
        -0xa226568 -> :sswitch_1
        0x388511ae -> :sswitch_3
        0x3885129c -> :sswitch_4
        0x4656fd07 -> :sswitch_9
        0x4944ced9 -> :sswitch_a
        0x534467ed -> :sswitch_2
        0x54b310e2 -> :sswitch_7
        0x720afd69 -> :sswitch_5
        0x76fd0d48 -> :sswitch_d
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public static getTriggers()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 131
    .local v1, "triggers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;>;"
    new-instance v0, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e01bc

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "screen_off"

    invoke-direct {v0, v2, v3}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    .local v0, "e":Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    new-instance v0, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    .end local v0    # "e":Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e01bd

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "screen_on"

    invoke-direct {v0, v2, v3}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    .restart local v0    # "e":Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    return-object v1
.end method

.method public static getValues(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 14
    .param p0, "action"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 223
    .local v10, "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;>;"
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 309
    :cond_0
    :goto_0
    return-object v10

    .line 226
    :cond_1
    const-string v11, "cpu_frequency_max"

    invoke-static {v11, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_2

    const-string v11, "cpu_frequency_min"

    invoke-static {v11, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 228
    :cond_2
    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->get()Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->getAvailableFrequencies(Z)[Ljava/lang/String;

    move-result-object v2

    .line 229
    .local v2, "freqs":[Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 231
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v6, :cond_0

    aget-object v7, v0, v5

    .line 232
    .local v7, "s":Ljava/lang/String;
    new-instance v11, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {v7}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->toMhz(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12, v7}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 231
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 237
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "freqs":[Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "s":Ljava/lang/String;
    :cond_3
    const-string v11, "cpu_governor"

    invoke-static {v11, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 238
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;

    move-result-object v11

    invoke-virtual {v11}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->getAvailableCpuGovernors()[Ljava/lang/String;

    move-result-object v3

    .line 239
    .local v3, "governors":[Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 241
    move-object v0, v3

    .restart local v0    # "arr$":[Ljava/lang/String;
    array-length v6, v0

    .restart local v6    # "len$":I
    const/4 v5, 0x0

    .restart local v5    # "i$":I
    :goto_2
    if-ge v5, v6, :cond_0

    aget-object v7, v0, v5

    .line 242
    .restart local v7    # "s":Ljava/lang/String;
    new-instance v11, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-direct {v11, v7, v7}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 247
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v3    # "governors":[Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "s":Ljava/lang/String;
    :cond_4
    const-string v11, "gpu_frequency_max"

    invoke-static {v11, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_5

    const-string v11, "gpu_frequency_min"

    invoke-static {v11, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 249
    :cond_5
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getAvailableFrequencies(Z)[Ljava/lang/String;

    move-result-object v2

    .line 250
    .restart local v2    # "freqs":[Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 252
    move-object v0, v2

    .restart local v0    # "arr$":[Ljava/lang/String;
    array-length v6, v0

    .restart local v6    # "len$":I
    const/4 v5, 0x0

    .restart local v5    # "i$":I
    :goto_3
    if-ge v5, v6, :cond_0

    aget-object v7, v0, v5

    .line 253
    .restart local v7    # "s":Ljava/lang/String;
    new-instance v11, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-static {v7}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->toMhz(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12, v7}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 252
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 258
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "freqs":[Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "s":Ljava/lang/String;
    :cond_6
    const-string v11, "gpu_governor"

    invoke-static {v11, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 259
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;

    move-result-object v11

    invoke-virtual {v11}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->getAvailableGpuGovernors()[Ljava/lang/String;

    move-result-object v3

    .line 260
    .restart local v3    # "governors":[Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 262
    move-object v0, v3

    .restart local v0    # "arr$":[Ljava/lang/String;
    array-length v6, v0

    .restart local v6    # "len$":I
    const/4 v5, 0x0

    .restart local v5    # "i$":I
    :goto_4
    if-ge v5, v6, :cond_0

    aget-object v7, v0, v5

    .line 263
    .restart local v7    # "s":Ljava/lang/String;
    new-instance v11, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-direct {v11, v7, v7}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 262
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 268
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v3    # "governors":[Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "s":Ljava/lang/String;
    :cond_7
    const-string v11, "3d_scaling"

    invoke-static {v11, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 269
    invoke-static {v10}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor;->addValuesOnOff(Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 273
    :cond_8
    const-string v11, "io_scheduler"

    invoke-static {v11, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 274
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/IoUtils;->get()Lorg/namelessrom/devicecontrol/hardware/IoUtils;

    move-result-object v11

    invoke-virtual {v11}, Lorg/namelessrom/devicecontrol/hardware/IoUtils;->getAvailableIoSchedulers()[Ljava/lang/String;

    move-result-object v8

    .line 275
    .local v8, "scheds":[Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 277
    move-object v0, v8

    .restart local v0    # "arr$":[Ljava/lang/String;
    array-length v6, v0

    .restart local v6    # "len$":I
    const/4 v5, 0x0

    .restart local v5    # "i$":I
    :goto_5
    if-ge v5, v6, :cond_0

    aget-object v7, v0, v5

    .line 278
    .restart local v7    # "s":Ljava/lang/String;
    new-instance v11, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-direct {v11, v7, v7}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 277
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 281
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "s":Ljava/lang/String;
    .end local v8    # "scheds":[Ljava/lang/String;
    :cond_9
    const-string v11, "read_ahead"

    invoke-static {v11, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 282
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v11

    const v12, 0x7f080029

    invoke-virtual {v11, v12}, Lorg/namelessrom/devicecontrol/Application;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 283
    .local v1, "entries":[Ljava/lang/String;
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v11

    const v12, 0x7f08002a

    invoke-virtual {v11, v12}, Lorg/namelessrom/devicecontrol/Application;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    .line 284
    .local v9, "vals":[Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_6
    array-length v11, v1

    if-ge v4, v11, :cond_0

    .line 285
    new-instance v11, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    aget-object v12, v1, v4

    aget-object v13, v9, v4

    invoke-direct {v11, v12, v13}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 284
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 289
    .end local v1    # "entries":[Ljava/lang/String;
    .end local v4    # "i":I
    .end local v9    # "vals":[Ljava/lang/String;
    :cond_a
    const-string v11, "ksm_enabled"

    invoke-static {v11, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_b

    const-string v11, "ksm_deferred"

    invoke-static {v11, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 291
    :cond_b
    invoke-static {v10}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor;->addValuesOnOff(Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 293
    :cond_c
    const-string v11, "ksm_pages"

    invoke-static {v11, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_d

    .line 294
    const/4 v11, 0x6

    new-array v9, v11, [Ljava/lang/String;

    const/4 v11, 0x0

    const-string v12, "32"

    aput-object v12, v9, v11

    const/4 v11, 0x1

    const-string v12, "64"

    aput-object v12, v9, v11

    const/4 v11, 0x2

    const-string v12, "128"

    aput-object v12, v9, v11

    const/4 v11, 0x3

    const-string v12, "256"

    aput-object v12, v9, v11

    const/4 v11, 0x4

    const-string v12, "512"

    aput-object v12, v9, v11

    const/4 v11, 0x5

    const-string v12, "1024"

    aput-object v12, v9, v11

    .line 295
    .restart local v9    # "vals":[Ljava/lang/String;
    move-object v0, v9

    .restart local v0    # "arr$":[Ljava/lang/String;
    array-length v6, v0

    .restart local v6    # "len$":I
    const/4 v5, 0x0

    .restart local v5    # "i$":I
    :goto_7
    if-ge v5, v6, :cond_0

    aget-object v7, v0, v5

    .line 296
    .restart local v7    # "s":Ljava/lang/String;
    new-instance v11, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-direct {v11, v7, v7}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 295
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    .line 299
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "s":Ljava/lang/String;
    .end local v9    # "vals":[Ljava/lang/String;
    :cond_d
    const-string v11, "ksm_sleep"

    invoke-static {v11, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_e

    .line 300
    const/16 v11, 0x8

    new-array v9, v11, [Ljava/lang/String;

    const/4 v11, 0x0

    const-string v12, "100"

    aput-object v12, v9, v11

    const/4 v11, 0x1

    const-string v12, "250"

    aput-object v12, v9, v11

    const/4 v11, 0x2

    const-string v12, "500"

    aput-object v12, v9, v11

    const/4 v11, 0x3

    const-string v12, "1000"

    aput-object v12, v9, v11

    const/4 v11, 0x4

    const-string v12, "2000"

    aput-object v12, v9, v11

    const/4 v11, 0x5

    const-string v12, "3000"

    aput-object v12, v9, v11

    const/4 v11, 0x6

    const-string v12, "4000"

    aput-object v12, v9, v11

    const/4 v11, 0x7

    const-string v12, "5000"

    aput-object v12, v9, v11

    .line 301
    .restart local v9    # "vals":[Ljava/lang/String;
    move-object v0, v9

    .restart local v0    # "arr$":[Ljava/lang/String;
    array-length v6, v0

    .restart local v6    # "len$":I
    const/4 v5, 0x0

    .restart local v5    # "i$":I
    :goto_8
    if-ge v5, v6, :cond_0

    aget-object v7, v0, v5

    .line 302
    .restart local v7    # "s":Ljava/lang/String;
    new-instance v11, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    invoke-direct {v11, v7, v7}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 301
    add-int/lit8 v5, v5, 0x1

    goto :goto_8

    .line 305
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "s":Ljava/lang/String;
    .end local v9    # "vals":[Ljava/lang/String;
    :cond_e
    const-string v11, "mpdecision_enabled"

    invoke-static {v11, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 306
    invoke-static {v10}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor;->addValuesOnOff(Ljava/util/ArrayList;)V

    goto/16 :goto_0
.end method

.method public static processAction(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "action"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "boot"    # Z

    .prologue
    .line 393
    invoke-static {p0, p1, p2}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor;->getProcessAction(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 394
    return-void
.end method
