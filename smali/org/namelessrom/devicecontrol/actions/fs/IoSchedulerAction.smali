.class public Lorg/namelessrom/devicecontrol/actions/fs/IoSchedulerAction;
.super Lorg/namelessrom/devicecontrol/actions/BaseAction;
.source "IoSchedulerAction.java"


# instance fields
.field public bootup:Z

.field public id:I

.field public trigger:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "bootup"    # Z

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/actions/BaseAction;-><init>()V

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lorg/namelessrom/devicecontrol/actions/fs/IoSchedulerAction;->id:I

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/actions/fs/IoSchedulerAction;->trigger:Ljava/lang/String;

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/actions/fs/IoSchedulerAction;->value:Ljava/lang/String;

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/actions/fs/IoSchedulerAction;->bootup:Z

    .line 43
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/actions/fs/IoSchedulerAction;->value:Ljava/lang/String;

    .line 44
    iput-boolean p2, p0, Lorg/namelessrom/devicecontrol/actions/fs/IoSchedulerAction;->bootup:Z

    .line 45
    return-void
.end method


# virtual methods
.method public getBootup()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/actions/fs/IoSchedulerAction;->bootup:Z

    return v0
.end method

.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    const-string v0, "fs"

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    const-string v0, "io_scheduler"

    return-object v0
.end method

.method public getTrigger()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/actions/fs/IoSchedulerAction;->trigger:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/actions/fs/IoSchedulerAction;->value:Ljava/lang/String;

    return-object v0
.end method

.method protected setupAction()V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method public triggerAction()V
    .locals 13

    .prologue
    .line 62
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/actions/fs/IoSchedulerAction;->value:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    const-string v0, "No value for action!"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->wtf(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    :goto_0
    return-void

    .line 67
    :cond_0
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 68
    .local v11, "sb":Ljava/lang/StringBuilder;
    const/4 v7, 0x0

    .line 69
    .local v7, "c":I
    sget-object v6, Lorg/namelessrom/devicecontrol/hardware/IoUtils;->IO_SCHEDULER_PATH:[Ljava/lang/String;

    .local v6, "arr$":[Ljava/lang/String;
    array-length v10, v6

    .local v10, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    move v8, v7

    .end local v7    # "c":I
    .local v8, "c":I
    :goto_1
    if-ge v9, v10, :cond_1

    aget-object v3, v6, v9

    .line 70
    .local v3, "ioPath":Ljava/lang/String;
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/actions/fs/IoSchedulerAction;->value:Ljava/lang/String;

    invoke-static {v3, v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/actions/fs/IoSchedulerAction;->bootup:Z

    if-eqz v0, :cond_2

    .line 72
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v12

    new-instance v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    const-string v1, "extras"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "io"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "c":I
    .restart local v7    # "c":I
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/actions/fs/IoSchedulerAction;->value:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/objects/BootupItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v12, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->setBootup(Landroid/content/Context;Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 69
    :goto_2
    add-int/lit8 v9, v9, 0x1

    move v8, v7

    .end local v7    # "c":I
    .restart local v8    # "c":I
    goto :goto_1

    .line 78
    .end local v3    # "ioPath":Ljava/lang/String;
    :cond_1
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;)V

    goto :goto_0

    .restart local v3    # "ioPath":Ljava/lang/String;
    :cond_2
    move v7, v8

    .end local v8    # "c":I
    .restart local v7    # "c":I
    goto :goto_2
.end method
