.class public Lorg/namelessrom/devicecontrol/actions/extras/MpDecisionAction;
.super Lorg/namelessrom/devicecontrol/actions/BaseAction;
.source "MpDecisionAction.java"


# instance fields
.field public bootup:Z

.field public id:I

.field public trigger:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "bootup"    # Z

    .prologue
    .line 21
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/actions/BaseAction;-><init>()V

    .line 15
    const/4 v0, -0x1

    iput v0, p0, Lorg/namelessrom/devicecontrol/actions/extras/MpDecisionAction;->id:I

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/actions/extras/MpDecisionAction;->trigger:Ljava/lang/String;

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/actions/extras/MpDecisionAction;->value:Ljava/lang/String;

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/actions/extras/MpDecisionAction;->bootup:Z

    .line 22
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/actions/extras/MpDecisionAction;->value:Ljava/lang/String;

    .line 23
    iput-boolean p2, p0, Lorg/namelessrom/devicecontrol/actions/extras/MpDecisionAction;->bootup:Z

    .line 24
    return-void
.end method

.method private enableMpDecision(Z)Ljava/lang/String;
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 52
    if-eqz p1, :cond_0

    const-string v0, "start mpdecision 2> /dev/null;\n"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "stop mpdecision 2> /dev/null;\n"

    goto :goto_0
.end method


# virtual methods
.method public getBootup()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/actions/extras/MpDecisionAction;->bootup:Z

    return v0
.end method

.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    const-string v0, "extras"

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    const-string v0, "mpdecision_enabled"

    return-object v0
.end method

.method public getTrigger()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/actions/extras/MpDecisionAction;->trigger:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/actions/extras/MpDecisionAction;->value:Ljava/lang/String;

    return-object v0
.end method

.method protected setupAction()V
    .locals 0

    .prologue
    .line 38
    return-void
.end method

.method public triggerAction()V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/actions/extras/MpDecisionAction;->value:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    const-string v0, "No value for action!"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->wtf(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    :goto_0
    return-void

    .line 46
    :cond_0
    const-string v0, "/system/bin/mpdecision"

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/actions/extras/MpDecisionAction;->setBootup(Ljava/lang/String;)V

    .line 48
    const-string v0, "1"

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/actions/extras/MpDecisionAction;->value:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-direct {p0, v0}, Lorg/namelessrom/devicecontrol/actions/extras/MpDecisionAction;->enableMpDecision(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;)V

    goto :goto_0
.end method
