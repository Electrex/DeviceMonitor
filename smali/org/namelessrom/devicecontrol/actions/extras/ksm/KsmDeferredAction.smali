.class public Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmDeferredAction;
.super Lorg/namelessrom/devicecontrol/actions/BaseAction;
.source "KsmDeferredAction.java"


# instance fields
.field public bootup:Z

.field public id:I

.field public trigger:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "bootup"    # Z

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/actions/BaseAction;-><init>()V

    .line 33
    const/4 v0, -0x1

    iput v0, p0, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmDeferredAction;->id:I

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmDeferredAction;->trigger:Ljava/lang/String;

    .line 35
    const-string v0, ""

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmDeferredAction;->value:Ljava/lang/String;

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmDeferredAction;->bootup:Z

    .line 40
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmDeferredAction;->value:Ljava/lang/String;

    .line 41
    iput-boolean p2, p0, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmDeferredAction;->bootup:Z

    .line 42
    return-void
.end method


# virtual methods
.method public getBootup()Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmDeferredAction;->bootup:Z

    return v0
.end method

.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    const-string v0, "extras"

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    const-string v0, "ksm_deferred"

    return-object v0
.end method

.method public getTrigger()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmDeferredAction;->trigger:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmDeferredAction;->value:Ljava/lang/String;

    return-object v0
.end method

.method protected setupAction()V
    .locals 0

    .prologue
    .line 56
    return-void
.end method

.method public triggerAction()V
    .locals 3

    .prologue
    .line 59
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmDeferredAction;->value:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 60
    const-string v1, "No value for action!"

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->wtf(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    :goto_0
    return-void

    .line 64
    :cond_0
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    const v2, 0x7f0e00eb

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 65
    .local v0, "path":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmDeferredAction;->setBootup(Ljava/lang/String;)V

    .line 67
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/actions/extras/ksm/KsmDeferredAction;->value:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;)V

    goto :goto_0
.end method
