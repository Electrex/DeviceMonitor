.class public abstract Lorg/namelessrom/devicecontrol/actions/BaseAction;
.super Ljava/lang/Object;
.source "BaseAction.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/actions/BaseAction;->setupAction()V

    .line 28
    return-void
.end method


# virtual methods
.method public abstract getBootup()Z
.end method

.method public abstract getCategory()Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getTrigger()Ljava/lang/String;
.end method

.method public abstract getValue()Ljava/lang/String;
.end method

.method public setBootup(Ljava/lang/String;)V
    .locals 7
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 72
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/actions/BaseAction;->getBootup()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v6

    new-instance v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/actions/BaseAction;->getCategory()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/actions/BaseAction;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/actions/BaseAction;->getValue()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/objects/BootupItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v6, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->setBootup(Landroid/content/Context;Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 76
    :cond_0
    return-void
.end method

.method protected abstract setupAction()V
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 66
    const-string v0, "category: %s | name: %s | trigger: %s | value: %s | bootup: %s"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/actions/BaseAction;->getCategory()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/actions/BaseAction;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/actions/BaseAction;->getTrigger()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/actions/BaseAction;->getValue()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/actions/BaseAction;->getBootup()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
