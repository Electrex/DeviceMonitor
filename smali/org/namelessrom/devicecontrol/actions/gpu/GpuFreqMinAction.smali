.class public Lorg/namelessrom/devicecontrol/actions/gpu/GpuFreqMinAction;
.super Lorg/namelessrom/devicecontrol/actions/BaseAction;
.source "GpuFreqMinAction.java"


# instance fields
.field public bootup:Z

.field public id:I

.field public trigger:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "bootup"    # Z

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/actions/BaseAction;-><init>()V

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lorg/namelessrom/devicecontrol/actions/gpu/GpuFreqMinAction;->id:I

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/actions/gpu/GpuFreqMinAction;->trigger:Ljava/lang/String;

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/actions/gpu/GpuFreqMinAction;->value:Ljava/lang/String;

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/actions/gpu/GpuFreqMinAction;->bootup:Z

    .line 39
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/actions/gpu/GpuFreqMinAction;->value:Ljava/lang/String;

    .line 40
    iput-boolean p2, p0, Lorg/namelessrom/devicecontrol/actions/gpu/GpuFreqMinAction;->bootup:Z

    .line 41
    return-void
.end method


# virtual methods
.method public getBootup()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/actions/gpu/GpuFreqMinAction;->bootup:Z

    return v0
.end method

.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    const-string v0, "gpu"

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    const-string v0, "gpu_frequency_min"

    return-object v0
.end method

.method public getTrigger()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/actions/gpu/GpuFreqMinAction;->trigger:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/actions/gpu/GpuFreqMinAction;->value:Ljava/lang/String;

    return-object v0
.end method

.method protected setupAction()V
    .locals 0

    .prologue
    .line 55
    return-void
.end method

.method public triggerAction()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/actions/gpu/GpuFreqMinAction;->value:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    const-string v0, "No value for action!"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->wtf(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    :goto_0
    return-void

    .line 63
    :cond_0
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getGpuFreqMinPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/actions/gpu/GpuFreqMinAction;->setBootup(Ljava/lang/String;)V

    .line 65
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getGpuFreqMinPath()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/actions/gpu/GpuFreqMinAction;->value:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;)V

    goto :goto_0
.end method
