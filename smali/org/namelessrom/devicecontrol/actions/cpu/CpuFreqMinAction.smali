.class public Lorg/namelessrom/devicecontrol/actions/cpu/CpuFreqMinAction;
.super Lorg/namelessrom/devicecontrol/actions/BaseAction;
.source "CpuFreqMinAction.java"


# instance fields
.field public bootup:Z

.field public id:I

.field public trigger:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "bootup"    # Z

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/actions/BaseAction;-><init>()V

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuFreqMinAction;->id:I

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuFreqMinAction;->trigger:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuFreqMinAction;->value:Ljava/lang/String;

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuFreqMinAction;->bootup:Z

    .line 44
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuFreqMinAction;->value:Ljava/lang/String;

    .line 45
    iput-boolean p2, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuFreqMinAction;->bootup:Z

    .line 46
    return-void
.end method


# virtual methods
.method public getBootup()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuFreqMinAction;->bootup:Z

    return v0
.end method

.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    const-string v0, "cpu"

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    const-string v0, "cpu_frequency_min"

    return-object v0
.end method

.method public getTrigger()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuFreqMinAction;->trigger:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuFreqMinAction;->value:Ljava/lang/String;

    return-object v0
.end method

.method protected setupAction()V
    .locals 0

    .prologue
    .line 60
    return-void
.end method

.method public triggerAction()V
    .locals 12

    .prologue
    .line 63
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuFreqMinAction;->value:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "No value for action!"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->wtf(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    :goto_0
    return-void

    .line 68
    :cond_0
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    iget-boolean v9, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->perfCpuLock:Z

    .line 70
    .local v9, "lockFreq":Z
    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->get()Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->getNumOfCpus()I

    move-result v7

    .line 71
    .local v7, "cpus":I
    new-instance v11, Ljava/lang/StringBuilder;

    if-eqz v9, :cond_3

    mul-int/lit8 v0, v7, 0x3

    :goto_1
    invoke-direct {v11, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 73
    .local v11, "sb":Ljava/lang/StringBuilder;
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    move-result-object v6

    .line 75
    .local v6, "configuration":Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_2
    if-ge v8, v7, :cond_4

    .line 76
    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->get()Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    move-result-object v0

    invoke-virtual {v0, v8}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->onlineCpu(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->get()Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    move-result-object v0

    invoke-virtual {v0, v8}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->getMinCpuFrequencyPath(I)Ljava/lang/String;

    move-result-object v10

    .line 78
    .local v10, "path":Ljava/lang/String;
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuFreqMinAction;->value:Ljava/lang/String;

    invoke-static {v10, v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuFreqMinAction;->bootup:Z

    if-eqz v0, :cond_1

    .line 80
    new-instance v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    const-string v1, "cpu"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cpu_min"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->get()Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    move-result-object v3

    invoke-virtual {v3, v8}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->getMinCpuFrequencyPath(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuFreqMinAction;->value:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/objects/BootupItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v6, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->addItem(Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 83
    :cond_1
    if-eqz v9, :cond_2

    .line 84
    invoke-static {v10}, Lorg/namelessrom/devicecontrol/utils/Utils;->lockFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 71
    .end local v6    # "configuration":Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;
    .end local v8    # "i":I
    .end local v10    # "path":Ljava/lang/String;
    .end local v11    # "sb":Ljava/lang/StringBuilder;
    :cond_3
    mul-int/lit8 v0, v7, 0x2

    goto :goto_1

    .line 87
    .restart local v6    # "configuration":Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;
    .restart local v8    # "i":I
    .restart local v11    # "sb":Ljava/lang/StringBuilder;
    :cond_4
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    invoke-virtual {v6, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 89
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
