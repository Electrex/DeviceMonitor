.class public Lorg/namelessrom/devicecontrol/actions/cpu/CpuGovAction;
.super Lorg/namelessrom/devicecontrol/actions/BaseAction;
.source "CpuGovAction.java"


# instance fields
.field public bootup:Z

.field public id:I

.field public trigger:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "bootup"    # Z

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/actions/BaseAction;-><init>()V

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuGovAction;->id:I

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuGovAction;->trigger:Ljava/lang/String;

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuGovAction;->value:Ljava/lang/String;

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuGovAction;->bootup:Z

    .line 45
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuGovAction;->value:Ljava/lang/String;

    .line 46
    iput-boolean p2, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuGovAction;->bootup:Z

    .line 47
    return-void
.end method


# virtual methods
.method public getBootup()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuGovAction;->bootup:Z

    return v0
.end method

.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    const-string v0, "cpu"

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    const-string v0, "cpu_governor"

    return-object v0
.end method

.method public getTrigger()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuGovAction;->trigger:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuGovAction;->value:Ljava/lang/String;

    return-object v0
.end method

.method protected setupAction()V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public triggerAction()V
    .locals 12

    .prologue
    .line 64
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuGovAction;->value:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const-string v0, "No value for action!"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->wtf(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    :goto_0
    return-void

    .line 69
    :cond_0
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    iget-boolean v9, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->perfCpuGovLock:Z

    .line 71
    .local v9, "lockGov":Z
    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->get()Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->getNumOfCpus()I

    move-result v7

    .line 72
    .local v7, "cpus":I
    new-instance v11, Ljava/lang/StringBuilder;

    mul-int/lit8 v0, v7, 0x2

    invoke-direct {v11, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 74
    .local v11, "sb":Ljava/lang/StringBuilder;
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    move-result-object v6

    .line 76
    .local v6, "configuration":Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    if-ge v8, v7, :cond_3

    .line 77
    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->get()Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    move-result-object v0

    invoke-virtual {v0, v8}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->onlineCpu(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;

    move-result-object v0

    invoke-virtual {v0, v8}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->getGovernorPath(I)Ljava/lang/String;

    move-result-object v10

    .line 79
    .local v10, "path":Ljava/lang/String;
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuGovAction;->value:Ljava/lang/String;

    invoke-static {v10, v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuGovAction;->bootup:Z

    if-eqz v0, :cond_1

    .line 81
    new-instance v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    const-string v1, "cpu"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cpu_gov"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;

    move-result-object v3

    invoke-virtual {v3, v8}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->getGovernorPath(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/actions/cpu/CpuGovAction;->value:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/objects/BootupItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v6, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->addItem(Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 84
    :cond_1
    if-eqz v9, :cond_2

    .line 85
    invoke-static {v10}, Lorg/namelessrom/devicecontrol/utils/Utils;->lockFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 88
    .end local v10    # "path":Ljava/lang/String;
    :cond_3
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    invoke-virtual {v6, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 90
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
