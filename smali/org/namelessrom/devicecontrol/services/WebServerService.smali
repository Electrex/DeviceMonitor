.class public Lorg/namelessrom/devicecontrol/services/WebServerService;
.super Landroid/app/Service;
.source "WebServerService.java"


# instance fields
.field private mServerWrapper:Lorg/namelessrom/devicecontrol/net/ServerWrapper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private addNotificationStopButton(Landroid/support/v4/app/NotificationCompat$Builder;)V
    .locals 4
    .param p1, "builder"    # Landroid/support/v4/app/NotificationCompat$Builder;

    .prologue
    .line 89
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lorg/namelessrom/devicecontrol/services/WebServerService;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 90
    .local v0, "stop":Landroid/content/Intent;
    const-string v2, "action_stop"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 91
    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {p0, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 93
    .local v1, "stopIntent":Landroid/app/PendingIntent;
    const v2, 0x1080038

    const v3, 0x7f0e01fd

    invoke-virtual {p0, v3}, Lorg/namelessrom/devicecontrol/services/WebServerService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 95
    return-void
.end method

.method private getNotification()Landroid/app/Notification;
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 59
    const v6, 0x7f0e024c

    invoke-virtual {p0, v6}, Lorg/namelessrom/devicecontrol/services/WebServerService;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 61
    .local v5, "title":Ljava/lang/String;
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/services/WebServerService;->mServerWrapper:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    if-eqz v6, :cond_1

    .line 62
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/services/WebServerService;->getServerSocket()Lcom/koushikdutta/async/AsyncServerSocket;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/services/WebServerService;->getServerSocket()Lcom/koushikdutta/async/AsyncServerSocket;

    move-result-object v6

    invoke-interface {v6}, Lcom/koushikdutta/async/AsyncServerSocket;->getLocalPort()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 65
    .local v3, "port":Ljava/lang/String;
    :goto_0
    const v6, 0x7f0e0243

    new-array v7, v10, [Ljava/lang/Object;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "http://"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lorg/namelessrom/devicecontrol/net/NetworkInfo;->getAnyIpAddress()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-virtual {p0, v6, v7}, Lorg/namelessrom/devicecontrol/services/WebServerService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 71
    .end local v3    # "port":Ljava/lang/String;
    .local v4, "text":Ljava/lang/String;
    :goto_1
    new-instance v1, Landroid/content/Intent;

    const-class v6, Lorg/namelessrom/devicecontrol/services/WebServerService;

    invoke-direct {v1, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 72
    .local v1, "intent":Landroid/content/Intent;
    const-string v6, "action_stop"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    const/high16 v6, 0x8000000

    invoke-static {p0, v11, v1, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 76
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    new-instance v0, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v0, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 77
    .local v0, "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    invoke-virtual {v0, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    const v7, 0x7f020081

    invoke-virtual {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v7

    invoke-virtual {v7}, Lorg/namelessrom/devicecontrol/Application;->getAccentColor()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 84
    invoke-direct {p0, v0}, Lorg/namelessrom/devicecontrol/services/WebServerService;->addNotificationStopButton(Landroid/support/v4/app/NotificationCompat$Builder;)V

    .line 85
    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v6

    return-object v6

    .line 62
    .end local v0    # "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v4    # "text":Ljava/lang/String;
    :cond_0
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    move-result-object v6

    iget v6, v6, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->port:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 68
    :cond_1
    const v6, 0x7f0e0242

    invoke-virtual {p0, v6}, Lorg/namelessrom/devicecontrol/services/WebServerService;->getString(I)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "text":Ljava/lang/String;
    goto :goto_1
.end method

.method private stopServer()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/services/WebServerService;->mServerWrapper:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/services/WebServerService;->mServerWrapper:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->stopServer()V

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/services/WebServerService;->mServerWrapper:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    .line 135
    :cond_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/services/WebServerService;->cancelNotification()V

    .line 136
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/services/WebServerService;->stopForeground(Z)V

    .line 137
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/services/WebServerService;->stopSelf()V

    .line 138
    return-void
.end method


# virtual methods
.method public cancelNotification()V
    .locals 2

    .prologue
    .line 105
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/services/WebServerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 107
    .local v0, "notificationManager":Landroid/app/NotificationManager;
    const/16 v1, 0x1eb5

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 108
    return-void
.end method

.method public getServerSocket()Lcom/koushikdutta/async/AsyncServerSocket;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/services/WebServerService;->mServerWrapper:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/services/WebServerService;->mServerWrapper:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->getServerSocket()Lcom/koushikdutta/async/AsyncServerSocket;

    move-result-object v0

    .line 144
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 46
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 54
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 55
    const/16 v0, 0x1eb5

    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/services/WebServerService;->getNotification()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/namelessrom/devicecontrol/services/WebServerService;->startForeground(ILandroid/app/Notification;)V

    .line 56
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/services/WebServerService;->stopServer()V

    .line 50
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 51
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v3, 0x2

    .line 111
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 112
    :cond_0
    const-string v1, "intent or action is null or empty!"

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->w(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/services/WebServerService;->stopServer()V

    .line 127
    :goto_0
    return v3

    .line 116
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 117
    .local v0, "action":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "action: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    const-string v1, "action_start"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 120
    const-string v1, "creating server!"

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    new-instance v1, Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    invoke-direct {v1, p0}, Lorg/namelessrom/devicecontrol/net/ServerWrapper;-><init>(Lorg/namelessrom/devicecontrol/services/WebServerService;)V

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/services/WebServerService;->mServerWrapper:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    .line 122
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/services/WebServerService;->mServerWrapper:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->createServer()V

    goto :goto_0

    .line 124
    :cond_2
    const-string v1, "stopping service!"

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/services/WebServerService;->stopServer()V

    goto :goto_0
.end method

.method public setNotification(Landroid/app/Notification;)V
    .locals 2
    .param p1, "notification"    # Landroid/app/Notification;

    .prologue
    .line 98
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/services/WebServerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 100
    .local v0, "notificationManager":Landroid/app/NotificationManager;
    if-nez p1, :cond_0

    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/services/WebServerService;->getNotification()Landroid/app/Notification;

    move-result-object p1

    .line 101
    :cond_0
    const/16 v1, 0x1eb5

    invoke-virtual {v0, v1, p1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 102
    return-void
.end method
