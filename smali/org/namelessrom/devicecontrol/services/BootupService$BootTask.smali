.class Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;
.super Landroid/os/AsyncTask;
.source "BootupService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/services/BootupService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BootTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field final synthetic this$0:Lorg/namelessrom/devicecontrol/services/BootupService;


# direct methods
.method private constructor <init>(Lorg/namelessrom/devicecontrol/services/BootupService;Landroid/content/Context;)V
    .locals 0
    .param p2, "c"    # Landroid/content/Context;

    .prologue
    .line 82
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;->this$0:Lorg/namelessrom/devicecontrol/services/BootupService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;->mContext:Landroid/content/Context;

    return-void
.end method

.method synthetic constructor <init>(Lorg/namelessrom/devicecontrol/services/BootupService;Landroid/content/Context;Lorg/namelessrom/devicecontrol/services/BootupService$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/namelessrom/devicecontrol/services/BootupService;
    .param p2, "x1"    # Landroid/content/Context;
    .param p3, "x2"    # Lorg/namelessrom/devicecontrol/services/BootupService$1;

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;-><init>(Lorg/namelessrom/devicecontrol/services/BootupService;Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 79
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 7
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    const/4 v6, 0x0

    .line 86
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v1

    .line 87
    .local v1, "configuration":Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;
    iget-boolean v4, v1, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->dcFirstStart:Z

    if-eqz v4, :cond_0

    .line 88
    const-string v4, "First start not completed, exiting"

    invoke-static {p0, v4}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    :goto_0
    return-object v6

    .line 93
    :cond_0
    invoke-static {}, Lorg/namelessrom/devicecontrol/Device;->get()Lorg/namelessrom/devicecontrol/Device;

    move-result-object v4

    invoke-virtual {v4}, Lorg/namelessrom/devicecontrol/Device;->update()V

    .line 98
    invoke-static {}, Lorg/namelessrom/devicecontrol/Device;->get()Lorg/namelessrom/devicecontrol/Device;

    move-result-object v4

    iget-boolean v4, v4, Lorg/namelessrom/devicecontrol/Device;->hasRoot:Z

    if-eqz v4, :cond_1

    invoke-static {}, Lorg/namelessrom/devicecontrol/Device;->get()Lorg/namelessrom/devicecontrol/Device;

    move-result-object v4

    iget-boolean v4, v4, Lorg/namelessrom/devicecontrol/Device;->hasBusyBox:Z

    if-nez v4, :cond_2

    .line 99
    :cond_1
    const-string v4, "No Root, No Friends, That\'s Life ..."

    invoke-static {p0, v4}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 103
    :cond_2
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    move-result-object v4

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->loadConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    move-result-object v4

    iget-object v4, v4, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->items:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 104
    .local v3, "size":I
    if-nez v3, :cond_3

    .line 105
    const-string v4, "No bootup items"

    invoke-static {p0, v4}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 112
    :cond_3
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v4

    iget-boolean v4, v4, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->fstrimEnabled:Z

    if-eqz v4, :cond_4

    .line 113
    const-string v4, "Scheduling Tasker - FSTRIM"

    invoke-static {p0, v4}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v5

    iget v5, v5, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->fstrimInterval:I

    invoke-static {v4, v5}, Lorg/namelessrom/devicecontrol/utils/AlarmHelper;->setAlarmFstrim(Landroid/content/Context;I)V

    .line 121
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 134
    .local v2, "sbCmd":Ljava/lang/StringBuilder;
    const-string v4, "----- DEVICE START -----"

    invoke-static {p0, v4}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    iget-boolean v4, v1, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobDevice:Z

    if-eqz v4, :cond_5

    .line 136
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureFragment;->restore(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 137
    .local v0, "cmd":Ljava/lang/String;
    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    .end local v0    # "cmd":Ljava/lang/String;
    :cond_5
    const-string v4, "----- DEVICE END -----"

    invoke-static {p0, v4}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    const-string v4, "----- CPU START -----"

    invoke-static {p0, v4}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    iget-boolean v4, v1, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobCpu:Z

    if-eqz v4, :cond_6

    .line 147
    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->get()Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    move-result-object v4

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->restore(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 148
    .restart local v0    # "cmd":Ljava/lang/String;
    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    .end local v0    # "cmd":Ljava/lang/String;
    :cond_6
    const-string v4, "----- CPU END -----"

    invoke-static {p0, v4}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    const-string v4, "----- GPU START -----"

    invoke-static {p0, v4}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    iget-boolean v4, v1, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobGpu:Z

    if-eqz v4, :cond_7

    .line 154
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    move-result-object v4

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->restore(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 155
    .restart local v0    # "cmd":Ljava/lang/String;
    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    .end local v0    # "cmd":Ljava/lang/String;
    :cond_7
    const-string v4, "----- GPU END -----"

    invoke-static {p0, v4}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    const-string v4, "----- EXTRAS START -----"

    invoke-static {p0, v4}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    iget-boolean v4, v1, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobExtras:Z

    if-eqz v4, :cond_8

    .line 161
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/device/DeviceFeatureKernelFragment;->restore(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 162
    .restart local v0    # "cmd":Ljava/lang/String;
    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    .end local v0    # "cmd":Ljava/lang/String;
    :cond_8
    const-string v4, "----- EXTRAS END -----"

    invoke-static {p0, v4}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    const-string v4, "----- VOLTAGE START -----"

    invoke-static {p0, v4}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    iget-boolean v4, v1, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobVoltage:Z

    if-eqz v4, :cond_9

    .line 169
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->restore(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 170
    .restart local v0    # "cmd":Ljava/lang/String;
    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    .end local v0    # "cmd":Ljava/lang/String;
    :cond_9
    const-string v4, "----- VOLTAGE END -----"

    invoke-static {p0, v4}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    const-string v4, "----- TOOLS START -----"

    invoke-static {p0, v4}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    iget-boolean v4, v1, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobSysctl:Z

    if-eqz v4, :cond_a

    .line 180
    new-instance v4, Ljava/io/File;

    const-string v5, "/system/etc/sysctl.conf"

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 181
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/editor/SysctlFragment;->restore(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 182
    .restart local v0    # "cmd":Ljava/lang/String;
    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    const-string v4, "busybox sysctl -p;\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    .end local v0    # "cmd":Ljava/lang/String;
    :cond_a
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->restore(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 189
    .restart local v0    # "cmd":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 190
    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    :cond_b
    const-string v4, "----- TOOLS END -----"

    invoke-static {p0, v4}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 199
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_c

    .line 200
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;)V

    .line 202
    :cond_c
    const-string v4, "Bootup Done!"

    invoke-static {p0, v4}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
