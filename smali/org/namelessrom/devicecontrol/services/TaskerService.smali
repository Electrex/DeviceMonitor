.class public Lorg/namelessrom/devicecontrol/services/TaskerService;
.super Landroid/app/Service;
.source "TaskerService.java"


# instance fields
.field private mScreenReceiver:Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/services/TaskerService;->mScreenReceiver:Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver;

    .line 36
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 39
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 79
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 81
    :try_start_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/services/TaskerService;->mScreenReceiver:Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/services/TaskerService;->mScreenReceiver:Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver;

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/services/TaskerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/services/TaskerService;->mScreenReceiver:Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver;

    .line 84
    return-void

    .line 82
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v2, 0x2

    .line 44
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v3

    iget-boolean v3, v3, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->enabled:Z

    if-nez v3, :cond_0

    .line 45
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/services/TaskerService;->stopSelf()V

    .line 73
    :goto_0
    return v2

    .line 49
    :cond_0
    const-string v0, ""

    .line 50
    .local v0, "action":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 51
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 53
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "action_stop"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 54
    :cond_2
    const-string v3, "Stopping TaskerService"

    invoke-static {p0, v3}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/services/TaskerService;->stopSelf()V

    goto :goto_0

    .line 59
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "TaskerService: Action: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    const-string v3, "action_start"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 62
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 63
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 64
    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 65
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/services/TaskerService;->mScreenReceiver:Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver;

    if-nez v2, :cond_4

    .line 66
    new-instance v2, Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver;

    invoke-direct {v2}, Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver;-><init>()V

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/services/TaskerService;->mScreenReceiver:Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver;

    .line 67
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/services/TaskerService;->mScreenReceiver:Lorg/namelessrom/devicecontrol/receivers/ScreenReceiver;

    invoke-virtual {p0, v2, v1}, Lorg/namelessrom/devicecontrol/services/TaskerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 68
    const-string v2, "Starting Taskerservice"

    invoke-static {p0, v2}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    :cond_4
    const/4 v2, 0x1

    goto :goto_0

    .line 72
    .end local v1    # "filter":Landroid/content/IntentFilter;
    :cond_5
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/services/TaskerService;->stopSelf()V

    goto :goto_0
.end method
