.class public Lorg/namelessrom/devicecontrol/services/FstrimService;
.super Landroid/app/IntentService;
.source "FstrimService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/services/FstrimService$1;,
        Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    const-string v0, "FstrimService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 46
    if-eqz p1, :cond_1

    .line 47
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 50
    const-string v1, "action_tasker_fstrim"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    new-instance v1, Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask;-><init>(Lorg/namelessrom/devicecontrol/services/FstrimService;Lorg/namelessrom/devicecontrol/services/FstrimService$1;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 57
    .end local v0    # "action":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/services/FstrimService;->stopSelf()V

    goto :goto_0
.end method
