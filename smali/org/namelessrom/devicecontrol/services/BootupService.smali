.class public Lorg/namelessrom/devicecontrol/services/BootupService;
.super Landroid/app/IntentService;
.source "BootupService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/services/BootupService$1;,
        Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;
    }
.end annotation


# static fields
.field private static final lockObject:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/services/BootupService;->lockObject:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    const-string v0, "BootUpService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onDestroy()V
    .locals 5

    .prologue
    .line 68
    sget-object v2, Lorg/namelessrom/devicecontrol/services/BootupService;->lockObject:Ljava/lang/Object;

    monitor-enter v2

    .line 69
    :try_start_0
    const-string v1, "closing shells"

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    :try_start_1
    invoke-static {}, Lcom/stericson/roottools/RootTools;->closeAllShells()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75
    :goto_0
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 76
    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    .line 77
    return-void

    .line 72
    :catch_0
    move-exception v0

    .line 73
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v1, "onDestroy(): %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 75
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 59
    if-nez p1, :cond_0

    .line 60
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/services/BootupService;->stopSelf()V

    .line 64
    :goto_0
    return-void

    .line 63
    :cond_0
    new-instance v0, Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, v1}, Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;-><init>(Lorg/namelessrom/devicecontrol/services/BootupService;Landroid/content/Context;Lorg/namelessrom/devicecontrol/services/BootupService$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/services/BootupService$BootTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
