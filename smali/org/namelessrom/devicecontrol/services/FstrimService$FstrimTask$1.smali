.class Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask$1;
.super Lcom/stericson/roottools/execution/CommandCapture;
.source "FstrimService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask;

.field final synthetic val$fos:Ljava/io/FileOutputStream;


# direct methods
.method varargs constructor <init>(Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask;I[Ljava/lang/String;Ljava/io/FileOutputStream;)V
    .locals 0
    .param p2, "x0"    # I
    .param p3, "x1"    # [Ljava/lang/String;

    .prologue
    .line 76
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask$1;->this$1:Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask;

    iput-object p4, p0, Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask$1;->val$fos:Ljava/io/FileOutputStream;

    invoke-direct {p0, p2, p3}, Lcom/stericson/roottools/execution/CommandCapture;-><init>(I[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public commandCompleted(II)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "exitcode"    # I

    .prologue
    .line 89
    :try_start_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask$1;->val$fos:Ljava/io/FileOutputStream;

    const-string v1, "\n\n"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 90
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask$1;->val$fos:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V

    .line 91
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask$1;->val$fos:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :goto_0
    return-void

    .line 92
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public commandOutput(ILjava/lang/String;)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "line"    # Ljava/lang/String;

    .prologue
    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Result: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    :try_start_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask$1;->val$fos:Ljava/io/FileOutputStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-void

    .line 83
    :catch_0
    move-exception v0

    goto :goto_0
.end method
