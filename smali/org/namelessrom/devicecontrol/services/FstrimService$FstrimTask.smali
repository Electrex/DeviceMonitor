.class Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask;
.super Landroid/os/AsyncTask;
.source "FstrimService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/services/FstrimService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FstrimTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/services/FstrimService;


# direct methods
.method private constructor <init>(Lorg/namelessrom/devicecontrol/services/FstrimService;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask;->this$0:Lorg/namelessrom/devicecontrol/services/FstrimService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/namelessrom/devicecontrol/services/FstrimService;Lorg/namelessrom/devicecontrol/services/FstrimService$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/namelessrom/devicecontrol/services/FstrimService;
    .param p2, "x1"    # Lorg/namelessrom/devicecontrol/services/FstrimService$1;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask;-><init>(Lorg/namelessrom/devicecontrol/services/FstrimService;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 62
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 9
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 66
    const-string v5, "FSTRIM RUNNING"

    invoke-static {p0, v5}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask;->this$0:Lorg/namelessrom/devicecontrol/services/FstrimService;

    invoke-virtual {v6}, Lorg/namelessrom/devicecontrol/services/FstrimService;->getFilesDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lorg/namelessrom/devicecontrol/DeviceConstants;->DC_LOG_FILE_FSTRIM:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 70
    .local v3, "path":Ljava/lang/String;
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 71
    .local v2, "fos":Ljava/io/FileOutputStream;
    const-string v4, "date;\nbusybox fstrim -v /system;\nbusybox fstrim -v /data;\nbusybox fstrim -v /cache;\n"

    .line 76
    .local v4, "sb":Ljava/lang/String;
    new-instance v0, Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask$1;

    const/4 v5, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "date;\nbusybox fstrim -v /system;\nbusybox fstrim -v /data;\nbusybox fstrim -v /cache;\n"

    aput-object v8, v6, v7

    invoke-direct {v0, p0, v5, v6, v2}, Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask$1;-><init>(Lorg/namelessrom/devicecontrol/services/FstrimService$FstrimTask;I[Ljava/lang/String;Ljava/io/FileOutputStream;)V

    .line 96
    .local v0, "comm":Lcom/stericson/roottools/execution/CommandCapture;
    const/4 v5, 0x1

    invoke-static {v5}, Lcom/stericson/roottools/RootTools;->getShell(Z)Lcom/stericson/roottools/execution/Shell;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/stericson/roottools/execution/Shell;->add(Lcom/stericson/roottools/execution/Command;)Lcom/stericson/roottools/execution/Command;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    .end local v0    # "comm":Lcom/stericson/roottools/execution/CommandCapture;
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .end local v3    # "path":Ljava/lang/String;
    .end local v4    # "sb":Ljava/lang/String;
    :goto_0
    const-string v5, "FSTRIM RAN"

    invoke-static {p0, v5}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    const/4 v5, 0x0

    return-object v5

    .line 97
    :catch_0
    move-exception v1

    .line 98
    .local v1, "exc":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Fstrim error: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method
