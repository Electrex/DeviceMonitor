.class public Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter$TaskerViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "FlasherAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TaskerViewHolder"
.end annotation


# instance fields
.field public final cardView:Landroid/support/v7/widget/CardView;

.field public final name:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 75
    check-cast p1, Landroid/support/v7/widget/CardView;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter$TaskerViewHolder;->cardView:Landroid/support/v7/widget/CardView;

    .line 76
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter$TaskerViewHolder;->cardView:Landroid/support/v7/widget/CardView;

    const v1, 0x7f0c00be

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter$TaskerViewHolder;->name:Landroid/widget/TextView;

    .line 77
    return-void
.end method
