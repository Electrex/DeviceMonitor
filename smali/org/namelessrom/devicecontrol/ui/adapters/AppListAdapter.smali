.class public Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "AppListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field final mActivity:Landroid/app/Activity;

.field private final mAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/objects/AppItem;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/util/List;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/objects/AppItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p2, "appList":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/objects/AppItem;>;"
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 39
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;->res:Landroid/content/res/Resources;

    .line 45
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;->mActivity:Landroid/app/Activity;

    .line 46
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;->mAppList:Ljava/util/List;

    .line 47
    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;->res:Landroid/content/res/Resources;

    return-object v0
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;->mAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "x1"    # I

    .prologue
    .line 38
    check-cast p1, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;

    .end local p1    # "x0":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;->onBindViewHolder(Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;I)V
    .locals 2
    .param p1, "viewHolder"    # Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 99
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;->mAppList:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/objects/AppItem;

    .line 100
    .local v0, "appItem":Lorg/namelessrom/devicecontrol/objects/AppItem;
    invoke-virtual {p1, v0}, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;->bind(Lorg/namelessrom/devicecontrol/objects/AppItem;)V

    .line 101
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1
    .param p1, "x0"    # Landroid/view/ViewGroup;
    .param p2, "x1"    # I

    .prologue
    .line 38
    invoke-virtual {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "type"    # I

    .prologue
    .line 94
    new-instance v0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040035

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;-><init>(Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;Landroid/view/View;)V

    return-object v0
.end method
