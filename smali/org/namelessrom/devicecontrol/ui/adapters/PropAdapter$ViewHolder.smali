.class final Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "PropAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ViewHolder"
.end annotation


# instance fields
.field private final pp:Landroid/widget/TextView;

.field private final pv:Landroid/widget/TextView;


# direct methods
.method private constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    const v0, 0x7f0c00c0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;->pp:Landroid/widget/TextView;

    .line 89
    const v0, 0x7f0c00c1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;->pv:Landroid/widget/TextView;

    .line 90
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/View;Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/view/View;
    .param p2, "x1"    # Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$1;

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;

    .prologue
    .line 83
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;->pp:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;

    .prologue
    .line 83
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;->pv:Landroid/widget/TextView;

    return-object v0
.end method
