.class public Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;
.super Landroid/widget/ArrayAdapter;
.source "PropAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$1;,
        Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$AppFilter;,
        Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lorg/namelessrom/devicecontrol/objects/Prop;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static mFilter:Landroid/widget/Filter;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mProps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/objects/Prop;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/objects/Prop;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 41
    .local p2, "objects":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/objects/Prop;>;"
    const v0, 0x7f040039

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 42
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->mContext:Landroid/content/Context;

    .line 43
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->mProps:Ljava/util/List;

    .line 44
    return-void
.end method

.method static synthetic access$300(Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    .prologue
    .line 35
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->mProps:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public getFilter()Landroid/widget/Filter;
    .locals 2

    .prologue
    .line 77
    sget-object v0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->mFilter:Landroid/widget/Filter;

    if-nez v0, :cond_0

    .line 78
    new-instance v0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$AppFilter;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->mProps:Ljava/util/List;

    invoke-direct {v0, p0, v1}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$AppFilter;-><init>(Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;Ljava/util/List;)V

    sput-object v0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->mFilter:Landroid/widget/Filter;

    .line 80
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->mFilter:Landroid/widget/Filter;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->getItem(I)Lorg/namelessrom/devicecontrol/objects/Prop;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Lorg/namelessrom/devicecontrol/objects/Prop;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 46
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->mProps:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/objects/Prop;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 51
    if-nez p2, :cond_3

    .line 52
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    const v3, 0x7f040039

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 55
    new-instance v1, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;

    const/4 v2, 0x0

    invoke-direct {v1, p2, v2}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;-><init>(Landroid/view/View;Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$1;)V

    .line 56
    .local v1, "viewHolder":Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;
    sget-boolean v2, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-nez p2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 57
    :cond_0
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 62
    :goto_0
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->mProps:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/objects/Prop;

    .line 63
    .local v0, "p":Lorg/namelessrom/devicecontrol/objects/Prop;
    if-eqz v0, :cond_2

    .line 64
    # getter for: Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;->pp:Landroid/widget/TextView;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;->access$100(Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 65
    # getter for: Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;->pp:Landroid/widget/TextView;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;->access$100(Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/objects/Prop;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    :cond_1
    # getter for: Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;->pv:Landroid/widget/TextView;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;->access$200(Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 68
    # getter for: Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;->pv:Landroid/widget/TextView;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;->access$200(Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/objects/Prop;->getVal()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    :cond_2
    return-object p2

    .line 59
    .end local v0    # "p":Lorg/namelessrom/devicecontrol/objects/Prop;
    .end local v1    # "viewHolder":Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;

    .restart local v1    # "viewHolder":Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$ViewHolder;
    goto :goto_0
.end method
