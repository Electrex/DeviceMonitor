.class public Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;
.super Landroid/widget/BaseAdapter;
.source "MenuListArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mIcons:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mLayoutResourceId:I

.field private final mTitles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layoutResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p3, "titles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .local p4, "icons":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 45
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;->mContext:Landroid/content/Context;

    .line 46
    iput p2, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;->mLayoutResourceId:I

    .line 47
    iput-object p3, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;->mTitles:Ljava/util/ArrayList;

    .line 48
    iput-object p4, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;->mIcons:Ljava/util/ArrayList;

    .line 49
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;->mTitles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 53
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;->mTitles:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 55
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 59
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;->mIcons:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 87
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;->getItemViewType(I)I

    move-result v4

    .line 88
    .local v4, "type":I
    if-nez p2, :cond_4

    .line 89
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 90
    .local v2, "inflater":Landroid/view/LayoutInflater;
    if-nez v4, :cond_1

    .line 91
    iget v6, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;->mLayoutResourceId:I

    invoke-virtual {v2, v6, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 95
    :cond_0
    :goto_0
    sget-boolean v6, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;->$assertionsDisabled:Z

    if-nez v6, :cond_2

    if-nez p2, :cond_2

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 92
    :cond_1
    if-ne v4, v8, :cond_0

    .line 93
    const v6, 0x7f04003a

    invoke-virtual {v2, v6, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 96
    :cond_2
    new-instance v5, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;

    invoke-direct {v5, p2, v4}, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;-><init>(Landroid/view/View;I)V

    .line 97
    .local v5, "viewHolder":Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;
    invoke-virtual {p2, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 102
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    :goto_1
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;->mTitles:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 103
    .local v3, "titleResId":I
    if-nez v4, :cond_6

    .line 104
    # getter for: Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;
    invoke-static {v5}, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->access$000(Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(I)V

    .line 106
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;->mIcons:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 107
    .local v1, "imageRes":I
    if-nez v1, :cond_5

    .line 108
    # getter for: Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->image:Landroid/widget/ImageView;
    invoke-static {v5}, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->access$100(Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v6

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 122
    .end local v1    # "imageRes":I
    :cond_3
    :goto_2
    return-object p2

    .line 99
    .end local v3    # "titleResId":I
    .end local v5    # "viewHolder":Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;
    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;

    .restart local v5    # "viewHolder":Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;
    goto :goto_1

    .line 110
    .restart local v1    # "imageRes":I
    .restart local v3    # "titleResId":I
    :cond_5
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 111
    .local v0, "icon":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_3

    .line 112
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/DrawableHelper;->applyAccentColorFilter(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 113
    # getter for: Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->image:Landroid/widget/ImageView;
    invoke-static {v5}, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->access$100(Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 116
    .end local v0    # "icon":Landroid/graphics/drawable/Drawable;
    .end local v1    # "imageRes":I
    :cond_6
    if-ne v4, v8, :cond_3

    .line 117
    # getter for: Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->header:Landroid/widget/TextView;
    invoke-static {v5}, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->access$200(Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(I)V

    .line 118
    # getter for: Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->header:Landroid/widget/TextView;
    invoke-static {v5}, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->access$200(Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setClickable(Z)V

    .line 119
    # getter for: Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->header:Landroid/widget/TextView;
    invoke-static {v5}, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->access$200(Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v6

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v7

    invoke-virtual {v7}, Lorg/namelessrom/devicecontrol/Application;->getAccentColor()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1, "pos"    # I

    .prologue
    const/4 v0, 0x1

    .line 61
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;->getItemViewType(I)I

    move-result v1

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
