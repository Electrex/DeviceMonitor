.class public Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "FlasherAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter$TaskerViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter$TaskerViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private mFiles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private final mFragment:Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;


# direct methods
.method public constructor <init>(Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;Ljava/util/List;)V
    .locals 0
    .param p1, "fragment"    # Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p2, "files":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 39
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter;->mFragment:Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;

    .line 40
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter;->mFiles:Ljava/util/List;

    .line 41
    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter;)Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter;->mFragment:Lorg/namelessrom/devicecontrol/flasher/FlasherFragment;

    return-object v0
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter;->mFiles:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter;->mFiles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "x1"    # I

    .prologue
    .line 34
    check-cast p1, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter$TaskerViewHolder;

    .end local p1    # "x0":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter;->onBindViewHolder(Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter$TaskerViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter$TaskerViewHolder;I)V
    .locals 3
    .param p1, "holder"    # Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter$TaskerViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 59
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter;->mFiles:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 61
    .local v0, "file":Ljava/io/File;
    iget-object v1, p1, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter$TaskerViewHolder;->name:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    iget-object v1, p1, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter$TaskerViewHolder;->cardView:Landroid/support/v7/widget/CardView;

    new-instance v2, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter$1;

    invoke-direct {v2, p0, v0}, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter$1;-><init>(Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter;Ljava/io/File;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/CardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1
    .param p1, "x0"    # Landroid/view/ViewGroup;
    .param p2, "x1"    # I

    .prologue
    .line 34
    invoke-virtual {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter$TaskerViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter$TaskerViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 49
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/Application;->isDarkTheme()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 50
    const v0, 0x7f040020

    .line 54
    .local v0, "resId":I
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 55
    .local v1, "v":Landroid/view/View;
    new-instance v2, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter$TaskerViewHolder;

    invoke-direct {v2, v1}, Lorg/namelessrom/devicecontrol/ui/adapters/FlasherAdapter$TaskerViewHolder;-><init>(Landroid/view/View;)V

    return-object v2

    .line 52
    .end local v0    # "resId":I
    .end local v1    # "v":Landroid/view/View;
    :cond_0
    const v0, 0x7f040021

    .restart local v0    # "resId":I
    goto :goto_0
.end method
