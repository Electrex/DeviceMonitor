.class public final Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "AppListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "ViewHolder"
.end annotation


# instance fields
.field private final appIcon:Landroid/widget/ImageView;

.field private final appLabel:Landroid/widget/TextView;

.field private final container:Landroid/view/View;

.field private mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

.field private final packageName:Landroid/widget/TextView;

.field private final rootView:Landroid/view/View;

.field final synthetic this$0:Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;


# direct methods
.method public constructor <init>(Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;Landroid/view/View;)V
    .locals 1
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 58
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;->this$0:Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;

    .line 59
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 60
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;->rootView:Landroid/view/View;

    .line 61
    const v0, 0x7f0c00a3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;->container:Landroid/view/View;

    .line 62
    const v0, 0x7f0c00a5

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;->appIcon:Landroid/widget/ImageView;

    .line 63
    const v0, 0x7f0c00a6

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;->appLabel:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f0c00a7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;->packageName:Landroid/widget/TextView;

    .line 66
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;->rootView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    return-void
.end method


# virtual methods
.method public bind(Lorg/namelessrom/devicecontrol/objects/AppItem;)V
    .locals 4
    .param p1, "appItem"    # Lorg/namelessrom/devicecontrol/objects/AppItem;

    .prologue
    .line 70
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    .line 72
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;->appIcon:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 73
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;->appLabel:Landroid/widget/TextView;

    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;->packageName:Landroid/widget/TextView;

    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/Application;->isDarkTheme()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, -0x1

    .line 77
    .local v0, "color":I
    :goto_0
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;->appLabel:Landroid/widget/TextView;

    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/objects/AppItem;->isSystemApp()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;->this$0:Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;

    # getter for: Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;->res:Landroid/content/res/Resources;
    invoke-static {v2}, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;->access$000(Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;)Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0063

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .end local v0    # "color":I
    :cond_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 79
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;->container:Landroid/view/View;

    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/objects/AppItem;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x106000d

    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 81
    return-void

    .line 76
    :cond_1
    const/high16 v0, -0x1000000

    goto :goto_0

    .line 79
    :cond_2
    const v1, 0x7f0a002a

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 84
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;->this$0:Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;->mActivity:Landroid/app/Activity;

    const-class v2, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 85
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "arg_from_activity"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 86
    const-string v1, "arg_package_name"

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 87
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter$ViewHolder;->this$0:Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 88
    return-void
.end method
