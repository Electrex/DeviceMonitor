.class Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$AppFilter;
.super Landroid/widget/Filter;
.source "PropAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AppFilter"
.end annotation


# instance fields
.field private final sourceObjects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/objects/Prop;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;


# direct methods
.method public constructor <init>(Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/objects/Prop;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 96
    .local p2, "props":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/objects/Prop;>;"
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$AppFilter;->this$0:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$AppFilter;->sourceObjects:Ljava/util/List;

    .line 98
    monitor-enter p0

    .line 99
    :try_start_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$AppFilter;->sourceObjects:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 100
    monitor-exit p0

    .line 101
    return-void

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 6
    .param p1, "chars"    # Ljava/lang/CharSequence;

    .prologue
    .line 105
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 106
    .local v1, "filterSeq":Ljava/lang/String;
    new-instance v4, Landroid/widget/Filter$FilterResults;

    invoke-direct {v4}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 107
    .local v4, "result":Landroid/widget/Filter$FilterResults;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_2

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 109
    .local v0, "filter":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/objects/Prop;>;"
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$AppFilter;->this$0:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    # getter for: Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->mProps:Ljava/util/List;
    invoke-static {v5}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->access$300(Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/namelessrom/devicecontrol/objects/Prop;

    .line 110
    .local v3, "o":Lorg/namelessrom/devicecontrol/objects/Prop;
    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/objects/Prop;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 112
    .end local v3    # "o":Lorg/namelessrom/devicecontrol/objects/Prop;
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    iput v5, v4, Landroid/widget/Filter$FilterResults;->count:I

    .line 113
    iput-object v0, v4, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 120
    .end local v0    # "filter":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/objects/Prop;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    :goto_1
    return-object v4

    .line 115
    :cond_2
    monitor-enter p0

    .line 116
    :try_start_0
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$AppFilter;->sourceObjects:Ljava/util/List;

    iput-object v5, v4, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 117
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$AppFilter;->sourceObjects:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    iput v5, v4, Landroid/widget/Filter$FilterResults;->count:I

    .line 118
    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 4
    .param p1, "constraint"    # Ljava/lang/CharSequence;
    .param p2, "results"    # Landroid/widget/Filter$FilterResults;

    .prologue
    .line 126
    iget-object v1, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v1, Ljava/util/List;

    .line 127
    .local v1, "filtered":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/objects/Prop;>;"
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$AppFilter;->this$0:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->notifyDataSetChanged()V

    .line 128
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$AppFilter;->this$0:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->clear()V

    .line 129
    if-eqz v1, :cond_0

    .line 130
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/objects/Prop;

    .local v0, "aFiltered":Lorg/namelessrom/devicecontrol/objects/Prop;
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$AppFilter;->this$0:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    invoke-virtual {v3, v0}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 132
    .end local v0    # "aFiltered":Lorg/namelessrom/devicecontrol/objects/Prop;
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter$AppFilter;->this$0:Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/ui/adapters/PropAdapter;->notifyDataSetInvalidated()V

    .line 133
    return-void
.end method
