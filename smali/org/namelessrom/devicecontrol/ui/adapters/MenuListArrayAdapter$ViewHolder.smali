.class final Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "MenuListArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ViewHolder"
.end annotation


# instance fields
.field private final header:Landroid/widget/TextView;

.field private final image:Landroid/widget/ImageView;

.field private final title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;I)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "type"    # I

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    if-nez p2, :cond_0

    .line 70
    iput-object v1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->header:Landroid/widget/TextView;

    .line 71
    const v0, 0x1020014

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f0c0045

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    .line 82
    :goto_0
    return-void

    .line 73
    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 74
    const v0, 0x7f0c00c2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->header:Landroid/widget/TextView;

    .line 75
    iput-object v1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 76
    iput-object v1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    goto :goto_0

    .line 78
    :cond_1
    iput-object v1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->header:Landroid/widget/TextView;

    .line 79
    iput-object v1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 80
    iput-object v1, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    goto :goto_0
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;

    .prologue
    .line 63
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;

    .prologue
    .line 63
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$200(Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;

    .prologue
    .line 63
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/adapters/MenuListArrayAdapter$ViewHolder;->header:Landroid/widget/TextView;

    return-object v0
.end method
