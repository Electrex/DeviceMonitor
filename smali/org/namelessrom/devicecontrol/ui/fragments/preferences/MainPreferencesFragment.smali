.class public Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;
.super Landroid/support/v4/preference/PreferenceFragment;
.source "MainPreferencesFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private mDarkTheme:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

.field private mExtensiveLogging:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

.field private mMonkeyPref:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

.field private mSetOnBoot:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mShowLauncher:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

.field private mShowPollfish:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

.field private mSkipChecks:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

.field private mSwipeOnContent:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/support/v4/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method private setupVersionPreference()V
    .locals 15

    .prologue
    const v14, 0x7f0e022e

    const v13, 0x7f0e0029

    const v12, 0x7f0e0028

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 135
    const-string v6, "prefs_version"

    invoke-virtual {p0, v6}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 136
    .local v1, "mVersion":Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    if-eqz v1, :cond_0

    .line 140
    :try_start_0
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v6

    invoke-virtual {v6}, Lorg/namelessrom/devicecontrol/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 141
    .local v3, "pm":Landroid/content/pm/PackageManager;
    if-eqz v3, :cond_1

    .line 142
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 143
    .local v2, "pInfo":Landroid/content/pm/PackageInfo;
    const v6, 0x7f0e0029

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-virtual {p0, v6, v7}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 144
    .local v5, "title":Ljava/lang/String;
    const v6, 0x7f0e0028

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget v9, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v6, v7}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 152
    .end local v2    # "pInfo":Landroid/content/pm/PackageInfo;
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    .local v4, "summary":Ljava/lang/String;
    :goto_0
    invoke-virtual {v1, v5}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 153
    invoke-virtual {v1, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 155
    .end local v4    # "summary":Ljava/lang/String;
    .end local v5    # "title":Ljava/lang/String;
    :cond_0
    return-void

    .line 146
    .restart local v3    # "pm":Landroid/content/pm/PackageManager;
    :cond_1
    :try_start_1
    new-instance v6, Ljava/lang/Exception;

    const-string v7, "pm not null"

    invoke-direct {v6, v7}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 148
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v0

    .line 149
    .local v0, "ignored":Ljava/lang/Exception;
    new-array v6, v11, [Ljava/lang/Object;

    invoke-virtual {p0, v14}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {p0, v13, v6}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 150
    .restart local v5    # "title":Ljava/lang/String;
    new-array v6, v11, [Ljava/lang/Object;

    invoke-virtual {p0, v14}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {p0, v12, v6}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "summary":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-super {p0, p1}, Landroid/support/v4/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 75
    const/high16 v2, 0x7f060000

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->addPreferencesFromResource(I)V

    .line 77
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v1

    .line 79
    .local v1, "configuration":Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;
    const-string v2, "extensive_logging"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mExtensiveLogging:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    .line 81
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mExtensiveLogging:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-eqz v2, :cond_0

    .line 82
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mExtensiveLogging:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iget-boolean v3, v1, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->extensiveLogging:Z

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 83
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mExtensiveLogging:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 86
    :cond_0
    const-string v2, "prefs_general"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 87
    .local v0, "category":Landroid/preference/PreferenceCategory;
    const-string v2, "prefs_set_on_boot"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mSetOnBoot:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 89
    const-string v2, "show_launcher"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mShowLauncher:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    .line 90
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mShowLauncher:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-eqz v2, :cond_1

    .line 91
    sget-boolean v2, Lorg/namelessrom/devicecontrol/Application;->IS_NAMELESS:Z

    if-eqz v2, :cond_4

    .line 92
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mShowLauncher:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iget-boolean v3, v1, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->showLauncher:Z

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 93
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mShowLauncher:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 99
    :cond_1
    :goto_0
    const-string v2, "skip_checks"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mSkipChecks:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    .line 100
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mSkipChecks:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-eqz v2, :cond_2

    .line 101
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mSkipChecks:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iget-boolean v3, v1, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->skipChecks:Z

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 102
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mSkipChecks:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 105
    :cond_2
    const-string v2, "prefs_app"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .end local v0    # "category":Landroid/preference/PreferenceCategory;
    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 107
    .restart local v0    # "category":Landroid/preference/PreferenceCategory;
    const-string v2, "/system/build.prop"

    const-string v3, "ro.nameless.secret=1"

    invoke-static {v2, v3}, Lorg/namelessrom/devicecontrol/utils/Utils;->existsInFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 108
    new-instance v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mMonkeyPref:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    .line 109
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mMonkeyPref:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    const-string v3, "monkey"

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setKey(Ljava/lang/String;)V

    .line 110
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mMonkeyPref:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    const v3, 0x7f0e0034

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setTitle(I)V

    .line 111
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mMonkeyPref:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    const v3, 0x7f0e0146

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setSummaryOn(I)V

    .line 112
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mMonkeyPref:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    const v3, 0x7f0e0175

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setSummaryOff(I)V

    .line 113
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mMonkeyPref:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v3

    iget-boolean v3, v3, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->monkey:Z

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 114
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mMonkeyPref:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 115
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mMonkeyPref:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 118
    :cond_3
    const-string v2, "dark_theme"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mDarkTheme:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    .line 119
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mDarkTheme:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v3

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/Application;->isDarkTheme()Z

    move-result v3

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 120
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mDarkTheme:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 122
    const-string v2, "swipe_on_content"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mSwipeOnContent:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    .line 124
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mSwipeOnContent:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iget-boolean v3, v1, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->swipeOnContent:Z

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 125
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mSwipeOnContent:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 127
    const-string v2, "show_pollfish"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mShowPollfish:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    .line 128
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mShowPollfish:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iget-boolean v3, v1, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->showPollfish:Z

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 129
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mShowPollfish:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 131
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->setupVersionPreference()V

    .line 132
    return-void

    .line 95
    :cond_4
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mShowLauncher:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    .line 158
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mShowPollfish:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-ne v3, p1, :cond_1

    .line 159
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 161
    .local v1, "value":Z
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v3

    iput-boolean v1, v3, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->showPollfish:Z

    .line 162
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v3

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    .line 164
    if-eqz v1, :cond_0

    .line 165
    invoke-static {}, Lcom/pollfish/main/PollFish;->show()V

    .line 169
    :goto_0
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mShowPollfish:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v3, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 230
    .end local v1    # "value":Z
    :goto_1
    return v2

    .line 167
    .restart local v1    # "value":Z
    :cond_0
    invoke-static {}, Lcom/pollfish/main/PollFish;->hide()V

    goto :goto_0

    .line 171
    .end local v1    # "value":Z
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_1
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mExtensiveLogging:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-ne v3, p1, :cond_2

    .line 172
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 174
    .restart local v1    # "value":Z
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v3

    iput-boolean v1, v3, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->extensiveLogging:Z

    .line 175
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v3

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    .line 177
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/Logger;->setEnabled(Z)V

    .line 178
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mExtensiveLogging:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v3, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    goto :goto_1

    .line 180
    .end local v1    # "value":Z
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_2
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mShowLauncher:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-ne v3, p1, :cond_3

    .line 181
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 183
    .restart local v1    # "value":Z
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v3

    iput-boolean v1, v3, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->showLauncher:Z

    .line 184
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v3

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    .line 186
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v3

    invoke-virtual {v3, v1}, Lorg/namelessrom/devicecontrol/Application;->toggleLauncherIcon(Z)V

    .line 187
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mShowLauncher:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v3, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    goto :goto_1

    .line 189
    .end local v1    # "value":Z
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_3
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mSkipChecks:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-ne v3, p1, :cond_4

    .line 190
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 192
    .restart local v1    # "value":Z
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v3

    iput-boolean v1, v3, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->skipChecks:Z

    .line 193
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v3

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    .line 195
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mSkipChecks:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v3, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    goto/16 :goto_1

    .line 197
    .end local v1    # "value":Z
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_4
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mMonkeyPref:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-ne v3, p1, :cond_5

    .line 198
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 199
    .restart local v1    # "value":Z
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v3

    iput-boolean v1, v3, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->monkey:Z

    .line 200
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v3

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    .line 201
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mMonkeyPref:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v3, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    goto/16 :goto_1

    .line 203
    .end local v1    # "value":Z
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_5
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mSwipeOnContent:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-ne v3, p1, :cond_6

    .line 204
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 206
    .restart local v1    # "value":Z
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v3

    iput-boolean v1, v3, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->swipeOnContent:Z

    .line 207
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v3

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    .line 209
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mSwipeOnContent:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v3, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 212
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/MainActivity;->setSwipeOnContent(Z)V

    goto/16 :goto_1

    .line 214
    .end local v1    # "value":Z
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_6
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mDarkTheme:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-ne v3, p1, :cond_8

    .line 215
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 216
    .local v0, "isDark":Z
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v3

    invoke-virtual {v3, v0}, Lorg/namelessrom/devicecontrol/Application;->setDarkTheme(Z)Lorg/namelessrom/devicecontrol/Application;

    .line 217
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mDarkTheme:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v3, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 219
    if-eqz v0, :cond_7

    .line 220
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v3

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0004

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lorg/namelessrom/devicecontrol/Application;->setAccentColor(I)Lorg/namelessrom/devicecontrol/Application;

    .line 226
    :goto_2
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/utils/Utils;->restartActivity(Landroid/app/Activity;)V

    goto/16 :goto_1

    .line 222
    :cond_7
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v3

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0005

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lorg/namelessrom/devicecontrol/Application;->setAccentColor(I)Lorg/namelessrom/devicecontrol/Application;

    goto :goto_2

    .line 230
    .end local v0    # "isDark":Z
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 3
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 235
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->mSetOnBoot:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne v0, p2, :cond_0

    .line 236
    new-instance v0, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;-><init>()V

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/preferences/MainPreferencesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "sob_dialog_fragment"

    invoke-virtual {v0, v1, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 238
    const/4 v0, 0x1

    .line 241
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/preference/PreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    goto :goto_0
.end method
