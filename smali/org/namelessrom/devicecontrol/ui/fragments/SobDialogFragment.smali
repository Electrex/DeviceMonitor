.class public Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "SobDialogFragment.java"


# instance fields
.field final entries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->entries:Ljava/util/ArrayList;

    .line 22
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->entries:Ljava/util/ArrayList;

    const v1, 0x7f0e0091

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 23
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->entries:Ljava/util/ArrayList;

    const v1, 0x7f0e007d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 24
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->entries:Ljava/util/ArrayList;

    const v1, 0x7f0e012a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 25
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->entries:Ljava/util/ArrayList;

    const v1, 0x7f0e00d2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 26
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->entries:Ljava/util/ArrayList;

    const v1, 0x7f0e020b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 28
    const-string v0, "/sys/devices/system/cpu/cpufreq/vdd_table/vdd_levels"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->entries:Ljava/util/ArrayList;

    const v1, 0x7f0e023c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 32
    :cond_1
    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;IZ)V
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->setChecked(IZ)V

    return-void
.end method

.method private isChecked(I)Z
    .locals 1
    .param p1, "entry"    # I

    .prologue
    .line 68
    sparse-switch p1, :sswitch_data_0

    .line 82
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 70
    :sswitch_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    iget-boolean v0, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobDevice:Z

    goto :goto_0

    .line 72
    :sswitch_1
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    iget-boolean v0, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobCpu:Z

    goto :goto_0

    .line 74
    :sswitch_2
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    iget-boolean v0, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobGpu:Z

    goto :goto_0

    .line 76
    :sswitch_3
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    iget-boolean v0, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobExtras:Z

    goto :goto_0

    .line 78
    :sswitch_4
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    iget-boolean v0, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobSysctl:Z

    goto :goto_0

    .line 80
    :sswitch_5
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    iget-boolean v0, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobVoltage:Z

    goto :goto_0

    .line 68
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0e007d -> :sswitch_1
        0x7f0e0091 -> :sswitch_0
        0x7f0e00d2 -> :sswitch_3
        0x7f0e012a -> :sswitch_2
        0x7f0e020b -> :sswitch_4
        0x7f0e023c -> :sswitch_5
    .end sparse-switch
.end method

.method private setChecked(IZ)V
    .locals 1
    .param p1, "entry"    # I
    .param p2, "checked"    # Z

    .prologue
    .line 87
    packed-switch p1, :pswitch_data_0

    .line 109
    :goto_0
    return-void

    .line 89
    :pswitch_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    iput-boolean p2, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobDevice:Z

    goto :goto_0

    .line 92
    :pswitch_1
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    iput-boolean p2, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobCpu:Z

    goto :goto_0

    .line 95
    :pswitch_2
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    iput-boolean p2, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobGpu:Z

    goto :goto_0

    .line 98
    :pswitch_3
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    iput-boolean p2, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobExtras:Z

    goto :goto_0

    .line 101
    :pswitch_4
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    iput-boolean p2, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobSysctl:Z

    goto :goto_0

    .line 104
    :pswitch_5
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    iput-boolean p2, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->sobVoltage:Z

    goto :goto_0

    .line 87
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->entries:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 36
    .local v4, "length":I
    new-array v3, v4, [Ljava/lang/String;

    .line 37
    .local v3, "items":[Ljava/lang/String;
    new-array v1, v4, [Z

    .line 39
    .local v1, "checked":[Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v4, :cond_0

    .line 40
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->entries:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {p0, v5}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v2

    .line 41
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->entries:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct {p0, v5}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->isChecked(I)Z

    move-result v5

    aput-boolean v5, v1, v2

    .line 39
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 44
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 46
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v5, 0x7f0e01a8

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 47
    new-instance v5, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment$1;

    invoke-direct {v5, p0}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment$1;-><init>(Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;)V

    invoke-virtual {v0, v3, v1, v5}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    .line 54
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 55
    const v5, 0x104000a

    new-instance v6, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment$2;

    invoke-direct {v6, p0}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment$2;-><init>(Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 59
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    return-object v5
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 63
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 64
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/SobDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    .line 65
    return-void
.end method
