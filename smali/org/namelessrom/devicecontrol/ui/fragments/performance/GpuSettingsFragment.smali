.class public Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;
.source "GpuSettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private m3dScaling:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

.field private mFreqMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

.field private mFreqMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

.field private mGpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

.field private mRoot:Landroid/preference/PreferenceCategory;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;-><init>()V

    .line 42
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    .line 43
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    .line 44
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mGpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    .line 45
    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->m3dScaling:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    return-void
.end method

.method private getGpu()V
    .locals 11

    .prologue
    const/4 v8, 0x0

    .line 89
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    move-result-object v9

    invoke-virtual {v9}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getGpuBasePath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 90
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    move-result-object v9

    invoke-virtual {v9}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getGpu()Lorg/namelessrom/devicecontrol/hardware/GpuUtils$Gpu;

    move-result-object v2

    .line 91
    .local v2, "gpu":Lorg/namelessrom/devicecontrol/hardware/GpuUtils$Gpu;
    iget-object v1, v2, Lorg/namelessrom/devicecontrol/hardware/GpuUtils$Gpu;->available:[Ljava/lang/String;

    .line 92
    .local v1, "frequencies":[Ljava/lang/String;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->freqsToMhz([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 94
    .local v4, "gpuNames":[Ljava/lang/String;
    if-nez v1, :cond_7

    move v0, v8

    .line 95
    .local v0, "freqsLength":I
    :goto_0
    if-nez v4, :cond_8

    move v6, v8

    .line 96
    .local v6, "namesLength":I
    :goto_1
    iget-object v7, v2, Lorg/namelessrom/devicecontrol/hardware/GpuUtils$Gpu;->max:Ljava/lang/String;

    .line 97
    .local v7, "tmp":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    if-eqz v0, :cond_1

    if-ne v0, v6, :cond_1

    .line 98
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 99
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    if-ge v5, v0, :cond_0

    .line 100
    aget-object v9, v1, v5

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 101
    aget-object v7, v4, v5

    .line 106
    :cond_0
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    if-eqz v9, :cond_a

    .line 107
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-static {v7}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->fromMHz(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 108
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v9, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 122
    .end local v5    # "i":I
    :cond_1
    :goto_3
    iget-object v7, v2, Lorg/namelessrom/devicecontrol/hardware/GpuUtils$Gpu;->min:Ljava/lang/String;

    .line 123
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    if-eqz v0, :cond_3

    if-ne v0, v6, :cond_3

    .line 124
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 125
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_4
    if-ge v5, v0, :cond_2

    .line 126
    aget-object v9, v1, v5

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 127
    aget-object v7, v4, v5

    .line 132
    :cond_2
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    if-eqz v9, :cond_c

    .line 133
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-static {v7}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->fromMHz(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 134
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v9, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 148
    .end local v5    # "i":I
    :cond_3
    :goto_5
    iget-object v7, v2, Lorg/namelessrom/devicecontrol/hardware/GpuUtils$Gpu;->governor:Ljava/lang/String;

    .line 149
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_4

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GpuUtils;

    move-result-object v9

    invoke-virtual {v9, v7}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->containsGov(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 150
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mGpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    if-eqz v9, :cond_d

    .line 151
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mGpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v9, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 152
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mGpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v9, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 168
    .end local v0    # "freqsLength":I
    .end local v1    # "frequencies":[Ljava/lang/String;
    .end local v2    # "gpu":Lorg/namelessrom/devicecontrol/hardware/GpuUtils$Gpu;
    .end local v4    # "gpuNames":[Ljava/lang/String;
    .end local v6    # "namesLength":I
    .end local v7    # "tmp":Ljava/lang/String;
    :cond_4
    :goto_6
    const-string v9, "/sys/devices/gr3d/enable_3d_scaling"

    invoke-static {v9}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 169
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->m3dScaling:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-nez v9, :cond_6

    .line 170
    const-string v9, "/sys/devices/gr3d/enable_3d_scaling"

    invoke-static {v9}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 171
    .restart local v7    # "tmp":Ljava/lang/String;
    new-instance v9, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-direct {v9, v10}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->m3dScaling:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    .line 172
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->m3dScaling:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    const-string v10, "3d_scaling"

    invoke-virtual {v9, v10}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setKey(Ljava/lang/String;)V

    .line 173
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->m3dScaling:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    const v10, 0x7f0e0122

    invoke-virtual {v9, v10}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setTitle(I)V

    .line 174
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->m3dScaling:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    const v10, 0x7f0e0123

    invoke-virtual {v9, v10}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setSummary(I)V

    .line 175
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->m3dScaling:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-eqz v7, :cond_5

    const-string v10, "1"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    const/4 v8, 0x1

    :cond_5
    invoke-virtual {v9, v8}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 176
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->m3dScaling:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v8, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 177
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mRoot:Landroid/preference/PreferenceCategory;

    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->m3dScaling:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v8, v9}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 181
    .end local v7    # "tmp":Ljava/lang/String;
    :cond_6
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mRoot:Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->isSupported(Landroid/preference/PreferenceCategory;Landroid/content/Context;)V

    .line 182
    return-void

    .line 94
    .restart local v1    # "frequencies":[Ljava/lang/String;
    .restart local v2    # "gpu":Lorg/namelessrom/devicecontrol/hardware/GpuUtils$Gpu;
    .restart local v4    # "gpuNames":[Ljava/lang/String;
    :cond_7
    array-length v0, v1

    goto/16 :goto_0

    .line 95
    .restart local v0    # "freqsLength":I
    :cond_8
    array-length v6, v4

    goto/16 :goto_1

    .line 99
    .restart local v5    # "i":I
    .restart local v6    # "namesLength":I
    .restart local v7    # "tmp":Ljava/lang/String;
    :cond_9
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    .line 110
    :cond_a
    new-instance v9, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-direct {v9, v10}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    .line 111
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    const-string v10, "pref_max_gpu"

    invoke-virtual {v9, v10}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setKey(Ljava/lang/String;)V

    .line 112
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    const v10, 0x7f0e0124

    invoke-virtual {v9, v10}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setTitle(I)V

    .line 113
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v9, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 114
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v9, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 115
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-static {v7}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->fromMHz(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 116
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v9, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v9, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 118
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mRoot:Landroid/preference/PreferenceCategory;

    iget-object v10, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v9, v10}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto/16 :goto_3

    .line 125
    :cond_b
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_4

    .line 136
    :cond_c
    new-instance v9, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-direct {v9, v10}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    .line 137
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    const-string v10, "pref_min_gpu"

    invoke-virtual {v9, v10}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setKey(Ljava/lang/String;)V

    .line 138
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    const v10, 0x7f0e0125

    invoke-virtual {v9, v10}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setTitle(I)V

    .line 139
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v9, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 140
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v9, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 141
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-static {v7}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->fromMHz(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 142
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v9, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v9, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 144
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mRoot:Landroid/preference/PreferenceCategory;

    iget-object v10, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v9, v10}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto/16 :goto_5

    .line 154
    .end local v5    # "i":I
    :cond_d
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->get()Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;

    move-result-object v9

    invoke-virtual {v9}, Lorg/namelessrom/devicecontrol/hardware/GovernorUtils;->getAvailableGpuGovernors()[Ljava/lang/String;

    move-result-object v3

    .line 155
    .local v3, "gpuGovs":[Ljava/lang/String;
    new-instance v9, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-direct {v9, v10}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mGpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    .line 156
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mGpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    const-string v10, "pref_gpu_gov"

    invoke-virtual {v9, v10}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setKey(Ljava/lang/String;)V

    .line 157
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mGpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    const v10, 0x7f0e0126

    invoke-virtual {v9, v10}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setTitle(I)V

    .line 158
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mGpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v9, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 159
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mGpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v9, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 160
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mGpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v9, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 161
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mGpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v9, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 162
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mGpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v9, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 163
    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mRoot:Landroid/preference/PreferenceCategory;

    iget-object v10, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mGpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v9, v10}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto/16 :goto_6
.end method


# virtual methods
.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 48
    const v0, 0x7f0e012a

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 57
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 58
    const v5, 0x7f060010

    invoke-virtual {p0, v5}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->addPreferencesFromResource(I)V

    .line 59
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    const-string v6, "gpu"

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/PreferenceCategory;

    iput-object v5, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mRoot:Landroid/preference/PreferenceCategory;

    .line 61
    const-string v5, "gpu_opengl"

    invoke-virtual {p0, v5}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 62
    .local v0, "category":Landroid/preference/PreferenceCategory;
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->isOpenGLES20Supported()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 66
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->getOpenGLESInformation()Ljava/util/ArrayList;

    move-result-object v1

    .line 69
    .local v1, "glesInformation":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 70
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 71
    .local v4, "tmp":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 72
    new-instance v3, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-direct {v3, v5}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;-><init>(Landroid/content/Context;)V

    .line 73
    .local v3, "infoPref":Landroid/preference/Preference;
    sget-object v5, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->GL_STRINGS:[I

    aget v5, v5, v2

    invoke-virtual {v3, v5}, Landroid/preference/Preference;->setTitle(I)V

    .line 74
    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 75
    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 69
    .end local v3    # "infoPref":Landroid/preference/Preference;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 81
    .end local v1    # "glesInformation":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "i":I
    .end local v4    # "tmp":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v5

    if-gtz v5, :cond_2

    .line 82
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 84
    :cond_2
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "objVal"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    .line 185
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    if-ne v1, p1, :cond_0

    .line 186
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 187
    .local v0, "value":Ljava/lang/String;
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 188
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMax:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->toMhz(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 189
    const-string v1, "gpu_frequency_max"

    invoke-static {v1, v0, v2}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor;->processAction(Ljava/lang/String;Ljava/lang/String;Z)V

    move v1, v2

    .line 211
    .end local v0    # "value":Ljava/lang/String;
    .end local p2    # "objVal":Ljava/lang/Object;
    :goto_0
    return v1

    .line 191
    .restart local p2    # "objVal":Ljava/lang/Object;
    :cond_0
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    if-ne v1, p1, :cond_1

    .line 192
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 193
    .restart local v0    # "value":Ljava/lang/String;
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 194
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mFreqMin:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/hardware/GpuUtils;->toMhz(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 195
    const-string v1, "gpu_frequency_min"

    invoke-static {v1, v0, v2}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor;->processAction(Ljava/lang/String;Ljava/lang/String;Z)V

    move v1, v2

    .line 196
    goto :goto_0

    .line 197
    .end local v0    # "value":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mGpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    if-ne v1, p1, :cond_2

    .line 198
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 199
    .restart local v0    # "value":Ljava/lang/String;
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mGpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 200
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->mGpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 201
    const-string v1, "gpu_governor"

    invoke-static {v1, v0, v2}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor;->processAction(Ljava/lang/String;Ljava/lang/String;Z)V

    move v1, v2

    .line 202
    goto :goto_0

    .line 203
    .end local v0    # "value":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->m3dScaling:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-ne v1, p1, :cond_4

    .line 204
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "objVal":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 205
    .local v0, "value":Z
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->m3dScaling:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 206
    const-string v3, "3d_scaling"

    if-eqz v0, :cond_3

    const-string v1, "1"

    :goto_1
    invoke-static {v3, v1, v2}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor;->processAction(Ljava/lang/String;Ljava/lang/String;Z)V

    move v1, v2

    .line 208
    goto :goto_0

    .line 206
    :cond_3
    const-string v1, "0"

    goto :goto_1

    .line 211
    .end local v0    # "value":Z
    .restart local p2    # "objVal":Ljava/lang/Object;
    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 52
    invoke-super {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onResume()V

    .line 53
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/GpuSettingsFragment;->getGpu()V

    .line 54
    return-void
.end method
