.class Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;
.super Landroid/os/AsyncTask;
.source "KsmFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RefreshTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;


# direct methods
.method private constructor <init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;)V
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;
    .param p2, "x1"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$1;

    .prologue
    .line 219
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;-><init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 219
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 224
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v1, "/sys/kernel/mm/ksm/full_scans"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    const-string v1, "/sys/kernel/mm/ksm/pages_shared"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 226
    const-string v1, "/sys/kernel/mm/ksm/pages_sharing"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 227
    const-string v1, "/sys/kernel/mm/ksm/pages_unshared"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 228
    const-string v1, "/sys/kernel/mm/ksm/pages_volatile"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 230
    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 219
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 234
    .local p1, "strings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 236
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mFullScans:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 237
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 238
    .local v0, "tmp":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "strings.get(0): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 239
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mFullScans:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 241
    .end local v0    # "tmp":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesShared:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->access$200(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 242
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 243
    .restart local v0    # "tmp":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "strings.get(1): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 244
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesShared:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->access$200(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 246
    .end local v0    # "tmp":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesSharing:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->access$300(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 247
    const/4 v1, 0x2

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 248
    .restart local v0    # "tmp":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "strings.get(2): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 249
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesSharing:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->access$300(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 251
    .end local v0    # "tmp":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesUnshared:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->access$400(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 252
    const/4 v1, 0x3

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 253
    .restart local v0    # "tmp":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "strings.get(3): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 254
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesUnshared:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->access$400(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 256
    .end local v0    # "tmp":Ljava/lang/String;
    :cond_3
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesVolatile:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->access$500(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 257
    const/4 v1, 0x4

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 258
    .restart local v0    # "tmp":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "strings.get(4): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 259
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesVolatile:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->access$500(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 262
    .end local v0    # "tmp":Ljava/lang/String;
    :cond_4
    return-void
.end method
