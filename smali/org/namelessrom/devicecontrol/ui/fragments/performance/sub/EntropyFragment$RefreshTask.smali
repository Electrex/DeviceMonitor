.class Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$RefreshTask;
.super Landroid/os/AsyncTask;
.source "EntropyFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RefreshTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;


# direct methods
.method private constructor <init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;)V
    .locals 0

    .prologue
    .line 220
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;
    .param p2, "x1"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$1;

    .prologue
    .line 220
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$RefreshTask;-><init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 220
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$RefreshTask;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 223
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 225
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v1, "/proc/sys/kernel/random/entropy_avail"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 227
    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 220
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$RefreshTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 231
    .local p1, "strings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 233
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mEntropyAvail:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->access$400(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 234
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 235
    .local v0, "tmp":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "strings.get(0): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 236
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mEntropyAvail:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->access$400(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 239
    .end local v0    # "tmp":Ljava/lang/String;
    :cond_0
    return-void
.end method
