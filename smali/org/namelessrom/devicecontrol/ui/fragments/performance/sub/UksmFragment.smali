.class public Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;
.source "UksmFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$1;,
        Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;
    }
.end annotation


# instance fields
.field private mCpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

.field private mEnable:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

.field private mFullScans:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mHashStrength:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mPagesScanned:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mPagesShared:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mPagesSharing:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mRoot:Landroid/preference/PreferenceScreen;

.field private mSleep:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mSleepTimes:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;-><init>()V

    .line 230
    return-void
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;

    .prologue
    .line 65
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mPagesShared:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    return-object v0
.end method

.method static synthetic access$200(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;

    .prologue
    .line 65
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mPagesScanned:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    return-object v0
.end method

.method static synthetic access$300(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;

    .prologue
    .line 65
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mFullScans:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    return-object v0
.end method

.method static synthetic access$400(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;

    .prologue
    .line 65
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mHashStrength:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    return-object v0
.end method

.method static synthetic access$500(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;

    .prologue
    .line 65
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mPagesSharing:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    return-object v0
.end method

.method static synthetic access$600(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;

    .prologue
    .line 65
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mSleepTimes:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    return-object v0
.end method

.method private removeIfEmpty(Landroid/preference/PreferenceCategory;)V
    .locals 1
    .param p1, "preferenceCategory"    # Landroid/preference/PreferenceCategory;

    .prologue
    .line 182
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mRoot:Landroid/preference/PreferenceScreen;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 183
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mRoot:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 185
    :cond_0
    return-void
.end method


# virtual methods
.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 81
    const v0, 0x7f0e05ef

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 84
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 85
    const v4, 0x7f06000e

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->addPreferencesFromResource(I)V

    .line 86
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->setHasOptionsMenu(Z)V

    .line 88
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mRoot:Landroid/preference/PreferenceScreen;

    .line 95
    const-string v4, "uksm_info"

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 96
    .local v0, "category":Landroid/preference/PreferenceCategory;
    if-eqz v0, :cond_6

    .line 97
    const-string v4, "uksm_pages_shared"

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mPagesShared:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 98
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mPagesShared:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-eqz v4, :cond_0

    .line 99
    const-string v4, "/sys/kernel/mm/uksm/pages_shared"

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 100
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mPagesShared:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 103
    :cond_0
    const-string v4, "uksm_pages_scanned"

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mPagesScanned:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 104
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mPagesScanned:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-eqz v4, :cond_1

    .line 105
    const-string v4, "/sys/kernel/mm/uksm/pages_scanned"

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 106
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mPagesScanned:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 109
    :cond_1
    const-string v4, "uksm_full_scans"

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mFullScans:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 110
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mFullScans:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-eqz v4, :cond_2

    .line 111
    const-string v4, "/sys/kernel/mm/uksm/full_scans"

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 112
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mFullScans:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 115
    :cond_2
    const-string v4, "uksm_hash_strength"

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mHashStrength:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 116
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mHashStrength:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-eqz v4, :cond_3

    .line 117
    const-string v4, "/sys/kernel/mm/uksm/hash_strength"

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 118
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mHashStrength:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 121
    :cond_3
    const-string v4, "uksm_pages_sharing"

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mPagesSharing:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 122
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mPagesSharing:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-eqz v4, :cond_4

    .line 123
    const-string v4, "/sys/kernel/mm/uksm/pages_sharing"

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 124
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mPagesSharing:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 127
    :cond_4
    const-string v4, "uksm_sleep_times"

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mSleepTimes:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 128
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mSleepTimes:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-eqz v4, :cond_5

    .line 129
    const-string v4, "/sys/kernel/mm/uksm/sleep_times"

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 130
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mSleepTimes:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 133
    :cond_5
    new-instance v4, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;-><init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$1;)V

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Void;

    invoke-virtual {v4, v5}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 135
    :cond_6
    invoke-direct {p0, v0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->removeIfEmpty(Landroid/preference/PreferenceCategory;)V

    .line 140
    const-string v4, "uksm_settings"

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .end local v0    # "category":Landroid/preference/PreferenceCategory;
    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 141
    .restart local v0    # "category":Landroid/preference/PreferenceCategory;
    if-eqz v0, :cond_9

    .line 142
    const-string v4, "uksm_run"

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mEnable:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 143
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mEnable:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    if-eqz v4, :cond_7

    .line 144
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mEnable:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v4}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 145
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mEnable:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v4}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 146
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mEnable:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v4, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 152
    :cond_7
    :goto_0
    const-string v4, "uksm_sleep"

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mSleep:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 153
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mSleep:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-eqz v4, :cond_8

    .line 154
    const-string v4, "/sys/kernel/mm/uksm/sleep_millisecs"

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 155
    const-string v4, "/sys/kernel/mm/uksm/sleep_millisecs"

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 156
    .local v1, "tmpString":Ljava/lang/String;
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mSleep:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v4, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 161
    .end local v1    # "tmpString":Ljava/lang/String;
    :cond_8
    :goto_1
    const-string v4, "uksm_governor"

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mCpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    .line 162
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mCpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    if-eqz v4, :cond_9

    .line 163
    const-string v4, "/sys/kernel/mm/uksm/cpu_governor"

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 164
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/UksmUtils;->get()Lorg/namelessrom/devicecontrol/hardware/UksmUtils;

    move-result-object v4

    invoke-virtual {v4}, Lorg/namelessrom/devicecontrol/hardware/UksmUtils;->getAvailableCpuGovernors()[Ljava/lang/String;

    move-result-object v3

    .line 165
    .local v3, "uksm_cpu_governors":[Ljava/lang/String;
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/UksmUtils;->get()Lorg/namelessrom/devicecontrol/hardware/UksmUtils;

    move-result-object v4

    invoke-virtual {v4}, Lorg/namelessrom/devicecontrol/hardware/UksmUtils;->getCurrentCpuGovernor()Ljava/lang/String;

    move-result-object v2

    .line 166
    .local v2, "uksm_cpu_governor":Ljava/lang/String;
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mCpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v4, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 167
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mCpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v4, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 168
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mCpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v4, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 169
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mCpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v4, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 170
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mCpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v4, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 176
    .end local v2    # "uksm_cpu_governor":Ljava/lang/String;
    .end local v3    # "uksm_cpu_governors":[Ljava/lang/String;
    :cond_9
    :goto_2
    invoke-direct {p0, v0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->removeIfEmpty(Landroid/preference/PreferenceCategory;)V

    .line 178
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mRoot:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->isSupported(Landroid/preference/PreferenceScreen;Landroid/content/Context;)V

    .line 179
    return-void

    .line 148
    :cond_a
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mEnable:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 158
    :cond_b
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mSleep:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1

    .line 172
    :cond_c
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mSleep:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 215
    invoke-super {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 216
    const v0, 0x7f100003

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 217
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v3, 0x0

    .line 220
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 221
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    .line 227
    :goto_0
    return v3

    .line 223
    :pswitch_0
    new-instance v1, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;-><init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$1;)V

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 221
    nop

    :pswitch_data_0
    .packed-switch 0x7f0c0111
        :pswitch_0
    .end packed-switch
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    .line 201
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mEnable:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    if-ne v2, p1, :cond_0

    .line 202
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mEnable:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->writeValue(Z)V

    .line 211
    :goto_0
    return v1

    .line 204
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_0
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mCpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    if-ne v2, p1, :cond_1

    .line 205
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 206
    .local v0, "value":Ljava/lang/String;
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mCpuGovernor:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v2, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 207
    const-string v2, "uksm_governor"

    invoke-static {v2, v0, v1}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor;->processAction(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 211
    .end local v0    # "value":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 8
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 189
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mSleep:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne v0, p2, :cond_0

    .line 190
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mSleep:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 191
    .local v2, "title":Ljava/lang/String;
    const-string v0, "/sys/kernel/mm/uksm/sleep_millisecs"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 192
    .local v1, "currentProgress":I
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/16 v3, 0x32

    const/16 v4, 0x3e8

    const-string v6, "/sys/kernel/mm/uksm/sleep_millisecs"

    const-string v7, "extras"

    move-object v5, p2

    invoke-static/range {v0 .. v7}, Lorg/namelessrom/devicecontrol/utils/DialogHelper;->openSeekbarDialog(Landroid/app/Activity;ILjava/lang/String;IILandroid/preference/Preference;Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    const/4 v0, 0x1

    .line 197
    .end local v1    # "currentProgress":I
    .end local v2    # "title":Ljava/lang/String;
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
