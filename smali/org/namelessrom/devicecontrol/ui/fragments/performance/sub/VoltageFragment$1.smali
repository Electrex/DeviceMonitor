.class Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1;
.super Ljava/lang/Object;
.source "VoltageFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

.field final synthetic val$list:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;Landroid/widget/ListView;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1;->val$list:Landroid/widget/ListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 108
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mCategory:Landroid/preference/PreferenceCategory;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$000(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)Landroid/preference/PreferenceCategory;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v0

    .line 110
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 111
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mCategory:Landroid/preference/PreferenceCategory;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$000(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)Landroid/preference/PreferenceCategory;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 112
    .local v2, "pref":Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mValues:[Ljava/lang/String;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v1

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mValues:[Ljava/lang/String;
    invoke-static {v4}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v2, v3, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->restoreSummaryKey(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 114
    .end local v2    # "pref":Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    :cond_0
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mButtonLayout:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$200(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)Landroid/widget/LinearLayout;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 115
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1;->val$list:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->bringToFront()V

    .line 116
    return-void
.end method
