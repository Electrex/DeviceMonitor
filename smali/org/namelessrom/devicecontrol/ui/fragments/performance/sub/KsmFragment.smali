.class public Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;
.source "KsmFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$1;,
        Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;
    }
.end annotation


# instance fields
.field private mDefer:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

.field private mEnable:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

.field private mFullScans:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mPagesShared:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mPagesSharing:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mPagesToScan:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mPagesUnshared:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mPagesVolatile:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mRoot:Landroid/preference/PreferenceScreen;

.field private mSleep:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;-><init>()V

    .line 219
    return-void
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;

    .prologue
    .line 44
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mFullScans:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    return-object v0
.end method

.method static synthetic access$200(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;

    .prologue
    .line 44
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesShared:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    return-object v0
.end method

.method static synthetic access$300(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;

    .prologue
    .line 44
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesSharing:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    return-object v0
.end method

.method static synthetic access$400(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;

    .prologue
    .line 44
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesUnshared:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    return-object v0
.end method

.method static synthetic access$500(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;

    .prologue
    .line 44
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesVolatile:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    return-object v0
.end method

.method private removeIfEmpty(Landroid/preference/PreferenceCategory;)V
    .locals 1
    .param p1, "preferenceCategory"    # Landroid/preference/PreferenceCategory;

    .prologue
    .line 166
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mRoot:Landroid/preference/PreferenceScreen;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 167
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mRoot:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 169
    :cond_0
    return-void
.end method


# virtual methods
.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 60
    const v0, 0x7f0e058b

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 63
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 64
    const v2, 0x7f06000c

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->addPreferencesFromResource(I)V

    .line 65
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->setHasOptionsMenu(Z)V

    .line 67
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mRoot:Landroid/preference/PreferenceScreen;

    .line 74
    const-string v2, "ksm_info"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 75
    .local v0, "category":Landroid/preference/PreferenceCategory;
    if-eqz v0, :cond_5

    .line 76
    const-string v2, "ksm_full_scans"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mFullScans:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 77
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mFullScans:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-eqz v2, :cond_0

    .line 78
    const-string v2, "/sys/kernel/mm/ksm/full_scans"

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 79
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mFullScans:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 83
    :cond_0
    const-string v2, "ksm_pages_shared"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesShared:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 84
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesShared:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-eqz v2, :cond_1

    .line 85
    const-string v2, "/sys/kernel/mm/ksm/pages_shared"

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 86
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesShared:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 90
    :cond_1
    const-string v2, "ksm_pages_sharing"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesSharing:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 91
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesSharing:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-eqz v2, :cond_2

    .line 92
    const-string v2, "/sys/kernel/mm/ksm/pages_sharing"

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 93
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesSharing:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 97
    :cond_2
    const-string v2, "ksm_pages_unshared"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesUnshared:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 98
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesUnshared:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-eqz v2, :cond_3

    .line 99
    const-string v2, "/sys/kernel/mm/ksm/pages_unshared"

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 100
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesUnshared:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 104
    :cond_3
    const-string v2, "ksm_pages_volatile"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesVolatile:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 105
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesVolatile:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-eqz v2, :cond_4

    .line 106
    const-string v2, "/sys/kernel/mm/ksm/pages_volatile"

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 107
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesVolatile:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 111
    :cond_4
    new-instance v2, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;-><init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$1;)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 113
    :cond_5
    invoke-direct {p0, v0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->removeIfEmpty(Landroid/preference/PreferenceCategory;)V

    .line 118
    const-string v2, "ksm_settings"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .end local v0    # "category":Landroid/preference/PreferenceCategory;
    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 119
    .restart local v0    # "category":Landroid/preference/PreferenceCategory;
    if-eqz v0, :cond_9

    .line 120
    const-string v2, "ksm_run"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mEnable:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 121
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mEnable:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    if-eqz v2, :cond_6

    .line 122
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mEnable:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 123
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mEnable:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 124
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mEnable:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 130
    :cond_6
    :goto_0
    const-string v2, "ksm_deferred"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mDefer:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 131
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mDefer:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    if-eqz v2, :cond_7

    .line 132
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mDefer:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 133
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mDefer:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 134
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mDefer:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 140
    :cond_7
    :goto_1
    const-string v2, "ksm_pages_to_scan"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesToScan:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 141
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesToScan:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-eqz v2, :cond_8

    .line 142
    const-string v2, "/sys/kernel/mm/ksm/pages_to_scan"

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 143
    const-string v2, "/sys/kernel/mm/ksm/pages_to_scan"

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 144
    .local v1, "tmpString":Ljava/lang/String;
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesToScan:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v2, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 150
    .end local v1    # "tmpString":Ljava/lang/String;
    :cond_8
    :goto_2
    const-string v2, "ksm_sleep"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mSleep:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 151
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mSleep:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-eqz v2, :cond_9

    .line 152
    const-string v2, "/sys/kernel/mm/ksm/sleep_millisecs"

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 153
    const-string v2, "/sys/kernel/mm/ksm/sleep_millisecs"

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 154
    .restart local v1    # "tmpString":Ljava/lang/String;
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mSleep:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v2, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 160
    .end local v1    # "tmpString":Ljava/lang/String;
    :cond_9
    :goto_3
    invoke-direct {p0, v0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->removeIfEmpty(Landroid/preference/PreferenceCategory;)V

    .line 162
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mRoot:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->isSupported(Landroid/preference/PreferenceScreen;Landroid/content/Context;)V

    .line 163
    return-void

    .line 126
    :cond_a
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mEnable:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 136
    :cond_b
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mDefer:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1

    .line 146
    :cond_c
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesToScan:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_2

    .line 156
    :cond_d
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mSleep:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_3
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 204
    invoke-super {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 205
    const v0, 0x7f100003

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 206
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v3, 0x0

    .line 209
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 210
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    .line 216
    :goto_0
    return v3

    .line 212
    :pswitch_0
    new-instance v1, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;-><init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$1;)V

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment$RefreshTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 210
    nop

    :pswitch_data_0
    .packed-switch 0x7f0c0111
        :pswitch_0
    .end packed-switch
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    .line 192
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mEnable:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    if-ne v1, p1, :cond_0

    .line 193
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mEnable:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->writeValue(Z)V

    .line 200
    :goto_0
    return v0

    .line 195
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_0
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mDefer:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    if-ne v1, p1, :cond_1

    .line 196
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mDefer:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->writeValue(Z)V

    goto :goto_0

    .line 200
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 12
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v3, 0x1

    .line 173
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesToScan:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne v0, p2, :cond_0

    .line 174
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mPagesToScan:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 175
    .local v2, "title":Ljava/lang/String;
    const-string v0, "/sys/kernel/mm/ksm/pages_to_scan"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 177
    .local v1, "currentProgress":I
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/16 v4, 0x400

    const-string v6, "/sys/kernel/mm/ksm/pages_to_scan"

    const-string v7, "extras"

    move-object v5, p2

    invoke-static/range {v0 .. v7}, Lorg/namelessrom/devicecontrol/utils/DialogHelper;->openSeekbarDialog(Landroid/app/Activity;ILjava/lang/String;IILandroid/preference/Preference;Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    .end local v1    # "currentProgress":I
    .end local v2    # "title":Ljava/lang/String;
    :goto_0
    return v3

    .line 180
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mSleep:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne v0, p2, :cond_1

    .line 181
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->mSleep:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 182
    .restart local v2    # "title":Ljava/lang/String;
    const-string v0, "/sys/kernel/mm/ksm/sleep_millisecs"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 183
    .restart local v1    # "currentProgress":I
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/KsmFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const/16 v7, 0x32

    const/16 v8, 0x1388

    const-string v10, "/sys/kernel/mm/ksm/sleep_millisecs"

    const-string v11, "extras"

    move v5, v1

    move-object v6, v2

    move-object v9, p2

    invoke-static/range {v4 .. v11}, Lorg/namelessrom/devicecontrol/utils/DialogHelper;->openSeekbarDialog(Landroid/app/Activity;ILjava/lang/String;IILandroid/preference/Preference;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 188
    .end local v1    # "currentProgress":I
    .end local v2    # "title":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method
