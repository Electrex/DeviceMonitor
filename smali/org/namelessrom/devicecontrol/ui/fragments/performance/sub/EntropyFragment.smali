.class public Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceProgressFragment;
.source "EntropyFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$RefreshTask;
    }
.end annotation


# static fields
.field private static final RNGD:Ljava/io/File;


# instance fields
.field private mEntropyAvail:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mReadWakeupThreshold:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

.field private mRngActive:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

.field private mRngStartup:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

.field private mWriteWakeupThreshold:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 54
    new-instance v0, Ljava/io/File;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/Application;->getFilesDirectory()Ljava/lang/String;

    move-result-object v1

    const-string v2, "rngd"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->RNGD:Ljava/io/File;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceProgressFragment;-><init>()V

    .line 220
    return-void
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mRngActive:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    return-object v0
.end method

.method static synthetic access$200()Ljava/io/File;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->RNGD:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$300(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;)Lcom/daimajia/numberprogressbar/NumberProgressBar;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mProgressBar:Lcom/daimajia/numberprogressbar/NumberProgressBar;

    return-object v0
.end method

.method static synthetic access$400(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mEntropyAvail:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    return-object v0
.end method

.method public static restore(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 214
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    move-result-object v0

    iget-boolean v0, v0, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->rngStartup:Z

    if-eqz v0, :cond_0

    .line 215
    const-string v0, "%s -P;\n"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->RNGD:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 217
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 63
    const v0, 0x7f0e06b7

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 66
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceProgressFragment;->onCreate(Landroid/os/Bundle;)V

    .line 67
    const v1, 0x7f060009

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->addPreferencesFromResource(I)V

    .line 68
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->setHasOptionsMenu(Z)V

    .line 70
    const-string v1, "entropy"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 71
    .local v0, "category":Landroid/preference/PreferenceCategory;
    const-string v1, "entropy_avail"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mEntropyAvail:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 72
    const-string v1, "/proc/sys/kernel/random/entropy_avail"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 73
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mEntropyAvail:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 75
    :cond_0
    const-string v1, "entropy_read_wakeup_threshold"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mReadWakeupThreshold:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    .line 77
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mReadWakeupThreshold:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;->isSupported()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 78
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mReadWakeupThreshold:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;->initValue()V

    .line 79
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mReadWakeupThreshold:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 83
    :goto_0
    const-string v1, "entropy_write_wakeup_threshold"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mWriteWakeupThreshold:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    .line 85
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mWriteWakeupThreshold:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;->isSupported()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 86
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mWriteWakeupThreshold:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;->initValue()V

    .line 87
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mWriteWakeupThreshold:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 93
    :goto_1
    const-string v1, "rng_startup"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mRngStartup:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    .line 94
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mRngStartup:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    move-result-object v2

    iget-boolean v2, v2, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->rngStartup:Z

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 95
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mRngStartup:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 98
    const-string v1, "rng_active"

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mRngActive:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    .line 99
    sget-object v1, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->RNGD:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->getProcess(Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;Ljava/lang/String;)V

    .line 100
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mRngActive:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 102
    new-instance v1, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$RefreshTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$RefreshTask;-><init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$1;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$RefreshTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 104
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->isSupported(Landroid/preference/PreferenceScreen;Landroid/content/Context;)V

    .line 105
    return-void

    .line 81
    :cond_1
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mReadWakeupThreshold:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 89
    :cond_2
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mWriteWakeupThreshold:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 177
    invoke-super {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceProgressFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 178
    const v0, 0x7f100003

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 179
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v3, 0x0

    .line 182
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 183
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    .line 191
    :goto_0
    return v3

    .line 185
    :pswitch_0
    new-instance v1, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$RefreshTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$RefreshTask;-><init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$1;)V

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$RefreshTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 186
    sget-object v1, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->RNGD:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->getProcess(Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;Ljava/lang/String;)V

    goto :goto_0

    .line 183
    :pswitch_data_0
    .packed-switch 0x7f0c0111
        :pswitch_0
    .end packed-switch
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 108
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mReadWakeupThreshold:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    if-ne v3, p1, :cond_0

    .line 109
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 110
    .local v0, "value":Ljava/lang/String;
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mReadWakeupThreshold:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    invoke-virtual {v2, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;->writeValue(Ljava/lang/String;)V

    .line 167
    .end local v0    # "value":Ljava/lang/String;
    .end local p2    # "o":Ljava/lang/Object;
    :goto_0
    return v1

    .line 112
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_0
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mWriteWakeupThreshold:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    if-ne v3, p1, :cond_1

    .line 113
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 114
    .restart local v0    # "value":Ljava/lang/String;
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mWriteWakeupThreshold:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    invoke-virtual {v2, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;->writeValue(Ljava/lang/String;)V

    goto :goto_0

    .line 116
    .end local v0    # "value":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mRngStartup:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-ne v3, p1, :cond_2

    .line 117
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    move-result-object v2

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iput-boolean v3, v2, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->rngStartup:Z

    .line 118
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    move-result-object v2

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    goto :goto_0

    .line 120
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_2
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mRngActive:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-ne v3, p1, :cond_5

    .line 121
    sget-object v3, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->RNGD:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3

    .line 122
    const-string v3, "%s does not exist, downloading..."

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v4, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->RNGD:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {p0, v3, v1}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 123
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mRngActive:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setEnabled(Z)V

    .line 124
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mProgressBar:Lcom/daimajia/numberprogressbar/NumberProgressBar;

    invoke-virtual {v1, v2}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->setVisibility(I)V

    .line 127
    invoke-static {p0}, Lcom/koushikdutta/ion/Ion;->with(Landroid/support/v4/app/Fragment;)Lcom/koushikdutta/ion/builder/LoadBuilder;

    move-result-object v1

    const-string v3, "http://sourceforge.net/projects/namelessrom/files/romextras/binaries/rngd/download"

    invoke-interface {v1, v3}, Lcom/koushikdutta/ion/builder/LoadBuilder;->load(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/koushikdutta/ion/builder/Builders$Any$B;

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mProgressBar:Lcom/daimajia/numberprogressbar/NumberProgressBar;

    invoke-interface {v1, v3}, Lcom/koushikdutta/ion/builder/Builders$Any$B;->progress(Lcom/koushikdutta/ion/ProgressCallback;)Lcom/koushikdutta/ion/builder/RequestBuilder;

    move-result-object v1

    check-cast v1, Lcom/koushikdutta/ion/builder/Builders$Any$B;

    sget-object v3, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->RNGD:Ljava/io/File;

    invoke-interface {v1, v3}, Lcom/koushikdutta/ion/builder/Builders$Any$B;->write(Ljava/io/File;)Lcom/koushikdutta/ion/future/ResponseFuture;

    move-result-object v1

    new-instance v3, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$1;

    invoke-direct {v3, p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$1;-><init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;)V

    invoke-interface {v1, v3}, Lcom/koushikdutta/ion/future/ResponseFuture;->setCallback(Lcom/koushikdutta/async/future/FutureCallback;)Lcom/koushikdutta/async/future/Future;

    move v1, v2

    .line 151
    goto :goto_0

    .line 154
    :cond_3
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->setRngdPermissions()V

    .line 156
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 157
    const-string v3, "Starting rngd"

    invoke-static {p0, v3}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    const-string v3, "%s -P;\n"

    new-array v4, v1, [Ljava/lang/Object;

    sget-object v5, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->RNGD:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;)V

    .line 163
    :goto_1
    sget-object v2, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->RNGD:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->getProcess(Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 160
    :cond_4
    const-string v2, "Stopping rngd"

    invoke-static {p0, v2}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    sget-object v2, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->RNGD:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->killProcess(Ljava/lang/String;)V

    goto :goto_1

    .restart local p2    # "o":Ljava/lang/Object;
    :cond_5
    move v1, v2

    .line 167
    goto/16 :goto_0
.end method

.method public onShellOutput(Lorg/namelessrom/devicecontrol/objects/ShellOutput;)V
    .locals 5
    .param p1, "shellOutput"    # Lorg/namelessrom/devicecontrol/objects/ShellOutput;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 195
    if-nez p1, :cond_1

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    iget v3, p1, Lorg/namelessrom/devicecontrol/objects/ShellOutput;->id:I

    const v4, 0x7ffffc17

    if-ne v3, v4, :cond_0

    .line 198
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mRngActive:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-eqz v3, :cond_0

    .line 199
    iget-object v3, p1, Lorg/namelessrom/devicecontrol/objects/ShellOutput;->output:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p1, Lorg/namelessrom/devicecontrol/objects/ShellOutput;->output:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    move v0, v1

    .line 201
    .local v0, "isActive":Z
    :goto_1
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mRngActive:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v3, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 202
    sget-object v3, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->RNGD:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3

    .line 203
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mRngActive:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    const v3, 0x7f0e013f

    invoke-virtual {v1, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setSummary(I)V

    .line 204
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mRngStartup:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setEnabled(Z)V

    goto :goto_0

    .end local v0    # "isActive":Z
    :cond_2
    move v0, v2

    .line 199
    goto :goto_1

    .line 206
    .restart local v0    # "isActive":Z
    :cond_3
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mRngActive:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    const-string v3, ""

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 207
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mRngStartup:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v2, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setEnabled(Z)V

    goto :goto_0
.end method

.method public setRngdPermissions()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 171
    const-string v0, "RNGD --> setReadable: %s, setExectuable: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->RNGD:Ljava/io/File;

    invoke-virtual {v2, v4, v3}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v3

    sget-object v2, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->RNGD:Ljava/io/File;

    invoke-virtual {v2, v4, v3}, Ljava/io/File;->setExecutable(ZZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {p0, v0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 174
    return-void
.end method
