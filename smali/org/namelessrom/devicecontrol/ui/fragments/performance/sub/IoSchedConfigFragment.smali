.class public Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/IoSchedConfigFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;
.source "IoSchedConfigFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 14
    const v0, 0x7f0e04e4

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "paramBundle"    # Landroid/os/Bundle;

    .prologue
    .line 18
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 19
    const v0, 0x7f06000b

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/IoSchedConfigFragment;->addPreferencesFromResource(I)V

    .line 21
    const-string v0, "cat_io_config"

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/IoSchedConfigFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreferenceCategory;

    .line 24
    .local v5, "ioCat":Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreferenceCategory;
    const-string v11, "/sys/block/mmcblk0/queue/iosched/"

    .line 25
    .local v11, "path":Ljava/lang/String;
    const-string v0, "/sys/block/mmcblk0/queue/iosched/"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->listFiles(Ljava/lang/String;Z)[Ljava/lang/String;

    move-result-object v8

    .line 26
    .local v8, "files":[Ljava/lang/String;
    if-eqz v8, :cond_0

    array-length v0, v8

    if-nez v0, :cond_2

    .line 27
    :cond_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/IoSchedConfigFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 38
    :cond_1
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/IoSchedConfigFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/IoSchedConfigFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0e0174

    invoke-virtual {p0, v0, v1, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/IoSchedConfigFragment;->isSupported(Landroid/preference/PreferenceScreen;Landroid/content/Context;I)V

    .line 39
    return-void

    .line 29
    :cond_2
    move-object v7, v8

    .local v7, "arr$":[Ljava/lang/String;
    array-length v10, v7

    .local v10, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_0
    if-ge v9, v10, :cond_1

    aget-object v4, v7, v9

    .line 30
    .local v4, "file":Ljava/lang/String;
    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->getType(Ljava/lang/String;)I

    move-result v12

    .line 31
    .local v12, "type":I
    if-nez v12, :cond_3

    .line 32
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/IoSchedConfigFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "io_conf_"

    const-string v2, "extras"

    const-string v3, "/sys/block/mmcblk0/queue/iosched/"

    move-object v6, p0

    invoke-static/range {v0 .. v6}, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->addAwesomeEditTextPreference(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/preference/PreferenceCategory;Landroid/preference/Preference$OnPreferenceChangeListener;)Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    .line 29
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_0
.end method
