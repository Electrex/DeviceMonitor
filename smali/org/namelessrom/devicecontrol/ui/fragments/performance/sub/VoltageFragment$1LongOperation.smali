.class Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;
.super Landroid/os/AsyncTask;
.source "VoltageFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->addPreferences(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LongOperation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

.field final synthetic val$millivolts:Z


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 220
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    iput-boolean p2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->val$millivolts:Z

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 220
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 224
    :try_start_0
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/VoltageUtils;->get()Lorg/namelessrom/devicecontrol/hardware/VoltageUtils;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lorg/namelessrom/devicecontrol/hardware/VoltageUtils;->getUvValues(Z)[Ljava/lang/String;

    move-result-object v7

    # setter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mNames:[Ljava/lang/String;
    invoke-static {v6, v7}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$502(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;[Ljava/lang/String;)[Ljava/lang/String;

    .line 225
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/VoltageUtils;->get()Lorg/namelessrom/devicecontrol/hardware/VoltageUtils;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lorg/namelessrom/devicecontrol/hardware/VoltageUtils;->getUvValues(Z)[Ljava/lang/String;

    move-result-object v7

    # setter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mValues:[Ljava/lang/String;
    invoke-static {v6, v7}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$102(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;[Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "UV TABLE: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    iget-object v8, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mValues:[Ljava/lang/String;
    invoke-static {v8}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)[Ljava/lang/String;

    move-result-object v8

    # invokes: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->buildTable([Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v7, v8}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$400(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 234
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mNames:[Ljava/lang/String;
    invoke-static {v6}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$500(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)[Ljava/lang/String;

    move-result-object v6

    array-length v3, v6

    .line 235
    .local v3, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_1

    .line 236
    move v2, v1

    .line 237
    .local v2, "j":I
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mNames:[Ljava/lang/String;
    invoke-static {v6}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$500(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)[Ljava/lang/String;

    move-result-object v6

    aget-object v4, v6, v1

    .line 238
    .local v4, "name":Ljava/lang/String;
    new-instance v5, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    invoke-virtual {v6}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;-><init>(Landroid/content/Context;)V

    .line 239
    .local v5, "pref":Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-virtual {v5, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 240
    iget-boolean v6, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->val$millivolts:Z

    invoke-virtual {v5, v6}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->areMilliVolts(Z)V

    .line 241
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->isVdd:Z
    invoke-static {v6}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$300(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 242
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mValues:[Ljava/lang/String;
    invoke-static {v6}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 246
    :goto_1
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mValues:[Ljava/lang/String;
    invoke-static {v6}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setKey(Ljava/lang/String;)V

    .line 247
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mCategory:Landroid/preference/PreferenceCategory;
    invoke-static {v6}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$000(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)Landroid/preference/PreferenceCategory;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 249
    new-instance v6, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1;

    invoke-direct {v6, p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1;-><init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;I)V

    invoke-virtual {v5, v6}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 235
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 226
    .end local v1    # "i":I
    .end local v2    # "j":I
    .end local v3    # "length":I
    .end local v4    # "name":Ljava/lang/String;
    .end local v5    # "pref":Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    :catch_0
    move-exception v0

    .line 227
    .local v0, "exc":Ljava/lang/Exception;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "UV ERROR: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    .line 228
    const-string v6, "ERROR"

    .line 317
    .end local v0    # "exc":Ljava/lang/Exception;
    :goto_2
    return-object v6

    .line 244
    .restart local v1    # "i":I
    .restart local v2    # "j":I
    .restart local v3    # "length":I
    .restart local v4    # "name":Ljava/lang/String;
    .restart local v5    # "pref":Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mValues:[Ljava/lang/String;
    invoke-static {v7}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)[Ljava/lang/String;

    move-result-object v7

    aget-object v7, v7, v1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mV"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 317
    .end local v2    # "j":I
    .end local v4    # "name":Ljava/lang/String;
    .end local v5    # "pref":Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    :cond_1
    const-string v6, "Executed"

    goto :goto_2
.end method
