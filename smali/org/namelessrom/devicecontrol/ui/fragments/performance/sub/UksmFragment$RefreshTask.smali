.class Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;
.super Landroid/os/AsyncTask;
.source "UksmFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RefreshTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;


# direct methods
.method private constructor <init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;
    .param p2, "x1"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$1;

    .prologue
    .line 230
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;-><init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 230
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 233
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 235
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v1, "/sys/kernel/mm/uksm/pages_shared"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 236
    const-string v1, "/sys/kernel/mm/uksm/pages_scanned"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 237
    const-string v1, "/sys/kernel/mm/uksm/full_scans"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 238
    const-string v1, "/sys/kernel/mm/uksm/hash_strength"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 239
    const-string v1, "/sys/kernel/mm/uksm/pages_sharing"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 240
    const-string v1, "/sys/kernel/mm/uksm/sleep_times"

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 242
    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 230
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 246
    .local p1, "strings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 248
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mPagesShared:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 249
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 250
    .local v0, "tmp":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "strings.get(0): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 251
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mPagesShared:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 253
    .end local v0    # "tmp":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mPagesScanned:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->access$200(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 254
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 255
    .restart local v0    # "tmp":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "strings.get(1): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 256
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mPagesScanned:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->access$200(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 258
    .end local v0    # "tmp":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mFullScans:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->access$300(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 259
    const/4 v1, 0x2

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 260
    .restart local v0    # "tmp":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "strings.get(2): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 261
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mFullScans:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->access$300(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 263
    .end local v0    # "tmp":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mHashStrength:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->access$400(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 264
    const/4 v1, 0x3

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 265
    .restart local v0    # "tmp":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "strings.get(3): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 266
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mHashStrength:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->access$400(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 268
    .end local v0    # "tmp":Ljava/lang/String;
    :cond_3
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mPagesSharing:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->access$500(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 269
    const/4 v1, 0x4

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 270
    .restart local v0    # "tmp":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "strings.get(4): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 271
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mPagesSharing:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->access$500(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 273
    .end local v0    # "tmp":Ljava/lang/String;
    :cond_4
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mSleepTimes:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->access$600(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 274
    const/4 v1, 0x5

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 275
    .restart local v0    # "tmp":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "strings.get(5): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 276
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment$RefreshTask;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->mSleepTimes:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;->access$600(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/UksmFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 279
    .end local v0    # "tmp":Ljava/lang/String;
    :cond_5
    return-void
.end method
