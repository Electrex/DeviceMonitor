.class public Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;
.source "VoltageFragment.java"


# instance fields
.field private isVdd:Z

.field private mButtonLayout:Landroid/widget/LinearLayout;

.field private mCategory:Landroid/preference/PreferenceCategory;

.field private mNames:[Ljava/lang/String;

.field private mValues:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->isVdd:Z

    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)Landroid/preference/PreferenceCategory;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mCategory:Landroid/preference/PreferenceCategory;

    return-object v0
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mValues:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;
    .param p1, "x1"    # [Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mValues:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mButtonLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$300(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)Z
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    .prologue
    .line 54
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->isVdd:Z

    return v0
.end method

.method static synthetic access$400(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;[Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;
    .param p1, "x1"    # [Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->buildTable([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mNames:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;
    .param p1, "x1"    # [Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mNames:[Ljava/lang/String;

    return-object p1
.end method

.method private buildTable([Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "vals"    # [Ljava/lang/String;

    .prologue
    .line 324
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 325
    .local v2, "sb":Ljava/lang/StringBuilder;
    array-length v1, p1

    .line 326
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 327
    add-int/lit8 v3, v1, -0x1

    if-eq v0, v3, :cond_0

    .line 328
    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 326
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 330
    :cond_0
    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 333
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private changeVoltage(Z)V
    .locals 8
    .param p1, "isPlus"    # Z

    .prologue
    const/16 v7, 0x61a8

    const/16 v6, 0x19

    .line 184
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v5}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v3

    .line 187
    .local v3, "prefsIndex":I
    const/4 v1, 0x0

    .line 188
    .local v1, "isCurrent":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_4

    .line 189
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v5, v0}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 190
    .local v2, "pref":Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    if-eqz v2, :cond_0

    .line 191
    iget-boolean v5, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->isVdd:Z

    if-eqz v5, :cond_2

    .line 192
    if-eqz p1, :cond_1

    .line 193
    invoke-virtual {v2, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setCustomSummaryKeyPlus(I)V

    .line 204
    :goto_1
    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->getKey()Ljava/lang/String;

    move-result-object v4

    .line 205
    .local v4, "value":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 206
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mValues:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 188
    .end local v4    # "value":Ljava/lang/String;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 195
    :cond_1
    invoke-virtual {v2, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setCustomSummaryKeyMinus(I)V

    goto :goto_1

    .line 198
    :cond_2
    if-eqz p1, :cond_3

    .line 199
    invoke-virtual {v2, v6}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setCustomSummaryKeyPlus(I)V

    goto :goto_1

    .line 201
    :cond_3
    invoke-virtual {v2, v6}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setCustomSummaryKeyMinus(I)V

    goto :goto_1

    .line 211
    .end local v2    # "pref":Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    :cond_4
    if-eqz v1, :cond_5

    .line 212
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mButtonLayout:Landroid/widget/LinearLayout;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 216
    :goto_2
    return-void

    .line 214
    :cond_5
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mButtonLayout:Landroid/widget/LinearLayout;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2
.end method

.method public static restore(Landroid/content/Context;)Ljava/lang/String;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 337
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 339
    .local v4, "restore":Ljava/lang/StringBuilder;
    const-string v8, "/sys/devices/system/cpu/cpufreq/vdd_table/vdd_levels"

    invoke-static {v8}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 340
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    move-result-object v8

    iget-object v6, v8, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->vdd:Ljava/lang/String;

    .line 341
    .local v6, "value":Ljava/lang/String;
    const-class v8, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "VDD Table: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 343
    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    .line 344
    const-string v8, "XXX"

    invoke-virtual {v6, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 345
    .local v7, "values":[Ljava/lang/String;
    move-object v0, v7

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v5, v0, v2

    .line 346
    .local v5, "s":Ljava/lang/String;
    const-string v8, "/sys/devices/system/cpu/cpufreq/vdd_table/vdd_levels"

    invoke-static {v8, v5}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 349
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v5    # "s":Ljava/lang/String;
    .end local v6    # "value":Ljava/lang/String;
    .end local v7    # "values":[Ljava/lang/String;
    :cond_0
    const-string v8, "/sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table"

    invoke-static {v8}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 350
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    move-result-object v8

    iget-object v6, v8, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->uv:Ljava/lang/String;

    .line 351
    .restart local v6    # "value":Ljava/lang/String;
    const-class v8, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "UV Table: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 353
    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    .line 354
    const-string v8, "/sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table"

    invoke-static {v8, v6}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 358
    .end local v6    # "value":Ljava/lang/String;
    :cond_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 359
    .local v1, "cmd":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_2

    .line 362
    .end local v1    # "cmd":Ljava/lang/String;
    :goto_1
    return-object v1

    .restart local v1    # "cmd":Ljava/lang/String;
    :cond_2
    const-string v1, "echo \'No UV to restore\';\n"

    goto :goto_1
.end method


# virtual methods
.method public addPreferences(Z)V
    .locals 2
    .param p1, "millivolts"    # Z

    .prologue
    .line 320
    new-instance v0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;

    invoke-direct {v0, p0, p1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;-><init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;Z)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 321
    return-void
.end method

.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 62
    const v0, 0x7f0e0653

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 65
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 66
    const v0, 0x7f060014

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->addPreferencesFromResource(I)V

    .line 67
    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->setHasOptionsMenu(Z)V

    .line 69
    const-string v0, "uv_category"

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mCategory:Landroid/preference/PreferenceCategory;

    .line 71
    const-string v0, "/sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 72
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    .line 75
    :cond_0
    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->addPreferences(Z)V

    .line 76
    iput-boolean v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->isVdd:Z

    .line 91
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->isSupported(Landroid/preference/PreferenceScreen;Landroid/content/Context;)V

    .line 92
    return-void

    .line 78
    :cond_2
    const-string v0, "/sys/devices/system/cpu/cpufreq/vdd_table/vdd_levels"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 79
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v0

    if-eqz v0, :cond_3

    .line 80
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    .line 82
    :cond_3
    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->addPreferences(Z)V

    .line 83
    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->isVdd:Z

    goto :goto_0

    .line 85
    :cond_4
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 162
    invoke-super {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 163
    const-string v0, "/sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/sys/devices/system/cpu/cpufreq/vdd_table/vdd_levels"

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165
    :cond_0
    const v0, 0x7f100005

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 167
    :cond_1
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstance"    # Landroid/os/Bundle;

    .prologue
    .line 96
    const v4, 0x7f040031

    const/4 v5, 0x0

    invoke-virtual {p1, v4, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 98
    .local v3, "v":Landroid/view/View;
    const v4, 0x102000a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 100
    .local v0, "list":Landroid/widget/ListView;
    const v4, 0x7f0c009e

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mButtonLayout:Landroid/widget/LinearLayout;

    .line 101
    const v4, 0x7f0c00a0

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 102
    .local v1, "mButtonApply":Landroid/widget/Button;
    const v4, 0x7f0c009f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 104
    .local v2, "mButtonCancel":Landroid/widget/Button;
    new-instance v4, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1;

    invoke-direct {v4, p0, v0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1;-><init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;Landroid/widget/ListView;)V

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    new-instance v4, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$2;

    invoke-direct {v4, p0, v0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$2;-><init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;Landroid/widget/ListView;)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 156
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mButtonLayout:Landroid/widget/LinearLayout;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 158
    return-object v3
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 170
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 171
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    move v1, v2

    .line 180
    :goto_0
    return v1

    .line 173
    :pswitch_0
    invoke-direct {p0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->changeVoltage(Z)V

    goto :goto_0

    .line 176
    :pswitch_1
    invoke-direct {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->changeVoltage(Z)V

    goto :goto_0

    .line 171
    nop

    :pswitch_data_0
    .packed-switch 0x7f0c0118
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
