.class Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$2;
.super Ljava/lang/Object;
.source "VoltageFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

.field final synthetic val$list:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;Landroid/widget/ListView;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$2;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$2;->val$list:Landroid/widget/ListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 124
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$2;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mCategory:Landroid/preference/PreferenceCategory;
    invoke-static {v7}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$000(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)Landroid/preference/PreferenceCategory;

    move-result-object v7

    invoke-virtual {v7}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v0

    .line 126
    .local v0, "count":I
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$2;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->isVdd:Z
    invoke-static {v7}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$300(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 128
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 129
    .local v4, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 130
    .local v1, "execute":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 131
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$2;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mCategory:Landroid/preference/PreferenceCategory;
    invoke-static {v7}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$000(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)Landroid/preference/PreferenceCategory;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 132
    .local v3, "pref":Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 133
    .local v6, "value":Ljava/lang/String;
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$2;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mValues:[Ljava/lang/String;
    invoke-static {v7}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)[Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->getKey()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    .line 134
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "XXX"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    const-string v7, "/sys/devices/system/cpu/cpufreq/vdd_table/vdd_levels"

    invoke-static {v7, v6}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 137
    .end local v3    # "pref":Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    .end local v6    # "value":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;)V

    .line 138
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$2;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    invoke-virtual {v7}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v7}, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    move-result-object v7

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->vdd:Ljava/lang/String;

    .line 139
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$2;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    invoke-virtual {v7}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v7}, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    move-result-object v7

    iget-object v8, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$2;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    invoke-virtual {v8}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v7, v8}, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    .line 150
    .end local v1    # "execute":Ljava/lang/StringBuilder;
    .end local v4    # "sb":Ljava/lang/StringBuilder;
    :goto_1
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$2;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mButtonLayout:Landroid/widget/LinearLayout;
    invoke-static {v7}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$200(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)Landroid/widget/LinearLayout;

    move-result-object v7

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 151
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$2;->val$list:Landroid/widget/ListView;

    invoke-virtual {v7}, Landroid/widget/ListView;->bringToFront()V

    .line 152
    return-void

    .line 141
    .end local v2    # "i":I
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    if-ge v2, v0, :cond_2

    .line 142
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$2;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mCategory:Landroid/preference/PreferenceCategory;
    invoke-static {v7}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$000(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)Landroid/preference/PreferenceCategory;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 143
    .restart local v3    # "pref":Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$2;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mValues:[Ljava/lang/String;
    invoke-static {v7}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)[Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->getKey()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    .line 141
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 145
    .end local v3    # "pref":Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    :cond_2
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$2;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    iget-object v8, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$2;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mValues:[Ljava/lang/String;
    invoke-static {v8}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)[Ljava/lang/String;

    move-result-object v8

    # invokes: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->buildTable([Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v7, v8}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$400(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 146
    .local v5, "table":Ljava/lang/String;
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$2;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    invoke-virtual {v7}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v7}, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    move-result-object v7

    iput-object v5, v7, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->uv:Ljava/lang/String;

    .line 147
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$2;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    invoke-virtual {v7}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v7}, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    move-result-object v7

    iget-object v8, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$2;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    invoke-virtual {v8}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v7, v8}, Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/ExtraConfiguration;

    .line 148
    const-string v7, "/sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table"

    invoke-static {v7, v5}, Lorg/namelessrom/devicecontrol/utils/Utils;->writeValue(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_1
.end method
