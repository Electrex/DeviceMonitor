.class Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;
.super Ljava/lang/Object;
.source "VoltageFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1;->onPreferenceClick(Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1;

.field final synthetic val$et:Landroid/widget/EditText;

.field final synthetic val$p:Landroid/preference/Preference;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1;Landroid/widget/EditText;Landroid/preference/Preference;)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;->this$2:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;->val$et:Landroid/widget/EditText;

    iput-object p3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;->val$p:Landroid/preference/Preference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 284
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;->val$et:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 285
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;->this$2:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1;

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1;->this$1:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->isVdd:Z
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$300(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 286
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;->val$p:Landroid/preference/Preference;

    invoke-virtual {v2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;->val$et:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 289
    .local v0, "value":Ljava/lang/String;
    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table"

    invoke-static {v1, v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->writeValue(Ljava/lang/String;Ljava/lang/String;)Z

    .line 291
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;->val$p:Landroid/preference/Preference;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;->val$et:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 292
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;->val$p:Landroid/preference/Preference;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;->val$et:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 293
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;->this$2:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1;

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1;->this$1:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mValues:[Ljava/lang/String;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;->this$2:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1;

    iget v2, v2, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1;->val$j:I

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;->val$p:Landroid/preference/Preference;

    invoke-virtual {v3}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 303
    .end local v0    # "value":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 295
    :cond_1
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;->val$et:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 296
    .restart local v0    # "value":Ljava/lang/String;
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;->val$p:Landroid/preference/Preference;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mV"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 297
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;->val$p:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 298
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;->this$2:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1;

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1;->this$1:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mValues:[Ljava/lang/String;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;->this$2:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1;

    iget v2, v2, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1;->val$j:I

    aput-object v0, v1, v2

    .line 299
    const-string v1, "/sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table"

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;->this$2:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1;

    iget-object v2, v2, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1;->this$1:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;

    iget-object v2, v2, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1$1;->this$2:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1;

    iget-object v3, v3, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation$1;->this$1:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;

    iget-object v3, v3, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment$1LongOperation;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->mValues:[Ljava/lang/String;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;)[Ljava/lang/String;

    move-result-object v3

    # invokes: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->buildTable([Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, v3}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;->access$400(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/VoltageFragment;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->writeValue(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method
