.class Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$1;
.super Ljava/lang/Object;
.source "EntropyFragment.java"

# interfaces
.implements Lcom/koushikdutta/async/future/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/koushikdutta/async/future/FutureCallback",
        "<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$1;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Ljava/lang/Exception;Ljava/io/File;)V
    .locals 2
    .param p1, "e"    # Ljava/lang/Exception;
    .param p2, "res"    # Ljava/io/File;

    .prologue
    .line 133
    if-eqz p1, :cond_1

    .line 134
    const-string v0, "Error downloading rngd!"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$1;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mRngActive:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$1;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mRngActive:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    move-result-object v0

    const v1, 0x7f0e00c8

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setSummary(I)V

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$1;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mRngActive:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 142
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$1;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mRngActive:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setEnabled(Z)V

    .line 145
    :cond_2
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$1;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->setRngdPermissions()V

    .line 146
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$1;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->RNGD:Ljava/io/File;
    invoke-static {}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->access$200()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->getProcess(Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$1;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->mProgressBar:Lcom/daimajia/numberprogressbar/NumberProgressBar;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;->access$300(Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment;)Lcom/daimajia/numberprogressbar/NumberProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method public bridge synthetic onCompleted(Ljava/lang/Exception;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Exception;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 131
    check-cast p2, Ljava/io/File;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/sub/EntropyFragment$1;->onCompleted(Ljava/lang/Exception;Ljava/io/File;)V

    return-void
.end method
