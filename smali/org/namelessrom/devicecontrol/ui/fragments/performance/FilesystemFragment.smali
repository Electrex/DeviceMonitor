.class public Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;
.source "FilesystemFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lorg/namelessrom/devicecontrol/hardware/IoUtils$IoSchedulerListener;


# instance fields
.field private mDynFsync:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

.field private mFstrim:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

.field private mFstrimInterval:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

.field private mFsync:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

.field private mIoScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

.field private mIoSchedulerConfigure:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mReadAhead:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

.field private mSoftwareCrc:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;-><init>()V

    return-void
.end method

.method private getFstrim()I
    .locals 3

    .prologue
    .line 286
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v2

    iget v1, v2, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->fstrimInterval:I

    .line 287
    .local v1, "value":I
    sparse-switch v1, :sswitch_data_0

    .line 311
    const/4 v0, 0x7

    .line 315
    .local v0, "position":I
    :goto_0
    return v0

    .line 289
    .end local v0    # "position":I
    :sswitch_0
    const/4 v0, 0x0

    .line 290
    .restart local v0    # "position":I
    goto :goto_0

    .line 292
    .end local v0    # "position":I
    :sswitch_1
    const/4 v0, 0x1

    .line 293
    .restart local v0    # "position":I
    goto :goto_0

    .line 295
    .end local v0    # "position":I
    :sswitch_2
    const/4 v0, 0x2

    .line 296
    .restart local v0    # "position":I
    goto :goto_0

    .line 298
    .end local v0    # "position":I
    :sswitch_3
    const/4 v0, 0x3

    .line 299
    .restart local v0    # "position":I
    goto :goto_0

    .line 301
    .end local v0    # "position":I
    :sswitch_4
    const/4 v0, 0x4

    .line 302
    .restart local v0    # "position":I
    goto :goto_0

    .line 304
    .end local v0    # "position":I
    :sswitch_5
    const/4 v0, 0x5

    .line 305
    .restart local v0    # "position":I
    goto :goto_0

    .line 307
    .end local v0    # "position":I
    :sswitch_6
    const/4 v0, 0x6

    .line 308
    .restart local v0    # "position":I
    goto :goto_0

    .line 287
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xa -> :sswitch_1
        0x14 -> :sswitch_2
        0x1e -> :sswitch_3
        0x3c -> :sswitch_4
        0x78 -> :sswitch_5
        0xf0 -> :sswitch_6
    .end sparse-switch
.end method

.method private mapReadAhead(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 189
    :try_start_0
    invoke-static {p1}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 196
    .local v1, "val":I
    :goto_0
    sparse-switch v1, :sswitch_data_0

    .line 216
    .end local p1    # "value":Ljava/lang/String;
    :goto_1
    return-object p1

    .line 190
    .end local v1    # "val":I
    .restart local p1    # "value":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 191
    .local v0, "exc":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    const/4 v1, -0x1

    .restart local v1    # "val":I
    goto :goto_0

    .line 198
    .end local v0    # "exc":Ljava/lang/Exception;
    :sswitch_0
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e01e7

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 200
    :sswitch_1
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e01e9

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 202
    :sswitch_2
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e01ee

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 204
    :sswitch_3
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e01e6

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 206
    :sswitch_4
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e01e8

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 208
    :sswitch_5
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e01eb

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 210
    :sswitch_6
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e01ec

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 212
    :sswitch_7
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e01f1

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 196
    :sswitch_data_0
    .sparse-switch
        0x80 -> :sswitch_0
        0x100 -> :sswitch_1
        0x200 -> :sswitch_2
        0x400 -> :sswitch_3
        0x800 -> :sswitch_4
        0xc00 -> :sswitch_5
        0x1000 -> :sswitch_6
        0x2000 -> :sswitch_7
    .end sparse-switch
.end method

.method private parseFstrim(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 253
    packed-switch p1, :pswitch_data_0

    .line 277
    const/16 v0, 0x1e0

    .line 280
    .local v0, "value":I
    :goto_0
    return v0

    .line 255
    .end local v0    # "value":I
    :pswitch_0
    const/4 v0, 0x5

    .line 256
    .restart local v0    # "value":I
    goto :goto_0

    .line 258
    .end local v0    # "value":I
    :pswitch_1
    const/16 v0, 0xa

    .line 259
    .restart local v0    # "value":I
    goto :goto_0

    .line 261
    .end local v0    # "value":I
    :pswitch_2
    const/16 v0, 0x14

    .line 262
    .restart local v0    # "value":I
    goto :goto_0

    .line 264
    .end local v0    # "value":I
    :pswitch_3
    const/16 v0, 0x1e

    .line 265
    .restart local v0    # "value":I
    goto :goto_0

    .line 267
    .end local v0    # "value":I
    :pswitch_4
    const/16 v0, 0x3c

    .line 268
    .restart local v0    # "value":I
    goto :goto_0

    .line 270
    .end local v0    # "value":I
    :pswitch_5
    const/16 v0, 0x78

    .line 271
    .restart local v0    # "value":I
    goto :goto_0

    .line 273
    .end local v0    # "value":I
    :pswitch_6
    const/16 v0, 0xf0

    .line 274
    .restart local v0    # "value":I
    goto :goto_0

    .line 253
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private parseFstrim(Ljava/lang/String;)I
    .locals 2
    .param p1, "position"    # Ljava/lang/String;

    .prologue
    .line 245
    :try_start_0
    invoke-static {p1}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-direct {p0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->parseFstrim(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 247
    :goto_0
    return v1

    .line 246
    :catch_0
    move-exception v0

    .line 247
    .local v0, "exc":Ljava/lang/Exception;
    const/16 v1, 0x1e0

    goto :goto_0
.end method

.method private updateIoSchedulerConfigure(Ljava/lang/String;)V
    .locals 5
    .param p1, "scheduler"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 238
    const v1, 0x7f0e0065

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {p0, v1, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 239
    .local v0, "title":Ljava/lang/String;
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mIoSchedulerConfigure:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 240
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mIoSchedulerConfigure:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v1, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setEnabled(Z)V

    .line 241
    return-void
.end method


# virtual methods
.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 58
    const v0, 0x7f0e00fc

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 61
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 62
    const v2, 0x7f06000a

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->addPreferencesFromResource(I)V

    .line 66
    const-string v2, "io"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mIoScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    .line 67
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mIoScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v2, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEnabled(Z)V

    .line 68
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/IoUtils;->get()Lorg/namelessrom/devicecontrol/hardware/IoUtils;

    move-result-object v2

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/hardware/IoUtils;->getIoScheduler(Lorg/namelessrom/devicecontrol/hardware/IoUtils$IoSchedulerListener;)V

    .line 70
    const-string v2, "io_configure"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mIoSchedulerConfigure:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 71
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mIoSchedulerConfigure:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-virtual {v2, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setEnabled(Z)V

    .line 73
    const-string v2, "read_ahead"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mReadAhead:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    .line 74
    sget-object v2, Lorg/namelessrom/devicecontrol/hardware/IoUtils;->READ_AHEAD_PATH:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 75
    .local v1, "value":Ljava/lang/String;
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mReadAhead:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v2, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 76
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mReadAhead:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-direct {p0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mapReadAhead(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mReadAhead:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 79
    const-string v2, "fsync"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mFsync:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 80
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mFsync:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 81
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mFsync:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 82
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mFsync:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 87
    :goto_0
    const-string v2, "dyn_fsync"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mDynFsync:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 88
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mDynFsync:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 89
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mDynFsync:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 90
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mDynFsync:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 95
    :goto_1
    const-string v2, "mmc_software_crc"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mSoftwareCrc:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 96
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mSoftwareCrc:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 97
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mSoftwareCrc:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 98
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mSoftwareCrc:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 103
    :goto_2
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/Emmc;->get()Lorg/namelessrom/devicecontrol/hardware/Emmc;

    move-result-object v2

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/hardware/Emmc;->canBrick()Z

    move-result v0

    .line 104
    .local v0, "canBrickEmmc":Z
    const-string v2, "fstrim"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mFstrim:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    .line 105
    if-eqz v0, :cond_3

    .line 106
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mFstrim:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v2, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setEnabled(Z)V

    .line 112
    :goto_3
    const-string v2, "fstrim_interval"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mFstrimInterval:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    .line 114
    if-eqz v0, :cond_4

    .line 115
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mFstrimInterval:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v2, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEnabled(Z)V

    .line 120
    :goto_4
    return-void

    .line 84
    .end local v0    # "canBrickEmmc":Z
    :cond_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mFsync:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 92
    :cond_1
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mDynFsync:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1

    .line 100
    :cond_2
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mSoftwareCrc:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_2

    .line 108
    .restart local v0    # "canBrickEmmc":Z
    :cond_3
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mFstrim:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v3

    iget-boolean v3, v3, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->fstrimEnabled:Z

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 109
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mFstrim:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_3

    .line 117
    :cond_4
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mFstrimInterval:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->getFstrim()I

    move-result v3

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValueIndex(I)V

    .line 118
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mFstrimInterval:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_4
.end method

.method public onIoScheduler(Lorg/namelessrom/devicecontrol/hardware/IoUtils$IoScheduler;)V
    .locals 3
    .param p1, "ioScheduler"    # Lorg/namelessrom/devicecontrol/hardware/IoUtils$IoScheduler;

    .prologue
    .line 221
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 222
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 223
    iget-object v1, p1, Lorg/namelessrom/devicecontrol/hardware/IoUtils$IoScheduler;->available:[Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lorg/namelessrom/devicecontrol/hardware/IoUtils$IoScheduler;->available:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_0

    iget-object v1, p1, Lorg/namelessrom/devicecontrol/hardware/IoUtils$IoScheduler;->current:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 225
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mIoScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iget-object v2, p1, Lorg/namelessrom/devicecontrol/hardware/IoUtils$IoScheduler;->available:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 226
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mIoScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iget-object v2, p1, Lorg/namelessrom/devicecontrol/hardware/IoUtils$IoScheduler;->available:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 227
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mIoScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iget-object v2, p1, Lorg/namelessrom/devicecontrol/hardware/IoUtils$IoScheduler;->current:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setValue(Ljava/lang/String;)V

    .line 228
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mIoScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    iget-object v2, p1, Lorg/namelessrom/devicecontrol/hardware/IoUtils$IoScheduler;->current:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 229
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mIoScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 230
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mIoScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setEnabled(Z)V

    .line 232
    iget-object v1, p1, Lorg/namelessrom/devicecontrol/hardware/IoUtils$IoScheduler;->current:Ljava/lang/String;

    invoke-direct {p0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->updateIoSchedulerConfigure(Ljava/lang/String;)V

    .line 235
    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 7
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 132
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mIoScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    if-ne p1, v4, :cond_0

    .line 133
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 134
    .local v1, "value":Ljava/lang/String;
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mIoScheduler:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v3, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 135
    const-string v3, "io_scheduler"

    invoke-static {v3, v1, v2}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor;->processAction(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 136
    invoke-direct {p0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->updateIoSchedulerConfigure(Ljava/lang/String;)V

    .line 181
    .end local v1    # "value":Ljava/lang/String;
    .end local p2    # "o":Ljava/lang/Object;
    :goto_0
    return v2

    .line 138
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_0
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mFsync:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    if-ne p1, v4, :cond_1

    .line 139
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mFsync:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v3, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->writeValue(Z)V

    goto :goto_0

    .line 141
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_1
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mDynFsync:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    if-ne p1, v4, :cond_2

    .line 142
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mDynFsync:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v3, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->writeValue(Z)V

    goto :goto_0

    .line 144
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_2
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mSoftwareCrc:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    if-ne p1, v4, :cond_3

    .line 145
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mSoftwareCrc:Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v3, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->writeValue(Z)V

    goto :goto_0

    .line 147
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_3
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mReadAhead:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    if-ne p1, v4, :cond_4

    .line 148
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 149
    .restart local v1    # "value":Ljava/lang/String;
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mReadAhead:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-direct {p0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mapReadAhead(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 150
    const-string v3, "read_ahead"

    invoke-static {v3, v1, v2}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor;->processAction(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 152
    .end local v1    # "value":Ljava/lang/String;
    :cond_4
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mFstrim:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-ne v4, p1, :cond_6

    .line 153
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 155
    .local v1, "value":Z
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v4

    iput-boolean v1, v4, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->fstrimEnabled:Z

    .line 156
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v4

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    .line 158
    if-eqz v1, :cond_5

    .line 159
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mFstrimInterval:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    invoke-virtual {v5}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->parseFstrim(Ljava/lang/String;)I

    move-result v5

    invoke-static {v4, v5}, Lorg/namelessrom/devicecontrol/utils/AlarmHelper;->setAlarmFstrim(Landroid/content/Context;I)V

    .line 164
    :goto_1
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mFstrim:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v4, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 165
    const-string v4, "mFstrim: %s"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 162
    :cond_5
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/AlarmHelper;->cancelAlarmFstrim(Landroid/content/Context;)V

    goto :goto_1

    .line 167
    .end local v1    # "value":Z
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_6
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mFstrimInterval:Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;

    if-ne v4, p1, :cond_8

    .line 168
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 169
    .local v1, "value":Ljava/lang/String;
    invoke-direct {p0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->parseFstrim(Ljava/lang/String;)I

    move-result v0

    .line 171
    .local v0, "realValue":I
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v3

    iput v0, v3, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->fstrimInterval:I

    .line 172
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v3

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    .line 174
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mFstrim:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 175
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3, v0}, Lorg/namelessrom/devicecontrol/utils/AlarmHelper;->setAlarmFstrim(Landroid/content/Context;I)V

    .line 177
    :cond_7
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mFstrimInterval: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .end local v0    # "realValue":I
    .end local v1    # "value":Ljava/lang/String;
    :cond_8
    move v2, v3

    .line 181
    goto/16 :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 2
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 124
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->mIoSchedulerConfigure:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne p2, v0, :cond_0

    .line 125
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/FilesystemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0e04e4

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/MainActivity;->loadFragment(Landroid/app/Activity;I)V

    .line 126
    const/4 v0, 0x1

    .line 128
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    goto :goto_0
.end method
