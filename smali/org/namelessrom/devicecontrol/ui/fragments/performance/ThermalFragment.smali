.class public Lorg/namelessrom/devicecontrol/ui/fragments/performance/ThermalFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;
.source "ThermalFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 35
    const v0, 0x7f0e0216

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 19
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 38
    invoke-super/range {p0 .. p1}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const v1, 0x7f06000d

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/ThermalFragment;->addPreferencesFromResource(I)V

    .line 46
    const-string v1, "core_control"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/ThermalFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v11

    check-cast v11, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 48
    .local v11, "coreControl":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    invoke-virtual {v11}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 49
    invoke-virtual {v11}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 50
    move-object/from16 v0, p0

    invoke-virtual {v11, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 58
    :goto_0
    const-string v1, "msm_thermal"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/ThermalFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;

    .line 60
    .local v7, "msmThermal":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;
    invoke-virtual {v7}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->isSupported()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 61
    invoke-virtual {v7}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->getPath()Ljava/lang/String;

    move-result-object v16

    .line 62
    .local v16, "path":Ljava/lang/String;
    const/4 v1, 0x1

    move-object/from16 v0, v16

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->listFiles(Ljava/lang/String;Z)[Ljava/lang/String;

    move-result-object v12

    .line 63
    .local v12, "files":[Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "enabled"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/ThermalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "msm_thermal_"

    const v3, 0x7f0e0217

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/ThermalFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "extras"

    invoke-virtual {v7}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->getPath()Ljava/lang/String;

    move-result-object v5

    const-string v6, "enabled"

    move-object/from16 v8, p0

    invoke-static/range {v1 .. v8}, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->addAwesomeTogglePreference(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/preference/PreferenceCategory;Landroid/preference/Preference$OnPreferenceChangeListener;)Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    move-result-object v17

    .line 67
    .local v17, "togglePref":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    if-eqz v17, :cond_0

    .line 68
    invoke-virtual/range {v17 .. v17}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setupTitle()V

    .line 71
    .end local v17    # "togglePref":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    :cond_0
    move-object v9, v12

    .local v9, "arr$":[Ljava/lang/String;
    array-length v14, v9

    .local v14, "len$":I
    const/4 v13, 0x0

    .local v13, "i$":I
    :goto_1
    if-ge v13, v14, :cond_3

    aget-object v6, v9, v13

    .line 72
    .local v6, "file":Ljava/lang/String;
    invoke-static {v6}, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->getType(Ljava/lang/String;)I

    move-result v18

    .line 73
    .local v18, "type":I
    if-nez v18, :cond_1

    .line 74
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/ThermalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string v3, "msm_thermal_"

    const-string v4, "extras"

    move-object/from16 v5, v16

    move-object/from16 v8, p0

    invoke-static/range {v2 .. v8}, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->addAwesomeEditTextPreference(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/preference/PreferenceCategory;Landroid/preference/Preference$OnPreferenceChangeListener;)Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    .line 71
    :cond_1
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 52
    .end local v6    # "file":Ljava/lang/String;
    .end local v7    # "msmThermal":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;
    .end local v9    # "arr$":[Ljava/lang/String;
    .end local v12    # "files":[Ljava/lang/String;
    .end local v13    # "i$":I
    .end local v14    # "len$":I
    .end local v16    # "path":Ljava/lang/String;
    .end local v18    # "type":I
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/ThermalFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v11}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 79
    .restart local v7    # "msmThermal":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/ThermalFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v7}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/ThermalFragment;->removeIfEmpty(Landroid/preference/PreferenceScreen;Landroid/preference/PreferenceGroup;)V

    .line 84
    const-string v1, "intelli_thermal"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/ThermalFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v10

    check-cast v10, Landroid/preference/PreferenceCategory;

    .line 85
    .local v10, "category":Landroid/preference/PreferenceCategory;
    const-string v1, "intelli_thermal_enabled"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/ThermalFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v15

    check-cast v15, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .line 87
    .local v15, "mIntelliThermal":Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
    invoke-virtual {v15}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 88
    invoke-virtual {v15}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue()V

    .line 89
    move-object/from16 v0, p0

    invoke-virtual {v15, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 93
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/ThermalFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v10}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/ThermalFragment;->removeIfEmpty(Landroid/preference/PreferenceScreen;Landroid/preference/PreferenceGroup;)V

    .line 95
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/ThermalFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/ThermalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/performance/ThermalFragment;->isSupported(Landroid/preference/PreferenceScreen;Landroid/content/Context;)V

    .line 96
    return-void

    .line 91
    :cond_4
    invoke-virtual {v10, v15}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_2
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    .line 99
    instance-of v1, p1, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    if-eqz v1, :cond_0

    .line 100
    check-cast p1, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;->writeValue(Ljava/lang/String;)V

    .line 107
    .end local p2    # "o":Ljava/lang/Object;
    :goto_0
    return v0

    .line 102
    .restart local p1    # "preference":Landroid/preference/Preference;
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v1, p1, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    if-eqz v1, :cond_1

    .line 103
    check-cast p1, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->writeValue(Z)V

    goto :goto_0

    .line 107
    .restart local p1    # "preference":Landroid/preference/Preference;
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
