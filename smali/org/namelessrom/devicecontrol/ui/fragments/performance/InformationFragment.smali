.class public Lorg/namelessrom/devicecontrol/ui/fragments/performance/InformationFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;
.source "InformationFragment.java"


# instance fields
.field private mCpuStates:Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;

.field private mDeviceStats:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 36
    const v0, 0x7f0e013a

    return v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    const v1, 0x7f04002e

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 42
    .local v0, "v":Landroid/view/View;
    const v1, 0x7f0c009c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/InformationFragment;->mDeviceStats:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    .line 43
    const v1, 0x7f0c009d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/InformationFragment;->mCpuStates:Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;

    .line 45
    return-object v0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 59
    invoke-super {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;->onPause()V

    .line 60
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/InformationFragment;->mDeviceStats:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/InformationFragment;->mDeviceStats:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->onPause()V

    .line 63
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/InformationFragment;->mCpuStates:Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;

    if-eqz v0, :cond_1

    .line 64
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/InformationFragment;->mCpuStates:Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->onPause()V

    .line 66
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;->onResume()V

    .line 50
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/InformationFragment;->mDeviceStats:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/InformationFragment;->mDeviceStats:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->onResume()V

    .line 53
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/InformationFragment;->mCpuStates:Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;

    if-eqz v0, :cond_1

    .line 54
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/performance/InformationFragment;->mCpuStates:Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->onResume()V

    .line 56
    :cond_1
    return-void
.end method
