.class final Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "FileAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ViewHolder"
.end annotation


# instance fields
.field private final icon:Landroid/widget/ImageView;

.field private final info:Landroid/widget/TextView;

.field private final name:Landroid/widget/TextView;

.field private final rootView:Landroid/view/View;


# direct methods
.method private constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->rootView:Landroid/view/View;

    .line 85
    const v0, 0x7f0c00bd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    .line 86
    const v0, 0x7f0c00be

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->name:Landroid/widget/TextView;

    .line 87
    const v0, 0x7f0c00bf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->info:Landroid/widget/TextView;

    .line 88
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/View;Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/view/View;
    .param p2, "x1"    # Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$1;

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$200(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;

    .prologue
    .line 77
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->rootView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;

    .prologue
    .line 77
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->name:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;

    .prologue
    .line 77
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$500(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;

    .prologue
    .line 77
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->info:Landroid/widget/TextView;

    return-object v0
.end method
