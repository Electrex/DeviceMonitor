.class public Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;
.super Landroid/app/ListFragment;
.source "FilePickerFragment.java"

# interfaces
.implements Lorg/namelessrom/devicecontrol/listeners/OnBackPressedListener;
.implements Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;
.implements Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerListener;


# instance fields
.field private breadcrumbs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private currentPath:Ljava/lang/String;

.field private fileType:Ljava/lang/String;

.field private mFileAdapter:Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;

.field private root:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 43
    const-string v0, "/"

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->root:Ljava/lang/String;

    .line 44
    const-string v0, "/"

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->currentPath:Ljava/lang/String;

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->breadcrumbs:Ljava/util/ArrayList;

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->fileType:Ljava/lang/String;

    return-void
.end method

.method private loadFiles(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "isBreadcrumb"    # Z

    .prologue
    const/4 v4, 0x1

    .line 64
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->currentPath:Ljava/lang/String;

    .line 65
    if-eqz p2, :cond_0

    .line 66
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->breadcrumbs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 68
    :cond_0
    const/16 v0, 0x64

    const-string v1, "ls %s"

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1, v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->getCommandResult(Lorg/namelessrom/devicecontrol/objects/ShellOutput$OnShellOutputListener;ILjava/lang/String;Z)V

    .line 69
    return-void
.end method


# virtual methods
.method public onBackPressed()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 116
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->currentPath:Ljava/lang/String;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->root:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 117
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->breadcrumbs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->breadcrumbs:Ljava/util/ArrayList;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->breadcrumbs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->breadcrumbs:Ljava/util/ArrayList;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->breadcrumbs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 120
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->breadcrumbs:Ljava/util/ArrayList;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->breadcrumbs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->loadFiles(Ljava/lang/String;Z)V

    .line 121
    const/4 v0, 0x1

    .line 123
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onFilePicked(Ljava/io/File;)V
    .locals 5
    .param p1, "f"    # Ljava/io/File;

    .prologue
    const/4 v4, 0x1

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->currentPath:Ljava/lang/String;

    .line 73
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->currentPath:Ljava/lang/String;

    const-string v1, "../"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->onBackPressed()Z

    .line 80
    :goto_0
    return-void

    .line 78
    :cond_0
    const-string v0, "onFile(%s)"

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->currentPath:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->currentPath:Ljava/lang/String;

    invoke-direct {p0, v0, v4}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->loadFiles(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onFlashItemPicked(Lorg/namelessrom/devicecontrol/objects/FlashItem;)V
    .locals 4
    .param p1, "item"    # Lorg/namelessrom/devicecontrol/objects/FlashItem;

    .prologue
    .line 83
    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/objects/FlashItem;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->fileType:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->isFiletypeMatching(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    const-string v0, "filePicked(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/objects/FlashItem;->getPath()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lorg/namelessrom/devicecontrol/activities/FilePickerActivity;

    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/activities/FilePickerActivity;

    invoke-virtual {v0, p1}, Lorg/namelessrom/devicecontrol/activities/FilePickerActivity;->onFlashItemPicked(Lorg/namelessrom/devicecontrol/objects/FlashItem;)V

    goto :goto_0
.end method

.method public onShellOutput(Lorg/namelessrom/devicecontrol/objects/ShellOutput;)V
    .locals 9
    .param p1, "shellOutput"    # Lorg/namelessrom/devicecontrol/objects/ShellOutput;

    .prologue
    .line 92
    if-nez p1, :cond_0

    .line 113
    :goto_0
    return-void

    .line 93
    :cond_0
    iget v6, p1, Lorg/namelessrom/devicecontrol/objects/ShellOutput;->id:I

    packed-switch v6, :pswitch_data_0

    goto :goto_0

    .line 95
    :pswitch_0
    iget-object v6, p1, Lorg/namelessrom/devicecontrol/objects/ShellOutput;->output:Ljava/lang/String;

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 96
    .local v4, "output":[Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    array-length v6, v4

    invoke-direct {v1, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 97
    .local v1, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->currentPath:Ljava/lang/String;

    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->root:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 98
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->currentPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "../"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    :cond_1
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_3

    aget-object v5, v0, v2

    .line 101
    .local v5, "s":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 100
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 102
    :cond_2
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->currentPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 104
    .end local v5    # "s":Ljava/lang/String;
    :cond_3
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->mFileAdapter:Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;

    invoke-virtual {v6, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->setFiles(Ljava/util/ArrayList;)V

    .line 105
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v6

    if-nez v6, :cond_4

    .line 106
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->mFileAdapter:Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;

    invoke-virtual {p0, v6}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    goto/16 :goto_0

    .line 108
    :cond_4
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->mFileAdapter:Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;

    invoke-virtual {v6}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->notifyDataSetChanged()V

    .line 109
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->getListView()Landroid/widget/ListView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/ListView;->setSelectionAfterHeaderView()V

    goto/16 :goto_0

    .line 93
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 53
    invoke-super {p0, p1, p2}, Landroid/app/ListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 55
    new-instance v0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;

    invoke-direct {v0, p0}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;-><init>(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerListener;)V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->mFileAdapter:Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;

    .line 58
    const-string v0, "zip"

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->fileType:Ljava/lang/String;

    .line 59
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->mFileAdapter:Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->fileType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->setFileType(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;

    .line 60
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->root:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;->loadFiles(Ljava/lang/String;Z)V

    .line 61
    return-void
.end method

.method public showBurger()Z
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    return v0
.end method
