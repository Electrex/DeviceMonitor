.class public Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;
.super Landroid/widget/BaseAdapter;
.source "FileAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private colorResId:I

.field private fileType:Ljava/lang/String;

.field private files:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private listener:Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerListener;

.field private final resources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerListener;)V
    .locals 1
    .param p1, "filePickerListener"    # Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerListener;

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 43
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->resources:Landroid/content/res/Resources;

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->fileType:Ljava/lang/String;

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->colorResId:I

    .line 53
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->listener:Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerListener;

    .line 54
    return-void
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;)Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerListener;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;

    .prologue
    .line 41
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->listener:Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerListener;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->files:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 63
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->files:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 65
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v2, 0x0

    .line 93
    if-nez p2, :cond_3

    .line 94
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f040038

    invoke-virtual {v4, v5, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 96
    sget-boolean v4, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-nez p2, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 97
    :cond_0
    new-instance v3, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;

    const/4 v4, 0x0

    invoke-direct {v3, p2, v4}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;-><init>(Landroid/view/View;Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$1;)V

    .line 98
    .local v3, "viewHolder":Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;
    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 103
    :goto_0
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->files:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    .line 106
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "../"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    const/4 v2, 0x1

    .line 109
    .local v2, "isDirectory":Z
    :cond_2
    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->rootView:Landroid/view/View;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->access$200(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v4

    new-instance v5, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$1;

    invoke-direct {v5, p0, v2, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$1;-><init>(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;ZLjava/io/File;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v4

    invoke-virtual {v4}, Lorg/namelessrom/devicecontrol/Application;->isDarkTheme()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v0, -0x1

    .line 123
    .local v0, "color":I
    :goto_1
    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->name:Landroid/widget/TextView;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->access$300(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    if-eqz v2, :cond_5

    .line 126
    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->icon:Landroid/widget/ImageView;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->access$400(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v4

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->resources:Landroid/content/res/Resources;

    const v6, 0x7f020067

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 128
    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->info:Landroid/widget/TextView;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->access$500(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    :goto_2
    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->name:Landroid/widget/TextView;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->access$300(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 141
    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->info:Landroid/widget/TextView;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->access$500(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->resources:Landroid/content/res/Resources;

    const v6, 0x7f0a0030

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 142
    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->icon:Landroid/widget/ImageView;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->access$400(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v4

    const-string v5, "#FFFFFF"

    invoke-static {v5}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 143
    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->icon:Landroid/widget/ImageView;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->access$400(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 145
    return-object p2

    .line 100
    .end local v0    # "color":I
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "isDirectory":Z
    .end local v3    # "viewHolder":Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;

    .restart local v3    # "viewHolder":Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;
    goto/16 :goto_0

    .line 121
    .restart local v1    # "file":Ljava/io/File;
    .restart local v2    # "isDirectory":Z
    :cond_4
    const/high16 v0, -0x1000000

    goto :goto_1

    .line 130
    .restart local v0    # "color":I
    :cond_5
    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->icon:Landroid/widget/ImageView;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->access$400(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v4

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->resources:Landroid/content/res/Resources;

    const v6, 0x7f020065

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 131
    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->info:Landroid/widget/TextView;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;->access$500(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v6, Ljava/util/Date;

    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    invoke-direct {v6, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " | "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-static {v6, v8, v9}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->fileType:Ljava/lang/String;

    invoke-static {v1, v4}, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->isFiletypeMatching(Ljava/io/File;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    iget v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->colorResId:I

    if-lez v4, :cond_7

    iget v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->colorResId:I

    :cond_6
    :goto_3
    goto/16 :goto_2

    :cond_7
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0a000f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_3
.end method

.method public setFileType(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;
    .locals 0
    .param p1, "fileType"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->fileType:Ljava/lang/String;

    .line 69
    return-object p0
.end method

.method public setFiles(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57
    .local p1, "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->files:Ljava/util/ArrayList;

    .line 58
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->files:Ljava/util/ArrayList;

    sget-object v1, Lorg/namelessrom/devicecontrol/utils/SortHelper;->sFileComparator:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 59
    return-void
.end method
