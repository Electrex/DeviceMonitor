.class Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$1;
.super Ljava/lang/Object;
.source "FileAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;

.field final synthetic val$file:Ljava/io/File;

.field final synthetic val$isDirectory:Z


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;ZLjava/io/File;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$1;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;

    iput-boolean p2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$1;->val$isDirectory:Z

    iput-object p3, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$1;->val$file:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 111
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$1;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->listener:Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerListener;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;)Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 112
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$1;->val$isDirectory:Z

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$1;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->listener:Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerListener;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;)Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerListener;

    move-result-object v0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$1;->val$file:Ljava/io/File;

    invoke-interface {v0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerListener;->onFilePicked(Ljava/io/File;)V

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$1;->this$0:Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;

    # getter for: Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->listener:Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerListener;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;->access$100(Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter;)Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerListener;

    move-result-object v0

    new-instance v1, Lorg/namelessrom/devicecontrol/objects/FlashItem;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FileAdapter$1;->val$file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/namelessrom/devicecontrol/objects/FlashItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerListener;->onFlashItemPicked(Lorg/namelessrom/devicecontrol/objects/FlashItem;)V

    goto :goto_0
.end method
