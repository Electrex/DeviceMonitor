.class public Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;
.source "WirelessFileManagerFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private mBrowseRoot:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

.field private final mHandler:Landroid/os/Handler;

.field private mPassword:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

.field private mPort:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

.field private mUseAuth:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

.field private mUsername:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

.field private mWirelessFileManager:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;-><init>()V

    .line 46
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;)Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mWirelessFileManager:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    return-object v0
.end method


# virtual methods
.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 48
    const v0, 0x7f0e0679

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 58
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 59
    const v2, 0x7f060012

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->addPreferencesFromResource(I)V

    .line 61
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    move-result-object v0

    .line 64
    .local v0, "configuration":Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;
    const-string v2, "wireless_file_manager"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mWirelessFileManager:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    .line 65
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->updateWebServerPreference()V

    .line 66
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mWirelessFileManager:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 68
    const-string v2, "wfm_root"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mBrowseRoot:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    .line 69
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mBrowseRoot:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iget-boolean v3, v0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->root:Z

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 70
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mBrowseRoot:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 72
    const-string v2, "wfm_port"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mPort:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    .line 73
    iget v2, v0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->port:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 74
    .local v1, "tmp":Ljava/lang/String;
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mPort:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v2, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mPort:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v2, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setText(Ljava/lang/String;)V

    .line 76
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mPort:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 78
    const-string v2, "wfm_auth"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mUseAuth:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    .line 79
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mUseAuth:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    iget-boolean v3, v0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->useAuth:Z

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 80
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mUseAuth:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 82
    const-string v2, "wfm_username"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mUsername:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    .line 83
    iget-object v1, v0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->username:Ljava/lang/String;

    .line 84
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mUsername:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v2, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mUsername:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v2, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setText(Ljava/lang/String;)V

    .line 86
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mUsername:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 88
    const-string v2, "wfm_password"

    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mPassword:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    .line 89
    iget-object v1, v0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->password:Ljava/lang/String;

    .line 90
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mPassword:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    const-string v3, "******"

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mPassword:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v2, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setText(Ljava/lang/String;)V

    .line 92
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mPassword:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 93
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    .line 96
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mBrowseRoot:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-ne v2, p1, :cond_0

    .line 97
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    move-result-object v2

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iput-boolean v3, v2, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->root:Z

    .line 98
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    move-result-object v2

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    .line 133
    :goto_0
    return v1

    .line 100
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_0
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mPort:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    if-ne v2, p1, :cond_1

    .line 101
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 103
    .local v0, "value":Ljava/lang/String;
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    move-result-object v2

    const/16 v3, 0x1f90

    invoke-static {v0, v3}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, v2, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->port:I

    .line 104
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    move-result-object v2

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    .line 106
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mPort:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v2, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setText(Ljava/lang/String;)V

    .line 107
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mPort:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v2, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 109
    .end local v0    # "value":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mUseAuth:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-ne v2, p1, :cond_2

    .line 110
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    move-result-object v2

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iput-boolean v3, v2, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->useAuth:Z

    .line 111
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    move-result-object v2

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    goto :goto_0

    .line 113
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_2
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mUsername:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    if-ne v2, p1, :cond_3

    .line 114
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 116
    .restart local v0    # "value":Ljava/lang/String;
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    move-result-object v2

    iput-object v0, v2, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->username:Ljava/lang/String;

    .line 117
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    move-result-object v2

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    .line 119
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mUsername:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v2, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setText(Ljava/lang/String;)V

    .line 120
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mUsername:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v2, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 122
    .end local v0    # "value":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mPassword:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    if-ne v2, p1, :cond_4

    .line 123
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 125
    .restart local v0    # "value":Ljava/lang/String;
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    move-result-object v2

    iput-object v0, v2, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->password:Ljava/lang/String;

    .line 126
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    move-result-object v2

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    .line 128
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mPassword:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    invoke-virtual {v2, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setText(Ljava/lang/String;)V

    .line 129
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mPassword:Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;

    const-string v3, "******"

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomEditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 133
    .end local v0    # "value":Ljava/lang/String;
    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 6
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v1, 0x0

    .line 137
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mWirelessFileManager:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-ne v2, p1, :cond_0

    .line 138
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lorg/namelessrom/devicecontrol/services/WebServerService;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 139
    .local v0, "i":Landroid/content/Intent;
    const-class v2, Lorg/namelessrom/devicecontrol/services/WebServerService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->isServiceRunning(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 140
    const-string v2, "action_stop"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mWirelessFileManager:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    const v3, 0x7f0e0242

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setSummary(I)V

    .line 146
    :goto_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/app/FragmentActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 147
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mWirelessFileManager:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v2, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setEnabled(Z)V

    .line 148
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mHandler:Landroid/os/Handler;

    new-instance v2, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment$1;

    invoke-direct {v2, p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment$1;-><init>(Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 156
    const/4 v1, 0x1

    .line 159
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    return v1

    .line 143
    .restart local v0    # "i":Landroid/content/Intent;
    :cond_1
    const-string v2, "action_start"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 51
    invoke-super {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onResume()V

    .line 52
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mWirelessFileManager:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->updateWebServerPreference()V

    .line 55
    :cond_0
    return-void
.end method

.method public updateWebServerPreference()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 163
    const-class v4, Lorg/namelessrom/devicecontrol/services/WebServerService;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->isServiceRunning(Ljava/lang/String;)Z

    move-result v1

    .line 165
    .local v1, "isRunning":Z
    if-eqz v1, :cond_0

    .line 166
    invoke-static {}, Lorg/namelessrom/devicecontrol/net/NetworkInfo;->getAnyIpAddress()Ljava/lang/String;

    move-result-object v0

    .line 167
    .local v0, "ip":Ljava/lang/String;
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    move-result-object v4

    iget v4, v4, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->port:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 168
    .local v2, "port":Ljava/lang/String;
    const v4, 0x7f0e0243

    new-array v5, v9, [Ljava/lang/Object;

    const-string v6, "http://%s:%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v0, v7, v8

    aput-object v2, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p0, v4, v5}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 172
    .end local v0    # "ip":Ljava/lang/String;
    .end local v2    # "port":Ljava/lang/String;
    .local v3, "text":Ljava/lang/String;
    :goto_0
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mWirelessFileManager:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v4, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 173
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->mWirelessFileManager:Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;

    invoke-virtual {v4, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setChecked(Z)V

    .line 174
    return-void

    .line 170
    .end local v3    # "text":Ljava/lang/String;
    :cond_0
    const v4, 0x7f0e0242

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/WirelessFileManagerFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "text":Ljava/lang/String;
    goto :goto_0
.end method
