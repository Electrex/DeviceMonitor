.class public Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;
.source "ToolsMoreFragment.java"


# instance fields
.field private mAppManager:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mBuildProp:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mMediaScan:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mSysctlVm:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

.field private mWirelessFileManager:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;-><init>()V

    return-void
.end method

.method private startMediaScan()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 79
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->mMediaScan:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    const v2, 0x7f0e015b

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(I)V

    .line 80
    const-string v0, "am broadcast -a android.intent.action.MEDIA_MOUNTED -d file://%s"

    .line 81
    .local v0, "format":Ljava/lang/String;
    const-string v1, "am broadcast -a android.intent.action.MEDIA_MOUNTED -d file://%s"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->get()Lorg/namelessrom/devicecontrol/utils/IOUtils;

    move-result-object v3

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->getPrimarySdCard()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;)V

    .line 82
    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->get()Lorg/namelessrom/devicecontrol/utils/IOUtils;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->getSecondarySdCard()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 83
    const-string v1, "am broadcast -a android.intent.action.MEDIA_MOUNTED -d file://%s"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->get()Lorg/namelessrom/devicecontrol/utils/IOUtils;

    move-result-object v3

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->getSecondarySdCard()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;)V

    .line 85
    :cond_0
    return-void
.end method


# virtual methods
.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 42
    const v0, 0x7f0e0165

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 45
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 46
    const v0, 0x7f060011

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->addPreferencesFromResource(I)V

    .line 48
    const-string v0, "media_scan"

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->mMediaScan:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 50
    const-string v0, "app_manager"

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->mAppManager:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 51
    const-string v0, "wireless_file_manager"

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->mWirelessFileManager:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 53
    const-string v0, "build_prop"

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->mBuildProp:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 54
    const-string v0, "sysctl_vm"

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->mSysctlVm:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    .line 55
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 3
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 59
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 61
    .local v0, "key":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    .line 75
    :goto_0
    return v1

    .line 63
    :cond_1
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->mMediaScan:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne v1, p2, :cond_3

    .line 64
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->startMediaScan()V

    .line 75
    :cond_2
    :goto_1
    invoke-super {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v1

    goto :goto_0

    .line 65
    :cond_3
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->mAppManager:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne v1, p2, :cond_4

    .line 66
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0e0615

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/MainActivity;->loadFragment(Landroid/app/Activity;I)V

    goto :goto_1

    .line 67
    :cond_4
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->mWirelessFileManager:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne v1, p2, :cond_5

    .line 68
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0e0679

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/MainActivity;->loadFragment(Landroid/app/Activity;I)V

    goto :goto_1

    .line 69
    :cond_5
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->mBuildProp:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne v1, p2, :cond_6

    .line 70
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0e05b1

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/MainActivity;->loadFragment(Landroid/app/Activity;I)V

    goto :goto_1

    .line 71
    :cond_6
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->mSysctlVm:Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    if-ne v1, p2, :cond_2

    .line 72
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/fragments/tools/ToolsMoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0e054d

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/MainActivity;->loadFragment(Landroid/app/Activity;I)V

    goto :goto_1
.end method
