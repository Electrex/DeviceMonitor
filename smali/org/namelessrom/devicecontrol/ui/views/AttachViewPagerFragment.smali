.class public abstract Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;
.source "AttachViewPagerFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment$ViewPagerAdapter;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;-><init>()V

    .line 80
    return-void
.end method


# virtual methods
.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public abstract getPagerAdapter()Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment$ViewPagerAdapter;
.end method

.method public onCreateAnimation(IZI)Landroid/view/animation/Animation;
    .locals 4
    .param p1, "transit"    # I
    .param p2, "enter"    # Z
    .param p3, "nextAnim"    # I

    .prologue
    .line 70
    sget-boolean v1, Lorg/namelessrom/devicecontrol/MainActivity;->sDisableFragmentAnimations:Z

    if-eqz v1, :cond_0

    .line 71
    new-instance v0, Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment$1;

    invoke-direct {v0, p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment$1;-><init>(Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment;)V

    .line 72
    .local v0, "a":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 75
    .end local v0    # "a":Landroid/view/animation/Animation;
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;->onCreateAnimation(IZI)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 56
    const v4, 0x7f040030

    const/4 v5, 0x0

    invoke-virtual {p1, v4, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 58
    .local v2, "view":Landroid/view/View;
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment;->getPagerAdapter()Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment$ViewPagerAdapter;

    move-result-object v0

    .line 59
    .local v0, "adapter":Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment$ViewPagerAdapter;
    const v4, 0x7f0c0073

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v4/view/ViewPager;

    .line 60
    .local v3, "viewPager":Landroid/support/v4/view/ViewPager;
    invoke-virtual {v3, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 61
    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 63
    const v4, 0x7f0c0062

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/astuetz/PagerSlidingTabStrip;

    .line 64
    .local v1, "tabHost":Lcom/astuetz/PagerSlidingTabStrip;
    invoke-virtual {v1, v3}, Lcom/astuetz/PagerSlidingTabStrip;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 66
    return-object v2
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;->onPause()V

    .line 51
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachViewPagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;

    move-result-object v0

    iget-boolean v0, v0, Lorg/namelessrom/devicecontrol/configuration/DeviceConfiguration;->swipeOnContent:Z

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/MainActivity;->setSwipeOnContent(Z)V

    .line 52
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;->onResume()V

    .line 46
    const/4 v0, 0x0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/MainActivity;->setSwipeOnContent(Z)V

    .line 47
    return-void
.end method
