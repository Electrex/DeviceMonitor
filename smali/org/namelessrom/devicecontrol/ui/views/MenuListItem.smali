.class public Lorg/namelessrom/devicecontrol/ui/views/MenuListItem;
.super Landroid/widget/LinearLayout;
.source "MenuListItem.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/namelessrom/devicecontrol/ui/views/MenuListItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/namelessrom/devicecontrol/ui/views/MenuListItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    invoke-direct {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/views/MenuListItem;->createViews(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method private createViews(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 47
    const v7, 0x7f04003c

    invoke-static {p1, v7, p0}, Lorg/namelessrom/devicecontrol/ui/views/MenuListItem;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 49
    .local v6, "view":Landroid/view/View;
    sget-object v7, Lorg/namelessrom/devicecontrol/R$styleable;->MenuListItem:[I

    invoke-virtual {p1, p2, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 50
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v7, 0x1

    const v8, 0x7f0e00b8

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    .line 52
    .local v5, "titleRes":I
    const/4 v7, 0x0

    const v8, 0x7f02008e

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 54
    .local v3, "imageRes":I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 56
    const v7, 0x1020014

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 57
    .local v4, "title":Landroid/widget/TextView;
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 59
    const v7, 0x7f0c0045

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 60
    .local v2, "image":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/views/MenuListItem;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 61
    .local v1, "icon":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/views/MenuListItem;->isInEditMode()Z

    move-result v7

    if-nez v7, :cond_0

    .line 62
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/DrawableHelper;->applyAccentColorFilter(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 64
    :cond_0
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 65
    return-void
.end method
