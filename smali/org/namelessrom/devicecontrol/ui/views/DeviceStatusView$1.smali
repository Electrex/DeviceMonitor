.class Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$1;
.super Landroid/content/BroadcastReceiver;
.source "DeviceStatusView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$1;->this$0:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    .line 88
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$1;->this$0:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    const-string v1, "temperature"

    invoke-virtual {p2, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    # setter for: Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mBatteryTemperature:I
    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->access$002(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;I)I

    .line 89
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$1;->this$0:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    const-string v1, " - %s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$1;->this$0:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e0131

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    const-string v4, "health"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->getBatteryHealth(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mBatteryExtra:Ljava/lang/String;
    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->access$102(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;Ljava/lang/String;)Ljava/lang/String;

    .line 92
    return-void
.end method
