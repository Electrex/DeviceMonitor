.class public Lorg/namelessrom/devicecontrol/ui/views/CpuCoreView;
.super Landroid/widget/LinearLayout;
.source "CpuCoreView.java"


# instance fields
.field public bar:Lcom/daimajia/numberprogressbar/NumberProgressBar;

.field public core:Landroid/widget/TextView;

.field public freq:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 21
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/CpuCoreView;->init(Landroid/content/Context;)V

    .line 22
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04004f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 26
    .local v0, "v":Landroid/view/View;
    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/views/CpuCoreView;->addView(Landroid/view/View;)V

    .line 28
    const v1, 0x7f0c00f4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuCoreView;->core:Landroid/widget/TextView;

    .line 29
    const v1, 0x7f0c00f5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuCoreView;->freq:Landroid/widget/TextView;

    .line 30
    const v1, 0x7f0c00f7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/daimajia/numberprogressbar/NumberProgressBar;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuCoreView;->bar:Lcom/daimajia/numberprogressbar/NumberProgressBar;

    .line 31
    return-void
.end method
