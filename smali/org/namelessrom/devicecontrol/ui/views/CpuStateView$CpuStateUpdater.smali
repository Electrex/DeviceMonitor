.class Lorg/namelessrom/devicecontrol/ui/views/CpuStateView$CpuStateUpdater;
.super Landroid/os/AsyncTask;
.source "CpuStateView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CpuStateUpdater"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;


# direct methods
.method private constructor <init>(Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView$CpuStateUpdater;->this$0:Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;Lorg/namelessrom/devicecontrol/ui/views/CpuStateView$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;
    .param p2, "x1"    # Lorg/namelessrom/devicecontrol/ui/views/CpuStateView$1;

    .prologue
    .line 102
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView$CpuStateUpdater;-><init>(Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 102
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView$CpuStateUpdater;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 105
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView$CpuStateUpdater;->this$0:Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;

    # getter for: Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mUpdatingData:Z
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->access$200(Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 106
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView$CpuStateUpdater;->this$0:Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;

    const/4 v2, 0x1

    # setter for: Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mUpdatingData:Z
    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->access$202(Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;Z)Z

    .line 108
    :try_start_0
    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;->getInstance()Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;

    move-result-object v1

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView$CpuStateUpdater;->this$0:Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor;->updateStates(Lorg/namelessrom/devicecontrol/cpu/CpuUtils$StateListener;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :cond_0
    :goto_0
    const/4 v1, 0x0

    return-object v1

    .line 109
    :catch_0
    move-exception v0

    .line 110
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "updateStates()"

    invoke-static {p0, v1, v0}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 102
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView$CpuStateUpdater;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    .line 116
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView$CpuStateUpdater;->this$0:Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;

    const/4 v1, 0x0

    # setter for: Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mUpdatingData:Z
    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->access$202(Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;Z)Z

    return-void
.end method
