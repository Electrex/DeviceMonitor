.class public Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;
.super Landroid/widget/LinearLayout;
.source "DeviceStatusView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;
    }
.end annotation


# static fields
.field private static final mHandler:Landroid/os/Handler;

.field private static final mLockObject:Ljava/lang/Object;


# instance fields
.field private mBatteryExtra:Ljava/lang/String;

.field private final mBatteryReceiver:Landroid/content/BroadcastReceiver;

.field private mBatteryTemperature:I

.field private mDeviceInfo:Landroid/widget/LinearLayout;

.field final mDeviceUpdater:Ljava/lang/Runnable;

.field private mIsAttached:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mHandler:Landroid/os/Handler;

    .line 54
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mLockObject:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v4, 0x0

    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    iput v4, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mBatteryTemperature:I

    .line 46
    const-string v0, " - %s\u2026"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    const v3, 0x7f0e0119

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mBatteryExtra:Ljava/lang/String;

    .line 49
    iput-boolean v4, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mIsAttached:Z

    .line 85
    new-instance v0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$1;

    invoke-direct {v0, p0}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$1;-><init>(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;)V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mBatteryReceiver:Landroid/content/BroadcastReceiver;

    .line 153
    new-instance v0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$2;

    invoke-direct {v0, p0}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$2;-><init>(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;)V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mDeviceUpdater:Ljava/lang/Runnable;

    .line 64
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->createViews(Landroid/content/Context;)V

    .line 65
    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;)I
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    .prologue
    .line 41
    iget v0, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mBatteryTemperature:I

    return v0
.end method

.method static synthetic access$002(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;I)I
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;
    .param p1, "x1"    # I

    .prologue
    .line 41
    iput p1, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mBatteryTemperature:I

    return p1
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    .prologue
    .line 41
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mBatteryExtra:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mBatteryExtra:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;)Z
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->isAttached()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    .prologue
    .line 41
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mDeviceInfo:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$400(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;
    .param p1, "x1"    # Landroid/view/ViewGroup;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Ljava/lang/String;
    .param p5, "x5"    # Ljava/lang/String;
    .param p6, "x6"    # I

    .prologue
    .line 41
    invoke-direct/range {p0 .. p6}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->generateRow(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mLockObject:Ljava/lang/Object;

    return-object v0
.end method

.method private createViews(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 96
    const v1, 0x7f040057

    invoke-static {p1, v1, p0}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 98
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f0c010d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mDeviceInfo:Landroid/widget/LinearLayout;

    .line 100
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->startRepeatingTask()V

    .line 101
    return-void
.end method

.method private generateRow(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/view/View;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;
    .param p4, "barLeft"    # Ljava/lang/String;
    .param p5, "barRight"    # Ljava/lang/String;
    .param p6, "progress"    # I

    .prologue
    const/4 v0, 0x0

    .line 105
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->isAttached()Z

    move-result v1

    if-nez v1, :cond_0

    .line 116
    :goto_0
    return-object v0

    .line 107
    :cond_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f04004f

    invoke-static {v1, v2, v0}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 109
    .local v0, "view":Landroid/widget/LinearLayout;
    const v1, 0x7f0c00f4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    const v1, 0x7f0c00f5

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    const v1, 0x7f0c00f6

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    const v1, 0x7f0c00f8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    const v1, 0x7f0c00f7

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/daimajia/numberprogressbar/NumberProgressBar;

    invoke-virtual {v1, p6}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->setProgress(I)V

    .line 115
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private isAttached()Z
    .locals 2

    .prologue
    .line 146
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 147
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->isAttachedToWindow()Z

    move-result v0

    .line 149
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mIsAttached:Z

    goto :goto_0
.end method


# virtual methods
.method public onPause()V
    .locals 2

    .prologue
    .line 78
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mIsAttached:Z

    .line 80
    :try_start_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mBatteryReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :goto_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->stopRepeatingTask()V

    .line 83
    return-void

    .line 81
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 5

    .prologue
    .line 68
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mIsAttached:Z

    .line 69
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mBatteryReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    .line 72
    .local v0, "sticky":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 73
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mBatteryReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/content/BroadcastReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 75
    :cond_0
    return-void
.end method

.method startRepeatingTask()V
    .locals 1

    .prologue
    .line 163
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->stopRepeatingTask()V

    .line 164
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mDeviceUpdater:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 165
    return-void
.end method

.method stopRepeatingTask()V
    .locals 2

    .prologue
    .line 167
    sget-object v0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mDeviceUpdater:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method
