.class public abstract Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/CustomPreferenceFragment;
.source "AttachPreferenceFragment.java"

# interfaces
.implements Lorg/namelessrom/devicecontrol/listeners/OnBackPressedListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/CustomPreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract getFragmentId()I
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 44
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/CustomPreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 46
    sget-object v0, Lorg/namelessrom/devicecontrol/MainActivity;->sSlidingMenu:Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;

    if-eqz v0, :cond_0

    sget-object v0, Lorg/namelessrom/devicecontrol/MainActivity;->sSlidingMenu:Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;

    invoke-virtual {v0}, Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;->isMenuShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    sget-object v0, Lorg/namelessrom/devicecontrol/MainActivity;->sSlidingMenu:Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/jeremyfeinstein/slidingmenu/lib/SlidingMenu;->toggle(Z)V

    .line 49
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 37
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/CustomPreferenceFragment;->onAttach(Landroid/app/Activity;)V

    .line 38
    instance-of v0, p1, Lorg/namelessrom/devicecontrol/listeners/OnSectionAttachedListener;

    if-eqz v0, :cond_0

    .line 39
    check-cast p1, Lorg/namelessrom/devicecontrol/listeners/OnSectionAttachedListener;

    .end local p1    # "activity":Landroid/app/Activity;
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->getFragmentId()I

    move-result v0

    invoke-interface {p1, v0}, Lorg/namelessrom/devicecontrol/listeners/OnSectionAttachedListener;->onSectionAttached(I)V

    .line 41
    :cond_0
    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return v0
.end method

.method public onCreateAnimation(IZI)Landroid/view/animation/Animation;
    .locals 4
    .param p1, "transit"    # I
    .param p2, "enter"    # Z
    .param p3, "nextAnim"    # I

    .prologue
    .line 61
    sget-boolean v1, Lorg/namelessrom/devicecontrol/MainActivity;->sDisableFragmentAnimations:Z

    if-eqz v1, :cond_0

    .line 62
    new-instance v0, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment$1;

    invoke-direct {v0, p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment$1;-><init>(Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;)V

    .line 63
    .local v0, "a":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 66
    .end local v0    # "a":Landroid/view/animation/Animation;
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lorg/namelessrom/devicecontrol/ui/views/CustomPreferenceFragment;->onCreateAnimation(IZI)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 52
    invoke-super {p0}, Lorg/namelessrom/devicecontrol/ui/views/CustomPreferenceFragment;->onResume()V

    .line 53
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 54
    .local v0, "activity":Landroid/app/Activity;
    sget-boolean v1, Lorg/namelessrom/devicecontrol/utils/AppHelper;->preventOnResume:Z

    if-nez v1, :cond_0

    instance-of v1, v0, Lorg/namelessrom/devicecontrol/MainActivity;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 55
    check-cast v1, Lorg/namelessrom/devicecontrol/MainActivity;

    invoke-virtual {v1, p0}, Lorg/namelessrom/devicecontrol/MainActivity;->setFragment(Landroid/support/v4/app/Fragment;)V

    .line 57
    :cond_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachPreferenceFragment;->getFragmentId()I

    move-result v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lorg/namelessrom/devicecontrol/MainActivity;->loadFragment(Landroid/app/Activity;IZ)V

    .line 58
    return-void
.end method

.method public showBurger()Z
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x1

    return v0
.end method
