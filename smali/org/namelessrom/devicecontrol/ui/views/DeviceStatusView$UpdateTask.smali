.class Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;
.super Landroid/os/AsyncTask;
.source "DeviceStatusView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private cpuTemp:I

.field final synthetic this$0:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;


# direct methods
.method private constructor <init>(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;->this$0:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;
    .param p2, "x1"    # Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$1;

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;-><init>(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 119
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 124
    invoke-static {}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->get()Lorg/namelessrom/devicecontrol/cpu/CpuUtils;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/cpu/CpuUtils;->getCpuTemperature()I

    move-result v0

    iput v0, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;->cpuTemp:I

    .line 125
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 119
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 7
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    .line 129
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;->this$0:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    # invokes: Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->isAttached()Z
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->access$200(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;->this$0:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;->this$0:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    # getter for: Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mDeviceInfo:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->access$300(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 131
    iget v0, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;->cpuTemp:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 132
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;->this$0:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;->this$0:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    # getter for: Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mDeviceInfo:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->access$300(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;)Landroid/widget/LinearLayout;

    move-result-object v1

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;->this$0:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e007c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;->cpuTemp:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " \u00b0C"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "0\u00b0C"

    const-string v5, "100\u00b0C"

    iget v6, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;->cpuTemp:I

    # invokes: Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->generateRow(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/view/View;
    invoke-static/range {v0 .. v6}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->access$400(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/view/View;

    .line 135
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;->this$0:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;->this$0:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    # getter for: Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mDeviceInfo:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->access$300(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;)Landroid/widget/LinearLayout;

    move-result-object v1

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;->this$0:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e0033

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;->this$0:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    # getter for: Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mBatteryTemperature:I
    invoke-static {v4}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->access$000(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;)I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x41200000    # 10.0f

    div-float/2addr v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " \u00b0C"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;->this$0:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    # getter for: Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mBatteryExtra:Ljava/lang/String;
    invoke-static {v4}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->access$100(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "0\u00b0C"

    const-string v5, "100\u00b0C"

    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;->this$0:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    # getter for: Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mBatteryTemperature:I
    invoke-static {v6}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->access$000(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;)I

    move-result v6

    div-int/lit8 v6, v6, 0xa

    # invokes: Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->generateRow(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/view/View;
    invoke-static/range {v0 .. v6}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->access$400(Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/view/View;

    .line 139
    # getter for: Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->access$500()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;->this$0:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mDeviceUpdater:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 140
    # getter for: Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->access$500()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView$UpdateTask;->this$0:Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/ui/views/DeviceStatusView;->mDeviceUpdater:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 142
    :cond_1
    return-void
.end method
