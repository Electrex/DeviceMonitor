.class public abstract Lorg/namelessrom/devicecontrol/ui/views/CustomPreferenceFragment;
.super Landroid/support/v4/preference/PreferenceFragment;
.source "CustomPreferenceFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/support/v4/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method private createPreference(Landroid/content/Context;I)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sId"    # I

    .prologue
    .line 76
    new-instance v0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    invoke-direct {v0, p1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;-><init>(Landroid/content/Context;)V

    .line 77
    .local v0, "pref":Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
    const v1, 0x7f0e0178

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setTitle(I)V

    .line 78
    invoke-virtual {v0, p2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(I)V

    .line 79
    return-object v0
.end method


# virtual methods
.method protected isSupported(Landroid/preference/PreferenceCategory;Landroid/content/Context;)V
    .locals 1
    .param p1, "preferenceCategory"    # Landroid/preference/PreferenceCategory;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    const v0, 0x7f0e0179

    invoke-virtual {p0, p1, p2, v0}, Lorg/namelessrom/devicecontrol/ui/views/CustomPreferenceFragment;->isSupported(Landroid/preference/PreferenceCategory;Landroid/content/Context;I)V

    .line 66
    return-void
.end method

.method protected isSupported(Landroid/preference/PreferenceCategory;Landroid/content/Context;I)V
    .locals 1
    .param p1, "preferenceCategory"    # Landroid/preference/PreferenceCategory;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "sId"    # I

    .prologue
    .line 70
    invoke-virtual {p1}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 71
    invoke-direct {p0, p2, p3}, Lorg/namelessrom/devicecontrol/ui/views/CustomPreferenceFragment;->createPreference(Landroid/content/Context;I)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 73
    :cond_0
    return-void
.end method

.method protected isSupported(Landroid/preference/PreferenceScreen;Landroid/content/Context;)V
    .locals 1
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    const v0, 0x7f0e0179

    invoke-virtual {p0, p1, p2, v0}, Lorg/namelessrom/devicecontrol/ui/views/CustomPreferenceFragment;->isSupported(Landroid/preference/PreferenceScreen;Landroid/content/Context;I)V

    .line 55
    return-void
.end method

.method protected isSupported(Landroid/preference/PreferenceScreen;Landroid/content/Context;I)V
    .locals 1
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "sId"    # I

    .prologue
    .line 59
    invoke-virtual {p1}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 60
    invoke-direct {p0, p2, p3}, Lorg/namelessrom/devicecontrol/ui/views/CustomPreferenceFragment;->createPreference(Landroid/content/Context;I)Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 62
    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    .line 36
    instance-of v1, p1, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    if-eqz v1, :cond_0

    .line 37
    check-cast p1, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "o":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->writeValue(Z)V

    .line 44
    :goto_0
    return v0

    .line 39
    .restart local p1    # "preference":Landroid/preference/Preference;
    .restart local p2    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v1, p1, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    if-eqz v1, :cond_1

    .line 40
    check-cast p1, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeEditTextPreference;->writeValue(Ljava/lang/String;)V

    goto :goto_0

    .line 44
    .restart local p1    # "preference":Landroid/preference/Preference;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeIfEmpty(Landroid/preference/PreferenceScreen;Landroid/preference/PreferenceGroup;)V
    .locals 1
    .param p1, "root"    # Landroid/preference/PreferenceScreen;
    .param p2, "preferenceGroup"    # Landroid/preference/PreferenceGroup;

    .prologue
    .line 48
    if-eqz p1, :cond_0

    invoke-virtual {p2}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 49
    invoke-virtual {p1, p2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 51
    :cond_0
    return-void
.end method
