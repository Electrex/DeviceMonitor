.class public Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;
.super Landroid/widget/LinearLayout;
.source "CpuStateView.java"

# interfaces
.implements Lorg/namelessrom/devicecontrol/cpu/CpuUtils$StateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/ui/views/CpuStateView$CpuStateUpdater;
    }
.end annotation


# instance fields
.field private mAdditionalStates:Landroid/widget/TextView;

.field private mHeaderAdditionalStates:Landroid/widget/TextView;

.field private mHeaderTotalStateTime:Landroid/widget/TextView;

.field private mStatesView:Landroid/widget/LinearLayout;

.field private mStatesWarning:Landroid/widget/TextView;

.field private mTotalStateTime:Landroid/widget/TextView;

.field private mUpdatingData:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 62
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mUpdatingData:Z

    .line 63
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->createViews(Landroid/content/Context;)V

    .line 64
    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;)V
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->refreshData()V

    return-void
.end method

.method static synthetic access$200(Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;)Z
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;

    .prologue
    .line 43
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mUpdatingData:Z

    return v0
.end method

.method static synthetic access$202(Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;Z)Z
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mUpdatingData:Z

    return p1
.end method

.method private createViews(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 75
    const v3, 0x7f040056

    invoke-static {p1, v3, p0}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 77
    .local v2, "view":Landroid/view/View;
    const v3, 0x7f0c0109

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mStatesView:Landroid/widget/LinearLayout;

    .line 78
    const v3, 0x7f0c010c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mAdditionalStates:Landroid/widget/TextView;

    .line 79
    const v3, 0x7f0c010b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mHeaderAdditionalStates:Landroid/widget/TextView;

    .line 80
    const v3, 0x7f0c0106

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mHeaderTotalStateTime:Landroid/widget/TextView;

    .line 81
    const v3, 0x7f0c010a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mStatesWarning:Landroid/widget/TextView;

    .line 82
    const v3, 0x7f0c0108

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mTotalStateTime:Landroid/widget/TextView;

    .line 84
    const v3, 0x7f0c0107

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 85
    .local v0, "mRefresh":Landroid/widget/ImageView;
    new-instance v3, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView$1;

    invoke-direct {v3, p0}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView$1;-><init>(Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020079

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 92
    .local v1, "refreshDrawable":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_0

    .line 93
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 95
    :cond_0
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/DrawableHelper;->applyAccentColorFilter(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 97
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->refreshData()V

    .line 98
    return-void
.end method

.method private generateStateRow(Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;Landroid/view/ViewGroup;J)Landroid/view/View;
    .locals 17
    .param p1, "state"    # Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "totalStateTime"    # J

    .prologue
    .line 140
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->getContext()Landroid/content/Context;

    move-result-object v11

    const v12, 0x7f040051

    const/4 v13, 0x0

    invoke-static {v11, v12, v13}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    .line 142
    .local v10, "view":Landroid/widget/LinearLayout;
    move-object/from16 v0, p1

    iget-wide v12, v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;->duration:J

    long-to-float v11, v12

    const/high16 v12, 0x42c80000    # 100.0f

    mul-float/2addr v11, v12

    move-wide/from16 v0, p3

    long-to-float v12, v0

    div-float/2addr v11, v12

    float-to-int v5, v11

    .line 143
    .local v5, "per":I
    const/16 v11, 0x64

    if-le v5, v11, :cond_1

    const/16 v5, 0x64

    .line 146
    :cond_0
    :goto_0
    move-object/from16 v0, p1

    iget v11, v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;->freq:I

    if-nez v11, :cond_2

    .line 147
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->getContext()Landroid/content/Context;

    move-result-object v11

    const v12, 0x7f0e0089

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 152
    .local v7, "sFreq":Ljava/lang/String;
    :goto_1
    move-object/from16 v0, p1

    iget-wide v12, v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;->duration:J

    const-wide/16 v14, 0x64

    div-long v8, v12, v14

    .line 153
    .local v8, "tSec":J
    invoke-static {v8, v9}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->toString(J)Ljava/lang/String;

    move-result-object v6

    .line 155
    .local v6, "sDur":Ljava/lang/String;
    const v11, 0x7f0c00fb

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 156
    .local v4, "freqText":Landroid/widget/TextView;
    const v11, 0x7f0c00fc

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 157
    .local v3, "durText":Landroid/widget/TextView;
    const v11, 0x7f0c00fd

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/daimajia/numberprogressbar/NumberProgressBar;

    .line 159
    .local v2, "bar":Lcom/daimajia/numberprogressbar/NumberProgressBar;
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    invoke-virtual {v2, v5}, Lcom/daimajia/numberprogressbar/NumberProgressBar;->setProgress(I)V

    .line 163
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 164
    return-object v10

    .line 143
    .end local v2    # "bar":Lcom/daimajia/numberprogressbar/NumberProgressBar;
    .end local v3    # "durText":Landroid/widget/TextView;
    .end local v4    # "freqText":Landroid/widget/TextView;
    .end local v6    # "sDur":Ljava/lang/String;
    .end local v7    # "sFreq":Ljava/lang/String;
    .end local v8    # "tSec":J
    :cond_1
    if-gez v5, :cond_0

    const/4 v5, 0x0

    goto :goto_0

    .line 149
    :cond_2
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    iget v12, v0, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;->freq:I

    div-int/lit16 v12, v12, 0x3e8

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " MHz"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "sFreq":Ljava/lang/String;
    goto :goto_1
.end method

.method private refreshData()V
    .locals 2

    .prologue
    .line 100
    new-instance v0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView$CpuStateUpdater;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView$CpuStateUpdater;-><init>(Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;Lorg/namelessrom/devicecontrol/ui/views/CpuStateView$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView$CpuStateUpdater;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private static toString(J)Ljava/lang/String;
    .locals 12
    .param p0, "tSec"    # J

    .prologue
    .line 120
    const-wide/16 v8, 0xe10

    div-long v8, p0, v8

    long-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    double-to-long v0, v8

    .line 121
    .local v0, "h":J
    const-wide/16 v8, 0x3c

    mul-long/2addr v8, v0

    const-wide/16 v10, 0x3c

    mul-long/2addr v8, v10

    sub-long v8, p0, v8

    const-wide/16 v10, 0x3c

    div-long/2addr v8, v10

    long-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    double-to-long v2, v8

    .line 122
    .local v2, "m":J
    const-wide/16 v8, 0x3c

    rem-long v4, p0, v8

    .line 124
    .local v4, "s":J
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 125
    .local v6, "sDur":Ljava/lang/StringBuilder;
    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x3a

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 126
    const-wide/16 v8, 0xa

    cmp-long v7, v2, v8

    if-gez v7, :cond_0

    .line 127
    const/16 v7, 0x30

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 129
    :cond_0
    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x3a

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 130
    const-wide/16 v8, 0xa

    cmp-long v7, v4, v8

    if-gez v7, :cond_1

    .line 131
    const/16 v7, 0x30

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 133
    :cond_1
    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 135
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method


# virtual methods
.method public onPause()V
    .locals 1

    .prologue
    .line 71
    const-string v0, "onPause"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 67
    const-string v0, "onResume"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    return-void
.end method

.method public onStates(Lorg/namelessrom/devicecontrol/cpu/CpuUtils$State;)V
    .locals 14
    .param p1, "states"    # Lorg/namelessrom/devicecontrol/cpu/CpuUtils$State;

    .prologue
    .line 168
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mStatesView:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 170
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 171
    .local v0, "extraStates":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v7, p1, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$State;->states:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;

    .line 172
    .local v6, "state":Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;
    iget-wide v10, v6, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;->duration:J

    const-wide/16 v12, 0x0

    cmp-long v7, v10, v12

    if-lez v7, :cond_0

    .line 173
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mStatesView:Landroid/widget/LinearLayout;

    iget-wide v10, p1, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$State;->totalTime:J

    invoke-direct {p0, v6, v7, v10, v11}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->generateStateRow(Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;Landroid/view/ViewGroup;J)Landroid/view/View;

    goto :goto_0

    .line 175
    :cond_0
    iget v7, v6, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;->freq:I

    if-nez v7, :cond_1

    .line 176
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->getContext()Landroid/content/Context;

    move-result-object v7

    const v10, 0x7f0e0089

    invoke-virtual {v7, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 178
    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget v10, v6, Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;->freq:I

    div-int/lit16 v10, v10, 0x3e8

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, " MHz"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 183
    .end local v6    # "state":Lorg/namelessrom/devicecontrol/cpu/monitors/CpuStateMonitor$CpuState;
    :cond_2
    iget-object v7, p1, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$State;->states:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_3

    .line 184
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mStatesWarning:Landroid/widget/TextView;

    const/4 v10, 0x0

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 185
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mHeaderTotalStateTime:Landroid/widget/TextView;

    const/16 v10, 0x8

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 186
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mTotalStateTime:Landroid/widget/TextView;

    const/16 v10, 0x8

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 187
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mStatesView:Landroid/widget/LinearLayout;

    const/16 v10, 0x8

    invoke-virtual {v7, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 190
    :cond_3
    iget-wide v10, p1, Lorg/namelessrom/devicecontrol/cpu/CpuUtils$State;->totalTime:J

    const-wide/16 v12, 0x64

    div-long v8, v10, v12

    .line 191
    .local v8, "totTime":J
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mTotalStateTime:Landroid/widget/TextView;

    invoke-static {v8, v9}, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->toString(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_6

    .line 194
    const/4 v2, 0x0

    .line 195
    .local v2, "n":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 197
    .local v5, "sb":Ljava/lang/StringBuilder;
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 198
    .local v4, "s":Ljava/lang/String;
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "n":I
    .local v3, "n":I
    if-lez v2, :cond_4

    .line 199
    const-string v7, ", "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    :cond_4
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v2, v3

    .line 202
    .end local v3    # "n":I
    .restart local v2    # "n":I
    goto :goto_1

    .line 204
    .end local v4    # "s":Ljava/lang/String;
    :cond_5
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mAdditionalStates:Landroid/widget/TextView;

    const/4 v10, 0x0

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 205
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mHeaderAdditionalStates:Landroid/widget/TextView;

    const/4 v10, 0x0

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 206
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mAdditionalStates:Landroid/widget/TextView;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    .end local v2    # "n":I
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    :goto_2
    return-void

    .line 208
    :cond_6
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mAdditionalStates:Landroid/widget/TextView;

    const/16 v10, 0x8

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 209
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/views/CpuStateView;->mHeaderAdditionalStates:Landroid/widget/TextView;

    const/16 v10, 0x8

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method
