.class public Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;
.super Landroid/preference/Preference;
.source "CustomPreference.java"


# instance fields
.field private areMilliVolts:Z

.field private sumColor:I

.field private summary:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 18
    const/4 v0, -0x1

    iput v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->sumColor:I

    .line 27
    const v0, 0x7f040045

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setLayoutResource(I)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    const/4 v0, -0x1

    iput v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->sumColor:I

    .line 32
    const v0, 0x7f040045

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setLayoutResource(I)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 18
    const/4 v0, -0x1

    iput v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->sumColor:I

    .line 37
    const v0, 0x7f040045

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setLayoutResource(I)V

    .line 38
    return-void
.end method


# virtual methods
.method public areMilliVolts(Z)V
    .locals 0
    .param p1, "areMillivolts"    # Z

    .prologue
    .line 69
    iput-boolean p1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->areMilliVolts:Z

    return-void
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 87
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 89
    const v2, 0x1020010

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->summary:Landroid/widget/TextView;

    .line 90
    iget v2, p0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->sumColor:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 91
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->summary:Landroid/widget/TextView;

    iget v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->sumColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 94
    :cond_0
    const v2, 0x1020006

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 95
    .local v1, "icon":Landroid/widget/ImageView;
    if-eqz v1, :cond_1

    .line 96
    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 97
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_1

    .line 98
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/DrawableHelper;->applyAccentColorFilter(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 99
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 102
    .end local v0    # "d":Landroid/graphics/drawable/Drawable;
    :cond_1
    return-void
.end method

.method public restoreSummaryKey(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "summary"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 61
    invoke-virtual {p0, p2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setKey(Ljava/lang/String;)V

    .line 62
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->areMilliVolts:Z

    if-eqz v0, :cond_0

    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mV"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 67
    :goto_0
    return-void

    .line 65
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setCustomSummaryKeyMinus(I)V
    .locals 3
    .param p1, "minus"    # I

    .prologue
    .line 51
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v1

    sub-int v0, v1, p1

    .line 52
    .local v0, "newValue":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setKey(Ljava/lang/String;)V

    .line 53
    iget-boolean v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->areMilliVolts:Z

    if-eqz v1, :cond_0

    .line 54
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mV"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 58
    :goto_0
    return-void

    .line 56
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setCustomSummaryKeyPlus(I)V
    .locals 3
    .param p1, "plus"    # I

    .prologue
    .line 41
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v1

    add-int v0, v1, p1

    .line 42
    .local v0, "newValue":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setKey(Ljava/lang/String;)V

    .line 43
    iget-boolean v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->areMilliVolts:Z

    if-eqz v1, :cond_0

    .line 44
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mV"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 48
    :goto_0
    return-void

    .line 46
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
