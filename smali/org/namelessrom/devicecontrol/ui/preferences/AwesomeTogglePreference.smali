.class public Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;
.super Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;
.source "AwesomeTogglePreference.java"


# instance fields
.field private mCategory:Ljava/lang/String;

.field private mMultiFile:Z

.field private mPath:Ljava/lang/String;

.field private mPaths:[Ljava/lang/String;

.field private mStartUp:Z

.field private mValueChecked:Ljava/lang/String;

.field private mValueNotChecked:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    const-string v0, "1"

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueChecked:Ljava/lang/String;

    .line 44
    const-string v0, "0"

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueNotChecked:Ljava/lang/String;

    .line 54
    invoke-direct {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    const-string v0, "1"

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueChecked:Ljava/lang/String;

    .line 44
    const-string v0, "0"

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueNotChecked:Ljava/lang/String;

    .line 60
    invoke-direct {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "paths"    # [Ljava/lang/String;
    .param p4, "category"    # Ljava/lang/String;
    .param p5, "multiFile"    # Z
    .param p6, "startUp"    # Z

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;-><init>(Landroid/content/Context;)V

    .line 43
    const-string v0, "1"

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueChecked:Ljava/lang/String;

    .line 44
    const-string v0, "0"

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueNotChecked:Ljava/lang/String;

    .line 66
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPath:Ljava/lang/String;

    .line 67
    iput-object p3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPaths:[Ljava/lang/String;

    .line 68
    iput-object p4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mCategory:Ljava/lang/String;

    .line 69
    iput-boolean p5, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mMultiFile:Z

    .line 70
    iput-boolean p6, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mStartUp:Z

    .line 71
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v12, 0x2

    const/4 v9, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v8, -0x1

    .line 74
    sget-object v7, Lorg/namelessrom/devicecontrol/R$styleable;->AwesomePreference:[I

    invoke-virtual {p1, p2, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 76
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, -0x1

    .local v1, "filePath":I
    const/4 v2, -0x1

    .local v2, "filePathList":I
    const/4 v3, -0x1

    .line 77
    .local v3, "fileValue":I
    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {v0, v10, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 79
    invoke-virtual {v0, v11, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 80
    invoke-virtual {v0, v12, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 81
    const/4 v7, 0x3

    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mCategory:Ljava/lang/String;

    .line 82
    const/4 v7, 0x4

    invoke-virtual {v0, v7, v11}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v7

    iput-boolean v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mStartUp:Z

    .line 83
    const/4 v7, 0x5

    invoke-virtual {v0, v7, v10}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v7

    iput-boolean v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mMultiFile:Z

    .line 84
    const/4 v7, 0x6

    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueChecked:Ljava/lang/String;

    .line 85
    const/4 v7, 0x7

    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueNotChecked:Ljava/lang/String;

    .line 86
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 89
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 90
    .local v5, "res":Landroid/content/res/Resources;
    if-eq v1, v8, :cond_7

    .line 91
    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lorg/namelessrom/devicecontrol/utils/Utils;->checkPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPath:Ljava/lang/String;

    .line 92
    iput-object v9, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPaths:[Ljava/lang/String;

    .line 104
    :cond_1
    :goto_0
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPath:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    if-eq v2, v8, :cond_2

    if-eq v3, v8, :cond_2

    .line 105
    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    iget-object v8, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPath:Ljava/lang/String;

    invoke-interface {v7, v8}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    .line 106
    .local v4, "index":I
    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    aget-object v7, v7, v4

    const-string v8, ";"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 107
    .local v6, "values":[Ljava/lang/String;
    aget-object v7, v6, v10

    iput-object v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueChecked:Ljava/lang/String;

    .line 108
    aget-object v7, v6, v11

    iput-object v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueNotChecked:Ljava/lang/String;

    .line 109
    const-string v7, "mValueChecked -> %s\nmValueNotChecked -> %s"

    new-array v8, v12, [Ljava/lang/Object;

    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueChecked:Ljava/lang/String;

    aput-object v9, v8, v10

    iget-object v9, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueNotChecked:Ljava/lang/String;

    aput-object v9, v8, v11

    invoke-static {p0, v7, v8}, Lorg/namelessrom/devicecontrol/Logger;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 113
    .end local v4    # "index":I
    .end local v6    # "values":[Ljava/lang/String;
    :cond_2
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mCategory:Ljava/lang/String;

    if-eqz v7, :cond_3

    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mCategory:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 114
    :cond_3
    const-string v7, "Category is not set! Defaulting to \"default\""

    invoke-static {p0, v7}, Lorg/namelessrom/devicecontrol/Logger;->w(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    const-string v7, "default"

    iput-object v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mCategory:Ljava/lang/String;

    .line 117
    :cond_4
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueChecked:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_5

    const-string v7, "1"

    iput-object v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueChecked:Ljava/lang/String;

    .line 118
    :cond_5
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueNotChecked:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_6

    const-string v7, "0"

    iput-object v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueNotChecked:Ljava/lang/String;

    .line 119
    :cond_6
    return-void

    .line 93
    :cond_7
    if-eq v2, v8, :cond_9

    .line 94
    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPaths:[Ljava/lang/String;

    .line 95
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPaths:[Ljava/lang/String;

    invoke-static {v7}, Lorg/namelessrom/devicecontrol/utils/Utils;->checkPaths([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPath:Ljava/lang/String;

    .line 96
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPath:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_8

    iget-boolean v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mMultiFile:Z

    if-nez v7, :cond_1

    .line 97
    :cond_8
    iput-object v9, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPaths:[Ljava/lang/String;

    goto/16 :goto_0

    .line 100
    :cond_9
    const-string v7, ""

    iput-object v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPath:Ljava/lang/String;

    .line 101
    iput-object v9, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPaths:[Ljava/lang/String;

    goto/16 :goto_0
.end method


# virtual methods
.method public initValue()V
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->initValue(Z)V

    return-void
.end method

.method public initValue(Z)V
    .locals 1
    .param p1, "contains"    # Z

    .prologue
    .line 124
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPath:Ljava/lang/String;

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lorg/namelessrom/devicecontrol/utils/Utils;->isEnabled(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setChecked(Z)V

    .line 125
    :cond_0
    return-void
.end method

.method public isSupported()Z
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPath:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPaths:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPaths:[Ljava/lang/String;

    array-length v0, v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setupTitle()V
    .locals 2

    .prologue
    .line 128
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v1

    if-nez v1, :cond_1

    .line 129
    const-string v1, "setupTitle -> not supported"

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPath:Ljava/lang/String;

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/PreferenceUtils;->getTitle(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 133
    .local v0, "title":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 134
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->setTitle(I)V

    goto :goto_0
.end method

.method public writeValue(Z)V
    .locals 9
    .param p1, "isChecked"    # Z

    .prologue
    const/4 v5, 0x1

    .line 156
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->isSupported()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 157
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPaths:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mMultiFile:Z

    if-eqz v0, :cond_3

    .line 158
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPaths:[Ljava/lang/String;

    array-length v7, v0

    .line 159
    .local v7, "length":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v7, :cond_4

    .line 160
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPaths:[Ljava/lang/String;

    aget-object v1, v0, v6

    if-eqz p1, :cond_1

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueChecked:Ljava/lang/String;

    :goto_1
    invoke-static {v1, v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->writeValue(Ljava/lang/String;Ljava/lang/String;)Z

    .line 161
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mStartUp:Z

    if-eqz v0, :cond_0

    .line 162
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->getContext()Landroid/content/Context;

    move-result-object v8

    new-instance v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mCategory:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPaths:[Ljava/lang/String;

    aget-object v3, v3, v6

    if-eqz p1, :cond_2

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueChecked:Ljava/lang/String;

    :goto_2
    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/objects/BootupItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v8, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->setBootup(Landroid/content/Context;Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 159
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 160
    :cond_1
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueNotChecked:Ljava/lang/String;

    goto :goto_1

    .line 162
    :cond_2
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueNotChecked:Ljava/lang/String;

    goto :goto_2

    .line 168
    .end local v6    # "i":I
    .end local v7    # "length":I
    :cond_3
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPath:Ljava/lang/String;

    if-eqz p1, :cond_5

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueChecked:Ljava/lang/String;

    :goto_3
    invoke-static {v1, v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->writeValue(Ljava/lang/String;Ljava/lang/String;)Z

    .line 169
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mStartUp:Z

    if-eqz v0, :cond_4

    .line 170
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->getContext()Landroid/content/Context;

    move-result-object v8

    new-instance v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mCategory:Ljava/lang/String;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->getKey()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mPath:Ljava/lang/String;

    if-eqz p1, :cond_6

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueChecked:Ljava/lang/String;

    :goto_4
    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/objects/BootupItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v8, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->setBootup(Landroid/content/Context;Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 176
    :cond_4
    return-void

    .line 168
    :cond_5
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueNotChecked:Ljava/lang/String;

    goto :goto_3

    .line 170
    :cond_6
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeTogglePreference;->mValueNotChecked:Ljava/lang/String;

    goto :goto_4
.end method
