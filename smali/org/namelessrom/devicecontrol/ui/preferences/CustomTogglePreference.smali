.class public Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;
.super Landroid/preference/SwitchPreference;
.source "CustomTogglePreference.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Landroid/preference/SwitchPreference;-><init>(Landroid/content/Context;)V

    .line 15
    const v0, 0x7f040045

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setLayoutResource(I)V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Landroid/preference/SwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const v0, 0x7f040045

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setLayoutResource(I)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/SwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    const v0, 0x7f040045

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomTogglePreference;->setLayoutResource(I)V

    .line 26
    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method

.method protected shouldPersist()Z
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    return v0
.end method
