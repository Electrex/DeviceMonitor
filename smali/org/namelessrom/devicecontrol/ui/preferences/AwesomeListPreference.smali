.class public Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;
.super Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;
.source "AwesomeListPreference.java"


# instance fields
.field private category:Ljava/lang/String;

.field private mPath:Ljava/lang/String;

.field private mPaths:[Ljava/lang/String;

.field private multiFile:Z

.field private startUp:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    invoke-direct {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 56
    sget-object v4, Lorg/namelessrom/devicecontrol/R$styleable;->AwesomePreference:[I

    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 58
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, -0x1

    .local v1, "filePath":I
    const/4 v2, -0x1

    .line 59
    .local v2, "filePathList":I
    if-eqz v0, :cond_0

    .line 60
    invoke-virtual {v0, v7, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 61
    invoke-virtual {v0, v8, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 62
    const/4 v4, 0x3

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->category:Ljava/lang/String;

    .line 63
    const/4 v4, 0x4

    invoke-virtual {v0, v4, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    iput-boolean v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->startUp:Z

    .line 64
    const/4 v4, 0x5

    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    iput-boolean v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->multiFile:Z

    .line 65
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 68
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 69
    .local v3, "res":Landroid/content/res/Resources;
    if-eq v1, v5, :cond_4

    .line 70
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->checkPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->mPath:Ljava/lang/String;

    .line 71
    iput-object v6, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->mPaths:[Ljava/lang/String;

    .line 83
    :cond_1
    :goto_0
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->category:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->category:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 84
    :cond_2
    const-string v4, "Category is not set! Defaulting to \"default\""

    invoke-static {p0, v4}, Lorg/namelessrom/devicecontrol/Logger;->w(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    const-string v4, "default"

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->category:Ljava/lang/String;

    .line 88
    :cond_3
    const v4, 0x7f040045

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->setLayoutResource(I)V

    .line 89
    return-void

    .line 72
    :cond_4
    if-eq v2, v5, :cond_6

    .line 73
    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->mPaths:[Ljava/lang/String;

    .line 74
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->mPaths:[Ljava/lang/String;

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->checkPaths([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->mPath:Ljava/lang/String;

    .line 75
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->mPath:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    iget-boolean v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->multiFile:Z

    if-nez v4, :cond_1

    .line 76
    :cond_5
    iput-object v6, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->mPaths:[Ljava/lang/String;

    goto :goto_0

    .line 79
    :cond_6
    const-string v4, ""

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->mPath:Ljava/lang/String;

    .line 80
    iput-object v6, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->mPaths:[Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public initValue()V
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->isSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->mPath:Ljava/lang/String;

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->setValue(Ljava/lang/String;)V

    .line 95
    :cond_0
    return-void
.end method

.method public isSupported()Z
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->mPath:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->mPath:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->mPaths:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->mPaths:[Ljava/lang/String;

    array-length v0, v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeValue(Ljava/lang/String;)V
    .locals 9
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 104
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->isSupported()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 105
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->mPaths:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->multiFile:Z

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->mPaths:[Ljava/lang/String;

    array-length v7, v0

    .line 107
    .local v7, "length":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v7, :cond_2

    .line 108
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->mPaths:[Ljava/lang/String;

    aget-object v0, v0, v6

    invoke-static {v0, p1}, Lorg/namelessrom/devicecontrol/utils/Utils;->writeValue(Ljava/lang/String;Ljava/lang/String;)Z

    .line 109
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->startUp:Z

    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->getContext()Landroid/content/Context;

    move-result-object v8

    new-instance v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->category:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->mPaths:[Ljava/lang/String;

    aget-object v3, v3, v6

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/objects/BootupItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v8, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->setBootup(Landroid/content/Context;Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 107
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 115
    .end local v6    # "i":I
    .end local v7    # "length":I
    :cond_1
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->mPath:Ljava/lang/String;

    invoke-static {v0, p1}, Lorg/namelessrom/devicecontrol/utils/Utils;->writeValue(Ljava/lang/String;Ljava/lang/String;)Z

    .line 116
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->startUp:Z

    if-eqz v0, :cond_2

    .line 117
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->getContext()Landroid/content/Context;

    move-result-object v8

    new-instance v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->category:Ljava/lang/String;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomeListPreference;->mPath:Ljava/lang/String;

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/objects/BootupItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v8, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->setBootup(Landroid/content/Context;Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 122
    :cond_2
    return-void
.end method
