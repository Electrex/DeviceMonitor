.class public Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;
.super Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreferenceCategory;
.source "AwesomePreferenceCategory.java"


# instance fields
.field private mPath:Ljava/lang/String;

.field private mPaths:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lorg/namelessrom/devicecontrol/ui/preferences/CustomPreferenceCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    invoke-direct {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 48
    sget-object v4, Lorg/namelessrom/devicecontrol/R$styleable;->AwesomePreference:[I

    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 50
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, -0x1

    .local v1, "filePath":I
    const/4 v2, -0x1

    .line 51
    .local v2, "filePathList":I
    if-eqz v0, :cond_0

    .line 52
    const/4 v4, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 53
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 54
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 57
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 58
    .local v3, "res":Landroid/content/res/Resources;
    if-eq v1, v5, :cond_2

    .line 59
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->checkPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->mPath:Ljava/lang/String;

    .line 60
    iput-object v6, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->mPaths:[Ljava/lang/String;

    .line 71
    :cond_1
    :goto_0
    return-void

    .line 61
    :cond_2
    if-eq v2, v5, :cond_3

    .line 62
    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->mPaths:[Ljava/lang/String;

    .line 63
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->mPaths:[Ljava/lang/String;

    invoke-static {v4}, Lorg/namelessrom/devicecontrol/utils/Utils;->checkPaths([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->mPath:Ljava/lang/String;

    .line 64
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->mPath:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 65
    iput-object v6, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->mPaths:[Ljava/lang/String;

    goto :goto_0

    .line 68
    :cond_3
    const-string v4, ""

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->mPath:Ljava/lang/String;

    .line 69
    iput-object v6, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->mPaths:[Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public isSupported()Z
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->mPath:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->mPath:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->mPaths:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/AwesomePreferenceCategory;->mPaths:[Ljava/lang/String;

    array-length v0, v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
