.class public Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;
.super Landroid/preference/DialogPreference;
.source "VibratorIntensity.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# static fields
.field private static mRedFilter:Landroid/graphics/LightingColorFilter;

.field private static paths:[Ljava/lang/String;


# instance fields
.field private defValue:I

.field private mOriginalValue:Ljava/lang/String;

.field private mProgressDrawable:Landroid/graphics/drawable/Drawable;

.field private mProgressThumb:Landroid/graphics/drawable/Drawable;

.field private mSeekBar:Landroid/widget/SeekBar;

.field private mValue:Landroid/widget/TextView;

.field private max:I

.field private min:I

.field private path:Ljava/lang/String;

.field private threshold:I

.field private final vib:Landroid/os/Vibrator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    sput-object v0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mRedFilter:Landroid/graphics/LightingColorFilter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mProgressThumb:Landroid/graphics/drawable/Drawable;

    .line 67
    const-string v0, "vibrator"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->vib:Landroid/os/Vibrator;

    .line 69
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->setupValues(Landroid/content/Context;)V

    .line 71
    const v0, 0x7f040045

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->setLayoutResource(I)V

    .line 72
    const v0, 0x7f040047

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->setDialogLayoutResource(I)V

    .line 73
    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;)Landroid/os/Vibrator;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;

    .prologue
    .line 43
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->vib:Landroid/os/Vibrator;

    return-object v0
.end method

.method public static isSupported()Z
    .locals 2

    .prologue
    .line 236
    sget-object v0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->paths:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 237
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    const v1, 0x7f080023

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/Application;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->paths:[Ljava/lang/String;

    .line 239
    :cond_0
    sget-object v0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->paths:[Ljava/lang/String;

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private percentToStrength(I)I
    .locals 3
    .param p1, "percent"    # I

    .prologue
    .line 224
    iget v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->max:I

    iget v2, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->min:I

    sub-int/2addr v1, v2

    mul-int/2addr v1, p1

    div-int/lit8 v1, v1, 0x64

    iget v2, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->min:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 226
    .local v0, "strength":I
    iget v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->max:I

    if-le v0, v1, :cond_1

    .line 227
    iget v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->max:I

    .line 232
    .end local v0    # "strength":I
    :cond_0
    :goto_0
    return v0

    .line 228
    .restart local v0    # "strength":I
    :cond_1
    iget v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->min:I

    if-ge v0, v1, :cond_0

    .line 229
    iget v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->min:I

    goto :goto_0
.end method

.method private setupValues(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 77
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 78
    .local v7, "res":Landroid/content/res/Resources;
    sget-object v9, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->paths:[Ljava/lang/String;

    if-nez v9, :cond_0

    .line 79
    const v9, 0x7f080023

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    sput-object v9, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->paths:[Ljava/lang/String;

    .line 81
    :cond_0
    const v9, 0x7f08001f

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 82
    .local v4, "maxs":[Ljava/lang/String;
    const v9, 0x7f080021

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    .line 83
    .local v6, "mins":[Ljava/lang/String;
    const v9, 0x7f080020

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 84
    .local v3, "maxPaths":[Ljava/lang/String;
    const v9, 0x7f080022

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    .line 85
    .local v5, "minPaths":[Ljava/lang/String;
    const v9, 0x7f08001e

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 86
    .local v0, "defs":[Ljava/lang/String;
    const v9, 0x7f080024

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v8

    .line 88
    .local v8, "thresholds":[Ljava/lang/String;
    sget-object v9, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->paths:[Ljava/lang/String;

    array-length v2, v9

    .line 89
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 91
    sget-object v9, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->paths:[Ljava/lang/String;

    aget-object v9, v9, v1

    invoke-static {v9}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 93
    sget-object v9, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->paths:[Ljava/lang/String;

    aget-object v9, v9, v1

    iput-object v9, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->path:Ljava/lang/String;

    .line 95
    const-string v9, "-"

    aget-object v10, v3, v1

    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 96
    aget-object v9, v4, v1

    invoke-static {v9}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v9

    iput v9, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->max:I

    .line 101
    :goto_1
    const-string v9, "-"

    aget-object v10, v5, v1

    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 102
    aget-object v9, v6, v1

    invoke-static {v9}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v9

    iput v9, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->min:I

    .line 107
    :goto_2
    const-string v9, "max"

    aget-object v10, v0, v1

    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 108
    iget v9, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->max:I

    iput v9, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->defValue:I

    .line 115
    :goto_3
    aget-object v9, v8, v1

    invoke-static {v9}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v9

    iput v9, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->threshold:I

    .line 120
    :cond_1
    return-void

    .line 98
    :cond_2
    aget-object v9, v3, v1

    invoke-static {v9}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v9

    iput v9, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->max:I

    goto :goto_1

    .line 104
    :cond_3
    aget-object v9, v5, v1

    invoke-static {v9}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v9

    iput v9, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->min:I

    goto :goto_2

    .line 109
    :cond_4
    const-string v9, "min"

    aget-object v10, v0, v1

    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 110
    iget v9, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->min:I

    iput v9, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->defValue:I

    goto :goto_3

    .line 112
    :cond_5
    aget-object v9, v0, v1

    invoke-static {v9}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v9

    iput v9, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->defValue:I

    goto :goto_3

    .line 89
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0
.end method

.method private strengthToPercent(I)I
    .locals 6
    .param p1, "strength"    # I

    .prologue
    const/16 v3, 0x64

    .line 213
    iget v2, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->min:I

    sub-int v4, p1, v2

    iget v2, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->max:I

    iget v5, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->min:I

    sub-int/2addr v2, v5

    if-eqz v2, :cond_0

    iget v2, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->max:I

    iget v5, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->min:I

    sub-int/2addr v2, v5

    :goto_0
    div-int v2, v3, v2

    mul-int/2addr v2, v4

    int-to-double v0, v2

    .line 215
    .local v0, "percent":D
    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    cmpl-double v2, v0, v4

    if-lez v2, :cond_1

    move v2, v3

    .line 217
    :goto_1
    return v2

    .line 213
    .end local v0    # "percent":D
    :cond_0
    const/4 v2, 0x1

    goto :goto_0

    .line 215
    .restart local v0    # "percent":D
    :cond_1
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_2

    const/4 v2, 0x0

    goto :goto_1

    .line 217
    :cond_2
    double-to-int v2, v0

    goto :goto_1
.end method


# virtual methods
.method protected onBindDialogView(Landroid/view/View;)V
    .locals 14
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 123
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindDialogView(Landroid/view/View;)V

    .line 125
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->getContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f0e0200

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": %s"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-array v9, v13, [Ljava/lang/Object;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget v11, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->defValue:I

    invoke-direct {p0, v11}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->strengthToPercent(I)I

    move-result v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "%"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 127
    .local v0, "def":Ljava/lang/String;
    const v8, 0x7f0c00d6

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    const v8, 0x7f0c00d8

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/SeekBar;

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mSeekBar:Landroid/widget/SeekBar;

    .line 130
    const v8, 0x7f0c00d9

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mValue:Landroid/widget/TextView;

    .line 131
    const v8, 0x7f0c00db

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 133
    .local v3, "mWarning":Landroid/widget/TextView;
    iget v8, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->threshold:I

    const/4 v9, -0x1

    if-eq v8, v9, :cond_3

    .line 134
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->getContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0e0239

    new-array v10, v13, [Ljava/lang/Object;

    iget v11, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->threshold:I

    invoke-direct {p0, v11}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->strengthToPercent(I)I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v12

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 136
    .local v6, "strWarnMsg":Ljava/lang/String;
    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    .end local v6    # "strWarnMsg":Ljava/lang/String;
    :goto_0
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v8}, Landroid/widget/SeekBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 142
    .local v5, "progressDrawable":Landroid/graphics/drawable/Drawable;
    instance-of v8, v5, Landroid/graphics/drawable/LayerDrawable;

    if-eqz v8, :cond_0

    move-object v2, v5

    .line 143
    check-cast v2, Landroid/graphics/drawable/LayerDrawable;

    .line 144
    .local v2, "ld":Landroid/graphics/drawable/LayerDrawable;
    const v8, 0x102000d

    invoke-virtual {v2, v8}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    .line 146
    .end local v2    # "ld":Landroid/graphics/drawable/LayerDrawable;
    :cond_0
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x10

    if-lt v8, v9, :cond_1

    .line 147
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v8}, Landroid/widget/SeekBar;->getThumb()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mProgressThumb:Landroid/graphics/drawable/Drawable;

    .line 149
    :cond_1
    sget-object v8, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mRedFilter:Landroid/graphics/LightingColorFilter;

    if-nez v8, :cond_2

    .line 150
    new-instance v8, Landroid/graphics/LightingColorFilter;

    const/high16 v9, -0x1000000

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x1060016

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    invoke-direct {v8, v9, v10}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    sput-object v8, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mRedFilter:Landroid/graphics/LightingColorFilter;

    .line 155
    :cond_2
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->path:Ljava/lang/String;

    invoke-static {v8}, Lorg/namelessrom/devicecontrol/utils/Utils;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mOriginalValue:Ljava/lang/String;

    .line 158
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    move-result-object v8

    const-string v9, "vibrator_tuning"

    invoke-virtual {v8, v9}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->getItemByName(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/objects/BootupItem;

    move-result-object v1

    .line 160
    .local v1, "item":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    if-eqz v1, :cond_4

    iget-object v7, v1, Lorg/namelessrom/devicecontrol/objects/BootupItem;->value:Ljava/lang/String;

    .line 161
    .local v7, "value":Ljava/lang/String;
    :goto_1
    if-eqz v7, :cond_5

    iget-object v8, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mOriginalValue:Ljava/lang/String;

    invoke-static {v8}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v8

    :goto_2
    invoke-direct {p0, v8}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->strengthToPercent(I)I

    move-result v4

    .line 163
    .local v4, "percent":I
    const-string v8, "value: %s, percent: %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v7, v9, v12

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v13

    invoke-static {p0, v8, v9}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 164
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v8, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 165
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v8, v4}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 167
    const v8, 0x7f0c00da

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    new-instance v9, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity$1;

    invoke-direct {v9, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity$1;-><init>(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;)V

    invoke-virtual {v8, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    return-void

    .line 138
    .end local v1    # "item":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    .end local v4    # "percent":I
    .end local v5    # "progressDrawable":Landroid/graphics/drawable/Drawable;
    .end local v7    # "value":Ljava/lang/String;
    :cond_3
    const/16 v8, 0x8

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 160
    .restart local v1    # "item":Lorg/namelessrom/devicecontrol/objects/BootupItem;
    .restart local v5    # "progressDrawable":Landroid/graphics/drawable/Drawable;
    :cond_4
    const/4 v7, 0x0

    goto :goto_1

    .line 161
    .restart local v7    # "value":Ljava/lang/String;
    :cond_5
    iget v8, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->defValue:I

    goto :goto_2
.end method

.method protected onDialogClosed(Z)V
    .locals 7
    .param p1, "positiveResult"    # Z

    .prologue
    .line 181
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onDialogClosed(Z)V

    .line 182
    if-eqz p1, :cond_0

    .line 183
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->getContext()Landroid/content/Context;

    move-result-object v6

    new-instance v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    const-string v1, "device"

    const-string v2, "vibrator_tuning"

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->path:Ljava/lang/String;

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getProgress()I

    move-result v4

    invoke-direct {p0, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->percentToStrength(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/objects/BootupItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v6, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->setBootup(Landroid/content/Context;Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 189
    :goto_0
    return-void

    .line 187
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->path:Ljava/lang/String;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mOriginalValue:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 6
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 192
    iget v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->threshold:I

    const/4 v5, -0x1

    if-eq v3, v5, :cond_3

    iget v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->threshold:I

    invoke-direct {p0, v3}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->strengthToPercent(I)I

    move-result v3

    if-lt p2, v3, :cond_3

    move v0, v1

    .line 193
    .local v0, "shouldWarn":Z
    :goto_0
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_0

    .line 194
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_4

    sget-object v3, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mRedFilter:Landroid/graphics/LightingColorFilter;

    :goto_1
    invoke-virtual {v5, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 196
    :cond_0
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mProgressThumb:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_2

    .line 197
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mProgressThumb:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    sget-object v4, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mRedFilter:Landroid/graphics/LightingColorFilter;

    :cond_1
    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 199
    :cond_2
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->mValue:Landroid/widget/TextView;

    const-string v4, "%d%%"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-static {v4, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    return-void

    .end local v0    # "shouldWarn":Z
    :cond_3
    move v0, v2

    .line 192
    goto :goto_0

    .restart local v0    # "shouldWarn":Z
    :cond_4
    move-object v3, v4

    .line 194
    goto :goto_1
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 202
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 205
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-direct {p0, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->percentToStrength(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 206
    .local v0, "value":Ljava/lang/String;
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/VibratorIntensity;->path:Ljava/lang/String;

    invoke-static {v1, v0}, Lorg/namelessrom/devicecontrol/utils/Utils;->getWriteCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;)V

    .line 207
    return-void
.end method
