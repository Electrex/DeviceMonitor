.class public Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;
.super Landroid/preference/DialogPreference;
.source "DisplayColor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;
    }
.end annotation


# static fields
.field private static final SEEKBAR_ID:[I

.field private static final SEEKBAR_VALUE_ID:[I


# instance fields
.field private mCurrentColors:[Ljava/lang/String;

.field private mOriginalColors:Ljava/lang/String;

.field private mSeekBars:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 49
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->SEEKBAR_ID:[I

    .line 55
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->SEEKBAR_VALUE_ID:[I

    return-void

    .line 49
    :array_0
    .array-data 4
        0x7f0c00de
        0x7f0c00e1
        0x7f0c00e4
    .end array-data

    .line 55
    :array_1
    .array-data 4
        0x7f0c00dd
        0x7f0c00e0
        0x7f0c00e3
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->SEEKBAR_ID:[I

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->mSeekBars:Ljava/util/List;

    .line 68
    const v0, 0x7f040045

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->setLayoutResource(I)V

    .line 69
    const v0, 0x7f040048

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->setDialogLayoutResource(I)V

    .line 70
    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;

    .prologue
    .line 47
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->mSeekBars:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;

    .prologue
    .line 47
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->mCurrentColors:[Ljava/lang/String;

    return-object v0
.end method

.method public static isSupported()Z
    .locals 1

    .prologue
    .line 121
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->isSupported()Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected onBindDialogView(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 73
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindDialogView(Landroid/view/View;)V

    .line 75
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;

    move-result-object v4

    invoke-virtual {v4}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->getCurColors()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->mOriginalColors:Ljava/lang/String;

    .line 76
    const-string v4, "mOriginalColors -> %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->mOriginalColors:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {p0, v4, v5}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->mOriginalColors:Ljava/lang/String;

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->mCurrentColors:[Ljava/lang/String;

    .line 79
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v4, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->SEEKBAR_ID:[I

    array-length v4, v4

    if-ge v1, v4, :cond_0

    .line 80
    sget-object v4, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->SEEKBAR_ID:[I

    aget v4, v4, v1

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    .line 81
    .local v2, "seekBar":Landroid/widget/SeekBar;
    sget-object v4, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->SEEKBAR_VALUE_ID:[I

    aget v4, v4, v1

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 82
    .local v3, "value":Landroid/widget/TextView;
    new-instance v0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;

    invoke-direct {v0, p0, v2, v3, v1}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;-><init>(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;Landroid/widget/SeekBar;Landroid/widget/TextView;I)V

    .line 83
    .local v0, "colorSeekBar":Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->mSeekBars:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->mCurrentColors:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v0, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;->setValueFromString(Ljava/lang/String;)V

    .line 79
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 86
    .end local v0    # "colorSeekBar":Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;
    .end local v2    # "seekBar":Landroid/widget/SeekBar;
    .end local v3    # "value":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method protected onDialogClosed(Z)V
    .locals 7
    .param p1, "positiveResult"    # Z

    .prologue
    .line 109
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onDialogClosed(Z)V

    .line 111
    if-eqz p1, :cond_1

    .line 112
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->getContext()Landroid/content/Context;

    move-result-object v6

    new-instance v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    const-string v1, "device"

    const-string v2, "display_color_calibration"

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;

    move-result-object v3

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;

    move-result-object v4

    invoke-virtual {v4}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->getCurColors()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/objects/BootupItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v6, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->setBootup(Landroid/content/Context;Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->mOriginalColors:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 117
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;

    move-result-object v0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->mOriginalColors:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->setColors(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected showDialog(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "state"    # Landroid/os/Bundle;

    .prologue
    .line 89
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->showDialog(Landroid/os/Bundle;)V

    .line 93
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    .line 94
    .local v0, "d":Landroid/app/AlertDialog;
    const/4 v2, -0x3

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 95
    .local v1, "defaultsButton":Landroid/widget/Button;
    new-instance v2, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$1;

    invoke-direct {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$1;-><init>(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    return-void
.end method
