.class Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;
.super Ljava/lang/Object;
.source "DisplayColor.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ColorSeekBar"
.end annotation


# instance fields
.field private mIndex:I

.field private mSeekBar:Landroid/widget/SeekBar;

.field private mValue:Landroid/widget/TextView;

.field final synthetic this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;


# direct methods
.method public constructor <init>(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;Landroid/widget/SeekBar;Landroid/widget/TextView;I)V
    .locals 3
    .param p2, "seekBar"    # Landroid/widget/SeekBar;
    .param p3, "value"    # Landroid/widget/TextView;
    .param p4, "index"    # I

    .prologue
    .line 128
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;->this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;->mSeekBar:Landroid/widget/SeekBar;

    .line 130
    iput-object p3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;->mValue:Landroid/widget/TextView;

    .line 131
    iput p4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;->mIndex:I

    .line 133
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;->mSeekBar:Landroid/widget/SeekBar;

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->getMaxValue()I

    move-result v1

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;

    move-result-object v2

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->getMinValue()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 135
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 136
    return-void
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;

    .prologue
    .line 123
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;->mSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 8
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 143
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;

    move-result-object v3

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->getMinValue()I

    move-result v1

    .line 144
    .local v1, "min":I
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;

    move-result-object v3

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->getMaxValue()I

    move-result v0

    .line 146
    .local v0, "max":I
    if-eqz p3, :cond_0

    .line 147
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;->this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;

    # getter for: Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->mCurrentColors:[Ljava/lang/String;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->access$200(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;)[Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;->mIndex:I

    add-int v5, p2, v1

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 148
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;

    move-result-object v3

    const-string v4, " "

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;->this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;

    # getter for: Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->mCurrentColors:[Ljava/lang/String;
    invoke-static {v5}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->access$200(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;)[Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->setColors(Ljava/lang/String;)V

    .line 151
    :cond_0
    const/high16 v3, 0x42c80000    # 100.0f

    int-to-float v4, p2

    mul-float/2addr v3, v4

    sub-int v4, v0, v1

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 152
    .local v2, "percent":I
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;->mValue:Landroid/widget/TextView;

    const-string v4, "%d%%"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 155
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 157
    return-void
.end method

.method public setValueFromString(Ljava/lang/String;)V
    .locals 2
    .param p1, "valueString"    # Ljava/lang/String;

    .prologue
    .line 139
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;->mSeekBar:Landroid/widget/SeekBar;

    invoke-static {p1}, Lorg/namelessrom/devicecontrol/utils/Utils;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 140
    return-void
.end method
