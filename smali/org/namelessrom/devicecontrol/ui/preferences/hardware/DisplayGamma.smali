.class public Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;
.super Landroid/preference/DialogPreference;
.source "DisplayGamma.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;,
        Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;
    }
.end annotation


# static fields
.field private static final BAR_COLORS:[I


# instance fields
.field private mCurrentColors:[[Ljava/lang/String;

.field private mNumberOfControls:I

.field private mOriginalColors:[Ljava/lang/String;

.field private mSeekBars:[[Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->BAR_COLORS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0e01ad
        0x7f0e012c
        0x7f0e0037
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    invoke-static {}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->isSupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    :goto_0
    return-void

    .line 68
    :cond_0
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->getNumberOfControls()I

    move-result v0

    iput v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mNumberOfControls:I

    .line 69
    iget v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mNumberOfControls:I

    sget-object v1, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->BAR_COLORS:[I

    array-length v1, v1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mSeekBars:[[Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;

    .line 71
    iget v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mNumberOfControls:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mOriginalColors:[Ljava/lang/String;

    .line 72
    iget v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mNumberOfControls:I

    new-array v0, v0, [[Ljava/lang/String;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mCurrentColors:[[Ljava/lang/String;

    .line 74
    const v0, 0x7f040045

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->setLayoutResource(I)V

    .line 75
    const v0, 0x7f040049

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->setDialogLayoutResource(I)V

    goto :goto_0
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;)[[Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;

    .prologue
    .line 48
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mSeekBars:[[Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;

    return-object v0
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;

    .prologue
    .line 48
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mOriginalColors:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200()[I
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->BAR_COLORS:[I

    return-object v0
.end method

.method static synthetic access$300(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;)[[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;

    .prologue
    .line 48
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mCurrentColors:[[Ljava/lang/String;

    return-object v0
.end method

.method public static isSupported()Z
    .locals 1

    .prologue
    .line 222
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->isSupported()Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected onBindDialogView(Landroid/view/View;)V
    .locals 14
    .param p1, "view"    # Landroid/view/View;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitPrefEdits"
        }
    .end annotation

    .prologue
    .line 79
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindDialogView(Landroid/view/View;)V

    .line 81
    const v10, 0x7f0c00e6

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 82
    .local v1, "container":Landroid/view/ViewGroup;
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 83
    .local v6, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v8

    .line 84
    .local v8, "prefs":Landroid/content/SharedPreferences;
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 85
    .local v9, "res":Landroid/content/res/Resources;
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;

    move-result-object v10

    invoke-virtual {v10}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->getDescriptors()[Ljava/lang/String;

    move-result-object v3

    .line 89
    .local v3, "gammaDescriptors":[Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, "index":I
    :goto_0
    iget v10, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mNumberOfControls:I

    if-ge v5, v10, :cond_4

    .line 90
    iget-object v10, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mOriginalColors:[Ljava/lang/String;

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;

    move-result-object v11

    invoke-virtual {v11, v5}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->getCurGamma(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v5

    .line 91
    iget-object v10, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mCurrentColors:[[Ljava/lang/String;

    iget-object v11, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mOriginalColors:[Ljava/lang/String;

    aget-object v11, v11, v5

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v5

    .line 93
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "display_gamma_default_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 94
    .local v2, "defaultKey":Ljava/lang/String;
    invoke-interface {v8, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 95
    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    iget-object v11, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mOriginalColors:[Ljava/lang/String;

    aget-object v11, v11, v5

    invoke-interface {v10, v2, v11}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    invoke-interface {v10}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 98
    :cond_0
    iget v10, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mNumberOfControls:I

    const/4 v11, 0x1

    if-eq v10, v11, :cond_1

    .line 99
    const v10, 0x7f04004a

    const/4 v11, 0x0

    invoke-virtual {v6, v10, v1, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 102
    .local v4, "header":Landroid/widget/TextView;
    array-length v10, v3

    if-ge v5, v10, :cond_2

    .line 103
    aget-object v10, v3, v5

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    :goto_1
    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 111
    .end local v4    # "header":Landroid/widget/TextView;
    :cond_1
    const/4 v0, 0x0

    .local v0, "color":I
    :goto_2
    sget-object v10, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->BAR_COLORS:[I

    array-length v10, v10

    if-ge v0, v10, :cond_3

    .line 112
    const v10, 0x7f04004b

    const/4 v11, 0x0

    invoke-virtual {v6, v10, v1, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    .line 115
    .local v7, "item":Landroid/view/ViewGroup;
    iget-object v10, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mSeekBars:[[Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;

    aget-object v10, v10, v5

    new-instance v11, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;

    invoke-direct {v11, p0, v5, v0, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;-><init>(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;IILandroid/view/ViewGroup;)V

    aput-object v11, v10, v0

    .line 116
    iget-object v10, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mSeekBars:[[Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;

    aget-object v10, v10, v5

    aget-object v10, v10, v0

    iget-object v11, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mCurrentColors:[[Ljava/lang/String;

    aget-object v11, v11, v5

    aget-object v11, v11, v0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-virtual {v10, v11}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->setGamma(I)V

    .line 120
    invoke-virtual {v1, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 105
    .end local v0    # "color":I
    .end local v7    # "item":Landroid/view/ViewGroup;
    .restart local v4    # "header":Landroid/widget/TextView;
    :cond_2
    const v10, 0x7f0e0114

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    add-int/lit8 v13, v5, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v9, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 89
    .end local v4    # "header":Landroid/widget/TextView;
    .restart local v0    # "color":I
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 123
    .end local v0    # "color":I
    .end local v2    # "defaultKey":Ljava/lang/String;
    :cond_4
    return-void
.end method

.method protected onDialogClosed(Z)V
    .locals 11
    .param p1, "positiveResult"    # Z

    .prologue
    .line 157
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onDialogClosed(Z)V

    .line 159
    if-eqz p1, :cond_3

    .line 160
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    move-result-object v7

    .line 161
    .local v7, "config":Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mNumberOfControls:I

    if-ge v8, v0, :cond_1

    .line 162
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;

    move-result-object v0

    invoke-virtual {v0, v8}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->getPaths(I)[Ljava/lang/String;

    move-result-object v6

    .local v6, "arr$":[Ljava/lang/String;
    array-length v10, v6

    .local v10, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_1
    if-ge v9, v10, :cond_0

    aget-object v3, v6, v9

    .line 163
    .local v3, "path":Ljava/lang/String;
    new-instance v0, Lorg/namelessrom/devicecontrol/objects/BootupItem;

    const-string v1, "device"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "display_gamma_calibration"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;

    move-result-object v4

    invoke-virtual {v4, v8}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->getCurGamma(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lorg/namelessrom/devicecontrol/objects/BootupItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v7, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->addItem(Lorg/namelessrom/devicecontrol/objects/BootupItem;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 162
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 161
    .end local v3    # "path":Ljava/lang/String;
    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 168
    .end local v6    # "arr$":[Ljava/lang/String;
    .end local v9    # "i$":I
    .end local v10    # "len$":I
    :cond_1
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v7, v0}, Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;

    .line 174
    .end local v7    # "config":Lorg/namelessrom/devicecontrol/configuration/BootupConfiguration;
    .end local v8    # "i":I
    :cond_2
    return-void

    .line 169
    :cond_3
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mOriginalColors:[Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 170
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_2
    iget v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mNumberOfControls:I

    if-ge v8, v0, :cond_2

    .line 171
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;

    move-result-object v0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mOriginalColors:[Ljava/lang/String;

    aget-object v1, v1, v8

    invoke-virtual {v0, v8, v1}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->setGamma(ILjava/lang/String;)V

    .line 170
    add-int/lit8 v8, v8, 0x1

    goto :goto_2
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 6
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 200
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-class v4, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 202
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 219
    :cond_1
    return-void

    :cond_2
    move-object v2, p1

    .line 206
    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;

    .line 207
    .local v2, "myState":Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;
    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v3

    invoke-super {p0, v3}, Landroid/preference/DialogPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 208
    iget v3, v2, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;->controlCount:I

    iput v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mNumberOfControls:I

    .line 209
    iget-object v3, v2, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;->originalColors:[Ljava/lang/String;

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mOriginalColors:[Ljava/lang/String;

    .line 210
    iget-object v3, v2, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;->currentColors:[[Ljava/lang/String;

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mCurrentColors:[[Ljava/lang/String;

    .line 212
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    iget v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mNumberOfControls:I

    if-ge v1, v3, :cond_1

    .line 213
    const/4 v0, 0x0

    .local v0, "color":I
    :goto_1
    sget-object v3, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->BAR_COLORS:[I

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 214
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mSeekBars:[[Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;

    aget-object v3, v3, v1

    aget-object v3, v3, v0

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mCurrentColors:[[Ljava/lang/String;

    aget-object v4, v4, v1

    aget-object v4, v4, v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->setGamma(I)V

    .line 213
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 216
    :cond_3
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;

    move-result-object v3

    const-string v4, " "

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mCurrentColors:[[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-static {v4, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->setGamma(ILjava/lang/String;)V

    .line 212
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 5

    .prologue
    .line 178
    invoke-super {p0}, Landroid/preference/DialogPreference;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    .line 179
    .local v2, "superState":Landroid/os/Parcelable;
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Dialog;->isShowing()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    move-object v1, v2

    .line 195
    :goto_0
    return-object v1

    .line 184
    :cond_1
    new-instance v1, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;

    invoke-direct {v1, v2}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 185
    .local v1, "myState":Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;
    iget v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mNumberOfControls:I

    iput v3, v1, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;->controlCount:I

    .line 186
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mCurrentColors:[[Ljava/lang/String;

    iput-object v3, v1, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;->currentColors:[[Ljava/lang/String;

    .line 187
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mOriginalColors:[Ljava/lang/String;

    iput-object v3, v1, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;->originalColors:[Ljava/lang/String;

    .line 190
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mNumberOfControls:I

    if-ge v0, v3, :cond_2

    .line 191
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;

    move-result-object v3

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mOriginalColors:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v0, v4}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->setGamma(ILjava/lang/String;)V

    .line 190
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 193
    :cond_2
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mOriginalColors:[Ljava/lang/String;

    goto :goto_0
.end method

.method protected showDialog(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "state"    # Landroid/os/Bundle;

    .prologue
    .line 127
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->showDialog(Landroid/os/Bundle;)V

    .line 131
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    .line 132
    .local v0, "d":Landroid/app/AlertDialog;
    const/4 v2, -0x3

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 133
    .local v1, "defaultsButton":Landroid/widget/Button;
    new-instance v2, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$1;

    invoke-direct {v2, p0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$1;-><init>(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    return-void
.end method
