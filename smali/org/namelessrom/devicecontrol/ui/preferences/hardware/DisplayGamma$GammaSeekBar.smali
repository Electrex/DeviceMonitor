.class Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;
.super Ljava/lang/Object;
.source "DisplayGamma.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GammaSeekBar"
.end annotation


# instance fields
.field private mColorIndex:I

.field private mControlIndex:I

.field private mMin:I

.field private mSeekBar:Landroid/widget/SeekBar;

.field private mValue:Landroid/widget/TextView;

.field final synthetic this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;


# direct methods
.method public constructor <init>(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;IILandroid/view/ViewGroup;)V
    .locals 4
    .param p2, "controlIndex"    # I
    .param p3, "colorIndex"    # I
    .param p4, "container"    # Landroid/view/ViewGroup;

    .prologue
    .line 275
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 276
    iput p2, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->mControlIndex:I

    .line 277
    iput p3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->mColorIndex:I

    .line 279
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;

    move-result-object v1

    invoke-virtual {v1, p2}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->getMinValue(I)I

    move-result v1

    iput v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->mMin:I

    .line 281
    const v1, 0x7f0c00e8

    invoke-virtual {p4, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->mValue:Landroid/widget/TextView;

    .line 282
    const v1, 0x7f0c00e9

    invoke-virtual {p4, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->mSeekBar:Landroid/widget/SeekBar;

    .line 284
    const v1, 0x7f0c00e7

    invoke-virtual {p4, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 285
    .local v0, "label":Landroid/widget/TextView;
    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    # getter for: Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->BAR_COLORS:[I
    invoke-static {}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->access$200()[I

    move-result-object v2

    aget v2, v2, p3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 287
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->mSeekBar:Landroid/widget/SeekBar;

    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;

    move-result-object v2

    invoke-virtual {v2, p2}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->getMaxValue(I)I

    move-result v2

    iget v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->mMin:I

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setMax(I)V

    .line 288
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->mSeekBar:Landroid/widget/SeekBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 289
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->mValue:Landroid/widget/TextView;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    iget v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->mMin:I

    add-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 292
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 293
    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 5
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 301
    if-eqz p3, :cond_0

    .line 302
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;

    # getter for: Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mCurrentColors:[[Ljava/lang/String;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->access$300(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;)[[Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->mControlIndex:I

    aget-object v0, v0, v1

    iget v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->mColorIndex:I

    iget v2, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->mMin:I

    add-int/2addr v2, p2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 303
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;

    move-result-object v0

    iget v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->mControlIndex:I

    const-string v2, " "

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;

    # getter for: Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mCurrentColors:[[Ljava/lang/String;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->access$300(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;)[[Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->mControlIndex:I

    aget-object v3, v3, v4

    invoke-static {v2, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->setGamma(ILjava/lang/String;)V

    .line 306
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->mValue:Landroid/widget/TextView;

    iget v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->mMin:I

    add-int/2addr v1, p2

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 307
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 312
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 317
    return-void
.end method

.method public setGamma(I)V
    .locals 2
    .param p1, "gamma"    # I

    .prologue
    .line 296
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->mSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->mMin:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 297
    return-void
.end method
