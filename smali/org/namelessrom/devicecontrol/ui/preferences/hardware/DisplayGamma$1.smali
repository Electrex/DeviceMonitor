.class Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$1;
.super Ljava/lang/Object;
.source "DisplayGamma.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->showDialog(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$1;->this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 136
    const/4 v3, 0x0

    .local v3, "index":I
    :goto_0
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$1;->this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;

    # getter for: Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mSeekBars:[[Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;
    invoke-static {v6}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->access$000(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;)[[Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;

    move-result-object v6

    array-length v6, v6

    if-ge v3, v6, :cond_2

    .line 137
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$1;->this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;

    invoke-virtual {v6}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v5

    .line 138
    .local v5, "prefs":Landroid/content/SharedPreferences;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "display_gamma_default_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 141
    .local v2, "defaultKey":Ljava/lang/String;
    const/4 v6, 0x0

    invoke-interface {v5, v2, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 142
    .local v4, "pref":Ljava/lang/String;
    if-eqz v4, :cond_0

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 144
    .local v1, "defaultColors":[Ljava/lang/String;
    :goto_1
    const/4 v0, 0x0

    .local v0, "color":I
    :goto_2
    # getter for: Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->BAR_COLORS:[I
    invoke-static {}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->access$200()[I

    move-result-object v6

    array-length v6, v6

    if-ge v0, v6, :cond_1

    .line 145
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$1;->this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;

    # getter for: Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mSeekBars:[[Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;
    invoke-static {v6}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->access$000(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;)[[Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;

    move-result-object v6

    aget-object v6, v6, v3

    aget-object v6, v6, v0

    aget-object v7, v1, v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6, v7}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$GammaSeekBar;->setGamma(I)V

    .line 146
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$1;->this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;

    # getter for: Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mCurrentColors:[[Ljava/lang/String;
    invoke-static {v6}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->access$300(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;)[[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v3

    aget-object v7, v1, v0

    aput-object v7, v6, v0

    .line 144
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 142
    .end local v0    # "color":I
    .end local v1    # "defaultColors":[Ljava/lang/String;
    :cond_0
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$1;->this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;

    # getter for: Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mOriginalColors:[Ljava/lang/String;
    invoke-static {v6}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->access$100(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;)[Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 148
    .restart local v0    # "color":I
    .restart local v1    # "defaultColors":[Ljava/lang/String;
    :cond_1
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;

    move-result-object v6

    const-string v7, " "

    iget-object v8, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$1;->this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;

    # getter for: Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->mCurrentColors:[[Ljava/lang/String;
    invoke-static {v8}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;->access$300(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;)[[Ljava/lang/String;

    move-result-object v8

    aget-object v8, v8, v3

    invoke-static {v7, v8}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v3, v7}, Lorg/namelessrom/devicecontrol/hardware/DisplayGammaCalibration;->setGamma(ILjava/lang/String;)V

    .line 136
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 151
    .end local v0    # "color":I
    .end local v1    # "defaultColors":[Ljava/lang/String;
    .end local v2    # "defaultKey":Ljava/lang/String;
    .end local v4    # "pref":Ljava/lang/String;
    .end local v5    # "prefs":Landroid/content/SharedPreferences;
    :cond_2
    return-void
.end method
