.class Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$1;
.super Ljava/lang/Object;
.source "DisplayColor.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->showDialog(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$1;->this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 98
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;

    move-result-object v2

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->getDefValue()I

    move-result v0

    .line 99
    .local v0, "defaultValue":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$1;->this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;

    # getter for: Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->mSeekBars:Ljava/util/List;
    invoke-static {v2}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->access$000(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 100
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$1;->this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;

    # getter for: Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->mSeekBars:Ljava/util/List;
    invoke-static {v2}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->access$000(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;

    # getter for: Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;->mSeekBar:Landroid/widget/SeekBar;
    invoke-static {v2}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;->access$100(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$ColorSeekBar;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 101
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$1;->this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;

    # getter for: Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->mCurrentColors:[Ljava/lang/String;
    invoke-static {v2}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->access$200(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 99
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 103
    :cond_0
    invoke-static {}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->get()Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;

    move-result-object v2

    const-string v3, " "

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor$1;->this$0:Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;

    # getter for: Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->mCurrentColors:[Ljava/lang/String;
    invoke-static {v4}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;->access$200(Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayColor;)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/hardware/DisplayColorCalibration;->setColors(Ljava/lang/String;)V

    .line 104
    return-void
.end method
