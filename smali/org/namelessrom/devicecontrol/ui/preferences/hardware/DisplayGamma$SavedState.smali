.class Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;
.super Landroid/preference/Preference$BaseSavedState;
.source "DisplayGamma.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field controlCount:I

.field currentColors:[[Ljava/lang/String;

.field originalColors:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 254
    new-instance v0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState$1;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState$1;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 235
    invoke-direct {p0, p1}, Landroid/preference/Preference$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 236
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;->controlCount:I

    .line 237
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;->originalColors:[Ljava/lang/String;

    .line 238
    iget v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;->controlCount:I

    new-array v1, v1, [[Ljava/lang/String;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;->currentColors:[[Ljava/lang/String;

    .line 239
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;->controlCount:I

    if-ge v0, v1, :cond_0

    .line 240
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;->currentColors:[[Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 239
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 242
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 0
    .param p1, "superState"    # Landroid/os/Parcelable;

    .prologue
    .line 231
    invoke-direct {p0, p1}, Landroid/preference/Preference$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 232
    return-void
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 246
    invoke-super {p0, p1, p2}, Landroid/preference/Preference$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 247
    iget v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;->controlCount:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 248
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;->originalColors:[Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 249
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;->controlCount:I

    if-ge v0, v1, :cond_0

    .line 250
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/ui/preferences/hardware/DisplayGamma$SavedState;->currentColors:[[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 249
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 252
    :cond_0
    return-void
.end method
