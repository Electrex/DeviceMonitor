.class public Lorg/namelessrom/devicecontrol/activities/FilePickerActivity;
.super Lorg/namelessrom/devicecontrol/activities/BaseActivity;
.source "FilePickerActivity.java"

# interfaces
.implements Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerListener;


# instance fields
.field private mCurrentFragment:Landroid/app/Fragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/activities/FilePickerActivity;->mCurrentFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lorg/namelessrom/devicecontrol/listeners/OnBackPressedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/activities/FilePickerActivity;->mCurrentFragment:Landroid/app/Fragment;

    check-cast v0, Lorg/namelessrom/devicecontrol/listeners/OnBackPressedListener;

    invoke-interface {v0}, Lorg/namelessrom/devicecontrol/listeners/OnBackPressedListener;->onBackPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    const-string v0, "onBackPressed()"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    :goto_0
    return-void

    .line 101
    :cond_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/activities/FilePickerActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 102
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/activities/FilePickerActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    goto :goto_0

    .line 104
    :cond_1
    invoke-super {p0}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 48
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    const v1, 0x7f04001b

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/activities/FilePickerActivity;->setContentView(I)V

    .line 51
    const v1, 0x7f0c0061

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/activities/FilePickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 52
    .local v0, "toolbar":Landroid/support/v7/widget/Toolbar;
    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/activities/FilePickerActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 54
    new-instance v1, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;

    invoke-direct {v1}, Lorg/namelessrom/devicecontrol/ui/fragments/filepicker/FilePickerFragment;-><init>()V

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/activities/FilePickerActivity;->mCurrentFragment:Landroid/app/Fragment;

    .line 55
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/activities/FilePickerActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0c0072

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/activities/FilePickerActivity;->mCurrentFragment:Landroid/app/Fragment;

    invoke-virtual {v1, v2, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 58
    return-void
.end method

.method public onFilePicked(Ljava/io/File;)V
    .locals 0
    .param p1, "ignored"    # Ljava/io/File;

    .prologue
    .line 95
    return-void
.end method

.method public onFlashItemPicked(Lorg/namelessrom/devicecontrol/objects/FlashItem;)V
    .locals 10
    .param p1, "flashItem"    # Lorg/namelessrom/devicecontrol/objects/FlashItem;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 61
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, v8}, Landroid/os/Bundle;-><init>(I)V

    .line 62
    .local v0, "b":Landroid/os/Bundle;
    const-string v6, "name"

    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/objects/FlashItem;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v6, "path"

    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/objects/FlashItem;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 65
    .local v2, "i":Landroid/content/Intent;
    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 66
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x10

    if-lt v6, v7, :cond_0

    .line 67
    new-instance v6, Ljava/io/File;

    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/objects/FlashItem;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    .line 68
    .local v5, "uri":Landroid/net/Uri;
    const-string v6, "Uri: %s"

    new-array v7, v8, [Ljava/lang/Object;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {p0, v6, v7}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 69
    invoke-virtual {v2, v5}, Landroid/content/Intent;->setDataAndNormalize(Landroid/net/Uri;)Landroid/content/Intent;

    .line 91
    :goto_0
    const/4 v6, -0x1

    invoke-virtual {p0, v6, v2}, Lorg/namelessrom/devicecontrol/activities/FilePickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 92
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/activities/FilePickerActivity;->finish()V

    .line 93
    return-void

    .line 74
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_0
    :try_start_0
    new-instance v6, Ljava/io/File;

    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/objects/FlashItem;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v6

    invoke-virtual {v6}, Ljava/net/URI;->normalize()Ljava/net/URI;

    move-result-object v6

    invoke-virtual {v6}, Ljava/net/URI;->getPath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 79
    .local v3, "path":Ljava/lang/String;
    :goto_1
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    .line 80
    .restart local v5    # "uri":Landroid/net/Uri;
    invoke-virtual {v5}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    .line 81
    .local v4, "scheme":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 82
    sget-object v6, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {v4, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 86
    :cond_1
    new-instance v6, Landroid/net/Uri$Builder;

    invoke-direct {v6}, Landroid/net/Uri$Builder;-><init>()V

    invoke-virtual {v6, v4}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 88
    const-string v6, "Legacy | Uri: %s"

    new-array v7, v8, [Ljava/lang/Object;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {p0, v6, v7}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    invoke-virtual {v2, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0

    .line 75
    .end local v3    # "path":Ljava/lang/String;
    .end local v4    # "scheme":Ljava/lang/String;
    .end local v5    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v1

    .line 76
    .local v1, "exc":Ljava/lang/Exception;
    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/objects/FlashItem;->getPath()Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "path":Ljava/lang/String;
    goto :goto_1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 43
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 44
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/activities/FilePickerActivity;->setIntent(Landroid/content/Intent;)V

    .line 45
    return-void
.end method
