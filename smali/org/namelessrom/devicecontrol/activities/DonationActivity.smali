.class public Lorg/namelessrom/devicecontrol/activities/DonationActivity;
.super Lorg/namelessrom/devicecontrol/activities/BaseActivity;
.source "DonationActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;
.implements Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;


# instance fields
.field private mBillingProcessor:Lcom/anjlab/android/iab/v3/BillingProcessor;

.field private mGooglePlay:Landroid/widget/Button;

.field private mRadioGroup:Landroid/widget/RadioGroup;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 150
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->mBillingProcessor:Lcom/anjlab/android/iab/v3/BillingProcessor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->mBillingProcessor:Lcom/anjlab/android/iab/v3/BillingProcessor;

    invoke-virtual {v0, p1, p2, p3}, Lcom/anjlab/android/iab/v3/BillingProcessor;->handleActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 152
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 154
    :cond_1
    return-void
.end method

.method public onBillingError(ILjava/lang/Throwable;)V
    .locals 0
    .param p1, "errorCode"    # I
    .param p2, "error"    # Ljava/lang/Throwable;

    .prologue
    .line 180
    return-void
.end method

.method public onBillingInitialized()V
    .locals 0

    .prologue
    .line 184
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 2
    .param p1, "group"    # Landroid/widget/RadioGroup;
    .param p2, "checkedId"    # I

    .prologue
    .line 97
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->mGooglePlay:Landroid/widget/Button;

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 98
    return-void

    .line 97
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 101
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 102
    .local v1, "id":I
    packed-switch v1, :pswitch_data_0

    .line 147
    :goto_0
    return-void

    .line 104
    :pswitch_0
    const v5, 0x7f0c006b

    invoke-virtual {p0, v5}, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 105
    .local v0, "cbSub":Landroid/widget/CheckBox;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v4, 0x1

    .line 107
    .local v4, "useSub":Z
    :goto_1
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->mRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v5}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v3

    .line 109
    .local v3, "radioButtonId":I
    packed-switch v3, :pswitch_data_1

    .line 112
    if-eqz v4, :cond_1

    const-string v2, "donation_sub_1"

    .line 137
    .local v2, "productId":Ljava/lang/String;
    :goto_2
    if-eqz v4, :cond_6

    .line 138
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->mBillingProcessor:Lcom/anjlab/android/iab/v3/BillingProcessor;

    invoke-virtual {v5, p0, v2}, Lcom/anjlab/android/iab/v3/BillingProcessor;->subscribe(Landroid/app/Activity;Ljava/lang/String;)Z

    goto :goto_0

    .line 105
    .end local v2    # "productId":Ljava/lang/String;
    .end local v3    # "radioButtonId":I
    .end local v4    # "useSub":Z
    :cond_0
    const/4 v4, 0x0

    goto :goto_1

    .line 112
    .restart local v3    # "radioButtonId":I
    .restart local v4    # "useSub":Z
    :cond_1
    const-string v2, "donation_1"

    goto :goto_2

    .line 117
    :pswitch_1
    if-eqz v4, :cond_2

    const-string v2, "donation_sub_2"

    .line 120
    .restart local v2    # "productId":Ljava/lang/String;
    :goto_3
    goto :goto_2

    .line 117
    .end local v2    # "productId":Ljava/lang/String;
    :cond_2
    const-string v2, "donation_2"

    goto :goto_3

    .line 122
    :pswitch_2
    if-eqz v4, :cond_3

    const-string v2, "donation_sub_3"

    .line 125
    .restart local v2    # "productId":Ljava/lang/String;
    :goto_4
    goto :goto_2

    .line 122
    .end local v2    # "productId":Ljava/lang/String;
    :cond_3
    const-string v2, "donation_3"

    goto :goto_4

    .line 127
    :pswitch_3
    if-eqz v4, :cond_4

    const-string v2, "donation_sub_4"

    .line 130
    .restart local v2    # "productId":Ljava/lang/String;
    :goto_5
    goto :goto_2

    .line 127
    .end local v2    # "productId":Ljava/lang/String;
    :cond_4
    const-string v2, "donation_4"

    goto :goto_5

    .line 132
    :pswitch_4
    if-eqz v4, :cond_5

    const-string v2, "donation_sub_5"

    .restart local v2    # "productId":Ljava/lang/String;
    :goto_6
    goto :goto_2

    .end local v2    # "productId":Ljava/lang/String;
    :cond_5
    const-string v2, "donation_5"

    goto :goto_6

    .line 140
    .restart local v2    # "productId":Ljava/lang/String;
    :cond_6
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->mBillingProcessor:Lcom/anjlab/android/iab/v3/BillingProcessor;

    invoke-virtual {v5, p0, v2}, Lcom/anjlab/android/iab/v3/BillingProcessor;->purchase(Landroid/app/Activity;Ljava/lang/String;)Z

    goto :goto_0

    .line 144
    .end local v0    # "cbSub":Landroid/widget/CheckBox;
    .end local v2    # "productId":Ljava/lang/String;
    .end local v3    # "radioButtonId":I
    .end local v4    # "useSub":Z
    :pswitch_5
    invoke-static {p0}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->startExternalDonation(Landroid/content/Context;)Z

    goto :goto_0

    .line 102
    :pswitch_data_0
    .packed-switch 0x7f0c006c
        :pswitch_0
        :pswitch_5
    .end packed-switch

    .line 109
    :pswitch_data_1
    .packed-switch 0x7f0c0067
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v9, 0x7f0e00a8

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 50
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    const v4, 0x7f040019

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->setContentView(I)V

    .line 54
    new-instance v4, Lcom/anjlab/android/iab/v3/BillingProcessor;

    invoke-static {}, Lorg/namelessrom/proprietary/Configuration;->getGooglePlayApiKeyDc()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, p0, v5, p0}, Lcom/anjlab/android/iab/v3/BillingProcessor;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;)V

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->mBillingProcessor:Lcom/anjlab/android/iab/v3/BillingProcessor;

    .line 57
    const v4, 0x7f0c0061

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/Toolbar;

    .line 58
    .local v3, "toolbar":Landroid/support/v7/widget/Toolbar;
    invoke-virtual {p0, v3}, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 59
    new-instance v4, Lorg/namelessrom/devicecontrol/activities/DonationActivity$1;

    invoke-direct {v4, p0}, Lorg/namelessrom/devicecontrol/activities/DonationActivity$1;-><init>(Lorg/namelessrom/devicecontrol/activities/DonationActivity;)V

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    new-instance v1, Lorg/namelessrom/devicecontrol/activities/DonationActivity$2;

    const/4 v4, -0x1

    sget-object v5, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;->THIN:Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    invoke-direct {v1, p0, p0, v4, v5}, Lorg/namelessrom/devicecontrol/activities/DonationActivity$2;-><init>(Lorg/namelessrom/devicecontrol/activities/DonationActivity;Landroid/app/Activity;ILcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;)V

    .line 66
    .local v1, "materialMenu":Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;
    sget-object v4, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->ARROW:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    invoke-virtual {v1, v4}, Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;->setState(Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;)V

    .line 67
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-lt v4, v5, :cond_0

    .line 68
    invoke-virtual {v1, v7}, Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;->setNeverDrawTouch(Z)V

    .line 72
    :cond_0
    const v4, 0x7f0c006c

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->mGooglePlay:Landroid/widget/Button;

    .line 73
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->mGooglePlay:Landroid/widget/Button;

    new-array v5, v7, [Ljava/lang/Object;

    const v6, 0x7f0e011d

    invoke-virtual {p0, v6}, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p0, v9, v5}, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 74
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->mGooglePlay:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    const v4, 0x7f0c006d

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 77
    .local v2, "payPal":Landroid/widget/Button;
    new-array v4, v7, [Ljava/lang/Object;

    const v5, 0x7f0e018e

    invoke-virtual {p0, v5}, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {p0, v9, v4}, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 78
    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    const v4, 0x7f0c0065

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioGroup;

    iput-object v4, p0, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->mRadioGroup:Landroid/widget/RadioGroup;

    .line 82
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->mRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v4, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 83
    const v4, 0x7f0e00a7

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 84
    .local v0, "donateValue":Ljava/lang/String;
    const v4, 0x7f0c0066

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    new-array v5, v7, [Ljava/lang/Object;

    const-string v6, "2\u20ac"

    aput-object v6, v5, v8

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 86
    const v4, 0x7f0c0067

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    new-array v5, v7, [Ljava/lang/Object;

    const-string v6, "5\u20ac"

    aput-object v6, v5, v8

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 88
    const v4, 0x7f0c0068

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    new-array v5, v7, [Ljava/lang/Object;

    const-string v6, "10\u20ac"

    aput-object v6, v5, v8

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 90
    const v4, 0x7f0c0069

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    new-array v5, v7, [Ljava/lang/Object;

    const-string v6, "20\u20ac"

    aput-object v6, v5, v8

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 92
    const v4, 0x7f0c006a

    invoke-virtual {p0, v4}, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    new-array v5, v7, [Ljava/lang/Object;

    const-string v6, "50\u20ac"

    aput-object v6, v5, v8

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 94
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->mBillingProcessor:Lcom/anjlab/android/iab/v3/BillingProcessor;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->mBillingProcessor:Lcom/anjlab/android/iab/v3/BillingProcessor;

    invoke-virtual {v0}, Lcom/anjlab/android/iab/v3/BillingProcessor;->release()V

    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->mBillingProcessor:Lcom/anjlab/android/iab/v3/BillingProcessor;

    .line 162
    :cond_0
    invoke-super {p0}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onDestroy()V

    .line 163
    return-void
.end method

.method public onProductPurchased(Ljava/lang/String;Lcom/anjlab/android/iab/v3/TransactionDetails;)V
    .locals 2
    .param p1, "productId"    # Ljava/lang/String;
    .param p2, "details"    # Lcom/anjlab/android/iab/v3/TransactionDetails;

    .prologue
    .line 166
    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/anjlab/android/iab/v3/TransactionDetails;->productId:Ljava/lang/String;

    const-string v1, "donation_sub_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    :goto_0
    return-void

    .line 171
    :cond_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/activities/DonationActivity;->mBillingProcessor:Lcom/anjlab/android/iab/v3/BillingProcessor;

    invoke-virtual {v0, p1}, Lcom/anjlab/android/iab/v3/BillingProcessor;->consumePurchase(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public onPurchaseHistoryRestored()V
    .locals 0

    .prologue
    .line 176
    return-void
.end method
