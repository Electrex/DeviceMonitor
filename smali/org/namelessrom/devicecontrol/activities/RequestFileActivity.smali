.class public Lorg/namelessrom/devicecontrol/activities/RequestFileActivity;
.super Landroid/app/Activity;
.source "RequestFileActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/activities/RequestFileActivity$RequestFileCallback;
    }
.end annotation


# static fields
.field private static sCallback:Lorg/namelessrom/devicecontrol/activities/RequestFileActivity$RequestFileCallback;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 49
    return-void
.end method

.method private handleActivityResult(Landroid/content/Intent;I)V
    .locals 13
    .param p1, "data"    # Landroid/content/Intent;
    .param p2, "reqCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 124
    const/4 v7, 0x0

    .line 125
    .local v7, "filePath":Ljava/lang/String;
    const/16 v2, 0xcb

    if-ne p2, v2, :cond_4

    .line 126
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 127
    .local v1, "uri":Landroid/net/Uri;
    if-eqz v1, :cond_0

    .line 128
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v7

    .line 135
    :cond_0
    :goto_0
    const-string v2, "uri: %s, filepath: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v4

    const/4 v4, 0x1

    aput-object v7, v3, v4

    invoke-static {p0, v2, v3}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 137
    invoke-static {v7}, Lorg/namelessrom/devicecontrol/utils/Utils;->fileExists(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz v1, :cond_2

    .line 138
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/activities/RequestFileActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 139
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 141
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 142
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 143
    const-string v2, "_data"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 144
    .local v9, "index":I
    if-ltz v9, :cond_5

    .line 145
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 170
    .end local v9    # "index":I
    :cond_1
    :goto_1
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 174
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    sget-object v2, Lorg/namelessrom/devicecontrol/activities/RequestFileActivity;->sCallback:Lorg/namelessrom/devicecontrol/activities/RequestFileActivity$RequestFileCallback;

    if-eqz v2, :cond_3

    .line 175
    sget-object v2, Lorg/namelessrom/devicecontrol/activities/RequestFileActivity;->sCallback:Lorg/namelessrom/devicecontrol/activities/RequestFileActivity$RequestFileCallback;

    invoke-interface {v2, v7}, Lorg/namelessrom/devicecontrol/activities/RequestFileActivity$RequestFileCallback;->fileRequested(Ljava/lang/String;)V

    .line 177
    :cond_3
    return-void

    .line 131
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_4
    const/4 v1, 0x0

    .line 132
    .restart local v1    # "uri":Landroid/net/Uri;
    const-string v2, "path"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 146
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v9    # "index":I
    :cond_5
    :try_start_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "content"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 148
    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string v3, "content"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "document"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    .line 153
    .local v10, "newUri":Ljava/lang/String;
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    .line 154
    .local v11, "path":Ljava/lang/String;
    const-string v2, ":"

    invoke-virtual {v7, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    .line 155
    invoke-virtual {v11, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    if-ltz v9, :cond_7

    .line 156
    const/4 v2, 0x0

    invoke-virtual {v7, v2, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 157
    .local v8, "firstPath":Ljava/lang/String;
    add-int/lit8 v2, v9, 0x1

    invoke-virtual {v7, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 158
    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->get()Lorg/namelessrom/devicecontrol/utils/IOUtils;

    move-result-object v2

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->getPrimarySdCard()Ljava/lang/String;

    move-result-object v12

    .line 159
    .local v12, "storage":Ljava/lang/String;
    const-string v2, "primary"

    invoke-virtual {v8, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 160
    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->get()Lorg/namelessrom/devicecontrol/utils/IOUtils;

    move-result-object v2

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/utils/IOUtils;->getSecondarySdCard()Ljava/lang/String;

    move-result-object v12

    .line 162
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    .line 163
    goto/16 :goto_1

    .line 164
    .end local v8    # "firstPath":Ljava/lang/String;
    .end local v12    # "storage":Ljava/lang/String;
    :cond_7
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 170
    .end local v9    # "index":I
    .end local v10    # "newUri":Ljava/lang/String;
    .end local v11    # "path":Ljava/lang/String;
    :catchall_0
    move-exception v2

    if-eqz v6, :cond_8

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v2
.end method

.method private launchInternalPicker()V
    .locals 4

    .prologue
    .line 88
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 89
    .local v2, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lorg/namelessrom/devicecontrol/activities/FilePickerActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 90
    .local v1, "i":Landroid/content/Intent;
    const-string v3, "application/zip"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    const/16 v3, 0xcc

    :try_start_0
    invoke-virtual {p0, v2, v3}, Lorg/namelessrom/devicecontrol/activities/RequestFileActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :goto_0
    return-void

    .line 93
    :catch_0
    move-exception v0

    .line 94
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "Could not start default activity to pick files"

    invoke-static {p0, v3, v0}, Lorg/namelessrom/devicecontrol/Logger;->wtf(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static setRequestFileCallback(Lorg/namelessrom/devicecontrol/activities/RequestFileActivity$RequestFileCallback;)V
    .locals 0
    .param p0, "callback"    # Lorg/namelessrom/devicecontrol/activities/RequestFileActivity$RequestFileCallback;

    .prologue
    .line 56
    sput-object p0, Lorg/namelessrom/devicecontrol/activities/RequestFileActivity;->sCallback:Lorg/namelessrom/devicecontrol/activities/RequestFileActivity$RequestFileCallback;

    .line 57
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const v3, 0x7f0e01f8

    const/4 v2, 0x1

    .line 100
    const/4 v1, -0x1

    if-ne p2, v1, :cond_1

    const/16 v1, 0xcb

    if-eq p1, v1, :cond_0

    const/16 v1, 0xcc

    if-ne p1, v1, :cond_1

    .line 103
    :cond_0
    if-nez p3, :cond_2

    .line 104
    invoke-virtual {p0, v3}, Lorg/namelessrom/devicecontrol/activities/RequestFileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 119
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/activities/RequestFileActivity;->finish()V

    .line 120
    return-void

    .line 110
    :cond_2
    :try_start_0
    invoke-direct {p0, p3, p1}, Lorg/namelessrom/devicecontrol/activities/RequestFileActivity;->handleActivityResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 111
    :catch_0
    move-exception v0

    .line 112
    .local v0, "se":Ljava/lang/SecurityException;
    const-string v1, "could not handle activity result"

    invoke-static {p0, v1, v0}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 113
    invoke-virtual {p0, v3}, Lorg/namelessrom/devicecontrol/activities/RequestFileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 61
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 63
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/activities/RequestFileActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 64
    .local v3, "pm":Landroid/content/pm/PackageManager;
    if-nez v3, :cond_0

    .line 65
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/activities/RequestFileActivity;->launchInternalPicker()V

    .line 85
    :goto_0
    return-void

    .line 69
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.GET_CONTENT"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 70
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "application/zip"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    const/4 v4, 0x1

    :try_start_0
    invoke-virtual {v3, v1, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 74
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    .end local v1    # "intent":Landroid/content/Intent;
    .local v2, "intent":Landroid/content/Intent;
    :try_start_1
    const-string v4, "application/zip"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 76
    const-string v4, "android.intent.action.GET_CONTENT"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 77
    const/16 v4, 0xcb

    invoke-virtual {p0, v2, v4}, Lorg/namelessrom/devicecontrol/activities/RequestFileActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v1, v2

    .line 84
    .end local v2    # "intent":Landroid/content/Intent;
    .restart local v1    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 79
    :cond_1
    :try_start_2
    new-instance v4, Landroid/content/ActivityNotFoundException;

    invoke-direct {v4}, Landroid/content/ActivityNotFoundException;-><init>()V

    throw v4
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    .line 81
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    :goto_1
    const-string v4, "No activity found to handle file picking! Falling back to default!"

    invoke-static {p0, v4, v0}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 83
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/activities/RequestFileActivity;->launchInternalPicker()V

    goto :goto_0

    .line 81
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "intent":Landroid/content/Intent;
    .restart local v2    # "intent":Landroid/content/Intent;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "intent":Landroid/content/Intent;
    .restart local v1    # "intent":Landroid/content/Intent;
    goto :goto_1
.end method
