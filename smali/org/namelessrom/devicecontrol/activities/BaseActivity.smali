.class public abstract Lorg/namelessrom/devicecontrol/activities/BaseActivity;
.super Landroid/support/v7/app/ActionBarActivity;
.source "BaseActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 31
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/Application;->isDarkTheme()Z

    move-result v1

    .line 34
    .local v1, "isDarkTheme":Z
    if-eqz v1, :cond_1

    const v2, 0x7f0f0001

    :goto_0
    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->setTheme(I)V

    .line 35
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_0

    .line 38
    :try_start_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v3

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/Application;->getPrimaryColor()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/Window;->setStatusBarColor(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    :cond_0
    :goto_1
    return-void

    .line 34
    :cond_1
    const v2, 0x7f0f0002

    goto :goto_0

    .line 39
    :catch_0
    move-exception v0

    .line 40
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "BaseActivity"

    const-string v3, "get a stone and throw it at your device vendor"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
