.class public Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;
.super Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;
.source "AppListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;
    }
.end annotation


# instance fields
.field private mIsLoading:Z

.field private mProgressContainer:Landroid/widget/LinearLayout;

.field private mRecyclerView:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;-><init>()V

    .line 131
    return-void
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->mProgressContainer:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$200(Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;)Landroid/support/v7/widget/RecyclerView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method static synthetic access$302(Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->mIsLoading:Z

    return p1
.end method

.method static synthetic access$400(Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;)V
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;

    .prologue
    .line 47
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->invalidateOptionsMenu()V

    return-void
.end method

.method private invalidateOptionsMenu()V
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->invalidateOptionsMenu()V

    .line 102
    :cond_0
    return-void
.end method

.method private loadApps(Z)V
    .locals 4
    .param p1, "animate"    # Z

    .prologue
    const/4 v3, 0x0

    .line 105
    iget-boolean v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->mIsLoading:Z

    if-eqz v1, :cond_0

    .line 129
    :goto_0
    return-void

    .line 107
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->mIsLoading:Z

    .line 108
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->mProgressContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 110
    if-eqz p1, :cond_1

    .line 111
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->mProgressContainer:Landroid/widget/LinearLayout;

    const-string v2, "alpha"

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 112
    .local v0, "anim":Landroid/animation/ObjectAnimator;
    new-instance v1, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$1;

    invoke-direct {v1, p0}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$1;-><init>(Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 123
    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 124
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    .line 126
    .end local v0    # "anim":Landroid/animation/ObjectAnimator;
    :cond_1
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->mProgressContainer:Landroid/widget/LinearLayout;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 127
    new-instance v1, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;-><init>(Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$1;)V

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 111
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method protected getFragmentId()I
    .locals 1

    .prologue
    .line 55
    const v0, 0x7f0e0615

    return v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 58
    invoke-super {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 59
    const v0, 0x7f100003

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 60
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 77
    const v1, 0x7f04002b

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 78
    .local v0, "rootView":Landroid/view/View;
    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 79
    const v1, 0x7f0c0096

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->mProgressContainer:Landroid/widget/LinearLayout;

    .line 80
    return-object v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 64
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 67
    .local v0, "id":I
    const v2, 0x7f0c0111

    if-ne v0, v2, :cond_0

    .line 68
    invoke-direct {p0, v1}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->loadApps(Z)V

    .line 72
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 94
    invoke-super {p0}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;->onResume()V

    .line 95
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->loadApps(Z)V

    .line 96
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    .line 84
    invoke-super {p0, p1, p2}, Lorg/namelessrom/devicecontrol/ui/views/AttachFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 85
    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->setHasOptionsMenu(Z)V

    .line 87
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 89
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 90
    .local v0, "linearLayoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 91
    return-void
.end method
