.class Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$4;
.super Ljava/lang/Object;
.source "AppDetailsActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->updateAppEnabled()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;)V
    .locals 0

    .prologue
    .line 489
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$4;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 491
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$4;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->invalidateOptionsMenu()V

    .line 493
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$4;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    # getter for: Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->access$200(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;)Lorg/namelessrom/devicecontrol/objects/AppItem;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/objects/AppItem;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    .line 494
    .local v0, "isEnabled":Z
    :goto_0
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$4;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    # getter for: Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->access$200(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;)Lorg/namelessrom/devicecontrol/objects/AppItem;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/objects/AppItem;->setEnabled(Z)V

    .line 496
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$4;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    # getter for: Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppContainer:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->access$300(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 497
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$4;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    # getter for: Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppContainer:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->access$300(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;)Landroid/widget/LinearLayout;

    move-result-object v2

    if-eqz v0, :cond_2

    const v1, 0x106000d

    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 500
    :cond_0
    return-void

    .line 493
    .end local v0    # "isEnabled":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 497
    .restart local v0    # "isEnabled":Z
    :cond_2
    const v1, 0x7f0a002a

    goto :goto_1
.end method
