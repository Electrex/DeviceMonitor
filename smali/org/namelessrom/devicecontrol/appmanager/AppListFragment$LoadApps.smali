.class Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;
.super Landroid/os/AsyncTask;
.source "AppListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadApps"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lorg/namelessrom/devicecontrol/objects/AppItem;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;


# direct methods
.method private constructor <init>(Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;
    .param p2, "x1"    # Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$1;

    .prologue
    .line 131
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;-><init>(Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 131
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 8
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/objects/AppItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;

    invoke-virtual {v5}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 134
    .local v4, "pm":Landroid/content/pm/PackageManager;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 135
    .local v0, "appList":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/objects/AppItem;>;"
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v3

    .line 137
    .local v3, "pkgInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PackageInfo;

    .line 138
    .local v2, "pkgInfo":Landroid/content/pm/PackageInfo;
    iget-object v5, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v5, :cond_0

    .line 141
    new-instance v5, Lorg/namelessrom/devicecontrol/objects/AppItem;

    iget-object v6, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v6, v4}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v7, v4}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-direct {v5, v2, v6, v7}, Lorg/namelessrom/devicecontrol/objects/AppItem;-><init>(Landroid/content/pm/PackageInfo;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 145
    .end local v2    # "pkgInfo":Landroid/content/pm/PackageInfo;
    :cond_1
    sget-object v5, Lorg/namelessrom/devicecontrol/utils/SortHelper;->sAppComparator:Ljava/util/Comparator;

    invoke-static {v0, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 147
    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 131
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/objects/AppItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 151
    .local p1, "appItems":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/objects/AppItem;>;"
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;

    # getter for: Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->mProgressContainer:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->access$100(Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    const-string v2, "alpha"

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 152
    .local v0, "anim":Landroid/animation/ObjectAnimator;
    new-instance v1, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps$1;

    invoke-direct {v1, p0, p1}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps$1;-><init>(Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 169
    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 170
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 172
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;

    # invokes: Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->invalidateOptionsMenu()V
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->access$400(Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;)V

    .line 173
    return-void

    .line 151
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method
