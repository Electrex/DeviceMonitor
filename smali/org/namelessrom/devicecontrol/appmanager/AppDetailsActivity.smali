.class public Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;
.super Lorg/namelessrom/devicecontrol/activities/BaseActivity;
.source "AppDetailsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lorg/namelessrom/devicecontrol/objects/PackageStatsObserver$OnPackageStatsListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$AppPagerAdapter;
    }
.end annotation


# static fields
.field private static final mHandler:Landroid/os/Handler;


# instance fields
.field private mAppContainer:Landroid/widget/LinearLayout;

.field private mAppDetailsContainer:Landroid/view/View;

.field private mAppDetailsError:Landroid/view/View;

.field private mAppIcon:Landroid/widget/ImageView;

.field private mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

.field private mAppLabel:Landroid/widget/TextView;

.field private mAppPackage:Landroid/widget/TextView;

.field private mAppVersion:Landroid/widget/TextView;

.field private mCacheGraph:Lcom/github/mikephil/charting/charts/PieChart;

.field private mCacheInfo:Landroid/widget/LinearLayout;

.field private final mClearRunnable:Ljava/lang/Runnable;

.field private mForceStop:Landroid/widget/Button;

.field private final mPm:Landroid/content/pm/PackageManager;

.field private mUninstall:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;-><init>()V

    .line 82
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mPm:Landroid/content/pm/PackageManager;

    .line 592
    new-instance v0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$5;

    invoke-direct {v0, p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$5;-><init>(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;)V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mClearRunnable:Ljava/lang/Runnable;

    .line 661
    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;)V
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    .prologue
    .line 74
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->refreshAppControls()V

    return-void
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;)V
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    .prologue
    .line 74
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->updateAppEnabled()V

    return-void
.end method

.method static synthetic access$200(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;)Lorg/namelessrom/devicecontrol/objects/AppItem;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    return-object v0
.end method

.method static synthetic access$202(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;Lorg/namelessrom/devicecontrol/objects/AppItem;)Lorg/namelessrom/devicecontrol/objects/AppItem;
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;
    .param p1, "x1"    # Lorg/namelessrom/devicecontrol/objects/AppItem;

    .prologue
    .line 74
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    return-object p1
.end method

.method static synthetic access$300(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppContainer:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$400(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;)Landroid/content/pm/PackageManager;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mPm:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method static synthetic access$500(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;)V
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    .prologue
    .line 74
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->disable()V

    return-void
.end method

.method static synthetic access$600(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;)V
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    .prologue
    .line 74
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->uninstall()V

    return-void
.end method

.method private addCacheWidget(ILjava/lang/String;)Landroid/view/View;
    .locals 7
    .param p1, "txtId"    # I
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 581
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f040055

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheInfo:Landroid/widget/LinearLayout;

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 583
    .local v2, "v":Landroid/view/View;
    const v3, 0x7f0c0104

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 584
    .local v0, "tvLeft":Landroid/widget/TextView;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 586
    const v3, 0x7f0c0105

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 587
    .local v1, "tvRight":Landroid/widget/TextView;
    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 589
    return-object v2
.end method

.method private clearAppCache()V
    .locals 4

    .prologue
    .line 401
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mPm:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->clearCache(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 402
    sget-object v0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mClearRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 403
    return-void
.end method

.method private clearAppData()V
    .locals 4

    .prologue
    .line 396
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mPm:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->clearData(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 397
    sget-object v0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mClearRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 398
    return-void
.end method

.method private disable()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 406
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    if-nez v2, :cond_0

    .line 428
    :goto_0
    return-void

    .line 409
    :cond_0
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/objects/AppItem;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 410
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pm disable "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " 2> /dev/null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 415
    .local v0, "cmd":Ljava/lang/String;
    :goto_1
    new-instance v1, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$2;

    new-array v2, v5, [Ljava/lang/String;

    aput-object v0, v2, v4

    invoke-direct {v1, p0, v4, v2}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$2;-><init>(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;I[Ljava/lang/String;)V

    .line 426
    .local v1, "commandCapture":Lcom/stericson/roottools/execution/CommandCapture;
    const/4 v2, 0x1

    :try_start_0
    invoke-static {v2}, Lcom/stericson/roottools/RootTools;->getShell(Z)Lcom/stericson/roottools/execution/Shell;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/stericson/roottools/execution/Shell;->add(Lcom/stericson/roottools/execution/Command;)Lcom/stericson/roottools/execution/Command;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 427
    :catch_0
    move-exception v2

    goto :goto_0

    .line 412
    .end local v0    # "cmd":Ljava/lang/String;
    .end local v1    # "commandCapture":Lcom/stericson/roottools/execution/CommandCapture;
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pm enable "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " 2> /dev/null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "cmd":Ljava/lang/String;
    goto :goto_1
.end method

.method private disableApp()V
    .locals 1

    .prologue
    .line 388
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->showConfirmationDialog(I)V

    .line 389
    return-void
.end method

.method private forceStopApp()V
    .locals 4

    .prologue
    .line 379
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->killProcess(Ljava/lang/String;)V

    .line 380
    sget-object v0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$1;

    invoke-direct {v1, p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$1;-><init>(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 385
    return-void
.end method

.method private getTargetPackageName(Landroid/content/Intent;)Ljava/lang/String;
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 177
    const/4 v1, 0x0

    .line 178
    .local v1, "packageName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 179
    .local v0, "args":Landroid/os/Bundle;
    if-eqz p1, :cond_0

    .line 180
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 181
    if-eqz v0, :cond_2

    const-string v2, "arg_package_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 184
    :cond_0
    :goto_0
    if-nez v1, :cond_1

    .line 185
    if-nez v0, :cond_3

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    .line 186
    :goto_1
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 187
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    .line 190
    :cond_1
    const-string v2, "packageName: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p0, v2, v3}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 191
    return-object v1

    .line 181
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 185
    :cond_3
    const-string v2, "intent"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    move-object p1, v2

    goto :goto_1
.end method

.method private refreshAppControls()V
    .locals 2

    .prologue
    .line 375
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mForceStop:Landroid/widget/Button;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->isAppRunning(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 376
    return-void
.end method

.method private refreshAppDetails()V
    .locals 7

    .prologue
    const/16 v5, 0x8

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 286
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    if-nez v2, :cond_0

    .line 287
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppDetailsContainer:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 288
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppDetailsError:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 289
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->invalidateOptionsMenu()V

    .line 319
    :goto_0
    return-void

    .line 292
    :cond_0
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppDetailsContainer:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 293
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppDetailsError:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 295
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppIcon:Landroid/widget/ImageView;

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v5}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 296
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppLabel:Landroid/widget/TextView;

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v5}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getLabel()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 297
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppPackage:Landroid/widget/TextView;

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v5}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 298
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppContainer:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/objects/AppItem;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x106000d

    :goto_1
    invoke-virtual {v5, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 301
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/objects/AppItem;->isSystemApp()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0a0063

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 304
    .local v0, "color":I
    :goto_2
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppLabel:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 306
    const-string v2, "%s (%s)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v6}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget-object v6, v6, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    aput-object v6, v5, v4

    iget-object v6, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v6}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget v6, v6, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 308
    .local v1, "version":Ljava/lang/String;
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppVersion:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 310
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->setupPermissionsView()V

    .line 313
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mUninstall:Landroid/widget/Button;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/Application;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v6}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    move v2, v3

    :goto_3
    invoke-virtual {v5, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 316
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mPm:Landroid/content/pm/PackageManager;

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, p0, v3}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->getSize(Landroid/content/pm/PackageManager;Lorg/namelessrom/devicecontrol/objects/PackageStatsObserver$OnPackageStatsListener;Ljava/lang/String;)V

    .line 317
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->refreshAppControls()V

    .line 318
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->invalidateOptionsMenu()V

    goto/16 :goto_0

    .line 298
    .end local v0    # "color":I
    .end local v1    # "version":Ljava/lang/String;
    :cond_1
    const v2, 0x7f0a002a

    goto :goto_1

    .line 301
    :cond_2
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v2

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/Application;->isDarkTheme()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v0, -0x1

    goto :goto_2

    :cond_3
    const/high16 v0, -0x1000000

    goto :goto_2

    .restart local v0    # "color":I
    .restart local v1    # "version":Ljava/lang/String;
    :cond_4
    move v2, v4

    .line 313
    goto :goto_3
.end method

.method private setupCacheGraph()V
    .locals 6

    .prologue
    const/16 v5, 0x3e8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 266
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheGraph:Lcom/github/mikephil/charting/charts/PieChart;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/github/mikephil/charting/charts/PieChart;->setDescription(Ljava/lang/String;)V

    .line 268
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheGraph:Lcom/github/mikephil/charting/charts/PieChart;

    const/high16 v2, 0x42700000    # 60.0f

    invoke-virtual {v1, v2}, Lcom/github/mikephil/charting/charts/PieChart;->setHoleRadius(F)V

    .line 269
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheGraph:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v1, v4}, Lcom/github/mikephil/charting/charts/PieChart;->setDrawHoleEnabled(Z)V

    .line 270
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheGraph:Lcom/github/mikephil/charting/charts/PieChart;

    const/high16 v2, 0x42820000    # 65.0f

    invoke-virtual {v1, v2}, Lcom/github/mikephil/charting/charts/PieChart;->setTransparentCircleRadius(F)V

    .line 272
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheGraph:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v1, v3}, Lcom/github/mikephil/charting/charts/PieChart;->setDrawSliceText(Z)V

    .line 273
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheGraph:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v1, v3}, Lcom/github/mikephil/charting/charts/PieChart;->setDrawMarkerViews(Z)V

    .line 274
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheGraph:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v1, v4}, Lcom/github/mikephil/charting/charts/PieChart;->setDrawCenterText(Z)V

    .line 276
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheGraph:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v1, v3}, Lcom/github/mikephil/charting/charts/PieChart;->setTouchEnabled(Z)V

    .line 278
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/Application;->isDarkTheme()Z

    move-result v1

    if-eqz v1, :cond_0

    const v0, 0x7f0a0026

    .line 280
    .local v0, "color":I
    :goto_0
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheGraph:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v1, v0}, Lcom/github/mikephil/charting/charts/PieChart;->setBackgroundResource(I)V

    .line 282
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheGraph:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v1, v5, v5}, Lcom/github/mikephil/charting/charts/PieChart;->animateXY(II)V

    .line 283
    return-void

    .line 278
    .end local v0    # "color":I
    :cond_0
    const v0, 0x7f0a003f

    goto :goto_0
.end method

.method private setupPermissionsView()V
    .locals 23

    .prologue
    .line 322
    const v18, 0x7f0c00b1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/LinearLayout;

    .line 323
    .local v12, "permissionsView":Landroid/widget/LinearLayout;
    sget v18, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v19, 0x10

    move/from16 v0, v18

    move/from16 v1, v19

    if-gt v0, v1, :cond_1

    const/16 v17, 0x1

    .line 324
    .local v17, "unsupported":Z
    :goto_0
    if-eqz v17, :cond_2

    .line 325
    const/16 v18, 0x8

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 372
    :cond_0
    :goto_1
    return-void

    .line 323
    .end local v17    # "unsupported":Z
    :cond_1
    const/16 v17, 0x0

    goto :goto_0

    .line 329
    .restart local v17    # "unsupported":Z
    :cond_2
    new-instance v6, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getPackageName()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v6, v0, v1}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 331
    .local v6, "asp":Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;
    const v18, 0x7f0c00b3

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/LinearLayout;

    .line 332
    .local v16, "securityList":Landroid/widget/LinearLayout;
    invoke-virtual/range {v16 .. v16}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 333
    invoke-virtual {v6}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->getPermissionsView()Landroid/view/View;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 336
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mPm:Landroid/content/pm/PackageManager;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v19

    move-object/from16 v0, v19

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v11

    .line 337
    .local v11, "packages":[Ljava/lang/String;
    if-eqz v11, :cond_0

    array-length v0, v11

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_0

    .line 338
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 339
    .local v14, "pnames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/CharSequence;>;"
    move-object v5, v11

    .local v5, "arr$":[Ljava/lang/String;
    array-length v10, v5

    .local v10, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_2
    if-ge v9, v10, :cond_4

    aget-object v13, v5, v9

    .line 340
    .local v13, "pkg":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getPackageName()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 339
    :goto_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 344
    :cond_3
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mPm:Landroid/content/pm/PackageManager;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v13, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 345
    .local v3, "ainfo":Landroid/content/pm/ApplicationInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mPm:Landroid/content/pm/PackageManager;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 346
    .end local v3    # "ainfo":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v18

    goto :goto_3

    .line 348
    .end local v13    # "pkg":Ljava/lang/String;
    :cond_4
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 349
    .local v2, "N":I
    if-lez v2, :cond_0

    .line 350
    invoke-virtual/range {p0 .. p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    .line 352
    .local v15, "res":Landroid/content/res/Resources;
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v2, v0, :cond_5

    .line 353
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/CharSequence;

    invoke-interface/range {v18 .. v18}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 366
    .local v4, "appListStr":Ljava/lang/String;
    :goto_4
    const v18, 0x7f0c00b2

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 367
    .local v7, "descr":Landroid/widget/TextView;
    const v18, 0x7f0e01c0

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mPm:Landroid/content/pm/PackageManager;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x1

    aput-object v4, v19, v20

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 354
    .end local v4    # "appListStr":Ljava/lang/String;
    .end local v7    # "descr":Landroid/widget/TextView;
    :cond_5
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ne v2, v0, :cond_6

    .line 355
    const v18, 0x7f0e014a

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x1

    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    aput-object v21, v19, v20

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "appListStr":Ljava/lang/String;
    goto :goto_4

    .line 358
    .end local v4    # "appListStr":Ljava/lang/String;
    :cond_6
    add-int/lit8 v18, v2, -0x2

    move/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/CharSequence;

    invoke-interface/range {v18 .. v18}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 359
    .restart local v4    # "appListStr":Ljava/lang/String;
    add-int/lit8 v8, v2, -0x3

    .local v8, "i":I
    :goto_5
    if-ltz v8, :cond_8

    .line 360
    if-nez v8, :cond_7

    const v18, 0x7f0e0147

    :goto_6
    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x1

    aput-object v4, v19, v20

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 359
    add-int/lit8 v8, v8, -0x1

    goto :goto_5

    .line 360
    :cond_7
    const v18, 0x7f0e0149

    goto :goto_6

    .line 363
    :cond_8
    const v18, 0x7f0e0148

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v4, v19, v20

    const/16 v20, 0x1

    add-int/lit8 v21, v2, -0x1

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    aput-object v21, v19, v20

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_4
.end method

.method private showConfirmationDialog(I)V
    .locals 10
    .param p1, "type"    # I

    .prologue
    const v9, 0x7f0e022a

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 601
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    if-nez v3, :cond_0

    .line 659
    :goto_0
    return-void

    .line 603
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 607
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 609
    :pswitch_0
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/objects/AppItem;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    const v3, 0x7f0e009d

    :goto_1
    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v5}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getLabel()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {p0, v3, v4}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 611
    .local v1, "message":Ljava/lang/String;
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/objects/AppItem;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    const v2, 0x7f0e009c

    .line 630
    .local v2, "positiveButton":I
    :goto_2
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$7;

    invoke-direct {v4, p0, p1}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$7;-><init>(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;I)V

    invoke-virtual {v3, v2, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/high16 v4, 0x1040000

    new-instance v5, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$6;

    invoke-direct {v5, p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$6;-><init>(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 658
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 609
    .end local v1    # "message":Ljava/lang/String;
    .end local v2    # "positiveButton":I
    :cond_1
    const v3, 0x7f0e00c1

    goto :goto_1

    .line 611
    .restart local v1    # "message":Ljava/lang/String;
    :cond_2
    const v2, 0x7f0e00b9

    goto :goto_2

    .line 615
    .end local v1    # "message":Ljava/lang/String;
    :pswitch_1
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/objects/AppItem;->isSystemApp()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 616
    const-string v3, "%s\n\n%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v6}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getLabel()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p0, v9, v5}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    const v5, 0x7f0e022b

    invoke-virtual {p0, v5}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 622
    .restart local v1    # "message":Ljava/lang/String;
    :goto_3
    const v2, 0x1040013

    .line 623
    .restart local v2    # "positiveButton":I
    goto :goto_2

    .line 620
    .end local v1    # "message":Ljava/lang/String;
    .end local v2    # "positiveButton":I
    :cond_3
    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v4}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getLabel()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {p0, v9, v3}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "message":Ljava/lang/String;
    goto :goto_3

    .line 607
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private uninstall()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 432
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mPm:Landroid/content/pm/PackageManager;

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v4}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->uninstallPackage(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 435
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 437
    .local v2, "sb":Ljava/lang/StringBuilder;
    const-string v3, "pm uninstall %s;"

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v5}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getPackageName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 439
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/objects/AppItem;->isSystemApp()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 440
    const-string v3, "busybox mount -o rw,remount /system;"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 443
    :cond_0
    const-string v3, "rm -rf %s;"

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v5}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 444
    const-string v3, "rm -rf %s;"

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v5}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    const-string v3, "rm -rf %s;"

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v5}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 447
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/objects/AppItem;->isSystemApp()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 448
    const-string v3, "busybox mount -o ro,remount /system;"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 451
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 452
    .local v0, "cmd":Ljava/lang/String;
    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 456
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 457
    .local v1, "dialog":Landroid/app/ProgressDialog;
    const v3, 0x7f0e022d

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 458
    const v3, 0x7f0e002e

    invoke-virtual {p0, v3}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 459
    invoke-virtual {v1, v6}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 460
    invoke-virtual {v1, v6}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 462
    new-instance v3, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$3;

    invoke-direct {v3, p0, v1, v0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$3;-><init>(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;Landroid/app/ProgressDialog;Ljava/lang/String;)V

    new-array v4, v6, [Ljava/lang/Void;

    invoke-virtual {v3, v4}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$3;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 483
    return-void
.end method

.method private uninstallApp()V
    .locals 1

    .prologue
    .line 392
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->showConfirmationDialog(I)V

    .line 393
    return-void
.end method

.method private updateAppEnabled()V
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    if-nez v0, :cond_0

    .line 502
    :goto_0
    return-void

    .line 489
    :cond_0
    new-instance v0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$4;

    invoke-direct {v0, p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$4;-><init>(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;)V

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 244
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 245
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    .line 263
    :goto_0
    :pswitch_0
    return-void

    .line 247
    :pswitch_1
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->forceStopApp()V

    goto :goto_0

    .line 251
    :pswitch_2
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->uninstallApp()V

    goto :goto_0

    .line 255
    :pswitch_3
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->clearAppCache()V

    goto :goto_0

    .line 259
    :pswitch_4
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->clearAppData()V

    goto :goto_0

    .line 245
    :pswitch_data_0
    .packed-switch 0x7f0c00aa
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x1

    const/high16 v10, 0x40000000    # 2.0f

    .line 107
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 108
    const v8, 0x7f040018

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->setContentView(I)V

    .line 111
    const v8, 0x7f0c0061

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/Toolbar;

    .line 112
    .local v6, "toolbar":Landroid/support/v7/widget/Toolbar;
    invoke-virtual {p0, v6}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 114
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 115
    .local v3, "i":Landroid/content/Intent;
    if-eqz v3, :cond_0

    const-string v8, "arg_from_activity"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 116
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v8

    invoke-virtual {v8, v9}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 119
    :cond_0
    new-array v5, v9, [Ljava/lang/String;

    const/4 v8, 0x0

    const v9, 0x7f0e0024

    invoke-virtual {p0, v9}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v8

    .line 120
    .local v5, "titles":[Ljava/lang/String;
    new-instance v0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$AppPagerAdapter;

    invoke-direct {v0, p0, v5}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$AppPagerAdapter;-><init>(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;[Ljava/lang/String;)V

    .line 121
    .local v0, "adapter":Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$AppPagerAdapter;
    const v8, 0x7f0c0063

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/support/v4/view/ViewPager;

    .line 122
    .local v7, "viewPager":Landroid/support/v4/view/ViewPager;
    invoke-virtual {v7, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 124
    const v8, 0x7f0c0062

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/astuetz/PagerSlidingTabStrip;

    .line 126
    .local v4, "tabHost":Lcom/astuetz/PagerSlidingTabStrip;
    const/16 v8, 0x8

    invoke-virtual {v4, v8}, Lcom/astuetz/PagerSlidingTabStrip;->setVisibility(I)V

    .line 129
    const v8, 0x7f0c00a3

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppDetailsContainer:Landroid/view/View;

    .line 130
    const v8, 0x7f0c00a2

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppDetailsError:Landroid/view/View;

    .line 132
    const v8, 0x7f0c00a4

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppContainer:Landroid/widget/LinearLayout;

    .line 133
    const v8, 0x7f0c00a5

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppIcon:Landroid/widget/ImageView;

    .line 134
    const v8, 0x7f0c00a6

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppLabel:Landroid/widget/TextView;

    .line 135
    const v8, 0x7f0c00a7

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppPackage:Landroid/widget/TextView;

    .line 136
    const v8, 0x7f0c00a8

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppVersion:Landroid/widget/TextView;

    .line 137
    const v8, 0x7f0c00ac

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/github/mikephil/charting/charts/PieChart;

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheGraph:Lcom/github/mikephil/charting/charts/PieChart;

    .line 138
    const v8, 0x7f0c00ad

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheInfo:Landroid/widget/LinearLayout;

    .line 140
    const v8, 0x7f0c00aa

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mForceStop:Landroid/widget/Button;

    .line 141
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mForceStop:Landroid/widget/Button;

    invoke-static {v8, v10}, Landroid/support/v4/view/ViewCompat;->setElevation(Landroid/view/View;F)V

    .line 142
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mForceStop:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    const v8, 0x7f0c00ab

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mUninstall:Landroid/widget/Button;

    .line 145
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mUninstall:Landroid/widget/Button;

    invoke-static {v8, v10}, Landroid/support/v4/view/ViewCompat;->setElevation(Landroid/view/View;F)V

    .line 146
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mUninstall:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    const v8, 0x7f0c00af

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 149
    .local v1, "clearCache":Landroid/widget/Button;
    invoke-static {v1, v10}, Landroid/support/v4/view/ViewCompat;->setElevation(Landroid/view/View;F)V

    .line 150
    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    const v8, 0x7f0c00b0

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 153
    .local v2, "clearData":Landroid/widget/Button;
    invoke-static {v2, v10}, Landroid/support/v4/view/ViewCompat;->setElevation(Landroid/view/View;F)V

    .line 154
    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v4, 0x7f0c0112

    const v3, 0x7f0c0113

    .line 195
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 196
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const/high16 v2, 0x7f100000

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 198
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    if-eqz v1, :cond_4

    .line 199
    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->isPlayStoreInstalled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 200
    invoke-interface {p1, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 203
    :cond_0
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/Application;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 204
    invoke-interface {p1, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 207
    :cond_1
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 208
    .local v0, "disable":Landroid/view/MenuItem;
    if-eqz v0, :cond_2

    .line 209
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/objects/AppItem;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    const v1, 0x7f0e009c

    :goto_0
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 216
    .end local v0    # "disable":Landroid/view/MenuItem;
    :cond_2
    :goto_1
    const/4 v1, 0x1

    return v1

    .line 209
    .restart local v0    # "disable":Landroid/view/MenuItem;
    :cond_3
    const v1, 0x7f0e00b9

    goto :goto_0

    .line 212
    .end local v0    # "disable":Landroid/view/MenuItem;
    :cond_4
    invoke-interface {p1, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 213
    invoke-interface {p1, v3}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 102
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 103
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->setIntent(Landroid/content/Intent;)V

    .line 104
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 220
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 222
    .local v0, "id":I
    sparse-switch v0, :sswitch_data_0

    .line 240
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    .line 224
    :sswitch_0
    invoke-static {p0}, Landroid/support/v4/app/NavUtils;->navigateUpFromSameTask(Landroid/app/Activity;)V

    goto :goto_0

    .line 228
    :sswitch_1
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->refreshAppDetails()V

    goto :goto_0

    .line 232
    :sswitch_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "market://details?id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->showInPlaystore(Ljava/lang/String;)Z

    goto :goto_0

    .line 236
    :sswitch_3
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->disableApp()V

    goto :goto_0

    .line 222
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c0111 -> :sswitch_1
        0x7f0c0112 -> :sswitch_2
        0x7f0c0113 -> :sswitch_3
    .end sparse-switch
.end method

.method public onPackageStats(Landroid/content/pm/PackageStats;)V
    .locals 14
    .param p1, "packageStats"    # Landroid/content/pm/PackageStats;

    .prologue
    .line 505
    const-string v8, "onAppSizeEvent()"

    invoke-static {p0, v8}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;)V

    .line 506
    if-nez p1, :cond_1

    .line 578
    :cond_0
    :goto_0
    return-void

    .line 508
    :cond_1
    iget-wide v8, p1, Landroid/content/pm/PackageStats;->codeSize:J

    iget-wide v10, p1, Landroid/content/pm/PackageStats;->dataSize:J

    add-long/2addr v8, v10

    iget-wide v10, p1, Landroid/content/pm/PackageStats;->externalCodeSize:J

    add-long/2addr v8, v10

    iget-wide v10, p1, Landroid/content/pm/PackageStats;->externalDataSize:J

    add-long/2addr v8, v10

    iget-wide v10, p1, Landroid/content/pm/PackageStats;->externalMediaSize:J

    add-long/2addr v8, v10

    iget-wide v10, p1, Landroid/content/pm/PackageStats;->externalObbSize:J

    add-long/2addr v8, v10

    iget-wide v10, p1, Landroid/content/pm/PackageStats;->cacheSize:J

    add-long/2addr v8, v10

    iget-wide v10, p1, Landroid/content/pm/PackageStats;->externalCacheSize:J

    add-long v6, v8, v10

    .line 513
    .local v6, "totalSize":J
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheInfo:Landroid/widget/LinearLayout;

    if-eqz v8, :cond_2

    .line 514
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 516
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheInfo:Landroid/widget/LinearLayout;

    const v9, 0x7f0e021e

    invoke-static {v6, v7}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->convertSize(J)Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v9, v10}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->addCacheWidget(ILjava/lang/String;)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 518
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheInfo:Landroid/widget/LinearLayout;

    const v9, 0x7f0e0023

    iget-wide v10, p1, Landroid/content/pm/PackageStats;->codeSize:J

    invoke-static {v10, v11}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->convertSize(J)Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v9, v10}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->addCacheWidget(ILjava/lang/String;)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 520
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheInfo:Landroid/widget/LinearLayout;

    const v9, 0x7f0e00cc

    iget-wide v10, p1, Landroid/content/pm/PackageStats;->externalCodeSize:J

    invoke-static {v10, v11}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->convertSize(J)Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v9, v10}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->addCacheWidget(ILjava/lang/String;)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 522
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheInfo:Landroid/widget/LinearLayout;

    const v9, 0x7f0e0084

    iget-wide v10, p1, Landroid/content/pm/PackageStats;->dataSize:J

    invoke-static {v10, v11}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->convertSize(J)Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v9, v10}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->addCacheWidget(ILjava/lang/String;)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 524
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheInfo:Landroid/widget/LinearLayout;

    const v9, 0x7f0e00ce

    iget-wide v10, p1, Landroid/content/pm/PackageStats;->externalDataSize:J

    invoke-static {v10, v11}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->convertSize(J)Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v9, v10}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->addCacheWidget(ILjava/lang/String;)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 526
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheInfo:Landroid/widget/LinearLayout;

    const v9, 0x7f0e00cf

    iget-wide v10, p1, Landroid/content/pm/PackageStats;->externalMediaSize:J

    invoke-static {v10, v11}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->convertSize(J)Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v9, v10}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->addCacheWidget(ILjava/lang/String;)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 528
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheInfo:Landroid/widget/LinearLayout;

    const v9, 0x7f0e00d0

    iget-wide v10, p1, Landroid/content/pm/PackageStats;->externalObbSize:J

    invoke-static {v10, v11}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->convertSize(J)Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v9, v10}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->addCacheWidget(ILjava/lang/String;)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 530
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheInfo:Landroid/widget/LinearLayout;

    const v9, 0x7f0e0040

    iget-wide v10, p1, Landroid/content/pm/PackageStats;->cacheSize:J

    invoke-static {v10, v11}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->convertSize(J)Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v9, v10}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->addCacheWidget(ILjava/lang/String;)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 532
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheInfo:Landroid/widget/LinearLayout;

    const v9, 0x7f0e00cd

    iget-wide v10, p1, Landroid/content/pm/PackageStats;->externalCacheSize:J

    invoke-static {v10, v11}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->convertSize(J)Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v9, v10}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->addCacheWidget(ILjava/lang/String;)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 536
    :cond_2
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheGraph:Lcom/github/mikephil/charting/charts/PieChart;

    if-eqz v8, :cond_0

    .line 537
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 538
    .local v4, "sliceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/github/mikephil/charting/data/Entry;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 540
    .local v5, "textList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const v8, 0x7f0e0023

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 541
    new-instance v8, Lcom/github/mikephil/charting/data/Entry;

    iget-wide v10, p1, Landroid/content/pm/PackageStats;->codeSize:J

    iget-wide v12, p1, Landroid/content/pm/PackageStats;->externalCodeSize:J

    add-long/2addr v10, v12

    long-to-float v9, v10

    const/4 v10, 0x0

    invoke-direct {v8, v9, v10}, Lcom/github/mikephil/charting/data/Entry;-><init>(FI)V

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 543
    const v8, 0x7f0e0084

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 544
    new-instance v8, Lcom/github/mikephil/charting/data/Entry;

    iget-wide v10, p1, Landroid/content/pm/PackageStats;->dataSize:J

    iget-wide v12, p1, Landroid/content/pm/PackageStats;->externalDataSize:J

    add-long/2addr v10, v12

    long-to-float v9, v10

    const/4 v10, 0x1

    invoke-direct {v8, v9, v10}, Lcom/github/mikephil/charting/data/Entry;-><init>(FI)V

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 546
    const v8, 0x7f0e00cb

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 547
    new-instance v8, Lcom/github/mikephil/charting/data/Entry;

    iget-wide v10, p1, Landroid/content/pm/PackageStats;->externalMediaSize:J

    iget-wide v12, p1, Landroid/content/pm/PackageStats;->externalObbSize:J

    add-long/2addr v10, v12

    long-to-float v9, v10

    const/4 v10, 0x2

    invoke-direct {v8, v9, v10}, Lcom/github/mikephil/charting/data/Entry;-><init>(FI)V

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 550
    const v8, 0x7f0e0040

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 551
    new-instance v8, Lcom/github/mikephil/charting/data/Entry;

    iget-wide v10, p1, Landroid/content/pm/PackageStats;->cacheSize:J

    iget-wide v12, p1, Landroid/content/pm/PackageStats;->externalCacheSize:J

    add-long/2addr v10, v12

    long-to-float v9, v10

    const/4 v10, 0x3

    invoke-direct {v8, v9, v10}, Lcom/github/mikephil/charting/data/Entry;-><init>(FI)V

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 553
    new-instance v2, Lcom/github/mikephil/charting/data/PieDataSet;

    const v8, 0x7f0e0027

    invoke-virtual {p0, v8}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v2, v4, v8}, Lcom/github/mikephil/charting/data/PieDataSet;-><init>(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 554
    .local v2, "dataSet":Lcom/github/mikephil/charting/data/PieDataSet;
    const/high16 v8, 0x40a00000    # 5.0f

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/PieDataSet;->setSliceSpace(F)V

    .line 555
    sget-object v8, Lcom/github/mikephil/charting/utils/ColorTemplate;->VORDIPLOM_COLORS:[I

    invoke-static {v8}, Lcom/github/mikephil/charting/utils/ColorTemplate;->createColors([I)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/PieDataSet;->setColors(Ljava/util/ArrayList;)V

    .line 557
    new-instance v1, Lcom/github/mikephil/charting/data/PieData;

    invoke-direct {v1, v5, v2}, Lcom/github/mikephil/charting/data/PieData;-><init>(Ljava/util/ArrayList;Lcom/github/mikephil/charting/data/PieDataSet;)V

    .line 558
    .local v1, "data":Lcom/github/mikephil/charting/data/PieData;
    invoke-virtual {v1}, Lcom/github/mikephil/charting/data/PieData;->getDataSet()Lcom/github/mikephil/charting/data/PieDataSet;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/github/mikephil/charting/data/PieDataSet;->setDrawValues(Z)V

    .line 559
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheGraph:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v8, v1}, Lcom/github/mikephil/charting/charts/PieChart;->setData(Lcom/github/mikephil/charting/data/ChartData;)V

    .line 561
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheGraph:Lcom/github/mikephil/charting/charts/PieChart;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/github/mikephil/charting/charts/PieChart;->highlightValues([Lcom/github/mikephil/charting/utils/Highlight;)V

    .line 563
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheGraph:Lcom/github/mikephil/charting/charts/PieChart;

    const-string v9, "%s\n%s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const v12, 0x7f0e021e

    invoke-virtual {p0, v12}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v6, v7}, Lorg/namelessrom/devicecontrol/utils/AppHelper;->convertSize(J)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/github/mikephil/charting/charts/PieChart;->setCenterText(Ljava/lang/String;)V

    .line 567
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheGraph:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/PieChart;->getLegend()Lcom/github/mikephil/charting/components/Legend;

    move-result-object v3

    .line 568
    .local v3, "l":Lcom/github/mikephil/charting/components/Legend;
    sget-object v8, Lcom/github/mikephil/charting/components/Legend$LegendPosition;->BELOW_CHART_CENTER:Lcom/github/mikephil/charting/components/Legend$LegendPosition;

    invoke-virtual {v3, v8}, Lcom/github/mikephil/charting/components/Legend;->setPosition(Lcom/github/mikephil/charting/components/Legend$LegendPosition;)V

    .line 569
    const/high16 v8, 0x40e00000    # 7.0f

    invoke-virtual {v3, v8}, Lcom/github/mikephil/charting/components/Legend;->setXEntrySpace(F)V

    .line 570
    const/high16 v8, 0x40a00000    # 5.0f

    invoke-virtual {v3, v8}, Lcom/github/mikephil/charting/components/Legend;->setYEntrySpace(F)V

    .line 572
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v8

    invoke-virtual {v8}, Lorg/namelessrom/devicecontrol/Application;->isDarkTheme()Z

    move-result v8

    if-eqz v8, :cond_3

    const/4 v0, -0x1

    .line 573
    .local v0, "color":I
    :goto_1
    invoke-virtual {v3, v0}, Lcom/github/mikephil/charting/components/Legend;->setTextColor(I)V

    .line 576
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mCacheGraph:Lcom/github/mikephil/charting/charts/PieChart;

    const/16 v9, 0x3e8

    const/16 v10, 0x3e8

    invoke-virtual {v8, v9, v10}, Lcom/github/mikephil/charting/charts/PieChart;->animateXY(II)V

    goto/16 :goto_0

    .line 572
    .end local v0    # "color":I
    :cond_3
    const/high16 v0, -0x1000000

    goto :goto_1
.end method

.method protected onResume()V
    .locals 6

    .prologue
    .line 158
    invoke-super {p0}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onResume()V

    .line 159
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getTargetPackageName(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 160
    .local v1, "packageName":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 161
    const/4 v0, 0x0

    .line 163
    .local v0, "info":Landroid/content/pm/PackageInfo;
    :try_start_0
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mPm:Landroid/content/pm/PackageManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 165
    :goto_0
    if-eqz v0, :cond_0

    iget-object v2, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v2, :cond_0

    .line 166
    new-instance v2, Lorg/namelessrom/devicecontrol/objects/AppItem;

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mPm:Landroid/content/pm/PackageManager;

    invoke-virtual {v3, v4}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mPm:Landroid/content/pm/PackageManager;

    invoke-virtual {v4, v5}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-direct {v2, v0, v3, v4}, Lorg/namelessrom/devicecontrol/objects/AppItem;-><init>(Landroid/content/pm/PackageInfo;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    iput-object v2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;

    .line 172
    .end local v0    # "info":Landroid/content/pm/PackageInfo;
    :cond_0
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->setupCacheGraph()V

    .line 173
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->refreshAppDetails()V

    .line 174
    return-void

    .line 164
    .restart local v0    # "info":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v2

    goto :goto_0
.end method
