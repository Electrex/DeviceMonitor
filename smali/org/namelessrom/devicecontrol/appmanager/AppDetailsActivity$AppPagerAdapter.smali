.class Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$AppPagerAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "AppDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AppPagerAdapter"
.end annotation


# instance fields
.field private final mTitles:[Ljava/lang/String;

.field final synthetic this$0:Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;


# direct methods
.method public constructor <init>(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;[Ljava/lang/String;)V
    .locals 0
    .param p2, "titles"    # [Ljava/lang/String;

    .prologue
    .line 664
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$AppPagerAdapter;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 665
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$AppPagerAdapter;->mTitles:[Ljava/lang/String;

    .line 666
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 691
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$AppPagerAdapter;->mTitles:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public bridge synthetic getPageTitle(I)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 661
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$AppPagerAdapter;->getPageTitle(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPageTitle(I)Ljava/lang/String;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 670
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$AppPagerAdapter;->mTitles:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 675
    .line 678
    const v0, 0x7f0c0064

    .line 687
    .local v0, "resId":I
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$AppPagerAdapter;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 695
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
