.class Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$3;
.super Landroid/os/AsyncTask;
.source "AppDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->uninstall()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

.field final synthetic val$cmd:Ljava/lang/String;

.field final synthetic val$dialog:Landroid/app/ProgressDialog;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;Landroid/app/ProgressDialog;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 462
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$3;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$3;->val$dialog:Landroid/app/ProgressDialog;

    iput-object p3, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$3;->val$cmd:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 462
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$3;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 469
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$3;->val$cmd:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->runRootCommand(Ljava/lang/String;Z)V

    .line 470
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 462
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$3;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 6
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    const/4 v5, 0x0

    .line 474
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$3;->val$dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 475
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$3;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$3;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    const v2, 0x7f0e022c

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$3;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    # getter for: Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;
    invoke-static {v4}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->access$200(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;)Lorg/namelessrom/devicecontrol/objects/AppItem;

    move-result-object v4

    invoke-virtual {v4}, Lorg/namelessrom/devicecontrol/objects/AppItem;->getLabel()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 478
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$3;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    const/4 v1, 0x0

    # setter for: Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->mAppItem:Lorg/namelessrom/devicecontrol/objects/AppItem;
    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->access$202(Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;Lorg/namelessrom/devicecontrol/objects/AppItem;)Lorg/namelessrom/devicecontrol/objects/AppItem;

    .line 479
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$3;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity;->finish()V

    .line 481
    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/AppDetailsActivity$3;->val$dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 466
    return-void
.end method
