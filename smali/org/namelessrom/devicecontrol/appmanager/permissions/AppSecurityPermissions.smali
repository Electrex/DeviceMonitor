.class public Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;
.super Ljava/lang/Object;
.source "AppSecurityPermissions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$PermissionInfoComparator;,
        Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$PermissionGroupInfoComparator;,
        Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;,
        Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mPermComparator:Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$PermissionInfoComparator;

.field private final mPermGroupComparator:Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$PermissionGroupInfoComparator;

.field private final mPermGroups:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mPermGroupsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mPm:Landroid/content/pm/PackageManager;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPermGroups:Ljava/util/Map;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPermGroupsList:Ljava/util/List;

    .line 68
    new-instance v0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$PermissionGroupInfoComparator;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$PermissionGroupInfoComparator;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPermGroupComparator:Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$PermissionGroupInfoComparator;

    .line 70
    new-instance v0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$PermissionInfoComparator;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$PermissionInfoComparator;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPermComparator:Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$PermissionInfoComparator;

    .line 128
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mContext:Landroid/content/Context;

    .line 129
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mInflater:Landroid/view/LayoutInflater;

    .line 130
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    .line 131
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 134
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;-><init>(Landroid/content/Context;)V

    .line 135
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 138
    .local v1, "permSet":Ljava/util/Set;, "Ljava/util/Set<Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;>;"
    :try_start_0
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    const/16 v5, 0x1000

    invoke-virtual {v4, p2, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 144
    .local v3, "pkgInfo":Landroid/content/pm/PackageInfo;
    iget-object v4, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v4, :cond_0

    iget-object v4, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_0

    .line 145
    iget-object v4, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-direct {p0, v4, v1}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->getAllUsedPermissions(ILjava/util/Set;)V

    .line 147
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 148
    .local v2, "permsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;>;"
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 149
    invoke-direct {p0, v2}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->setPermissions(Ljava/util/List;)V

    .line 150
    .end local v2    # "permsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;>;"
    .end local v3    # "pkgInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return-void

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v4, "AppSecurityPermissions"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Couldn\'t retrieve permissions for package:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private addPermToList(Ljava/util/List;Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;)V
    .locals 2
    .param p2, "pInfo"    # Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;",
            ">;",
            "Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 384
    .local p1, "permList":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;>;"
    iget-object v1, p2, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;->mLabel:Ljava/lang/CharSequence;

    if-nez v1, :cond_0

    .line 385
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    invoke-virtual {p2, v1}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p2, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;->mLabel:Ljava/lang/CharSequence;

    .line 387
    :cond_0
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPermComparator:Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$PermissionInfoComparator;

    invoke-static {p1, p2, v1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    .line 388
    .local v0, "idx":I
    if-gez v0, :cond_1

    .line 389
    neg-int v1, v0

    add-int/lit8 v0, v1, -0x1

    .line 390
    invoke-interface {p1, v0, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 392
    :cond_1
    return-void
.end method

.method private displayPermissions(Ljava/util/List;Landroid/widget/LinearLayout;)V
    .locals 10
    .param p2, "permListView"    # Landroid/widget/LinearLayout;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;",
            ">;",
            "Landroid/widget/LinearLayout;",
            ")V"
        }
    .end annotation

    .prologue
    .line 290
    .local p1, "groups":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;>;"
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 292
    const/high16 v8, 0x41000000    # 8.0f

    iget-object v9, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v8, v9

    float-to-int v6, v8

    .line 294
    .local v6, "spacing":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    if-ge v1, v8, :cond_5

    .line 295
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;

    .line 296
    .local v0, "grp":Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;
    const v8, 0xffff

    invoke-direct {p0, v0, v8}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->getPermissionList(Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;I)Ljava/util/List;

    move-result-object v5

    .line 297
    .local v5, "perms":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;>;"
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v8

    if-ge v2, v8, :cond_4

    .line 298
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;

    .line 299
    .local v4, "perm":Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;
    if-nez v2, :cond_3

    const/4 v8, 0x1

    :goto_2
    invoke-direct {p0, v0, v4, v8}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->getPermissionItemView(Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;Z)Lorg/namelessrom/devicecontrol/appmanager/permissions/PermissionItemView;

    move-result-object v7

    .line 300
    .local v7, "view":Landroid/view/View;
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v8, -0x1

    const/4 v9, -0x2

    invoke-direct {v3, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 303
    .local v3, "lp":Landroid/widget/LinearLayout$LayoutParams;
    if-nez v2, :cond_0

    .line 304
    iput v6, v3, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 306
    :cond_0
    iget-object v8, v0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;->mAllPermissions:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ne v2, v8, :cond_1

    .line 307
    iput v6, v3, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 309
    :cond_1
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v8

    if-nez v8, :cond_2

    .line 310
    iget v8, v3, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    mul-int/lit8 v8, v8, 0x2

    iput v8, v3, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 312
    :cond_2
    invoke-virtual {p2, v7, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 297
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 299
    .end local v3    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v7    # "view":Landroid/view/View;
    :cond_3
    const/4 v8, 0x0

    goto :goto_2

    .line 294
    .end local v4    # "perm":Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 315
    .end local v0    # "grp":Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;
    .end local v2    # "j":I
    .end local v5    # "perms":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;>;"
    :cond_5
    return-void
.end method

.method private extractPerms(Landroid/content/pm/PackageInfo;Ljava/util/Set;Landroid/content/pm/PackageInfo;)V
    .locals 21
    .param p1, "info"    # Landroid/content/pm/PackageInfo;
    .param p3, "installedPkgInfo"    # Landroid/content/pm/PackageInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/PackageInfo;",
            "Ljava/util/Set",
            "<",
            "Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;",
            ">;",
            "Landroid/content/pm/PackageInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 173
    .local p2, "permSet":Ljava/util/Set;, "Ljava/util/Set<Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;>;"
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    move-object/from16 v16, v0

    .line 174
    .local v16, "strList":[Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/content/pm/PackageInfo;->requestedPermissionsFlags:[I

    .line 175
    .local v6, "flagsList":[I
    if-eqz v16, :cond_0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v18, v0

    if-nez v18, :cond_1

    .line 245
    :cond_0
    return-void

    .line 178
    :cond_1
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v10, v0, :cond_0

    .line 179
    aget-object v15, v16, v10

    .line 182
    .local v15, "permName":Ljava/lang/String;
    if-eqz p3, :cond_3

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    if-ne v0, v1, :cond_3

    .line 183
    aget v18, v6, v10

    and-int/lit8 v18, v18, 0x2

    if-nez v18, :cond_3

    .line 178
    :cond_2
    :goto_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 188
    :cond_3
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v15, v1}, Landroid/content/pm/PackageManager;->getPermissionInfo(Ljava/lang/String;I)Landroid/content/pm/PermissionInfo;

    move-result-object v17

    .line 189
    .local v17, "tmpPermInfo":Landroid/content/pm/PermissionInfo;
    if-eqz v17, :cond_2

    .line 192
    const/4 v5, -0x1

    .line 193
    .local v5, "existingIndex":I
    if-eqz p3, :cond_4

    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_4

    .line 194
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_2
    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v11, v0, :cond_4

    .line 195
    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    move-object/from16 v18, v0

    aget-object v18, v18, v11

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 196
    move v5, v11

    .line 201
    .end local v11    # "j":I
    :cond_4
    if-eqz p3, :cond_9

    if-ltz v5, :cond_9

    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->requestedPermissionsFlags:[I

    move-object/from16 v18, v0

    aget v4, v18, v5

    .line 203
    .local v4, "existingFlags":I
    :goto_3
    aget v18, v6, v10

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v1, v2, v4}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->isDisplayablePermission(Landroid/content/pm/PermissionInfo;II)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 208
    move-object/from16 v0, v17

    iget-object v14, v0, Landroid/content/pm/PermissionInfo;->group:Ljava/lang/String;

    .line 209
    .local v14, "origGroupName":Ljava/lang/String;
    move-object v8, v14

    .line 210
    .local v8, "groupName":Ljava/lang/String;
    if-nez v8, :cond_5

    .line 211
    move-object/from16 v0, v17

    iget-object v8, v0, Landroid/content/pm/PermissionInfo;->packageName:Ljava/lang/String;

    .line 212
    move-object/from16 v0, v17

    iput-object v8, v0, Landroid/content/pm/PermissionInfo;->group:Ljava/lang/String;

    .line 214
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPermGroups:Ljava/util/Map;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;

    .line 215
    .local v7, "group":Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;
    if-nez v7, :cond_7

    .line 216
    const/4 v9, 0x0

    .line 217
    .local v9, "grp":Landroid/content/pm/PermissionGroupInfo;
    if-eqz v14, :cond_6

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v14, v1}, Landroid/content/pm/PackageManager;->getPermissionGroupInfo(Ljava/lang/String;I)Landroid/content/pm/PermissionGroupInfo;

    move-result-object v9

    .line 220
    :cond_6
    if-eqz v9, :cond_a

    .line 221
    new-instance v7, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;

    .end local v7    # "group":Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;
    invoke-direct {v7, v9}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;-><init>(Landroid/content/pm/PermissionGroupInfo;)V

    .line 230
    .restart local v7    # "group":Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPermGroups:Ljava/util/Map;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Landroid/content/pm/PermissionInfo;->group:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v0, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    .end local v9    # "grp":Landroid/content/pm/PermissionGroupInfo;
    :cond_7
    if-eqz p3, :cond_b

    and-int/lit8 v18, v4, 0x2

    if-nez v18, :cond_b

    const/4 v13, 0x1

    .line 234
    .local v13, "newPerm":Z
    :goto_5
    new-instance v12, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;

    move-object/from16 v0, v17

    invoke-direct {v12, v0}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;-><init>(Landroid/content/pm/PermissionInfo;)V

    .line 235
    .local v12, "myPerm":Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;
    aget v18, v6, v10

    move/from16 v0, v18

    iput v0, v12, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;->mNewReqFlags:I

    .line 236
    iput v4, v12, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;->mExistingReqFlags:I

    .line 239
    iput-boolean v13, v12, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;->mNew:Z

    .line 240
    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 241
    .end local v4    # "existingFlags":I
    .end local v5    # "existingIndex":I
    .end local v7    # "group":Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;
    .end local v8    # "groupName":Ljava/lang/String;
    .end local v12    # "myPerm":Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;
    .end local v13    # "newPerm":Z
    .end local v14    # "origGroupName":Ljava/lang/String;
    .end local v17    # "tmpPermInfo":Landroid/content/pm/PermissionInfo;
    :catch_0
    move-exception v3

    .line 242
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v18, "AppSecurityPermissions"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Ignoring unknown permission:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 194
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v5    # "existingIndex":I
    .restart local v11    # "j":I
    .restart local v17    # "tmpPermInfo":Landroid/content/pm/PermissionInfo;
    :cond_8
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_2

    .line 201
    .end local v11    # "j":I
    :cond_9
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 227
    .restart local v4    # "existingFlags":I
    .restart local v7    # "group":Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;
    .restart local v8    # "groupName":Ljava/lang/String;
    .restart local v9    # "grp":Landroid/content/pm/PermissionGroupInfo;
    .restart local v14    # "origGroupName":Ljava/lang/String;
    :cond_a
    :try_start_1
    move-object/from16 v0, v17

    iget-object v0, v0, Landroid/content/pm/PermissionInfo;->packageName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Landroid/content/pm/PermissionInfo;->group:Ljava/lang/String;

    .line 228
    new-instance v7, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;

    .end local v7    # "group":Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;
    move-object/from16 v0, v17

    invoke-direct {v7, v0}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;-><init>(Landroid/content/pm/PermissionInfo;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .restart local v7    # "group":Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;
    goto :goto_4

    .line 232
    .end local v9    # "grp":Landroid/content/pm/PermissionGroupInfo;
    :cond_b
    const/4 v13, 0x0

    goto :goto_5
.end method

.method private getAllUsedPermissions(ILjava/util/Set;)V
    .locals 6
    .param p1, "sharedUid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Set",
            "<",
            "Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 153
    .local p2, "permSet":Ljava/util/Set;, "Ljava/util/Set<Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;>;"
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    invoke-virtual {v5, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v4

    .line 154
    .local v4, "sharedPkgList":[Ljava/lang/String;
    if-eqz v4, :cond_0

    array-length v5, v4

    if-nez v5, :cond_1

    .line 160
    :cond_0
    return-void

    .line 157
    :cond_1
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 158
    .local v3, "sharedPkg":Ljava/lang/String;
    invoke-direct {p0, v3, p2}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->getPermissionsForPackage(Ljava/lang/String;Ljava/util/Set;)V

    .line 157
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private getPermissionItemView(Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;Z)Lorg/namelessrom/devicecontrol/appmanager/permissions/PermissionItemView;
    .locals 7
    .param p1, "grp"    # Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;
    .param p2, "perm"    # Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;
    .param p3, "first"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 319
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x11

    if-gt v4, v5, :cond_0

    move v1, v0

    .line 320
    .local v1, "oldApi":Z
    :goto_0
    if-nez v1, :cond_1

    iget v4, p2, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;->flags:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_1

    .line 321
    .local v0, "costsMoney":Z
    :goto_1
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mInflater:Landroid/view/LayoutInflater;

    if-eqz v0, :cond_2

    const v4, 0x7f04001e

    :goto_2
    const/4 v6, 0x0

    invoke-virtual {v5, v4, v6, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/appmanager/permissions/PermissionItemView;

    .line 324
    .local v2, "permView":Lorg/namelessrom/devicecontrol/appmanager/permissions/PermissionItemView;
    invoke-virtual {v2, p1, p2, p3}, Lorg/namelessrom/devicecontrol/appmanager/permissions/PermissionItemView;->setPermission(Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;Z)V

    .line 325
    return-object v2

    .end local v0    # "costsMoney":Z
    .end local v1    # "oldApi":Z
    .end local v2    # "permView":Lorg/namelessrom/devicecontrol/appmanager/permissions/PermissionItemView;
    :cond_0
    move v1, v3

    .line 319
    goto :goto_0

    .restart local v1    # "oldApi":Z
    :cond_1
    move v0, v3

    .line 320
    goto :goto_1

    .line 321
    .restart local v0    # "costsMoney":Z
    :cond_2
    const v4, 0x7f04001d

    goto :goto_2
.end method

.method private getPermissionList(Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;I)Ljava/util/List;
    .locals 1
    .param p1, "grp"    # Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;
    .param p2, "which"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 252
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    .line 253
    iget-object v0, p1, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;->mNewPermissions:Ljava/util/ArrayList;

    .line 259
    :goto_0
    return-object v0

    .line 254
    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 255
    iget-object v0, p1, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;->mPersonalPermissions:Ljava/util/ArrayList;

    goto :goto_0

    .line 256
    :cond_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    .line 257
    iget-object v0, p1, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;->mDevicePermissions:Ljava/util/ArrayList;

    goto :goto_0

    .line 259
    :cond_2
    iget-object v0, p1, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;->mAllPermissions:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method private getPermissionsForPackage(Ljava/lang/String;Ljava/util/Set;)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 164
    .local p2, "permSet":Ljava/util/Set;, "Ljava/util/Set<Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;>;"
    :try_start_0
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    const/16 v3, 0x1000

    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 165
    .local v1, "pkgInfo":Landroid/content/pm/PackageInfo;
    invoke-direct {p0, v1, p2, v1}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->extractPerms(Landroid/content/pm/PackageInfo;Ljava/util/Set;Landroid/content/pm/PackageInfo;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    .end local v1    # "pkgInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return-void

    .line 166
    :catch_0
    move-exception v0

    .line 167
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "AppSecurityPermissions"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t retrieve permissions for package: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isDisplayablePermission(Landroid/content/pm/PermissionInfo;II)Z
    .locals 10
    .param p1, "pInfo"    # Landroid/content/pm/PermissionInfo;
    .param p2, "newReqFlags"    # I
    .param p3, "existingReqFlags"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 330
    iget v9, p1, Landroid/content/pm/PermissionInfo;->protectionLevel:I

    and-int/lit8 v0, v9, 0xf

    .line 331
    .local v0, "base":I
    if-nez v0, :cond_2

    move v4, v7

    .line 332
    .local v4, "isNormal":Z
    :goto_0
    if-ne v0, v7, :cond_3

    move v1, v7

    .line 333
    .local v1, "isDangerous":Z
    :goto_1
    and-int/lit8 v9, p2, 0x1

    if-eqz v9, :cond_4

    move v5, v7

    .line 335
    .local v5, "isRequired":Z
    :goto_2
    iget v9, p1, Landroid/content/pm/PermissionInfo;->protectionLevel:I

    and-int/lit8 v9, v9, 0x20

    if-eqz v9, :cond_5

    move v2, v7

    .line 337
    .local v2, "isDevelopment":Z
    :goto_3
    and-int/lit8 v9, p3, 0x2

    if-eqz v9, :cond_6

    move v6, v7

    .line 339
    .local v6, "wasGranted":Z
    :goto_4
    and-int/lit8 v9, p2, 0x2

    if-eqz v9, :cond_7

    move v3, v7

    .line 344
    .local v3, "isGranted":Z
    :goto_5
    if-nez v4, :cond_0

    if-eqz v1, :cond_8

    :cond_0
    if-nez v5, :cond_1

    if-nez v6, :cond_1

    if-eqz v3, :cond_8

    .line 351
    :cond_1
    :goto_6
    return v7

    .end local v1    # "isDangerous":Z
    .end local v2    # "isDevelopment":Z
    .end local v3    # "isGranted":Z
    .end local v4    # "isNormal":Z
    .end local v5    # "isRequired":Z
    .end local v6    # "wasGranted":Z
    :cond_2
    move v4, v8

    .line 331
    goto :goto_0

    .restart local v4    # "isNormal":Z
    :cond_3
    move v1, v8

    .line 332
    goto :goto_1

    .restart local v1    # "isDangerous":Z
    :cond_4
    move v5, v8

    .line 333
    goto :goto_2

    .restart local v5    # "isRequired":Z
    :cond_5
    move v2, v8

    .line 335
    goto :goto_3

    .restart local v2    # "isDevelopment":Z
    :cond_6
    move v6, v8

    .line 337
    goto :goto_4

    .restart local v6    # "wasGranted":Z
    :cond_7
    move v3, v8

    .line 339
    goto :goto_5

    .line 351
    .restart local v3    # "isGranted":Z
    :cond_8
    if-eqz v2, :cond_9

    if-nez v6, :cond_1

    :cond_9
    move v7, v8

    goto :goto_6
.end method

.method private setPermissions(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 395
    .local p1, "permList":Ljava/util/List;, "Ljava/util/List<Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;>;"
    if-eqz p1, :cond_3

    .line 397
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;

    .line 398
    .local v4, "pInfo":Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;
    iget v6, v4, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;->mNewReqFlags:I

    iget v7, v4, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;->mExistingReqFlags:I

    invoke-direct {p0, v4, v6, v7}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->isDisplayablePermission(Landroid/content/pm/PermissionInfo;II)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 401
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPermGroups:Ljava/util/Map;

    iget-object v7, v4, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;->group:Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;

    .line 402
    .local v2, "group":Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;
    if-eqz v2, :cond_0

    .line 403
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    invoke-virtual {v4, v6}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    iput-object v6, v4, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;->mLabel:Ljava/lang/CharSequence;

    .line 404
    iget-object v6, v2, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;->mAllPermissions:Ljava/util/ArrayList;

    invoke-direct {p0, v6, v4}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->addPermToList(Ljava/util/List;Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;)V

    .line 405
    iget-boolean v6, v4, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;->mNew:Z

    if-eqz v6, :cond_1

    .line 406
    iget-object v6, v2, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;->mNewPermissions:Ljava/util/ArrayList;

    invoke-direct {p0, v6, v4}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->addPermToList(Ljava/util/List;Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;)V

    .line 408
    :cond_1
    iget v6, v2, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;->flags:I

    and-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_2

    .line 409
    iget-object v6, v2, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;->mPersonalPermissions:Ljava/util/ArrayList;

    invoke-direct {p0, v6, v4}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->addPermToList(Ljava/util/List;Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;)V

    goto :goto_0

    .line 411
    :cond_2
    iget-object v6, v2, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;->mDevicePermissions:Ljava/util/ArrayList;

    invoke-direct {p0, v6, v4}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->addPermToList(Ljava/util/List;Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;)V

    goto :goto_0

    .line 417
    .end local v2    # "group":Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "pInfo":Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;
    :cond_3
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPermGroups:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;

    .line 418
    .local v5, "pgrp":Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;
    iget v6, v5, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;->labelRes:I

    if-nez v6, :cond_4

    iget-object v6, v5, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    if-eqz v6, :cond_5

    .line 419
    :cond_4
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    invoke-virtual {v5, v6}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    iput-object v6, v5, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;->mLabel:Ljava/lang/CharSequence;

    .line 429
    :goto_2
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPermGroupsList:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 423
    :cond_5
    :try_start_0
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    iget-object v7, v5, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;->packageName:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 424
    .local v0, "app":Landroid/content/pm/ApplicationInfo;
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v6}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    iput-object v6, v5, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;->mLabel:Ljava/lang/CharSequence;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 425
    .end local v0    # "app":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v1

    .line 426
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    invoke-virtual {v5, v6}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    iput-object v6, v5, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;->mLabel:Ljava/lang/CharSequence;

    goto :goto_2

    .line 431
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v5    # "pgrp":Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionGroupInfo;
    :cond_6
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPermGroupsList:Ljava/util/List;

    iget-object v7, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPermGroupComparator:Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$PermissionGroupInfoComparator;

    invoke-static {v6, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 432
    return-void
.end method


# virtual methods
.method public getPermissionsView()Landroid/view/View;
    .locals 7

    .prologue
    .line 272
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f04001f

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 274
    .local v2, "permsView":Landroid/widget/LinearLayout;
    const v3, 0x7f0c007c

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 275
    .local v0, "displayList":Landroid/widget/LinearLayout;
    const v3, 0x7f0c007b

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 277
    .local v1, "noPermsView":Landroid/view/View;
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->mPermGroupsList:Ljava/util/List;

    invoke-direct {p0, v3, v0}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;->displayPermissions(Ljava/util/List;Landroid/widget/LinearLayout;)V

    .line 278
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 279
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 282
    :cond_0
    return-object v2
.end method
