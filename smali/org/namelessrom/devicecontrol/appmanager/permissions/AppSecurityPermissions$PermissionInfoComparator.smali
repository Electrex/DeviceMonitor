.class Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$PermissionInfoComparator;
.super Ljava/lang/Object;
.source "AppSecurityPermissions.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PermissionInfoComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final sCollator:Ljava/text/Collator;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 374
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 372
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$PermissionInfoComparator;->sCollator:Ljava/text/Collator;

    .line 375
    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 371
    check-cast p1, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$PermissionInfoComparator;->compare(Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;)I

    move-result v0

    return v0
.end method

.method public final compare(Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;)I
    .locals 3
    .param p1, "a"    # Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;
    .param p2, "b"    # Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;

    .prologue
    .line 378
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$PermissionInfoComparator;->sCollator:Ljava/text/Collator;

    iget-object v1, p1, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;->mLabel:Ljava/lang/CharSequence;

    iget-object v2, p2, Lorg/namelessrom/devicecontrol/appmanager/permissions/AppSecurityPermissions$MyPermissionInfo;->mLabel:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
