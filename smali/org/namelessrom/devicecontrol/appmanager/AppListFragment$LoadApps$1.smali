.class Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps$1;
.super Ljava/lang/Object;
.source "AppListFragment.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;->onPostExecute(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;

.field final synthetic val$appItems:Ljava/util/List;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps$1;->this$1:Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps$1;->val$appItems:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 165
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 156
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps$1;->val$appItems:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 157
    new-instance v0, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps$1;->this$1:Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps$1;->val$appItems:Ljava/util/List;

    invoke-direct {v0, v1, v2}, Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;-><init>(Landroid/app/Activity;Ljava/util/List;)V

    .line 158
    .local v0, "adapter":Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps$1;->this$1:Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;

    # getter for: Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->access$200(Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 161
    .end local v0    # "adapter":Lorg/namelessrom/devicecontrol/ui/adapters/AppListAdapter;
    :cond_0
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps$1;->this$1:Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;

    # getter for: Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->mProgressContainer:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->access$100(Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 162
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps$1;->this$1:Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;

    iget-object v1, v1, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment$LoadApps;->this$0:Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;

    const/4 v2, 0x0

    # setter for: Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->mIsLoading:Z
    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;->access$302(Lorg/namelessrom/devicecontrol/appmanager/AppListFragment;Z)Z

    .line 163
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 167
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 153
    return-void
.end method
