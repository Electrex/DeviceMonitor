.class Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$7;
.super Ljava/lang/Object;
.source "AddTaskActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->doNext()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$7;->this$0:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 177
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$7;->this$0:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    # getter for: Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->access$400(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 178
    .local v0, "currentItem":I
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$7;->this$0:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    # getter for: Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPageList:Lorg/namelessrom/devicecontrol/wizard/setup/PageList;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->access$200(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)Lorg/namelessrom/devicecontrol/wizard/setup/PageList;

    move-result-object v3

    invoke-virtual {v3, v0}, Lorg/namelessrom/devicecontrol/wizard/setup/PageList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    .line 179
    .local v1, "currentPage":Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->getId()I

    move-result v3

    const v4, 0x7f0e01da

    if-ne v3, v4, :cond_0

    .line 180
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$7;->this$0:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    # getter for: Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mSetupData:Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->access$500(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;

    move-result-object v3

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->getSetupData()Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    move-result-object v2

    .line 181
    .local v2, "item":Lorg/namelessrom/devicecontrol/tasker/TaskerItem;
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$7;->this$0:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    invoke-static {v3}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->deleteItem(Lorg/namelessrom/devicecontrol/tasker/TaskerItem;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->addItem(Lorg/namelessrom/devicecontrol/tasker/TaskerItem;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    move-result-object v3

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$7;->this$0:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    invoke-virtual {v3, v4}, Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;->saveConfiguration(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/TaskerConfiguration;

    .line 185
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$7;->this$0:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    # invokes: Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->finishSetup()V
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->access$600(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)V

    .line 189
    .end local v2    # "item":Lorg/namelessrom/devicecontrol/tasker/TaskerItem;
    :goto_0
    return-void

    .line 187
    :cond_0
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$7;->this$0:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    # getter for: Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->access$400(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v3

    add-int/lit8 v4, v0, 0x1

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    goto :goto_0
.end method
