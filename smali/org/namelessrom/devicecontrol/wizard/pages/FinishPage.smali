.class public Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage;
.super Lorg/namelessrom/devicecontrol/wizard/setup/Page;
.source "FinishPage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage$FinishFragment;
    }
.end annotation


# instance fields
.field private fragment:Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage$FinishFragment;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callbacks"    # Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;
    .param p3, "titleResourceId"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;-><init>(Landroid/content/Context;Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;I)V

    .line 41
    return-void
.end method


# virtual methods
.method public createFragment()Landroid/app/Fragment;
    .locals 3

    .prologue
    .line 44
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 45
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "key_arg"

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    new-instance v1, Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage$FinishFragment;

    invoke-direct {v1}, Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage$FinishFragment;-><init>()V

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage;->fragment:Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage$FinishFragment;

    .line 48
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage;->fragment:Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage$FinishFragment;

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage$FinishFragment;->setArguments(Landroid/os/Bundle;)V

    .line 49
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage;->fragment:Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage$FinishFragment;

    return-object v1
.end method

.method public getNextButtonResId()I
    .locals 1

    .prologue
    .line 59
    const v0, 0x7f0e00fe

    return v0
.end method

.method public refresh()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage;->fragment:Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage$FinishFragment;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage;->fragment:Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage$FinishFragment;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage$FinishFragment;->setUpPage()V

    .line 56
    :cond_0
    return-void
.end method
