.class public Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;
.super Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;
.source "TriggerPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TriggerFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;->setData(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;)Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;->mPage:Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    return-object v0
.end method

.method static synthetic access$200(Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;)Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;->mCallbacks:Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    return-object v0
.end method

.method private setData(Ljava/lang/String;)V
    .locals 2
    .param p1, "entry"    # Ljava/lang/String;

    .prologue
    .line 100
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;->mCallbacks:Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    invoke-interface {v0}, Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;->getSetupData()Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    move-result-object v0

    iput-object p1, v0, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->trigger:Ljava/lang/String;

    .line 101
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;->mPage:Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->setCompleted(Z)V

    .line 102
    return-void
.end method


# virtual methods
.method protected getLayoutResource()I
    .locals 1

    .prologue
    .line 105
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/Application;->isDarkTheme()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    const v0, 0x7f04005b

    .line 108
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f04005c

    goto :goto_0
.end method

.method protected getTitleResource()I
    .locals 1

    .prologue
    .line 111
    const v0, 0x7f0e01db

    return v0
.end method

.method protected setUpPage()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 67
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;->mPage:Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    invoke-virtual {v7, v9}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->setRequired(Z)Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    .line 68
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;->mRootView:Landroid/view/View;

    const v8, 0x102000a

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ListView;

    .line 70
    .local v4, "listView":Landroid/widget/ListView;
    invoke-static {}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor;->getTriggers()Ljava/util/ArrayList;

    move-result-object v5

    .line 71
    .local v5, "triggers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-direct {v1, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 72
    .local v1, "entries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 73
    .local v6, "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    .line 74
    .local v2, "entry":Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;
    iget-object v7, v2, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;->name:Ljava/lang/String;

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    iget-object v7, v2, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;->value:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 78
    .end local v2    # "entry":Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;
    :cond_0
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const v8, 0x109000f

    invoke-direct {v0, v7, v8, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 81
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    invoke-virtual {v4, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 82
    invoke-virtual {v4, v9}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 83
    new-instance v7, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment$1;

    invoke-direct {v7, p0, v6}, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment$1;-><init>(Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;Ljava/util/ArrayList;)V

    invoke-virtual {v4, v7}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 92
    iget-object v7, p0, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;->mCallbacks:Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    invoke-interface {v7}, Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;->getSetupData()Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    move-result-object v7

    iget-object v2, v7, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->trigger:Ljava/lang/String;

    .line 93
    .local v2, "entry":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 94
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v7

    invoke-virtual {v4, v7, v9}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 95
    invoke-direct {p0, v2}, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;->setData(Ljava/lang/String;)V

    .line 97
    :cond_1
    return-void
.end method
