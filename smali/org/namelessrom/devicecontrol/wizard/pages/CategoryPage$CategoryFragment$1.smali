.class Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment$1;
.super Ljava/lang/Object;
.source "CategoryPage.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;->setUpPage()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;

.field final synthetic val$values:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment$1;->this$0:Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment$1;->val$values:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "i"    # I
    .param p4, "l"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 89
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const-string v0, "position: %s | values: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment$1;->val$values:Ljava/util/ArrayList;

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment$1;->this$0:Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment$1;->val$values:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    # invokes: Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;->setData(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;->access$000(Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment$1;->this$0:Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;

    # getter for: Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;->mCallbacks:Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;->access$200(Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;)Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    move-result-object v0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment$1;->this$0:Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;

    # getter for: Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;->mPage:Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;->access$100(Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;)Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;->onPageLoaded(Lorg/namelessrom/devicecontrol/wizard/setup/Page;)V

    .line 92
    return-void
.end method
