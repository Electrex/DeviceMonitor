.class public Lorg/namelessrom/devicecontrol/wizard/pages/InformationPage;
.super Lorg/namelessrom/devicecontrol/wizard/setup/Page;
.source "InformationPage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/wizard/pages/InformationPage$InformationFragment;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callbacks"    # Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;
    .param p3, "titleResourceId"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;-><init>(Landroid/content/Context;Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;I)V

    .line 35
    return-void
.end method


# virtual methods
.method public createFragment()Landroid/app/Fragment;
    .locals 4

    .prologue
    .line 38
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 39
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "key_arg"

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/pages/InformationPage;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    new-instance v1, Lorg/namelessrom/devicecontrol/wizard/pages/InformationPage$InformationFragment;

    invoke-direct {v1}, Lorg/namelessrom/devicecontrol/wizard/pages/InformationPage$InformationFragment;-><init>()V

    .line 42
    .local v1, "fragment":Lorg/namelessrom/devicecontrol/wizard/pages/InformationPage$InformationFragment;
    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/wizard/pages/InformationPage$InformationFragment;->setArguments(Landroid/os/Bundle;)V

    .line 43
    return-object v1
.end method

.method public getNextButtonResId()I
    .locals 1

    .prologue
    .line 48
    const v0, 0x7f0e0170

    return v0
.end method

.method public refresh()V
    .locals 0

    .prologue
    .line 46
    return-void
.end method
