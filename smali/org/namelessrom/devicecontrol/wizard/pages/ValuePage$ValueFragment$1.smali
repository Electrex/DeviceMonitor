.class Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment$1;
.super Ljava/lang/Object;
.source "ValuePage.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment;->setUpPage()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment;

.field final synthetic val$entryValues:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment$1;->this$0:Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment$1;->val$entryValues:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "i"    # I
    .param p4, "l"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 87
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const-string v0, "position: %s | values: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment$1;->val$entryValues:Ljava/util/ArrayList;

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment$1;->this$0:Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment;

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment$1;->val$entryValues:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    # invokes: Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment;->setData(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment;->access$000(Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment$1;->this$0:Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment;

    # getter for: Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment;->mCallbacks:Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment;->access$200(Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment;)Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    move-result-object v0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment$1;->this$0:Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment;

    # getter for: Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment;->mPage:Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment;->access$100(Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage$ValueFragment;)Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;->onPageLoaded(Lorg/namelessrom/devicecontrol/wizard/setup/Page;)V

    .line 90
    return-void
.end method
