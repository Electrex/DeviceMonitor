.class public Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage$ActionFragment;
.super Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;
.source "ActionPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ActionFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage$ActionFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage$ActionFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage$ActionFragment;->setData(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage$ActionFragment;)Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage$ActionFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage$ActionFragment;->mPage:Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    return-object v0
.end method

.method static synthetic access$200(Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage$ActionFragment;)Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage$ActionFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage$ActionFragment;->mCallbacks:Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    return-object v0
.end method

.method private setData(Ljava/lang/String;)V
    .locals 2
    .param p1, "entry"    # Ljava/lang/String;

    .prologue
    .line 101
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage$ActionFragment;->mCallbacks:Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    invoke-interface {v0}, Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;->getSetupData()Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    move-result-object v0

    iput-object p1, v0, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->name:Ljava/lang/String;

    .line 102
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage$ActionFragment;->mPage:Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->setCompleted(Z)V

    .line 103
    return-void
.end method


# virtual methods
.method protected getLayoutResource()I
    .locals 1

    .prologue
    .line 106
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/Application;->isDarkTheme()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    const v0, 0x7f04005b

    .line 109
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f04005c

    goto :goto_0
.end method

.method protected getTitleResource()I
    .locals 1

    .prologue
    .line 112
    const v0, 0x7f0e01d8

    return v0
.end method

.method protected setUpPage()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 67
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage$ActionFragment;->mPage:Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    invoke-virtual {v8, v10}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->setRequired(Z)Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    .line 68
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage$ActionFragment;->mRootView:Landroid/view/View;

    const v9, 0x102000a

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ListView;

    .line 70
    .local v6, "listView":Landroid/widget/ListView;
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage$ActionFragment;->mCallbacks:Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    invoke-interface {v8}, Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;->getSetupData()Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    move-result-object v8

    iget-object v2, v8, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->category:Ljava/lang/String;

    .line 71
    .local v2, "category":Ljava/lang/String;
    invoke-static {v2}, Lorg/namelessrom/devicecontrol/actions/ActionProcessor;->getActions(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 72
    .local v0, "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-direct {v3, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 73
    .local v3, "entries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 74
    .local v7, "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;

    .line 75
    .local v4, "entry":Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;
    iget-object v8, v4, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;->name:Ljava/lang/String;

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    iget-object v8, v4, Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;->value:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 79
    .end local v4    # "entry":Lorg/namelessrom/devicecontrol/actions/ActionProcessor$Entry;
    :cond_0
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage$ActionFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const v9, 0x109000f

    invoke-direct {v1, v8, v9, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 82
    .local v1, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    invoke-virtual {v6, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 83
    invoke-virtual {v6, v10}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 84
    new-instance v8, Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage$ActionFragment$1;

    invoke-direct {v8, p0, v7}, Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage$ActionFragment$1;-><init>(Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage$ActionFragment;Ljava/util/ArrayList;)V

    invoke-virtual {v6, v8}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 93
    iget-object v8, p0, Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage$ActionFragment;->mCallbacks:Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    invoke-interface {v8}, Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;->getSetupData()Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    move-result-object v8

    iget-object v4, v8, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->name:Ljava/lang/String;

    .line 94
    .local v4, "entry":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 95
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v8

    invoke-virtual {v6, v8, v10}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 96
    invoke-direct {p0, v4}, Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage$ActionFragment;->setData(Ljava/lang/String;)V

    .line 98
    :cond_1
    return-void
.end method
