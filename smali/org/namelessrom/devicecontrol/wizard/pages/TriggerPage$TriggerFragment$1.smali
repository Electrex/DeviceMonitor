.class Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment$1;
.super Ljava/lang/Object;
.source "TriggerPage.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;->setUpPage()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;

.field final synthetic val$values:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment$1;->this$0:Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;

    iput-object p2, p0, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment$1;->val$values:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "i"    # I
    .param p4, "l"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 86
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const-string v0, "position: %s | values: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment$1;->val$values:Ljava/util/ArrayList;

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Lorg/namelessrom/devicecontrol/Logger;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment$1;->this$0:Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment$1;->val$values:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    # invokes: Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;->setData(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;->access$000(Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment$1;->this$0:Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;

    # getter for: Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;->mCallbacks:Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;->access$200(Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;)Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    move-result-object v0

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment$1;->this$0:Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;

    # getter for: Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;->mPage:Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;->access$100(Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;)Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;->onPageLoaded(Lorg/namelessrom/devicecontrol/wizard/setup/Page;)V

    .line 89
    return-void
.end method
