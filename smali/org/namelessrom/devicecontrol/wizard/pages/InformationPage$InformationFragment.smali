.class public Lorg/namelessrom/devicecontrol/wizard/pages/InformationPage$InformationFragment;
.super Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;
.source "InformationPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/wizard/pages/InformationPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InformationFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected getLayoutResource()I
    .locals 1

    .prologue
    .line 59
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/Application;->isDarkTheme()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    const v0, 0x7f04005d

    .line 62
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f04005e

    goto :goto_0
.end method

.method protected getTitleResource()I
    .locals 1

    .prologue
    .line 65
    const v0, 0x7f0e01dd

    return v0
.end method

.method protected setUpPage()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 53
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/InformationPage$InformationFragment;->mPage:Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    invoke-virtual {v0, v3}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->setRequired(Z)Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    .line 54
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/InformationPage$InformationFragment;->mPage:Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->setCompleted(Z)V

    .line 55
    const-string v0, "TaskerItem: %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/wizard/pages/InformationPage$InformationFragment;->mCallbacks:Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    invoke-interface {v2}, Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;->getSetupData()Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    move-result-object v2

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {p0, v0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    return-void
.end method
