.class public Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage;
.super Lorg/namelessrom/devicecontrol/wizard/setup/Page;
.source "CategoryPage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;
    }
.end annotation


# instance fields
.field private fragment:Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callbacks"    # Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;
    .param p3, "titleResourceId"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;-><init>(Landroid/content/Context;Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;I)V

    .line 45
    return-void
.end method


# virtual methods
.method public createFragment()Landroid/app/Fragment;
    .locals 3

    .prologue
    .line 48
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 49
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "key_arg"

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    new-instance v1, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;

    invoke-direct {v1}, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;-><init>()V

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage;->fragment:Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;

    .line 52
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage;->fragment:Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;->setArguments(Landroid/os/Bundle;)V

    .line 53
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage;->fragment:Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;

    return-object v1
.end method

.method public getNextButtonResId()I
    .locals 1

    .prologue
    .line 62
    const v0, 0x7f0e0170

    return v0
.end method

.method public refresh()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage;->fragment:Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage;->fragment:Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage$CategoryFragment;->setUpPage()V

    .line 60
    :cond_0
    return-void
.end method
