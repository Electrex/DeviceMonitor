.class public Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage;
.super Lorg/namelessrom/devicecontrol/wizard/setup/Page;
.source "TriggerPage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;
    }
.end annotation


# instance fields
.field private fragment:Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callbacks"    # Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;
    .param p3, "titleResourceId"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;-><init>(Landroid/content/Context;Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;I)V

    .line 45
    return-void
.end method


# virtual methods
.method public createFragment()Landroid/app/Fragment;
    .locals 3

    .prologue
    .line 48
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 49
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "key_arg"

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    new-instance v1, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;

    invoke-direct {v1}, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;-><init>()V

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage;->fragment:Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;

    .line 52
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage;->fragment:Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;

    invoke-virtual {v1, v0}, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;->setArguments(Landroid/os/Bundle;)V

    .line 53
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage;->fragment:Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;

    return-object v1
.end method

.method public getNextButtonResId()I
    .locals 1

    .prologue
    .line 62
    const v0, 0x7f0e0170

    return v0
.end method

.method public refresh()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage;->fragment:Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage;->fragment:Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage$TriggerFragment;->setUpPage()V

    .line 60
    :cond_0
    return-void
.end method
