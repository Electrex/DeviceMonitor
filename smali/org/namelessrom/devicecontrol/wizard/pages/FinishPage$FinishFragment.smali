.class public Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage$FinishFragment;
.super Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;
.source "FinishPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FinishFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected getLayoutResource()I
    .locals 1

    .prologue
    .line 88
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/Application;->isDarkTheme()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    const v0, 0x7f04005b

    .line 91
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f04005c

    goto :goto_0
.end method

.method protected getTitleResource()I
    .locals 1

    .prologue
    .line 95
    const v0, 0x7f0e01da

    return v0
.end method

.method protected setUpPage()V
    .locals 9

    .prologue
    const/4 v7, 0x4

    .line 66
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage$FinishFragment;->mRootView:Landroid/view/View;

    const v6, 0x102000a

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    .line 68
    .local v3, "listView":Landroid/widget/ListView;
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage$FinishFragment;->mCallbacks:Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    invoke-interface {v5}, Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;->getSetupData()Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    move-result-object v2

    .line 69
    .local v2, "item":Lorg/namelessrom/devicecontrol/tasker/TaskerItem;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 70
    .local v1, "entries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 71
    .local v4, "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const v5, 0x7f0e0223

    invoke-virtual {p0, v5}, Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage$FinishFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 72
    iget-object v5, v2, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->trigger:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    const v5, 0x7f0e0042

    invoke-virtual {p0, v5}, Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage$FinishFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    iget-object v5, v2, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->category:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    const v5, 0x7f0e0014

    invoke-virtual {p0, v5}, Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage$FinishFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    iget-object v5, v2, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    const v5, 0x7f0e0234

    invoke-virtual {p0, v5}, Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage$FinishFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object v5, v2, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->value:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    new-instance v0, Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage$FinishFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const v6, 0x7f04005a

    invoke-direct {v0, v5, v6, v1, v4}, Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 83
    .local v0, "adapter":Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter;
    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 84
    const-string v5, "TaskerItem: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage$FinishFragment;->mCallbacks:Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    invoke-interface {v8}, Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;->getSetupData()Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    move-result-object v8

    invoke-virtual {v8}, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {p0, v5, v6}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    return-void
.end method
