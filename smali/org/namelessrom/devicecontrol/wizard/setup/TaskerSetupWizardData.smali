.class public Lorg/namelessrom/devicecontrol/wizard/setup/TaskerSetupWizardData;
.super Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;
.source "TaskerSetupWizardData.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected onNewPageList()Lorg/namelessrom/devicecontrol/wizard/setup/PageList;
    .locals 6

    .prologue
    .line 36
    new-instance v0, Lorg/namelessrom/devicecontrol/wizard/setup/PageList;

    const/4 v1, 0x6

    new-array v1, v1, [Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    const/4 v2, 0x0

    new-instance v3, Lorg/namelessrom/devicecontrol/wizard/pages/InformationPage;

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/wizard/setup/TaskerSetupWizardData;->mContext:Landroid/content/Context;

    const v5, 0x7f0e01dd

    invoke-direct {v3, v4, p0, v5}, Lorg/namelessrom/devicecontrol/wizard/pages/InformationPage;-><init>(Landroid/content/Context;Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;I)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage;

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/wizard/setup/TaskerSetupWizardData;->mContext:Landroid/content/Context;

    const v5, 0x7f0e01db

    invoke-direct {v3, v4, p0, v5}, Lorg/namelessrom/devicecontrol/wizard/pages/TriggerPage;-><init>(Landroid/content/Context;Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;I)V

    aput-object v3, v1, v2

    const/4 v2, 0x2

    new-instance v3, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage;

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/wizard/setup/TaskerSetupWizardData;->mContext:Landroid/content/Context;

    const v5, 0x7f0e01d9

    invoke-direct {v3, v4, p0, v5}, Lorg/namelessrom/devicecontrol/wizard/pages/CategoryPage;-><init>(Landroid/content/Context;Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;I)V

    aput-object v3, v1, v2

    const/4 v2, 0x3

    new-instance v3, Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage;

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/wizard/setup/TaskerSetupWizardData;->mContext:Landroid/content/Context;

    const v5, 0x7f0e01d8

    invoke-direct {v3, v4, p0, v5}, Lorg/namelessrom/devicecontrol/wizard/pages/ActionPage;-><init>(Landroid/content/Context;Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;I)V

    aput-object v3, v1, v2

    const/4 v2, 0x4

    new-instance v3, Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage;

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/wizard/setup/TaskerSetupWizardData;->mContext:Landroid/content/Context;

    const v5, 0x7f0e01dc

    invoke-direct {v3, v4, p0, v5}, Lorg/namelessrom/devicecontrol/wizard/pages/ValuePage;-><init>(Landroid/content/Context;Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;I)V

    aput-object v3, v1, v2

    const/4 v2, 0x5

    new-instance v3, Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage;

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/wizard/setup/TaskerSetupWizardData;->mContext:Landroid/content/Context;

    const v5, 0x7f0e01da

    invoke-direct {v3, v4, p0, v5}, Lorg/namelessrom/devicecontrol/wizard/pages/FinishPage;-><init>(Landroid/content/Context;Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;I)V

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lorg/namelessrom/devicecontrol/wizard/setup/PageList;-><init>([Lorg/namelessrom/devicecontrol/wizard/setup/Page;)V

    return-object v0
.end method
