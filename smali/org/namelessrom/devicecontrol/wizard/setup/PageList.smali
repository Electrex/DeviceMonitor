.class public Lorg/namelessrom/devicecontrol/wizard/setup/PageList;
.super Ljava/util/ArrayList;
.source "PageList.java"

# interfaces
.implements Lorg/namelessrom/devicecontrol/wizard/setup/PageNode;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList",
        "<",
        "Lorg/namelessrom/devicecontrol/wizard/setup/Page;",
        ">;",
        "Lorg/namelessrom/devicecontrol/wizard/setup/PageNode;"
    }
.end annotation


# direct methods
.method public varargs constructor <init>([Lorg/namelessrom/devicecontrol/wizard/setup/Page;)V
    .locals 4
    .param p1, "pages"    # [Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 25
    move-object v0, p1

    .local v0, "arr$":[Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 26
    .local v3, "page":Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    invoke-virtual {p0, v3}, Lorg/namelessrom/devicecontrol/wizard/setup/PageList;->add(Ljava/lang/Object;)Z

    .line 25
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 28
    .end local v3    # "page":Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    :cond_0
    return-void
.end method


# virtual methods
.method public findPage(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/setup/PageList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    .line 33
    .local v0, "childPage":Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    invoke-virtual {v0, p1}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->findPage(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    move-result-object v1

    .line 34
    .local v1, "found":Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    if-eqz v1, :cond_0

    .line 38
    .end local v0    # "childPage":Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    .end local v1    # "found":Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
