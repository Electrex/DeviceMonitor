.class public abstract Lorg/namelessrom/devicecontrol/wizard/setup/Page;
.super Ljava/lang/Object;
.source "Page.java"

# interfaces
.implements Lorg/namelessrom/devicecontrol/wizard/setup/PageNode;


# instance fields
.field private mCallbacks:Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

.field private mCompleted:Z

.field private mData:Landroid/os/Bundle;

.field private mRequired:Z

.field private mTitle:Ljava/lang/String;

.field private mTitleResourceId:I


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callbacks"    # Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;
    .param p3, "titleResourceId"    # I

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->mData:Landroid/os/Bundle;

    .line 33
    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->mRequired:Z

    .line 34
    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->mCompleted:Z

    .line 37
    iput-object p2, p0, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->mCallbacks:Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    .line 38
    iput p3, p0, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->mTitleResourceId:I

    .line 39
    iget v0, p0, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->mTitleResourceId:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->mTitle:Ljava/lang/String;

    .line 40
    return-void
.end method


# virtual methods
.method public abstract createFragment()Landroid/app/Fragment;
.end method

.method public findPage(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .end local p0    # "this":Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    :goto_0
    return-object p0

    .restart local p0    # "this":Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public getData()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->mData:Landroid/os/Bundle;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->mTitleResourceId:I

    return v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public abstract getNextButtonResId()I
.end method

.method public isCompleted()Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->mCompleted:Z

    return v0
.end method

.method public isRequired()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->mRequired:Z

    return v0
.end method

.method public notifyDataChanged()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->mCallbacks:Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    invoke-interface {v0, p0}, Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;->onPageLoaded(Lorg/namelessrom/devicecontrol/wizard/setup/Page;)V

    .line 91
    return-void
.end method

.method public abstract refresh()V
.end method

.method public resetData(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    .line 85
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->mData:Landroid/os/Bundle;

    .line 86
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->notifyDataChanged()V

    .line 87
    return-void
.end method

.method public setCompleted(Z)V
    .locals 0
    .param p1, "completed"    # Z

    .prologue
    .line 81
    iput-boolean p1, p0, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->mCompleted:Z

    .line 82
    return-void
.end method

.method public setRequired(Z)Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    .locals 0
    .param p1, "required"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->mRequired:Z

    .line 95
    return-object p0
.end method
