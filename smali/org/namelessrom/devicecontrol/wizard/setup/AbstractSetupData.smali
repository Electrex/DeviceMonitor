.class public abstract Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;
.super Ljava/lang/Object;
.source "AbstractSetupData.java"

# interfaces
.implements Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;


# instance fields
.field private item:Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

.field protected mContext:Landroid/content/Context;

.field private mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;",
            ">;"
        }
    .end annotation
.end field

.field private mPageList:Lorg/namelessrom/devicecontrol/wizard/setup/PageList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->mListeners:Ljava/util/ArrayList;

    .line 31
    new-instance v0, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->item:Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    .line 34
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->mContext:Landroid/content/Context;

    .line 35
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->onNewPageList()Lorg/namelessrom/devicecontrol/wizard/setup/PageList;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->mPageList:Lorg/namelessrom/devicecontrol/wizard/setup/PageList;

    .line 36
    return-void
.end method


# virtual methods
.method public findPage(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 71
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->mPageList:Lorg/namelessrom/devicecontrol/wizard/setup/PageList;

    invoke-virtual {v0, p1}, Lorg/namelessrom/devicecontrol/wizard/setup/PageList;->findPage(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    move-result-object v0

    return-object v0
.end method

.method public getPage(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->findPage(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    move-result-object v0

    return-object v0
.end method

.method public getPageList()Lorg/namelessrom/devicecontrol/wizard/setup/PageList;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->mPageList:Lorg/namelessrom/devicecontrol/wizard/setup/PageList;

    return-object v0
.end method

.method public getSetupData()Lorg/namelessrom/devicecontrol/tasker/TaskerItem;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->item:Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    return-object v0
.end method

.method public load(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedValues"    # Landroid/os/Bundle;

    .prologue
    .line 75
    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 76
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->mPageList:Lorg/namelessrom/devicecontrol/wizard/setup/PageList;

    invoke-virtual {v2, v1}, Lorg/namelessrom/devicecontrol/wizard/setup/PageList;->findPage(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    move-result-object v2

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->resetData(Landroid/os/Bundle;)V

    goto :goto_0

    .line 78
    .end local v1    # "key":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method protected abstract onNewPageList()Lorg/namelessrom/devicecontrol/wizard/setup/PageList;
.end method

.method public onPageLoaded(Lorg/namelessrom/devicecontrol/wizard/setup/Page;)V
    .locals 3
    .param p1, "page"    # Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    .prologue
    .line 41
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    .line 42
    .local v1, "mListener":Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;
    invoke-interface {v1, p1}, Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;->onPageLoaded(Lorg/namelessrom/devicecontrol/wizard/setup/Page;)V

    goto :goto_0

    .line 44
    .end local v1    # "mListener":Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;
    :cond_0
    return-void
.end method

.method public registerListener(Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;)V
    .locals 1
    .param p1, "listener"    # Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    .prologue
    .line 99
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    return-void
.end method

.method public save()Landroid/os/Bundle;
    .locals 5

    .prologue
    .line 81
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 82
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->getPageList()Lorg/namelessrom/devicecontrol/wizard/setup/PageList;

    move-result-object v3

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/wizard/setup/PageList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    .line 83
    .local v2, "page":Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->getData()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 85
    .end local v2    # "page":Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    :cond_0
    return-object v0
.end method

.method public setSetupData(Lorg/namelessrom/devicecontrol/tasker/TaskerItem;)V
    .locals 0
    .param p1, "item"    # Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    .prologue
    .line 63
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->item:Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    .line 64
    return-void
.end method

.method public unregisterListener(Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;)V
    .locals 1
    .param p1, "listener"    # Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    .prologue
    .line 107
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 108
    return-void
.end method
