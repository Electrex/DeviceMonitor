.class Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$4;
.super Ljava/lang/Object;
.source "AddTaskActivity.java"

# interfaces
.implements Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip$OnPageSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$4;->this$0:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageStripSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 132
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$4;->this$0:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    # getter for: Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPagerAdapter:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->access$300(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 133
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$4;->this$0:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    # getter for: Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->access$400(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 134
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$4;->this$0:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    # getter for: Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->access$400(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 136
    :cond_0
    return-void
.end method
