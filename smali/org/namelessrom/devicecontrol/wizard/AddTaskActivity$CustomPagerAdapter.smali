.class Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;
.super Landroid/support/v13/app/FragmentStatePagerAdapter;
.source "AddTaskActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomPagerAdapter"
.end annotation


# instance fields
.field private mCutOffPage:I

.field final synthetic this$0:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;


# direct methods
.method private constructor <init>(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;Landroid/app/FragmentManager;)V
    .locals 0
    .param p2, "fm"    # Landroid/app/FragmentManager;

    .prologue
    .line 291
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;->this$0:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    .line 292
    invoke-direct {p0, p2}, Landroid/support/v13/app/FragmentStatePagerAdapter;-><init>(Landroid/app/FragmentManager;)V

    .line 293
    return-void
.end method

.method synthetic constructor <init>(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;Landroid/app/FragmentManager;Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;
    .param p2, "x1"    # Landroid/app/FragmentManager;
    .param p3, "x2"    # Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$1;

    .prologue
    .line 287
    invoke-direct {p0, p1, p2}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;-><init>(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;Landroid/app/FragmentManager;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 304
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;->this$0:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    # getter for: Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPageList:Lorg/namelessrom/devicecontrol/wizard/setup/PageList;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->access$200(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)Lorg/namelessrom/devicecontrol/wizard/setup/PageList;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 305
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;->mCutOffPage:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;->this$0:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    # getter for: Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPageList:Lorg/namelessrom/devicecontrol/wizard/setup/PageList;
    invoke-static {v1}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->access$200(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)Lorg/namelessrom/devicecontrol/wizard/setup/PageList;

    move-result-object v1

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/wizard/setup/PageList;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method public getCutOffPage()I
    .locals 1

    .prologue
    .line 316
    iget v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;->mCutOffPage:I

    return v0
.end method

.method public getItem(I)Landroid/app/Fragment;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 296
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;->this$0:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    # getter for: Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPageList:Lorg/namelessrom/devicecontrol/wizard/setup/PageList;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->access$200(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)Lorg/namelessrom/devicecontrol/wizard/setup/PageList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/namelessrom/devicecontrol/wizard/setup/PageList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->createFragment()Landroid/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 300
    const/4 v0, -0x2

    return v0
.end method

.method public setCutOffPage(I)V
    .locals 0
    .param p1, "cutOffPage"    # I

    .prologue
    .line 309
    if-gez p1, :cond_0

    .line 310
    const p1, 0x7fffffff

    .line 312
    :cond_0
    iput p1, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;->mCutOffPage:I

    .line 313
    return-void
.end method
