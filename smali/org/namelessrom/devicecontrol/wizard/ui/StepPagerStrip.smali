.class public Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;
.super Landroid/view/View;
.source "StepPagerStrip.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip$OnPageSelectedListener;
    }
.end annotation


# static fields
.field private static final ATTRS:[I


# instance fields
.field private mCurrentPage:I

.field private mGravity:I

.field private mNextTabPaint:Landroid/graphics/Paint;

.field private mOnPageSelectedListener:Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip$OnPageSelectedListener;

.field private mPageCount:I

.field private mPrevTabPaint:Landroid/graphics/Paint;

.field private mScroller:Landroid/widget/Scroller;

.field private mSelectedLastTabPaint:Landroid/graphics/Paint;

.field private mSelectedTabPaint:Landroid/graphics/Paint;

.field private mTabHeight:F

.field private mTabSpacing:F

.field private mTabWidth:F

.field private mTempRectF:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100af

    aput v2, v0, v1

    sput-object v0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->ATTRS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 66
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    const v3, 0x800033

    iput v3, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mGravity:I

    .line 51
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTempRectF:Landroid/graphics/RectF;

    .line 68
    sget-object v3, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->ATTRS:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 69
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v3, 0x0

    iget v4, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mGravity:I

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    iput v3, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mGravity:I

    .line 70
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 72
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 73
    .local v2, "res":Landroid/content/res/Resources;
    const v3, 0x7f0b004b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabWidth:F

    .line 74
    const v3, 0x7f0b0049

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabHeight:F

    .line 75
    const v3, 0x7f0b004a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabSpacing:F

    .line 79
    const v3, 0x7f0a006f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 80
    .local v1, "color":I
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mPrevTabPaint:Landroid/graphics/Paint;

    .line 81
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mPrevTabPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 83
    const v3, 0x7f0a0073

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 84
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mSelectedTabPaint:Landroid/graphics/Paint;

    .line 85
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mSelectedTabPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 87
    const v3, 0x7f0a0071

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 88
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mSelectedLastTabPaint:Landroid/graphics/Paint;

    .line 89
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mSelectedLastTabPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 91
    const v3, 0x7f0a006d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 92
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mNextTabPaint:Landroid/graphics/Paint;

    .line 93
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mNextTabPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 94
    return-void
.end method

.method private hitTest(F)I
    .locals 9
    .param p1, "x"    # F

    .prologue
    const/4 v5, -0x1

    .line 194
    iget v6, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mPageCount:I

    if-nez v6, :cond_1

    .line 227
    :cond_0
    :goto_0
    return v5

    .line 198
    :cond_1
    iget v6, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mPageCount:I

    int-to-float v6, v6

    iget v7, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabWidth:F

    iget v8, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabSpacing:F

    add-float/2addr v7, v8

    mul-float/2addr v6, v7

    iget v7, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabSpacing:F

    sub-float v4, v6, v7

    .line 200
    .local v4, "totalWidth":F
    const/4 v0, 0x0

    .line 202
    .local v0, "fillHorizontal":Z
    iget v6, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mGravity:I

    and-int/lit8 v6, v6, 0x7

    sparse-switch v6, :sswitch_data_0

    .line 214
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getPaddingLeft()I

    move-result v6

    int-to-float v2, v6

    .line 217
    .local v2, "totalLeft":F
    :goto_1
    iget v1, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabWidth:F

    .line 218
    .local v1, "tabWidth":F
    if-eqz v0, :cond_2

    .line 219
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getPaddingRight()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getPaddingLeft()I

    move-result v7

    sub-int/2addr v6, v7

    int-to-float v6, v6

    iget v7, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mPageCount:I

    add-int/lit8 v7, v7, -0x1

    int-to-float v7, v7

    iget v8, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabSpacing:F

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    iget v7, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mPageCount:I

    int-to-float v7, v7

    div-float v1, v6, v7

    .line 223
    :cond_2
    iget v6, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mPageCount:I

    int-to-float v6, v6

    iget v7, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabSpacing:F

    add-float/2addr v7, v1

    mul-float/2addr v6, v7

    add-float v3, v2, v6

    .line 224
    .local v3, "totalRight":F
    cmpl-float v6, p1, v2

    if-ltz v6, :cond_0

    cmpg-float v6, p1, v3

    if-gtz v6, :cond_0

    cmpl-float v6, v3, v2

    if-lez v6, :cond_0

    .line 225
    sub-float v5, p1, v2

    sub-float v6, v3, v2

    div-float/2addr v5, v6

    iget v6, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mPageCount:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    float-to-int v5, v5

    goto :goto_0

    .line 204
    .end local v1    # "tabWidth":F
    .end local v2    # "totalLeft":F
    .end local v3    # "totalRight":F
    :sswitch_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getWidth()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v6, v4

    const/high16 v7, 0x40000000    # 2.0f

    div-float v2, v6, v7

    .line 205
    .restart local v2    # "totalLeft":F
    goto :goto_1

    .line 207
    .end local v2    # "totalLeft":F
    :sswitch_1
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getPaddingRight()I

    move-result v7

    sub-int/2addr v6, v7

    int-to-float v6, v6

    sub-float v2, v6, v4

    .line 208
    .restart local v2    # "totalLeft":F
    goto :goto_1

    .line 210
    .end local v2    # "totalLeft":F
    :sswitch_2
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getPaddingLeft()I

    move-result v6

    int-to-float v2, v6

    .line 211
    .restart local v2    # "totalLeft":F
    const/4 v0, 0x1

    .line 212
    goto :goto_1

    .line 202
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x7 -> :sswitch_2
        0x800005 -> :sswitch_1
    .end sparse-switch
.end method

.method private scrollCurrentPageIntoView()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 242
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getPaddingLeft()I

    move-result v5

    int-to-float v5, v5

    iget v6, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mCurrentPage:I

    add-int/lit8 v6, v6, 0x1

    int-to-float v6, v6

    iget v7, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabWidth:F

    iget v8, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabSpacing:F

    add-float/2addr v7, v8

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    iget v6, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabSpacing:F

    sub-float v3, v5, v6

    .line 244
    .local v3, "widthToActive":F
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getWidth()I

    move-result v2

    .line 246
    .local v2, "viewWidth":I
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getScrollX()I

    move-result v1

    .line 247
    .local v1, "startScrollX":I
    int-to-float v5, v2

    cmpl-float v5, v3, v5

    if-lez v5, :cond_1

    int-to-float v5, v2

    sub-float v5, v3, v5

    float-to-int v0, v5

    .line 249
    .local v0, "destScrollX":I
    :goto_0
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mScroller:Landroid/widget/Scroller;

    if-nez v5, :cond_0

    .line 250
    new-instance v5, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mScroller:Landroid/widget/Scroller;

    .line 253
    :cond_0
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->abortAnimation()V

    .line 254
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mScroller:Landroid/widget/Scroller;

    sub-int v6, v0, v1

    invoke-virtual {v5, v1, v4, v6, v4}, Landroid/widget/Scroller;->startScroll(IIII)V

    .line 255
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->postInvalidate()V

    .line 256
    return-void

    .end local v0    # "destScrollX":I
    :cond_1
    move v0, v4

    .line 247
    goto :goto_0
.end method


# virtual methods
.method public computeScroll()V
    .locals 1

    .prologue
    .line 270
    invoke-super {p0}, Landroid/view/View;->computeScroll()V

    .line 271
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->setScrollX(I)V

    .line 274
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 101
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 103
    iget v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mPageCount:I

    if-nez v5, :cond_1

    .line 156
    :cond_0
    return-void

    .line 107
    :cond_1
    iget v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mPageCount:I

    int-to-float v5, v5

    iget v6, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabWidth:F

    iget v7, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabSpacing:F

    add-float/2addr v6, v7

    mul-float/2addr v5, v6

    iget v6, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabSpacing:F

    sub-float v4, v5, v6

    .line 109
    .local v4, "totalWidth":F
    const/4 v0, 0x0

    .line 111
    .local v0, "fillHorizontal":Z
    iget v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mGravity:I

    and-int/lit8 v5, v5, 0x7

    sparse-switch v5, :sswitch_data_0

    .line 123
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getPaddingLeft()I

    move-result v5

    int-to-float v3, v5

    .line 126
    .local v3, "totalLeft":F
    :goto_0
    iget v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mGravity:I

    and-int/lit8 v5, v5, 0x70

    sparse-switch v5, :sswitch_data_1

    .line 134
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getPaddingTop()I

    move-result v6

    int-to-float v6, v6

    iput v6, v5, Landroid/graphics/RectF;->top:F

    .line 137
    :goto_1
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTempRectF:Landroid/graphics/RectF;

    iget-object v6, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTempRectF:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iget v7, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabHeight:F

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->bottom:F

    .line 139
    iget v2, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabWidth:F

    .line 140
    .local v2, "tabWidth":F
    if-eqz v0, :cond_2

    .line 141
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getPaddingLeft()I

    move-result v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    iget v6, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mPageCount:I

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    iget v7, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabSpacing:F

    mul-float/2addr v6, v7

    sub-float/2addr v5, v6

    iget v6, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mPageCount:I

    int-to-float v6, v6

    div-float v2, v5, v6

    .line 145
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    iget v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mPageCount:I

    if-ge v1, v5, :cond_0

    .line 146
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTempRectF:Landroid/graphics/RectF;

    int-to-float v6, v1

    iget v7, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabSpacing:F

    add-float/2addr v7, v2

    mul-float/2addr v6, v7

    add-float/2addr v6, v3

    iput v6, v5, Landroid/graphics/RectF;->left:F

    .line 147
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTempRectF:Landroid/graphics/RectF;

    iget-object v6, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTempRectF:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    add-float/2addr v6, v2

    iput v6, v5, Landroid/graphics/RectF;->right:F

    .line 148
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTempRectF:Landroid/graphics/RectF;

    iget v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mCurrentPage:I

    if-ge v1, v5, :cond_3

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mPrevTabPaint:Landroid/graphics/Paint;

    :goto_3
    invoke-virtual {p1, v6, v5}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 145
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 113
    .end local v1    # "i":I
    .end local v2    # "tabWidth":F
    .end local v3    # "totalLeft":F
    :sswitch_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getWidth()I

    move-result v5

    int-to-float v5, v5

    sub-float/2addr v5, v4

    const/high16 v6, 0x40000000    # 2.0f

    div-float v3, v5, v6

    .line 114
    .restart local v3    # "totalLeft":F
    goto :goto_0

    .line 116
    .end local v3    # "totalLeft":F
    :sswitch_1
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    sub-float v3, v5, v4

    .line 117
    .restart local v3    # "totalLeft":F
    goto :goto_0

    .line 119
    .end local v3    # "totalLeft":F
    :sswitch_2
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getPaddingLeft()I

    move-result v5

    int-to-float v3, v5

    .line 120
    .restart local v3    # "totalLeft":F
    const/4 v0, 0x1

    .line 121
    goto :goto_0

    .line 128
    :sswitch_3
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getHeight()I

    move-result v6

    int-to-float v6, v6

    iget v7, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabHeight:F

    sub-float/2addr v6, v7

    float-to-int v6, v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    iput v6, v5, Landroid/graphics/RectF;->top:F

    goto/16 :goto_1

    .line 131
    :sswitch_4
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getHeight()I

    move-result v6

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    int-to-float v6, v6

    iget v7, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabHeight:F

    sub-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->top:F

    goto/16 :goto_1

    .line 148
    .restart local v1    # "i":I
    .restart local v2    # "tabWidth":F
    :cond_3
    iget v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mCurrentPage:I

    if-le v1, v5, :cond_4

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mNextTabPaint:Landroid/graphics/Paint;

    goto :goto_3

    :cond_4
    iget v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mPageCount:I

    add-int/lit8 v5, v5, -0x1

    if-ne v1, v5, :cond_5

    iget-object v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mSelectedLastTabPaint:Landroid/graphics/Paint;

    goto :goto_3

    :cond_5
    iget-object v5, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mSelectedTabPaint:Landroid/graphics/Paint;

    goto :goto_3

    .line 111
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x7 -> :sswitch_2
        0x800005 -> :sswitch_1
    .end sparse-switch

    .line 126
    :sswitch_data_1
    .sparse-switch
        0x10 -> :sswitch_3
        0x50 -> :sswitch_4
    .end sparse-switch
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 159
    iget v0, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mPageCount:I

    int-to-float v0, v0

    iget v1, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabWidth:F

    iget v2, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabSpacing:F

    add-float/2addr v1, v2

    mul-float/2addr v0, v1

    iget v1, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabSpacing:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    invoke-static {v0, p1}, Landroid/view/View;->resolveSize(II)I

    move-result v0

    iget v1, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mTabHeight:F

    float-to-int v1, v1

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {v1, p2}, Landroid/view/View;->resolveSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->setMeasuredDimension(II)V

    .line 171
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 174
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->scrollCurrentPageIntoView()V

    .line 175
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 176
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 179
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mOnPageSelectedListener:Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip$OnPageSelectedListener;

    if-eqz v1, :cond_0

    .line 180
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 190
    :cond_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    :goto_0
    return v1

    .line 183
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-direct {p0, v1}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->hitTest(F)I

    move-result v0

    .line 184
    .local v0, "position":I
    if-ltz v0, :cond_1

    .line 185
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mOnPageSelectedListener:Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip$OnPageSelectedListener;

    invoke-interface {v1, v0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip$OnPageSelectedListener;->onPageStripSelected(I)V

    .line 187
    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    .line 180
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setCurrentPage(I)V
    .locals 0
    .param p1, "currentPage"    # I

    .prologue
    .line 232
    iput p1, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mCurrentPage:I

    .line 237
    return-void
.end method

.method public setOnPageSelectedListener(Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip$OnPageSelectedListener;)V
    .locals 0
    .param p1, "onPageSelectedListener"    # Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip$OnPageSelectedListener;

    .prologue
    .line 97
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mOnPageSelectedListener:Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip$OnPageSelectedListener;

    .line 98
    return-void
.end method

.method public setPageCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 259
    iput p1, p0, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->mPageCount:I

    .line 260
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->invalidate()V

    .line 263
    return-void
.end method
