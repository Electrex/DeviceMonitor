.class public Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter;
.super Landroid/widget/BaseAdapter;
.source "ReviewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mEntries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mLayoutResId:I

.field private final mValues:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p3, "entries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 37
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter;->mContext:Landroid/content/Context;

    .line 38
    iput p2, p0, Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter;->mLayoutResId:I

    .line 39
    iput-object p3, p0, Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter;->mEntries:Ljava/util/ArrayList;

    .line 40
    iput-object p4, p0, Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter;->mValues:Ljava/util/ArrayList;

    .line 41
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter;->mEntries:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 45
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter;->mEntries:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 47
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "i"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 61
    if-nez p2, :cond_0

    .line 62
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iget v2, p0, Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter;->mLayoutResId:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 64
    new-instance v0, Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;

    invoke-direct {v0, p2}, Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 65
    .local v0, "holder":Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 70
    :goto_0
    # getter for: Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;->name:Landroid/widget/TextView;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;->access$000(Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter;->mEntries:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    # getter for: Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;->value:Landroid/widget/TextView;
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;->access$100(Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter;->mValues:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    return-object p2

    .line 67
    .end local v0    # "holder":Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;

    .restart local v0    # "holder":Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;
    goto :goto_0
.end method
