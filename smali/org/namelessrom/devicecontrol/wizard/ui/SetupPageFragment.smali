.class public abstract Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;
.super Landroid/app/Fragment;
.source "SetupPageFragment.java"


# instance fields
.field protected mCallbacks:Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

.field protected mKey:Ljava/lang/String;

.field protected mPage:Lorg/namelessrom/devicecontrol/wizard/setup/Page;

.field protected mRootView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract getLayoutResource()I
.end method

.method protected abstract getTitleResource()I
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 59
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 60
    const-string v0, "onActivityCreated"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;->mCallbacks:Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;->mKey:Ljava/lang/String;

    invoke-interface {v0, v1}, Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;->getPage(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;->mPage:Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    .line 62
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;->setUpPage()V

    .line 63
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 66
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 67
    instance-of v0, p1, Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    if-nez v0, :cond_0

    .line 68
    new-instance v0, Ljava/lang/ClassCastException;

    const-string v1, "Activity must implement SetupDataCallbacks"

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_0
    check-cast p1, Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;->mCallbacks:Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    .line 71
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 42
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 43
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 44
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "key_arg"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;->mKey:Ljava/lang/String;

    .line 45
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;->mKey:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 46
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "No KEY_PAGE_ARGUMENT given"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 48
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 52
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;->getLayoutResource()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;->mRootView:Landroid/view/View;

    .line 53
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;->mRootView:Landroid/view/View;

    const v2, 0x1020016

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 54
    .local v0, "titleView":Landroid/widget/TextView;
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;->getTitleResource()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 55
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;->mRootView:Landroid/view/View;

    return-object v1
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/ui/SetupPageFragment;->mCallbacks:Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;

    .line 76
    return-void
.end method

.method protected abstract setUpPage()V
.end method
