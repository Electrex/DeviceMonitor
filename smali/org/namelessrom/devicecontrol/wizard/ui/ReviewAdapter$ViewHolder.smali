.class final Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "ReviewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ViewHolder"
.end annotation


# instance fields
.field private final name:Landroid/widget/TextView;

.field private final value:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const v0, 0x1020014

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;->name:Landroid/widget/TextView;

    .line 55
    const v0, 0x1020015

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;->value:Landroid/widget/TextView;

    .line 56
    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;

    .prologue
    .line 49
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;->name:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;

    .prologue
    .line 49
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/ui/ReviewAdapter$ViewHolder;->value:Landroid/widget/TextView;

    return-object v0
.end method
