.class public Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;
.super Lorg/namelessrom/devicecontrol/activities/BaseActivity;
.source "AddTaskActivity.java"

# interfaces
.implements Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;
    }
.end annotation


# static fields
.field private static mBackPressed:J


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mNextButton:Landroid/widget/Button;

.field private mPageList:Lorg/namelessrom/devicecontrol/wizard/setup/PageList;

.field private mPagerAdapter:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;

.field private mPrevButton:Landroid/widget/Button;

.field private mSetupData:Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;

.field private mStepPagerStrip:Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;

.field private mToast:Landroid/widget/Toast;

.field private mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;-><init>()V

    .line 63
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mHandler:Landroid/os/Handler;

    .line 287
    return-void
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mStepPagerStrip:Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;

    return-object v0
.end method

.method static synthetic access$200(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)Lorg/namelessrom/devicecontrol/wizard/setup/PageList;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPageList:Lorg/namelessrom/devicecontrol/wizard/setup/PageList;

    return-object v0
.end method

.method static synthetic access$300(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPagerAdapter:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;

    return-object v0
.end method

.method static synthetic access$400(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$500(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mSetupData:Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;

    return-object v0
.end method

.method static synthetic access$600(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)V
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    .prologue
    .line 49
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->finishSetup()V

    return-void
.end method

.method static synthetic access$700(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)V
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;

    .prologue
    .line 49
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->shouldExit()V

    return-void
.end method

.method private finishSetup()V
    .locals 0

    .prologue
    .line 284
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->finish()V

    .line 285
    return-void
.end method

.method private recalculateCutOffPage()Z
    .locals 4

    .prologue
    .line 265
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPageList:Lorg/namelessrom/devicecontrol/wizard/setup/PageList;

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/wizard/setup/PageList;->size()I

    move-result v0

    .line 267
    .local v0, "cutOffPage":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPageList:Lorg/namelessrom/devicecontrol/wizard/setup/PageList;

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/wizard/setup/PageList;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 268
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPageList:Lorg/namelessrom/devicecontrol/wizard/setup/PageList;

    invoke-virtual {v3, v1}, Lorg/namelessrom/devicecontrol/wizard/setup/PageList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    .line 269
    .local v2, "page":Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->isRequired()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->isCompleted()Z

    move-result v3

    if-nez v3, :cond_1

    .line 270
    move v0, v1

    .line 275
    .end local v2    # "page":Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    :cond_0
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPagerAdapter:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;->getCutOffPage()I

    move-result v3

    if-eq v3, v0, :cond_2

    .line 276
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPagerAdapter:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;

    invoke-virtual {v3, v0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;->setCutOffPage(I)V

    .line 277
    const/4 v3, 0x1

    .line 280
    :goto_1
    return v3

    .line 267
    .restart local v2    # "page":Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 280
    .end local v2    # "page":Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private shouldExit()V
    .locals 4

    .prologue
    .line 208
    sget-wide v0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mBackPressed:J

    const-wide/16 v2, 0x7d0

    add-long/2addr v0, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 209
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 210
    :cond_0
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->finish()V

    .line 216
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mBackPressed:J

    .line 217
    return-void

    .line 212
    :cond_1
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e0015

    invoke-virtual {p0, v1}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mToast:Landroid/widget/Toast;

    .line 214
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private updateNextPreviousState()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 220
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 221
    .local v0, "position":I
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mNextButton:Landroid/widget/Button;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPagerAdapter:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;->getCutOffPage()I

    move-result v1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 222
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPrevButton:Landroid/widget/Button;

    if-gtz v0, :cond_0

    const/4 v2, 0x4

    :cond_0
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 223
    return-void

    :cond_1
    move v1, v2

    .line 221
    goto :goto_0
.end method


# virtual methods
.method public doNext()V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$7;

    invoke-direct {v1, p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$7;-><init>(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 191
    return-void
.end method

.method public doPrevious()V
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$8;

    invoke-direct {v1, p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$8;-><init>(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 205
    return-void
.end method

.method public getPage(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/wizard/setup/Page;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 246
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mSetupData:Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;

    invoke-virtual {v0, p1}, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->findPage(Ljava/lang/String;)Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    move-result-object v0

    return-object v0
.end method

.method public getSetupData()Lorg/namelessrom/devicecontrol/tasker/TaskerItem;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mSetupData:Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->getSetupData()Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 170
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->doPrevious()V

    .line 171
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    .line 69
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 70
    const v3, 0x7f040059

    invoke-virtual {p0, v3}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->setContentView(I)V

    .line 73
    const v3, 0x7f0c0061

    invoke-virtual {p0, v3}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/Toolbar;

    .line 74
    .local v2, "toolbar":Landroid/support/v7/widget/Toolbar;
    invoke-virtual {p0, v2}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 77
    new-instance v1, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$1;

    const/4 v3, -0x1

    sget-object v4, Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;->THIN:Lcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;

    invoke-direct {v1, p0, p0, v3, v4}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$1;-><init>(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;Landroid/app/Activity;ILcom/balysv/materialmenu/MaterialMenuDrawable$Stroke;)V

    .line 81
    .local v1, "materialMenu":Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_0

    .line 82
    invoke-virtual {v1, v5}, Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;->setNeverDrawTouch(Z)V

    .line 84
    :cond_0
    sget-object v3, Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;->ARROW:Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;

    invoke-virtual {v1, v3}, Lcom/balysv/materialmenu/extras/toolbar/MaterialMenuIconToolbar;->animateState(Lcom/balysv/materialmenu/MaterialMenuDrawable$IconState;)V

    .line 86
    new-instance v3, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$2;

    invoke-direct {v3, p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$2;-><init>(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mSetupData:Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;

    .line 93
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mSetupData:Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;

    if-nez v3, :cond_1

    .line 94
    new-instance v3, Lorg/namelessrom/devicecontrol/wizard/setup/TaskerSetupWizardData;

    invoke-direct {v3, p0}, Lorg/namelessrom/devicecontrol/wizard/setup/TaskerSetupWizardData;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mSetupData:Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;

    .line 97
    :cond_1
    if-eqz p1, :cond_2

    .line 98
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mSetupData:Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;

    const-string v4, "data"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->load(Landroid/os/Bundle;)V

    .line 101
    :cond_2
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "arg_item"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    .line 102
    .local v0, "item":Lorg/namelessrom/devicecontrol/tasker/TaskerItem;
    const-string v4, "TaskerItem: %s"

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    if-nez v0, :cond_5

    const-string v3, "null"

    :goto_0
    aput-object v3, v5, v6

    invoke-static {p0, v4, v5}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103
    if-eqz v0, :cond_3

    .line 104
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mSetupData:Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;

    invoke-virtual {v3, v0}, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->setSetupData(Lorg/namelessrom/devicecontrol/tasker/TaskerItem;)V

    .line 105
    const v3, 0x7f0e00ad

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 107
    :cond_3
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mSetupData:Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;

    invoke-virtual {v3}, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->getSetupData()Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    move-result-object v3

    if-nez v3, :cond_4

    .line 108
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mSetupData:Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;

    new-instance v4, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;

    invoke-direct {v4}, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;-><init>()V

    invoke-virtual {v3, v4}, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->setSetupData(Lorg/namelessrom/devicecontrol/tasker/TaskerItem;)V

    .line 111
    :cond_4
    const v3, 0x7f0c00ed

    invoke-virtual {p0, v3}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mNextButton:Landroid/widget/Button;

    .line 112
    const v3, 0x7f0c0110

    invoke-virtual {p0, v3}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPrevButton:Landroid/widget/Button;

    .line 113
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mSetupData:Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;

    invoke-virtual {v3, p0}, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->registerListener(Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;)V

    .line 114
    new-instance v3, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, p0, v4, v5}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;-><init>(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;Landroid/app/FragmentManager;Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$1;)V

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPagerAdapter:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;

    .line 115
    const v3, 0x7f0c0063

    invoke-virtual {p0, v3}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v4/view/ViewPager;

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 116
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v4, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPagerAdapter:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 117
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0031

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 118
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    new-instance v4, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$3;

    invoke-direct {v4, p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$3;-><init>(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)V

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 128
    const v3, 0x7f0c010f

    invoke-virtual {p0, v3}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;

    iput-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mStepPagerStrip:Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;

    .line 129
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mStepPagerStrip:Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;

    new-instance v4, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$4;

    invoke-direct {v4, p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$4;-><init>(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)V

    invoke-virtual {v3, v4}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->setOnPageSelectedListener(Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip$OnPageSelectedListener;)V

    .line 138
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mNextButton:Landroid/widget/Button;

    new-instance v4, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$5;

    invoke-direct {v4, p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$5;-><init>(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    iget-object v3, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPrevButton:Landroid/widget/Button;

    new-instance v4, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$6;

    invoke-direct {v4, p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$6;-><init>(Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->onPageTreeChanged()V

    .line 152
    return-void

    .line 102
    :cond_5
    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/tasker/TaskerItem;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mSetupData:Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;

    invoke-virtual {v0, p0}, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->unregisterListener(Lorg/namelessrom/devicecontrol/wizard/setup/SetupDataCallbacks;)V

    .line 161
    invoke-super {p0}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onDestroy()V

    .line 162
    return-void
.end method

.method public onPageLoaded(Lorg/namelessrom/devicecontrol/wizard/setup/Page;)V
    .locals 2
    .param p1, "page"    # Lorg/namelessrom/devicecontrol/wizard/setup/Page;

    .prologue
    .line 226
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->getNextButtonResId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 227
    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->isRequired()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->recalculateCutOffPage()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPagerAdapter:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;->notifyDataSetChanged()V

    .line 232
    :cond_0
    invoke-virtual {p1}, Lorg/namelessrom/devicecontrol/wizard/setup/Page;->refresh()V

    .line 233
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->updateNextPreviousState()V

    .line 234
    return-void
.end method

.method public onPageTreeChanged()V
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mSetupData:Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->getPageList()Lorg/namelessrom/devicecontrol/wizard/setup/PageList;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPageList:Lorg/namelessrom/devicecontrol/wizard/setup/PageList;

    .line 238
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->recalculateCutOffPage()Z

    .line 239
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPagerAdapter:Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;

    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity$CustomPagerAdapter;->notifyDataSetChanged()V

    .line 240
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->updateNextPreviousState()V

    .line 242
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mStepPagerStrip:Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mPageList:Lorg/namelessrom/devicecontrol/wizard/setup/PageList;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/wizard/setup/PageList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/wizard/ui/StepPagerStrip;->setPageCount(I)V

    .line 243
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 155
    invoke-super {p0}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onResume()V

    .line 156
    invoke-virtual {p0}, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->onPageTreeChanged()V

    .line 157
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 165
    invoke-super {p0, p1}, Lorg/namelessrom/devicecontrol/activities/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 166
    const-string v0, "data"

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/wizard/AddTaskActivity;->mSetupData:Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;

    invoke-virtual {v1}, Lorg/namelessrom/devicecontrol/wizard/setup/AbstractSetupData;->save()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 167
    return-void
.end method
