.class public Lorg/namelessrom/devicecontrol/Logger;
.super Ljava/lang/Object;
.source "Logger.java"


# static fields
.field private static DEBUG:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    sput-boolean v0, Lorg/namelessrom/devicecontrol/Logger;->DEBUG:Z

    return-void
.end method

.method public static d(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 2
    .param p0, "object"    # Ljava/lang/Object;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 36
    sget-boolean v0, Lorg/namelessrom/devicecontrol/Logger;->DEBUG:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lorg/namelessrom/devicecontrol/Logger;->getTag(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lorg/namelessrom/devicecontrol/Logger;->getMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    :cond_0
    return-void
.end method

.method public static varargs d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p0, "object"    # Ljava/lang/Object;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "objects"    # [Ljava/lang/Object;

    .prologue
    .line 40
    sget-boolean v0, Lorg/namelessrom/devicecontrol/Logger;->DEBUG:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lorg/namelessrom/devicecontrol/Logger;->getTag(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2}, Lorg/namelessrom/devicecontrol/Logger;->getMessage(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    :cond_0
    return-void
.end method

.method public static e(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 2
    .param p0, "object"    # Ljava/lang/Object;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 48
    sget-boolean v0, Lorg/namelessrom/devicecontrol/Logger;->DEBUG:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lorg/namelessrom/devicecontrol/Logger;->getTag(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lorg/namelessrom/devicecontrol/Logger;->getMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    :cond_0
    return-void
.end method

.method public static e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2
    .param p0, "object"    # Ljava/lang/Object;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 56
    sget-boolean v0, Lorg/namelessrom/devicecontrol/Logger;->DEBUG:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lorg/namelessrom/devicecontrol/Logger;->getTag(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lorg/namelessrom/devicecontrol/Logger;->getMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 57
    :cond_0
    return-void
.end method

.method public static getEnabled()Z
    .locals 1

    .prologue
    .line 33
    sget-boolean v0, Lorg/namelessrom/devicecontrol/Logger;->DEBUG:Z

    return v0
.end method

.method public static getMessage(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 116
    const-string v0, "--> %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static varargs getMessage(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 4
    .param p0, "msg"    # Ljava/lang/String;
    .param p1, "objects"    # [Ljava/lang/Object;

    .prologue
    .line 120
    const-string v0, "--> %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getTag(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "object"    # Ljava/lang/Object;

    .prologue
    .line 108
    instance-of v0, p0, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 109
    check-cast p0, Ljava/lang/String;

    .line 111
    .end local p0    # "object":Ljava/lang/Object;
    :goto_0
    return-object p0

    .restart local p0    # "object":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static i(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 2
    .param p0, "object"    # Ljava/lang/Object;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 60
    sget-boolean v0, Lorg/namelessrom/devicecontrol/Logger;->DEBUG:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lorg/namelessrom/devicecontrol/Logger;->getTag(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lorg/namelessrom/devicecontrol/Logger;->getMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    :cond_0
    return-void
.end method

.method public static varargs i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p0, "object"    # Ljava/lang/Object;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "objects"    # [Ljava/lang/Object;

    .prologue
    .line 64
    sget-boolean v0, Lorg/namelessrom/devicecontrol/Logger;->DEBUG:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lorg/namelessrom/devicecontrol/Logger;->getTag(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2}, Lorg/namelessrom/devicecontrol/Logger;->getMessage(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    :cond_0
    return-void
.end method

.method public static declared-synchronized setEnabled(Z)V
    .locals 2
    .param p0, "enable"    # Z

    .prologue
    .line 31
    const-class v0, Lorg/namelessrom/devicecontrol/Logger;

    monitor-enter v0

    :try_start_0
    sput-boolean p0, Lorg/namelessrom/devicecontrol/Logger;->DEBUG:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static v(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 2
    .param p0, "object"    # Ljava/lang/Object;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 72
    sget-boolean v0, Lorg/namelessrom/devicecontrol/Logger;->DEBUG:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lorg/namelessrom/devicecontrol/Logger;->getTag(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lorg/namelessrom/devicecontrol/Logger;->getMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :cond_0
    return-void
.end method

.method public static v(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2
    .param p0, "object"    # Ljava/lang/Object;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 80
    sget-boolean v0, Lorg/namelessrom/devicecontrol/Logger;->DEBUG:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lorg/namelessrom/devicecontrol/Logger;->getTag(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lorg/namelessrom/devicecontrol/Logger;->getMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 81
    :cond_0
    return-void
.end method

.method public static varargs v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p0, "object"    # Ljava/lang/Object;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "objects"    # [Ljava/lang/Object;

    .prologue
    .line 76
    sget-boolean v0, Lorg/namelessrom/devicecontrol/Logger;->DEBUG:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lorg/namelessrom/devicecontrol/Logger;->getTag(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2}, Lorg/namelessrom/devicecontrol/Logger;->getMessage(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    :cond_0
    return-void
.end method

.method public static w(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 2
    .param p0, "object"    # Ljava/lang/Object;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 84
    sget-boolean v0, Lorg/namelessrom/devicecontrol/Logger;->DEBUG:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lorg/namelessrom/devicecontrol/Logger;->getTag(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lorg/namelessrom/devicecontrol/Logger;->getMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    :cond_0
    return-void
.end method

.method public static varargs w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p0, "object"    # Ljava/lang/Object;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "objects"    # [Ljava/lang/Object;

    .prologue
    .line 88
    sget-boolean v0, Lorg/namelessrom/devicecontrol/Logger;->DEBUG:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lorg/namelessrom/devicecontrol/Logger;->getTag(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2}, Lorg/namelessrom/devicecontrol/Logger;->getMessage(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :cond_0
    return-void
.end method

.method public static wtf(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 2
    .param p0, "object"    # Ljava/lang/Object;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 96
    sget-boolean v0, Lorg/namelessrom/devicecontrol/Logger;->DEBUG:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lorg/namelessrom/devicecontrol/Logger;->getTag(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lorg/namelessrom/devicecontrol/Logger;->getMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :cond_0
    return-void
.end method

.method public static wtf(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2
    .param p0, "object"    # Ljava/lang/Object;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 104
    sget-boolean v0, Lorg/namelessrom/devicecontrol/Logger;->DEBUG:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lorg/namelessrom/devicecontrol/Logger;->getTag(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lorg/namelessrom/devicecontrol/Logger;->getMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 105
    :cond_0
    return-void
.end method
