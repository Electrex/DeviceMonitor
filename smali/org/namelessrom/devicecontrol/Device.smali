.class public Lorg/namelessrom/devicecontrol/Device;
.super Ljava/lang/Object;
.source "Device.java"


# static fields
.field private static final sInstance:Lorg/namelessrom/devicecontrol/Device;


# instance fields
.field public final androidId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "android_id"
    .end annotation
.end field

.field public final board:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "device_board"
    .end annotation
.end field

.field public final bootloader:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "device_bootloader"
    .end annotation
.end field

.field public hasBusyBox:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "device_has_busybox"
    .end annotation
.end field

.field public hasRoot:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "device_has_root"
    .end annotation
.end field

.field public final manufacturer:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "device_manufacturer"
    .end annotation
.end field

.field public final model:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "device_model"
    .end annotation
.end field

.field public final platformBuildType:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "platform_build_date"
    .end annotation
.end field

.field public final platformId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "platform_id"
    .end annotation
.end field

.field public final platformTags:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "platform_tags"
    .end annotation
.end field

.field public final platformType:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "platform_type"
    .end annotation
.end field

.field public final platformVersion:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "platform_version"
    .end annotation
.end field

.field public final product:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "device_product"
    .end annotation
.end field

.field public final radio:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "device_radio_version"
    .end annotation
.end field

.field public suVersion:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "device_su_version"
    .end annotation
.end field

.field public final vmLibrary:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "vm_library"
    .end annotation
.end field

.field public final vmVersion:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "vm_version"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lorg/namelessrom/devicecontrol/Device;

    invoke-direct {v0}, Lorg/namelessrom/devicecontrol/Device;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/Device;->sInstance:Lorg/namelessrom/devicecontrol/Device;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/Device;->platformVersion:Ljava/lang/String;

    .line 55
    sget-object v0, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/Device;->platformId:Ljava/lang/String;

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/Device;->platformType:Ljava/lang/String;

    .line 57
    sget-object v0, Landroid/os/Build;->TAGS:Ljava/lang/String;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/Device;->platformTags:Ljava/lang/String;

    .line 58
    sget-wide v0, Landroid/os/Build;->TIME:J

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/utils/Utils;->getDate(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/Device;->platformBuildType:Ljava/lang/String;

    .line 60
    const-string v0, "java.vm.version"

    const-string v1, "-"

    invoke-static {v0, v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/Device;->vmVersion:Ljava/lang/String;

    .line 61
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/Device;->getRuntime()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/Device;->vmLibrary:Ljava/lang/String;

    .line 63
    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/Utils;->getAndroidId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/Device;->androidId:Ljava/lang/String;

    .line 64
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/Device;->manufacturer:Ljava/lang/String;

    .line 65
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/Device;->model:Ljava/lang/String;

    .line 66
    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/Device;->product:Ljava/lang/String;

    .line 67
    sget-object v0, Landroid/os/Build;->BOARD:Ljava/lang/String;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/Device;->board:Ljava/lang/String;

    .line 68
    sget-object v0, Landroid/os/Build;->BOOTLOADER:Ljava/lang/String;

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/Device;->bootloader:Ljava/lang/String;

    .line 69
    invoke-static {}, Landroid/os/Build;->getRadioVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/Device;->radio:Ljava/lang/String;

    .line 72
    iput-boolean v2, p0, Lorg/namelessrom/devicecontrol/Device;->hasBusyBox:Z

    .line 73
    iput-boolean v2, p0, Lorg/namelessrom/devicecontrol/Device;->hasRoot:Z

    .line 74
    const-string v0, "-"

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/Device;->suVersion:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public static get()Lorg/namelessrom/devicecontrol/Device;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lorg/namelessrom/devicecontrol/Device;->sInstance:Lorg/namelessrom/devicecontrol/Device;

    return-object v0
.end method

.method private getRuntime()Ljava/lang/String;
    .locals 5

    .prologue
    .line 99
    const-string v2, "getprop persist.sys.dalvik.vm.lib.2"

    const-string v3, "-"

    invoke-static {v2, v3}, Lorg/namelessrom/devicecontrol/utils/Utils;->getCommandResult(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 100
    .local v1, "tmp":Ljava/lang/String;
    const-string v2, "-"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    const-string v2, "getprop persist.sys.dalvik.vm.lib"

    const-string v3, "-"

    invoke-static {v2, v3}, Lorg/namelessrom/devicecontrol/utils/Utils;->getCommandResult(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 105
    :cond_0
    const-string v2, "-"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 108
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/Device;->vmVersion:Ljava/lang/String;

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v1, "libdvm.so"

    .line 111
    :cond_1
    :goto_0
    const-string v2, "libdvm.so"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v0, "Dalvik"

    .line 113
    .local v0, "runtime":Ljava/lang/String;
    :goto_1
    const-string v2, "%s (%s)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 115
    return-object v1

    .line 108
    .end local v0    # "runtime":Ljava/lang/String;
    :cond_2
    const-string v1, "libart.so"

    goto :goto_0

    .line 111
    :cond_3
    const-string v2, "libart.so"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v0, "ART"

    goto :goto_1

    :cond_4
    const-string v0, "-"

    goto :goto_1
.end method


# virtual methods
.method public update()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 83
    invoke-static {}, Lcom/stericson/roottools/RootTools;->isRootAvailable()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/io/File;

    const-string v4, "/system/bin/su"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/io/File;

    const-string v4, "/system/xbin/su"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/io/File;

    const-string v4, "/system/bin/.ext/.su"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/io/File;

    const-string v4, "/system/xbin/sugote"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move v0, v1

    .line 88
    .local v0, "binaryExists":Z
    :goto_0
    if-eqz v0, :cond_2

    invoke-static {}, Lcom/stericson/roottools/RootTools;->isAccessGiven()Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_1
    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/Device;->hasRoot:Z

    .line 91
    const-string v1, "su -v"

    const-string v2, "-"

    invoke-static {v1, v2}, Lorg/namelessrom/devicecontrol/utils/Utils;->getCommandResult(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/namelessrom/devicecontrol/Device;->suVersion:Ljava/lang/String;

    .line 94
    invoke-static {}, Lcom/stericson/roottools/RootTools;->isBusyboxAvailable()Z

    move-result v1

    iput-boolean v1, p0, Lorg/namelessrom/devicecontrol/Device;->hasBusyBox:Z

    .line 95
    return-void

    .end local v0    # "binaryExists":Z
    :cond_1
    move v0, v2

    .line 83
    goto :goto_0

    .restart local v0    # "binaryExists":Z
    :cond_2
    move v1, v2

    .line 88
    goto :goto_1
.end method
