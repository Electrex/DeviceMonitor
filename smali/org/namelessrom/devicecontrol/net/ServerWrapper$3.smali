.class Lorg/namelessrom/devicecontrol/net/ServerWrapper$3;
.super Ljava/lang/Object;
.source "ServerWrapper.java"

# interfaces
.implements Lcom/koushikdutta/async/http/server/HttpServerRequestCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/net/ServerWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/net/ServerWrapper;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/net/ServerWrapper;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper$3;->this$0:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRequest(Lcom/koushikdutta/async/http/server/AsyncHttpServerRequest;Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;)V
    .locals 22
    .param p1, "req"    # Lcom/koushikdutta/async/http/server/AsyncHttpServerRequest;
    .param p2, "res"    # Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;

    .prologue
    .line 156
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/net/ServerWrapper$3;->this$0:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-boolean v0, v0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->isStopped:Z

    move/from16 v17, v0

    if-eqz v17, :cond_0

    .line 157
    const/16 v17, 0x194

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;->responseCode(I)V

    .line 158
    invoke-interface/range {p2 .. p2}, Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;->end()V

    .line 238
    :goto_0
    return-void

    .line 161
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/net/ServerWrapper$3;->this$0:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    # invokes: Lorg/namelessrom/devicecontrol/net/ServerWrapper;->isAuthenticated(Lcom/koushikdutta/async/http/server/AsyncHttpServerRequest;)Z
    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->access$000(Lorg/namelessrom/devicecontrol/net/ServerWrapper;Lcom/koushikdutta/async/http/server/AsyncHttpServerRequest;)Z

    move-result v17

    if-nez v17, :cond_1

    .line 162
    invoke-interface/range {p2 .. p2}, Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;->getHeaders()Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;->getHeaders()Lcom/koushikdutta/async/http/libcore/RawHeaders;

    move-result-object v17

    const-string v18, "WWW-Authenticate"

    const-string v19, "Basic realm=\"DeviceControl\""

    invoke-virtual/range {v17 .. v19}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->add(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const/16 v17, 0x191

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;->responseCode(I)V

    .line 166
    invoke-interface/range {p2 .. p2}, Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;->end()V

    goto :goto_0

    .line 169
    :cond_1
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v17

    const-string v18, "mounted"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_2

    .line 170
    const-string v17, "sdcard not mounted!"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;->send(Ljava/lang/String;)V

    goto :goto_0

    .line 173
    :cond_2
    const/4 v14, 0x1

    .line 174
    .local v14, "isDirectory":Z
    invoke-interface/range {p1 .. p1}, Lcom/koushikdutta/async/http/server/AsyncHttpServerRequest;->getPath()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lorg/namelessrom/devicecontrol/utils/HtmlHelper;->urlDecode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    const-string v18, "/files/"

    const-string v19, ""

    invoke-virtual/range {v17 .. v19}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 175
    .local v10, "filePath":Ljava/lang/String;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "req.getPath(): "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-interface/range {p1 .. p1}, Lcom/koushikdutta/async/http/server/AsyncHttpServerRequest;->getPath()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "filePath: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/net/ServerWrapper$3;->this$0:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    move-object/from16 v17, v0

    # getter for: Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mService:Lorg/namelessrom/devicecontrol/services/WebServerService;
    invoke-static/range {v17 .. v17}, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->access$200(Lorg/namelessrom/devicecontrol/net/ServerWrapper;)Lorg/namelessrom/devicecontrol/services/WebServerService;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    move-result-object v17

    move-object/from16 v0, v17

    iget-boolean v0, v0, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->root:Z

    move/from16 v17, v0

    if-eqz v17, :cond_3

    .line 180
    new-instance v7, Ljava/io/File;

    const-string v17, "/"

    move-object/from16 v0, v17

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 181
    .local v7, "file":Ljava/io/File;
    const-string v16, ""

    .local v16, "sdRoot":Ljava/lang/String;
    move-object v8, v7

    .line 186
    .end local v7    # "file":Ljava/io/File;
    .local v8, "file":Ljava/io/File;
    :goto_1
    if-eqz v10, :cond_c

    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    move-result v17

    if-nez v17, :cond_c

    .line 187
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v8, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 188
    .end local v8    # "file":Ljava/io/File;
    .restart local v7    # "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_4

    .line 189
    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    move-result v14

    .line 195
    :goto_2
    if-eqz v14, :cond_b

    .line 196
    invoke-virtual {v7}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v12

    .line 197
    .local v12, "fs":[Ljava/io/File;
    if-nez v12, :cond_5

    .line 198
    const-string v17, "An error occured!"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;->send(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 183
    .end local v7    # "file":Ljava/io/File;
    .end local v12    # "fs":[Ljava/io/File;
    .end local v16    # "sdRoot":Ljava/lang/String;
    :cond_3
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v7

    .line 184
    .restart local v7    # "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v16

    .restart local v16    # "sdRoot":Ljava/lang/String;
    move-object v8, v7

    .end local v7    # "file":Ljava/io/File;
    .restart local v8    # "file":Ljava/io/File;
    goto :goto_1

    .line 191
    .end local v8    # "file":Ljava/io/File;
    .restart local v7    # "file":Ljava/io/File;
    :cond_4
    const-string v17, "File or directory does not exist!"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;->send(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 201
    .restart local v12    # "fs":[Ljava/io/File;
    :cond_5
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 202
    .local v5, "directories":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 203
    .local v11, "files":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    move-object v3, v12

    .local v3, "arr$":[Ljava/io/File;
    array-length v15, v3

    .local v15, "len$":I
    const/4 v13, 0x0

    .local v13, "i$":I
    :goto_3
    if-ge v13, v15, :cond_8

    aget-object v6, v3, v13

    .line 204
    .local v6, "f":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_6

    .line 205
    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    move-result v17

    if-eqz v17, :cond_7

    .line 206
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    :cond_6
    :goto_4
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    .line 208
    :cond_7
    invoke-interface {v11, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 213
    .end local v6    # "f":Ljava/io/File;
    :cond_8
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 214
    .local v9, "fileEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/namelessrom/devicecontrol/net/ServerWrapper$FileEntry;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v17

    if-lez v17, :cond_9

    .line 215
    sget-object v17, Lorg/namelessrom/devicecontrol/utils/SortHelper;->sFileComparator:Ljava/util/Comparator;

    move-object/from16 v0, v17

    invoke-static {v5, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 216
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_9

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/io/File;

    .line 217
    .restart local v6    # "f":Ljava/io/File;
    new-instance v17, Lorg/namelessrom/devicecontrol/net/ServerWrapper$FileEntry;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/net/ServerWrapper$3;->this$0:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    move-object/from16 v18, v0

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    const-string v21, ""

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x1

    invoke-direct/range {v17 .. v21}, Lorg/namelessrom/devicecontrol/net/ServerWrapper$FileEntry;-><init>(Lorg/namelessrom/devicecontrol/net/ServerWrapper;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 221
    .end local v6    # "f":Ljava/io/File;
    .end local v13    # "i$":Ljava/util/Iterator;
    :cond_9
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v17

    if-lez v17, :cond_a

    .line 222
    sget-object v17, Lorg/namelessrom/devicecontrol/utils/SortHelper;->sFileComparator:Ljava/util/Comparator;

    move-object/from16 v0, v17

    invoke-static {v11, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 223
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .restart local v13    # "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_a

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/io/File;

    .line 224
    .restart local v6    # "f":Ljava/io/File;
    new-instance v17, Lorg/namelessrom/devicecontrol/net/ServerWrapper$FileEntry;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/namelessrom/devicecontrol/net/ServerWrapper$3;->this$0:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    move-object/from16 v18, v0

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    const-string v21, ""

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x0

    invoke-direct/range {v17 .. v21}, Lorg/namelessrom/devicecontrol/net/ServerWrapper$FileEntry;-><init>(Lorg/namelessrom/devicecontrol/net/ServerWrapper;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 229
    .end local v6    # "f":Ljava/io/File;
    .end local v13    # "i$":Ljava/util/Iterator;
    :cond_a
    new-instance v17, Lcom/google/gson/Gson;

    invoke-direct/range {v17 .. v17}, Lcom/google/gson/Gson;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;->send(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 231
    .end local v3    # "arr$":[Ljava/io/File;
    .end local v5    # "directories":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    .end local v9    # "fileEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/namelessrom/devicecontrol/net/ServerWrapper$FileEntry;>;"
    .end local v11    # "files":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    .end local v12    # "fs":[Ljava/io/File;
    .end local v15    # "len$":I
    :cond_b
    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->getInstance()Lorg/namelessrom/devicecontrol/utils/ContentTypes;

    move-result-object v17

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->getContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 233
    .local v4, "contentType":Ljava/lang/String;
    const-string v17, "Requested file: %s"

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 234
    const-string v17, "Content-Type: %s"

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v4, v18, v19

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 235
    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;->setContentType(Ljava/lang/String;)V

    .line 236
    move-object/from16 v0, p2

    invoke-interface {v0, v7}, Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;->sendFile(Ljava/io/File;)V

    goto/16 :goto_0

    .end local v4    # "contentType":Ljava/lang/String;
    .end local v7    # "file":Ljava/io/File;
    .restart local v8    # "file":Ljava/io/File;
    :cond_c
    move-object v7, v8

    .end local v8    # "file":Ljava/io/File;
    .restart local v7    # "file":Ljava/io/File;
    goto/16 :goto_2
.end method
