.class Lorg/namelessrom/devicecontrol/net/ServerWrapper$8;
.super Landroid/content/BroadcastReceiver;
.source "ServerWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/net/ServerWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/net/ServerWrapper;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/net/ServerWrapper;)V
    .locals 0

    .prologue
    .line 371
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper$8;->this$0:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 373
    const-string v4, "batteryLevel|%s"

    new-array v5, v8, [Ljava/lang/Object;

    const-string v6, "level"

    invoke-virtual {p2, v6, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 375
    .local v1, "batteryLevel":Ljava/lang/String;
    const-string v5, "batteryCharging|%s"

    new-array v6, v8, [Ljava/lang/Object;

    const-string v4, "plugged"

    invoke-virtual {p2, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "1"

    :goto_0
    aput-object v4, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 377
    .local v0, "batteryCharging":Ljava/lang/String;
    # getter for: Lorg/namelessrom/devicecontrol/net/ServerWrapper;->_sockets:Ljava/util/ArrayList;
    invoke-static {}, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->access$300()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/koushikdutta/async/http/WebSocket;

    .line 378
    .local v3, "socket":Lcom/koushikdutta/async/http/WebSocket;
    invoke-interface {v3, v1}, Lcom/koushikdutta/async/http/WebSocket;->send(Ljava/lang/String;)V

    .line 379
    invoke-interface {v3, v0}, Lcom/koushikdutta/async/http/WebSocket;->send(Ljava/lang/String;)V

    goto :goto_1

    .line 375
    .end local v0    # "batteryCharging":Ljava/lang/String;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "socket":Lcom/koushikdutta/async/http/WebSocket;
    :cond_0
    const-string v4, "0"

    goto :goto_0

    .line 381
    .restart local v0    # "batteryCharging":Ljava/lang/String;
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method
