.class Lorg/namelessrom/devicecontrol/net/ServerWrapper$2;
.super Ljava/lang/Object;
.source "ServerWrapper.java"

# interfaces
.implements Lcom/koushikdutta/async/http/server/HttpServerRequestCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/namelessrom/devicecontrol/net/ServerWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/net/ServerWrapper;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/net/ServerWrapper;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper$2;->this$0:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRequest(Lcom/koushikdutta/async/http/server/AsyncHttpServerRequest;Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;)V
    .locals 8
    .param p1, "req"    # Lcom/koushikdutta/async/http/server/AsyncHttpServerRequest;
    .param p2, "res"    # Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;

    .prologue
    .line 121
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper$2;->this$0:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    iget-boolean v4, v4, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->isStopped:Z

    if-eqz v4, :cond_0

    .line 122
    const/16 v4, 0x194

    invoke-interface {p2, v4}, Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;->responseCode(I)V

    .line 123
    invoke-interface {p2}, Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;->end()V

    .line 125
    :cond_0
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper$2;->this$0:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    # invokes: Lorg/namelessrom/devicecontrol/net/ServerWrapper;->isAuthenticated(Lcom/koushikdutta/async/http/server/AsyncHttpServerRequest;)Z
    invoke-static {v4, p1}, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->access$000(Lorg/namelessrom/devicecontrol/net/ServerWrapper;Lcom/koushikdutta/async/http/server/AsyncHttpServerRequest;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 126
    invoke-interface {p2}, Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;->getHeaders()Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    move-result-object v4

    invoke-virtual {v4}, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;->getHeaders()Lcom/koushikdutta/async/http/libcore/RawHeaders;

    move-result-object v4

    const-string v5, "WWW-Authenticate"

    const-string v6, "Basic realm=\"DeviceControl\""

    invoke-virtual {v4, v5, v6}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->add(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const/16 v4, 0x191

    invoke-interface {p2, v4}, Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;->responseCode(I)V

    .line 129
    invoke-interface {p2}, Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;->end()V

    .line 150
    :goto_0
    return-void

    .line 132
    :cond_1
    const-string v4, "[+] Received connection from: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {p1}, Lcom/koushikdutta/async/http/server/AsyncHttpServerRequest;->getHeaders()Lcom/koushikdutta/async/http/libcore/RequestHeaders;

    move-result-object v7

    invoke-virtual {v7}, Lcom/koushikdutta/async/http/libcore/RequestHeaders;->getUserAgent()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {p0, v4, v5}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 133
    iget-object v4, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper$2;->this$0:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    invoke-interface {p1}, Lcom/koushikdutta/async/http/server/AsyncHttpServerRequest;->getPath()Ljava/lang/String;

    move-result-object v5

    # invokes: Lorg/namelessrom/devicecontrol/net/ServerWrapper;->remapPath(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v4, v5}, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->access$100(Lorg/namelessrom/devicecontrol/net/ServerWrapper;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 134
    .local v3, "path":Ljava/lang/String;
    invoke-interface {p2}, Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;->getHeaders()Lcom/koushikdutta/async/http/libcore/ResponseHeaders;

    move-result-object v4

    invoke-virtual {v4}, Lcom/koushikdutta/async/http/libcore/ResponseHeaders;->getHeaders()Lcom/koushikdutta/async/http/libcore/RawHeaders;

    move-result-object v4

    const-string v5, "Content-Type"

    invoke-static {}, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->getInstance()Lorg/namelessrom/devicecontrol/utils/ContentTypes;

    move-result-object v6

    invoke-virtual {v6, v3}, Lorg/namelessrom/devicecontrol/utils/ContentTypes;->getContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/utils/HtmlHelper;->loadPath(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 138
    .local v2, "is":Ljava/io/InputStream;
    if-eqz v2, :cond_2

    .line 139
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 141
    .local v0, "dis":Ljava/io/DataInputStream;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/DataInputStream;->available()I

    move-result v4

    int-to-long v4, v4

    invoke-interface {p2, v0, v4, v5}, Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;->sendStream(Ljava/io/InputStream;J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    :try_start_1
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    goto :goto_0

    .line 143
    :catch_1
    move-exception v1

    .line 144
    .local v1, "ioe":Ljava/io/IOException;
    :try_start_2
    const-string v4, "Error!"

    invoke-static {p0, v4, v1}, Lorg/namelessrom/devicecontrol/Logger;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 146
    :try_start_3
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 149
    .end local v0    # "dis":Ljava/io/DataInputStream;
    .end local v1    # "ioe":Ljava/io/IOException;
    :cond_2
    :goto_1
    invoke-static {v3}, Lorg/namelessrom/devicecontrol/utils/HtmlHelper;->loadPathAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v4}, Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;->send(Ljava/lang/String;)V

    goto :goto_0

    .line 146
    .restart local v0    # "dis":Ljava/io/DataInputStream;
    :catchall_0
    move-exception v4

    :try_start_4
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :goto_2
    throw v4

    .restart local v1    # "ioe":Ljava/io/IOException;
    :catch_2
    move-exception v4

    goto :goto_1

    .end local v1    # "ioe":Ljava/io/IOException;
    :catch_3
    move-exception v5

    goto :goto_2
.end method
