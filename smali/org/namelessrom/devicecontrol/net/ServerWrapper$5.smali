.class Lorg/namelessrom/devicecontrol/net/ServerWrapper$5;
.super Ljava/lang/Object;
.source "ServerWrapper.java"

# interfaces
.implements Lcom/koushikdutta/async/http/server/AsyncHttpServer$WebSocketRequestCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/net/ServerWrapper;->setupWebSockets()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/net/ServerWrapper;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/net/ServerWrapper;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper$5;->this$0:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected(Lcom/koushikdutta/async/http/WebSocket;Lcom/koushikdutta/async/http/libcore/RequestHeaders;)V
    .locals 2
    .param p1, "webSocket"    # Lcom/koushikdutta/async/http/WebSocket;
    .param p2, "headers"    # Lcom/koushikdutta/async/http/libcore/RequestHeaders;

    .prologue
    .line 281
    # getter for: Lorg/namelessrom/devicecontrol/net/ServerWrapper;->_sockets:Ljava/util/ArrayList;
    invoke-static {}, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->access$300()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 282
    # getter for: Lorg/namelessrom/devicecontrol/net/ServerWrapper;->_sockets:Ljava/util/ArrayList;
    invoke-static {}, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->access$300()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 284
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper$5;->this$0:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    # invokes: Lorg/namelessrom/devicecontrol/net/ServerWrapper;->registerReceivers()V
    invoke-static {v0}, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->access$400(Lorg/namelessrom/devicecontrol/net/ServerWrapper;)V

    .line 287
    :cond_0
    new-instance v0, Lorg/namelessrom/devicecontrol/net/ServerWrapper$5$1;

    invoke-direct {v0, p0, p1}, Lorg/namelessrom/devicecontrol/net/ServerWrapper$5$1;-><init>(Lorg/namelessrom/devicecontrol/net/ServerWrapper$5;Lcom/koushikdutta/async/http/WebSocket;)V

    invoke-interface {p1, v0}, Lcom/koushikdutta/async/http/WebSocket;->setClosedCallback(Lcom/koushikdutta/async/callback/CompletedCallback;)V

    .line 297
    new-instance v0, Lorg/namelessrom/devicecontrol/net/ServerWrapper$5$2;

    invoke-direct {v0, p0}, Lorg/namelessrom/devicecontrol/net/ServerWrapper$5$2;-><init>(Lorg/namelessrom/devicecontrol/net/ServerWrapper$5;)V

    invoke-interface {p1, v0}, Lcom/koushikdutta/async/http/WebSocket;->setStringCallback(Lcom/koushikdutta/async/http/WebSocket$StringCallback;)V

    .line 307
    const-string v0, "---CONNECTED---"

    invoke-interface {p1, v0}, Lcom/koushikdutta/async/http/WebSocket;->send(Ljava/lang/String;)V

    .line 308
    return-void
.end method
