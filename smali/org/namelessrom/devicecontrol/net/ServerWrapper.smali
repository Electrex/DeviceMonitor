.class public Lorg/namelessrom/devicecontrol/net/ServerWrapper;
.super Ljava/lang/Object;
.source "ServerWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/namelessrom/devicecontrol/net/ServerWrapper$FileEntry;
    }
.end annotation


# static fields
.field private static final _sockets:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/koushikdutta/async/http/WebSocket;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final filesCallback:Lcom/koushikdutta/async/http/server/HttpServerRequestCallback;

.field private final informationCallback:Lcom/koushikdutta/async/http/server/HttpServerRequestCallback;

.field public isStopped:Z

.field private final mBatteryReceiver:Landroid/content/BroadcastReceiver;

.field private mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

.field private mServerSocket:Lcom/koushikdutta/async/AsyncServerSocket;

.field private final mService:Lorg/namelessrom/devicecontrol/services/WebServerService;

.field private final mainCallback:Lcom/koushikdutta/async/http/server/HttpServerRequestCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->_sockets:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Lorg/namelessrom/devicecontrol/services/WebServerService;)V
    .locals 1
    .param p1, "service"    # Lorg/namelessrom/devicecontrol/services/WebServerService;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->isStopped:Z

    .line 118
    new-instance v0, Lorg/namelessrom/devicecontrol/net/ServerWrapper$2;

    invoke-direct {v0, p0}, Lorg/namelessrom/devicecontrol/net/ServerWrapper$2;-><init>(Lorg/namelessrom/devicecontrol/net/ServerWrapper;)V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mainCallback:Lcom/koushikdutta/async/http/server/HttpServerRequestCallback;

    .line 153
    new-instance v0, Lorg/namelessrom/devicecontrol/net/ServerWrapper$3;

    invoke-direct {v0, p0}, Lorg/namelessrom/devicecontrol/net/ServerWrapper$3;-><init>(Lorg/namelessrom/devicecontrol/net/ServerWrapper;)V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->filesCallback:Lcom/koushikdutta/async/http/server/HttpServerRequestCallback;

    .line 241
    new-instance v0, Lorg/namelessrom/devicecontrol/net/ServerWrapper$4;

    invoke-direct {v0, p0}, Lorg/namelessrom/devicecontrol/net/ServerWrapper$4;-><init>(Lorg/namelessrom/devicecontrol/net/ServerWrapper;)V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->informationCallback:Lcom/koushikdutta/async/http/server/HttpServerRequestCallback;

    .line 371
    new-instance v0, Lorg/namelessrom/devicecontrol/net/ServerWrapper$8;

    invoke-direct {v0, p0}, Lorg/namelessrom/devicecontrol/net/ServerWrapper$8;-><init>(Lorg/namelessrom/devicecontrol/net/ServerWrapper;)V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mBatteryReceiver:Landroid/content/BroadcastReceiver;

    .line 56
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mService:Lorg/namelessrom/devicecontrol/services/WebServerService;

    .line 57
    return-void
.end method

.method static synthetic access$000(Lorg/namelessrom/devicecontrol/net/ServerWrapper;Lcom/koushikdutta/async/http/server/AsyncHttpServerRequest;)Z
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/net/ServerWrapper;
    .param p1, "x1"    # Lcom/koushikdutta/async/http/server/AsyncHttpServerRequest;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->isAuthenticated(Lcom/koushikdutta/async/http/server/AsyncHttpServerRequest;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lorg/namelessrom/devicecontrol/net/ServerWrapper;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/net/ServerWrapper;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->remapPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lorg/namelessrom/devicecontrol/net/ServerWrapper;)Lorg/namelessrom/devicecontrol/services/WebServerService;
    .locals 1
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    .prologue
    .line 42
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mService:Lorg/namelessrom/devicecontrol/services/WebServerService;

    return-object v0
.end method

.method static synthetic access$300()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->_sockets:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lorg/namelessrom/devicecontrol/net/ServerWrapper;)V
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->registerReceivers()V

    return-void
.end method

.method static synthetic access$500(Lorg/namelessrom/devicecontrol/net/ServerWrapper;)V
    .locals 0
    .param p0, "x0"    # Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->unregisterReceivers()V

    return-void
.end method

.method private isAuthenticated(Lcom/koushikdutta/async/http/server/AsyncHttpServerRequest;)Z
    .locals 9
    .param p1, "req"    # Lcom/koushikdutta/async/http/server/AsyncHttpServerRequest;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 332
    iget-object v6, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mService:Lorg/namelessrom/devicecontrol/services/WebServerService;

    invoke-static {v6}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    move-result-object v1

    .line 333
    .local v1, "configuration":Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;
    iget-boolean v6, v1, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->useAuth:Z

    if-nez v6, :cond_0

    move v2, v4

    .line 334
    .local v2, "isAuth":Z
    :goto_0
    invoke-interface {p1}, Lcom/koushikdutta/async/http/server/AsyncHttpServerRequest;->getHeaders()Lcom/koushikdutta/async/http/libcore/RequestHeaders;

    move-result-object v6

    invoke-virtual {v6}, Lcom/koushikdutta/async/http/libcore/RequestHeaders;->hasAuthorization()Z

    move-result v6

    if-eqz v6, :cond_2

    if-nez v2, :cond_2

    .line 335
    invoke-interface {p1}, Lcom/koushikdutta/async/http/server/AsyncHttpServerRequest;->getHeaders()Lcom/koushikdutta/async/http/libcore/RequestHeaders;

    move-result-object v6

    invoke-virtual {v6}, Lcom/koushikdutta/async/http/libcore/RequestHeaders;->getHeaders()Lcom/koushikdutta/async/http/libcore/RawHeaders;

    move-result-object v6

    const-string v7, "Authorization"

    invoke-virtual {v6, v7}, Lcom/koushikdutta/async/http/libcore/RawHeaders;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 336
    .local v0, "auth":Ljava/lang/String;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 337
    new-instance v6, Ljava/lang/String;

    const-string v7, "Basic"

    const-string v8, ""

    invoke-virtual {v0, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>([B)V

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 339
    .local v3, "parts":[Ljava/lang/String;
    aget-object v6, v3, v5

    if-eqz v6, :cond_1

    aget-object v6, v3, v4

    if-eqz v6, :cond_1

    aget-object v6, v3, v5

    iget-object v7, v1, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->username:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    aget-object v6, v3, v4

    iget-object v7, v1, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->password:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 345
    .end local v0    # "auth":Ljava/lang/String;
    .end local v3    # "parts":[Ljava/lang/String;
    :goto_1
    return v4

    .end local v2    # "isAuth":Z
    :cond_0
    move v2, v5

    .line 333
    goto :goto_0

    .restart local v0    # "auth":Ljava/lang/String;
    .restart local v2    # "isAuth":Z
    .restart local v3    # "parts":[Ljava/lang/String;
    :cond_1
    move v4, v5

    .line 339
    goto :goto_1

    .end local v0    # "auth":Ljava/lang/String;
    .end local v3    # "parts":[Ljava/lang/String;
    :cond_2
    move v4, v2

    .line 345
    goto :goto_1
.end method

.method private registerReceivers()V
    .locals 5

    .prologue
    .line 349
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mService:Lorg/namelessrom/devicecontrol/services/WebServerService;

    if-nez v1, :cond_1

    .line 350
    const-string v1, "mService is null!"

    invoke-static {p0, v1}, Lorg/namelessrom/devicecontrol/Logger;->wtf(Ljava/lang/Object;Ljava/lang/String;)V

    .line 359
    :cond_0
    :goto_0
    return-void

    .line 353
    :cond_1
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mService:Lorg/namelessrom/devicecontrol/services/WebServerService;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mBatteryReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lorg/namelessrom/devicecontrol/services/WebServerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    .line 356
    .local v0, "sticky":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 357
    iget-object v1, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mBatteryReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mService:Lorg/namelessrom/devicecontrol/services/WebServerService;

    invoke-virtual {v1, v2, v0}, Landroid/content/BroadcastReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private remapPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 249
    const-string v0, "/"

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    const-string p1, "index.html"

    .line 252
    .end local p1    # "path":Ljava/lang/String;
    :cond_0
    return-object p1
.end method

.method private setupApi()V
    .locals 3

    .prologue
    .line 313
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    const-string v1, "/api"

    new-instance v2, Lorg/namelessrom/devicecontrol/net/ServerWrapper$6;

    invoke-direct {v2, p0}, Lorg/namelessrom/devicecontrol/net/ServerWrapper$6;-><init>(Lorg/namelessrom/devicecontrol/net/ServerWrapper;)V

    invoke-virtual {v0, v1, v2}, Lcom/koushikdutta/async/http/server/AsyncHttpServer;->get(Ljava/lang/String;Lcom/koushikdutta/async/http/server/HttpServerRequestCallback;)V

    .line 320
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    const-string v1, "/api/device"

    new-instance v2, Lorg/namelessrom/devicecontrol/net/ServerWrapper$7;

    invoke-direct {v2, p0}, Lorg/namelessrom/devicecontrol/net/ServerWrapper$7;-><init>(Lorg/namelessrom/devicecontrol/net/ServerWrapper;)V

    invoke-virtual {v0, v1, v2}, Lcom/koushikdutta/async/http/server/AsyncHttpServer;->get(Ljava/lang/String;Lcom/koushikdutta/async/http/server/HttpServerRequestCallback;)V

    .line 329
    return-void
.end method

.method private setupFonts()V
    .locals 4

    .prologue
    .line 257
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    const-string v2, "/fonts/glyphicons-halflings-regular.eot"

    const-string v3, "fonts/glyphicons-halflings-regular.eot"

    invoke-virtual {v0, v1, v2, v3}, Lcom/koushikdutta/async/http/server/AsyncHttpServer;->directory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    const-string v2, "/fonts/glyphicons-halflings-regular.svg"

    const-string v3, "fonts/glyphicons-halflings-regular.svg"

    invoke-virtual {v0, v1, v2, v3}, Lcom/koushikdutta/async/http/server/AsyncHttpServer;->directory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    const-string v2, "/fonts/glyphicons-halflings-regular.ttf"

    const-string v3, "fonts/glyphicons-halflings-regular.ttf"

    invoke-virtual {v0, v1, v2, v3}, Lcom/koushikdutta/async/http/server/AsyncHttpServer;->directory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    const-string v2, "/fonts/glyphicons-halflings-regular.woff"

    const-string v3, "fonts/glyphicons-halflings-regular.woff"

    invoke-virtual {v0, v1, v2, v3}, Lcom/koushikdutta/async/http/server/AsyncHttpServer;->directory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    const-string v2, "/fonts/FontAwesome.otf"

    const-string v3, "fonts/FontAwesome.otf"

    invoke-virtual {v0, v1, v2, v3}, Lcom/koushikdutta/async/http/server/AsyncHttpServer;->directory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    const-string v2, "/fonts/fontawesome-webfont.eot"

    const-string v3, "fonts/fontawesome-webfont.eot"

    invoke-virtual {v0, v1, v2, v3}, Lcom/koushikdutta/async/http/server/AsyncHttpServer;->directory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    const-string v2, "/fonts/fontawesome-webfont.svg"

    const-string v3, "fonts/fontawesome-webfont.svg"

    invoke-virtual {v0, v1, v2, v3}, Lcom/koushikdutta/async/http/server/AsyncHttpServer;->directory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    const-string v2, "/fonts/fontawesome-webfont.ttf"

    const-string v3, "fonts/fontawesome-webfont.ttf"

    invoke-virtual {v0, v1, v2, v3}, Lcom/koushikdutta/async/http/server/AsyncHttpServer;->directory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    const-string v2, "/fonts/fontawesome-webfont.woff"

    const-string v3, "fonts/fontawesome-webfont.woff"

    invoke-virtual {v0, v1, v2, v3}, Lcom/koushikdutta/async/http/server/AsyncHttpServer;->directory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    return-void
.end method

.method private setupWebSockets()V
    .locals 3

    .prologue
    .line 279
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    const-string v1, "/live"

    new-instance v2, Lorg/namelessrom/devicecontrol/net/ServerWrapper$5;

    invoke-direct {v2, p0}, Lorg/namelessrom/devicecontrol/net/ServerWrapper$5;-><init>(Lorg/namelessrom/devicecontrol/net/ServerWrapper;)V

    invoke-virtual {v0, v1, v2}, Lcom/koushikdutta/async/http/server/AsyncHttpServer;->websocket(Ljava/lang/String;Lcom/koushikdutta/async/http/server/AsyncHttpServer$WebSocketRequestCallback;)V

    .line 310
    return-void
.end method

.method private unregisterReceivers()V
    .locals 2

    .prologue
    .line 362
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mService:Lorg/namelessrom/devicecontrol/services/WebServerService;

    if-nez v0, :cond_0

    .line 363
    const-string v0, "mService is null!"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->wtf(Ljava/lang/Object;Ljava/lang/String;)V

    .line 369
    :goto_0
    return-void

    .line 367
    :cond_0
    :try_start_0
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mService:Lorg/namelessrom/devicecontrol/services/WebServerService;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mBatteryReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/services/WebServerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 368
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public createServer()V
    .locals 4

    .prologue
    .line 81
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    if-eqz v0, :cond_0

    .line 116
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->registerReceivers()V

    .line 84
    new-instance v0, Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    invoke-direct {v0}, Lcom/koushikdutta/async/http/server/AsyncHttpServer;-><init>()V

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    .line 85
    const-string v0, "[!] Server created"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->setupFonts()V

    .line 88
    const-string v0, "[!] Setup fonts"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->setupWebSockets()V

    .line 91
    const-string v0, "[!] Setup websockets"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->setupApi()V

    .line 94
    const-string v0, "[!] Setup api"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v1

    const-string v2, "/license"

    const-string v3, "license.html"

    invoke-virtual {v0, v1, v2, v3}, Lcom/koushikdutta/async/http/server/AsyncHttpServer;->directory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string v0, "[!] Setup route: /license"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    const-string v1, "/files"

    new-instance v2, Lorg/namelessrom/devicecontrol/net/ServerWrapper$1;

    invoke-direct {v2, p0}, Lorg/namelessrom/devicecontrol/net/ServerWrapper$1;-><init>(Lorg/namelessrom/devicecontrol/net/ServerWrapper;)V

    invoke-virtual {v0, v1, v2}, Lcom/koushikdutta/async/http/server/AsyncHttpServer;->get(Ljava/lang/String;Lcom/koushikdutta/async/http/server/HttpServerRequestCallback;)V

    .line 103
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    const-string v1, "/files/(?s).*"

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->filesCallback:Lcom/koushikdutta/async/http/server/HttpServerRequestCallback;

    invoke-virtual {v0, v1, v2}, Lcom/koushikdutta/async/http/server/AsyncHttpServer;->get(Ljava/lang/String;Lcom/koushikdutta/async/http/server/HttpServerRequestCallback;)V

    .line 104
    const-string v0, "[!] Setup route: /files/(?s).*"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    const-string v1, "/information"

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->informationCallback:Lcom/koushikdutta/async/http/server/HttpServerRequestCallback;

    invoke-virtual {v0, v1, v2}, Lcom/koushikdutta/async/http/server/AsyncHttpServer;->get(Ljava/lang/String;Lcom/koushikdutta/async/http/server/HttpServerRequestCallback;)V

    .line 107
    const-string v0, "[!] Setup route: /information"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    const-string v1, "/(?s).*"

    iget-object v2, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mainCallback:Lcom/koushikdutta/async/http/server/HttpServerRequestCallback;

    invoke-virtual {v0, v1, v2}, Lcom/koushikdutta/async/http/server/AsyncHttpServer;->get(Ljava/lang/String;Lcom/koushikdutta/async/http/server/HttpServerRequestCallback;)V

    .line 111
    const-string v0, "[!] Setup route: /"

    invoke-static {p0, v0}, Lorg/namelessrom/devicecontrol/Logger;->v(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    iget-object v1, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mService:Lorg/namelessrom/devicecontrol/services/WebServerService;

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->get(Landroid/content/Context;)Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;

    move-result-object v1

    iget v1, v1, Lorg/namelessrom/devicecontrol/configuration/WebServerConfiguration;->port:I

    invoke-virtual {v0, v1}, Lcom/koushikdutta/async/http/server/AsyncHttpServer;->listen(I)Lcom/koushikdutta/async/AsyncServerSocket;

    move-result-object v0

    iput-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServerSocket:Lcom/koushikdutta/async/AsyncServerSocket;

    .line 115
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mService:Lorg/namelessrom/devicecontrol/services/WebServerService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/namelessrom/devicecontrol/services/WebServerService;->setNotification(Landroid/app/Notification;)V

    goto/16 :goto_0
.end method

.method public getServerSocket()Lcom/koushikdutta/async/AsyncServerSocket;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServerSocket:Lcom/koushikdutta/async/AsyncServerSocket;

    return-object v0
.end method

.method public stopServer()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 60
    invoke-direct {p0}, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->unregisterReceivers()V

    .line 62
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    if-eqz v2, :cond_0

    .line 63
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    invoke-virtual {v2}, Lcom/koushikdutta/async/http/server/AsyncHttpServer;->stop()V

    .line 64
    iput-object v3, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServer:Lcom/koushikdutta/async/http/server/AsyncHttpServer;

    .line 66
    :cond_0
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServerSocket:Lcom/koushikdutta/async/AsyncServerSocket;

    if-eqz v2, :cond_1

    .line 67
    iget-object v2, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServerSocket:Lcom/koushikdutta/async/AsyncServerSocket;

    invoke-interface {v2}, Lcom/koushikdutta/async/AsyncServerSocket;->stop()V

    .line 68
    iput-object v3, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->mServerSocket:Lcom/koushikdutta/async/AsyncServerSocket;

    .line 70
    :cond_1
    sget-object v2, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->_sockets:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/koushikdutta/async/http/WebSocket;

    .line 71
    .local v1, "socket":Lcom/koushikdutta/async/http/WebSocket;
    if-eqz v1, :cond_2

    .line 72
    const-string v2, "---TERMINATING---"

    invoke-interface {v1, v2}, Lcom/koushikdutta/async/http/WebSocket;->send(Ljava/lang/String;)V

    .line 73
    invoke-interface {v1}, Lcom/koushikdutta/async/http/WebSocket;->close()V

    goto :goto_0

    .line 75
    .end local v1    # "socket":Lcom/koushikdutta/async/http/WebSocket;
    :cond_3
    sget-object v2, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->_sockets:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 77
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper;->isStopped:Z

    .line 78
    return-void
.end method
