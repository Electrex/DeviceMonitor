.class Lorg/namelessrom/devicecontrol/net/ServerWrapper$7;
.super Ljava/lang/Object;
.source "ServerWrapper.java"

# interfaces
.implements Lcom/koushikdutta/async/http/server/HttpServerRequestCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/namelessrom/devicecontrol/net/ServerWrapper;->setupApi()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/namelessrom/devicecontrol/net/ServerWrapper;


# direct methods
.method constructor <init>(Lorg/namelessrom/devicecontrol/net/ServerWrapper;)V
    .locals 0

    .prologue
    .line 320
    iput-object p1, p0, Lorg/namelessrom/devicecontrol/net/ServerWrapper$7;->this$0:Lorg/namelessrom/devicecontrol/net/ServerWrapper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRequest(Lcom/koushikdutta/async/http/server/AsyncHttpServerRequest;Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;)V
    .locals 3
    .param p1, "req"    # Lcom/koushikdutta/async/http/server/AsyncHttpServerRequest;
    .param p2, "res"    # Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;

    .prologue
    .line 323
    invoke-static {}, Lorg/namelessrom/devicecontrol/Device;->get()Lorg/namelessrom/devicecontrol/Device;

    move-result-object v0

    .line 324
    .local v0, "device":Lorg/namelessrom/devicecontrol/Device;
    invoke-virtual {v0}, Lorg/namelessrom/devicecontrol/Device;->update()V

    .line 325
    new-instance v2, Lcom/google/gson/Gson;

    invoke-direct {v2}, Lcom/google/gson/Gson;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 326
    .local v1, "result":Ljava/lang/String;
    invoke-interface {p2, v1}, Lcom/koushikdutta/async/http/server/AsyncHttpServerResponse;->send(Ljava/lang/String;)V

    .line 327
    return-void
.end method
