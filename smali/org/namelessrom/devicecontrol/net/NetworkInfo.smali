.class public Lorg/namelessrom/devicecontrol/net/NetworkInfo;
.super Ljava/lang/Object;
.source "NetworkInfo.java"


# direct methods
.method public static getAnyIpAddress()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40
    invoke-static {}, Lorg/namelessrom/devicecontrol/net/NetworkInfo;->getWifiIp()Ljava/lang/String;

    move-result-object v0

    .line 41
    .local v0, "ip":Ljava/lang/String;
    const-string v1, "0.0.0.0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-static {v1}, Lorg/namelessrom/devicecontrol/net/NetworkInfo;->getIpAddress(Z)Ljava/lang/String;

    move-result-object v0

    .line 42
    :cond_0
    return-object v0
.end method

.method public static getIpAddress(Z)Ljava/lang/String;
    .locals 11
    .param p0, "useIPv4"    # Z

    .prologue
    .line 68
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v10

    invoke-static {v10}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 72
    .local v6, "interfaces":Ljava/util/List;, "Ljava/util/List<Ljava/net/NetworkInterface;>;"
    :goto_0
    if-nez v6, :cond_1

    const-string v9, "0.0.0.0"

    .line 90
    :cond_0
    :goto_1
    return-object v9

    .line 69
    .end local v6    # "interfaces":Ljava/util/List;, "Ljava/util/List<Ljava/net/NetworkInterface;>;"
    :catch_0
    move-exception v3

    .line 70
    .local v3, "e":Ljava/lang/Exception;
    const/4 v6, 0x0

    .restart local v6    # "interfaces":Ljava/util/List;, "Ljava/util/List<Ljava/net/NetworkInterface;>;"
    goto :goto_0

    .line 73
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/net/NetworkInterface;

    .line 74
    .local v7, "intf":Ljava/net/NetworkInterface;
    invoke-virtual {v7}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v10

    invoke-static {v10}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;

    move-result-object v1

    .line 75
    .local v1, "addrs":Ljava/util/List;, "Ljava/util/List<Ljava/net/InetAddress;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 76
    .local v0, "addr":Ljava/net/InetAddress;
    invoke-virtual {v0}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v10

    if-nez v10, :cond_3

    .line 77
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v9

    .line 78
    .local v9, "sAddr":Ljava/lang/String;
    invoke-static {v9}, Lorg/apache/http/conn/util/InetAddressUtils;->isIPv4Address(Ljava/lang/String;)Z

    move-result v8

    .line 79
    .local v8, "isIPv4":Z
    if-eqz p0, :cond_4

    .line 80
    if-eqz v8, :cond_3

    goto :goto_1

    .line 82
    :cond_4
    if-nez v8, :cond_3

    .line 83
    const/16 v10, 0x25

    invoke-virtual {v9, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 84
    .local v2, "delim":I
    if-ltz v2, :cond_0

    const/4 v10, 0x0

    invoke-virtual {v9, v10, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    goto :goto_1

    .line 90
    .end local v0    # "addr":Ljava/net/InetAddress;
    .end local v1    # "addrs":Ljava/util/List;, "Ljava/util/List<Ljava/net/InetAddress;>;"
    .end local v2    # "delim":I
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v7    # "intf":Ljava/net/NetworkInterface;
    .end local v8    # "isIPv4":Z
    .end local v9    # "sAddr":Ljava/lang/String;
    :cond_5
    const-string v9, "0.0.0.0"

    goto :goto_1
.end method

.method public static getWifiIp()Ljava/lang/String;
    .locals 8

    .prologue
    .line 46
    invoke-static {}, Lorg/namelessrom/devicecontrol/Application;->get()Lorg/namelessrom/devicecontrol/Application;

    move-result-object v5

    const-string v6, "wifi"

    invoke-virtual {v5, v6}, Lorg/namelessrom/devicecontrol/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/wifi/WifiManager;

    .line 48
    .local v4, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/wifi/WifiInfo;->getIpAddress()I

    move-result v1

    .line 49
    .local v1, "ipAddress":I
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v5

    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v5, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 50
    invoke-static {v1}, Ljava/lang/Integer;->reverseBytes(I)I

    move-result v1

    .line 53
    :cond_0
    int-to-long v6, v1

    invoke-static {v6, v7}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual {v5}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v3

    .line 57
    .local v3, "ipByteArray":[B
    :try_start_0
    invoke-static {v3}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 62
    .local v2, "ipAddressString":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 58
    .end local v2    # "ipAddressString":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 59
    .local v0, "ex":Ljava/net/UnknownHostException;
    const-string v2, "0.0.0.0"

    .restart local v2    # "ipAddressString":Ljava/lang/String;
    goto :goto_0
.end method
